# Orsetto — A framework for structured data interchange languages

Orsetto is a standalone library comprising a core toolkit...

    - Core functional data structures and processes.
    - Unicode transport, normalization, parsing and formatting.
    - General purpose packet format encoder-decoder processes.

...used to implement streaming parsers and formatters for a useful variety of
structured data interchange languages...

    - JavaScript Object Notation (JSON).
    - Concise Binary Object Representation (CBOR).
    - DjB's 64-bit Temps Atomique International (TAI64).
    - Base-64 and Base-32 binary data encodings.
    - Uniform Resource Identifier (URI).

...and other languages as need and opportunity arise.

The primary design goal of Orsetto is to provide a general common programming
interface for encoding and decoding structured application data with
specializations only as necessary for any given interchange language.

A secondary design goal is economization of compute resources for translating
structured application data between multiple interchange languages. Using a
single toolkit for implementation aspects common to a wide variety of languages
promotes reusability, and reduces duplication of functionality in programs that
use multiple interchange languages in conjunction.

In general, Orsetto is meant to facilitate separation of logic that A)
comprehends syntax, i.e. the grammar of an interchange language and the format
of protocol messages, and logic that B) comprehends semantics, i.e. the
meaning of protocol messages and transport over communication channels.

Accordingly, it includes neither logic for storage and transport, nor logic for
application data. Application protocols like HTTP, SMTP, XMPP, et cetera, are
not included.

## Community

None. This is a prototype, written mainly for programming exercise. Created by
an author interested in sustainable software for safety critical applications.

Collaborators are welcome!

## Documentation

None. Use the source code, Luke. The source code is organized as follows:

    - etc
        - src-templates: source code file starting templates.
    - src
        - cf: core functional data structures and processes.
        - ucs: Unicode transport, normalization, parsing and formatting.
        - json: JavaScript Object Notation
        - cbor: Concise Binary Object Representation

## Installing

Prerequisites:

    - ocaml >= 4.08.1

Best to use OPAM to install everything and have the dependencies managed
automatically:

```
    $ opam install orsetto
```

## Building from source

Additional prerequisites apply while building. These can be removed after
installation.

    - conjury >= 2.0.1
    - omake >= 0.10.3
    - uucd = 15.1.0

Further prerequisites apply when building with unit test execution. Again,
these can be removed after installation.

    - ounit2 >= 2.2

### Contributing

Report issues with [Jira Cloud](https://conjury.atlassian.net/browse/ORS).

You'll need Git installed for OPAM to fetch the version in active development:

```
    $ opam repo add bitbucket.org/jhw git+https://bitbucket.org/jhw/opam-personal.git#principal
```

The best way to contribute changes to Orsetto is to follow the steps in the
procedure outlined below:

    - File an issue report with the Orsetto project on Bitbucket.Org describing
      the problem motivating development of a proposed contribution.
    - Discuss the problem with the community of sustaining engineers to reach
      rough consensus on a work plan.
    - Clone the Orsetto project repository on Bitbucket.Org.
    - Clone that repository to your development host.
    - Develop your proposed change.
        - Use the _omake_ command to commence the build.
        - Results compile to the _stage_ directory.
        - Add necessary and sufficient unit tests.
        - Use the _omake test_ command to execute the unit tests locally.
        - When the tests pass, use _git commit_ and _git push_.
    - File a pull request with all changes committed to the _default_ branch.
    - Resolve issues arising from pull request validation test runs.
    - Discuss the proposed change with sustaining engineers.
    - Respond to issues after merge from continuous integration test runs.
    - Close the issue request when finished.
