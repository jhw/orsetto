(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional LL(x) parsing of Unicode text with monadic combinators. *)

(** {6 Overview}

    This module specializes the {Cf_scan} core foundation parser combinator
    interface for use with Unicode texts. A scanner is a functional
    left-shift/left-reduce parser combinator, using a state-exception monad
    over the stream of input code points annotated with their position in the
    stream.
*)

(** {6 Interface } *)

(** The signature of a Unicode scanner module. *)
module type Profile = sig

    (** The annotation system for Unicode encoded texts. *)
    module Annot: Cf_annot.Textual.Unicode.Profile

    (** The basic scanner composed with the annotation system. *)
    include Cf_scan.Profile
       with type symbol := Uchar.t
        and type position := Cf_annot.Textual.position
        and type 'a form := 'a Annot.form

    (** The optional white space parser, which recognizes a sequence of zero or
        more code points with the {White Space} property.
    *)
    val ows: unit Annot.form t

    (** The required white space parser, which recognizes a sequence of one or
        more code points with the {White Space} property except. Returns the
        unit value annotated with its location.
    *)
    val rws: unit Annot.form t

    (** A parser that recognizes a programming language identifier, i.e. a
        sequence of code points beginning with one that has the {Id Start}
        property, followed by zero or more with the {Id Continue} property.
        Returns the text of the identifier, normalized to NFC, and annotated
        with its location in the input stream.
    *)
    val ids: Ucs_text.t Annot.form t

    (** Use [of_text p s] to parse [s] with [p] and return the result. Raises
        [Not_found] if [p] does not recognize the entire text of [s].
    *)
    val of_text: 'a t -> Ucs_text.t -> 'a
end

(** Use [Create(UTF)] to make scanner module for the encoding [UTF]. *)
module Create(P: Ucs_transport.Profile): Profile

(** The distinguished scanner for UTF-8 encoded texts. *)
module UTF8: Profile

(** Use [create utf] to make a scanner module for the UTF-8 encoding
    corresponding to [utf].
*)
val create: [< Ucs_transport.id ] -> (module Profile)

(*--- End ---*)
