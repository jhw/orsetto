(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type level = Level of { shift: int; mask: int }

let index (Level h) c = ((Uchar.to_int c) lsr h.shift) land h.mask

module Set = struct
    type t = Trie of string array

    let h0 = Level { shift = 11; mask = 0x21f }
    let h1 = Level { shift = 3; mask = 0xff }
    let h2 = Level { shift = 0; mask = 0x7; }

    let of_seq =
        let loop t0 c =
            let i0 = index h0 c in
            assert (i0 >= 0 && i0 < Array.length t0);
            let t1 = Array.unsafe_get t0 i0 in
            let t1 =
                if Bytes.length t1 > 0 then
                    t1
                else
                    let Level h = h1 in
                    let t1 = Bytes.make (succ h.mask) '\000' in
                    Array.unsafe_set t0 i0 t1;
                    t1
            in
            let i1 = index h1 c in
            assert (i1 >= 0 && i1 < Bytes.length t1);
            let t2 = Bytes.unsafe_get t1 i1 in
            let i2 = index h2 c in
            assert (i2 >= 0 && i2 < 8);
            Char.code t2 lor (1 lsl i2) |> Char.chr |> Bytes.unsafe_set t1 i1
        in
        let enter s =
            let Level h = h0 in
            let t0 = Array.make (succ h.mask) Bytes.empty in
            Cf_seq.iterate (loop t0) s;
            Trie (Array.map Bytes.unsafe_to_string t0)
        in
        enter

    let member c (Trie t0) =
        let i0 = index h0 c in
        assert (i0 >= 0 && i0 < Array.length t0);
        let t1 = Array.unsafe_get t0 i0 in
        if String.length t1 = 0 then
            false
        else
            let i1 = index h1 c in
            assert (i1 >= 0 && i1 < String.length t1);
            let t2 = String.unsafe_get t1 i1 in
            let i2 = index h2 c in
            assert (i2 >= 0 && i2 < 8);
            Char.code t2 land (1 lsl i2) <> 0

    module Unsafe = struct
        let import t0 = Trie t0
        let export (Trie t0) = t0
    end
end

module Map = struct
    type 'a t = Trie of 'a option array array array

    let h0 = Level { shift = 14; mask = 0x43 }
    let h1 = Level { shift = 7; mask = 0x7f }
    let h2 = Level { shift = 0; mask = 0x7f }

    let of_seq =
        let loop t0 (c, v) =
            let _: 'a option array array array = t0 in
            let i0 = index h0 c in
            assert (i0 >= 0 && i0 < Array.length t0);
            let t1 = Array.unsafe_get t0 i0 in
            let t1: 'a option array array =
                if Array.length t1 > 0 then
                    t1
                else
                    let Level h = h1 in
                    let t1 = Array.make (succ h.mask) [| |] in
                    Array.unsafe_set t0 i0 t1;
                    t1
            in
            let i1 = index h1 c in
            assert (i1 >= 0 && i1 < Array.length t1);
            let t2 = Array.unsafe_get t1 i1 in
            let t2: 'a option array =
                if Array.length t2 > 0 then
                    t2
                else
                    let Level h = h2 in
                    let t2 = Array.make (succ h.mask) None in
                    Array.unsafe_set t1 i1 t2;
                    t2
            in
            let i2 = index h2 c in
            assert (i2 >= 0 && i2 < Array.length t2);
            Array.unsafe_set t2 i2 (Some v)
        in
        let enter s =
            let Level h = h0 in
            let t0 = Array.make (succ h.mask) [| |] in
            Cf_seq.iterate (loop t0) s;
            Trie t0
        in
        enter

    type ('i, 'r) ret = Ret of {
        none: unit -> 'r;
        some: 'i -> 'r;
    }

    let find (Ret r) c (Trie t0) =
        let i0 = index h0 c in
        assert (i0 >= 0 && i0 < Array.length t0);
        let t1 = Array.unsafe_get t0 i0 in
        if Array.length t1 = 0 then
            r.none ()
        else
            let i1 = index h1 c in
            assert (i1 >= 0 && i1 < Array.length t1);
            let t2 = Array.unsafe_get t1 i1 in
            let i2 = index h2 c in
            assert (i2 >= 0 && i2 < 8);
            match Array.unsafe_get t2 i1 with
            | None -> r.none ()
            | Some v -> r.some v

    let member =
        let none () = false and some _ = true in
        let r = Ret { none; some } in
        fun m -> find r m

    let search =
        let none () = None and some m = Some m in
        let r = Ret { none; some } in
        fun m -> find r m

    let require =
        let none () = raise Not_found and some m = m in
        let r = Ret { none; some } in
        fun m -> find r m

    module Unsafe = struct
        let import t0 = Trie t0
        let export (Trie t0) = t0
    end
end

(*--- End ---*)
