(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Profile = sig
    val descript: string
    val size_of_uchar: Uchar.t -> int
    val uchar_decode_scheme: Uchar.t Cf_decode.scheme
    val uchar_encode_scheme: Uchar.t Cf_encode.scheme
    val bom_decode_scheme: unit Cf_decode.scheme
    val bom_encode_scheme: unit Cf_encode.scheme
    val seq_of_scanner: #Cf_decode.scanner -> Uchar.t Seq.t
    val seq_of_string: string -> Uchar.t Seq.t
    val seq_of_slice: string Cf_slice.t -> Uchar.t Seq.t
    val seq_to_emitter: #Cf_encode.emitter -> Uchar.t Seq.t -> unit
    val seq_to_string: Uchar.t Seq.t -> string
    val validate_string: string -> unit
    val validate_slice: string Cf_slice.t -> unit
    val is_valid_string: string -> bool
    val is_valid_slice: string Cf_slice.t -> bool
end

module Private = struct
    let bom = Uchar.of_int 0xFEFF

    let decode_invalid p msg =
        Printf.sprintf "Ucs_transport: %s!" msg |> Cf_decode.invalid p

    module type Basis = sig
        val descript: string
        val size_of_uchar: Uchar.t -> int
        val size_of_codeunit: int

        val decode_ck:
            string -> Cf_decode.position -> Cf_decode.size ->
            int Cf_decode.analysis

        val encode_wr: int -> bytes -> int -> unit
    end

    module Create(B: Basis) = struct
        let descript = B.descript

        let size_of_uchar = B.size_of_uchar

        let uchar_decode_scheme =
            let rd cp _ =
                assert (Uchar.is_valid cp);
                Uchar.unsafe_of_int cp
            in
            Cf_decode.scheme B.size_of_codeunit B.decode_ck rd

        let uchar_encode_scheme =
            let ck _ i c =
                let n = B.size_of_uchar c and cp = Uchar.to_int c in
                Cf_encode.analyze i n cp
            in
            Cf_encode.scheme B.size_of_codeunit ck B.encode_wr

        let seq_of_scanner sxr s =
            let sxr = (sxr :> Cf_decode.scanner) in
            Cf_decode.scanner_to_vals uchar_decode_scheme sxr s

        let seq_of_string str =
            Cf_decode.string_to_vals uchar_decode_scheme str

        let seq_of_slice =
            Cf_decode.slice_to_vals uchar_decode_scheme

        let seq_to_emitter exr s =
            let exr = (exr :> Cf_encode.emitter) in
            Seq.iter (exr#emit uchar_encode_scheme) s

        let seq_to_string s =
            let b = Buffer.create 0 in
            let exr = Cf_encode.buffer_emitter b in
            Seq.iter (exr#emit uchar_encode_scheme) s;
            Buffer.contents b

        let bom_decode_scheme =
            let x0 = Uchar.to_int bom in
            let malformed p = decode_invalid p "invalid byte order mark." in
            let ck s p n =
                let Cf_decode.Analysis (_, x) = B.decode_ck s p n in
                if x <> x0 then
                    malformed p
                else
                    Cf_decode.analyze n B.size_of_codeunit ()
            in
            let rd () _ = () in
            Cf_decode.scheme B.size_of_codeunit ck rd

        let bom_encode_scheme =
            let open Cf_encode in
            let f _pos () = bom in
            map f uchar_encode_scheme

        let validate_scanner sxr n =
            let sxr = (sxr :> Cf_decode.scanner) in
            while let Cf_decode.Position i = sxr#position in i < n do
                ignore (sxr#scan uchar_decode_scheme)
            done

        let validate_string s =
            validate_scanner (Cf_decode.string_scanner s) (String.length s)

        let validate_slice sl =
            validate_scanner (Cf_decode.slice_scanner sl) (Cf_slice.length sl)

        let is_valid_scanner sxr n =
            try
                validate_scanner sxr n;
                true
            with
            | Cf_decode.Invalid _
            | Cf_decode.Incomplete _ ->
                false

        let is_valid_string s =
            is_valid_scanner (Cf_decode.string_scanner s) (String.length s)

        let is_valid_slice sl =
            is_valid_scanner (Cf_decode.slice_scanner sl) (Cf_slice.length sl)
    end
end

(*--- End ---*)
