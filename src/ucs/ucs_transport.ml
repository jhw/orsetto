(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Profile = Ucs_transport_aux.Profile

module UTF8 = Ucs_transport_utf8

module UTF16be = Ucs_transport_utf16.BE
module UTF16le = Ucs_transport_utf16.LE
module UTF16se = Ucs_transport_utf16.SE

module UTF32be = Ucs_transport_utf32.BE
module UTF32le = Ucs_transport_utf32.LE
module UTF32se = Ucs_transport_utf32.SE

type id = [
    | `UTF8
    | `UTF16be
    | `UTF16le
    | `UTF16se
    | `UTF32be
    | `UTF32le
    | `UTF32se
]

let create = function
    | `UTF8 -> (module UTF8 : Profile)
    | `UTF16be -> (module UTF16be : Profile)
    | `UTF16le -> (module UTF16le : Profile)
    | `UTF16se -> (module UTF16se : Profile)
    | `UTF32be -> (module UTF32be : Profile)
    | `UTF32le -> (module UTF32le : Profile)
    | `UTF32se -> (module UTF32se : Profile)

(*--- End ---*)
