(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Ucs_transport_aux.Private

module D = Cf_decode
(* module E = Cf_encode *)

type aux = Aux of { n: int; m: int; d: int; p: int; q: int }

let mkaux n m p q =
    assert (n >= 0);
    let d = p lxor 0xff in
    Aux { n; m; d; p; q }

let tuple =
    mkaux 0 0x7f 0b11000000 0,
    mkaux 1 0x7ff 0b11100000 0b11000000,
    mkaux 2 0x7fff 0b11110000 0b11100000,
    mkaux 3 0x7ffff 0b11111000 0b11110000,
    mkaux 4 0x10ffff 0b11111100 0b11111000

module Basis = struct
    let descript = "UTF-8"

    let size_of_uchar c =
        let x = Uchar.to_int c in
        let Aux a =
            match tuple with
            | (Aux a as aux), _, _, _, _ when x = x land a.m -> aux
            | _, (Aux a as aux), _, _, _ when x = x land a.m -> aux
            | _, _, (Aux a as aux), _, _ when x = x land a.m -> aux
            | _, _, _, (Aux a as aux), _ when x = x land a.m -> aux
            | _, _, _, _, (Aux _ as aux)                     -> aux
        in
        succ a.n

    let size_of_codeunit = 1

    let malformed p = decode_invalid p "invalid UTF-8 form"
    let bad_code_point p = decode_invalid p "invalid code point"

    let decode_ck =
        let get_u8 s i =
            assert (i >= 0 && i < String.length s);
            String.unsafe_get s i |> Char.code
        in
        let rec loop p cp s i n =
            let u8 = get_u8 s i in
            if u8 < 0b10000000 || u8 > 0b10111111 then malformed p;
            let cp = (cp lsl 6) lor (u8 land 0b111111) in
            if n > 0 then (loop[@tailloop]) p cp s (succ i) (pred n) else cp
        in
        let enter s p n =
            let D.Position i = p in
            let u8 = get_u8 s i in
            if u8 > 0b11111100 then malformed p;
            let Aux a =
                match tuple with
                | (Aux a as aux), _, _, _, _ when u8 < a.p -> aux
                | _, (Aux a as aux), _, _, _ when u8 < a.p -> aux
                | _, _, (Aux a as aux), _, _ when u8 < a.p -> aux
                | _, _, _, (Aux a as aux), _ when u8 < a.p -> aux
                | _, _, _, _, (Aux _ as aux)               -> aux
            in
            let analyze = D.analyze n (succ a.n) in
            let cp = if a.n > 0 then
                loop p (u8 land a.d) s (succ i) (pred a.n)
            else
                u8
            in
            if not (Uchar.is_valid cp) then bad_code_point p;
            analyze cp
        in
        enter

    let encode_wr =
        let rec loop (Aux a as aux) i0 b i x =
            assert (i >= 0 && i < Bytes.length b);
            if i > i0 then begin
                let c = (x land 0x3f) lor 0x80 in
                Char.unsafe_chr c |> Bytes.unsafe_set b i;
                (loop[@tailcall]) aux i0 b (pred i) (x lsr 6)
            end
            else begin
                let c = x lor a.q in
                Char.unsafe_chr c |> Bytes.unsafe_set b i
            end
        in
        let enter cp b i =
            let Aux a as aux =
                match tuple with
                | a, _, _, _, _ when cp = cp land 0x7f ->     a
                | _, a, _, _, _ when cp = cp land 0x7ff ->    a
                | _, _, a, _, _ when cp = cp land 0x7fff ->   a
                | _, _, _, a, _ when cp = cp land 0x7ffff ->  a
                | _, _, _, _, a                            -> a
            in
            loop aux i b (i + a.n) cp
        in
        enter
end

include Create(Basis)

(*--- End ---*)
