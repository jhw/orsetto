(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Unicode Normalization Form *)

(** {6 Interface} *)

(** The module type of normalization forms. *)
module type Profile = sig
    open Ucs_db_aux

    (** The quick-check property corresponding to the form. *)
    val quick_check: qc map

    (** Use [boundary_check s] to consume [s], and if possible, quickly find
        whether the sequence conforms to the normalization form.

        Returns [QC_yes] if [s] is normalized, or [QC_no] if [s] is not
        normalized, and [QC_maybe] if the predicate can only be determined by
        the more costly function of comparing the sequence with the output of
        the [transform] function (see below).
    *)
    val boundary_check: Uchar.t Seq.t -> qc

    (** Use [transform s] to normalize [s] according to the form. *)
    val transform: Uchar.t Seq.t -> Uchar.t Seq.t
end

(** The fully-composed canonical normal form. *)
module NFC: Profile

(** The fully-decomposed canonical normal form. *)
module NFD: Profile

(** The fully-composed compatibility normal form. *)
module NFKC: Profile

(** The fully-decomposed compatibility normal form. *)
module NFKD: Profile

(** Identifiers *)
type id = [ `NFC | `NFD | `NFKC | `NFKD ]

(** Use [create e] to select the normalization form identified by [e]. *)
val create: [< id ] -> (module Profile)

(*--- End ---*)
