(*---------------------------------------------------------------------------*
  Copyright (C) 2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(*** Functional Unicode lexical analyzers. *)

(** {6 Overview}

    This module implements functional combinators for parsing languages that
    comprise sequences of regular tokens recognized by a deterministic finite
    automaton, using the {Ucs_regx} module.
*)

(** The speciailization of the {Cf_lex_scan.Profile} signature for Unicode *)
module type Profile = Cf_lex_scan.Profile
   with type symbol := Uchar.t
    and type lexeme := Ucs_text.t

(** Use [Create(S)] to make a lexical analyzer for the Unicode scanner [S]. *)
module Create(S: Ucs_scan.Profile): Profile
   with type 'a t := 'a S.t
    and type 'a form := 'a S.Annot.form

(** A distinguished lexical analyzer for the UTF-8 scanner. *)
module UTF8: Profile
   with type 'a t := 'a Ucs_scan.UTF8.t
    and type 'a form := 'a Ucs_scan.UTF8.Annot.form

(*--- End ---*)
