(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let jout = Cf_journal.stdout
let jerr = Cf_journal.stdout

module Aux = Ucs_ucdgen_aux
module DbAux = Ucs_db_aux

module XP = struct
    type 'a t = {
        m: 'a Aux.map;
        s: string;
    }

    let emit:
        type a. pp:Format.formatter -> vn:string -> vt:string ->
        v0s:(a -> string) -> ?qe:string -> a t -> unit
    = fun ~pp ~vn ~vt ~v0s ?qe xp ->
        let open Format in
        fprintf pp "let %s =\n" vn;
        let Aux.Map m = xp.m in
        fprintf pp "  let v0: %s = %s in\n" vt (v0s m.v0);
        let qe =
            match qe with
            | None -> "None"
            | Some qe -> Printf.sprintf "Some (%s)" qe
        in
        fprintf pp "  let q = %s in\n" qe;
        fprintf pp "  let s = \"%s\" in\n" (String.escaped xp.s);
        match Lazy.force m.tbl with
        | Aux.Tbl_dset _ ->
            fprintf pp "  Aux.of_exported_dbool ~v0 ?q s\n\n";
            jout#info "Generated %s as discrete set." vn
        | Aux.Tbl_rset _ ->
            fprintf pp "  Aux.of_exported_rbool ~v0 ?q s\n\n";
            jout#info "Generated %s as interval set." vn
        | Aux.Tbl_dmap _ ->
            fprintf pp "  Aux.of_exported_duniv ~v0 ?q s\n\n";
            jout#info "Generated %s as discrete map." vn
        | Aux.Tbl_rmap _ ->
            fprintf pp "  Aux.of_exported_runiv ~v0 ?q s\n\n";
            jout#info "Generated %s as interval map." vn

    let mk: type a. a Aux.map -> a t =
        let mflags = Marshal.([ No_sharing; Compat_32 ]) in
        fun m ->
            let s =
                let Aux.Map m = m in
                match Lazy.force m.tbl with
                | Aux.Tbl_dset u ->
                    let w2 = Aux.D_set.Unsafe.export u in
                    Marshal.to_string w2 mflags
                | Aux.Tbl_rset u ->
                    let w2 = Aux.R_set.Unsafe.export u in
                    Marshal.to_string w2 mflags
                | Aux.Tbl_dmap m ->
                    let w3 = Aux.D_map.Unsafe.export m in
                    Marshal.to_string w3 mflags
                | Aux.Tbl_rmap m ->
                    let w3 = Aux.R_map.Unsafe.export m in
                    Marshal.to_string w3 mflags
            in
            { m; s }

    let size xp = String.length xp.s

    let of_filter f =
        let pa = mk @@ Aux.of_filter ~v0:false ~t:`D f in
        let n, p = size pa, pa in
        let pb = mk @@ Aux.of_filter ~v0:true ~t:`D f in
        let nb = size pb in
        let n, p = if nb < n then nb, pb else n, p in
        let pc = mk @@ Aux.of_filter ~v0:false ~t:`R f in
        let nc = size pc in
        let n, p = if nc < n then nc, pc else n, p in
        let pd = mk @@ Aux.of_filter ~v0:true ~t:`R f in
        let nd = size pd in
        if nd < n then pd else p

    let of_optmap ~v0 ?(eq = ( == )) f =
        let pa = mk @@ Aux.of_optmap ~v0 f in
        let n, _ = size pa, pa in
        let pb = mk @@ Aux.of_optmap ~v0 ~eq f in
        let nb = size pb in
        if nb < n then pb else pa

    let of_uucd_property_bool ~db ~v0 prop =
        let f cp =
            assert (Uucd.is_scalar_value cp);
            match Uucd.cp_prop db cp prop with
            | None ->
                jerr#fail "Uucd: property undefined for code point u+%04X" cp
            | Some v ->
                v != v0
        in
        of_filter f

    let of_uucd_property_univ ~db ~v0 ~xf ?eq prop =
        let equal = match eq with None -> ( == ) | Some f -> f in
        let f cp =
            assert (Uucd.is_scalar_value cp);
            match Uucd.cp_prop db cp prop with
            | None ->
                jerr#fail "Uucd: property undefined for code point u+%04X" cp
            | Some v ->
                let v = xf v in
                if equal v v0 then None else Some (cp, v)
        in
        of_optmap ~v0 ?eq f
end

type ('a, 'b) pcons = Pcons of {
    snym: string;
    cname: string;
    prop: 'a Uucd.prop;
    vstr: string -> string;
}

let mkpcons ~snym ~cname ~prop ~vstr = Pcons { snym; cname; prop; vstr }

module P_bool = struct
    type t = bool

    let vt = "bool"
    let v0 = false
    let v0s = function true -> "true" | false -> "false"
    let xf (b : bool) = b

    let vstr = function
        | "N" -> "false"
        | "Y" -> "true"
        | s -> jerr#fail "Unrecognized boolean property value: '%s'" s

    let generate pp db (Pcons c) =
        let qe =
            match c.snym with
            | "grbase" ->
                let vn = String.lowercase_ascii c.cname in
                Some (Printf.sprintf "DbAux.Quick.%s" vn)
            | _ ->
                None
        in
        let vn = String.lowercase_ascii c.cname in
        XP.of_uucd_property_bool ~db ~v0 c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ?qe
end

module P_int = struct
    type t = int

    let vt = "int"
    let v0 = 0
    let v0s = string_of_int
    let xf (n : int) = n

    let vstr s =
        match int_of_string s with
        | exception _ ->
            jerr#fail "Unrecognized integer property value: '%s'" s
        | _ ->
            s

    let generate pp db (Pcons c) =
        let qe =
            match c.snym with
            | "ccc" ->
                let vn = String.lowercase_ascii c.cname in
                Some (Printf.sprintf "DbAux.Quick.%s" vn)
            | _ ->
                None
        in
        let vn = String.lowercase_ascii c.cname in
        XP.of_uucd_property_univ ~db ~v0 ~xf c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ?qe
end

module P_general_category = struct
    type t = DbAux.gc

    let vt = "DbAux.gc"
    let v0 = `Cn (* Unassigned *)
    let eq = DbAux.equal_gc
    let v0s = DbAux.show_gc
    let xf v = (v :> t)
    let vstr v = Printf.sprintf "`%s" v

    let generate pp db (Pcons c) =
        let vn = String.lowercase_ascii c.cname in
        XP.of_uucd_property_univ ~db ~v0 ~xf ~eq c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ?qe:None
end

module P_quick_check = struct
    type t = DbAux.qc

    let vt = "DbAux.qc"
    let v0 = DbAux.QC_yes
    let eq = DbAux.equal_qc
    let v0s = DbAux.show_qc

    let xf = function
        | `True -> DbAux.QC_yes
        | `False -> DbAux.QC_no
        | `Maybe -> DbAux.QC_maybe

    let vstr = function
        | "Y" -> "DbAux.QC_yes"
        | "N" -> "DbAux.QC_no"
        | "M" -> "DbAux.QC_maybe"
        | s -> jerr#fail "Unrecognized quick check property value: '%s'" s

    let generate pp db (Pcons c) =
        let vn = String.lowercase_ascii c.cname in
        let qe = Printf.sprintf "DbAux.Quick.%s" vn in
        XP.of_uucd_property_univ ~db ~v0 ~xf ~eq c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ~qe
end

module P_block = struct
    type t = DbAux.blk

    let vt = "DbAux.blk"
    let v0 = `No_Block_Assigned
    let eq = DbAux.equal_blk
    let v0s = DbAux.show_blk
    let xf v = (v :> t)
    let vstr v = Printf.sprintf "`%s" v

    let generate pp db (Pcons c) =
        let vn = String.lowercase_ascii c.cname in
        let qe = Printf.sprintf "DbAux.Quick.%s" vn in
        XP.of_uucd_property_univ ~db ~v0 ~xf ~eq c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ~qe
end

module P_script = struct
    type t = DbAux.script

    let vt = "DbAux.script"
    let v0 = `Zzzz
    let eq = DbAux.equal_script
    let v0s = DbAux.show_script
    let xf v = (v :> t)
    let vstr v = Printf.sprintf "`%s" v

    let generate pp db (Pcons c) =
        let vn = String.lowercase_ascii c.cname in
        XP.of_uucd_property_univ ~db ~v0 ~xf ~eq c.prop |>
        XP.emit ~pp ~vn ~vt ~v0s ?qe:None
end

let uucd_property_require db prop cp =
    assert (Uucd.is_scalar_value cp);
    match Uucd.cp_prop db cp prop with
    | Some v -> v
    | None -> failwith "Uucd: property undefined for code point."

module Int_map = Cf_rbtree.Map.Create(Cf_relations.Int)
module String_map = Cf_rbtree.Map.Create(String)

let header pp () = Format.pp_print_string pp "\
  (*---\n\
  | Copyright (C) 2017-2019, james woodyatt\n\
  | All rights reserved.\n\
  | \n\
  | Redistribution and use in source and binary forms, with or without\n\
  | modification, are permitted provided that the following conditions\n\
  | are met:\n\
  | \n\
  |  Redistributions of source code must retain the above copyright\n\
  |  notice, this list of conditions and the following disclaimer.\n\
  |  \n\
  |  Redistributions in binary form must reproduce the above copyright\n\
  |  notice, this list of conditions and the following disclaimer in\n\
  |  the documentation and/or other materials provided with the\n\
  |  distribution\n\
  | \n\
  | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS\n\
  | \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT\n\
  | LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS\n\
  | FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE\n\
  | COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,\n\
  | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n\
  | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR\n\
  | SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)\n\
  | HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,\n\
  | STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)\n\
  | ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED\n\
  | OF THE POSSIBILITY OF SUCH DAMAGE.\n\
  +---*)\n\
  \n\
  (* WARNING! Do not edit. This file was automatically generated. *)\n\
  \n\
  module Aux = Ucs_ucdgen_aux\n\
  module DbAux = Ucs_db_aux\n\
  \n"

let trailer pp () =
    Format.pp_print_string pp "(*--- End of generated file ---*)\n"

module PropDbCommon = struct
    module S = Cf_scan.ASCII
    module CS = Cf_chain_scan.ASCII
    module LS = Cf_lex_scan.ASCII
    open S.Affix
    open LS.Affix

    let exp_name = !$ {|[-_\i]+|}
    let exp_numeric = !$ {|\d+(\.\d+)?|}
    let exp_value = exp_name $| exp_numeric

    let s_name = LS.simple exp_name
    let s_value = LS.simple exp_value

    let s_comment =
        let* _ = S.one '#' in
        let* _ = S.ign (function '\n' -> false | _ -> true) in
        S.return '\n'

    let s_newline =
        let* _ = S.owsl in
        S.alt [ S.one '\n'; s_comment ]

    let s_separator =
        let* _ = S.owsl in
        let* _ = S.one ';' in
        let* _ = S.owsl in
        S.return ()

    let chn_separator = CS.mk s_separator
end

module PropAliases = struct
    include PropDbCommon
    open S.Affix

    let s_search_nym = s_name >>: Aux.search_query_nym

    let s_alias =
        let* sname = s_name in
        let* _ = s_separator in
        let* cname = s_name in
        let* anymsopt =
            S.opt begin
                let* _ = s_separator in
                CS.seq ~c:chn_separator ~a:1 s_search_nym
            end
        in
        let* _ = S.seq s_newline in
        let anyms = match anymsopt with None -> [] | Some anyms -> anyms in
        let anyms = Aux.search_query_nym sname :: anyms in
        let anyms = Aux.search_query_nym cname :: anyms in
        S.return (cname, sname, anyms)

    let s_file =
        let* _ = S.seq s_newline in
        S.seq s_alias

    let load =
        let put cname sname pm nym =
            String_map.replace (nym, (cname, sname)) pm
        in
        let rec loop pm = function
            | [] ->
                pm
            | hd :: tl ->
                let cname, sname, anyms = hd in
                let nyms = cname :: sname :: anyms in
                let pm = List.fold_left (put cname sname) pm nyms in
                loop pm tl
        in
        let enter fname =
            let ic = open_in fname in
            let s = Cf_seq.of_buffered_channel 1024 ic in
            match Cf_scan.ASCII.of_seq s_file s with
            | exception Not_found ->
                close_in ic;
                failwith "Cannot parse PropertyAliases.txt"
            | exception x ->
                close_in ic;
                raise x
            | aliases ->
                close_in ic;
                let len = List.length aliases in
                jout#info "Loaded %d records from %s" len fname;
                loop String_map.nil aliases
        in
        enter
end

module PropValAliases = struct
    include PropDbCommon
    open S.Affix

    let s_search_nym = s_value >>: Aux.search_query_nym

    let s_valalias =
        let* pname  = s_name in
        let* _      = s_separator in
        let* sval   = s_value in
        let* _      = s_separator in
        let* cval   = s_value in
        let* anymsopt =
            S.opt begin
                let* _ = s_separator in
                CS.seq ~c:chn_separator s_search_nym
            end
        in
        let* _ = S.seq ~a:1 s_newline in
        let anyms = match anymsopt with None -> [] | Some anyms -> anyms in
        let anyms = Aux.search_query_nym sval :: anyms in
        let anyms = Aux.search_query_nym cval :: anyms in
        S.return (pname, sval, anyms)

    let s_file =
        let* _ = S.seq s_newline in
        S.seq s_valalias

    let load =
        let put sval vm nym =
            String_map.replace (nym, sval) vm
        in
        let rec loop pvm pm = function
            | [] ->
                pvm
            | hd :: tl ->
                let pname, sval, anyms = hd in
                let pnym = Aux.search_query_nym pname in
                if not (String_map.member pnym pm) then
                    jerr#fail
                      "Property value aliases for unknown property nym '%s'"
                      pnym;
                let _, sname = String_map.require pnym pm in
                let snym = Aux.search_query_nym sname in
                let vm =
                    match String_map.search snym pvm with
                    | None -> String_map.nil
                    | Some vm -> vm
                in
                let vm = List.fold_left (put sval) vm anyms in
                let pvm = String_map.replace (pnym, vm) pvm in
                loop pvm pm tl

        in
        let enter pm fname =
            let ic = open_in fname in
            let s = Cf_seq.of_buffered_channel 16384 ic in
            match Cf_scan.ASCII.of_seq s_file s with
            | exception Not_found ->
                close_in ic;
                failwith "Cannot parse PropertyValueAliases.txt"
            | exception x ->
                close_in ic;
                raise x
            | aliases ->
                close_in ic;
                let len = List.length aliases in
                jout#info "Loaded %d records from %s" len fname;
                loop String_map.nil pm aliases
        in
        enter
end

module Db_core = struct
    let equal_int_array a b =
        let a = Array.to_seq a and b = Array.to_seq b in
        Cf_seq.eqf (==) a b

    let show_int_array va =
        let b = Buffer.create 16 in
        Buffer.add_string b "[|";
        Array.iter begin fun i ->
            Buffer.add_string b (string_of_int i);
            Buffer.add_char b ';';
        end va;
        Buffer.add_string b "|]";
        Buffer.contents b

    let emit_da_map pp vtyp name map =
        let open Format in
        let mflags = Marshal.([ No_sharing; Compat_32 ]) in
        let w3 = Aux.D_map.Unsafe.export map in
        let exp = Marshal.to_string w3 mflags in
        fprintf pp "let %s =\n" name;
        fprintf pp "  let w3: int array * int array * %s array =\n" vtyp;
        fprintf pp "     Marshal.from_string \"%s\" 0\n" (String.escaped exp);
        fprintf pp "  in\n";
        fprintf pp "  let idx, adj, va = w3 in\n";
        fprintf pp "  Aux.D_map.Unsafe.import idx adj va\n\n";
        jout#info "Emitted %s" name

    let emit_composition_map pp db =
        let g (cp, ccs) =
            let ccs = List.sort compare ccs in
            let n = List.length ccs in
            let cpa = Array.make (n * 2) 0 in
            let update i (cp2, cpx) =
                let i = i * 2 in
                cpa.(i) <- cp2;
                cpa.(succ i) <- cpx
            in
            List.iteri update ccs;
            cp, cpa
        in
        let f ccm cp =
            if Aux.is_hangul_syllable cp then
                ccm
            else if
                uucd_property_require db Uucd.full_composition_exclusion cp
            then
                ccm
            else if
                uucd_property_require db Uucd.decomposition_type cp <> `Can
            then
                ccm
            else
                match
                    uucd_property_require db Uucd.decomposition_mapping cp
                with
                | `Self ->
                    ccm
                | `Cps cps ->
                    match cps with
                    | []
                    | _ :: []
                    | _ :: _ :: _ :: _ ->
                        assert (not true);
                        ccm
                    | cp1 :: cp2 :: [] ->
                        assert (Uchar.is_valid cp1);
                        assert (Uchar.is_valid cp2);
                        let s =
                            match Int_map.search cp1 ccm with
                            | None -> []
                            | Some s -> s
                        in
                        Int_map.replace (cp1, ((cp2, cp) :: s)) ccm
        in
        Aux.all_uchars |>
        Seq.fold_left f Int_map.nil |>
        Int_map.to_seq_incr |>
        Seq.map g |>
        Aux.D_map.of_seq |>
        emit_da_map pp "int array" "composition_map"

    let emit_canonical_decomposition_prop pp db =
        let f cp =
            if Aux.is_hangul_syllable cp then
                None
            else if
                uucd_property_require db Uucd.decomposition_type cp <> `Can
            then
                None
            else
                match
                    uucd_property_require db Uucd.decomposition_mapping cp
                with
                | `Self -> None
                | `Cps cps -> Some (cp, Array.of_list cps)
        in
        let vn = "canonical_decomposition" in
        let vt = "int array" in
        let v0 = [| |] in
        let v0s = show_int_array in
        let eq = equal_int_array in
        XP.of_optmap ~v0 ~eq f |> XP.emit ~pp ~vn ~vt ~v0s ?qe:None

    let emit_compatibility_decomposition_prop pp db =
        let f cp =
            if uucd_property_require db Uucd.decomposition_type cp = `Can then
                None
            else begin
                assert (not (Aux.is_hangul_syllable cp));
                match
                    uucd_property_require db Uucd.decomposition_mapping cp
                with
                | `Self -> None
                | `Cps cps -> Some (cp, Array.of_list cps)
            end
        in
        let vn = "compatibility_decomposition" in
        let vt = "int array" in
        let v0 = [| |] in
        let v0s = show_int_array in
        let eq = equal_int_array in
        XP.of_optmap ~v0 ~eq f |> XP.emit ~pp ~vn ~vt ~v0s ?qe:None

    let pcons_bool = let open Uucd in [
        mkpcons
            ~snym:"grbase"
            ~cname:"grapheme_base"
            ~prop:grapheme_base
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"ids"
            ~cname:"id_start"
            ~prop:id_start
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"idc"
            ~cname:"id_continue"
            ~prop:id_continue
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"wspace"
            ~cname:"white_space"
            ~prop:white_space
            ~vstr:P_bool.vstr;
    ]

    let pcons_int = let open Uucd in [
        mkpcons
            ~snym:"ccc"
            ~cname:"canonical_combining_class"
            ~prop:canonical_combining_class
            ~vstr:P_int.vstr;
    ]

    let pcons_qc = let open Uucd in [
        mkpcons
            ~snym:"nfcqc"
            ~cname:"quick_check_nfc"
            ~prop:nfc_quick_check
            ~vstr:P_quick_check.vstr;

        mkpcons
            ~snym:"nfdqc"
            ~cname:"quick_check_nfd"
            ~prop:nfd_quick_check
            ~vstr:P_quick_check.vstr;

        mkpcons
            ~snym:"nfkcqc"
            ~cname:"quick_check_nfkc"
            ~prop:nfkc_quick_check
            ~vstr:P_quick_check.vstr;

        mkpcons
            ~snym:"nfkdqc"
            ~cname:"quick_check_nfkd"
            ~prop:nfkd_quick_check
            ~vstr:P_quick_check.vstr;
    ]

    let generate pp db =
        header pp ();
        emit_composition_map pp db;
        emit_canonical_decomposition_prop pp db;
        emit_compatibility_decomposition_prop pp db;
        List.iter (P_bool.generate pp db) pcons_bool;
        List.iter (P_int.generate pp db) pcons_int;
        List.iter (P_quick_check.generate pp db) pcons_qc;
        trailer pp ()
end

module Db_regx1 = struct
    module Aliases_order = struct
        type t = (string * string) list [@@deriving ord, show]

        let compare = Stdlib.compare

        let show s =
            let b = Buffer.create 16 in
            Buffer.add_char b '[';
            let f (s0, s1) =
                Buffer.add_char b '"';
                Buffer.add_string b (String.escaped s0);
                Buffer.add_string b "\", \"";
                Buffer.add_string b (String.escaped s1);
                Buffer.add_string b "\";"
            in
            List.iter f s;
            Buffer.add_char b ']';
            Buffer.contents b
    end

    module Aliases_map = Cf_rbtree.Map.Create(Aliases_order)

    let emit_value_alias_table =
        let open Format in
        let loop ~pp (nym, vstr) =
            fprintf pp "  \"%s\", %s;\n" nym vstr
        in
        let enter ~vn ~pp vm =
            fprintf pp "let %s = DbAux.create_index [\n" vn;
            Seq.iter (loop ~pp) @@ String_map.to_seq_incr vm;
            fprintf pp "]\n\n";
            jout#info "Emitted property value alias table %s" vn
        in
        enter

    let emit_typed_value_aliases_map =
        let loop ~pp (_, (vn, vm)) = emit_value_alias_table ~vn ~pp vm in
        let enter ~pp tvam =
            Seq.iter (loop ~pp) @@ Aliases_map.to_seq_incr tvam
        in
        enter

    let emit_value_aliases_map =
        let loop ~pp (_, tvam) =
            ignore (emit_typed_value_aliases_map ~pp tvam)
        in
        let enter ~pp vam =
            Seq.iter (loop ~pp) @@ String_map.to_seq_incr vam
        in
        enter

    let pcons_bool = let open Uucd in [
        mkpcons
            ~snym:"alpha"
            ~cname:"alphabetic"
            ~prop:alphabetic
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"lower"
            ~cname:"lowercase"
            ~prop:lowercase
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"upper"
            ~cname:"uppercase"
            ~prop:uppercase
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"nchar"
            ~cname:"noncharacter_code_point"
            ~prop:noncharacter_code_point
            ~vstr:P_bool.vstr;

        mkpcons
            ~snym:"di"
            ~cname:"default_ignorable_code_point"
            ~prop:default_ignorable_code_point
            ~vstr:P_bool.vstr;
    ]

    let pcons_blk = let open Uucd in [
        mkpcons
            ~snym:"blk"
            ~cname:"block"
            ~prop:block
            ~vstr:P_block.vstr;
    ]

    let pcons_gc = let open Uucd in [
        mkpcons
            ~snym:"gc"
            ~cname:"general_category"
            ~prop:general_category
            ~vstr:P_general_category.vstr;
    ]

    let pcons_script = let open Uucd in [
        mkpcons
            ~snym:"sc"
            ~cname:"script"
            ~prop:script
            ~vstr:P_script.vstr;
    ]

    let get_vm ~snym ~vstr pvm = pvm
        |> String_map.require snym
        |> String_map.to_seq_incr
        |> Seq.map (fun (nym, sval) -> nym, vstr sval)
        |> String_map.of_seq

    let put_tvam ~pvm ~typ tvam (Pcons c) =
        let vm = get_vm ~snym:c.snym ~vstr:c.vstr pvm in
        let vxs = String_map.to_seq_decr vm |> Cf_seq.reverse in
        match Aliases_map.search vxs tvam with
        | None ->
            let id = Aliases_map.size tvam in
            let vn = Printf.sprintf "valias_%s_%u" typ id in
            Aliases_map.replace (vxs, (vn, vm)) tvam
        | Some _ ->
            tvam

    let put_vam ~pvm ~typ vam pc =
        let tvam =
            match String_map.search typ vam with
            | None -> Aliases_map.nil
            | Some tvam -> tvam
        in
        let tvam = put_tvam ~pvm ~typ tvam pc in
        String_map.replace (typ, tvam) vam

    let get_pa ~pvm ~vam ~typ ~vstr snym =
        assert (String_map.member snym pvm);
        let vm = get_vm ~snym ~vstr pvm in
        let vxs = String_map.to_seq_decr vm |> Cf_seq.reverse in
        assert (String_map.member typ vam);
        let tvam = String_map.require typ vam in
        assert (Aliases_map.member vxs tvam);
        let vn, _ = Aliases_map.require vxs tvam in
        vn

    let put_utyp ~pvm ~vam ~typ ?dbm m (Pcons c) =
        let dbm =
            match dbm with
            | None -> ""
            | Some s -> Printf.sprintf "%s." s
        in
        let vn = String.lowercase_ascii c.cname in
        let pa = get_pa ~pvm ~vam ~typ ~vstr:c.vstr c.snym in
        let pstr = Printf.sprintf "DbAux.Typ_%s (%s%s, %s)" typ dbm vn pa in
        String_map.replace (c.snym, pstr) m

    let property_value_idx_map ~pvm =
        let m = String_map.nil in
        let f = put_vam ~pvm ~typ:"bool" in
        let m = List.fold_left f m Db_core.pcons_bool in
        let m = List.fold_left f m pcons_bool in
        let f = put_vam ~pvm ~typ:"int" in
        let m = List.fold_left f m Db_core.pcons_int in
        let f = put_vam ~pvm ~typ:"quick_check" in
        let m = List.fold_left f m Db_core.pcons_qc in
        let f = put_vam ~pvm ~typ:"block" in
        let m = List.fold_left f m pcons_blk in
        let f = put_vam ~pvm ~typ:"general_category" in
        let m = List.fold_left f m pcons_gc in
        let f = put_vam ~pvm ~typ:"script" in
        let m = List.fold_left f m pcons_script in
        m

    let property_idx_map ~pvm ~vam =
        let m = String_map.nil in
        let f = put_utyp ~pvm ~vam ~typ:"bool" ~dbm:"Core" in
        let m = List.fold_left f m Db_core.pcons_bool in
        let f = put_utyp ~pvm ~vam ~typ:"int" ~dbm:"Core" in
        let m = List.fold_left f m Db_core.pcons_int in
        let f = put_utyp ~pvm ~vam ~typ:"quick_check" ~dbm:"Core" in
        let m = List.fold_left f m Db_core.pcons_qc in
        let f = put_utyp ~pvm ~vam ~typ:"bool" ?dbm:None in
        let m = List.fold_left f m pcons_bool in
        let f = put_utyp ~pvm ~vam ~typ:"block" ?dbm:None in
        let m = List.fold_left f m pcons_blk in
        let f = put_utyp ~pvm ~vam ~typ:"general_category" ?dbm:None in
        let m = List.fold_left f m pcons_gc in
        let f = put_utyp ~pvm ~vam ~typ:"script" ?dbm:None in
        let m = List.fold_left f m pcons_script in
        m

    let select_pm =
        let loop ~pxm retm (pnym, names) =
            let _, sname = names in
            let snym = Aux.search_query_nym sname in
            if String_map.member snym pxm then begin
                let pnym = Aux.search_query_nym pnym in
                String_map.replace (pnym, names) retm
            end
            else
                retm
        in
        let enter ~pxm pm =
            String_map.to_seq_incr pm |> Seq.fold_left (loop ~pxm) String_map.nil
        in
        enter

    let select_pvm =
        let loop ~pm retm (pnym, vm) =
            if String_map.member pnym pm then
                String_map.replace (pnym, vm) retm
            else
                retm
        in
        let enter ~pm pvm =
            String_map.to_seq_incr pvm |> Seq.fold_left (loop ~pm) String_map.nil
        in
        enter

    let emit_property_index pp xm pm =
        let open Format in
        fprintf pp "let property_index = DbAux.create_index [\n";
        let f (pnym, _) =
            assert (String_map.member pnym pm);
            let _, sname = String_map.require pnym pm in
            let snym = Aux.search_query_nym sname in
            assert (String_map.member snym xm);
            let pstr = String_map.require snym xm in
            fprintf pp "  \"%s\", %s;\n" pnym pstr
        in
        Seq.iter f @@ String_map.to_seq_incr pm;
        fprintf pp "]\n\n";
        jout#info "Emitted property_index"

    let generate ~pafile ~pavfile pp db =
        let pm = PropAliases.load pafile in
        let pvm = PropValAliases.load pm pavfile in
        let vam = property_value_idx_map ~pvm in
        let pxm = property_idx_map ~pvm ~vam in
        let pm = select_pm ~pxm pm in
        let _pvm = select_pvm ~pm pvm in
        header pp ();
        Format.pp_print_string pp "module Core = Ucs_db_core\n\n";
        List.iter (P_bool.generate pp db) pcons_bool;
        List.iter (P_block.generate pp db) pcons_blk;
        List.iter (P_general_category.generate pp db) pcons_gc;
        List.iter (P_script.generate pp db) pcons_script;
        emit_value_aliases_map ~pp vam;
        emit_property_index pp pxm pm;
        trailer pp ()
end

let program = Filename.basename Sys.executable_name

let load infile cin =
    let d = Uucd.decoder (`Channel cin) in
    match Uucd.decode d with
    | `Error e ->
        let (l0, c0), (l1, c1) = Uucd.decoded_range d in
        jerr#fail "%s:%d.%d-%d.%d: %s\n%!" infile l0 c0 l1 c1 e
    | `Ok db ->
        jout#info "Database loaded.";
        db

let usage = Printf.sprintf "\
    Usage: %s [OPTION]... [file]\n\
    \n\
    Generates the OCaml source for the modules containing data extracted\n\
    from a Unicode Character Set database file.\n" program

let main () =
    let infile_opt = ref None in
    let infile_arg f =
        if !infile_opt = None then
            infile_opt := Some f
        else
            raise (Arg.Bad "Error: use exactly one input file!")
    in
    let verbose = ref false in
    let propaliases = ref None and propvalaliases = ref None in
    let dbcore = ref None and dbregx1 = ref None in
    let optset r = Arg.String (fun s -> r := Some s) in
    let boolset r = Arg.Set r in
    let syntax = [
        "-verbose",
            boolset verbose, " Verbose diagnostic output.";

        "-core",
            optset dbcore, "<FILE> Output for core properties.";

        "-regx1",
            optset dbregx1, "<FILE> Output for regular expression properties.";

        "-propaliases",
            optset propaliases, "<FILE> Property aliases file.";

        "-propvalaliases",
            optset propvalaliases, "<FILE> Property value aliases file.";
    ] in
    Arg.parse (Arg.align syntax) infile_arg usage;
    if !verbose then jout#setlimit `Debug;
    let outfile, generate =
        match !dbcore with
        | Some name ->
            name, Db_core.generate
        | None ->
            match !dbregx1 with
            | Some name ->
                let pafile =
                    match !propaliases with
                    | None ->
                        raise (Arg.Bad "Error: property aliases required!")
                    | Some fname ->
                        fname
                and pavfile =
                    match !propvalaliases with
                    | None ->
                        raise (Arg.Bad
                            "Error: property value aliases required!")
                    | Some fname ->
                        fname
                in
                name, Db_regx1.generate ~pafile ~pavfile
            | None ->
                raise (Arg.Bad "Error: no output form specified!")
    in
    try
        let infile, cin =
            match !infile_opt with
            | None ->
                jout#info "Database input: <stdin>";
                "-", stdin
            | Some infile ->
                jout#info "Database input: %s" infile;
                infile, open_in infile
        in
        let db = load infile cin in
        close_in cin;
        let cout = open_out outfile in
        let pp = Format.formatter_of_out_channel cout in
        generate pp db;
        Format.pp_print_flush pp ();
        close_out cout
    with
    | Sys_error e ->
        jerr#fail "error: %s" e

let _ = main ()

(*--- End ---*)
