(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Ucs_transport_aux.Private

module Aux(N: Cf_endian.Unsafe.Profile) = struct
    module D = Cf_decode
    (* module E = Cf_encode *)

    let descript = Printf.sprintf "UTF-32%s" N.descript

    let size_of_uchar _ = 4
    let size_of_codeunit = 4

    let decode_ck s (D.Position p as p0) n =
        let c = N.ldu32 s p in
        if Uchar.is_valid c then
            D.analyze n 4 c
        else
            decode_invalid p0 "invalid UTF-32 form"

    let encode_wr = N.stu32
end

module BE = Create(Aux(Cf_endian.Unsafe.BE))
module LE = Create(Aux(Cf_endian.Unsafe.LE))
module SE = Create(Aux(Cf_endian.Unsafe.SE))

let of_endian = function
    | `BE -> (module BE : Ucs_transport_aux.Profile)
    | `LE -> (module LE : Ucs_transport_aux.Profile)
    | `SE -> (module SE : Ucs_transport_aux.Profile)

(*--- End ---*)
