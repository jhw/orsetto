(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Core = struct
    module Basis = struct
        module Event = Uchar

        module Dispatch = struct
            type event = Uchar.t

            module Short = struct
                let eager = Cf_dfa.Aux.Eager.of_ints @@ Cf_seq.range 0 0x300
                include (val eager : Cf_dfa.Aux.Int_dispatch)
            end

            module Long = Cf_dfa.Aux.Memo(Uchar)

            type 'a t = {
                short: 'a Short.t;
                long: 'a Long.t;
            }

            let create f = {
                short = Short.create (fun c -> f @@ Uchar.of_int c);
                long = Long.create f;
            }

            let dispatch e d =
                let cp = Uchar.to_int e in
                if cp < 0x300 then
                    Short.dispatch cp d.short
                else
                    Long.dispatch e d.long
        end
    end

    include Cf_dfa.Core(Basis)
end

module Affix = Cf_dfa.Mk_affix(Core)

module Scan = struct
    module DFA = Core
    open Affix

    module S = Ucs_scan.UTF8
    open S.Affix

    module A = S.Annot

    module Chain = struct
        module Basis = struct
            type symbol = Uchar.t
            type position = Cf_annot.Textual.position
            type 'a form = 'a A.form
            module Scan = S
        end

        include Cf_chain_scan.Create(Basis)
    end

    module DbAux = Ucs_db_aux
    module DbRL1 = Ucs_db_regx1

    (*---
      NOTE: this module contains scanners that combine for recognizing syntax
      elements found in Unicode regular expressions. Most of the functions
      defined here use an ad-hoc naming convention to indicate the return type
      of the scanner, as illustrated by the following examples:

        val l_xxxxx:    unit A.form S.t
        val nl_xxxxx:   int A.form S.t
        val ucl_xxxxx:  Uchar.t A.form S.t
        val txtl_xxxxx: Ucs_text.t A.form S.t
        val satl_xxxxx: (Uchar.t -> bool) A.form S.t
        val expl_xxxxx: DFA.exp A.form S.t
      ---*)

    let fail_always msg = S.err ~x:(Failure msg) ()
    let fail_unless msg sp = S.req ~x:(Failure msg) sp

    let dnx_ucl ucl = A.mv (DFA.one (A.dn ucl)) ucl
    let dnx_satl satl = A.mv (DFA.sat (A.dn satl)) satl

    let ucl_char ch = S.one @@ Uchar.of_char ch

    (* let ucl_binary_property prop = sat (Ucs_property.require prop) *)

    let ucl_lbrace =
        fail_unless "opening brace '{' required" @@ ucl_char '{'

    let ucl_rbrace =
        fail_unless "closing brace '}' required" @@ ucl_char '}'

    let ucl_rsquare =
        fail_unless "closing bracket ']' required" @@ ucl_char ']'

    let ucl_rparen =
        fail_unless "closing parenthesis ')' required" @@ ucl_char ')'

    let l_colon_rbrace =
        fail_unless "closing colon brace ':]' required" begin
            let* c0l = ucl_char ':' in
            let* c1l = ucl_char ']' in
            S.return @@ A.span c0l c1l ()
        end

    let is_special_uchar specials uc =
        Uchar.(is_char uc && String.contains specials @@ to_char uc)

    let isnt_special_uchar specials uc =
        Uchar.(is_char uc && not (String.contains specials @@ to_char uc))

    let textl_ascii_uint =
        let p_digit =
            S.sat begin fun uc ->
                let cp = Uchar.to_int uc in
                cp >= 48 && cp < 58
            end
        in
        let rec loop p0l p1l b =
            S.opt p_digit >>= function
            | Some ucl ->
                Buffer.add_char b @@ Uchar.to_char @@ A.dn ucl;
                loop p0l ucl b
            | None ->
                S.return
                    @@ A.span p0l p1l
                    @@ Ucs_text.of_string
                    @@ Buffer.contents b
        in
        let enter =
            let* ucl = p_digit in
            let b = Buffer.create 8 in
            Buffer.add_char b (Uchar.to_char @@ A.dn ucl);
            (loop[@tailcall]) ucl ucl b
        in
        enter

    let nl_ascii_hexdigit =
        let is_uchar uc =
            match Uchar.to_int uc with
            | cp when cp >= 48 && cp < 58 -> true   (* '0'-'9' *)
            | cp when cp >= 65 && cp < 71 -> true   (* 'A'-'F' *)
            | cp when cp >= 97 && cp < 103 -> true  (* 'a'-'f' *)
            | _ -> false
        and to_digit i =
            if i < 58 then i - 48       (* '0'-'9' -> 0..9 *)
            else if i < 71 then i - 55  (* 'A'-'F' -> 10..15 *)
            else i - 87                 (* 'a'-'f' -> 10..15 *)
        in
        let* ucl = S.sat is_uchar in
        S.return @@ A.mv (to_digit @@ Uchar.to_int @@ A.dn ucl) ucl

    let ucl_sedicet =
        let rec loop nla nlb j cp =
            if j < 3 then begin
                let* nlb = fail_unless "digit required" nl_ascii_hexdigit in
                loop nla nlb (succ j) (cp * 16 + (A.dn nlb))
            end
            else if (Uchar.is_valid cp) then
                S.return @@ A.span nla nlb @@ Uchar.unsafe_of_int cp
            else
                fail_always "code point invalid (16-bit)"
        in
        let enter =
            let* nl = nl_ascii_hexdigit in
            (loop[@tailcall]) nl nl 0 (A.dn nl)
        in
        enter

    let ucl_hexdigits =
        let f cp nl =
            let cp = cp * 16 + (A.dn nl) in
            if not (Uchar.is_valid cp) then raise Not_found;
            cp
        in
        let enter =
            let* nls = S.seq ~a:1 nl_ascii_hexdigit in
            let nsl = A.collect nls in
            try
                let uc = Uchar.unsafe_of_int @@ List.fold_left f 0 nls in
                S.return @@ A.mv uc nsl
            with
            | Not_found ->
                fail_always "code point invalid"
        in
        enter

    let item_special_chars = " \\-]"

    let ucl_item_literal = S.sat (isnt_special_uchar item_special_chars)
    let ucl_item_special_uchar = S.sat (is_special_uchar item_special_chars)

    let ucl_item_escaped_number =
        let enclosed =
            let* p0 = ucl_char '{' in
            let* ucl = ucl_hexdigits in
            let* p1 = ucl_rbrace in
            S.return @@ A.span p0 p1 @@ A.dn ucl
        and fail =
            fail_always "escaped code point required"
        in
        let enter =
            let* _ = ucl_char 'u' in
            S.alt [ ucl_sedicet; enclosed; fail ]
        in
        enter

    let ucl_item_escaped_alt = S.alt [
        ucl_item_escaped_number;
        ucl_item_special_uchar;
        fail_always "invalid escaped range limit";
    ]

    let ucl_item_escaped =
        let* pl = ucl_char '\\' in
        let* ucl = ucl_item_escaped_alt in
        S.return @@ A.span pl ucl @@ A.dn ucl

    let ucl_item_single = S.alt [
        ucl_item_escaped;
        ucl_item_literal;
    ]

    let textl_query_nym =
        let* wordls = Chain.seq ~c:(Chain.mk S.rws) ~a:1 S.ids in
        let wordsl = A.collect wordls in
        let b = Buffer.create 8 in
        let f = function Ucs_text.Octets s -> Buffer.add_string b s in
        List.iter f @@ A.dn wordsl;
        let nym = Ucs_text.of_string @@ Buffer.contents b in
        S.return @@ A.mv nym wordsl

    let textl_prop_value = S.alt [ textl_query_nym; textl_ascii_uint ]

    let satl_item_single =
        let* ucl = ucl_item_single in
        S.return @@ A.mv (Uchar.equal @@ A.dn ucl) ucl

    let satl_item_range_pair =
        let* ucal = ucl_item_single in
        let* _ = S.ows in
        let* _ = ucl_char '-' in
        let* _ = S.ows in
        let* ucbl = fail_unless "range limit required" ucl_item_single in
        let cpa = Uchar.to_int @@ A.dn ucal in
        let cpb = Uchar.to_int @@ A.dn ucbl in
        let f uc = let cp = Uchar.to_int uc in cp >= cpa && cp <= cpb in
        S.return @@ A.span ucal ucbl f

    let satl_prop_spec_predicates =
        let some_true = Some true and some_false = Some false in
        let niltbl = Ucs_ucdgen_aux.(Lazy.from_val @@ Tbl_dset D_set.nil) in
        let q_is_ascii cp = if cp < 0x80 then some_true else some_false in
        let ascii_map = Ucs_ucdgen_aux.Map {
            q = q_is_ascii;
            v0 = false;
            tbl = niltbl;
        } in
        let some_ascii_typ =
            Some (DbAux.Typ_bool (ascii_map, DbRL1.valias_bool_0))
        in
        let q_is_any cp =
            if Uchar.is_valid cp then some_true else some_false
        in
        let any_map = Ucs_ucdgen_aux.Map {
            q = q_is_any;
            v0 = false;
            tbl = niltbl;
        } in
        let some_any_typ =
            Some (DbAux.Typ_bool (any_map, DbRL1.valias_bool_0))
        in
        let q_is_assigned cp =
            if
                not (Uchar.is_valid cp) ||
                DbAux.query_map DbRL1.general_category (Uchar.of_int cp) == `Cn
            then
                some_false
            else
                some_true
        in
        let assigned_map = Ucs_ucdgen_aux.Map {
            q = q_is_assigned;
            v0 = false;
            tbl = niltbl;
        } in
        let some_assigned_typ =
            Some (DbAux.Typ_bool (assigned_map, DbRL1.valias_bool_0))
        in
        let psearch nym =
            match DbAux.search_property DbRL1.property_index nym with
            | Some _ as r ->
                r
            | None ->
                match Ucs_ucdgen_aux.search_query_nym nym with
                | "any" -> some_any_typ
                | "ascii" -> some_ascii_typ
                | "assigned" -> some_assigned_typ
                | _ -> None
        in
        let nilindex () = DbAux.create_index [] in
        let gcprop = DbRL1.general_category and scprop = DbRL1.script in
        let gcindex =
            match
                DbAux.require_property DbRL1.property_index "gc"
            with
            | DbAux.Typ_general_category (_, idx) -> idx
            | _ -> assert (not true); nilindex ()
        and scindex =
            match
                DbAux.require_property DbRL1.property_index "sc"
            with
            | DbAux.Typ_script (_, idx) -> idx
            | _ -> assert (not true); nilindex ()
        in
        let ckscript nym =
            let Ucs_text.Octets s = nym in
            match DbAux.search_index scindex s with
            | Some sc ->
                let f uc = DbAux.query_map scprop uc == sc in
                S.return f
            | None ->
                fail_always "not a predicate"
        in
        let ckgencat nym =
            let Ucs_text.Octets s = nym in
            match DbAux.search_index gcindex s with
            | Some gc ->
                let f uc = DbAux.query_map gcprop uc == gc in
                S.return f
            | None ->
                ckscript nym
        in
        let ckbool nym =
            let Ucs_text.Octets s = nym in
            match psearch s with
            | Some (DbAux.Typ_bool (prop, _)) ->
                S.return @@ DbAux.query_map prop
            | Some _ ->
                fail_always "named property not boolean"
            | None ->
                ckgencat nym
        in
        let rec loop fs = function
            | [] ->
                S.return fs
            | hd :: tl ->
                let* f = ckbool (A.dn hd) in
                (loop[@tailcall]) (f :: fs) tl
        in
        let enter nymls =
            let* fs = loop [] nymls in
            let f uc = List.exists (fun f -> f uc) fs in
            S.return @@ A.mv f @@ A.collect nymls
        in
        enter

    type op_enum = Op_equal | Op_not_equal

    let opl_enum =
        let equal =
            let* ucl = ucl_char '=' in
            S.return @@ A.mv Op_equal ucl
        and not_equal =
            let* ucl = S.one (Uchar.of_int 0x2260) in
            S.return @@ A.mv Op_not_equal ucl
        in
        let colon =
            let* ucl = ucl_char ':' in
            S.return @@ A.mv Op_equal ucl
        in
        let bang_equal =
            let* uc1l = ucl_char '!' in
            let* uc2l = ucl_char '=' in
            S.return @@ A.span uc1l uc2l Op_not_equal
        in
        S.alt [ equal; colon; not_equal; bang_equal ]

    let satl_prop_spec_enumerated_aux =
        let rec loop w4 fs = function
            | [] ->
                S.return fs
            | hd :: tl ->
                let op, prop, idx, eqf = w4 in
                let Ucs_text.Octets vnym = A.dn hd in
                match DbAux.search_index idx vnym with
                | None ->
                    fail_always "unrecognized value enumeration"
                | Some ck ->
                    let f uc =
                        let v = DbAux.query_map prop uc in
                        match op with
                        | Op_equal -> eqf v ck
                        | Op_not_equal -> not (eqf v ck)
                    in
                    loop w4 (f :: fs) tl
        in
        let enter w4 pnyml vnymls =
            let* fs = loop w4 [] vnymls in
            let f uc = List.exists (fun f -> f uc) fs in
            let vnymsl = A.collect vnymls in
            S.return @@ A.span pnyml vnymsl f
        in
        enter

    let equal_uchar_list_option a b = begin[@warning "-4"]
        match a, b with
        | Some a, Some b ->
            let a = List.to_seq a and b = List.to_seq b in
            Cf_seq.eqf Uchar.equal a b
        | _ ->
            false
    end

    let satl_prop_spec_enumerated pnyml op vnymls =
        let Ucs_text.Octets pnym = A.dn pnyml in
        match DbAux.search_property DbRL1.property_index pnym with
        | None ->
            fail_always "unrecognized property name"
        | Some typ ->
            match typ with
            | DbAux.Typ_bool (prop, idx) ->
                let eqf = (==) in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_int (prop, idx) ->
                let eqf = (==) in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_string (prop, idx) ->
                let eqf = String.equal in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_uchars (prop, idx) ->
                let eqf = equal_uchar_list_option in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_quick_check (prop, idx) ->
                let eqf = DbAux.equal_qc in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_block (prop, idx) ->
                let eqf = DbAux.equal_blk in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_general_category (prop, idx) ->
                let eqf = DbAux.equal_gc in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | DbAux.Typ_script (prop, idx) ->
                let eqf = DbAux.equal_script in
                let w4 = op, prop, idx, eqf in
                satl_prop_spec_enumerated_aux w4 pnyml vnymls
            | _ ->
                assert (not true);
                fail_always "unrecognized property type"

    let satl_prop_spec_aux =
        let vchain =
            Chain.mk begin
                let* _ = S.ows in
                let* _ = ucl_char '|' in
                let* _ = S.ows in
                S.return ()
            end
        in
        let predicate =
            Chain.seq ~c:vchain ~a:1 textl_query_nym >>=
                satl_prop_spec_predicates
        and enumerated  =
            let* pnyml = textl_query_nym in
            let* _ = S.ows in
            let* opl = opl_enum in
            let* _ = S.ows in
            let* nymls = Chain.seq ~c:vchain ~a:1 textl_prop_value in
            satl_prop_spec_enumerated pnyml (A.dn opl) nymls
        in
        let aux =
            let* _ = S.ows in
            let* satl = S.alt [ enumerated; predicate ] in
            let* _ = S.ows in
            S.return satl
        in
        let enter = fail_unless "property specification required" aux in
        enter

    let satl_opt_negate satl neglopt =
        S.return begin
            match neglopt with
            | Some _ -> A.mv (fun uc -> not @@ A.dn satl uc) satl
            | None -> satl
        end

    let satl_bracketed_prop_spec =
        let* p0l = ucl_char '[' in
        let* _ = ucl_char ':' in
        let* neglopt = S.opt (ucl_char '^') in
        let* satl = satl_prop_spec_aux in
        let* satl = satl_opt_negate satl neglopt in
        let* p1l = l_colon_rbrace in
        S.return @@ A.span p0l p1l @@ A.dn satl

    let satl_escaped_prop_spec_form =
        let positive = let* _ = ucl_char 'p' in S.return None in
        let negative = let* ucl = ucl_char 'S' in S.return (Some ucl) in
        let* neglopt = S.alt [ positive; negative ] in
        let* p0l = ucl_lbrace in
        let* satl = satl_prop_spec_aux in
        let* satl = satl_opt_negate satl neglopt in
        let* p1l = ucl_rbrace in
        S.return @@ A.span p0l p1l @@ A.dn satl

    let satl_escaped_prop_spec =
        let* escl = ucl_char '\\' in
        let* satl = satl_escaped_prop_spec_form in
        S.return @@ A.span escl satl @@ A.dn satl

    let chain_of_items = Chain.mk ~a:`Opt ~b:`Opt begin
        let* _ = S.ows in
        let* _ = S.opt (let* _ = ucl_char '|' in ucl_char '|') in
        let* _ = S.ows in
        S.return ()
    end

    type op_arith =
        | Op_intersect
        | Op_subtract
        | Op_exclusive

    let opl_arith =
        let pair c =
            let* p0l = ucl_char c in
            let* p1l = ucl_char c in
            let op =
                match c with
                | '&' -> Op_intersect
                | '-' -> Op_subtract
                | '~' -> Op_exclusive
                | _ -> assert (not true); Op_exclusive
            in
            S.return @@ A.span p0l p1l op
        in
        let enter = S.alt [ pair '&'; pair '-'; pair '~' ] in
        enter

    let expl_bracketed_items =
        let factor =
            S.alt [
                satl_escaped_prop_spec;
                satl_item_range_pair;
                satl_item_single;
            ]
        in
        let factors =
            let* satls = Chain.seq ~c:chain_of_items ~a:1 factor in
            let satsl = A.collect satls in
            let sats = A.dn satsl in
            let f uc = List.exists (fun sat -> sat uc) sats in
            S.return @@ A.mv f satsl
        in
        let rec enclose () =
            let* synl = ucl_char '[' in
            let* nlopt = S.opt @@ ucl_char '^' in
            let* satl = inner (nlopt == None) in
            let* finl = ucl_rsquare in
            S.return @@ A.span synl finl @@ A.dn satl
        and inner pos =
            let* satl = term () in
            let sat = A.dn satl in
            let f uc = if pos then sat uc else not (sat uc) in
            S.return @@ A.mv f satl
        and term () =
            let* _ = S.ows in
            let* f0l = S.alt [satl_bracketed_prop_spec; enclose (); factors] in
            S.opt (terms ()) >>= function
            | None ->
                S.return f0l
            | Some (opl, f1l) ->
                let f0 = A.dn f0l and f1 = A.dn f1l in
                let op = A.dn opl in
                let f uc =
                    match op with
                    | Op_intersect -> f0 uc && f1 uc
                    | Op_subtract -> f0 uc && not (f1 uc)
                    | Op_exclusive -> f0 uc <> f1 uc
                in
                S.return @@ A.span f0l f1l f
        and terms () =
            let* _ = S.ows in
            let* opl = opl_arith in
            let* _ = S.ows in
            let* satl = term () in
            S.return (opl, satl)
        in
        let enter = enclose () >>: dnx_satl in
        enter

    let special_chars = {|\+*?(|)[]|}

    let expl_escaped_special =
        S.sat (is_special_uchar special_chars) >>: dnx_ucl

    let expl_escaped_codes =
        let dn_ucl ucl = DFA.one (A.dn ucl) in
        let rws_chain = Chain.mk S.rws in
        let sedicet =
            ucl_sedicet >>: dnx_ucl
        and enclosed =
            let* p0 = ucl_char '{' in
            let* expls = Chain.seq ~c:rws_chain ~a:1 ucl_hexdigits in
            let* p1 = ucl_rbrace in
            let exps = Seq.map dn_ucl @@ List.to_seq expls in
            S.return @@ A.span p0 p1 @@ DFA.cats exps
        and fail =
            fail_always "escaped code point sequence required"
        in
        let choices = [| sedicet; enclosed; fail |] in
        let enter =
            let* _ = ucl_char 'u' in
            S.altz @@ Array.to_seq choices
        in
        enter

    let expl_escaped_prop_spec =
        satl_escaped_prop_spec_form >>: dnx_satl

    let expl_escaped_form = S.alt [
        expl_escaped_special;
        expl_escaped_codes;
        expl_escaped_prop_spec;
        fail_always "invalid escaped form";
    ]

    let expl_escape_prefixed =
        let* pl = ucl_char '\\' in
        let* expl = expl_escaped_form in
        S.return @@ A.span pl expl @@ A.dn expl

    let expl_literal_uchar =
        S.sat (isnt_special_uchar special_chars) >>: dnx_ucl

    let expl_simple = ?^~(Array.to_seq [|
        expl_bracketed_items;
        expl_escape_prefixed;
        expl_literal_uchar;
    |])

    let expl_postfix =
        let question expl =
            let* ucl = ucl_char '?' in
            S.return @@ A.span expl ucl @@ !? (A.dn expl)
        in
        let plus expl =
            let* ucl = ucl_char '+' in
            S.return @@ A.span expl ucl @@ !+ (A.dn expl)
        in
        let star expl =
            let* ucl = ucl_char '*' in
            S.return @@ A.span expl ucl @@ !* (A.dn expl)
        in
        let ps = Array.to_seq [| question; plus; star; S.return |] in
        let enter x = ?^~(Seq.map (fun f -> f x) ps) in
        enter

    let expl_one fix = ?^[ expl_simple; fix ] >>= expl_postfix

    let expl_cats fix =
        let* expls = S.seq ~a:1 ?b:None @@ expl_one fix in
        let expsl = A.collect expls in
        let exp = DFA.cats @@ List.to_seq @@ A.dn expsl in
        S.return @@ A.mv exp expsl

    let expl_alts fix =
        let fix = expl_cats fix in
        let* hd = fix in
        let* tl =  ?* (let* _ = ucl_char '|' in fix) in
        let expsl = A.collect (hd :: tl) in
        let exp = DFA.alts @@ List.to_seq @@ A.dn expsl in
        S.return @@ A.mv exp expsl

    let rec expl_group () =
        let* _ = ucl_char '(' in
        let* expl = expl_alts @@ expl_group () in
        let* _ = ucl_rparen in
        S.return expl

    let exp_scan =
        let* expl = expl_cats @@ expl_group () in
        S.return @@ A.dn expl
end

let invalid fn (Ucs_text.Octets s) =
    invalid_arg @@ Printf.sprintf
        "%s.%s: invalid regular expression, \"%s\"" __MODULE__ fn s

module DFA = struct
    include Core

    let uchars_to_term s =
        try Scan.(S.of_seq exp_scan s) with
        | Not_found -> invalid "uchars_to_term" @@ Ucs_text.of_seq s

    let text_to_term t =
        try Scan.(S.of_text exp_scan t) with
        | Not_found -> invalid "text_to_term" t

    let string_to_term s = text_to_term @@ Ucs_text.of_string s

    module Affix = struct
        include Affix

        let ( !$ ) = string_to_term
        let ( $$= ) s r = fin (string_to_term s) r
    end
end

type t = Regx of { mk: unit -> unit DFA.t }
open DFA.Affix

let of_dfa_term term =
    let mk () = DFA.create @@ List.to_seq [ term $= () ] in
    Regx { mk }

let of_text text =
    match DFA.text_to_term text with
    | exception (Failure msg) -> invalid "of_text" @@ Ucs_text.of_string msg
    | term -> of_dfa_term term

let of_uchars s =
    match DFA.uchars_to_term s with
    | exception (Failure msg) -> invalid "of_uchars" @@ Ucs_text.of_string msg
    | term -> of_dfa_term term

module UTF8 = Ucs_transport.UTF8

let test =
    let rec loop w sxr =
        if DFA.rejected w then
            false
        else begin
            let sxr = (sxr : Cf_decode.scanner) in
            match sxr#scan UTF8.uchar_decode_scheme with
            | exception (Cf_decode.Incomplete _) ->
                DFA.accepted w
            | uc ->
                DFA.advance w uc;
                (loop[@tailcall]) w sxr
        end
    in
    let enter (Regx r) (Ucs_text.Octets s) =
        let w = r.mk () and sxr = Cf_decode.string_scanner s in
        (loop[@tailcall]) w sxr
    in
    enter

let contains =
    let rec loop w s i j =
        let i = (i : Cf_decode.scanner) and j = (j : Cf_decode.scanner) in
        match j#scan UTF8.uchar_decode_scheme with
        | exception (Cf_decode.Incomplete _) ->
            DFA.accepted w
        | uc ->
            if DFA.rejected w then begin
                DFA.reset w;
                let _ = i#scan UTF8.uchar_decode_scheme in
                let start = j#position in
                let j = Cf_decode.string_scanner ~start s in
                (loop[@tailcall]) w s i j
            end
            else begin
                DFA.advance w uc;
                (loop[@tailcall]) w s i j
            end
    in
    let enter (Regx r) (Ucs_text.Octets s) =
        let w = r.mk () in
        let i = Cf_decode.string_scanner s in
        let j = Cf_decode.string_scanner s in
        (loop[@tailcall]) w s i j
    in
    enter

let search =
    let rec loop w s0 s i j =
        match s () with
        | Seq.Cons (uc, s) ->
            if DFA.rejected w then begin
                if DFA.accepted w then
                    Some (i, j)
                else begin
                    DFA.reset w;
                    let i = succ i and s = Cf_seq.tail s0 in
                    (loop[@tailcall]) w s s i i
                end
            end
            else begin
                DFA.advance w uc;
                (loop[@tailcall]) w s0 s i (succ j)
            end
        | Seq.Nil ->
            if DFA.accepted w then Some (i, j) else None
    in
    let enter (Regx r) s = (loop[@tailcall]) (r.mk ()) s s 0 0 in
    enter

let split =
    let rec loop w s b exr sxr () =
        let exr = (exr : Cf_encode.emitter) in
        let sxr = (sxr : Cf_decode.scanner) in
        match sxr#scan UTF8.uchar_decode_scheme with
        | exception (Cf_decode.Incomplete _) ->
            if DFA.accepted w then begin
                let hd = Ucs_text.Unsafe.of_string @@ Buffer.contents b in
                Seq.(Cons (hd, empty))
            end
            else
                Seq.Nil
        | uc ->
            if DFA.rejected w then begin
                if DFA.accepted w then begin
                    DFA.reset w;
                    let hd = Ucs_text.Unsafe.of_string @@ Buffer.contents b in
                    Buffer.reset b;
                    let exr = Cf_encode.buffer_emitter b in
                    let tl = loop w s b exr sxr in
                    Seq.Cons (hd, tl)
                end
                else begin
                    DFA.reset w;
                    exr#emit UTF8.uchar_encode_scheme uc;
                    (loop[@tailcall]) w s b exr sxr ()
                end
            end
            else begin
                DFA.advance w uc;
                (loop[@tailcall]) w s b exr sxr ()
            end
    in
    let enter (Regx r) (Ucs_text.Octets s) =
        let w = r.mk () and b = Buffer.create 0 in
        let exr = Cf_encode.buffer_emitter b in
        let sxr = Cf_decode.string_scanner s in
        loop w s b exr sxr
    in
    enter

let quote =
    let rec loop s () =
        match s () with
        | Seq.Cons (c, s) when Scan.(is_special_uchar special_chars c) ->
            let s () = Seq.Cons (c, loop s) in
            Seq.Cons (Uchar.of_char '\\' , s)
        | Seq.Cons (c, s) ->
            Seq.Cons (c, loop s)
        | Seq.Nil ->
            Seq.Nil
    in
    let enter t = Ucs_text.(to_seq t |> loop |> of_seq) in
    enter

let unquote =
    let rec loop s () =
        match s () with
        | Seq.Cons (c, s) when Scan.(is_special_uchar special_chars c) ->
            esc s
        | Seq.Cons (c, s) ->
            Seq.Cons (c, loop s)
        | Seq.Nil ->
            Seq.Nil
    and esc s =
        match s () with
        | Seq.Cons (c, s) -> Seq.Cons (c, loop s)
        | Seq.Nil -> Seq.Cons (Uchar.of_char '\\', Seq.empty)
    in
    let enter t = Ucs_text.(to_seq t |> loop |> of_seq) in
    enter

(*--- End ---*)
