(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let is_hangul_syllable cp = cp >= 0xAC00 && cp < 0xD7A4

let min_combining_code      = 0x0300    (* U+0300 COMBINING GRAVE ACCENT *)
let min_volatile_nfc_code   = 0x0300    (* U+0300 COMBINING GRAVE ACCENT *)
let min_volatile_nfd_code   = 0x00A0    (* U+00A0 NO-BREAK SPACE *)
let min_volatile_nfkc_code  = 0x00A0    (* U+00A0 NO-BREAK SPACE *)
let min_volatile_nfkd_code  = 0x00A0    (* U+00A0 NO-BREAK SPACE *)

module D_set = Cf_bsearch_data.Set.Of_int
module D_map = Cf_bsearch_data.Map.Of_int

module R_core = Cf_disjoint_interval.Core.Of_int
module R_set = Cf_disjoint_interval.Set.Of_int
module R_map = Cf_disjoint_interval.Map.Of_int

module Nym_map = Cf_bsearch_data.Map.Of_string

(*
module T_set = Ucs_trie.Set
module T_map = Ucs_trie.Map
*)

type 'a tbl =
    | Tbl_dset: D_set.t -> bool tbl
    | Tbl_rset: R_set.t -> bool tbl
    | Tbl_dmap: 'a D_map.t -> 'a tbl
    | Tbl_rmap: 'a R_map.t -> 'a tbl

type 'a map = Map of {
    q: int -> 'a option;
    v0: 'a;
    tbl: 'a tbl Lazy.t;
}

let query_map: type a. a map -> Uchar.t -> a = fun (Map m) c ->
    let cp = Uchar.to_int c in
    match m.q cp with
    | Some v ->
        v
    | None ->
        match Lazy.force m.tbl with
        | Tbl_dset tbl ->
            if D_set.member cp tbl then not m.v0 else m.v0
        | Tbl_rset tbl ->
            if R_set.member cp tbl then not m.v0 else m.v0
        | Tbl_dmap tbl ->
            (match D_map.search cp tbl with None -> m.v0 | Some v -> v)
        | Tbl_rmap tbl ->
            (match R_map.search cp tbl with None -> m.v0 | Some v -> v)

let qnil _ = None

let tbl_dbool s =
    let w2: int array * int array = Marshal.from_string s 0 in
    let idx, adj = w2 in
    Tbl_dset (D_set.Unsafe.import idx adj)

let tbl_rbool s =
    let w2: int array * int array = Marshal.from_string s 0 in
    let idx, adj = w2 in
    Tbl_rset (R_set.Unsafe.import idx adj)

let tbl_duniv: type a. string -> a tbl = fun s ->
    let w3: int array * int array * a array = Marshal.from_string s 0 in
    let idx, adj, va = w3 in
    Tbl_dmap (D_map.Unsafe.import idx adj va)

let tbl_runiv: type a. string -> a tbl = fun s ->
    let w3: int array * int array * a array = Marshal.from_string s 0 in
    let idx, adj, va = w3 in
    Tbl_rmap (R_map.Unsafe.import idx adj va)

let of_exported_dbool ?(v0 = false) ?(q = qnil) s =
    Map { q; v0; tbl = lazy (tbl_dbool s) }

let of_exported_rbool ?(v0 = false) ?(q = qnil) s =
    Map { q; v0; tbl = lazy (tbl_rbool s) }

let of_exported_duniv ~v0 ?(q = qnil) s =
    Map { q; v0; tbl = lazy (tbl_duniv s) }

let of_exported_runiv ~v0 ?(q = qnil) s =
    Map { q; v0; tbl = lazy (tbl_runiv s) }

let all_uchars =
    let f n =
        try
            let n = Uchar.succ n in
            Some (n, Uchar.to_int n)
        with
        | Invalid_argument _ -> None
    in
    Cf_seq.compose f Uchar.min

let of_filter ?(v0 = false) ?(q = qnil) ?(t = `D) f =
    let tbl =
        lazy begin
            let s = Seq.filter f all_uchars in
            match t with
            | `D -> Tbl_dset (D_set.of_seq s)
            | `R -> Tbl_rset (R_core.lift s |> R_set.of_seq)
        end
    in
    Map { q; v0; tbl }

let of_optmap ~v0 ?(q = qnil) ?eq f =
    let tbl =
        lazy begin
            let s = Seq.filter_map f all_uchars in
            match eq with
            | None -> Tbl_dmap (D_map.of_seq s)
            | Some f -> Tbl_rmap (R_core.lift2 f s |> R_map.of_seq)
        end
    in
    Map { q; v0; tbl }

let cut_prefix s =
    let n = String.length s in
    if n < 2 then s else begin
        let c0 = String.unsafe_get s 0 |> Char.lowercase_ascii in
        let c1 = String.unsafe_get s 1 |> Char.lowercase_ascii in
        if c0 = 'i' && c1 = 's' then String.sub s 2 (n - 2) else s
    end

let search_query_nym =
    let down = function
        | ('_' | '-' | ' ') -> None
        | 'A'..'Z' as c -> Some (Char.lowercase_ascii c)
        | c -> Some c
    in
    let enter nym =
        Buffer.contents @@ Buffer.of_seq @@ Seq.filter_map down @@
            String.to_seq nym
    in
    enter

let search_property_nym nym = cut_prefix nym |> search_query_nym

(*--- End ---*)
