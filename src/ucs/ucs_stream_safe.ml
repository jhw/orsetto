(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Cf_seq.Infix

module NFKD = Ucs_normal.NFKD

let succ_if_ccc_nonzero u n =
    let ccc =
        Ucs_db_aux.query_map Ucs_property_core.canonical_combining_class u
    in
    if ccc > 0 then
        succ n
    else
        0

let is_not_nfkd u =
    Ucs_db_aux.(query_map Ucs_property_core.quick_check_nfkd u <> QC_yes)

let check =
    let rec loop z n us =
        match us () with
        | Seq.Nil ->
            true
        | Seq.Cons (u, us) ->
            let z, us =
                if z <> `Z && is_not_nfkd u then
                    `N, (u @: us |> NFKD.transform)
                else
                    z, us
            in
            let n = succ_if_ccc_nonzero u n in
            if n > 30 then false else (loop[@tailcall]) z n us
    in
    loop `Z 0

let transform =
    let gcj = Uchar.unsafe_of_int 0x34f in
    let rec down n u =
        if is_not_nfkd u then
            Seq.return u |> NFKD.transform |> count n
        else
            succ_if_ccc_nonzero u n
    and count n us =
        match us () with
        | Seq.Nil -> n
        | Seq.Cons (u, us) -> (count[@tailcall]) (succ_if_ccc_nonzero u n) us
    in
    let rec loop n us () =
        match us () with
        | Seq.Nil ->
            Seq.Nil
        | Seq.Cons (u, us) ->
            let n = down n u in
            if n > 30 then
                Seq.Cons (gcj, u @: loop 0 us)
            else
                Seq.Cons (u, loop n us)
    in
    loop 0

(*--- End ---*)
