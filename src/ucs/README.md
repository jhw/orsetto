# src/ucs - Unicode character set

Many structured data interchange languages are either presented as text or they
admit elements that encode text. In both cases, Unicode is commonly used, but
not always. Accordingly, the Unicode support library is a separate component in
the Orsetto framework.

The necessary parts of the Unicode character set database are compiled into
binary search tables used by the normalization and regular expression modules
here. To conserve space, only those parts of the Unicode character set database
needed by the modules referenced in the application are linked.

The following modules are intended to comprise public interfaces:

- *Ucs_lex_scan* -- Unicode lexical analyzers.
- *Ucs_normal* -- Unicode Normalization Form
- *Ucs_regx* -- Unicode regular expressions
- *Ucs_scan* -- Functional scanner/parsers for Unicode texts.
- *Ucs_stream_safe* -- Unicode Stream Safe Format.
- *Ucs_text* -- Unicode text strings.
- *Ucs_transport* -- Unicode Transport Form (UTF)
- *Ucs_type* -- An extension of *Cf_type* that includes Unicode texts.

The remaining modules are internal to Orsetto, typically used as inclusions in
the public modules, and no commitment to their separate stability is offered.

- *Ucs_db_aux* -- Unicode character set property database support.
- *Ucs_property_core* -- Unicode character set property database.
- *Ucs_transport_aux* -- Partial implementation of UTF transports.
- *Ucs_transport_utf8* -- Implementation of UTF-8
- *Ucs_transport_utf16* -- Implementation of UTF-16
- *Ucs_transport_utf32* -- Implementation of UTF-32
- *Ucs_trie* -- A currently unused alternative to binary search data tables.
- *Ucs_ucdgen_aux* -- A common module between the Ucs library and its code
    generator that builds binary search tables.