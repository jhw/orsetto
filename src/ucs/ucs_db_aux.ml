(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

include Ucs_ucdgen_aux

type utyp = ..
type 'a index = 'a Nym_map.t

type utyp +=
    | Typ_bool of bool map * bool index
    | Typ_int of int map * int index
    | Typ_string of string map * string index
    | Typ_uchars of Uchar.t list option map * Uchar.t list option index

let create_index s = Nym_map.of_seq @@ List.to_seq s

let search_index idx name = Nym_map.search (search_query_nym name) idx
let search_property idx name = Nym_map.search (search_property_nym name) idx
let require_property idx name = Nym_map.require (search_property_nym name) idx

type blk = [
    | `ASCII
    | `Adlam
    | `Aegean_Numbers
    | `Ahom
    | `Alchemical
    | `Alphabetic_PF
    | `Anatolian_Hieroglyphs
    | `Ancient_Greek_Music
    | `Ancient_Greek_Numbers
    | `Ancient_Symbols
    | `Arabic
    | `Arabic_Ext_A
    | `Arabic_Ext_B
    | `Arabic_Ext_C
    | `Arabic_Math
    | `Arabic_PF_A
    | `Arabic_PF_B
    | `Arabic_Sup
    | `Armenian
    | `Arrows
    | `Avestan
    | `Balinese
    | `Bamum
    | `Bamum_Sup
    | `Bassa_Vah
    | `Batak
    | `Bengali
    | `Bhaiksuki
    | `Block_Elements
    | `Bopomofo
    | `Bopomofo_Ext
    | `Box_Drawing
    | `Brahmi
    | `Braille
    | `Buginese
    | `Buhid
    | `Byzantine_Music
    | `CJK
    | `CJK_Compat
    | `CJK_Compat_Forms
    | `CJK_Compat_Ideographs
    | `CJK_Compat_Ideographs_Sup
    | `CJK_Ext_A
    | `CJK_Ext_B
    | `CJK_Ext_C
    | `CJK_Ext_D
    | `CJK_Ext_E
    | `CJK_Ext_F
    | `CJK_Ext_G
    | `CJK_Ext_H
    | `CJK_Ext_I
    | `CJK_Radicals_Sup
    | `CJK_Strokes
    | `CJK_Symbols
    | `Carian
    | `Caucasian_Albanian
    | `Chakma
    | `Cham
    | `Cherokee
    | `Cherokee_Sup
    | `Chess_Symbols
    | `Chorasmian
    | `Compat_Jamo
    | `Control_Pictures
    | `Coptic
    | `Coptic_Epact_Numbers
    | `Counting_Rod
    | `Cuneiform
    | `Cuneiform_Numbers
    | `Currency_Symbols
    | `Cypriot_Syllabary
    | `Cypro_Minoan
    | `Cyrillic
    | `Cyrillic_Ext_A
    | `Cyrillic_Ext_B
    | `Cyrillic_Ext_C
    | `Cyrillic_Ext_D
    | `Cyrillic_Sup
    | `Deseret
    | `Devanagari
    | `Devanagari_Ext
    | `Devanagari_Ext_A
    | `Diacriticals
    | `Diacriticals_Ext
    | `Diacriticals_For_Symbols
    | `Diacriticals_Sup
    | `Dingbats
    | `Dives_Akuru
    | `Dogra
    | `Domino
    | `Duployan
    | `Early_Dynastic_Cuneiform
    | `Egyptian_Hieroglyphs
    | `Egyptian_Hieroglyph_Format_Controls
    | `Elbasan
    | `Elymaic
    | `Emoticons
    | `Enclosed_Alphanum
    | `Enclosed_Alphanum_Sup
    | `Enclosed_CJK
    | `Enclosed_Ideographic_Sup
    | `Ethiopic
    | `Ethiopic_Ext
    | `Ethiopic_Ext_A
    | `Ethiopic_Ext_B
    | `Ethiopic_Sup
    | `Geometric_Shapes
    | `Geometric_Shapes_Ext
    | `Georgian
    | `Georgian_Ext
    | `Georgian_Sup
    | `Glagolitic
    | `Glagolitic_Sup
    | `Gothic
    | `Grantha
    | `Greek
    | `Greek_Ext
    | `Gujarati
    | `Gunjala_Gondi
    | `Gurmukhi
    | `Half_And_Full_Forms
    | `Half_Marks
    | `Hangul
    | `Hanifi_Rohingya
    | `Hanunoo
    | `Hatran
    | `Hebrew
    | `High_PU_Surrogates
    | `High_Surrogates
    | `Hiragana
    | `IDC
    | `IPA_Ext
    | `Ideographic_Symbols
    | `Imperial_Aramaic
    | `Indic_Number_Forms
    | `Indic_Siyaq_Numbers
    | `Inscriptional_Pahlavi
    | `Inscriptional_Parthian
    | `Jamo
    | `Jamo_Ext_A
    | `Jamo_Ext_B
    | `Javanese
    | `Kaithi
    | `Kaktovik_Numerals
    | `Kana_Ext_A
    | `Kana_Ext_B
    | `Kana_Sup
    | `Kanbun
    | `Kangxi
    | `Kannada
    | `Katakana
    | `Katakana_Ext
    | `Kayah_Li
    | `Kawi
    | `Kharoshthi
    | `Khitan_Small_Script
    | `Khmer
    | `Khmer_Symbols
    | `Khojki
    | `Khudawadi
    | `Lao
    | `Latin_1_Sup
    | `Latin_Ext_A
    | `Latin_Ext_Additional
    | `Latin_Ext_B
    | `Latin_Ext_C
    | `Latin_Ext_D
    | `Latin_Ext_E
    | `Latin_Ext_F
    | `Latin_Ext_G
    | `Lepcha
    | `Letterlike_Symbols
    | `Limbu
    | `Linear_A
    | `Linear_B_Ideograms
    | `Linear_B_Syllabary
    | `Lisu
    | `Lisu_Sup
    | `Low_Surrogates
    | `Lycian
    | `Lydian
    | `Mahajani
    | `Mahjong
    | `Makasar
    | `Malayalam
    | `Mandaic
    | `Manichaean
    | `Marchen
    | `Masaram_Gondi
    | `Math_Alphanum
    | `Math_Operators
    | `Mayan_Numerals
    | `Medefaidrin
    | `Meetei_Mayek
    | `Meetei_Mayek_Ext
    | `Mende_Kikakui
    | `Meroitic_Cursive
    | `Meroitic_Hieroglyphs
    | `Miao
    | `Misc_Arrows
    | `Misc_Math_Symbols_A
    | `Misc_Math_Symbols_B
    | `Misc_Pictographs
    | `Misc_Symbols
    | `Misc_Technical
    | `Modi
    | `Modifier_Letters
    | `Modifier_Tone_Letters
    | `Mongolian
    | `Mongolian_Sup
    | `Mro
    | `Multani
    | `Music
    | `Myanmar
    | `Myanmar_Ext_A
    | `Myanmar_Ext_B
    | `NB
    | `NKo
    | `Nabataean
    | `Nag_Mundari
    | `Nandinagari
    | `New_Tai_Lue
    | `Newa
    | `No_Block_Assigned
    | `Number_Forms
    | `Nushu
    | `Nyiakeng_Puachue_Hmong
    | `OCR
    | `Ogham
    | `Ol_Chiki
    | `Old_Hungarian
    | `Old_Italic
    | `Old_North_Arabian
    | `Old_Permic
    | `Old_Persian
    | `Old_Sogdian
    | `Old_South_Arabian
    | `Old_Turkic
    | `Old_Uyghur
    | `Oriya
    | `Ornamental_Dingbats
    | `Osage
    | `Osmanya
    | `Ottoman_Siyaq_Numbers
    | `PUA
    | `Pahawh_Hmong
    | `Palmyrene
    | `Pau_Cin_Hau
    | `Phags_Pa
    | `Phaistos
    | `Phoenician
    | `Phonetic_Ext
    | `Phonetic_Ext_Sup
    | `Playing_Cards
    | `Psalter_Pahlavi
    | `Punctuation
    | `Rejang
    | `Rumi
    | `Runic
    | `Samaritan
    | `Saurashtra
    | `Sharada
    | `Shavian
    | `Shorthand_Format_Controls
    | `Siddham
    | `Sinhala
    | `Sinhala_Archaic_Numbers
    | `Small_Forms
    | `Small_Kana_Ext
    | `Sogdian
    | `Sora_Sompeng
    | `Soyombo
    | `Specials
    | `Sundanese
    | `Sundanese_Sup
    | `Sup_Arrows_A
    | `Sup_Arrows_B
    | `Sup_Arrows_C
    | `Sup_Math_Operators
    | `Sup_PUA_A
    | `Sup_PUA_B
    | `Sup_Punctuation
    | `Sup_Symbols_And_Pictographs
    | `Super_And_Sub
    | `Sutton_SignWriting
    | `Syloti_Nagri
    | `Symbols_And_Pictographs_Ext_A
    | `Symbols_For_Legacy_Computing
    | `Syriac
    | `Syriac_Sup
    | `Tagalog
    | `Tagbanwa
    | `Tags
    | `Tai_Le
    | `Tai_Tham
    | `Tai_Viet
    | `Tai_Xuan_Jing
    | `Takri
    | `Tamil
    | `Tamil_Sup
    | `Tangsa
    | `Tangut
    | `Tangut_Components
    | `Tangut_Sup
    | `Telugu
    | `Thaana
    | `Thai
    | `Tibetan
    | `Tifinagh
    | `Tirhuta
    | `Toto
    | `Transport_And_Map
    | `UCAS
    | `UCAS_Ext
    | `UCAS_Ext_A
    | `Ugaritic
    | `VS
    | `VS_Sup
    | `Vai
    | `Vedic_Ext
    | `Vertical_Forms
    | `Vithkuqi
    | `Wancho
    | `Warang_Citi
    | `Yezidi
    | `Yi_Radicals
    | `Yi_Syllables
    | `Yijing
    | `Zanabazar_Square
    | `Znamenny_Music
]

let equal_blk a b = (a : blk) == (b : blk)

let show_blk = function
    | `ASCII -> "`ASCII"
    | `Adlam -> "`Adlam"
    | `Aegean_Numbers -> "`Aegean_Numbers"
    | `Ahom -> "`Ahom"
    | `Alchemical -> "`Alchemical"
    | `Alphabetic_PF -> "`Alphabetic_PF"
    | `Anatolian_Hieroglyphs -> "`Anatolian_Hieroglyphs"
    | `Ancient_Greek_Music -> "`Ancient_Greek_Music"
    | `Ancient_Greek_Numbers -> "`Ancient_Greek_Numbers"
    | `Ancient_Symbols -> "`Ancient_Symbols"
    | `Arabic -> "`Arabic"
    | `Arabic_Ext_A -> "`Arabic_Ext_A"
    | `Arabic_Ext_B -> "`Arabic_Ext_B"
    | `Arabic_Ext_C -> "`Arabic_Ext_C"
    | `Arabic_Math -> "`Arabic_Math"
    | `Arabic_PF_A -> "`Arabic_PF_A"
    | `Arabic_PF_B -> "`Arabic_PF_B"
    | `Arabic_Sup -> "`Arabic_Sup"
    | `Armenian -> "`Armenian"
    | `Arrows -> "`Arrows"
    | `Avestan -> "`Avestan"
    | `Balinese -> "`Balinese"
    | `Bamum -> "`Bamum"
    | `Bamum_Sup -> "`Bamum_Sup"
    | `Bassa_Vah -> "`Bassa_Vah"
    | `Batak -> "`Batak"
    | `Bengali -> "`Bengali"
    | `Bhaiksuki -> "`Bhaiksuki"
    | `Block_Elements -> "`Block_Elements"
    | `Bopomofo -> "`Bopomofo"
    | `Bopomofo_Ext -> "`Bopomofo_Ext"
    | `Box_Drawing -> "`Box_Drawing"
    | `Brahmi -> "`Brahmi"
    | `Braille -> "`Braille"
    | `Buginese -> "`Buginese"
    | `Buhid -> "`Buhid"
    | `Byzantine_Music -> "`Byzantine_Music"
    | `CJK -> "`CJK"
    | `CJK_Compat -> "`CJK_Compat"
    | `CJK_Compat_Forms -> "`CJK_Compat_Forms"
    | `CJK_Compat_Ideographs -> "`CJK_Compat_Ideographs"
    | `CJK_Compat_Ideographs_Sup -> "`CJK_Compat_Ideographs_Sup"
    | `CJK_Ext_A -> "`CJK_Ext_A"
    | `CJK_Ext_B -> "`CJK_Ext_B"
    | `CJK_Ext_C -> "`CJK_Ext_C"
    | `CJK_Ext_D -> "`CJK_Ext_D"
    | `CJK_Ext_E -> "`CJK_Ext_E"
    | `CJK_Ext_F -> "`CJK_Ext_F"
    | `CJK_Ext_G -> "`CJK_Ext_G"
    | `CJK_Ext_H -> "`CJK_Ext_H"
    | `CJK_Ext_I -> "`CJK_Ext_I"
    | `CJK_Radicals_Sup -> "`CJK_Radicals_Sup"
    | `CJK_Strokes -> "`CJK_Strokes"
    | `CJK_Symbols -> "`CJK_Symbols"
    | `Carian -> "`Carian"
    | `Caucasian_Albanian -> "`Caucasian_Albanian"
    | `Chakma -> "`Chakma"
    | `Cham -> "`Cham"
    | `Cherokee -> "`Cherokee"
    | `Cherokee_Sup -> "`Cherokee_Sup"
    | `Chess_Symbols -> "`Chess_Symbols"
    | `Chorasmian -> "`Chorasmian"
    | `Compat_Jamo -> "`Compat_Jamo"
    | `Control_Pictures -> "`Control_Pictures"
    | `Coptic -> "`Coptic"
    | `Coptic_Epact_Numbers -> "`Coptic_Epact_Numbers"
    | `Counting_Rod -> "`Counting_Rod"
    | `Cuneiform -> "`Cuneiform"
    | `Cuneiform_Numbers -> "`Cuneiform_Numbers"
    | `Currency_Symbols -> "`Currency_Symbols"
    | `Cypriot_Syllabary -> "`Cypriot_Syllabary"
    | `Cypro_Minoan -> "`Cypro_Minoan"
    | `Cyrillic -> "`Cyrillic"
    | `Cyrillic_Ext_A -> "`Cyrillic_Ext_A"
    | `Cyrillic_Ext_B -> "`Cyrillic_Ext_B"
    | `Cyrillic_Ext_C -> "`Cyrillic_Ext_C"
    | `Cyrillic_Ext_D -> "`Cyrillic_Ext_D"
    | `Cyrillic_Sup -> "`Cyrillic_Sup"
    | `Deseret -> "`Deseret"
    | `Devanagari -> "`Devanagari"
    | `Devanagari_Ext -> "`Devanagari_Ext"
    | `Devanagari_Ext_A -> "`Devanagari_Ext_A"
    | `Diacriticals -> "`Diacriticals"
    | `Diacriticals_Ext -> "`Diacriticals_Ext"
    | `Diacriticals_For_Symbols -> "`Diacriticals_For_Symbols"
    | `Diacriticals_Sup -> "`Diacriticals_Sup"
    | `Dingbats -> "`Dingbats"
    | `Dives_Akuru -> "`Dives_Akuru"
    | `Dogra -> "`Dogra"
    | `Domino -> "`Domino"
    | `Duployan -> "`Duployan"
    | `Early_Dynastic_Cuneiform -> "`Early_Dynastic_Cuneiform"
    | `Egyptian_Hieroglyphs -> "`Egyptian_Hieroglyphs"
    | `Egyptian_Hieroglyph_Format_Controls ->
        "Egyptian_Hieroglyph_Format_Controls"
    | `Elbasan -> "`Elbasan"
    | `Elymaic -> "`Elymaic"
    | `Emoticons -> "`Emoticons"
    | `Enclosed_Alphanum -> "`Enclosed_Alphanum"
    | `Enclosed_Alphanum_Sup -> "`Enclosed_Alphanum_Sup"
    | `Enclosed_CJK -> "`Enclosed_CJK"
    | `Enclosed_Ideographic_Sup -> "`Enclosed_Ideographic_Sup"
    | `Ethiopic -> "`Ethiopic"
    | `Ethiopic_Ext -> "`Ethiopic_Ext"
    | `Ethiopic_Ext_A -> "`Ethiopic_Ext_A"
    | `Ethiopic_Ext_B -> "`Ethiopic_Ext_B"
    | `Ethiopic_Sup -> "`Ethiopic_Sup"
    | `Geometric_Shapes -> "`Geometric_Shapes"
    | `Geometric_Shapes_Ext -> "`Geometric_Shapes_Ext"
    | `Georgian -> "`Georgian"
    | `Georgian_Ext -> "`Georgian_Ext"
    | `Georgian_Sup -> "`Georgian_Sup"
    | `Glagolitic -> "`Glagolitic"
    | `Glagolitic_Sup -> "`Glagolitic_Sup"
    | `Gothic -> "`Gothic"
    | `Grantha -> "`Grantha"
    | `Greek -> "`Greek"
    | `Greek_Ext -> "`Greek_Ext"
    | `Gujarati -> "`Gujarati"
    | `Gunjala_Gondi -> "`Gunjala_Gondi"
    | `Gurmukhi -> "`Gurmukhi"
    | `Half_And_Full_Forms -> "`Half_And_Full_Forms"
    | `Half_Marks -> "`Half_Marks"
    | `Hangul -> "`Hangul"
    | `Hanifi_Rohingya -> "`Hanifi_Rohingya"
    | `Hanunoo -> "`Hanunoo"
    | `Hatran -> "`Hatran"
    | `Hebrew -> "`Hebrew"
    | `High_PU_Surrogates -> "`High_PU_Surrogates"
    | `High_Surrogates -> "`High_Surrogates"
    | `Hiragana -> "`Hiragana"
    | `IDC -> "`IDC"
    | `IPA_Ext -> "`IPA_Ext"
    | `Ideographic_Symbols -> "`Ideographic_Symbols"
    | `Imperial_Aramaic -> "`Imperial_Aramaic"
    | `Indic_Number_Forms -> "`Indic_Number_Forms"
    | `Indic_Siyaq_Numbers -> "`Indic_Siyaq_Numbers"
    | `Inscriptional_Pahlavi -> "`Inscriptional_Pahlavi"
    | `Inscriptional_Parthian -> "`Inscriptional_Parthian"
    | `Jamo -> "`Jamo"
    | `Jamo_Ext_A -> "`Jamo_Ext_A"
    | `Jamo_Ext_B -> "`Jamo_Ext_B"
    | `Javanese -> "`Javanese"
    | `Kaithi -> "`Kaithi"
    | `Kaktovik_Numerals -> "`Kaktovik_Numerals"
    | `Kana_Ext_A -> "`Kana_Ext_A"
    | `Kana_Ext_B -> "`Kana_Ext_B"
    | `Kana_Sup -> "`Kana_Sup"
    | `Kanbun -> "`Kanbun"
    | `Kangxi -> "`Kangxi"
    | `Kannada -> "`Kannada"
    | `Katakana -> "`Katakana"
    | `Katakana_Ext -> "`Katakana_Ext"
    | `Kayah_Li -> "`Kayah_Li"
    | `Kawi -> "`Kawi"
    | `Kharoshthi -> "`Kharoshthi"
    | `Khitan_Small_Script -> "`Khitan_Small_Script"
    | `Khmer -> "`Khmer"
    | `Khmer_Symbols -> "`Khmer_Symbols"
    | `Khojki -> "`Khojki"
    | `Khudawadi -> "`Khudawadi"
    | `Lao -> "`Lao"
    | `Latin_1_Sup -> "`Latin_1_Sup"
    | `Latin_Ext_A -> "`Latin_Ext_A"
    | `Latin_Ext_Additional -> "`Latin_Ext_Additional"
    | `Latin_Ext_B -> "`Latin_Ext_B"
    | `Latin_Ext_C -> "`Latin_Ext_C"
    | `Latin_Ext_D -> "`Latin_Ext_D"
    | `Latin_Ext_E -> "`Latin_Ext_E"
    | `Latin_Ext_F -> "`Latin_Ext_F"
    | `Latin_Ext_G -> "`Latin_Ext_G"
    | `Lepcha -> "`Lepcha"
    | `Letterlike_Symbols -> "`Letterlike_Symbols"
    | `Limbu -> "`Limbu"
    | `Linear_A -> "`Linear_A"
    | `Linear_B_Ideograms -> "`Linear_B_Ideograms"
    | `Linear_B_Syllabary -> "`Linear_B_Syllabary"
    | `Lisu -> "`Lisu"
    | `Lisu_Sup -> "Lisu_Sup"
    | `Low_Surrogates -> "`Low_Surrogates"
    | `Lycian -> "`Lycian"
    | `Lydian -> "`Lydian"
    | `Mahajani -> "`Mahajani"
    | `Mahjong -> "`Mahjong"
    | `Makasar -> "`Makasar"
    | `Malayalam -> "`Malayalam"
    | `Mandaic -> "`Mandaic"
    | `Manichaean -> "`Manichaean"
    | `Marchen -> "`Marchen"
    | `Masaram_Gondi -> "`Masaram_Gondi"
    | `Math_Alphanum -> "`Math_Alphanum"
    | `Math_Operators -> "`Math_Operators"
    | `Mayan_Numerals -> "`Mayan_Numerals"
    | `Medefaidrin -> "`Medefaidrin"
    | `Meetei_Mayek -> "`Meetei_Mayek"
    | `Meetei_Mayek_Ext -> "`Meetei_Mayek_Ext"
    | `Mende_Kikakui -> "`Mende_Kikakui"
    | `Meroitic_Cursive -> "`Meroitic_Cursive"
    | `Meroitic_Hieroglyphs -> "`Meroitic_Hieroglyphs"
    | `Miao -> "`Miao"
    | `Misc_Arrows -> "`Misc_Arrows"
    | `Misc_Math_Symbols_A -> "`Misc_Math_Symbols_A"
    | `Misc_Math_Symbols_B -> "`Misc_Math_Symbols_B"
    | `Misc_Pictographs -> "`Misc_Pictographs"
    | `Misc_Symbols -> "`Misc_Symbols"
    | `Misc_Technical -> "`Misc_Technical"
    | `Modi -> "`Modi"
    | `Modifier_Letters -> "`Modifier_Letters"
    | `Modifier_Tone_Letters -> "`Modifier_Tone_Letters"
    | `Mongolian -> "`Mongolian"
    | `Mongolian_Sup -> "`Mongolian_Sup"
    | `Mro -> "`Mro"
    | `Multani -> "`Multani"
    | `Music -> "`Music"
    | `Myanmar -> "`Myanmar"
    | `Myanmar_Ext_A -> "`Myanmar_Ext_A"
    | `Myanmar_Ext_B -> "`Myanmar_Ext_B"
    | `NB -> "`NB"
    | `NKo -> "`NKo"
    | `Nabataean -> "`Nabataean"
    | `Nag_Mundari -> "`Nag_Mundari"
    | `Nandinagari -> "`Nandinagari"
    | `New_Tai_Lue -> "`New_Tai_Lue"
    | `Newa -> "`Newa"
    | `No_Block_Assigned -> "`No_Block_Assigned"
    | `Number_Forms -> "`Number_Forms"
    | `Nushu -> "`Nushu"
    | `Nyiakeng_Puachue_Hmong -> "`Nyiakeng_Puachue_Hmong"
    | `OCR -> "`OCR"
    | `Ogham -> "`Ogham"
    | `Ol_Chiki -> "`Ol_Chiki"
    | `Old_Hungarian -> "`Old_Hungarian"
    | `Old_Italic -> "`Old_Italic"
    | `Old_North_Arabian -> "`Old_North_Arabian"
    | `Old_Permic -> "`Old_Permic"
    | `Old_Persian -> "`Old_Persian"
    | `Old_Sogdian -> "`Old_Sogdian"
    | `Old_South_Arabian -> "`Old_South_Arabian"
    | `Old_Turkic -> "`Old_Turkic"
    | `Old_Uyghur -> "`Old_Uyghur"
    | `Oriya -> "`Oriya"
    | `Ornamental_Dingbats -> "`Ornamental_Dingbats"
    | `Osage -> "`Osage"
    | `Osmanya -> "`Osmanya"
    | `Ottoman_Siyaq_Numbers -> "`Ottoman_Siyaq_Numbers"
    | `PUA -> "`PUA"
    | `Pahawh_Hmong -> "`Pahawh_Hmong"
    | `Palmyrene -> "`Palmyrene"
    | `Pau_Cin_Hau -> "`Pau_Cin_Hau"
    | `Phags_Pa -> "`Phags_Pa"
    | `Phaistos -> "`Phaistos"
    | `Phoenician -> "`Phoenician"
    | `Phonetic_Ext -> "`Phonetic_Ext"
    | `Phonetic_Ext_Sup -> "`Phonetic_Ext_Sup"
    | `Playing_Cards -> "`Playing_Cards"
    | `Psalter_Pahlavi -> "`Psalter_Pahlavi"
    | `Punctuation -> "`Punctuation"
    | `Rejang -> "`Rejang"
    | `Rumi -> "`Rumi"
    | `Runic -> "`Runic"
    | `Samaritan -> "`Samaritan"
    | `Saurashtra -> "`Saurashtra"
    | `Sharada -> "`Sharada"
    | `Shavian -> "`Shavian"
    | `Shorthand_Format_Controls -> "`Shorthand_Format_Controls"
    | `Siddham -> "`Siddham"
    | `Sinhala -> "`Sinhala"
    | `Sinhala_Archaic_Numbers -> "`Sinhala_Archaic_Numbers"
    | `Small_Forms -> "`Small_Forms"
    | `Small_Kana_Ext -> "`Small_Kana_Ext"
    | `Sogdian -> "`Sogdian"
    | `Sora_Sompeng -> "`Sora_Sompeng"
    | `Soyombo -> "`Soyombo"
    | `Specials -> "`Specials"
    | `Sundanese -> "`Sundanese"
    | `Sundanese_Sup -> "`Sundanese_Sup"
    | `Sup_Arrows_A -> "`Sup_Arrows_A"
    | `Sup_Arrows_B -> "`Sup_Arrows_B"
    | `Sup_Arrows_C -> "`Sup_Arrows_C"
    | `Sup_Math_Operators -> "`Sup_Math_Operators"
    | `Sup_PUA_A -> "`Sup_PUA_A"
    | `Sup_PUA_B -> "`Sup_PUA_B"
    | `Sup_Punctuation -> "`Sup_Punctuation"
    | `Sup_Symbols_And_Pictographs -> "`Sup_Symbols_And_Pictographs"
    | `Super_And_Sub -> "`Super_And_Sub"
    | `Sutton_SignWriting -> "`Sutton_SignWriting"
    | `Syloti_Nagri -> "`Syloti_Nagri"
    | `Symbols_And_Pictographs_Ext_A -> "`Symbols_And_Pictographs_Ext_A"
    | `Symbols_For_Legacy_Computing -> "`Symbols_For_Legacy_Computing"
    | `Syriac -> "`Syriac"
    | `Syriac_Sup -> "`Syriac_Sup"
    | `Tagalog -> "`Tagalog"
    | `Tagbanwa -> "`Tagbanwa"
    | `Tags -> "`Tags"
    | `Tai_Le -> "`Tai_Le"
    | `Tai_Tham -> "`Tai_Tham"
    | `Tai_Viet -> "`Tai_Viet"
    | `Tai_Xuan_Jing -> "`Tai_Xuan_Jing"
    | `Takri -> "`Takri"
    | `Tamil -> "`Tamil"
    | `Tamil_Sup -> "`Tamil_Sup"
    | `Tangsa -> "`Tangsa"
    | `Tangut -> "`Tangut"
    | `Tangut_Sup -> "`Tangut_Sup"
    | `Tangut_Components -> "`Tangut_Components"
    | `Telugu -> "`Telugu"
    | `Thaana -> "`Thaana"
    | `Thai -> "`Thai"
    | `Tibetan -> "`Tibetan"
    | `Tifinagh -> "`Tifinagh"
    | `Tirhuta -> "`Tirhuta"
    | `Toto -> "`Toto"
    | `Transport_And_Map -> "`Transport_And_Map"
    | `UCAS -> "`UCAS"
    | `UCAS_Ext -> "`UCAS_Ext"
    | `UCAS_Ext_A -> "`UCAS_Ext_A"
    | `Ugaritic -> "`Ugaritic"
    | `VS -> "`VS"
    | `VS_Sup -> "`VS_Sup"
    | `Vai -> "`Vai"
    | `Vedic_Ext -> "`Vedic_Ext"
    | `Vertical_Forms -> "`Vertical_Forms"
    | `Vithkuqi -> "`Vithkuqi"
    | `Wancho -> "`Wancho"
    | `Warang_Citi -> "`Warang_Citi"
    | `Yezidi -> "`Yezidi"
    | `Yi_Radicals -> "`Yi_Radicals"
    | `Yi_Syllables -> "`Yi_Syllables"
    | `Yijing -> "`Yijing"
    | `Zanabazar_Square -> "`Zanabazar_Square"
    | `Znamenny_Music -> "`Znamenny_Music"

type utyp +=
    | Typ_block of blk map * blk index

(** The general category property value type. *)
type gc = [
    | `C
    | `Cc
    | `Cf
    | `Cs
    | `Co
    | `Cn
    | `L
    | `LC
    | `Lu
    | `Ll
    | `Lt
    | `Lm
    | `Lo
    | `M
    | `Mn
    | `Mc
    | `Me
    | `N
    | `Nd
    | `Nl
    | `No
    | `P
    | `Pc
    | `Pd
    | `Ps
    | `Pe
    | `Pi
    | `Pf
    | `Po
    | `S
    | `Sm
    | `Sc
    | `Sk
    | `So
    | `Z
    | `Zs
    | `Zl
    | `Zp
]

let equal_gc a b = begin[@warning "-4"]
    match a, b with
    | `C, `C
    | `Cc, `Cc
    | `Cf, `Cf
    | `Cs, `Cs
    | `Co, `Co
    | `Cn, `Cn
    | `L, `L
    | `LC, `LC
    | `Lu, `Lu
    | `Ll, `Ll
    | `Lt, `Lt
    | `Lm, `Lm
    | `Lo, `Lo
    | `M, `M
    | `Mn, `Mn
    | `Mc, `Mc
    | `Me, `Me
    | `N, `N
    | `Nd, `Nd
    | `Nl, `Nl
    | `No, `No
    | `P, `P
    | `Pc, `Pc
    | `Pd, `Pd
    | `Ps, `Ps
    | `Pe, `Pe
    | `Pi, `Pi
    | `Pf, `Pf
    | `Po, `Po
    | `S, `S
    | `Sm, `Sm
    | `Sc, `Sc
    | `Sk, `Sk
    | `So, `So
    | `Z, `Z
    | `Zs, `Zs
    | `Zl, `Zl
    | `Zp, `Zp ->
        true
    | _ ->
        false
end

let show_gc = function
    | `C -> "`C"
    | `Cc -> "`Cc"
    | `Cf -> "`Cf"
    | `Cs -> "`Cs"
    | `Co -> "`Co"
    | `Cn -> "`Cn"
    | `L -> "`L"
    | `LC -> "`LC"
    | `Lu -> "`Lu"
    | `Ll -> "`Ll"
    | `Lt -> "`Lt"
    | `Lm -> "`Lm"
    | `Lo -> "`Lo"
    | `M -> "`M"
    | `Mn -> "`Mn"
    | `Mc -> "`Mc"
    | `Me -> "`Me"
    | `N -> "`N"
    | `Nd -> "`Nd"
    | `Nl -> "`Nl"
    | `No -> "`No"
    | `P -> "`P"
    | `Pc -> "`Pc"
    | `Pd -> "`Pd"
    | `Ps -> "`Ps"
    | `Pe -> "`Pe"
    | `Pi -> "`Pi"
    | `Pf -> "`Pf"
    | `Po -> "`Po"
    | `S -> "`S"
    | `Sm -> "`Sm"
    | `Sc -> "`Sc"
    | `Sk -> "`Sk"
    | `So -> "`So"
    | `Z -> "`Z"
    | `Zs -> "`Zs"
    | `Zl -> "`Zl"
    | `Zp -> "`Zp"

type utyp +=
    | Typ_general_category of gc map * gc index

type qc = QC_yes | QC_no | QC_maybe

let equal_qc a b = begin[@warning "-4"]
    match a, b with
    | QC_yes, QC_yes
    | QC_no, QC_no
    | QC_maybe, QC_maybe ->
        true
    | _, _ ->
        false
end

let show_qc = function
    | QC_yes -> "Ucs_db_aux.QC_yes"
    | QC_no -> "Ucs_db_aux.QC_no"
    | QC_maybe -> "Ucs_db_aux.QC_maybe"

type utyp +=
    | Typ_quick_check of qc map * qc index

type script = [
    | `Adlm
    | `Aghb
    | `Ahom
    | `Arab
    | `Armi
    | `Armn
    | `Avst
    | `Bali
    | `Bamu
    | `Bass
    | `Batk
    | `Beng
    | `Bhks
    | `Bopo
    | `Brah
    | `Brai
    | `Bugi
    | `Buhd
    | `Cakm
    | `Cans
    | `Cari
    | `Cham
    | `Cher
    | `Chrs
    | `Copt
    | `Cpmn
    | `Cprt
    | `Cyrl
    | `Deva
    | `Diak
    | `Dogr
    | `Dsrt
    | `Dupl
    | `Egyp
    | `Elba
    | `Elym
    | `Ethi
    | `Geor
    | `Glag
    | `Gong
    | `Gonm
    | `Goth
    | `Gran
    | `Grek
    | `Gujr
    | `Guru
    | `Hang
    | `Hani
    | `Hano
    | `Hatr
    | `Hebr
    | `Hira
    | `Hluw
    | `Hmng
    | `Hmnp
    | `Hrkt
    | `Hung
    | `Ital
    | `Java
    | `Kali
    | `Kana
    | `Kawi
    | `Khar
    | `Khmr
    | `Khoj
    | `Kits
    | `Knda
    | `Kthi
    | `Lana
    | `Laoo
    | `Latn
    | `Lepc
    | `Limb
    | `Lina
    | `Linb
    | `Lisu
    | `Lyci
    | `Lydi
    | `Mahj
    | `Maka
    | `Mand
    | `Mani
    | `Marc
    | `Medf
    | `Mend
    | `Merc
    | `Mero
    | `Mlym
    | `Modi
    | `Mong
    | `Mroo
    | `Mtei
    | `Mult
    | `Mymr
    | `Nagm
    | `Nand
    | `Narb
    | `Nbat
    | `Newa
    | `Nkoo
    | `Nshu
    | `Ogam
    | `Olck
    | `Orkh
    | `Orya
    | `Osge
    | `Osma
    | `Ougr
    | `Palm
    | `Pauc
    | `Perm
    | `Phag
    | `Phli
    | `Phlp
    | `Phnx
    | `Plrd
    | `Prti
    | `Qaai
    | `Rjng
    | `Rohg
    | `Runr
    | `Samr
    | `Sarb
    | `Saur
    | `Sgnw
    | `Shaw
    | `Shrd
    | `Sidd
    | `Sind
    | `Sinh
    | `Sogd
    | `Sogo
    | `Sora
    | `Soyo
    | `Sund
    | `Sylo
    | `Syrc
    | `Tagb
    | `Takr
    | `Tale
    | `Talu
    | `Taml
    | `Tang
    | `Tavt
    | `Telu
    | `Tfng
    | `Tglg
    | `Thaa
    | `Thai
    | `Tibt
    | `Tirh
    | `Tnsa
    | `Toto
    | `Ugar
    | `Vaii
    | `Vith
    | `Wara
    | `Wcho
    | `Xpeo
    | `Xsux
    | `Yezi
    | `Yiii
    | `Zanb
    | `Zinh
    | `Zyyy
    | `Zzzz
]

let equal_script a b = (a : script) == (b : script)

let show_script = function
    | `Adlm -> "`Adlm"
    | `Aghb -> "`Aghb"
    | `Ahom -> "`Ahom"
    | `Arab -> "`Arab"
    | `Armi -> "`Armi"
    | `Armn -> "`Armn"
    | `Avst -> "`Avst"
    | `Bali -> "`Bali"
    | `Bamu -> "`Bamu"
    | `Bass -> "`Bass"
    | `Batk -> "`Batk"
    | `Beng -> "`Beng"
    | `Bhks -> "`Bhks"
    | `Bopo -> "`Bopo"
    | `Brah -> "`Brah"
    | `Brai -> "`Brai"
    | `Bugi -> "`Bugi"
    | `Buhd -> "`Buhd"
    | `Cakm -> "`Cakm"
    | `Cans -> "`Cans"
    | `Cari -> "`Cari"
    | `Cham -> "`Cham"
    | `Cher -> "`Cher"
    | `Chrs -> "`Chrs"
    | `Cpmn -> "`Cpmn"
    | `Copt -> "`Copt"
    | `Cprt -> "`Cprt"
    | `Cyrl -> "`Cyrl"
    | `Deva -> "`Deva"
    | `Diak -> "`Diak"
    | `Dogr -> "`Dogr"
    | `Dsrt -> "`Dsrt"
    | `Dupl -> "`Dupl"
    | `Egyp -> "`Egyp"
    | `Elba -> "`Elba"
    | `Elym -> "`Elym"
    | `Ethi -> "`Ethi"
    | `Geor -> "`Geor"
    | `Glag -> "`Glag"
    | `Gong -> "`Gong"
    | `Gonm -> "`Gonm"
    | `Goth -> "`Goth"
    | `Gran -> "`Gran"
    | `Grek -> "`Grek"
    | `Gujr -> "`Gujr"
    | `Guru -> "`Guru"
    | `Hang -> "`Hang"
    | `Hani -> "`Hani"
    | `Hano -> "`Hano"
    | `Hatr -> "`Hatr"
    | `Hebr -> "`Hebr"
    | `Hira -> "`Hira"
    | `Hluw -> "`Hluw"
    | `Hmng -> "`Hmng"
    | `Hmnp -> "`Hmnp"
    | `Hrkt -> "`Hrkt"
    | `Hung -> "`Hung"
    | `Ital -> "`Ital"
    | `Java -> "`Java"
    | `Kali -> "`Kali"
    | `Kana -> "`Kana"
    | `Kawi -> "`Kawi"
    | `Khar -> "`Khar"
    | `Khmr -> "`Khmr"
    | `Khoj -> "`Khoj"
    | `Knda -> "`Knda"
    | `Kits -> "`Kits"
    | `Kthi -> "`Kthi"
    | `Lana -> "`Lana"
    | `Laoo -> "`Laoo"
    | `Latn -> "`Latn"
    | `Lepc -> "`Lepc"
    | `Limb -> "`Limb"
    | `Lina -> "`Lina"
    | `Linb -> "`Linb"
    | `Lisu -> "`Lisu"
    | `Lyci -> "`Lyci"
    | `Lydi -> "`Lydi"
    | `Mahj -> "`Mahj"
    | `Maka -> "`Maka"
    | `Mand -> "`Mand"
    | `Mani -> "`Mani"
    | `Marc -> "`Marc"
    | `Medf -> "`Medf"
    | `Mend -> "`Mend"
    | `Merc -> "`Merc"
    | `Mero -> "`Mero"
    | `Mlym -> "`Mlym"
    | `Modi -> "`Modi"
    | `Mong -> "`Mong"
    | `Mroo -> "`Mroo"
    | `Mtei -> "`Mtei"
    | `Mult -> "`Mult"
    | `Mymr -> "`Mymr"
    | `Nagm -> "`Nagm"
    | `Nand -> "`Nand"
    | `Narb -> "`Narb"
    | `Nbat -> "`Nbat"
    | `Newa -> "`Newa"
    | `Nkoo -> "`Nkoo"
    | `Nshu -> "`Nshu"
    | `Ogam -> "`Ogam"
    | `Olck -> "`Olck"
    | `Orkh -> "`Orkh"
    | `Orya -> "`Orya"
    | `Osge -> "`Osge"
    | `Osma -> "`Osma"
    | `Ougr -> "`Ougr"
    | `Palm -> "`Palm"
    | `Pauc -> "`Pauc"
    | `Perm -> "`Perm"
    | `Phag -> "`Phag"
    | `Phli -> "`Phli"
    | `Phlp -> "`Phlp"
    | `Phnx -> "`Phnx"
    | `Plrd -> "`Plrd"
    | `Prti -> "`Prti"
    | `Qaai -> "`Qaai"
    | `Rjng -> "`Rjng"
    | `Rohg -> "`Rohg"
    | `Runr -> "`Runr"
    | `Samr -> "`Samr"
    | `Sarb -> "`Sarb"
    | `Saur -> "`Saur"
    | `Sgnw -> "`Sgnw"
    | `Shaw -> "`Shaw"
    | `Shrd -> "`Shrd"
    | `Sidd -> "`Sidd"
    | `Sind -> "`Sind"
    | `Sinh -> "`Sinh"
    | `Sogd -> "`Sogd"
    | `Sogo -> "`Sogo"
    | `Sora -> "`Sora"
    | `Soyo -> "`Soyo"
    | `Sund -> "`Sund"
    | `Sylo -> "`Sylo"
    | `Syrc -> "`Syrc"
    | `Tagb -> "`Tagb"
    | `Takr -> "`Takr"
    | `Tale -> "`Tale"
    | `Talu -> "`Talu"
    | `Taml -> "`Taml"
    | `Tang -> "`Tang"
    | `Tavt -> "`Tavt"
    | `Telu -> "`Telu"
    | `Tfng -> "`Tfng"
    | `Tglg -> "`Tglg"
    | `Thaa -> "`Thaa"
    | `Thai -> "`Thai"
    | `Tibt -> "`Tibt"
    | `Tirh -> "`Tirh"
    | `Tnsa -> "`Tnsa"
    | `Toto -> "`Toto"
    | `Ugar -> "`Ugar"
    | `Vaii -> "`Vaii"
    | `Vith -> "`Vith"
    | `Wara -> "`Wara"
    | `Wcho -> "`Wcho"
    | `Xpeo -> "`Xpeo"
    | `Xsux -> "`Xsux"
    | `Yezi -> "`Yezi"
    | `Yiii -> "`Yiii"
    | `Zanb -> "`Zanb"
    | `Zinh -> "`Zinh"
    | `Zyyy -> "`Zyyy"
    | `Zzzz -> "`Zzzz"

type utyp +=
    | Typ_script of script map * script index

module Quick = struct
    let some_zero = Some 0
    let some_qc_yes = Some QC_yes
    let some_blk_ascii = Some `ASCII
    let some_false = Some false
    let some_true = Some true

    let canonical_combining_class cp =
        if cp < min_combining_code then some_zero else None

    let quick_check_nfc cp =
        if cp < min_volatile_nfc_code then some_qc_yes else None

    let quick_check_nfd cp =
        if cp < min_volatile_nfd_code then some_qc_yes else None

    let quick_check_nfkc cp =
        if cp < min_volatile_nfkc_code then some_qc_yes else None

    let quick_check_nfkd cp =
        if cp < min_volatile_nfkd_code then some_qc_yes else None

    let block cp =
        if cp < 0x80 then some_blk_ascii else None

    let grapheme_base cp =
        if cp < 0x20 then
            some_false
        else if cp < 0x7f then
            some_true
        else if cp < 0xA0 then
            some_false
        else
            None
end

(*--- End ---*)
