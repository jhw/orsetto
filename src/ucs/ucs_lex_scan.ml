(*---------------------------------------------------------------------------*
  Copyright (C) 2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Aux(S: Ucs_scan.Profile) = struct
    type symbol = Uchar.t
    type position = Cf_annot.Textual.position
    type lexeme = Ucs_text.t

    type 'a form = 'a S.Annot.form

    module Scan = S
    module Form = S.Annot
    module DFA = Ucs_regx.DFA

    let string_to_term s = Ucs_text.of_string s |> DFA.text_to_term

    module Buffer = struct
        type t = {
            buf: Buffer.t;
            exr: Cf_encode.emitter;
        }

        let create () =
            let buf = Buffer.create 0 in
            let exr = Cf_encode.buffer_emitter buf in
            { buf; exr }

        let reset b = Buffer.reset b.buf
        let advance b uc = b.exr#emit Ucs_transport.UTF8.uchar_encode_scheme uc

        let lexeme b =
            Buffer.contents b.buf |> Ucs_text.Unsafe.of_string
    end
end

module type Profile = Cf_lex_scan.Profile
   with type symbol := Uchar.t
    and type lexeme := Ucs_text.t

module Create(S: Ucs_scan.Profile) = Cf_lex_scan.Create(Aux(S))

module UTF8 = Create(Ucs_scan.UTF8)

(*--- End ---*)
