(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open OUnit2

let opt_exhaustive =
    Conf.make_bool "exhaustive" false "Exhaustive normalization tests."

let opt_normtestdata =
    Conf.make_string "normtestdata" "NormalizationTest.txt"
        "Normalization test data"

let jout = Cf_journal.stdout
let _ = jout#setlimit `Debug
let jerr = Cf_journal.stderr
let _ = jerr#setlimit `Debug

(*
Gc.set {
    (Gc.get ()) with
    (* Gc.verbose = 0x3ff; *)
    Gc.verbose = 0x14;
};;

Gc.create_alarm begin fun () ->
    let min, pro, maj = Gc.counters () in
    Printf.printf "[Gc] minor=%f promoted=%f major=%f\n" min pro maj;
    flush stdout
end
*)

let _ = Printexc.record_backtrace true

let assert_formatted f =
    let pp = Format.str_formatter in
    Buffer.clear Format.stdbuf;
    f pp;
    Format.pp_print_flush pp ();
    assert_string (Buffer.contents Format.stdbuf)

module type T_signature = sig
    val test: test
end

module T_utf: T_signature = struct
    let of_cps cps = Array.of_seq @@ Seq.map Uchar.of_int @@ List.to_seq cps

    let equal_uchar_array a b =
        let a, b = Array.(to_seq a, to_seq b) in
        Cf_seq.eqf Uchar.equal a b

    let ck utf ca s ctxt =
        let utf = Ucs_transport.create utf in
        let module UTF = (val utf : Ucs_transport.Profile) in
        let ca' = Array.of_seq @@ UTF.seq_of_string s in
        let s' = UTF.seq_to_string @@ Array.to_seq ca in
        assert_equal ~ctxt ~cmp:String.equal ~msg:"encode" s s';
        assert_equal ~ctxt ~cmp:equal_uchar_array ~msg:"decode" ca ca'

    type case = Case of {
        ca: Uchar.t array;
        utf8: string;
        utf16be: string;
        utf16le: string;
        utf32be: string;
        utf32le: string;
    }

    let v0 = Case {
        ca = of_cps [];
        utf8 = "";
        utf16be = "";
        utf16le = "";
        utf32be = "";
        utf32le = "";
    }

    let abc = Case {
        ca = of_cps [ 0x61; 0x62; 0x63 ];
        utf8 = "abc";
        utf16be = "\x00\x61\x00\x62\x00\x63";
        utf16le = "\x61\x00\x62\x00\x63\x00";
        utf32be = "\x00\x00\x00\x61\x00\x00\x00\x62\x00\x00\x00\x63";
        utf32le = "\x61\x00\x00\x00\x62\x00\x00\x00\x63\x00\x00\x00";
    }

    let x4 = Case {
        ca = of_cps [ 0x24; 0xA2; 0x20AC; 0x10348 ];
        utf8 = "\x24\xC2\xA2\xE2\x82\xAC\xF0\x90\x8D\x88";
        utf16be = "\x00\x24\x00\xA2\x20\xAC\xD8\x00\xDF\x48";
        utf16le = "\x24\x00\xA2\x00\xAC\x20\x00\xD8\x48\xDF";
        utf32be =
            "\x00\x00\x00\x24" ^
            "\x00\x00\x00\xA2" ^
            "\x00\x00\x20\xAC" ^
            "\x00\x01\x03\x48";
        utf32le =
            "\x24\x00\x00\x00" ^
            "\xA2\x00\x00\x00" ^
            "\xAC\x20\x00\x00" ^
            "\x48\x03\x01\x00";
    }

    let of_case id (Case c) =
        id >::: [
            "UTF8" >:: ck `UTF8 c.ca c.utf8;
            "UTF16be" >:: ck `UTF16be c.ca c.utf16be;
            "UTF16le" >:: ck `UTF16le c.ca c.utf16le;
            "UTF32be" >:: ck `UTF32be c.ca c.utf32be;
            "UTF32le" >:: ck `UTF32le c.ca c.utf32le;
        ]

    let test =
        "utf" >::: [
            of_case "v0" v0;
            of_case "abc" abc;
            of_case "x4" x4;
        ]
end

module T_norm: T_signature = struct
    module S = Cf_scan.ASCII
    module LS = Cf_lex_scan.ASCII
    open S.Affix
    open LS.Affix

    let s_comment =
        let* _ = S.one '#' in
        let+ _ = S.ign (function '\n' -> false | _ -> true) in
        '\n'

    let s_newline =
        let* _ = S.owsl in
        S.alt [ S.one '\n'; s_comment ]

    let s_code_point =
        ?$$ {|\s*[\dA-F]+|} >>: fun s ->
        Scanf.sscanf s " %x" Uchar.of_int

    let s_separator =
        let* _ = S.owsl in
        let* _ = ?. ';' in
        let+ _ = S.owsl in
        ()

    let s_column =
        let* cp, cps = ?+ s_code_point in
        let+ _ = s_separator in
        List.to_seq @@ cp :: cps

    let s_parthead = ?$$ {|\s*@Part|}
    let s_partnum = ?$$ {|\d+|}

    let s_partnumber =
        let* _ = s_parthead in
        let+ s = s_partnum in
        Scanf.sscanf s "%u" (fun n -> n)

    let idstr = function
        | `NFC -> "NFC"
        | `NFD -> "NFD"
        | `NFKC -> "NFKC"
        | `NFKD -> "NFKD"

    (*
    let showcps =
        let rec first b cps =
            match cps () with
            | Seq.Nil ->
                ()
            | Seq.Cons (cp, cps) ->
                Buffer.add_string b (Printf.sprintf "%04X" cp);
                loop b cps
        and loop b cps =
            match cps () with
            | Seq.Nil ->
                ()
            | Seq.Cons (cp, cps) ->
                Buffer.add_string b (Printf.sprintf "; %04X" cp);
                loop b cps
        in
        let enter cps =
            let b = Buffer.create 4 in
            Buffer.add_char b '[';
            Seq.map Uchar.to_int cps |> first b;
            Buffer.add_char b ']';
            Buffer.contents b
        in
        enter
    *)

    type c = C of {
        src: Uchar.t Seq.t;
        nfc: Uchar.t Seq.t;
        nfd: Uchar.t Seq.t;
        nfkc: Uchar.t Seq.t;
        nfkd: Uchar.t Seq.t;
    }

    let transform ~id (C c) =
        let module NF = (val (Ucs_normal.create id) : Ucs_normal.Profile) in
        let src = NF.transform c.src in
        let nfc = NF.transform c.nfc in
        let nfd = NF.transform c.nfd in
        let nfkc = NF.transform c.nfkc in
        let nfkd = NF.transform c.nfkd in
        let src = Cf_seq.persist src in
        let nfc = Cf_seq.persist nfc in
        let nfd = Cf_seq.persist nfd in
        let nfkc = Cf_seq.persist nfkc in
        let nfkd = Cf_seq.persist nfkd in
        (*
        let fname = Printf.sprintf "to%s" (idstr id) in
        let sa = showcps c.src and sb = showcps src in
        jout#info "transform: src %s %s -> %s" fname sa sb;
        let sa = showcps c.nfc and sb = showcps nfc in
        jout#info "transform: nfc %s %s -> %s" fname sa sb;
        let sa = showcps c.nfd and sb = showcps nfd in
        jout#info "transform: nfd %s %s -> %s" fname sa sb;
        let sa = showcps c.nfkc and sb = showcps nfkc in
        jout#info "transform: nfkc %s %s -> %s" fname sa sb;
        let sa = showcps c.nfkd and sb = showcps nfkd in
        jout#info "transform: nfkd %s %s -> %s" fname sa sb;
        *)
        C { src; nfc; nfd; nfkc; nfkd }

    let s_testcase =
        let* src = s_column in
        let* nfc = s_column in
        let* nfd = s_column in
        let* nfkc = s_column in
        let* nfkd = s_column in
        let* _ = s_newline in
        S.return @@ C { src; nfc; nfd; nfkc; nfkd }

    type p = P of {
        part: int;
        cases: c list;
    }

    let s_partition =
        let* _ = ?* s_newline in
        let* part = s_partnumber in
        let* _ = ?* s_newline in
        let* cases = ?* s_testcase in
        S.return @@ P { part; cases}

    let s_testfile =
        let* _ = S.return () in
        let* p = ?* s_partition in
        let+ _ = ?* s_newline in p

    type d = D of p list

    let normdata = ref (D [])

    let load_normalization_data ctxt =
        let D ps = !normdata in
        if ps = [] then begin
            let fname = opt_normtestdata ctxt in
            let ic = open_in fname in
            let s = Cf_seq.of_buffered_channel 1024 ic in
            match Cf_scan.ASCII.of_seq s_testfile s with
            | exception Not_found ->
                assert_failure "Cannot parse Normalization.txt"
            | parts ->
                close_in ic;
                normdata := D parts
        end

    let cmp = Cf_seq.eqf Uchar.equal

    let assert_nfequiv ~ctxt ~pfx ~nf a b =
        let msg = Printf.sprintf "%s to%s" pfx nf in
        assert_equal ~ctxt ~cmp ~msg a b

    let ck0 (C c) ctxt =
        let open Ucs_db_aux in
        let open Ucs_normal in
        let ck = NFC.boundary_check c.nfc <> QC_no in
        assert_equal ~ctxt ~msg:"isNFC" true ck;
        let ck = NFD.boundary_check c.nfd <> QC_no in
        assert_equal ~ctxt ~msg:"isNFD" true ck;
        let ck = NFKC.boundary_check c.nfkc <> QC_no in
        assert_equal ~ctxt ~msg:"isNFKC" true ck;
        let ck = NFKD.boundary_check c.nfkd <> QC_no in
        assert_equal ~ctxt ~msg:"isNFKD" true ck

    let ck1 id (C a as a0) ctxt =
        let C b = transform ~id a0 and nf = idstr id in
        match id with
        | `NFC ->
            assert_nfequiv ~ctxt ~pfx:"src" ~nf a.nfc b.src;
            assert_nfequiv ~ctxt ~pfx:"nfc" ~nf a.nfc b.nfc;
            assert_nfequiv ~ctxt ~pfx:"nfd" ~nf a.nfc b.nfd;
            assert_nfequiv ~ctxt ~pfx:"nfkc" ~nf a.nfkc b.nfkc;
            assert_nfequiv ~ctxt ~pfx:"nfkd" ~nf a.nfkc b.nfkd
        | `NFD ->
            assert_nfequiv ~ctxt ~pfx:"src" ~nf a.nfd b.src;
            assert_nfequiv ~ctxt ~pfx:"nfc" ~nf a.nfd b.nfc;
            assert_nfequiv ~ctxt ~pfx:"nfd" ~nf a.nfd b.nfd;
            assert_nfequiv ~ctxt ~pfx:"nfkc" ~nf a.nfkd b.nfkc;
            assert_nfequiv ~ctxt ~pfx:"nfkd" ~nf a.nfkd b.nfkd
        | `NFKC ->
            assert_nfequiv ~ctxt ~pfx:"src" ~nf a.nfkc b.src;
            assert_nfequiv ~ctxt ~pfx:"nfc" ~nf a.nfkc b.nfc;
            assert_nfequiv ~ctxt ~pfx:"nfd" ~nf a.nfkc b.nfd;
            assert_nfequiv ~ctxt ~pfx:"nfkc" ~nf a.nfkc b.nfkc;
            assert_nfequiv ~ctxt ~pfx:"nfkd" ~nf a.nfkc b.nfkd
        | `NFKD ->
            assert_nfequiv ~ctxt ~pfx:"src" ~nf a.nfkd b.src;
            assert_nfequiv ~ctxt ~pfx:"nfc" ~nf a.nfkd b.nfc;
            assert_nfequiv ~ctxt ~pfx:"nfd" ~nf a.nfkd b.nfd;
            assert_nfequiv ~ctxt ~pfx:"nfkc" ~nf a.nfkd b.nfkc;
            assert_nfequiv ~ctxt ~pfx:"nfkd" ~nf a.nfkd b.nfkd

    (*
    let ck2 cp ctxt =
        let cps = Seq.return cp in
        let nfd = Ucs_normal.NFD.transform cps in
        let nfc = Ucs_normal.NFC.transform cps in
        let nfkd = Ucs_normal.NFKD.transform cps in
        let nfkc = Ucs_normal.NFKC.transform cps in
        assert_nfequiv ~ctxt ~pfx:"is" ~nf:"NFC" cps nfc;
        assert_nfequiv ~ctxt ~pfx:"is" ~nf:"NFD" cps nfd;
        assert_nfequiv ~ctxt ~pfx:"is" ~nf:"NFKC" cps nfkc;
        assert_nfequiv ~ctxt ~pfx:"is" ~nf:"NFKD" cps nfkd

    let cp_to_buf b cp =
        let s = Printf.sprintf "%04X" (Uchar.to_int cp) in
        Buffer.add_string b s

    let cps_to_buf b cps =
        match cps () with
        | Seq.Nil ->
            assert (not true);
            Buffer.add_char b 'Z'
        | Seq.Cons (cp, cps) ->
            cp_to_buf b cp;
            let f cp =
                Buffer.add_char b ',';
                cp_to_buf b cp
            in
            Seq.iter f cps

    let c_desc (C c) =
        let b = Buffer.create 4 in
        cps_to_buf b c.src;
        Buffer.contents b

    let t_testcase c =
        c_desc c >:: begin fun ctxt ->
            ck0 c ctxt;
            ck1 `NFC c ctxt;
            ck1 `NFD c ctxt;
            ck1 `NFKC c ctxt;
            ck1 `NFKD c ctxt
        end

    let t_partition (P p) =
        let name = Printf.sprintf "part%u" p.part in
        let cases = List.map t_testcase p.cases in
        name >::: cases
    *)

    let ck2 id cp ctxt =
        let cps = Seq.return cp in
        let module NF = (val (Ucs_normal.create id) : Ucs_normal.Profile) in
        assert_nfequiv ~ctxt ~pfx:"is" ~nf:(idstr id) cps (NF.transform cps)

    let ck ctxt c =
        ck0 c ctxt;
        ck1 `NFC c ctxt;
        ck1 `NFD c ctxt;
        ck1 `NFKC c ctxt;
        ck1 `NFKD c ctxt

    let t_partition ctxt (P p) =
        skip_if (not (opt_exhaustive ctxt) && p.part > 0) "not exhaustive";
        List.iter (ck ctxt) p.cases

    let all_code_points =
        let f n =
            try
                let n = Uchar.succ n in
                Some (n, Uchar.to_int n)
            with
            | Invalid_argument _ -> None
        in
        Cf_seq.compose f Uchar.min

    module Int_set = Cf_rbtree.Set.Create(Cf_relations.Int)

    let listed_code_points ctxt =
        load_normalization_data ctxt;
        let D d = !normdata in
        let P p = List.nth d 1 in
        let put cps u = Int_set.put (Uchar.to_int u) cps in
        let f cps (C c) = Seq.fold_left put cps c.src in
        List.fold_left f Int_set.nil p.cases

    let unlisted_code_points ctxt =
        let listed = listed_code_points ctxt in
        let f cps cp =
            if Int_set.member cp listed then
                cps
            else
                Int_set.put cp cps
        in
        Seq.map Uchar.of_int @@
            Int_set.to_seq_incr @@
            Seq.fold_left f Int_set.nil all_code_points

    let unlisted_code_point_ranges ctxt =
        let rec loop n ua us () =
            match us () with
            | Seq.Cons (u, us) ->
                if n < 10000 then
                    (loop[@tailcall]) (succ n) (u :: ua) us ()
                else
                    (pack[@tailcall]) ua us
            | Seq.Nil ->
                if ua <> [] then
                    (pack[@tailcall]) ua Seq.empty
                else
                    Seq.Nil
        and pack ua us =
            let ur = Array.of_list @@ List.rev ua in
            Seq.Cons (ur, loop 0 [] us)
        in
        loop 0 [] @@ unlisted_code_points ctxt

    let t_unlist id =
        let nym = idstr id in
        let f ctxt ur =
            let f c = ck2 id c ctxt in
            Seq.iter f @@ Array.to_seq ur
        in
        nym >: test_case ~length:OUnitTest.Long begin fun ctxt ->
            skip_if (not (opt_exhaustive ctxt)) "not exhaustive";
            Seq.iter (f ctxt) @@ unlisted_code_point_ranges ctxt
        end

    let test =
        "norm" >::: [

            "list" >: test_case ~length:OUnitTest.Long begin fun ctxt ->
                load_normalization_data ctxt;
                let D parts = !normdata in
                List.iter (t_partition ctxt) parts
            end;

            "unlist" >::: [
                t_unlist `NFC;
                t_unlist `NFD;
                t_unlist `NFKC;
                t_unlist `NFKD
            ]
        ]
end

module T_text: T_signature = struct
    module L_of_string = struct
        let test ctxt =
            let s = "test vector" in
            let ut = Ucs_text.of_string s in
            let Ucs_text.Octets utf8 = ut in
            assert_equal ~ctxt ~cmp:String.equal ~msg:"equal" s utf8
    end

    let test = "text" >::: [
        "of_string" >:: L_of_string.test;
    ]
end


module T_horizon: T_signature = struct
    type bottom and 'a test = Test of 'a

    type _ Cf_type.nym +=
        | Container: 'a Cf_type.nym -> 'a test Cf_type.nym
        | Bottom: bottom Cf_type.nym

    let horizon = object(self)
        inherit Ucs_type.horizon as ucs

        method! equiv:
            type a b. a Cf_type.nym -> b Cf_type.nym -> (a, b) Cf_type.eq
                = fun a b ->
            match[@warning "-4"] a, b with
            | Container a, Container b ->
                let Cf_type.Eq = self#equiv a b in Cf_type.Eq
            | _ ->
                ucs#equiv a b
    end

    module Form = (val Cf_type.form horizon : Cf_type.Form)

    open Cf_type
    open Ucs_type
    open! Form

    type case = Case: {
        name: string;
        nym: 'v nym;
        equal: 'v -> 'v -> bool;
        value: 'v;
        typerrs: opaque list;
    } -> case

    let of_case (Case c) = c.name >:: begin fun ctxt ->
        let v = witness c.nym c.value in
        assert_bool "Cf_type.ck -> true" @@ ck c.nym v;
        assert_bool "Cf_type.ck -> false" @@ not @@ ck Bottom v;
        let _ =
            match opt c.nym v with
            | Some value ->
                assert_equal ~ctxt ~msg:"opt v -> Some" ~cmp:c.equal
                    c.value value
            | None ->
                assert_failure "opt v -> !Some"
        in
        let _ =
            match opt Bottom v with
            | Some _ -> assert_failure "opt v Bottom -> Some"
            | None -> ()
        in
        assert_equal ~ctxt ~msg:"req v = c.value" ~cmp:c.equal
            c.value (req c.nym v);
        let _ =
            match req Bottom v with
            | exception Type_error ->
                ()
            | exception x ->
                logf ctxt `Error "exn %s %s"
                    (Printexc.to_string x) (Printexc.get_backtrace ());
                assert_failure "req v Bottom -> exn?"
            | _ ->
                assert_failure "req v Bottom -> value!"
        in
        let f notv =
            assert_bool "ck notv -> true!" @@ not (ck c.nym notv)
        in
        List.iter f c.typerrs
    end

    let test = "horizon" >::: List.map of_case [
        Case {
            name = "text";
            nym = Text;
            equal = Ucs_text.equal;
            value = Ucs_text.of_string "text";
            typerrs = [];
        };

        Case {
            name = "contained-text";
            nym = Container Text;
            equal = (fun (Test a) (Test b) -> Ucs_text.equal a b);
            value = Test (Ucs_text.of_string "test");
            typerrs = [ Cf_type.witness (Cf_type.Seq String) Seq.empty ];
        };

        Case {
            name = "seq-text";
            nym = Cf_type.Seq Text;
            equal = Cf_seq.eqf Ucs_text.equal;
            value = Seq.return (Ucs_text.of_string "test");
            typerrs = [ Cf_type.witness (Cf_type.Seq String) Seq.empty ];
        };
    ]
end

module T_regx: T_signature = struct
    module R = Ucs_regx

    module Quote = struct
        let qlist = [
            "abc", "abc";
            {|(a*b+c?[\t +?]|d)|}, {|\(a\*b\+c\?\[\\t \+\?\]\|d\)|};
        ]

        let test1 (a, b) =
            a >:: fun ctxt ->
                let a = Ucs_text.of_string a and b = Ucs_text.of_string b in
                let a' = R.quote a and b' = R.unquote b in
                assert_equal ~ctxt ~cmp:Ucs_text.equal ~msg:"quote" a b';
                assert_equal ~ctxt ~cmp:Ucs_text.equal ~msg:"unquote" b a'

        let test = List.map test1 qlist
    end

    module Recognize = struct
        let xlist = [
            "a", [ "a" ], [ "x"; "ax"; "" ];
            {|\u0061|}, [ "a" ], [ ];
            {|\u2028|}, [ "\xE2\x80\xA8" ], [ ];
            "abc", [ "abc" ], [ "xxxx" ];
            {|\u{61 62 63}|}, [ "abc" ], [ ];
            "ab+c*d?e", [ "abe"; "abbcde" ], [ "ae"; "acde"; "xxxx" ];
            "a(bc)+d", [ "abcd"; "abcbcd" ], [ "abc"; "abcabc"; "abcdd" ];
            "(a|b)*abb", [ "abababb"; "ababbabb" ], [ "ababab"; "cde" ];
            "[abc]+", [ "abccabc" ], [ "abcdabcd" ];
            "[a-z -- aeiou]+", [ "bcd"; "xyz" ], [ "oeoeoe" ];
            "[[:alpha:]]", [ "a"; "A" ], [ "*" ];
            {|[\p{lower}]|}, [ "a" ], [ "*"; "A"; " " ];
            {|\p{upper}+|}, [ "ABC"; "Z" ], [ "abc"; ""; "2837498" ];
            "[[:ascii:]]", [ "a"; "A"; "*" ], [ "\x80" ];
            "[[:latin:]]+", [ "alfa"; "bravo"; "colibr\xC3\xAC" ],
                [ "^%@^&#%$&@" ];
            "[[:upper|lower:]]+", [ "Alfa"; "Bravo" ], [ "123"; "!@#" ];
            {|\p{ccc=0}+|}, [ "foo"; "bar" ], [ "" ];
        ]

        let ck ctxt s x e v =
            let fmt: ('a, 'b, 'c) format =
                match e with
                | true -> "recognition of \"%s\" expected with \"%s\""
                | false -> "recognition of \"%s\" not expected with \"%s\""
            in
            let Ucs_text.Octets v0 = v in
            let msg = Printf.sprintf fmt s v0 in
            assert_equal ~ctxt ~msg e (R.test x v)

        let test1 (s, yes, no) =
            s >:: fun ctxt ->
                let yes = List.rev_map Ucs_text.of_string yes |> List.rev in
                let no = List.rev_map Ucs_text.of_string no |> List.rev in
                let x = R.of_text @@ Ucs_text.of_string s in
                List.iter (ck ctxt s x true) yes;
                List.iter (ck ctxt s x false) no

        let test = List.map test1 xlist
    end

    let test = "regx" >::: [
        "quote" >::: Quote.test;
        "recognize" >::: Recognize.test;
    ]
end

let all = "t" >::: [
    T_utf.test;
    T_norm.test;
    T_text.test;
    T_horizon.test;
    T_regx.test;
]

let _ = run_test_tt_main all

(*--- End ---*)
