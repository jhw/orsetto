(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type t = Octets of string [@@ocaml.unboxed]

let equal (Octets a) (Octets b) = String.equal a b
let compare (Octets a) (Octets b) = String.compare a b

module UTF8 = Ucs_transport.UTF8
(* module NFC = Ucs_normal.NFC *)

module Unsafe = struct
    let of_string octets = Octets octets
    let of_slice octets = of_string (Cf_slice.to_string octets)
end

let invalid utf =
    let msg = Printf.sprintf "Ucs_text: invalid %s coding." utf in
    invalid_arg msg

(* let utf8 = (module Ucs_transport.UTF8 : Ucs_transport.Profile) *)
let nfc = (module Ucs_normal.NFC : Ucs_normal.Profile)

let nil = Octets ""

let of_seq s = Octets (UTF8.seq_to_string s)

let of_slice octets =
    if not (UTF8.is_valid_slice octets) then invalid UTF8.descript;
    Octets (Cf_slice.to_string octets)

let of_string octets =
    if not (UTF8.is_valid_string octets) then invalid UTF8.descript;
    Octets octets

let to_seq (Octets octets) = UTF8.seq_of_string octets

let length t = to_seq t |> Cf_seq.length

let sub t pos len = to_seq t |> Cf_seq.shift pos |> Cf_seq.limit len |> of_seq

let is_normalized ?(nf = nfc) t =
    let module NF = (val nf : Ucs_normal.Profile) in
    let s = to_seq t in
    match NF.boundary_check s with
    | Ucs_db_aux.QC_yes -> true
    | Ucs_db_aux.QC_no -> false
    | Ucs_db_aux.QC_maybe -> Cf_seq.eqf Uchar.equal s (NF.transform s)

let normalize ?(nf = nfc) t =
    let module NF = (val nf : Ucs_normal.Profile) in
    let s = to_seq t in
    match NF.boundary_check s with
    | Ucs_db_aux.QC_yes ->
        t
    | Ucs_db_aux.QC_no ->
        of_seq (NF.transform s)
    | Ucs_db_aux.QC_maybe ->
        let s' = NF.transform s in
        if Cf_seq.eqf Uchar.equal s s' then t else of_seq s'

let encode_scheme =
    let to_string ?utf (Octets s) =
        match utf with
        | None ->
            s
        | Some utf ->
            let module UTFX = (val utf : Ucs_transport.Profile) in
            UTF8.seq_of_string s |> UTFX.seq_to_string
    in
    let wr s b i = Bytes.blit_string s 0 b i (String.length s) in
    let enter ?utf () =
        let ck _ n t =
            let s = to_string ?utf t in
            Cf_encode.analyze n (String.length s) s
        in
        Cf_encode.scheme 0 ck wr
    in
    enter

let decode_scheme =
    let of_slice ?utf sl =
        match utf with
        | None ->
            of_slice sl
        | Some utf ->
            let module UTFX = (val utf : Ucs_transport.Profile) in
            UTFX.seq_of_slice sl |> of_seq
    in
    let enter ?utf sz =
        if sz < 0 then
            invalid_arg (__MODULE__ ^ ": non-negative integer required!");
        let ck _ (Cf_decode.Position i) n = Cf_decode.analyze n sz i
        and rd i b = Cf_slice.of_substring b i (i + sz) |> of_slice ?utf in
        Cf_decode.scheme sz ck rd
    in
    enter

(*--- End ---*)
