(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Unicode character set properties. *)

(** {6 Overview}

    This module provides an interface to the Unicode character set database.
*)

(** {6 Types} *)

(** An alias for the abstract type representing a map of all Unicode code
    points to the value of its corresponding property.
*)
type 'a map = 'a Ucs_ucdgen_aux.map

(** The property index type. The full Unicode character database is large, and
    the portion required by the Orsetto {b Ucs} library itself is small, so
    values of this type provide an abstraction of the relevant portion of the
    database available to the application.
*)
type 'a index

(** The extensible universal property type. *)
type utyp = ..

(** The core population of the extensible universal property type. *)
type utyp +=
    | Typ_bool of bool map * bool index
    | Typ_int of int map * int index
    | Typ_string of string map * string index
    | Typ_uchars of Uchar.t list option map * Uchar.t list option index

(** {6 Functions and Constants} *)

(** Use [create_index s] to compose an index from a sequence of pairs. *)
val create_index: (string * 'a) list -> 'a index

(** Use [query m c] to resolve the value property [m] for character [c]. *)
val query_map: 'a map -> Uchar.t -> 'a

(** Use [search_index idx nym] to query the index [idx] for the entry named by
    [nym]. Index keys are loosely matched.
*)
val search_index: 'a index -> string -> 'a option

(** Use [search_property idx nym] to query the property database index [idx]
    for the property named [nym]. Property names are loosely matched.
*)
val search_property: utyp index -> string -> utyp option

(** Use [require_property idx nym] to query the property database index [idx]
    for the property named [nym]. Property names are loosedly matched. Raises
    [Not_found] if no property named [nym] is indexed.
*)
val require_property: utyp index -> string -> utyp

(** Unicode code block *)
type blk = [
    | `ASCII
    | `Adlam
    | `Aegean_Numbers
    | `Ahom
    | `Alchemical
    | `Alphabetic_PF
    | `Anatolian_Hieroglyphs
    | `Ancient_Greek_Music
    | `Ancient_Greek_Numbers
    | `Ancient_Symbols
    | `Arabic
    | `Arabic_Ext_A
    | `Arabic_Ext_B
    | `Arabic_Ext_C
    | `Arabic_Math
    | `Arabic_PF_A
    | `Arabic_PF_B
    | `Arabic_Sup
    | `Armenian
    | `Arrows
    | `Avestan
    | `Balinese
    | `Bamum
    | `Bamum_Sup
    | `Bassa_Vah
    | `Batak
    | `Bengali
    | `Bhaiksuki
    | `Block_Elements
    | `Bopomofo
    | `Bopomofo_Ext
    | `Box_Drawing
    | `Brahmi
    | `Braille
    | `Buginese
    | `Buhid
    | `Byzantine_Music
    | `CJK
    | `CJK_Compat
    | `CJK_Compat_Forms
    | `CJK_Compat_Ideographs
    | `CJK_Compat_Ideographs_Sup
    | `CJK_Ext_A
    | `CJK_Ext_B
    | `CJK_Ext_C
    | `CJK_Ext_D
    | `CJK_Ext_E
    | `CJK_Ext_F
    | `CJK_Ext_G
    | `CJK_Ext_H
    | `CJK_Ext_I
    | `CJK_Radicals_Sup
    | `CJK_Strokes
    | `CJK_Symbols
    | `Carian
    | `Caucasian_Albanian
    | `Chakma
    | `Cham
    | `Cherokee
    | `Cherokee_Sup
    | `Chess_Symbols
    | `Chorasmian
    | `Compat_Jamo
    | `Control_Pictures
    | `Coptic
    | `Coptic_Epact_Numbers
    | `Counting_Rod
    | `Cuneiform
    | `Cuneiform_Numbers
    | `Currency_Symbols
    | `Cypriot_Syllabary
    | `Cypro_Minoan
    | `Cyrillic
    | `Cyrillic_Ext_A
    | `Cyrillic_Ext_B
    | `Cyrillic_Ext_C
    | `Cyrillic_Ext_D
    | `Cyrillic_Sup
    | `Deseret
    | `Devanagari
    | `Devanagari_Ext
    | `Devanagari_Ext_A
    | `Diacriticals
    | `Diacriticals_Ext
    | `Diacriticals_For_Symbols
    | `Diacriticals_Sup
    | `Dingbats
    | `Dives_Akuru
    | `Dogra
    | `Domino
    | `Duployan
    | `Early_Dynastic_Cuneiform
    | `Egyptian_Hieroglyphs
    | `Egyptian_Hieroglyph_Format_Controls
    | `Elbasan
    | `Elymaic
    | `Emoticons
    | `Enclosed_Alphanum
    | `Enclosed_Alphanum_Sup
    | `Enclosed_CJK
    | `Enclosed_Ideographic_Sup
    | `Ethiopic
    | `Ethiopic_Ext
    | `Ethiopic_Ext_A
    | `Ethiopic_Ext_B
    | `Ethiopic_Sup
    | `Geometric_Shapes
    | `Geometric_Shapes_Ext
    | `Georgian
    | `Georgian_Ext
    | `Georgian_Sup
    | `Glagolitic
    | `Glagolitic_Sup
    | `Gothic
    | `Grantha
    | `Greek
    | `Greek_Ext
    | `Gujarati
    | `Gunjala_Gondi
    | `Gurmukhi
    | `Half_And_Full_Forms
    | `Half_Marks
    | `Hangul
    | `Hanifi_Rohingya
    | `Hanunoo
    | `Hatran
    | `Hebrew
    | `High_PU_Surrogates
    | `High_Surrogates
    | `Hiragana
    | `IDC
    | `IPA_Ext
    | `Ideographic_Symbols
    | `Imperial_Aramaic
    | `Indic_Number_Forms
    | `Indic_Siyaq_Numbers
    | `Inscriptional_Pahlavi
    | `Inscriptional_Parthian
    | `Jamo
    | `Jamo_Ext_A
    | `Jamo_Ext_B
    | `Javanese
    | `Kaithi
    | `Kaktovik_Numerals
    | `Kana_Ext_A
    | `Kana_Ext_B
    | `Kana_Sup
    | `Kanbun
    | `Kangxi
    | `Kannada
    | `Katakana
    | `Katakana_Ext
    | `Kayah_Li
    | `Kawi
    | `Kharoshthi
    | `Khitan_Small_Script
    | `Khmer
    | `Khmer_Symbols
    | `Khojki
    | `Khudawadi
    | `Lao
    | `Latin_1_Sup
    | `Latin_Ext_A
    | `Latin_Ext_Additional
    | `Latin_Ext_B
    | `Latin_Ext_C
    | `Latin_Ext_D
    | `Latin_Ext_E
    | `Latin_Ext_F
    | `Latin_Ext_G
    | `Lepcha
    | `Letterlike_Symbols
    | `Limbu
    | `Linear_A
    | `Linear_B_Ideograms
    | `Linear_B_Syllabary
    | `Lisu
    | `Lisu_Sup
    | `Low_Surrogates
    | `Lycian
    | `Lydian
    | `Mahajani
    | `Mahjong
    | `Makasar
    | `Malayalam
    | `Mandaic
    | `Manichaean
    | `Marchen
    | `Masaram_Gondi
    | `Math_Alphanum
    | `Math_Operators
    | `Mayan_Numerals
    | `Medefaidrin
    | `Meetei_Mayek
    | `Meetei_Mayek_Ext
    | `Mende_Kikakui
    | `Meroitic_Cursive
    | `Meroitic_Hieroglyphs
    | `Miao
    | `Misc_Arrows
    | `Misc_Math_Symbols_A
    | `Misc_Math_Symbols_B
    | `Misc_Pictographs
    | `Misc_Symbols
    | `Misc_Technical
    | `Modi
    | `Modifier_Letters
    | `Modifier_Tone_Letters
    | `Mongolian
    | `Mongolian_Sup
    | `Mro
    | `Multani
    | `Music
    | `Myanmar
    | `Myanmar_Ext_A
    | `Myanmar_Ext_B
    | `NB
    | `NKo
    | `Nabataean
    | `Nag_Mundari
    | `Nandinagari
    | `New_Tai_Lue
    | `Newa
    | `No_Block_Assigned
    | `Number_Forms
    | `Nushu
    | `Nyiakeng_Puachue_Hmong
    | `OCR
    | `Ogham
    | `Ol_Chiki
    | `Old_Hungarian
    | `Old_Italic
    | `Old_North_Arabian
    | `Old_Permic
    | `Old_Persian
    | `Old_Sogdian
    | `Old_South_Arabian
    | `Old_Turkic
    | `Old_Uyghur
    | `Oriya
    | `Ornamental_Dingbats
    | `Osage
    | `Osmanya
    | `Ottoman_Siyaq_Numbers
    | `PUA
    | `Pahawh_Hmong
    | `Palmyrene
    | `Pau_Cin_Hau
    | `Phags_Pa
    | `Phaistos
    | `Phoenician
    | `Phonetic_Ext
    | `Phonetic_Ext_Sup
    | `Playing_Cards
    | `Psalter_Pahlavi
    | `Punctuation
    | `Rejang
    | `Rumi
    | `Runic
    | `Samaritan
    | `Saurashtra
    | `Sharada
    | `Shavian
    | `Shorthand_Format_Controls
    | `Siddham
    | `Sinhala
    | `Sinhala_Archaic_Numbers
    | `Small_Forms
    | `Small_Kana_Ext
    | `Sogdian
    | `Sora_Sompeng
    | `Soyombo
    | `Specials
    | `Sundanese
    | `Sundanese_Sup
    | `Sup_Arrows_A
    | `Sup_Arrows_B
    | `Sup_Arrows_C
    | `Sup_Math_Operators
    | `Sup_PUA_A
    | `Sup_PUA_B
    | `Sup_Punctuation
    | `Sup_Symbols_And_Pictographs
    | `Super_And_Sub
    | `Sutton_SignWriting
    | `Syloti_Nagri
    | `Symbols_And_Pictographs_Ext_A
    | `Symbols_For_Legacy_Computing
    | `Syriac
    | `Syriac_Sup
    | `Tagalog
    | `Tagbanwa
    | `Tags
    | `Tai_Le
    | `Tai_Tham
    | `Tai_Viet
    | `Tai_Xuan_Jing
    | `Takri
    | `Tamil
    | `Tamil_Sup
    | `Tangsa
    | `Tangut
    | `Tangut_Components
    | `Tangut_Sup
    | `Telugu
    | `Thaana
    | `Thai
    | `Tibetan
    | `Tifinagh
    | `Tirhuta
    | `Toto
    | `Transport_And_Map
    | `UCAS
    | `UCAS_Ext
    | `UCAS_Ext_A
    | `Ugaritic
    | `VS
    | `VS_Sup
    | `Vai
    | `Vedic_Ext
    | `Vertical_Forms
    | `Vithkuqi
    | `Wancho
    | `Warang_Citi
    | `Yezidi
    | `Yi_Radicals
    | `Yi_Syllables
    | `Yijing
    | `Zanabazar_Square
    | `Znamenny_Music
]

(** Equality *)
val equal_blk: blk -> blk -> bool

(** String representation *)
val show_blk: blk -> string

(** Extend the universal type *)
type utyp +=
    | Typ_block of blk map * blk index

(** The general category property value type. *)
type gc = [
    | `C
    | `Cc
    | `Cf
    | `Cs
    | `Co
    | `Cn
    | `L
    | `LC
    | `Lu
    | `Ll
    | `Lt
    | `Lm
    | `Lo
    | `M
    | `Mn
    | `Mc
    | `Me
    | `N
    | `Nd
    | `Nl
    | `No
    | `P
    | `Pc
    | `Pd
    | `Ps
    | `Pe
    | `Pi
    | `Pf
    | `Po
    | `S
    | `Sm
    | `Sc
    | `Sk
    | `So
    | `Z
    | `Zs
    | `Zl
    | `Zp
]

(** Equality *)
val equal_gc: gc -> gc -> bool

(** String representation *)
val show_gc: gc -> string

type utyp +=
    | Typ_general_category of gc map * gc index

(** The normalization quick check property type. *)
type qc = QC_yes | QC_no | QC_maybe

(** Equality *)
val equal_qc: qc -> qc -> bool

(** String representation *)
val show_qc: qc -> string

(** Extension of the universal type *)
type utyp +=
    | Typ_quick_check of qc map * qc index

(** Unicode script identifier *)
type script = [
    | `Adlm
    | `Aghb
    | `Ahom
    | `Arab
    | `Armi
    | `Armn
    | `Avst
    | `Bali
    | `Bamu
    | `Bass
    | `Batk
    | `Beng
    | `Bhks
    | `Bopo
    | `Brah
    | `Brai
    | `Bugi
    | `Buhd
    | `Cakm
    | `Cans
    | `Cari
    | `Cham
    | `Cher
    | `Chrs
    | `Copt
    | `Cpmn
    | `Cprt
    | `Cyrl
    | `Deva
    | `Diak
    | `Dogr
    | `Dsrt
    | `Dupl
    | `Egyp
    | `Elba
    | `Elym
    | `Ethi
    | `Geor
    | `Glag
    | `Gong
    | `Gonm
    | `Goth
    | `Gran
    | `Grek
    | `Gujr
    | `Guru
    | `Hang
    | `Hani
    | `Hano
    | `Hatr
    | `Hebr
    | `Hira
    | `Hluw
    | `Hmng
    | `Hmnp
    | `Hrkt
    | `Hung
    | `Ital
    | `Java
    | `Kali
    | `Kana
    | `Kawi
    | `Khar
    | `Khmr
    | `Khoj
    | `Kits
    | `Knda
    | `Kthi
    | `Lana
    | `Laoo
    | `Latn
    | `Lepc
    | `Limb
    | `Lina
    | `Linb
    | `Lisu
    | `Lyci
    | `Lydi
    | `Mahj
    | `Maka
    | `Mand
    | `Mani
    | `Marc
    | `Medf
    | `Mend
    | `Merc
    | `Mero
    | `Mlym
    | `Modi
    | `Mong
    | `Mroo
    | `Mtei
    | `Mult
    | `Mymr
    | `Nagm
    | `Nand
    | `Narb
    | `Nbat
    | `Newa
    | `Nkoo
    | `Nshu
    | `Ogam
    | `Olck
    | `Orkh
    | `Orya
    | `Osge
    | `Osma
    | `Ougr
    | `Palm
    | `Pauc
    | `Perm
    | `Phag
    | `Phli
    | `Phlp
    | `Phnx
    | `Plrd
    | `Prti
    | `Qaai
    | `Rjng
    | `Rohg
    | `Runr
    | `Samr
    | `Sarb
    | `Saur
    | `Sgnw
    | `Shaw
    | `Shrd
    | `Sidd
    | `Sind
    | `Sinh
    | `Sogd
    | `Sogo
    | `Sora
    | `Soyo
    | `Sund
    | `Sylo
    | `Syrc
    | `Tagb
    | `Takr
    | `Tale
    | `Talu
    | `Taml
    | `Tang
    | `Tavt
    | `Telu
    | `Tfng
    | `Tglg
    | `Thaa
    | `Thai
    | `Tibt
    | `Tirh
    | `Tnsa
    | `Toto
    | `Ugar
    | `Vaii
    | `Vith
    | `Wara
    | `Wcho
    | `Xpeo
    | `Xsux
    | `Yezi
    | `Yiii
    | `Zanb
    | `Zinh
    | `Zyyy
    | `Zzzz
]

(** Equality *)
val equal_script: script -> script -> bool

(** String representation *)
val show_script: script -> string

(** Extend the universal type. *)
type utyp +=
    | Typ_script of script map * script index

(** This module contains internal fast-path functions for property query. *)
module Quick: sig
    val canonical_combining_class: int -> int option
    val quick_check_nfc: int -> qc option
    val quick_check_nfd: int -> qc option
    val quick_check_nfkc: int -> qc option
    val quick_check_nfkd: int -> qc option
    val block: int -> blk option
    val grapheme_base: int -> bool option
end

(*--- End ---*)
