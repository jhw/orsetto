(*---------------------------------------------------------------------------*
  Copyright (C) 2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

val is_hangul_syllable: int -> bool
val min_combining_code: int
val min_volatile_nfc_code: int
val min_volatile_nfd_code: int
val min_volatile_nfkc_code: int
val min_volatile_nfkd_code: int

module D_set = Cf_bsearch_data.Set.Of_int
module D_map = Cf_bsearch_data.Map.Of_int
module R_core = Cf_disjoint_interval.Core.Of_int
module R_set = Cf_disjoint_interval.Set.Of_int
module R_map = Cf_disjoint_interval.Map.Of_int
module Nym_map = Cf_bsearch_data.Map.Of_string

type 'a tbl =
    | Tbl_dset: D_set.t -> bool tbl
    | Tbl_rset: R_set.t -> bool tbl
    | Tbl_dmap: 'a D_map.t -> 'a tbl
    | Tbl_rmap: 'a R_map.t -> 'a tbl

type 'a map = Map of { q: int -> 'a option; v0: 'a; tbl: 'a tbl Lazy.t; }

val query_map: 'a map -> Uchar.t -> 'a
val qnil: 'a -> 'b option
val tbl_dbool: string -> bool tbl
val tbl_rbool: string -> bool tbl
val tbl_duniv: string -> 'a tbl
val tbl_runiv: string -> 'a tbl

val of_exported_dbool:
    ?v0:bool -> ?q:(int -> bool option) -> string -> bool map

val of_exported_rbool:
    ?v0:bool -> ?q:(int -> bool option) -> string -> bool map

val of_exported_duniv: v0:'a -> ?q:(int -> 'a option) -> string -> 'a map
val of_exported_runiv: v0:'a -> ?q:(int -> 'a option) -> string -> 'a map
val all_uchars: int Seq.t

val of_filter:
    ?v0:bool -> ?q:(int -> bool option) ->
    ?t:[< `D | `R > `D ] -> (Int.t -> bool) -> bool map

val of_optmap:
    v0:'a -> ?q:(int -> 'a option) -> ?eq:('a -> 'a -> bool) ->
    (int -> (Cf_bsearch.Int_basis.t * 'a) option) -> 'a map

val cut_prefix: string -> string
val search_query_nym: String.t -> string
val search_property_nym: String.t -> string

(*--- End ---*)
