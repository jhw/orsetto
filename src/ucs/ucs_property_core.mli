(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Unicode character set properties. *)

(** {6 Overview}

    This module provides an interface to the Unicode character set database.
*)

(** {6 Open Modules} *)
open Ucs_db_aux

(** {6 Interface} *)

(** The canonical combining class property. *)
val canonical_combining_class: int map

(** The grapheme base property. *)
val grapheme_base: bool map

(** The identifier continuation property. *)
val id_continue: bool map

(** The identifier start property. *)
val id_start: bool map

(** The NFC quick check property. *)
val quick_check_nfc: qc map

(** The NFD quick check property. *)
val quick_check_nfd: qc map

(** The NFKC quick check property. *)
val quick_check_nfkc: qc map

(** The NFKD quick check property. *)
val quick_check_nfkd: qc map

(** The white space property. *)
val white_space: bool map

(*--- UNUSED ---*

(** The alphabetic property. *)
val alphabetic: bool map

(** The ASCII hexadecimal digit property. *)
val ascii_hex_digit: bool map

(** The bidirectional control property. *)
val bidi_control: bool map

(** The bidirection mirrored property. *)
val bidi_mirrored: bool map

(** The cased property. *)
val cased: bool map

(** The case ignorable property. *)
val case_ignorable: bool map

(** The changes when case-folded property. *)
val changes_when_casefolded: bool map

(** The changes when case-mapped property. *)
val changes_when_casemapped: bool map

(** The changes when NKFC case-folded property. *)
val changes_when_nfkc_casefolded: bool map

(** The changes when title-cased property. *)
val changes_when_titlecased: bool map

(** The changes when upper-cased property. *)
val changes_when_uppercased: bool map

(** The composition exclusion property. *)
val composition_exclusion: bool map

(** The dash property. *)
val dash: bool map

(** The default ignorable code point property. *)
val default_ignorable_code_point: bool map

(** The deprecated code point property. *)
val deprecated: bool map

(** The diacritical mark property. *)
val diacritic: bool map

(** The expands on NFC property. *)
val expands_on_nfc: bool map

(** The expands on NFD property. *)
val expands_on_nfd: bool map

(** The expands on NKFC property. *)
val expands_on_nfkc: bool map

(** The expands on NKFD property. *)
val expands_on_nfkd: bool map

(** The extended property. *)
val extender: bool map

(** The full composition exclusion property. *)
val full_composition_exclusion: bool map

(** The grapheme extend property. *)
val grapheme_extend: bool map

(** The grapheme link property. *)
val grapheme_link: bool map

(** The hexadecimal digit property. *)
val hex_digit: bool map

(** The hyphen property. *)
val hyphen: bool map

(** The ideographic property. *)
val ideographic: bool map

(** The identifiers binary operator property. *)
val ids_binary_operator: bool map

(** The identifiers trinary operator property. *)
val ids_trinary_operator: bool map

(** The join control property. *)
val join_control: bool map

(** The logical order exception property. *)
val logical_order_exception: bool map

(** The lower-case property. *)
val lowercase: bool map

(** The math symbol property. *)
val math: bool map

(** The non-character code point property. *)
val noncharacter_code_point: bool map

(** The other alphabetic property. *)
val other_alphabetic: bool map

(** The other default ignorable code point property. *)
val other_default_ignorable_code_point: bool map

(** The other grapheme extend property. *)
val other_grapheme_extend: bool map

(** The other identifier continuation property. *)
val other_id_continue: bool map

(** The other identifier start property. *)
val other_id_start: bool map

(** The other lower-case property. *)
val other_lowercase: bool map

(** The other math symbol property. *)
val other_math: bool map

(** The other upper-case property. *)
val other_uppercase: bool map

(** The pattern syntax property. *)
val pattern_syntax: bool map

(** The patter white space property. *)
val pattern_white_space: bool map

(** The prepended concatenation mark property. *)
val prepended_concatenation_mark: bool map

(** The quotation mark property. *)
val quotation_mark: bool map

(** The radical property. *)
val radical: bool map

(** The regional indicator property. *)
val regional_indicator: bool map

(** The soft dotted property. *)
val soft_dotted: bool map

(** The s-term property. *)
val sterm: bool map

(** The terminal punctuation property. *)
val terminal_punctuation: bool map

(** The unified ideograph property. *)
val unified_ideograph: bool map

(** The upper-case property. *)
val uppercase: bool map

(** The variation selector property. *)
val variation_selector: bool map

(** The XID continuation property. *)
val xid_continue: bool map

(** The XID start property. *)
val xid_start: bool map
*)

(*--- End ---*)
