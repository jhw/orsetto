(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Db = Ucs_db_core
module Aux = Ucs_ucdgen_aux
module Core = Ucs_property_core

let not_ucs_code = (-1)

(*
let cp_quick_check_nfc = Aux.query_map Core.quick_check_nfc
let cp_quick_check_nfd = Aux.query_map Core.quick_check_nfd
let cp_quick_check_nfkc = Aux.query_map Core.quick_check_nfkc
let cp_quick_check_nfkd = Aux.query_map Core.quick_check_nfkd
*)

module Hangul = struct
    type r = {
        base: int;
        count: int;
    }

    let lr = { base = 0x1100; count = 19 }
    let vr = { base = 0x1161; count = 21 }
    let tr = { base = 0x11A7; count = 28 }
    let sr = { base = 0xAC00; count = 11172 }

    let ln = 588

    let is_syllable = Aux.is_hangul_syllable

    let is_lform cp =
        cp >= lr.base && cp < lr.base + lr.count

    let is_vform cp =
        cp >= vr.base && cp < vr.base + vr.count

    let is_tform cp =
        cp >= tr.base && cp < tr.base + tr.count

    let is_lvform cp =
        is_syllable cp && (cp - sr.base) mod tr.count = 0

    let compose cp1 cp2 =
        if is_lform cp1 then begin
            if is_vform cp2 then begin
                let cpl = cp1 - lr.base and cpv = cp2 - vr.base in
                sr.base + (cpl * ln) + (cpv * tr.count)
            end
            else
                not_ucs_code
        end
        else if is_lvform cp1 then begin
            if is_tform cp2 then
                cp1 + cp2 - tr.base
            else
                not_ucs_code
        end
        else
            not_ucs_code

    let decompose cp =
        let i = cp - sr.base in
        let lc = lr.base + (i / ln) in
        let vc = vr.base + (i mod ln) / tr.count in
        let tc = tr.base + (i mod tr.count) in
        let q = Cf_deque.one lc in
        if tc = tr.base then
            Cf_deque.A.push vc q
        else
            Cf_deque.A.push vc q |>
            Cf_deque.A.push tc
end

let cp_decompose_canonical cp =
    if cp < Aux.min_volatile_nfd_code then
        Cf_deque.nil
    else if Hangul.is_syllable cp then
        Hangul.decompose cp
    else
        match Uchar.of_int cp |> Aux.query_map Db.canonical_decomposition with
        | [||] -> Cf_deque.nil
        | cpa -> Array.fold_right Cf_deque.B.push cpa Cf_deque.nil

let cp_decompose_compatibility cp =
    if cp < Aux.min_volatile_nfkd_code then
        Cf_deque.nil
    else if Hangul.is_syllable cp then
        Hangul.decompose cp
    else
        let uc = Uchar.of_int cp in
        match Aux.query_map Db.compatibility_decomposition uc with
        | [||] -> begin
            match Aux.query_map Db.canonical_decomposition uc with
            | [||] -> Cf_deque.nil
            | cpa -> Array.fold_right Cf_deque.B.push cpa Cf_deque.nil
          end
        | cpa ->
            Array.fold_right Cf_deque.B.push cpa Cf_deque.nil

let cp_compose_pair cp1 cp2 =
    if cp2 < Aux.min_combining_code then
        not_ucs_code
    else
        let cp = Hangul.compose cp1 cp2 in
        if cp <> not_ucs_code then
            cp
        else
            match Aux.D_map.search cp1 Db.composition_map with
            | None ->
                not_ucs_code
            | Some cca ->
                let n = Array.length cca / 2 in
                let i = ref 0 and r = ref not_ucs_code in
                while !i < n do
                    let cki = !i * 2 in
                    let rti = succ cki in
                    if cp2 = Array.unsafe_get cca cki then begin
                        r := Array.unsafe_get cca rti;
                        i := n
                    end
                    else
                        incr i
                done;
                !r

let ccc_query = Aux.query_map Core.canonical_combining_class

module Int_map = Cf_rbtree.Map.Create(Cf_relations.Int)

let ccm_push ccc cp ccm =
    let cpq =
        match Int_map.search ccc ccm with
        | None -> Cf_deque.one cp
        | Some cpq -> Cf_deque.A.push cp cpq
    in
    Int_map.replace (ccc, cpq) ccm

let ccm_to_seq ccm =
    Int_map.to_seq_incr ccm |>
    Seq.map (fun (_, cpq) -> Cf_deque.B.to_seq cpq) |>
    Cf_seq.join

let cp_compose_noop _ _ = not_ucs_code

module type Basis = sig
    open Ucs_db_aux
    val quick_check: qc map
    val decompose: int -> int Cf_deque.t
    val compose_pair: int -> int -> int
end

module NFC_basis = struct
    let quick_check = Core.quick_check_nfc
    let decompose = cp_decompose_canonical
    let compose_pair = cp_compose_pair
end

module NFD_basis = struct
    let quick_check = Core.quick_check_nfd
    let decompose = cp_decompose_canonical
    let compose_pair = cp_compose_noop
end

module NFKC_basis = struct
    let quick_check = Core.quick_check_nfkc
    let decompose = cp_decompose_compatibility
    let compose_pair = cp_compose_pair
end

module NFKD_basis = struct
    let quick_check = Core.quick_check_nfkd
    let decompose = cp_decompose_compatibility
    let compose_pair = cp_compose_noop
end

module type Profile = sig
    open Ucs_db_aux
    val quick_check: qc map
    val boundary_check: Uchar.t Seq.t -> qc
    val transform: Uchar.t Seq.t -> Uchar.t Seq.t
end

module Create(B: Basis) = struct
    open Ucs_db_aux

    let quick_check = B.quick_check

    let boundary_check =
        let rec loop ret ccc' us =
            match us () with
            | Seq.Nil ->
                ret
            | Seq.Cons (u, us) ->
                let ccc = ccc_query u in
                if ccc > 0 && ccc < ccc' then
                    QC_no
                else
                    match query_map quick_check u with
                    | QC_no -> QC_no
                    | QC_yes -> (loop[@tailcall]) ret ccc us
                    | QC_maybe -> (loop[@tailcall]) QC_maybe ccc us
        in
        loop QC_yes 0

    let decompose =
        let rec loop us () =
            match us () with
            | Seq.Nil ->
                Seq.Nil
            | Seq.Cons (u, us) ->
                let cp = Uchar.to_int u in
                let cpq = B.decompose cp in
                if Cf_deque.empty cpq then
                    Seq.Cons (cp, loop us)
                else
                    (drain[@tailcall]) us cpq ()
        and drain us cpq () =
            match Cf_deque.B.pop cpq with
            | None ->
                (loop[@tailvall]) us ()
            | Some (cp, cpq) ->
                let nq = B.decompose cp in
                if Cf_deque.empty nq then
                    Seq.Cons (cp, drain us cpq)
                else
                    (drain[@tailcall]) us (Cf_deque.catenate cpq nq) ()
        in
        loop

    let reorder =
        let push cp0 ccm cxs =
            let cx = cp0, ccm_to_seq ccm in
            Seq.Cons (cx, cxs)
        in
        let rec loop cp0 ccm cps () =
            match cps () with
            | Seq.Nil ->
                push cp0 ccm Seq.empty
            | Seq.Cons (cp1, cps) ->
                assert (Uchar.is_valid cp1);
                let ccc = ccc_query (Uchar.unsafe_of_int cp1) in
                if ccc > 0 then
                    (loop[@tailcall]) cp0 (ccm_push ccc cp1 ccm) cps ()
                else if cp0 = not_ucs_code && Int_map.empty ccm then
                    (loop[@tailcall]) cp1 Int_map.nil cps ()
                else
                    push cp0 ccm (loop cp1 Int_map.nil cps)
        in
        let enter cps () = (loop[@tailcall]) not_ucs_code Int_map.nil cps () in
        enter

    let combine =
        let rec enter cxs () =
            match cxs () with
            | Seq.Nil ->
                Seq.Nil
            | Seq.Cons (cx, cxs) ->
                let cp0, cps = cx in
                (step[@tailcall]) 0 [] cp0 cps cxs ()
        and step ccc cpa cp0 cps cxs () =
            match cps () with
            | Seq.Nil ->
                if cpa <> [] then begin
                    let cpa = List.rev cpa in
                    Seq.Cons (Uchar.unsafe_of_int cp0, drain cpa cxs)
                end
                else
                    (next[@tailcall]) cp0 cxs ()
            | Seq.Cons (cp1, cps) ->
                assert (Uchar.is_valid cp1);
                let ccc' = ccc_query (Uchar.unsafe_of_int cp1) in
                if cp0 = not_ucs_code then
                    (step[@tailcall]) ccc' [] cp1 cps cxs ()
                else if ccc' > ccc then begin
                    let cpx = B.compose_pair cp0 cp1 in
                    if cpx <> not_ucs_code then
                        (step[@tailcall]) ccc cpa cpx cps cxs ()
                    else
                        (step[@tailcall]) ccc' (cp1 :: cpa) cp0 cps cxs ()
                end
                else
                    (step[@tailcall]) ccc' (cp1 :: cpa) cp0 cps cxs ()
        and drain cpa cxs () =
            match cpa with
            | cp :: cpa -> Seq.Cons (Uchar.unsafe_of_int cp, drain cpa cxs)
            | [] -> (enter[@tailcall]) cxs ()
        and next cp0 cxs () =
            match cxs () with
            | Seq.Nil ->
                Seq.Cons (Uchar.unsafe_of_int cp0, Seq.empty)
            | Seq.Cons (cx, cxs) ->
                let cp1, cps = cx in
                let cpx = B.compose_pair cp0 cp1 in
                if cpx <> not_ucs_code then
                    (step[@tailcall]) 0 [] cpx cps cxs ()
                else
                    Seq.Cons (Uchar.unsafe_of_int cp0, step 0 [] cp1 cps cxs)
        in
        enter

    let transform us = decompose us |> reorder |> combine
end

module NFC = Create(NFC_basis)
module NFD = Create(NFD_basis)
module NFKC = Create(NFKC_basis)
module NFKD = Create(NFKD_basis)

type id = [ `NFC | `NFD | `NFKC | `NFKD ]

let create = function
    | `NFC -> (module NFC : Profile)
    | `NFD -> (module NFD : Profile)
    | `NFKC -> (module NFKC : Profile)
    | `NFKD -> (module NFKD : Profile)

(*--- End ---*)
