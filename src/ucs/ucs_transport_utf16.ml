(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Ucs_transport_aux.Private

module Aux(N: Cf_endian.Unsafe.Profile) = struct
    module D = Cf_decode
    (* module E = Cf_encode *)

    let descript = Printf.sprintf "UTF-16%s" N.descript

    let size_of_uchar c =
        let c = Uchar.to_int c in
        if c > 0xffff then 4 else 2

    let size_of_codeunit = 2

    let malformed p = decode_invalid p "invalid UTF-16 form"
    let bad_code_point p = decode_invalid p "invalid code point"

    let decode_ck s (D.Position p as p0) n =
        let ca = N.ldu16 s p in
        if ca < 0xd800 || ca >= 0xe000 then
            D.analyze n 2 ca
        else if ca >= 0xdc00 then
            malformed p0
        else begin
            let analyze = D.analyze n 4 in
            let cb = N.ldu16 s (p + 2) in
            if cb < 0xdc00 || cb >= 0xe000 then malformed p0;
            let ca = ca land 0x3ff and cb = cb land 0x3ff in
            let cp = 0x10000 + ((ca lsl 10) lor cb) in
            if not (Uchar.is_valid cp) then bad_code_point p0;
            analyze cp
        end

    let encode_wr cp s i =
        if cp > 0xffff then begin
            let cp' = cp - 0x10000 in
            let a = 0xd800 lor (cp' lsr 10) in
            let b = 0xdc00 lor (cp' land 0x3ff) in
            N.stu16 a s i;
            N.stu16 b s (i + 2)
        end
        else begin
            N.stu16 cp s i
        end
end

module BE = Create(Aux(Cf_endian.Unsafe.BE))
module LE = Create(Aux(Cf_endian.Unsafe.LE))
module SE = Create(Aux(Cf_endian.Unsafe.SE))

let of_endian = function
    | `BE -> (module BE : Ucs_transport_aux.Profile)
    | `LE -> (module LE : Ucs_transport_aux.Profile)
    | `SE -> (module SE : Ucs_transport_aux.Profile)

(*--- End ---*)
