(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Regular expression parsing, search and matching with Unicode text. *)

(** {6 Overview}

    This module implements Unicode regular expression parsing, search and
    matching in pure Objective Caml. Implementation claims support for
    Requirements Level 1 (Basic Unicode Support) with the following exceptions:

    + No support for line boundaries.
    + No support for word boundaries.
    + No support for case insensitive matching.
    + Additional support for the Block enumerated property.

    At present, there is no support for the Script_Extensions property.
*)

(** {6 Modules} *)

(** Deterministic finite automata for Unicode code points. *)
module DFA: sig
    include Cf_dfa.Regular with type event := Uchar.t

    include Cf_dfa.Machine
       with type event := Uchar.t
        and type 'r fin := 'r fin

    (** Use [text_to_term s] to make a term representing the regular
        expression in [s]. Raises [Invalid_argument] if [s] is not a valid
        regular expression.
    *)
    val text_to_term: Ucs_text.t -> term

    (** Use [uchars_to_term s] to make a term representing the regular
        expression parsed from the Unicode code points in [s]. Raises
        [Invalid_argument] if [s] does not comprise a valid regular expression.
    *)
    val uchars_to_term: Uchar.t Seq.t -> term

    (** This module extends the usual set of affix operators. *)
    module Affix: sig
        include Cf_dfa.Affix
           with type event := Uchar.t
            and type term := term
            and type 'a fin := 'a fin

        (** Using [!:s] is the same as [string_to_term s]. *)
        val ( !$ ): string -> term

        (** Using [s $$= v] is the same as [fin (string_to_term s) v]. *)
        val ( $$= ): string -> 'a -> 'a fin
    end
end

(** The type of a compiled regular expression. *)
type t

(** Use [of_text s] to make a regular expression denoted by [s]. Raises
    [Invalid_argment] if [s] does not denote a valid regular expression.
*)
val of_text: Ucs_text.t -> t

(** Use [of_uchars s] to make a regular expression denoted by the Unicode
    codepoints in [s]. Raises [Invalid_argment] if the characters do not denote
    a valid regular expression.
*)
val of_uchars: Uchar.t Seq.t -> t

(** Use [of_dfa_term s] to make a regular expression for recognizing the
    language term [s].
*)
val of_dfa_term: DFA.term -> t

(** Use [test r t] to test whether the text [t] matches the regular
    expression [r].
*)
val test: t -> Ucs_text.t -> bool

(** Use [contains r t] to test whether [r] recognizes any substring of [t]. *)
val contains: t -> Ucs_text.t -> bool

(** Use [search r s] to search with [r] in a confluently persistent sequence
    [s] for the first accepted subsequence. Returns [None] if [s] does not
    contain a matching subsequence. Otherwise, returns [Some (start, limit)]
    where [start] is the index of the first matching subsequence, and [limit]
    is the index after the end of the longest matching subsequence.
*)
val search: t -> Uchar.t Seq.t -> (int * int) option

(** Use [split r s] to split [s] into a sequence of slices comprising the
    substrings in [s] that are separated by disjoint substrings matching [r],
    which are found by searching from left to right. If [r] does not match any
    substring in [s], then a sequence containing just [s] is returned, even if
    [s] is an empty slice.
*)
val split: t -> Ucs_text.t -> Ucs_text.t Seq.t

(** Use [quote s] to make a copy of [s] by converting all the special
    characters into escape sequences.
*)
val quote: Ucs_text.t -> Ucs_text.t

(** Use [unquote s] to make a copy of [s] by converting all the escape
    sequences into ordinary characters.
*)
val unquote: Ucs_text.t -> Ucs_text.t

(*--- End ---*)
