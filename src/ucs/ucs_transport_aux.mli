(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** {6 Interface} *)

(** The signature of a transport form module. *)
module type Profile = sig

    (** A Latin-1 text string containing the name of the transport form, e.g.
        ["UTF-8"] or ["UTF-16LE"].
    *)
    val descript: string

    (** Use [size_of_uchar c] to compute the number of octets required to
        represent [c] in this transport form.
    *)
    val size_of_uchar: Uchar.t -> int

    (** Use [s#scan uchar_decode_scheme] to scan the next Unicode code point
        available as encoded octets to [s]. Raises [Cf_decode.Invalid] if the
        octets do not comprise a valid Unicode character.
    *)
    val uchar_decode_scheme: Uchar.t Cf_decode.scheme

    (** Use [e#emit of_char uchar_encode_scheme c] to emit [c] as encoded
        octets with [e].
    *)
    val uchar_encode_scheme: Uchar.t Cf_encode.scheme

    (** Use [s#scan bom_decode_scheme] to scan the octets of the next code
        point in the stream and, if they are the encoding of the Unicode Byte
        Order Mark (BOM) character, [U+FEFF], to produce the unit value. Raises
        [Cf_decode.Invalid] if the octets do not comprise a BOM character.
    *)
    val bom_decode_scheme: unit Cf_decode.scheme

    (** Use [e#emit bom_encode_scheme `UTF8] to emit the encoding of the
        Unicode Byte Order Mark (BOM) character, [U+FEFF].
    *)
    val bom_encode_scheme: unit Cf_encode.scheme

    (** Use [seq_of_scanner sxr] to make a volatile sequence that scans
        Unicode characters with [sxr]. Consuming the sequence can raise
        exceptions from {Cf_decode}.
    *)
    val seq_of_scanner: #Cf_decode.scanner -> Uchar.t Seq.t

    (** Use [seq_of_string str] to make a volatile sequence that scans Unicode
        characters from [str]. Consuming the sequence can raise exceptions from
        {Cf_decode}.
    *)
    val seq_of_string: string -> Uchar.t Seq.t

    (** Use [seq_of_slice str] to make a volatile sequence that scans Unicode
        characters from [str]. Consuming the sequence can raise exceptions from
        {Cf_decode}.
    *)
    val seq_of_slice: string Cf_slice.t -> Uchar.t Seq.t

    (** Use [seq_to_emitter exr s] to consume all the Unicode characters in [s]
        and emit them with [exr].
    *)
    val seq_to_emitter: #Cf_encode.emitter -> Uchar.t Seq.t -> unit

    (** Use [seq_to_string s] to make a string containing all the Unicode
        characters in [s] encoded according to the transport form. Raises
        [Failure] if more than [Sys.max_string_length] octets are required.
    *)
    val seq_to_string: Uchar.t Seq.t -> string

    (** Use [validate_string s] to raise [Cf_decode.Invalid] with the position
        in [s] of the first invalid octet. Returns normally if the string is
        a valid encoding.
    *)
    val validate_string: string -> unit

    (** Use [validate_slice s] to raise [Cf_decode.Invalid] with the position
        in [s] of the first invalid octet. Returns normally if the slice
        comprises a valid encoding.
    *)
    val validate_slice: string Cf_slice.t -> unit

    (** Use [is_valid_string s] to test whether [s] comprises a valid encoding
        of octets in the transport form.
    *)
    val is_valid_string: string -> bool

    (** Use [is_valid_slice s] to test whether [s] comprises a valid encoding
        of octets in the transport form.
    *)
    val is_valid_slice: string Cf_slice.t -> bool
end

(** Private interfaces used in common by all transport forms. *)
module Private: sig
    val bom: Uchar.t
    val decode_invalid: Cf_decode.position -> string -> 'a

    module type Basis = sig
        val descript: string
        val size_of_uchar: Uchar.t -> int
        val size_of_codeunit: int

        val decode_ck:
            string -> Cf_decode.position -> Cf_decode.size ->
            int Cf_decode.analysis

        val encode_wr: int -> bytes -> int -> unit
    end

    module Create(B: Basis): Profile
end

(*--- End ---*)
