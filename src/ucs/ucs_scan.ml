(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let is_white_space = Ucs_db_aux.query_map Ucs_property_core.white_space
let is_grapheme_base = Ucs_db_aux.query_map Ucs_property_core.grapheme_base
let is_id_start = Ucs_db_aux.query_map Ucs_property_core.id_start
let is_id_continue = Ucs_db_aux.query_map Ucs_property_core.id_continue

module type Profile = sig
    module Annot: Cf_annot.Textual.Unicode.Profile

    include Cf_scan.Profile
       with type symbol := Uchar.t
        and type position := Cf_annot.Textual.position
        and type 'a form := 'a Annot.form

    val ows: unit Annot.form t
    val rws: unit Annot.form t
    val ids: Ucs_text.t Annot.form t

    val of_text: 'a t -> Ucs_text.t -> 'a
end

module Create(U: Ucs_transport.Profile) = struct
    module Basis = struct
        include U
        let is_grapheme_base = is_grapheme_base
    end

    module Annot = Cf_annot.Textual.Unicode.Create(Basis)
    include Cf_scan.Create(Annot.Scan_basis)
    open Infix

    let ows = ign is_white_space >>: Annot.mv ()

    let rws =
        let* synl = sat is_white_space in
        let* finl = ign is_white_space in
        return @@ Annot.span synl finl ()

    let ids =
        let module UTF8 = Ucs_transport.UTF8 in
        let rec loop synl finl b exr =
            opt @@ sat is_id_continue >>= function
            | Some ucl ->
                let exr = (exr :> Cf_encode.emitter) in
                exr#emit UTF8.uchar_encode_scheme (Annot.dn ucl);
                (loop[@tailcall]) synl ucl b exr
            | None ->
                return @@ begin
                    Buffer.contents b
                    |> Ucs_text.Unsafe.of_string
                    |> Ucs_text.normalize ?nf:None
                    |> Annot.span synl finl
                end
        in
        let enter =
            let* ucl = sat is_id_start in
            let b = Buffer.create 1 in
            let exr = Cf_encode.buffer_emitter b in
            exr#emit UTF8.uchar_encode_scheme (Annot.dn ucl);
            (loop[@tailcall]) ucl ucl b exr
        in
        enter


    let of_text p s = of_seq p @@ Ucs_text.to_seq s
end

module UTF8 = Create(Ucs_transport.UTF8)

let create id =
    let module UTF = (val Ucs_transport.create id : Ucs_transport.Profile) in
    let module M = Create(UTF) in
    (module M : Profile)

(*--- End ---*)
