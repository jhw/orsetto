# src/ -- Library source files

The Orsetto framework is modular, with its various component libraries
installed using ocamlfind subpackages. The source code for each component
resides in the folder named for its subpackage here.

Briefly, these are the components listed in increasing order of dependency:

- *cf*   -- The core foundation component. Required by all others.
- *ucs*  -- The Unicode character set component. Required by components that
    use Unicode text parsing and emitting. (Requires *cf*.)
- *cbor* -- The Concise Binary Object Representation (CBOR) layer. (Requires
    *ucs* and *cf*.)
- *json* -- The JavaScript Object Notation (JSON) layer. (Requires *ucs* and
    *cf*.)

*note well:* names conventionally use different letter cases to distinguish
between libraries and ocamlfind subpackages. For example, the orsetto.cf
subpackage installs the OrsettoCF.cma library, which contains OCaml modules
that all conventionally begin with the *Cf_* name prefix.
