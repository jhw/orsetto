(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Concise Binary Object Representation (CBOR) event decoder and parsers. *)

(** {6 Overview}

    This module provides a {Cf_decode} scheme for CBOR events, and a system of
    {Cf_scan} parsers for scanning CBOR encoded octet streams.
*)

(** {6 Events} *)

(** The CBOR event decoder scheme. This scheme can decode most valid CBOR
    encodings.

    Note well: scanning events with this scheme raises [Cf_decode.Invalid] when
    it encounters a definite length array or map with more elements than an
    OCaml array type can comprise, and when it encounters a definite length
    octet sequence or Unicode text with more octets than an OCaml string type
    can comprise. All other valid CBOR encodings are recognized.
*)
val event: Cbor_event.t Cf_decode.scheme

(** The annotation system. *)
module Annot: Cf_annot.Coded.Profile with type symbol := Cbor_event.t

(** Include the scanner profile. Errors from the symbol decoder are translated
    to raise [Bad_syntax].
*)
include Cf_scan.Profile
   with type symbol := Cbor_event.t
    and type position := Cf_decode.position
    and type 'a form := 'a Annot.form

(** {6 Parsers} *)

(** Use [of_slice p s] to parse the octets in [s] with [p]. *)
val of_slice: 'a t -> string Cf_slice.t -> 'a

(** Use [of_string p s] to parse the octets in [s] with [p]. *)
val of_string: 'a t -> string -> 'a

(** {4 Scalar Values} *)

(** A parser that recognizes the CBOR {i null} value. *)
val null: unit Annot.form t

(** A parser that recognizes the CBOR {i break} value, signifying the end of
    an indefinite length sequence.
*)
val break: unit Annot.form t

(** A parser that recognizes the CBOR {i true} and {i false} values. *)
val boolean: bool Annot.form t

(** A parser that recognizes a positive or negative CBOR {i integer} value,
    provided that it can be represented by an OCaml integer.
*)
val integer: int Annot.form t

(** A parser that recognizes a positive or negative CBOR {i integer} value,
    provided that it can be represented by an OCaml {i int32} value. The
    returned 32 octets are to be regarded unsigned. The arithmetic sign of
    the encoded integer is returned separately.
*)
val int32: (Cbor_event.sign * int32) Annot.form t

(** A parser that recognizes any positive or negative CBOR {i integer} value.
    The returned 64 octets are to be regarded unsigned. The arithmetic sign of
    the encoded integer is returned separately.
*)
val int64: (Cbor_event.sign * int64) Annot.form t

(** A parser that recognizes a CBOR {i floating point} number value. *)
val float: float Annot.form t

(** A parser that recognizes a CBOR {i octets} value. Indefinite length and
    definite length octet sequences are recognized equivalently.

    If [~a] is used, then it specifies the minimum number of octets in the
    recognized value. If [~b] is used, then it specifies the maximum number of
    octets in the recognized value. The default value of [~a] is zero and the
    default value of [~b] is [Sys.max_string_length]. Raises [Invalid_argument]
    if [a < 0], [b < a] or [b > Sys.max_string_length].

    If the encoded value has indefinite length, then CBOR events are only
    consumed from the input stream while the total number of octets does not
    exceeed [b].
*)
val octets: ?a:int -> ?b:int -> unit -> string Annot.form t

(** A parser that recognizes a CBOR {i text} value. Indefinite length and
    definite length texts are recognized equivalently.

    If [~nf] is used, then the decoded text is normalized accordingly.

    If [~a] is used, then it specifies the minimum number of UTF-8 encoded
    octets in the encoded text. If [~b] is used, then it specifies the maximum
    number of UTF-8 encoded octets in the encoded text. The default value of
    [~a] is zero and the default value of [~b] is [Sys.max_string_length].
    Raises [Invalid_argument] if [a < 0], [b < a] or
    [b > Sys.max_string_length]. If [~nf] is used with either [~a] or [~b],
    then it's possible that normalization may produce a decoded text with fewer
    than [a] UTF-8 encoded octets, or more than [b] UTF-8 encoded octets.
    Raises [Failure] if the normalized result requires more than
    [Sys.max_string_length] UTF-8 encoded octets.

    If the encoded value has indefinite length, then CBOR events are only
    consumed from the input stream while the total number of octets does not
    exceeed [b].
*)
val text: ?nf:(module Ucs_normal.Profile) -> ?a:int -> ?b:int -> unit ->
    Ucs_text.t Annot.form t

(** {4 Aggregate Values} *)

(** A parser that recognizes a CBOR {i tag} number. *)
val tag: int64 Annot.form t

(** Use [start s] to make a parser that recognizes the start of an indefinite
    length sequence of the form [s]. What follows in a valid CBOR event stream
    depends on the form accordingly:

        - [`Octets]: a sequence of definite length {i octet} values.
        - [`Text]: a sequence of definite length {i text} values.
        - [`Array]: a sequence of CBOR values, each of arbitrary form.
        - [`Map]: a sequence of CBOR key-value pairs, where keys and values are
            both of arbitrary form.

    All indefinite length sequences in a valid CBOR event stream are terminated
    by a {i break} value.
*)
val start: [< `Octets | `Text | `Array | `Map ] -> unit Annot.form t

(** Use [group ?a ?b g p] to make a parser that recognizes a group comprising,
    according to [g], either a CBOR {i array} or a CBOR {i map} with content
    recognized by [p].

    If the encoded group is definite length, then the input stream provided to
    [p] is limited to the events comprising the content, and no result from [p]
    is recognized unless it consumes all its input. If the encoded content is
    indefinite length, then the result is only recognized if a {i break} event
    follows the content.

    If [~a] is used, then it specifies the minimum number of elements in the
    group. In the case of a map, each group element is a key-value pair. If
    [~b] is used, then it specifies the maximum number of elements in the
    group. The default value of [~a] is zero and the default value of [~b] is
    [Sys.max_array_length].

    Composing raises [Invalid_argument] if [a < 0], [b < a] or
    [b > Sys.max_array_length]. If the encoded value has indefinite length,
    then the input stream provided to [p] is limited to the events comprising
    the first [b] array elements (or map key-value pairs) in the group.

    Parsing raises [Bad_syntax] if a {i break} event appears in the content of
    a definite length array or map, or after a key in map content, or if an
    indefinite length group is not terminated by a {i break} event.
*)
val group: ?a:int -> ?b:int -> [< `Array | `Map ] -> 'a t -> 'a Annot.form t

(** {4 Opaque Types} *)

(** A submodule containing logic for parsing CBOR messages to values of type
    [Cf_type.opaque] according to optional mode selectors.
*)
module Opaque: sig

    (** The mode selectors for decoding CBOR messages to opaque values. *)
    type mode

    (** Use [mode ()] to create a mode selector record for the opaque value
        decoder. Use any of the various optional parameters to set a mode
        selector to other than its default value. The modes are as follows:

        - {strings}: Controls how UTF8 texts are decoded, either as
            [Ucs_text.t] (the default) or as [string].
    *)
    val mode: ?strings:[< `Text | `String ] -> unit -> mode

    (** A parser that recognizes any CBOR encoded value and produces an
        annotated value with encapsulated runtime type indication. The
        following table describes the runtime type indications produced for
        values decoded.

        - {i null}:     [Cf_type.Unit]
        - {i boolean}:  [Cf_type.Bool]
        - {i integer}:  [Cf_type.Int], [Cf_type.Int32], or [Cf_type.Int64]
        - {i float}:    [Cf_type.Float]
        - {i octets}:   [Cf_type.String]
        - {i text}:     [Ucs_type.Text]
        - {i array}:    [Cf_type.(Seq Opaque)]
        - {i map}:      [Cf_type.(Seq (Pair (Opaque, Opaque))]
        - {i tag}:      [Cbor_type.(Tag Cf_type.Opaque))]
        - {i reserved}: [Cbor_type.Reserved]

        Use the [~mode] parameter to select modes other than the default.

        Use the {Cbor_type} module for unpacking.

        Note well: the {i array} and {i map} containers are sequences of
        untyped values, which must be unpacked recursively. Exceeding a maximum
        recursion depth of 1000 raises [Bad_syntax].

        Also: the {i tag} container is a pair comprising a tag number and the
        typed value witnessed by the parameter of the [Tag] type.

        Finally, integers decoded into [Int] unless the value is outside the
        range of valid OCaml integers, in which case the value is decoded as
        [Int32] or [Int64], whichever is smaller and yet still large enough to
        contain the decoded value.
    *)
    val value: ?mode:mode -> unit -> Cf_type.opaque Annot.form t

    (** Use [model mode] to make a modified primitive data ingest model for
        opaque values that uses [mode] to control its input formatting.
    *)
    val model: mode -> Cf_type.opaque Cf_data_ingest.model
end

(** {6 Abstract Data Ingestion} *)

(** The data ingestor module specialized for CBOR encoded octet streams. *)
module Ingest: sig
    open Cf_data_ingest

    (** Use [tagged n m] to make a data ingesting model that requires the CBOR
        tag [n] to be applied to the data ingested by [m]. Such models can only
        be ingested with CBOR scanners produced by [scan] (see below).
    *)
    val tagged: int64 -> 'v model -> 'v model

    (** Use [scan m] to make a parser that recognizes the data model [m]. *)
    val scan: 'a model -> 'a Annot.form t
end

(** (4 Obsolescent}

    The following interfaces are obsolescent with attributes noting their
    preferable alternatives. They may be removed in a future revision.
*)

(** An opaque value decode created by [Opaque.value] with default mode. *)
val value: Cf_type.opaque Annot.form t
    [@@ocaml.alert deprecated "Use Opaque.value instead!"]

(** This module provides an adaptation of the {Cf_record_scan} interface for
    scanning CBOR map values.
*)
module Record_obsolescent: sig

    (** The basis signature for record modules. *)
    module type Basis = sig
        module Index: Cf_relations.Order
        val index: Index.t t
    end

    (** The signature of CBOR map value scanners. *)
    module type Profile = Cf_record_scan.Profile
       with type 'a form := 'a Annot.form
        and type 'a t := 'a t

    (** Use [Create(B)] to make a record module for the index type in [B]. The
        [scan] function in this module extracts the minimum [a] and maximum [b]
        numbers of fields admitted by the schema, and uses them with the
        [group ~a ~b `Map] combinator defined above.
    *)
    module Create(B: Basis):
        Profile with type index := B.Index.t
        [@@ocaml.alert deprecated "Use Ingest instead!"]
end

module Record = Record_obsolescent
    [@@ocaml.alert deprecated "Use Ingest instead!"]

(** A record module with integer indexes. *)
module Int_map: Record_obsolescent.Profile with type index := int
    [@@ocaml.alert deprecated "Use Ingest instead!"]

(** A record module with text indexes. *)
module String_map: Record_obsolescent.Profile with type index := string
    [@@ocaml.alert deprecated "Use Ingest instead!"]

(*--- End ---*)
