(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type signal = [ `Break | `Octets | `Text | `Array | `Map ]
type sign = [ `Positive | `Negative ]
type ieee754_precision = [ `Half | `Single | `Double ]

let force_precision p n =
    match p with
    | `Half -> Cf_endian_core.(to_fp16_bits_unsafe n |> of_fp16_bits)
    | `Single -> Int32.(bits_of_float n |> float_of_bits)
    | `Double -> n

let select_precision n =
    if n = force_precision `Half n then
        `Half
    else if n = force_precision `Single n then
        `Single
    else
        `Double

type minor = Minor of int64 [@@unboxed]

let minor_equal (Minor a) (Minor b) = Int64.equal a b

let minor_to_intopt =
    let max64 = Int64.of_int max_int in
    let enter (Minor n) =
        if Int64.compare n 0L < 0 || Int64.compare n max64 > 0 then
            None
        else
            Some (Int64.to_int n)
    in
    enter

(*
let minor_to_z =
    let e64 = Z.(shift_left one 64) in
    let enter (Minor n) =
        let z = Z.of_int64 n in
        if Int64.compare n 0L < 0 then Z.(add z e64) else z
    in
    enter
*)

let int_to_minor n =
    if n < 0 then invalid_arg (__MODULE__ ^ ".int_to_minor: negative.");
    Minor (Int64.of_int n)

let int64_to_minor ?unsigned n64 =
    match unsigned with
    | Some () -> Minor n64
    | None when n64 >= 0L -> Minor n64
    | None -> invalid_arg (__MODULE__ ^ ".int64_to_minor: negative.");

type t =
    | Null
    | Signal of signal
    | Boolean of bool
    | Integer of sign * minor
    | Float of ieee754_precision * float
    | Octets of string
    | Text of string
    | Array of minor
    | Map of minor
    | Tag of minor
    | Reserved of int
    | Undefined of string

let equal a b = begin[@warning "-4"]
    match a, b with
    | Null, Null ->
        true
    | Signal a, Signal b ->
        a == b
    | Boolean a, Boolean b ->
        a == b
    | Integer (sa, a), Integer (sb, b) ->
        a == b && sa == sb
    | Float (_, a), Float (_, b) ->
        a = b
    | Octets a, Octets b
    | Text a, Text b ->
        String.equal a b
    | Array a, Array b
    | Map a, Map b
    | Tag a, Tag b ->
        minor_equal a b
    | Reserved a, Reserved b ->
        Int.equal a b
    | _ ->
        false
end

let null = Null
let signal s = Signal (s :> signal)
let boolean b = Boolean b
let integer sign minor = Integer ((sign :> sign), minor)

let float ?precision n =
    if Cf_endian_core.is_nan n then
        Float (`Half, nan)
    else begin
        let p =
            match precision with
            | None -> select_precision n
            | Some p -> (p :> ieee754_precision)
        in
        Float (p, n)
    end

let octets s = Octets s

let text s =
    Ucs_transport.UTF8.validate_string s;
    Text s

let array n = Array n
let map n = Map n
let tag n = Tag n

let unsafe_minor n = Minor n
let unsafe_text s = Text s
let unsafe_reserved n = Reserved n
let unsafe_undefined s = Undefined s

let width_octets =
    let intwidth = function
        | Minor n when n < 24L -> 0
        | Minor n when n < 0x100L -> 1
        | Minor n when n < 0x10000L -> 2
        | Minor n when Sys.int_size < 32 || n < 0x100000000L -> 4
        | Minor _ -> 8
    and floatwidth = function
        | `Half -> 2
        | `Single -> 4
        | `Double -> 8
    in
    let minor = function
        | Integer (_, n)
        | Array n
        | Map n
        | Tag n ->
            intwidth n
        | Float (p, _) ->
            floatwidth p
        | Octets s
        | Text s ->
            let n0 = String.length s in
            let n1 = intwidth @@ Minor (Int64.of_int n0) in
            n0 + n1
        | Reserved n ->
            if n > 0x20 then 1 else 0
        | Null
        | Boolean _
        | Signal _
        | Undefined _  ->
            0
    in
    let enter e = 1 + minor e in
    enter

(*--- End ---*)
