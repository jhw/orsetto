(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Concise Binary Object Representation (CBOR) event and value encoders. *)

(** {6 Overview}

    This module provides a {Cf_encode} scheme for CBOR events, and a system of
    schemes for emitting CBOR values as octet streams.
*)

(** {5 Events} *)

(** The CBOR event encoder scheme. This scheme can encode most valid CBOR
    encodings.
*)
val event: Cbor_event.t Cf_encode.scheme

(** {6 Encoding Monad} *)

(** A CBOR encoding is a unary monad. *)
include Cf_monad.Unary.Profile

(** The CBOR encoding of the {i null} value. *)
val null: unit t

(** Use [boolean b] to make the CBOR encoding of [b]. *)
val boolean: bool -> unit t

(** Use [integer n] to make the CBOR encoding of the integer value [n]. *)
val integer: int -> unit t

(** Use [int32 n] to make the CBOR encoding of the `int32` value [n]. Use the
    [~sign] argument to regard [n] as unsigned rather than 2's-complement, and
    to explicitly control the arithmetic sign of the encoded integer.
*)
val int32: ?sign:[< Cbor_event.sign ] -> int32 -> unit t

(** Use [int64 n] to make the CBOR encoding of the `int64` value [n]. Use the
    [~sign] argument to regard [n] as unsigned rather than 2's-complement, and
    to explicitly control the arithmetic sign of the encoded integer.
*)
val int64: ?sign:[< Cbor_event.sign ] -> int64 -> unit t

(** Use [float n] to make the CBOR encoding of the floating point value [n].
    If [~precision] is not provided, then the most precise form required to
    represent [n] without loss is selected.
*)
val float: ?precision:Cbor_event.ieee754_precision -> float -> unit t

(** Use [octets s] to make the CBOR encoding of the octet sequence [s]. *)
val octets: string -> unit t

(** Use [text s] to make the CBOR encoding of the Unicode text [s]. *)
val text: Ucs_text.t -> unit t

(** Use [array s] to compose the CBOR definite size array from the encodings in
    the list [s].
*)
val array: unit t list -> unit t

(** Use [map s] to compose the CBOR definite size map containing the key-value
    pairs encoded in the list [s]. Use the [~sort:()] option to sort the keys
    according to the deterministic encoding of CBOR.
*)
val map: ?sort:unit -> (unit t * unit t) list -> unit t

(** Use [tagged n v] to make the CBOR encoding of [v] tagged with [n] *)
val tagged: int64 -> unit t -> unit t

(** {5 Indefinite length sequences} *)

(** Use [octets_seq s] to make the CBOR encoding of the indefinite length octet
    sequence comprising the strings in [s].
*)
val octets_seq: string Seq.t -> unit t

(** Use [text_seq s] to make the CBOR encoding of the indefinite length Unicode
    text comprising the concatenation of the UTF-8 encoded fragments in [s].
*)
val text_seq: Ucs_text.t Seq.t -> unit t

(** Use [array_seq s] to make the CBOR encoding of the indefinite length array
    comprising the elements encoded in [s].
*)
val array_seq: unit t Seq.t -> unit t

(** Use [map_seq s] to make the CBOR encoding of the indefinite length map
    comprising the key-value pairs encoded in [s].
*)
val map_seq: (unit t * unit t) Seq.t -> unit t

(** {5 Value Schemes} *)

(** Use [scheme f] to make the encoding scheme that applies [f] to the emitted
    value to obtain the encoding of the sequence of CBOR events comprising its
    encoding.
*)
val scheme: ('v -> unit t) -> 'v Cf_encode.scheme

(** {5 Abstract Data}

    A specialization of the {Cf_data_render.Profile} for use with CBOR is
    provided here.

    Containers with positional elements, e.g. vectors and records, are encoded
    as definite length CBOR arrays. Containers with indexed elements, e.g.
    tables and structures, are encoded as definite length CBOR maps.
*)

(** A submodule to encapsulate functions for use with data rendering models. *)
module Render: sig
    open Cf_data_render

    (** Use [new index p] to compose indexes for tables and structures encoded
        as CBOR maps with keys sorted in deterministic order. Use the [~model]
        parameter to provide the data model for keys that are not primitive.
    *)
    class ['a] deterministic_index:
        ?model:'a model -> 'a Cf_type.nym -> ['a] index

    (** Use [tagged n m] to make a data rendering model that applies the CBOR
        tag [n] to the data rendered by [m]. Such models can only be rendered
        into encoding schemes by the [scheme] function for CBOR (see below).
    *)
    val tagged: int64 -> 'v model -> 'v model

    (** Use [scheme m] to make an encoding scheme for values according to an
        abstract data model [m].
    *)
    val scheme: 'v model -> 'v Cf_encode.scheme

    (** Use [packet m v] to make the CBOR encoding of [v] according to the data
        render model [m].
    *)
    val packet: 'v model -> 'v -> unit t
end

(** {5 Opaque Types} *)

(** A submodule containing logic for encoding CBOR messages from values of type
    [Cf_type.opaque] according to optional mode selectors.
*)
module Opaque: sig

    (** The mode selectors for encoding CBOR messges from opaque values. *)
    type mode

    (** Use [mode ()] to create a mode selector record for the opaque value
        encoder. Use any of the various optional parameters to set a mode
        selector to other than its default value. The modes are as follows:

        - {strings}: Controls how OCaml strings are encoded. The default is
            {i octets}.
    *)
    val mode: ?strings:[< `Octets | `Text ] -> unit -> mode

    (** Use [value v] to create the CBOR encoding of the opaque value [v]
        provided it was witnessed by a type valid for output. The following
        table describes the runtime type indications required for values
        emitted.

        Scalars:

        - [Cf_type.Unit]:   {i null}
        - [Cf_type.Bool]:   {i boolean}
        - [Cf_type.Int]:    {i integer}
        - [Cf_type.Int32]:  {i integer}
        - [Cf_type.Int64]:  {i integer}
        - [Cf_type.Float]:  {i integer}
        - [Cf_type.String]: {i octets} (or {i text} if mode.strings = `Text)
        - [Ucs_type.Text]:  {i text}

        Containers:

        - [Cf_type.(Seq {i nym})]:                      {i array}
        - [Cf_type.(Seq (Pair ({i nym}, {i nym})))]:    {i map}
        - [Cbor_type.(Tag ({i nym}))]:                  {i tag}

        ...and...

        - [Cf_type.Opaque]: {i any of the above}

        Use the [~mode] parameter to select modes other than the default.

        Raises [Invalid_argument] if the witnessed type is not valid for output
        as a CBOR value.
    *)
    val value: ?mode:mode -> Cf_type.opaque -> unit t

    (** Use [model mode] to make a modified primitive data render model for
        opaque values that uses [mode] to control its output formatting.
    *)
    val model: mode -> Cf_type.opaque Cf_data_render.model
end

(** {5 Utility} *)

(** Use [to_string v] to make a string comprising the encoding of [v]. *)
val to_string: unit t -> string

(** {6 Deprecated} *)

(** Use [opaque v] to make the CBOR encoding of the opaque value [v].
    Raises [Invalid_argument] if [v] witnesses a type that cannot be encoded.

*)
val value: Cf_type.opaque -> unit t
    [@@ocaml.alert deprecated "Use Opaque.value instead!"]

(*--- End ---*)
