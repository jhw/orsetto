(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Runtime type indications for CBOR encoded values. *)

(** {6 Interface} *)

(** The runtime type indicator for a pair comprising a CBOR tag number and its
    tagged value.
*)
type _ Cf_type.nym += Tag: 'a Cf_type.nym -> (int64 * 'a) Cf_type.nym

(** The runtime type indicator for unrecognized simple values, to be registered
    with IANA, and reserved for future specifications or by standards action.
*)
type _ Cf_type.nym += Reserved: int Cf_type.nym

(** The runtime type indicator for a CBOR event. *)
type _ Cf_type.nym += Event: Cbor_event.t Cf_type.nym

(** The type relations for all CBOR encoded values. *)
class horizon: Cf_type.horizon

(** Include the form. *)
include Cf_type.Form

(*--- End ---*)
