(*---------------------------------------------------------------------------*
  Copyright (C) 2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Concise Binary Object Representation (CBOR) diagnostic notation. *)

(** {6 Overview}

    Specialization of {Cf_emit} to output texts in the CBOR diagnostic format.
    Combinators are provided for the basic CBOR types to be used for directly
    emitting application data as CBOR diagnostic notation. Additional
    conveniences are provided for emitting encapsulated CBOR values as text.
*)

(** {5 Type} *)

(** The type of CBOR diagnostic notation emitters. *)
type 'a t = private 'a Cf_emit.To_formatter.t

(** {5 Convenience} *)

(** Use [of_string s] to convert the CBOR encoded octets in [s] to the
    diagnostic notation for the encoded CBOR events.
*)
val of_string: string -> string

(** Use [of_slice s] to convert the CBOR encoded octets in [s] to the
    diagnostic notation for the encoded CBOR events.
*)
val of_slice: string Cf_slice.t -> string

(** {5 Emitters} *)

(** The emitter for a sequence of decoded CBOR events. Output may contain
    encoding indications.
*)
val events: Cbor_event.t Seq.t t

(*--- End ---*)
