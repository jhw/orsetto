# src/cbor - Concise Binary Object Representation (CBOR)

The abstract from RFC 7049 says:

> The Concise Binary Object Representation (CBOR) is a data format whose design
goals include the possibility of extremely small code size, fairly small
message size, and extensibility without the need for version negotiation. These
design goals make it different from earlier binary serializations such as ASN.1
and MessagePack.

The following modules are intended to comprise public interfaces:

- *Cbor_decode* -- A general purpose CBOR decoder.
- *Cbor_encode* -- A general purpose CBOR encoder.
- *Cbor_event* -- A representation of a CBOR stream event.
- *Cbor_flyweight* -- A flyweight implementation of CBOR, useful for
    applications where code size is very limited.
- *Cbor_notation* -- Emitters for the CBOR diagnostic notation.
- *Cbor_type* -- An extension of *Ucs_type* that includes CBOR tagged values.
