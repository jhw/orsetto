(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module E = Cbor_event

let event =
    let open Cf_endian_big.Unsafe in
    let open Cf_encode in
    let intwidth = function
        | n when n < 0L -> 8
        | u when u < 24L -> 0
        | u when u < 0x100L -> 1
        | u when u < 0x10000L -> 2
        | u when u < 0x100000000L -> 4
        | _ -> 8
    and floatwidth = function
        | `Half -> 2
        | `Single -> 4
        | `Double -> 8
    and simplewidth n =
        if n < 0x20 then 0 else 1
    in
    let need = function
        | E.Integer (_, n)
        | E.Array n
        | E.Map n
        | E.Tag n ->
            let E.Minor n = n in
            intwidth n
        | E.Float (p, _) ->
            floatwidth p
        | E.Reserved n ->
            simplewidth n
        | E.Octets s
        | E.Text s ->
            let n = String.length s in
            n + Int64.(intwidth @@ of_int n)
        | E.Null
        | E.Boolean _
        | E.Signal _
        | E.Undefined _  ->
            0
    in
    let wrcode major quintet b i =
        (stu8[@tailcall]) ((major lsl 5) lor quintet) b i
    in
    let putcode major quintet b i =
        wrcode major quintet b i;
        succ i
    in
    let intquintet n =
        if n < 0L then
            27
        else if n < 24L then
            Int64.to_int n
        else if n < 0x100L then
            24
        else if n < 0x10000L then
            25
        else if n < 0x100000000L then
            26
        else
            27
    and floatquintet = function
        | `Half -> 25
        | `Single -> 26
        | `Double -> 27
    and wrfloat n prec b i =
        match prec with
        | `Half ->
            let fp16 = Cf_endian_core.to_fp16_bits_unsafe n in
            (stu16[@tailcall]) fp16 b i
        | `Single ->
            (sti32[@tailcall]) (Int32.bits_of_float n) b i
        | `Double ->
            (sti64[@tailcall]) (Int64.bits_of_float n) b i
    in
    let wrinfo quintet info b i =
        match quintet with
        | 24 -> (stu8[@tailcall]) (Int64.to_int info) b i;
        | 25 -> (stu16[@tailcall]) (Int64.to_int info) b i;
        | 26 -> (stu32[@tailcall]) (Int64.to_int info) b i;
        | 27 -> (sti64[@tailcall]) info b i;
        | _ -> ()
    in
    let wroctets major s b i =
        let n = String.length s in
        let n64 = Int64.of_int n in
        let quintet = intquintet n64 in
        let i = putcode major quintet b i in
        wrinfo quintet n64 b i;
        let width =
            if quintet < 24 || quintet > 27 then 0 else 1 lsl (quintet - 24)
        in
        (Bytes.blit_string[@tailcall]) s 0 b (i + width) n
    in
    let ck _pos sz v =
        analyze sz (need v + 1) v
    and wr v b i =
        match v with
        | E.Null ->
            (wrcode[@tailcall]) 7 22 b i
        | E.Boolean v ->
            (wrcode[@tailcall]) 7 (if v then 21 else 20) b i
        | E.Undefined _ ->
            (wrcode[@tailcall]) 7 23 b i
        | E.Reserved n ->
            (wrcode[@tailcall]) 7 n b i
        | E.Integer (`Negative, E.Minor n) ->
            let q5 = intquintet n in
            let i = putcode 1 q5 b i in
            (wrinfo[@tailcall]) q5 n b i
        | E.Integer (`Positive, E.Minor n) ->
            let q5 = intquintet n in
            let i = putcode 0 q5 b i in
            (wrinfo[@tailcall]) q5 n b i
        | E.Float (p, n) ->
            let i = putcode 7 (floatquintet p) b i in
            (wrfloat[@tailcall]) n p b i
        | E.Octets s ->
            (wroctets[@tailcall]) 2 s b i
        | E.Text s ->
            (wroctets[@tailcall]) 3 s b i
        | E.Array (E.Minor n) ->
            let q5 = intquintet n in
            let i = putcode 4 q5 b i in
            (wrinfo[@tailcall]) q5 n b i
        | E.Map (E.Minor n) ->
            let q5 = intquintet n in
            let i = putcode 5 q5 b i in
            (wrinfo[@tailcall]) q5 n b i
        | E.Tag (E.Minor n) ->
            let q5 = intquintet n in
            let i = putcode 6 q5 b i in
            (wrinfo[@tailcall]) q5 n b i
        | E.Signal signal ->
            let major =
                match signal with
                | `Break -> 7
                | `Octets -> 2
                | `Text -> 3
                | `Array -> 4
                | `Map -> 5
            in
            (wrcode[@tailcall]) major 31 b i
    in
    scheme 1 ck wr

include Cf_encode.Monad
open Affix

let to_string v =
    let f () = v in
    Cf_encode.to_string (eval f) ()

module String_map = Cf_rbtree.Map.Create(String)

let null = seal event E.null
let boolean b = seal event @@ E.boolean b

let integer n =
    let sign, n = if n < 0 then `Negative, (-1) - n else `Positive, n in
    let n = E.int_to_minor n in
    seal event @@ E.integer sign n

let int64_unapply_sign_ sign n64 =
    match (sign :> Cbor_event.sign option) with
    | None ->
        if n64 < 0L then `Negative, Int64.sub (-1L) n64 else `Positive, n64
    | Some sign ->
        let n64 = if sign == `Negative then Int64.sub (-1L) n64 else n64 in
        sign, n64

let int64 ?sign n64 =
    let sign, n64 = int64_unapply_sign_ sign n64 in
    let minor = E.int64_to_minor ~unsigned:() n64 in
    seal event @@ E.integer sign minor

let int32 ?sign n32 =
    let open Int64 in
    let n64 = of_int32 n32 in
    let sign, n64 = int64_unapply_sign_ sign n64 in
    let minor = E.int64_to_minor ~unsigned:() n64 in
    seal event @@ E.integer sign minor

let float ?precision n = seal event @@ E.float ?precision n
let octets s = seal event @@ E.octets s
let text (Ucs_text.Octets s) = seal event @@ E.unsafe_text s

let array s =
    let n = E.int_to_minor @@ List.length s in
    let* _ = seal event @@ E.array n in
    serial @@ List.to_seq s

let map_sort_ =
    let f (k, _ as kv) = to_string k, kv in
    fun s ->
        Seq.map snd @@ String_map.to_seq_incr @@ String_map.of_seq @@
        Seq.map f s

let map ?sort s =
    let n = E.int_to_minor @@ List.length s in
    let* _ = seal event @@ E.map n in
    let s = List.to_seq s in
    let s = match sort with None -> s | Some () -> map_sort_ s in
    let pair (k, v) = let* _ = k in v in
    serial @@ Seq.map pair s

let tagged n m =
    let* _ = seal event @@ E.tag @@ E.int64_to_minor n in m

let octets_seq s =
    let* _ = seal event @@ E.signal `Octets in
    let f s = seal event @@ E.octets s in
    let* _ = serial @@ Seq.map f s in
    seal event @@ E.signal `Break

let text_seq s =
    let* _ = seal event @@ E.signal `Text in
    let f (Ucs_text.Octets s) = seal event @@ E.text s in
    let* _ = serial @@ Seq.map f s in
    seal event @@ E.signal `Break

let array_seq s =
    let* _ = seal event @@ E.signal `Array in
    let* _ = serial s in
    seal event @@ E.signal `Break

let map_seq s =
    let* _ = seal event @@ E.signal `Map in
    let f (k, v) = let* _ = k in v in
    let* _ = serial @@ Seq.map f s in
    seal event @@ E.signal `Break

module Opaque = struct
    type mode = Mode of {
        strings: [ `Octets | `Text ]
    }

    let mode =
        let or_octets opt =
            match opt with
            | Some strings -> (strings :> [ `Octets | `Text ])
            | None -> `Octets
        in
        let enter ?strings () =
            let strings = or_octets strings in
            Mode { strings }
        in
        enter

    let value =
        let rec loop: mode -> Cf_type.opaque -> unit t = fun mode v0 ->
            let Cf_type.Witness (v, nym) = v0 in
            match nym with
            | Cf_type.Opaque ->
                loop mode @@ Cf_type.(req Opaque) v
            | Cf_type.Unit ->
                null
            | Cf_type.Bool ->
                boolean v
            | Cf_type.Int ->
                integer v
            | Cf_type.Float ->
                float v
            | Cf_type.String ->
                let Mode m = mode in begin
                    match m.strings with
                    | `Octets -> octets v
                    | `Text -> text @@ Ucs_text.of_string v
                end
            | Ucs_type.Text ->
                text v
            | Cf_type.(Seq Opaque) ->
                array @@ List.of_seq @@ Seq.map (loop mode) v
            | Cf_type.(Seq (Pair (Opaque, Opaque))) ->
                let f (k, v) = loop mode k, loop mode v in
                map ~sort:() @@ List.of_seq @@ Seq.map f v
            | Cbor_type.Tag Cf_type.Opaque ->
                let n, v = v in
                tagged n @@ loop mode v
            | Cf_type.Int32 ->
                int32 v
            | Cf_type.Int64 ->
                int64 v
            | Cbor_type.Reserved ->
                if v < 0 || v > 255 then
                    invalid_arg (__MODULE__ ^
                        ".Opaque.value: invalid reserved value.");
                if v > 19 && v < 28 || v = 31 then
                    invalid_arg (__MODULE__ ^
                        ".Opaque.value: reserved value already registered.");
                seal event @@ E.unsafe_reserved v
            | _ ->
                invalid_arg (__MODULE__ ^
                    ".Opaque.value: invalid opaque value type.")
        in
        let mode0 = mode () in
        let enter ?(mode = mode0) v = loop mode v in
        enter

    type _ Cf_type.nym += With_mode: mode -> Cf_type.opaque Cf_type.nym

    let model mode = Cf_data_render.primitive @@ With_mode mode
end

module Render = struct
    type (_, _) Cf_data_render.control +=
        Tagged: int64 -> ('a, 'a) Cf_data_render.control

    module Basis = struct
        let primitive: type a. a Cf_type.nym -> a Cf_encode.scheme = fun nym ->
            match nym with
            | Cf_type.Unit ->
                eval (fun () -> null)
            | Cf_type.Bool ->
                eval boolean
            | Cf_type.Int ->
                eval integer
            | Cf_type.Int32 ->
                eval ?sz:None @@ int32 ?sign:None
            | Cf_type.Int64 ->
                eval ?sz:None @@ int64 ?sign:None
            | Cf_type.Float ->
                eval (fun n -> float n)
            | Ucs_type.Text ->
                eval text
            | Cf_type.String ->
                eval octets
            | Cf_type.Opaque ->
                eval ?sz:None @@ Opaque.value ?mode:None
            | Opaque.With_mode mode ->
                eval ?sz:None @@ Opaque.value ~mode
            | _ ->
                invalid_arg (__MODULE__ ^ ": primitive undefined for CBOR!")

        let control =
            let f:
                type a b. a Cf_encode.scheme ->
                    (a, b) Cf_data_render.control -> b Cf_encode.scheme
                = fun scheme control ->
                    match control with
                    | Tagged n -> eval (fun v -> tagged n @@ seal scheme v)
                    | _ -> Cf_encode.Render.basis_control scheme control
            in
            `Special f

        let pair = `Default

        let sequence = `Special begin fun container packets ->
            match container with
            | (`Vector | `Record) ->
                array @@ List.of_seq packets
            | (`Table _ | `Structure _ | `Variant _) ->
                let packets = List.of_seq packets in
                let n = E.int_to_minor @@ List.length packets in
                let* _ = seal event @@ E.map n in
                serial @@ List.to_seq packets
        end
    end

    include Cf_encode.Render.Create(Basis)

    let deterministic_compare_ scheme a b =
        let a = Cf_encode.to_string scheme a in
        let b = Cf_encode.to_string scheme b in
        String.compare a b

    let deterministic_scheme_ ?model nym =
        match model with
        | None -> Basis.primitive nym
        | Some m -> scheme m

    class ['a] deterministic_index ?model nym =
        let scheme = deterministic_scheme_ ?model nym in
        let cmp = deterministic_compare_ scheme in
        ['a] Cf_data_render.index ~cmp ?model nym

    let tagged n m = Cf_data_render.control m @@ Tagged n
    let packet m = seal (scheme m)
end

let scheme f = eval f

let value = Opaque.value ?mode:None

(*--- End ---*)
