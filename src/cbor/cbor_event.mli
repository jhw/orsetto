(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Concise Binary Object Representation (CBOR) events. *)

(** {6 Overview}

    As defined in RFC 7049, a CBOR object is an octet sequence encoding a
    series of core events that correspond to a simple grammar describing the
    composition of structured data.

    This module provides a safe interface for constructing valid CBOR events
    used by the encoder in {Cbor_encode} and the decoder in {Cbor_decode}.
*)

(** {6 Types} *)

(** The type of indefinite-length sequencing signals. *)
type signal = [ `Break | `Octets | `Text | `Array | `Map ]

(** The type of integer sign. *)
type sign = [ `Positive | `Negative ]

(** The type of IEEE-754 floating point number precision. *)
type ieee754_precision = [ `Half | `Single | `Double ]

(** The private type of CBOR minor codepoints. This is conceptually an unsigned
    64-bit integer, encapsulated here in a private data constructor because the
    OCaml [int64] type is a signed integer.
*)
type minor = private Minor of int64 [@@unboxed]

(** The private type of CBOR events. *)
type t = private
    | Null
    | Signal of signal
    | Boolean of bool
    | Integer of sign * minor
    | Float of ieee754_precision * float
    | Octets of string
    | Text of string
    | Array of minor
    | Map of minor
    | Tag of minor
    | Reserved of int
    | Undefined of string

(** {6 Constructors} *)

(** Use [int_to_minor n] to make a CBOR minor codepoint of value [n]. Raises
    [Invalid_argument] if [n] is negative.
*)
val int_to_minor: int -> minor

(** Use [int64_to_minor n] to make a CBOR minor codepoint of value [n]. Raises
    [Invalid_argument] if [n] is negative unless the [~unsigned:()] optional
    flag is provided to explicitly signal that [n] is to be regarded as
    unsigned.
*)
val int64_to_minor: ?unsigned:unit -> int64 -> minor

(** Use [minor_to_intopt minor] to extract the positive integer corresponding
    to [minor]. Returns [None] if the codepoint cannot be represented by the
    positive range of values of the OCaml integer type.
*)
val minor_to_intopt: minor -> int option

(** The distinguished "null" value. *)
val null: t

(** Use [signal s] to make an event signaling [s]. *)
val signal: [< signal ] -> t

(** Use [boolean b] to select either the distinguished "true" or "false" event
    according to [b].
*)
val boolean: bool -> t

(** Use [integer sign minor] to make a positive or negative integer event,
    according to [sign], with the value [minor].
*)
val integer: [< sign ] -> minor -> t

(** Use [float n] to make an IEEE 754 floating point number event with [n]. If
    [~precision] is not used, then the most precise form required to represent
    [n] without loss is selected.
*)
val float: ?precision:[< ieee754_precision ] -> float -> t

(** Use [octets s] to make a definite length octet sequence event comprising
    the octets in [s]. To make an indefinite length octet sequence event, use the
    [signal `Octets] constructor.
*)
val octets: string -> t

(** Use [text s] to make a definite length Unicode text sequence event
    comprising the UTF-8 encoded octets in [s]. If the octets in [s] do not
    comprise a valid UTF-8 encoding, then [Cf_decode.Invalid] is raised with
    the position of the invalid octet in [s]. To make an indefinite length
    Unicode text event, use the [signal `Text] constructor.
*)
val text: string -> t

(** Use [array minor] to make an event signaling the start of a definite length
    array with the length indicated by [minor]. To make an indefinite length
    array event, use the [signal `Array] constructor.
*)
val array: minor -> t

(** Use [map minor] to make an event signaling the start of a definite length
    map with the length indicated by [minor]. To make an indefinite length map
    event, use the [signal `Map] constructor.
*)
val map: minor -> t

(** Use [tag minor] to make a tagged value event. *)
val tag: minor -> t

(** {6 Utilities} *)

(** Equivalence relation *)
include Cf_relations.Equal with type t := t

(** Use [force_precision p n] to truncate [n] for the precision [p]. *)
val force_precision: [< ieee754_precision ] -> float -> float

(**/**)
val select_precision: float -> ieee754_precision
val unsafe_minor: int64 -> minor
val unsafe_text: string -> t
val unsafe_reserved: int -> t
val unsafe_undefined: string -> t
val width_octets: t -> int

(*--- End ---*)
