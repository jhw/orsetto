(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module E = struct
    type signal = [ `Break | `Octets | `Text | `Array | `Map ]
    type ieee754_precision = [ `Half | `Single | `Double ]

    type t =
        | Null
        | Signal of signal
        | Boolean of bool
        | Integer of int
        | Float of ieee754_precision * float
        | Octets of string
        | Text of string
        | Array of int
        | Map of int
        | Tag of int
        | Undefined of string

    let force_precision p n =
        match p with
        | `Half -> Cf_endian_core.(of_fp16_bits @@ to_fp16_bits_unsafe n)
        | `Single -> Int32.(float_of_bits @@ bits_of_float n)

    let select_precision n =
        if n = force_precision `Half n then
            `Half
        else if n = force_precision `Single n then
            `Single
        else
            `Double

    let of_float n =
        if Cf_endian_core.is_nan n then
            Float (`Half, nan)
        else
            Float (select_precision n, n)

    module BE = Cf_endian_big.Unsafe

    let decoder =
        let open Cf_decode in
        let rdcode s pos =
            let Position i = pos in
            let octet = BE.ldu8 s i in
            let major = octet lsr 5 and info = octet land 0x1f in
            let width =
                if info < 24 || (info == 31 && major >= 2 && major <> 6) then
                    0
                else if info < 28 then
                    1 lsl (info - 24)
                else begin
                    invalid pos begin
                        Printf.(sprintf "unrecognized info part %u" info)
                    end
                end
            in
            major, info, width
        in
        let rdminor s pos width =
            let Position i = pos in
            match width with
            | 1 ->
                BE.ldu8 s i
            | 2 ->
                BE.ldu16 s i
            | 4 ->
                if Sys.int_size < 32 then begin
                    let v = BE.ldi32 s i in
                    if v < 0l then
                        invalid pos "unsigned 32-bit integer too large";
                    Int32.to_int v
                end
                else
                    BE.ldu32 s i
            | 8 ->
                let v = BE.ldi64 s i in
                if v < 0L || v > Int64.of_int max_int then
                    invalid pos "unsigned 64-bit integer too large";
                Int64.to_int v
            | _ ->
                assert (not true);
                0
        in
        let rdoctets s pos width length =
            let Position i = pos in
            let start = i + width in
            String.sub s start length
        in
        let ckdone w2 need =
            let pos, sz = w2 in
            analyze sz need pos
        in
        let cktype w2 major minor need s pos =
            if major == 2 || major == 3 then begin
                if minor > Sys.max_string_length then
                    invalid pos "exceeded maximum string length";
                let pos', have = w2 and need = need + minor in
                let analyze = analyze have need in
                if major == 3 then begin
                    let Position i = pos in
                    let sl = Cf_slice.of_substring s i (i + minor) in
                    try Ucs_transport.UTF8.validate_slice sl with
                    | Invalid (Position adj, msg) ->
                        invalid (advance adj pos) msg
                end;
                analyze pos'
            end
            else
                ckdone w2 need
        in
        let ck s pos sz =
            let w2 = pos, sz in
            let major, info, width = rdcode s pos in
            let need = 1 + width and pos = advance 1 pos in
            let Size have = sz in
            if have < need || (info == 31 && (major >= 2 && major <> 6)) then
                ckdone w2 need
            else if major == 7 && width > 2 then begin
                let pos = advance width pos in
                cktype w2 major info need s pos
            end
            else if width > 0 then begin
                let minor = rdminor s pos width and pos = advance width pos in
                cktype w2 major minor need s pos
            end
            else
                cktype w2 major info need s pos
        and rd pos s =
            let major, info, width = rdcode s pos in
            let pos = advance 1 pos in
            if major < 7 then begin
                let minor = if width > 0 then rdminor s pos width else info in
                if major == 0 then
                    Integer minor
                else if major == 1 then
                    Integer ((-1) - minor)
                else if major == 2 then begin
                    if info == 31 then
                        Signal `Octets
                    else
                        Octets (rdoctets s pos width minor)
                end
                else if major == 3 then
                    if info == 31 then
                        Signal `Text
                    else
                        Text (rdoctets s pos width minor)
                else if major == 4 then begin
                    if info == 31 then Signal `Array else Array minor
                end
                else if major == 5 then begin
                    if info == 31 then Signal `Map else Map minor
                end
                else if major == 6 then
                    Tag minor
                else begin
                    assert (not true);
                    let Position i = pos in
                    Undefined (String.sub s (pred i) (succ width))
                end
            end
            else begin
                let Position i = pos in
                match info with
                | 20 ->
                    Boolean false
                | 21 ->
                    Boolean true
                | 22 ->
                    Null
                | 23 ->
                    Undefined ""
                | 24 ->
                    Undefined (String.sub s (pred i) 2)
                | 25 ->
                    Float (`Half, Cf_endian_core.of_fp16_bits @@ BE.ldu16 s i)
                | 26 ->
                    Float (`Single, Int32.float_of_bits @@ BE.ldi32 s i)
                | 27 ->
                    Float (`Double, Int64.float_of_bits @@ BE.ldi64 s i)
                | 31 ->
                    Signal `Break
                | _ ->
                    Undefined (String.sub s (pred i) 1)
            end
        in
        scheme 1 ck rd

    let encoder =
        let open Cf_encode in
        let max_uint32 = Int64.(to_int 0xffffffffL) in
        let intwidth n =
            match if n < 0 then (-1) - n else n with
            | u when u < 24 -> 0
            | u when u < 0x100 -> 1
            | u when u < 0x10000 -> 2
            | u when Sys.int_size < 32 || u <= max_uint32 -> 4
            | _ -> 8
        and floatwidth = function
            | `Half -> 2
            | `Single -> 4
            | `Double -> 8
        in
        let need = function
            | Integer n
            | Array n
            | Map n
            | Tag n ->
                intwidth n
            | Float (p, _) ->
                floatwidth p
            | Octets s
            | Text s ->
                let n = String.length s in
                intwidth n + n
            | Null
            | Boolean _
            | Signal _
            | Undefined _  ->
                0
        in
        let wrcode major quintet b i =
            (BE.stu8[@tailcall]) ((major lsl 5) lor quintet) b i
        in
        let putcode major quintet b i =
            wrcode major quintet b i;
            succ i
        in
        let intquintet n =
            if n < 24 then
                n
            else if n <= 0xff then
                24
            else if n <= 0xffff then
                25
            else if Sys.int_size < 32 || n <= max_uint32 then
                26
            else
                27
        and floatquintet = function
            | `Half -> 25
            | `Single -> 26
            | `Double -> 27
        and wrfloat n prec b i =
            match prec with
            | `Half ->
                let fp16 = Cf_endian_core.to_fp16_bits_unsafe n in
                (BE.stu16[@tailcall]) fp16 b i
            | `Single ->
                (BE.sti32[@tailcall]) (Int32.bits_of_float n) b i
            | `Double ->
                (BE.sti64[@tailcall]) (Int64.bits_of_float n) b i
        in
        let wrinfo quintet info b i =
            match quintet with
            | 24 -> (BE.stu8[@tailcall]) info b i;
            | 25 -> (BE.stu16[@tailcall]) info b i;
            | 26 -> (BE.stu32[@tailcall]) info b i;
            | 27 -> (BE.stu64[@tailcall]) info b i;
            | _ -> ()
        in
        let wroctets major s b i =
            let n = String.length s in
            let quintet = intquintet n in
            let i = putcode major quintet b i in
            wrinfo quintet n b i;
            let i =
                if quintet < 24 || quintet > 27 then
                    i
                else
                    i + 1 lsl (quintet - 24)
            in
            (Bytes.blit_string[@tailcall]) s 0 b i n
        in
        let ck _pos sz v =
            analyze sz (need v + 1) v
        and wr v b i =
            match v with
            | Null ->
                (wrcode[@tailcall]) 7 22 b i
            | Boolean v ->
                (wrcode[@tailcall]) 7 (if v then 21 else 20) b i
            | Undefined _ ->
                (wrcode[@tailcall]) 7 23 b i
            | Integer n when n < 0 ->
                let n = (-1) - n in
                let q5 = intquintet n in
                let i = putcode 1 q5 b i in
                (wrinfo[@tailcall]) q5 n b i
            | Integer n ->
                let q5 = intquintet n in
                let i = putcode 0 q5 b i in
                (wrinfo[@tailcall]) q5 n b i
            | Float (p, n) ->
                let i = putcode 7 (floatquintet p) b i in
                (wrfloat[@tailcall]) n p b i
            | Octets s ->
                (wroctets[@tailcall]) 2 s b i
            | Text s ->
                (wroctets[@tailcall]) 3 s b i
            | Array n ->
                let q5 = intquintet n in
                let i = putcode 4 q5 b i in
                (wrinfo[@tailcall]) q5 n b i
            | Map n ->
                let q5 = intquintet n in
                let i = putcode 5 q5 b i in
                (wrinfo[@tailcall]) q5 n b i
            | Tag n ->
                let q5 = intquintet n in
                let i = putcode 6 q5 b i in
                (wrinfo[@tailcall]) q5 n b i
            | Signal _ ->
                assert (not true);
                (wrcode[@tailcall]) 7 23 b i
        in
        scheme 1 ck wr
end

type t =
    | Null
    | Undefined of string
    | Boolean of bool
    | Integer of int
    | Float of float
    | Octets of string
    | Text of string
    | Array of t list
    | Map of (t * t) list
    | Tag of int * t

let rec equal a b = begin[@warning "-4"]
    match a, b with
    | Null, Null ->
        true
    | Undefined _, Undefined _ ->
        false
    | Boolean a, Boolean b ->
        a == b
    | Integer a, Integer b ->
        a == b
    | Float a, Float b ->
        Float.equal a b
    | Octets a, Octets b
    | Text a, Text b ->
        String.equal a b
    | Array a, Array b ->
        let a = List.to_seq a and b = List.to_seq b in
        Cf_seq.eqf equal a b
    | Map a, Map b ->
        let eq2 (a0, a1) (b0, b1) = equal a0 b0 && equal a1 b1 in
        let a = List.to_seq a and b = List.to_seq b in
        Cf_seq.eqf eq2 a b
    | Tag (na, a), Tag (nb, b) ->
        na == nb && equal a b
    | _ ->
        false
end

let rec compare a b = begin[@warning "-4"]
    match a, b with
    | Null, Null ->
        0
    | Null, _ -> (-1)
    | Undefined a, Undefined b ->
        let d = String.compare a b in
        if d <> 0 then d else (-1)
    | Undefined _, _ -> (-1)
    | Boolean a, Boolean b ->
        Bool.compare a b
    | Boolean _, _ -> (-1)
    | Integer a, Integer b ->
        Int.compare a b
    | Integer _, _ -> (-1)
    | Float a, Float b ->
        Float.compare a b
    | Float _, _ -> (-1)
    | Octets a, Octets b ->
        String.compare a b
    | Octets _, _ -> (-1)
    | Text a, Text b ->
        String.compare a b
    | Text _, _ -> (-1)
    | Array a, Array b ->
        let a = List.to_seq a and b = List.to_seq b in
        Cf_seq.cmpf compare a b
    | Array _, _ -> (-1)
    | Map a, Map b ->
        let cmp2 (a0, a1) (b0, b1) =
            let d = compare a0 b0 in
            if d <> 0 then d else compare a1 b1
        in
        let a = List.to_seq a and b = List.to_seq b in
        Cf_seq.cmpf cmp2 a b
    | Map _, _ -> (-1)
    | _, Map _ -> (1)
    | Tag (na, a), Tag (nb, b) ->
        let d = Int.compare na nb in
        if d <> 0 then d else compare a b
    | _ ->
        (1)
end

let null = Null

let boolean b = Boolean b
let integer i = Integer i
let float n = Float n
let octets s = Octets s

let text ?unsafe s =
    if unsafe = None then Ucs_transport.UTF8.validate_string s;
    Text s

let array s = Array s
let map s = Map s

let tag n v =
    if n < 0 then invalid_arg (__MODULE__ ^ ": invalid tag!");
    Tag (n, v)

let decoder =
    let module M = Cf_decode.Monad in
    let open M.Affix in
    let rec loop () =
        let* pos = Cf_decode.pos in
        E.decoder >>= do_event pos
    and do_event pos ev =
        match ev with
        | E.Tag n -> (do_tag[@tailcall]) n
        | E.Null -> M.return Null
        | E.Boolean b -> M.return @@ Boolean b
        | E.Integer n -> M.return @@ Integer n
        | E.Float (_, n) -> M.return @@ Float n
        | E.Octets s -> M.return @@ Octets s
        | E.Text s -> M.return @@ Text s
        | E.Array n -> (do_fixed_array[@tailcall]) [] n
        | E.Map n -> (do_fixed_map[@tailcall]) [] n
        | E.Undefined s -> M.return @@ Undefined s
        | E.Signal x -> (do_signal[@tailcall]) pos x
    and do_tag n =
        let+ v = loop () in Tag (n, v)
    and do_fixed_array stack = function
        | 0 ->
            M.return @@ Array (List.rev stack)
        | count ->
            let* element = loop () in
            (do_fixed_array[@tailcall]) (element :: stack) (pred count)
    and do_fixed_map stack = function
        | 0 ->
            M.return @@ Map (List.rev stack)
        | count ->
            let* key = loop () in
            let* value = loop () in
            let pair = key, value in
            (do_fixed_map[@tailcall]) (pair :: stack) (pred count)
    and do_signal pos = function
        | `Break -> (do_error[@tailcall]) pos "unexpected break event"
        | `Octets -> (do_indef_octets[@tailcall]) (Buffer.create 0)
        | `Text -> (do_indef_text[@tailcall]) (Buffer.create 0)
        | `Array -> (do_indef_array[@tailcall]) []
        | `Map -> (do_indef_map[@tailcall]) []
    and do_indef_octets b =
        let* pos = Cf_decode.pos in
        let* event = E.decoder in
        begin [@warning "-4"] match event with
        | E.Signal `Break ->
            M.return @@ Octets (Buffer.contents b)
        | E.Octets s ->
            Buffer.add_string b s;
            (do_indef_octets[@tailcall]) b
        | _ ->
            let msg = "unexpected event in indefinite length octets" in
            (do_error[@tailcall]) pos msg
        end
    and do_indef_text b =
        let* pos = Cf_decode.pos in
        let* event = E.decoder in
        begin [@warning "-4"] match event with
        | E.Signal `Break ->
            M.return @@ Text (Buffer.contents b)
        | E.Text s ->
            Buffer.add_string b s;
            (do_indef_text[@tailcall]) b
        | _ ->
            let msg = "unexpected event in indefinite length text" in
            (do_error[@tailcall]) pos msg
        end
    and do_indef_array stack =
        let* pos = Cf_decode.pos in
        let* event = E.decoder in
        begin [@warning "-4"] match event with
        | E.Signal `Break ->
            M.return @@ Array (List.rev stack)
        | event ->
            let* element = do_event pos event in
            (do_indef_array[@tailcall]) (element :: stack)
        end
    and do_indef_map stack =
        let* pos = Cf_decode.pos in
        let* event = E.decoder in
        begin [@warning "-4"] match event with
        | E.Signal `Break ->
            M.return @@ Map (List.rev stack)
        | event ->
            let* key = do_event pos event in
            let* value = loop () in
            let pair = key, value in
            (do_indef_map[@tailcall]) (pair :: stack)
        end
    and do_error pos msg =
        Cf_decode.invalid pos msg
    in
    loop ()

let encoder =
    let module M = Cf_encode.Monad in
    let open M.Affix in
    let rec loop ev =
        match ev with
        | Null ->
            M.seal E.encoder E.Null
        | Undefined s ->
            M.seal E.encoder @@ E.Undefined s
        | Boolean b -> M.seal E.encoder @@ E.Boolean b
        | Integer n -> M.seal E.encoder @@ E.Integer n
        | Float n -> M.seal E.encoder @@ E.of_float n
        | Octets s -> M.seal E.encoder @@ E.Octets s
        | Text s -> M.seal E.encoder @@ E.Text s
        | Array vs ->
            let* _ = M.seal E.encoder @@ E.Array (List.length vs) in
            M.serial @@ Seq.map loop @@ List.to_seq vs
        | Map kvs ->
            let* _ = M.seal E.encoder @@ E.Map (List.length kvs) in
            M.serial @@ Seq.map pair @@ List.to_seq kvs
        | Tag (n, v) ->
            let* _ = M.seal E.encoder @@ E.Tag n in
            (loop[@tailcall]) v
    and pair (k, v) =
        let* _ = loop k in
        (loop[@tailcall]) v
    in
    let Cf_encode.Size sz = Cf_encode.required_size E.encoder E.Null in
    M.eval ~sz loop

(*--- End ---*)
