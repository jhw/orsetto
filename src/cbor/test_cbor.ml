(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open OUnit2

let jout = Cf_journal.stdout

let _ =
    Printexc.record_backtrace true;
    Random.self_init ();
    jout#setlimit `Debug

(*
    let gc = Gc.get () in
    gc.Gc.verbose <- 0x1f;
    Gc.set gc

let _ = Gc.create_alarm begin fun () ->
    let min, pro, maj = Gc.counters () in
    jout#info "[Gc] minor=%f promoted=%f major=%f\n" min pro maj
end
*)

module type T_signature = sig
    val test: test
end

module T_horizon: T_signature = struct
    type bottom and 'a test = Test of 'a

    type _ Cf_type.nym +=
        | Container: 'a Cf_type.nym -> 'a test Cf_type.nym
        | Bottom: bottom Cf_type.nym

    let horizon = object(self)
        inherit Cbor_type.horizon as cbor

        method! equiv:
            type a b. a Cf_type.nym -> b Cf_type.nym -> (a, b) Cf_type.eq
                = fun a b ->
            match[@warning "-4"] a, b with
            | Container a, Container b ->
                let Cf_type.Eq = self#equiv a b in Cf_type.Eq
            | _ ->
                cbor#equiv a b
    end

    module Form = (val Cf_type.form horizon : Cf_type.Form)

    open Cf_type
    open Ucs_type
    open Cbor_type
    open! Form

    type case = Case: {
        name: string;
        nym: 'v nym;
        equal: 'v -> 'v -> bool;
        value: 'v;
        typerr: opaque list;
    } -> case

    let of_case (Case c) = c.name >:: begin fun ctxt ->
        let v = witness c.nym c.value in
        assert_bool "Cf_type.ck -> true" @@ ck c.nym v;
        assert_bool "Cf_type.ck -> false" @@ not @@ ck Bottom v;
        let _ =
            match opt c.nym v with
            | Some value ->
                assert_equal ~ctxt ~msg:"opt v -> Some" ~cmp:c.equal
                    c.value value
            | None ->
                assert_failure "opt v -> !Some"
        in
        let _ =
            match opt Bottom v with
            | Some _ -> assert_failure "opt v Bottom -> Some"
            | None -> ()
        in
        assert_equal ~ctxt ~msg:"req v = c.value" ~cmp:c.equal
            c.value (req c.nym v);
        let _ =
            match req Bottom v with
            | exception Type_error ->
                ()
            | exception x ->
                logf ctxt `Error "exn %s %s"
                    (Printexc.to_string x) (Printexc.get_backtrace ());
                assert_failure "req v Bottom -> exn?"
            | _ ->
                assert_failure "req v Bottom -> value!"
        in
        let f notv =
            assert_bool "ck notv -> true!" @@ not (ck c.nym notv)
        in
        List.iter f c.typerr
    end

    let test = "horizon" >::: List.map of_case [
        Case {
            name = "tag-unit";
            nym = Tag Unit;
            equal = (fun (na, ()) (nb, ()) -> Int64.equal na nb);
            value = 23L, ();
            typerr = [];
        };

        Case {
            name = "reserved";
            nym = Reserved;
            equal = Int.equal;
            value = 44;
            typerr = [];
        };

        Case {
            name = "tag-tag-unit";
            nym = Tag (Tag Unit);
            equal = begin fun a b ->
                let na1, (na2, ()) = a and nb1, (nb2, ()) = b in
                Int64.equal na1 nb1 && Int64.equal na2 nb2
            end;
            value = 23L, (99L, ());
            typerr = [ witness (Tag Unit) (5L, ()) ];
        };

        Case {
            name = "contained-tag-unit";
            nym = Container (Tag Unit);
            equal = begin fun (Test a) (Test b) ->
                let a, () = a and b, () = b in
                Int64.equal a b
            end;
            value = Test (55L, ());
            typerr = [ witness (Container Unit) @@ Test () ];
        };

        Case {
            name = "tag-text";
            nym = Tag Text;
            equal = begin fun (na, sa) (nb, sb) ->
                Int64.equal na nb && Ucs_text.equal sa sb
            end;
            value = 23L, Ucs_text.of_string "test";
            typerr = [ witness (Tag Char) (66L, 'x') ];
        };

        Case {
            name = "event-null";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.null;
            typerr = [];
        };

        Case {
            name = "event-bool";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.boolean true;
            typerr = [];
        };

        Case {
            name = "event-int";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.(integer `Positive @@ int_to_minor 0);
            typerr = [];
        };

        Case {
            name = "event-float";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.float 0.0;
            typerr = [];
        };

        Case {
            name = "event-signal";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.signal `Break;
            typerr = [];
        };

        Case {
            name = "event-octets";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.octets "test";
            typerr = [];
        };

        Case {
            name = "event-text";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.text "test";
            typerr = [];
        };

        Case {
            name = "event-tag";
            nym = Event;
            equal = Cbor_event.equal;
            value = Cbor_event.(tag @@ int_to_minor 0);
            typerr = [];
        };
    ]
end

module Aux_common = struct
    type case = Case of {
        octets: string;
        decode: unit Cbor_decode.t;
        encode: unit Cbor_encode.t;
        opaque: Cf_type.opaque;
        validate: Cf_type.opaque -> bool;
        flyweight: Cbor_flyweight.t;
        notation: string;
    }

    let to_octets s = String.of_seq @@ Seq.map Char.chr @@ List.to_seq s

    let reserved_additional_info = "\xfe"
    let reserved_simple = "\xf8\x1f"
    let undefined_simple = "\xf8\xff"

    let not_decode_reserved_additional_info (Case c) _ctxt =
        match Cbor_decode.of_string c.decode reserved_additional_info with
        | exception Not_found -> ()
        | exception (Cf_decode.Invalid _) -> ()
        | _ -> assert_failure "unexpected decode of reserved additional info"

    let not_decode_reserved_simple (Case c) _ctxt =
        match Cbor_decode.of_string c.decode reserved_simple with
        | exception Not_found -> ()
        | exception (Cf_decode.Invalid _) -> ()
        | _ -> assert_failure "unexpected decode of reserved simple"

    let not_decode_undefined_simple (Case c) _ctxt =
        match Cbor_decode.of_string c.decode undefined_simple with
        | exception Not_found -> ()
        | _ -> assert_failure "unexpected decode of undefined simple"

    let do_concrete_decode (Case c) _ctxt =
        let msg = "decoding error (concrete)" in
        try Cbor_decode.of_string c.decode c.octets with
        | Not_found -> assert_failure msg

    let do_concrete_encode (Case c) ctxt =
        let encoded = Cbor_encode.to_string c.encode in
        let msg = "encoding error (concrete)" in
        let printer s = Cf_base16.Std.encode_string s in
        let cmp = String.equal in
        assert_equal ~ctxt ~cmp ~msg ~printer c.octets encoded

    let do_opaque_decode (Case c) _ctxt =
        let msg = "decoding error (opaque)" in
        match Cbor_decode.(of_string (Opaque.value ())) c.octets with
        | exception Cbor_decode.Bad_syntax form ->
            let explain = Cbor_decode.Annot.dn form in
            assert_failure @@ Printf.sprintf "%s (%s)" msg explain
        | value ->
            assert_bool msg @@ c.validate @@ Cbor_decode.Annot.dn value

    let do_opaque_encode (Case c) ctxt =
        let encoded = Cbor_encode.(to_string @@ Opaque.value c.opaque) in
        let msg = "encoding error (opaque)" in
        let printer = Cbor_notation.of_string in
        let cmp = String.equal in
        assert_equal ~ctxt ~cmp ~msg ~printer c.octets encoded

    let do_flyweight_decode (Case c) ctxt =
        let msg = "decoding error (flyweight)" in
        match Cf_decode.of_string Cbor_flyweight.decoder c.octets with
        | Some decoded ->
            let cmp a b =
                Cbor_flyweight.equal a b || begin
                    match[@warning "-4"] a, b with
                    | Cbor_flyweight.Float a, Cbor_flyweight.Float b ->
                        Cf_endian_core.(is_nan a && is_nan b)
                    | _ ->
                        false
                end
            in
            assert_equal ~ctxt ~cmp ~msg decoded c.flyweight
        | None ->
            assert_failure msg

    let do_flyweight_encode (Case c) ctxt =
        let encoded = Cf_encode.to_string Cbor_flyweight.encoder c.flyweight in
        let msg = "encoding error (flyweight)" in
        let printer s = Cf_base16.Std.encode_string s in
        let cmp = String.equal in
        assert_equal ~ctxt ~cmp ~msg ~printer c.octets encoded

    let do_diagnostic (Case c) ctxt =
        let notation = Cbor_notation.of_string c.octets in
        let msg = "diagnostic notation mismatch" in
        let printer s = Printf.sprintf "\"%s\"" @@ String.escaped s in
        let cmp = String.equal in
        assert_equal ~ctxt ~cmp ~printer ~msg c.notation notation

    let do_case case ctxt =
        not_decode_reserved_additional_info case ctxt;
        not_decode_reserved_simple case ctxt;
        not_decode_undefined_simple case ctxt;
        do_concrete_decode case ctxt;
        do_concrete_encode case ctxt;
        do_opaque_decode case ctxt;
        do_opaque_encode case ctxt;
        do_flyweight_decode case ctxt;
        do_flyweight_encode case ctxt;
        do_diagnostic case ctxt

    let to_codes s = List.of_seq @@ Seq.map Char.code @@ String.to_seq s
    let of_codes s = String.of_seq @@ Seq.map Char.chr @@ List.to_seq s

    module Codes = struct
        type t = int list

        let equal a b =
            let a = List.to_seq (a : t) and b = List.to_seq (b : t) in
            Cf_seq.eqf (==) a b

        let show s =
            let b = Buffer.create 16 in
            Buffer.add_char b '[';
            let f i =
                Buffer.add_string b @@ Printf.sprintf "0x%02x" i;
                Buffer.add_char b ';'
            in
            List.iter f s;
            Buffer.add_char b ']';
            Buffer.contents b
    end
end

module T_literals: T_signature = struct
    open Aux_common

    let c_null = Case {
        octets = to_octets [ 0xf6 ];
        decode = begin
            let open Cbor_decode in
            let open Affix in
            let+ _ = null in ()
        end;
        encode = Cbor_encode.null;
        opaque = Cf_type.(witness Unit ());
        validate = Cbor_type.ck Cf_type.Unit;
        flyweight = Cbor_flyweight.null;
        notation = "null";
    }

    let c_boolean ~notation ~octets b = Case {
        octets = to_octets octets;
        decode = begin
            let open Cbor_decode in
            let open Affix in
            let* vl = boolean in
            let v = Annot.dn vl in
            if v == b then return () else nil
        end;
        encode = Cbor_encode.boolean b;
        opaque = Cf_type.(witness Bool b);
        validate = begin fun v ->
            match Cbor_type.opt Cf_type.Bool v with
            | None -> false
            | Some v -> v == b
        end;
        flyweight = Cbor_flyweight.boolean b;
        notation;
    }

    let c_true = c_boolean ~notation:"true" ~octets:[ 0xf5 ] true
    let c_false = c_boolean ~notation:"false" ~octets:[ 0xf4 ] false

    let c_integer ~octets i = Case {
        octets = to_octets octets;
        decode = begin
            let open Cbor_decode in
            let open Affix in
            let* vl = integer in
            let v = Annot.dn vl in
            if v == i then return () else nil
        end;
        encode = Cbor_encode.integer i;
        opaque = Cf_type.(witness Int i);
        validate = begin fun v ->
            match Cbor_type.opt Cf_type.Int v with
            | None -> false
            | Some v -> v == i
        end;
        flyweight = Cbor_flyweight.integer i;
        notation = Printf.sprintf "%d" i;
    }

    let c_zero = c_integer ~octets:[ 0 ] 0
    let c_one = c_integer ~octets:[ 1 ] 1
    let c_minus_one = c_integer ~octets:[ 0x20 ] (-1)

    let c_k10 = c_integer ~octets:[ 0x0a ] 10
    let c_k23 = c_integer ~octets:[ 0x17 ] 23
    let c_k24 = c_integer ~octets:[ 0x18; 0x18 ] 24
    let c_k25 = c_integer ~octets:[ 0x18; 0x19 ] 25
    let c_k100 = c_integer ~octets:[ 0x18; 0x64 ] 100
    let c_k1000 = c_integer ~octets:[ 0x19; 0x03; 0xe8 ] 1000
    let c_k1000000 = c_integer ~octets:[ 0x1a; 0x00; 0x0f; 0x42; 0x40 ] 1000000
    let c_kn10 = c_integer ~octets:[ 0x29 ] (-10)
    let c_kn25 = c_integer ~octets:[ 0x38; 0x18 ] (-25)
    let c_kn100 = c_integer ~octets:[ 0x38; 0x63 ] (-100)
    let c_kn1000 = c_integer ~octets:[ 0x39; 0x03; 0xe7 ] (-1000)

    let c_max_int31 =
        let int31 = pred (1 lsl 30) in
        let octets = [ 0x1a; 0x3f; 0xff; 0xff; 0xff ] in
        c_integer ~octets int31

    let c_min_int31 =
        let int31 = (-1) - (pred (1 lsl 30)) in
        let octets = [ 0x3a; 0x3f; 0xff; 0xff; 0xff ] in
        c_integer ~octets int31

    let c_max_int =
        let octets =
            [ 0x1b; 0x3f; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff ]
        in
        if Sys.int_size < 32 then c_max_int31 else c_integer ~octets max_int

    let c_min_int =
        let octets =
            [ 0x3b; 0x3f; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff ]
        in
        if Sys.int_size < 32 then c_min_int31 else c_integer ~octets min_int

    let c_float ?notation ~octets q = Case {
        octets = to_octets octets;
        decode = begin
            let module D = Cbor_decode in
            let open D.Affix in
            let* vl = D.float in
            let v = D.Annot.dn vl in
            if v = q || Cf_endian_core.(is_nan v && is_nan q) then
                D.return ()
            else
                D.nil
        end;
        encode = Cbor_encode.float q;
        opaque = Cf_type.(witness Float q);
        validate = begin fun v ->
            match Cbor_type.opt Cf_type.Float v with
            | None ->
                false
            | Some v ->
                v = q || Cf_endian_core.(is_nan v && is_nan q)
        end;
        flyweight = Cbor_flyweight.float q;
        notation =
            if Cf_endian_core.is_nan q then
                "NaN"
            else if classify_float q = FP_infinite && q > 0.0 then
                "Infinity"
            else if classify_float q = FP_infinite && q < 0.0 then
                "-Infinity"
            else begin
                match notation with
                | None -> Printf.sprintf "%.16g" q
                | Some s -> s
            end;
    }

    let c_fpzero = c_float ~notation:"0.0" ~octets:[ 0xf9; 0; 0 ] 0.0
    let c_fpnzero = c_float ~notation:"-0.0" ~octets:[ 0xf9; 0x80; 0 ] (-0.0)
    let c_fpk1 = c_float ~notation:"1.0" ~octets:[ 0xf9; 0x3c; 0 ] 1.0
    let c_fpkn1 = c_float ~notation:"-1.0" ~octets:[ 0xf9; 0xbc; 0 ] (-1.0)
    let c_fpnan = c_float ~octets:[ 0xf9; 0x7e; 0 ] nan
    let c_posinf = c_float ~octets:[ 0xf9; 0x7c; 0 ] infinity
    let c_neginf = c_float ~octets:[ 0xf9; 0xfc; 0 ] neg_infinity

    let c_fp1_1 =
        let octets =
            [ 0xfb; 0x3f; 0xf1; 0x99; 0x99; 0x99; 0x99; 0x99; 0x9a ]
        in
        c_float ~notation:"1.1" ~octets 1.1

    let c_fp1_5 = c_float ~octets:[ 0xf9; 0x3e; 0 ] 1.5

    let c_fp65504 =
        c_float ~notation:"65504.0" ~octets:[ 0xf9; 0x7b; 0xff ] 65504.0

    let c_fp10k =
        let notation = "100000.0" and octets = [ 0xfa; 0x47; 0xc3; 0x50; 0 ] in
        c_float ~notation ~octets 100000.0

    let c_fpmax32 =
        c_float ~octets:[ 0xfa; 0x7f; 0x7f; 0xff; 0xff ] 3.4028234663852886e+38

    let c_fp1e300 =
        let octets =
            [ 0xfb; 0x7e; 0x37; 0xe4; 0x3c; 0x88; 0x00; 0x75; 0x9c ]
        in
        c_float ~octets 1.0e+300

    let c_fp16epsilon =
        let octets = [ 0xf9; 0x00; 0x01 ] in
        let notation = "5.960464477539062e-08" in
        let epsilon = Cf_endian_core.of_fp16_bits 1 in
        c_float ~notation ~octets epsilon

    let c_fp16_smallest_normal =
        let octets = [ 0xf9; 0x04; 0x00 ] in
        let notation = "6.103515625e-05" in
        c_float ~notation ~octets 0.00006103515625

    let c_fp_n4e0 =
        let octets = [ 0xf9; 0xc4; 0 ] in
        let notation = "-4.0" in
        c_float ~notation ~octets (-4.0)

    let c_fp_n4p1e0 =
        let octets =
            [ 0xfb; 0xc0; 0x10; 0x66; 0x66; 0x66; 0x66; 0x66; 0x66 ]
        in
        let notation = "-4.1" in
        c_float ~notation ~octets (-4.1)

    let c_octets ~octets s = Case {
        octets = to_octets octets;
        decode = begin
            let module D = Cbor_decode in
            let open D.Affix in
            let* sl = D.octets () in
            let s' = D.Annot.dn sl in
            if String.equal s s' then D.return () else D.nil
        end;
        encode = Cbor_encode.octets s;
        opaque = Cf_type.(witness String s);
        validate = begin fun v ->
            match Cbor_type.opt Cf_type.String v with
            | None -> false
            | Some s' -> String.equal s s'
        end;
        flyweight = Cbor_flyweight.octets s;
        notation = Printf.sprintf "b64'%s'" (Cf_base64.Std.encode_string s);
    }

    let c_b64_zero = c_octets ~octets:[ 0x40 ] ""
    let c_b64_word = c_octets ~octets:[ 0x44; 1; 2; 3; 4 ] "\x01\x02\x03\x04"

    let c_text ?notation ~octets s = let text = Ucs_text.of_string s in Case {
        octets = to_octets octets;
        decode = begin
            let module D = Cbor_decode in
            let open D.Affix in
            let* textl = D.text () in
            let text' = D.Annot.dn textl in
            if Ucs_text.equal text text' then D.return () else D.nil
        end;
        encode = Cbor_encode.text text;
        opaque = Cf_type.witness Ucs_type.Text text;
        validate = begin fun v ->
            match Cbor_type.opt Ucs_type.Text v with
            | None -> false
            | Some text' -> Ucs_text.equal text text'
        end;
        flyweight = begin
            let Ucs_text.Octets s = text in
            Cbor_flyweight.text s;
        end;
        notation =
            Printf.sprintf "\"%s\"" begin
                match notation with
                | Some s -> s
                | None -> let Ucs_text.Octets s = text in s
            end
    }

    let c_txt_empty = c_text ~octets:[ 0x60 ] ""
    let c_txt_onechar = c_text ~octets:[ 0x61; 0x61 ] "a"
    let c_txt_ietf = c_text ~octets:[ 0x64; 0x49; 0x45; 0x54; 0x46 ] "IETF"

    let c_txt_2special =
        let octets = [ 0x62; 0x22; 0x5c ] in
        let notation = {|\"\\|} in
        c_text ~notation ~octets {|"\|}

    let c_txt_u0xfc =
        let octets = [ 0x62; 0xc3; 0xbc ] in
        let notation = "\\xfc" in
        let Ucs_text.Octets s =
            Ucs_text.of_seq @@ Seq.return @@ Uchar.of_int 0xfc
        in
        c_text ~notation ~octets s

    let c_txt_useq =
        let octets =
            [ 0x6A; 0x24; 0xC2; 0xA2; 0xE2; 0x82; 0xAC; 0xF0; 0x90; 0x8D;
              0x88 ]
        in
        let notation = {|$\xa2\u{20ac}\u{10348}|} in
        let Ucs_text.Octets s =
            Ucs_text.of_seq @@ Seq.map Uchar.of_int @@
                List.to_seq [ 0x24; 0xA2; 0x20AC; 0x10348 ]
        in
        c_text ~notation ~octets s

    let test = "literals" >::: [
        "null" >:: do_case c_null;
        "true" >:: do_case c_true;
        "false" >:: do_case c_false;
        "zero" >:: do_case c_zero;
        "one" >:: do_case c_one;
        "minus-one" >:: do_case c_minus_one;
        "k10" >:: do_case c_k10;
        "k23" >:: do_case c_k23;
        "k24" >:: do_case c_k24;
        "k25" >:: do_case c_k25;
        "k100" >:: do_case c_k100;
        "k1000" >:: do_case c_k1000;
        "k1000000" >:: do_case c_k1000000;
        "kn10" >:: do_case c_kn10;
        "kn25" >:: do_case c_kn25;
        "kn100" >:: do_case c_kn100;
        "kn1000" >:: do_case c_kn1000;
        "max_int31" >:: do_case c_max_int31;
        "min_int31" >:: do_case c_min_int31;
        "max_int" >:: do_case c_max_int;
        "min_int" >:: do_case c_min_int;
        "fpzero" >:: do_case c_fpzero;
        "fpnzero" >:: do_case c_fpnzero;
        "fpk1" >:: do_case c_fpk1;
        "fpkn1" >:: do_case c_fpkn1;
        "fpnan" >:: do_case c_fpnan;
        "posinf" >:: do_case c_posinf;
        "neginf" >:: do_case c_neginf;
        "k1.1" >:: do_case c_fp1_1;
        "k1.5" >:: do_case c_fp1_5;
        "k65504.0" >:: do_case c_fp65504;
        "fp10k" >:: do_case c_fp10k;
        "fpmax32" >:: do_case c_fpmax32;
        "fp1e300" >:: do_case c_fp1e300;
        "fp16epsilon" >:: do_case c_fp16epsilon;
        "fp16_smallest_normal" >:: do_case c_fp16_smallest_normal;
        "k-4.0" >:: do_case c_fp_n4e0;
        "k-4.1" >:: do_case c_fp_n4p1e0;
        "b64zero" >:: do_case c_b64_zero;
        "b64word" >:: do_case c_b64_word;
        "txtempty" >:: do_case c_txt_empty;
        "txt_a" >:: do_case c_txt_onechar;
        "txt_ietf" >:: do_case c_txt_ietf;
        "txt_2special" >:: do_case c_txt_2special;
        "txt_u0xfc" >:: do_case c_txt_u0xfc;
        "txt_useq" >:: do_case c_txt_useq;
    ]
end

module T_integers: T_signature = struct
    module E = Cbor_encode
    module D = Cbor_decode
    module A = D.Annot

    let roundtrip i =
        E.integer i |> E.to_string |> D.of_string D.integer |> A.dn

    let ck ~ctxt i = begin [@warning "-4"]
        match roundtrip i with
        | exception Not_found ->
            let msg = Printf.sprintf "Integer %d -> *error*" i in
            assert_failure msg
        | j ->
            let msg = Printf.sprintf "Integer %d -> Integer %d" i j in
            assert_equal ~ctxt ~msg i j
    end

    let rec loop ~ctxt n i =
        if n > 0 then begin
            let bit = if Random.bool () then 1 else 0 in
            let i = (i lsl 1) lor bit in
            ck ~ctxt i;
            loop ~ctxt (pred n) i
        end

    let test = "integers" >:: (fun ctxt -> loop ~ctxt 500 0)
end

module T_int64_edge: T_signature = struct
    module E = Cbor_encode
    module D = Cbor_decode
    module A = D.Annot

    type case = Case of {
        name: string;
        sign: Cbor_event.sign option;
        value: int64;
    }

    let cases = [
        Case {
            name = "min";
            sign = None;
            value = Int64.min_int;
        };
        Case {
            name = "max";
            sign = None;
            value = Int64.max_int;
        };
        Case {
            name = "positive-cbor-max";
            sign = Some `Positive;
            value = 0xFFFF_FFFF_FFFF_FFFFL;
        };
        Case {
            name = "positive-0x8000_0000_0000_0001";
            sign = Some `Positive;
            value = 0x8000_0000_0000_0001L;
        };
        Case {
            name = "negative-0x8000_0000_0000_0001";
            sign = Some `Negative;
            value = 0x8000_0000_0000_0001L;
        };
        Case {
            name = "negative-0xFFFF_FFFF_FFFF_FFFE";
            sign = Some `Negative;
            value = 0xFFFF_FFFF_FFFF_FFFEL;
        };
        Case {
            name = "negative-0xFFFF_FFFF_FFFF_FFFF";
            sign = Some `Negative;
            value = 0xFFFF_FFFF_FFFF_FFFFL;
        };
    ]

    let show_sign optsign =
        match optsign with
        | None -> "None"
        | Some `Positive -> "`Positive"
        | Some `Negative -> "`Negative"

    let show_pair (optsign, n64) =
        Printf.sprintf "Integer (%s, 0x%Lx)" (show_sign optsign) n64

    let show_octets s =
        let b = Buffer.create 32 in
        let each c =
            Buffer.add_string b @@ Printf.sprintf "%02x" @@ Char.code c
        in
        String.iter each s;
        Buffer.contents b

    let each (Case c) = c.name >:: fun ctxt ->
        let message = E.(to_string @@ int64 ?sign:c.sign c.value) in
        match[@warning "-4"] A.dn @@ D.(of_string int64 message) with
        | exception Not_found ->
            begin
                match A.dn @@ D.(of_string any message) with
                | exception Not_found ->
                    assert_failure "cannot decode event!"
                | Cbor_event.Integer (sign, Cbor_event.Minor value) ->
                    assert_failure begin
                        let pair = Some sign, value in
                        Printf.sprintf "cannot decode %s [%s]"
                            (show_pair pair) (show_octets message)
                    end
                | _ ->
                    assert_failure begin
                        Printf.sprintf "did not decode integer event! [%s]"
                            (show_octets message)
                    end
            end
        | sign, value ->
            let expected_sign =
                match c.sign with
                | None -> Some (if c.value < 0L then `Negative else `Positive)
                | sign -> sign
            in
            let expected = expected_sign, c.value in
            let decoded = Some sign, value in
            let msg = Printf.sprintf "data error [%s]" (show_octets message) in
            assert_equal ~ctxt ~msg ~printer:show_pair expected decoded

    let test = "int64-edge" >::: List.map each cases
end

module T_floats: T_signature = struct
    module E = Cbor_encode
    module D = Cbor_decode
    module A = D.Annot

    let roundtrip n =
        E.float n |> E.to_string |> D.of_string D.float |> A.dn

    let ck ~ctxt a = begin [@warning "-4"]
        match roundtrip a with
        | exception Not_found ->
            let msg = Printf.sprintf "Float %e -> *error*" a in
            assert_failure msg
        | b ->
            if not Cf_endian_core.(is_nan a && is_nan b) then begin
                let msg = Printf.sprintf "Float %e -> Float %e" a b in
                assert_equal ~ctxt ~msg a b
            end
    end

    let max_fp16 = 65504.0
    let max_fp32 = 3.402823466e+38
    let max_fp64 = max_float

    let rec loop ~ctxt n =
        if n > 0 then begin
            let prec, lim =
                match Random.int 3 with
                | 1 -> `Single, max_fp32
                | 2 -> `Double, max_fp64
                | v -> assert (v = 0); `Half, max_fp16
            in
            let lim = if Random.bool () then ~-.lim else lim in
            let a = Cbor_event.force_precision prec @@ Random.float lim in
            ck ~ctxt a;
            loop ~ctxt (pred n)
        end

    let test = "floats" >:: (fun ctxt -> loop ~ctxt 500)
end

module T_strings: T_signature = struct
    module E = Cbor_encode
    module D = Cbor_decode
    module A = D.Annot

    let roundtrip ~enc ~dec s =
        enc s |> E.to_string |> D.of_string dec |> A.dn

    let ck ~typ ~enc ~dec ~cmp s ctxt =
        match roundtrip ~enc ~dec s with
        | exception Not_found ->
            let msg = Printf.sprintf "%s _ -> *error*" typ in
            assert_failure msg
        | s' ->
            let msg = Printf.sprintf "%s _ -> %s _" typ typ in
            assert_equal ~ctxt ~msg ~cmp s s'

    let max_length = 256 * 1024

    let expand n =
        if n < 24 then
            succ n
        else if n < 255 then begin
            let n = n + Random.int (n / 16 + 2) in
            if n < 255 then n else 255
        end
        else if n == 255 then
            256
        else if n < 65535 then begin
            let n = n + Random.int n in
            if n < 65535 then n else 65535
        end
        else if n == 65535 then
            65536
        else if n < max_length then begin
            let n = n * 2 + Random.int n in
            if n < max_length then n else max_length
        end
        else begin
            assert (n == max_length);
            raise Not_found
        end

    let rec loop ~typ ~mk ~enc ~dec ~cmp n ctxt =
        let b = Buffer.create n in
        for _ = 0 to n do
            Buffer.add_char b @@ Char.chr @@ (+) 33 @@ Random.int 95
        done;
        let s = mk @@ Buffer.contents b in
        ck ~typ ~enc ~dec ~cmp s ctxt;
        match expand n with
        | exception Not_found -> ()
        | n -> loop ~typ ~mk ~enc ~dec ~cmp n ctxt

    let test = "string" >::: [
        begin
            let typ = "Octets" and enc = E.octets and dec = D.octets () in
            let mk s = s and cmp = String.equal in
            "octets" >:: loop ~typ ~mk ~enc ~dec ~cmp 1
        end;

        begin
            let typ = "Text" and enc = E.text and dec = D.text () in
            let mk = Ucs_text.of_string and cmp = Ucs_text.equal in
            "text" >:: loop ~typ ~mk ~enc ~dec ~cmp 1
        end;
    ]
end

module T_flyweight: T_signature = struct
    open Aux_common

    module M = Cbor_flyweight

    let roundtrip v =
        v |> Cf_encode.to_string M.encoder |> Cf_decode.of_string M.decoder

    let ck2way v codes ctxt =
        let cstr = Codes.show codes in
        let codes' = to_codes @@ Cf_encode.to_string M.encoder v in
        let cstr' = Codes.show codes' in
        let msg = Printf.sprintf "encoded %s -> %s" cstr cstr' in
        assert_equal ~ctxt ~cmp:Codes.equal ~msg codes codes';
        match Cf_decode.of_string M.decoder @@ of_codes codes with
        | None ->
            assert_failure (Printf.sprintf "cannot decode %s" cstr)
        | Some v' ->
            (*
            let vsh = show v and vsh' = show v' in
            let msg = Printf.sprintf "show-decoded %s -> %s" vsh vsh' in
            assert_equal ~ctxt ~cmp:String.equal ~msg vsh vsh';
            *)
            let msg = Printf.sprintf "decoded %s -> unexpected" cstr in
            begin [@warning "-4"] match v, v' with
            | M.Float a, M.Float b
              when Cf_endian_core.(is_nan a && is_nan b) ->
                ()
            | _, _ ->
                assert_equal ~ctxt ~cmp:M.equal ~msg v v'
            end

    let ck_decode_only v codes ctxt =
        let src = of_codes codes in
        let sxr = Cf_decode.string_scanner ?start:None src in
        match[@warning "-4"] sxr#scan M.decoder with
        | exception (Cf_decode.Invalid (pos, msg)) ->
            let cstr = Codes.show codes in
            let Cf_decode.Position p = pos in
            assert_failure begin
                Printf.sprintf "cannot decode %s as pos=%u (%s)" cstr p msg
            end
        | v' ->
            assert_equal ~ctxt ~msg:"decoded" ~cmp:M.equal v v'

    module T_integers: T_signature = struct
        let ck ~ctxt i = begin [@warning "-4"]
            match roundtrip @@ M.integer i with
            | None ->
                let msg = Printf.sprintf "Int %d -> *error*" i in
                assert_failure msg
            | Some (M.Integer j) ->
                let msg = Printf.sprintf "Int %d -> Int %d" i j in
                assert_equal ~ctxt ~msg i j
            | Some _ ->
                let msg = Printf.sprintf "Int %d -> _?_" i in
                assert_failure msg
        end

        let rec loop ~ctxt n i =
            if n > 0 then begin
                let bit = if Random.bool () then 1 else 0 in
                let i = (i lsl 1) lor bit in
                ck ~ctxt i;
                loop ~ctxt (pred n) i
            end

        let test = "random-ints" >:: (fun ctxt -> loop ~ctxt 500 0)
    end

    module T_floats: T_signature = struct
        let ck ~ctxt a = begin [@warning "-4"]
            match roundtrip @@ M.float a with
            | None ->
                let msg = Printf.sprintf "Float %e -> *error*" a in
                assert_failure msg
            | Some (M.Float b) ->
                if not Cf_endian_core.(is_nan a && is_nan b) then begin
                    let msg = Printf.sprintf "Float %e -> Float %e" a b in
                    assert_equal ~ctxt ~msg a b
                end
            | Some _ ->
                let msg = Printf.sprintf "Float %e -> _?_" a in
                assert_failure msg
        end

        let max_fp16 = 65504.0
        let max_fp32 = 3.402823466e+38
        let max_fp64 = max_float

        let rec loop ~ctxt n =
            if n > 0 then begin
                let prec, lim =
                    match Random.int 3 with
                    | 1 -> `Single, max_fp32
                    | 2 -> `Double, max_fp64
                    | v -> assert (v = 0); `Half, max_fp16
                in
                let lim = if Random.bool () then ~-.lim else lim in
                let a = Cbor_event.force_precision prec @@ Random.float lim in
                ck ~ctxt a;
                loop ~ctxt (pred n)
            end

        let test = "random-floats" >:: (fun ctxt -> loop ~ctxt 500)
    end

    module T_strings: T_signature = struct
        let max_length = 256 * 1024

        let expand n =
            if n < 24 then
                succ n
            else if n < 255 then begin
                let n = n + Random.int (n / 16 + 2) in
                if n < 255 then n else 255
            end
            else if n == 255 then
                256
            else if n < 65535 then begin
                let n = n + Random.int n in
                if n < 65535 then n else 65535
            end
            else if n == 65535 then
                65536
            else if n < max_length then begin
                let n = n * 2 + Random.int n in
                if n < max_length then n else max_length
            end
            else begin
                assert (n == max_length);
                raise Not_found
            end

        let rec loop n mk dn ctxt =
            let b = Buffer.create n in
            for _ = 0 to n do
                Buffer.add_char b @@ Char.chr @@ (+) 33 @@ Random.int 95
            done;
            let s = Buffer.contents b in
            let cbor = mk s in
            let cbor = Cf_encode.to_string M.encoder cbor in
            let s' = dn @@ Cf_decode.of_string M.decoder cbor in
            let msg = Printf.sprintf {|"%s" -> "%s"|} s s' in
            assert_equal ~ctxt ~msg ~cmp:String.equal s s';
            match expand n with
            | exception Not_found -> ()
            | n -> loop n mk dn ctxt

        let dn_octets = begin [@warning "-4"] function
            | Some (M.Octets s) -> s
            | _ -> assert_failure "Octets _ -> *error*"
        end

        let dn_text = begin [@warning "-4"] function
            | Some (M.Text s) -> s
            | _ -> assert_failure "Text _ -> *error*"
        end

        let test = "string" >::: [
            "octets" >:: loop 1 M.octets dn_octets;
            "text" >:: loop 1 (M.text ?unsafe:None) dn_text;
            "octets-indef" >:: begin
                let v = M.octets "ab" in
                let codes = [ 0x5f; 0x41; 0x61; 0x41; 0x62; 0xff ] in
                ck_decode_only v codes
            end;
            "text-indef" >:: begin
                let v = M.text ~unsafe:() "ab" in
                let codes = [ 0x7f; 0x61; 0x61; 0x61; 0x62; 0xff ] in
                ck_decode_only v codes
            end;
        ]
    end

    module T_arrays: T_signature = struct
        let v_empty = M.array []
        let v_null = M.array [ M.null ]
        let v_bools = M.array @@ List.map M.boolean @@ [ false; true ]
        let v_one = M.array [ v_empty ]

        let test = "array" >::: [
            "empty" >:: ck2way v_empty [ 0x80 ];
            "null" >:: ck2way v_null [ 0x81; 0xf6 ];
            "bools" >:: ck2way v_bools [ 0x82; 0xf4; 0xf5 ];
            "one" >:: ck2way v_one [ 0x81; 0x80 ];
        ]
    end

    module T_maps: T_signature = struct
        let v_empty = M.map []
        let v_null_null = M.map [ M.null, M.null ]

        let v_bool_int =
            let f (k, v) = M.boolean k, M.integer v in
            M.map @@ List.map f @@ [ false, 0; true, 1 ]

        let v_null_empty = M.map [ M.null, v_empty ]

        let test = "map" >::: [
            "empty" >:: ck2way v_empty [ 0xa0 ];
            "null->null" >:: ck2way v_null_null [ 0xa1; 0xf6; 0xf6 ];

            "bool->int" >::
                ck2way v_bool_int [ 0xa2; 0xf4; 0x00; 0xf5; 0x01 ];

            "null->empty" >:: ck2way v_null_empty [ 0xa1; 0xf6; 0xa0 ];
        ]
    end

    module T_tags: T_signature = struct
        let v_enc = M.tag 24 @@ M.null

        let test = "tag" >::: [
            "enc-null" >:: ck2way v_enc [ 0xd8; 0x18; 0xf6 ];
        ]
    end

    module T_undefined: T_signature = struct
        let ck_simple_undef ctxt =
            let codes = [ 0xf7 ] in
            let cstr = Codes.show codes in
            match[@warning "-4"]
                Cf_decode.of_string M.decoder @@ of_codes codes
            with
            | Some (M.Undefined s as v) ->
                let codes' = to_codes s in
                assert_equal ~ctxt ~msg:"decode" ~cmp:Codes.equal
                    ~printer:Codes.show [] codes';
                assert_bool "undefined not equal itself" (not (M.equal v v));
                assert_bool "undefined not equal null" (not (M.equal v M.null))
            | Some _ ->
                assert_failure "expected undefined value!"
            | None ->
                assert_failure (Printf.sprintf "cannot decode %s" cstr)

        let ck_reserved_simple_1 ctxt =
            let codes = [ 0xe1 ] in
            let cstr = Codes.show codes in
            match[@warning "-4"]
                Cf_decode.of_string M.decoder @@ of_codes codes
            with
            | Some (M.Undefined s) ->
                let codes' = to_codes s in
                assert_equal ~ctxt ~msg:"decode" ~cmp:Codes.equal
                    ~printer:Codes.show codes codes'
            | Some _ ->
                assert_failure "expected undefined value!"
            | None ->
                assert_failure (Printf.sprintf "cannot decode %s" cstr)

        let ck_reserved_simple_255 ctxt =
            let codes = [ 0xf8; 0xff ] in
            let cstr = Codes.show codes in
            match[@warning "-4"]
                Cf_decode.of_string M.decoder @@ of_codes codes
            with
            | Some (M.Undefined s) ->
                let codes' = to_codes s in
                assert_equal ~ctxt ~msg:"decode" ~cmp:Codes.equal
                    ~printer:Codes.show codes codes'
            | Some _ ->
                assert_failure "expected undefined value!"
            | None ->
                assert_failure (Printf.sprintf "cannot decode %s" cstr)

        let ck_bad_simple ctxt =
            let codes = [ 0xf8; 0x1f ] in
            let cstr = Codes.show codes in
            match[@warning "-4"]
                Cf_decode.of_string M.decoder @@ of_codes codes
            with
            | Some (M.Undefined s) ->
                let codes' = to_codes s in
                assert_equal ~ctxt ~msg:"decode" ~cmp:Codes.equal
                    ~printer:Codes.show codes codes'
            | Some _ ->
                assert_failure "expected undefined value!"
            | None ->
                assert_failure (Printf.sprintf "cannot decode %s" cstr)

        let ck_reserved_additional_info ctxt =
            let codes = [ 0xfe ] in
            let sxr = Cf_decode.string_scanner ?start:None @@ of_codes codes in
            match[@warning "-4"] sxr#scan M.decoder with
            | exception (Cf_decode.Invalid (pos, msg)) ->
                let Cf_decode.Position pos = pos in
                assert_equal ~ctxt ~msg:"decode invalid position" 0 pos;
                assert_equal ~ctxt ~msg:"invalid exception" ~cmp:String.equal
                    ~printer:Fun.id "unrecognized info part 30" msg
            | _ ->
                assert_failure "unexpected value scanned!"

        let ck_break ctxt =
            let codes = [ 0xff ] in
            let sxr = Cf_decode.string_scanner ?start:None @@ of_codes codes in
            match[@warning "-4"] sxr#scan M.decoder with
            | exception (Cf_decode.Invalid (pos, msg)) ->
                let Cf_decode.Position pos = pos in
                assert_equal ~ctxt ~msg:"decode invalid position"
                    ~printer:string_of_int 0 pos;
                assert_equal ~ctxt ~msg:"invalid exception" ~cmp:String.equal
                    ~printer:Fun.id "unexpected break event" msg
            | _ ->
                assert_failure "unexpected value scanned!"

        let test = "undefined" >::: [
            "simple-undef" >:: ck_simple_undef;
            "reserved-simple-1" >:: ck_reserved_simple_1;
            "reserved-simple-255" >:: ck_reserved_simple_255;
            "bad-simple" >:: ck_bad_simple;
            "reserved-additional-info" >:: ck_reserved_additional_info;
            "break" >:: ck_break;
        ]
    end

    let test = "flyweight" >::: [
        T_integers.test;
        T_floats.test;
        T_strings.test;
        T_arrays.test;
        T_maps.test;
        T_tags.test;
        T_undefined.test;
    ]
end

module Aux_expect = struct
    let opt_update_references =
        Conf.make_bool "update_references" false "Overwrite reference files."

    let readable_file path =
        try
            Unix.(access path [ R_OK ]);
            path
        with
        | Unix.Unix_error (error, _, _) ->
            let msg = Unix.error_message error in
            jout#fail "Unable to access %s (%s)" path msg

    let assert_expected_text ~ctxt fname text =
        if opt_update_references ctxt then begin
            let cout = Stdlib.open_out fname in
            Stdlib.output_string cout text;
            Stdlib.flush cout;
            Stdlib.close_out cout
        end
        else begin
            logf ctxt `Info "bracketing tmpfile";
            let result, cout = bracket_tmpfile ctxt in
            Stdlib.output_string cout text;
            Stdlib.flush cout;
            Stdlib.close_out cout;
            assert_command ~exit_code:(Unix.WEXITED 0) ~ctxt
                "diff" [ "-u"; fname; result ]
        end

    let path_to_case name = Printf.sprintf "test-cases/%s" name

    let assert_valid_text ~ctxt ~path ~file text =
        let filename = readable_file @@ path ^ "/" ^ file in
        assert_expected_text ~ctxt filename text

    let _show_octets message =
        let b = Buffer.create 16 in
        Buffer.add_char b '<';
        let first = ref true in
        let each c =
            if not !first then Buffer.add_char b ' ';
            first := false;
            Buffer.add_string b @@ Printf.sprintf "%02x" @@ Char.code c
        in
        String.iter each message;
        Buffer.add_char b '>';
        Buffer.contents b
end

module T_basic_expect: T_signature = struct
    open Aux_expect

    module D = Cbor_decode
    module E = Cbor_encode
    module N = Cbor_notation
    module Fly = Cbor_flyweight
    module B64 = Cf_base64.Mime

    open D.Affix

    type expect = Expect: {
        name: string;
        value: 'a;
        encode: 'a -> unit E.t;
        decode: 'a D.Annot.form D.t;
        toFly: 'a -> Fly.t;
        ofFly: Fly.t -> 'a option;
        equal: 'a -> 'a -> bool;
        printer: 'a -> string;
    } -> expect

    let assert_valid_encoding ~ctxt ~path ~case:(Expect c) =
        let message = E.to_string @@ c.encode c.value in
        let text = B64.encode_string ~brk:(70, "\r\n") ?np:None @@ message in
        assert_valid_text ~ctxt ~path ~file:"concise" text;
        message

    let assert_valid_decoding ~ctxt ~case:(Expect c) message =
        let value = D.of_string c.decode message in
        assert_equal ~ctxt ~msg:"decode error" ~cmp:c.equal ~printer:c.printer
            c.value (D.Annot.dn value);
        let D.Annot.Form form = value in
        assert_bool "no span on decoded form" (form.span <> None);
        let D.Annot.Span span = Option.get form.span in
        let Cf_decode.Position start = span.start in
        assert_equal ~ctxt ~msg:"start at zero" ~printer:Int.to_string 0 start;
        let Cf_decode.Position limit = span.limit in
        assert_equal ~ctxt ~msg:"limit at end" ~printer:Int.to_string
            (String.length message) limit

    let assert_valid_diagnostic ~ctxt ~path message =
        let text = N.of_string message in
        assert_valid_text ~ctxt ~path ~file:"diagnostic" text

    let assert_valid_flyweight_encoding ~ctxt ~path ~case:(Expect c) =
        let message = Cf_encode.to_string Fly.encoder @@ c.toFly c.value in
        let text = B64.encode_string ~brk:(70, "\r\n") ?np:None @@ message in
        assert_valid_text ~ctxt ~path ~file:"flyweight" text;
        message

    let assert_valid_flyweight_decoding ~ctxt ~case:(Expect c) message =
        let sxr = Cf_decode.string_scanner message in
        match sxr#scan Fly.decoder with
        | exception (Cf_decode.Invalid (pos, msg)) ->
            let Cf_decode.Position pos = pos in
            let msg = Printf.sprintf "decode invalid at %u (%s)" pos msg in
            assert_failure msg
        | exception (Cf_decode.Incomplete (_, need)) ->
            let msg = Printf.sprintf "incomplete message (need %u)" need in
            assert_failure msg
        | obj ->
            match c.ofFly obj with
            | None ->
                let diagnostic = N.of_string message in
                let msg = Printf.sprintf "unexpected object: %s" diagnostic in
                assert_failure msg
            | Some value ->
                assert_equal ~ctxt ~msg:"decompose error" ~cmp:c.equal
                    ~printer:c.printer c.value value

    let each (Expect c as case) = c.name >:: fun ctxt ->
        let path = path_to_case c.name in
        let message = assert_valid_encoding ~ctxt ~path ~case in
        assert_valid_decoding ~ctxt ~case message;
        assert_valid_diagnostic ~ctxt ~path message;
        let flyweight = assert_valid_flyweight_encoding ~ctxt ~path ~case in
        assert_valid_flyweight_decoding ~ctxt ~case flyweight

    let exp_empty_def_group kind =
        let name =
            let s = match kind with `Array -> "array" | `Map -> "map" in
            Printf.sprintf "empty-def-%s" s
        and value = ()
        and encode () = match kind with `Array -> E.array [] | `Map -> E.map []
        and decode = D.group ~b:0 kind @@ D.return ()
        and toFly () =
            match kind with  `Array -> Fly.array [] | `Map -> Fly.map []
        and ofFly obj =
            match[@warning "-4"] kind, obj with
            | `Array, Fly.Array []
            | `Map, Fly.Map [] ->
                Some ()
            | _ ->
                None
        and equal () () = true
        and printer () = "()"
        in Expect { name; value; encode; decode; toFly; ofFly; equal; printer }

    let exp_empty_indef_group kind =
        let name =
            let s = match kind with `Array -> "array" | `Map -> "map" in
            Printf.sprintf "empty-indef-%s" s
        and value = ()
        and encode () =
            match kind with
            | `Array -> E.array_seq Seq.empty
            | `Map -> E.map_seq Seq.empty
        and decode = D.group ~b:0 kind @@ D.return ()
        and toFly () =
            match kind with  `Array -> Fly.array [] | `Map -> Fly.map []
        and ofFly obj =
            match[@warning "-4"] kind, obj with
            | `Array, Fly.Array []
            | `Map, Fly.Map [] ->
                Some ()
            | _ ->
                None
        and equal () () = true
        and printer () = "()"
        in Expect { name; value; encode; decode; toFly; ofFly; equal; printer }

    let exp_option v =
        let name =
            Printf.sprintf "option-%s" @@
            match v with
            | None -> "none"
            | Some s -> "some-" ^ s
        and value = v
        and encode v =
            E.array @@
            match v with
            | None -> []
            | Some s -> [ E.text @@ Ucs_text.of_string s ]
        and decode =
            D.group ~b:1 `Array @@
            let+ vlopt = D.opt @@ D.text () in
            match vlopt with
            | None -> None
            | Some vl -> Some (let Ucs_text.Octets s = D.Annot.dn vl in s)
        and toFly v =
            Fly.array @@
            match v with
            | None -> []
            | Some s -> [ Fly.text s ]
        and ofFly obj =
            match[@warning "-4"] obj with
            | Fly.Array [] ->
                Some None
            | Fly.Array [ obj; ] -> begin
                match obj with
                | Fly.Text s -> Some (Some s)
                | _ -> None
              end
            | _ ->
                None
        and equal = Option.equal String.equal
        and printer v =
            match v with
            | None -> "None"
            | Some s -> Printf.sprintf "Some \"%s\"" @@ String.escaped s
        in Expect { name; value; encode; decode; toFly; ofFly; equal; printer }

    let exp_int_vector name vs =
        let length = List.length vs in
        let value = vs
        and encode vs = E.array @@ List.map E.integer @@ vs
        and decode =
            D.group ~a:length ~b:length `Array @@
                D.seq ~a:length ~b:length @@
                (D.integer >>: D.Annot.dn)
        and toFly vs = Fly.array @@ List.map Fly.integer vs
        and ofFly obj =
            match[@warning "-4"] obj with
            | Fly.Array objs -> begin
                let is_integer v =
                    match v with Fly.Integer _ -> true | _ -> false
                in
                let req_integer v =
                    match v with Fly.Integer n -> Some n | _ -> None
                in
                if List.for_all is_integer objs then
                    Some (List.filter_map req_integer objs)
                else
                    None
              end
            | _ ->
                None
        and equal = Stdlib.( = )
        and printer vs =
            match vs with
            | [] ->
                "[]"
            | hd :: tl ->
                let b = Buffer.create 16 in
                Buffer.add_string b "[ ";
                Buffer.add_string b @@ Printf.sprintf "%d" hd;
                let f v = Buffer.add_string b @@ Printf.sprintf "; %d" v in
                List.iter f tl;
                Buffer.add_string b " ]";
                Buffer.contents b
        in Expect { name; value; encode; decode; toFly; ofFly; equal; printer }

    module Text_map = Cf_rbtree.Map.Create(Ucs_text)

    let exp_text_float_table name precision pairs =
        let length = Array.length pairs in
        let value =
            let each (key, value) = Ucs_text.of_string key, value in
            Text_map.of_seq @@ Seq.map each @@ Array.to_seq pairs
        and encode v =
            let each (key, value) = E.text key, E.float ~precision value in
            E.map ~sort:() @@
            List.of_seq @@
            Seq.map each @@
            Text_map.to_seq_incr v
        and decode =
            let* pairsl =
                D.group ~a:length ~b:length `Map @@
                D.seq ~a:length ~b:length @@ begin
                    let* key = D.text () in
                    let* value = D.float in
                    D.return (D.Annot.dn key, D.Annot.dn value)
                end
            in
            let v = Text_map.of_seq @@ List.to_seq @@ D.Annot.dn pairsl in
            D.return @@ D.Annot.mv v pairsl
        and toFly v =
            let each (Ucs_text.Octets key, value) =
                Fly.text key, Fly.float value
            in
            Fly.map @@ List.of_seq @@ Seq.map each @@ Text_map.to_seq_incr v
        and ofFly obj =
            match[@warning "-4"] obj with
            | Fly.Map pairs ->
                let cktype (key, value) =
                    match key, value with
                    | Fly.Text _, Fly.Float _ -> true
                    | _ -> false
                and reqtype (key, value) =
                    match key, value with
                    | Fly.Text key, Fly.Float value ->
                        Some (Ucs_text.of_string key, value)
                    | _ -> None
                in
                if List.for_all cktype pairs then begin
                    let pairs = List.filter_map reqtype pairs in
                    Some (Text_map.of_seq @@ List.to_seq pairs)
                end
                else
                    None
            | _ ->
                None
        and equal a b =
            let a = Text_map.to_seq_incr a and b = Text_map.to_seq_incr b in
            let each (k1, v1) (k2, v2) =
                Ucs_text.equal k1 k2 && Float.equal v1 v2
            in
            Cf_seq.eqf each a b
        and printer v =
            let b = Buffer.create 16 in
            Buffer.add_char b '{';
            let each (key, value) =
                Buffer.add_char b ' ';
                let Ucs_text.Octets key = key in
                Buffer.add_string b key;
                Buffer.add_string b ": ";
                Buffer.add_string b @@ Printf.sprintf "%f" value
            in
            Seq.iter each @@ Text_map.to_seq_incr v;
            if not (Text_map.empty v) then Buffer.add_char b ' ';
            Buffer.add_char b '}';
            Buffer.contents b
        in Expect { name; value; encode; decode; toFly; ofFly; equal; printer }

    let test = "basic-expect" >::: List.map each [
        exp_empty_def_group `Array;
        exp_empty_indef_group `Array;
        exp_option None;
        exp_option @@ Some "foo";
        exp_int_vector "int-vector-one" [ 33 ];
        exp_int_vector "int-vector-five" [ 2; -3; 500; -800; 123456 ];
        exp_empty_def_group `Map;
        exp_empty_indef_group `Map;
        exp_text_float_table "text-half-table" `Half [| "x", 0.5; "y", 2.0 |];
        exp_text_float_table "text-single-table" `Single [|
            "foo", Float.of_int 0x12345;
            "bar", Float.of_int 0x23456;
            "baz", Float.of_int 0x34567;
        |];
        exp_text_float_table "text-double-table" `Double [|
            "zuux", Int64.to_float 0x123456789L;
            "frob", Int64.to_float 0x987654321L;
        |];
    ]
end

module T_advanced_expect: T_signature = struct
    open Aux_expect

    module D = Cbor_decode
    module E = Cbor_encode

    open D.Affix

    type case = Case: {
        name: string;
        value: 'a;
        equal: 'a -> 'a -> bool;
        printer: 'a -> string;
        encode: 'a -> unit E.t;
        decode: 'a D.Annot.form D.t;
    } -> case

    let indef_octets =
        let name = "indef-octets"
        and value = "These are indefinite length octets!"
        and equal = String.equal
        and printer s = Printf.sprintf "\"%s\"" @@ String.escaped s
        and encode v =
            let words = String.split_on_char ' ' v in
            let hd = List.hd words and tl = List.tl words in
            let tl = List.map (fun s -> " " ^ s) tl in
            E.octets_seq @@
            List.to_seq (hd :: tl)
        and decode = D.octets ()
        in Case { name; value; equal; printer; encode; decode }

    let indef_text =
        let name = "indef-text"
        and value = "This is an indefinite length text!"
        and equal = String.equal
        and printer s = Printf.sprintf "\"%s\"" @@ String.escaped s
        and encode v =
            let words = String.split_on_char ' ' v in
            let hd = List.hd words and tl = List.tl words in
            let tl = List.map (fun s -> " " ^ s) tl in
            E.text_seq @@
            Seq.map Ucs_text.of_string @@
            List.to_seq (hd :: tl)
        and decode =
            let* textl = D.text () in
            let Ucs_text.Octets s = D.Annot.dn textl in
            D.return @@ D.Annot.mv s textl
        in Case { name; value; equal; printer; encode; decode }

    let cases = [|
        indef_octets;
        indef_text;
    |]

    let assert_valid_encoding ~ctxt ~path ~case:(Case c) =
        let message = E.to_string @@ c.encode c.value in
        let text =
            Cf_base64.Mime.encode_string ~brk:(70, "\r\n") ?np:None @@ message
        in
        assert_valid_text ~ctxt ~path ~file:"concise" text;
        message

    let assert_valid_decoding ~ctxt ~case:(Case c) message =
        let value = D.of_string c.decode message in
        assert_equal ~ctxt ~msg:"decode error" ~cmp:c.equal ~printer:c.printer
            c.value (D.Annot.dn value);
        let D.Annot.Form form = value in
        assert_bool "no span on decoded form" (form.span <> None);
        let D.Annot.Span span = Option.get form.span in
        let Cf_decode.Position start = span.start in
        assert_equal ~ctxt ~msg:"start at zero" ~printer:Int.to_string 0 start;
        let Cf_decode.Position limit = span.limit in
        assert_equal ~ctxt ~msg:"limit at end" ~printer:Int.to_string
            (String.length message) limit

    let assert_valid_diagnostic ~ctxt ~path message =
        let text = Cbor_notation.of_string message in
        assert_valid_text ~ctxt ~path ~file:"diagnostic" text

    let each (Case c as case) = c.name >:: fun ctxt ->
        let path = path_to_case c.name in
        let message = assert_valid_encoding ~ctxt ~path ~case in
        assert_valid_decoding ~ctxt ~case message;
        assert_valid_diagnostic ~ctxt ~path message

    let test = "advanced-expect" >::: List.map each @@ Array.to_list cases
end

module T_data_model: T_signature = struct
    open Aux_expect

    type case = Case: {
        name: string;
        value: 'a;
        equal: 'a -> 'a -> bool;
        printer: 'a -> string;
        render: 'a Cf_data_render.model;
        ingest: 'a Cf_data_ingest.model;
    } -> case

    let assert_valid_encoding ~ctxt ~path ~case:(Case c) =
        let scheme = Cbor_encode.Render.scheme c.render in
        let message = Cf_encode.to_string scheme c.value in
        let text =
            Cf_base64.Mime.encode_string ~brk:(70, "\r\n") ?np:None @@ message
        in
        assert_valid_text ~ctxt ~path ~file:"concise" text;
        message

    let assert_valid_decoding ~ctxt ~case:(Case c) message =
        let scan = Cbor_decode.Ingest.scan c.ingest in
        match Cbor_decode.of_slice scan @@ Cf_slice.of_string message with
        | exception Not_found ->
            assert_failure "unrecognized by data ingestor"
        | exception (Cbor_decode.(Bad_syntax (Annot.Form msgl))) ->
            let Cbor_decode.Annot.Span span = Option.get msgl.span in
            let Cf_decode.Position start = span.start in
            let Cf_decode.Position limit = span.limit in
            let msg = Printf.sprintf "%u-%u: %s" start limit msgl.value in
            assert_failure msg
        | value ->
            let module Annot = Cbor_decode.Annot in
            assert_equal ~ctxt ~msg:"decode error" ~cmp:c.equal
                ~printer:c.printer c.value (Annot.dn value);
            let Annot.Form form = value in
            assert_bool "no span on decoded form" (form.span <> None);
            let Annot.Span span = Option.get form.span in
            let Cf_decode.Position start = span.start in
            assert_equal ~ctxt ~msg:"start at zero" ~printer:Int.to_string
                0 start;
            let Cf_decode.Position limit = span.limit in
            assert_equal ~ctxt ~msg:"limit at end" ~printer:Int.to_string
                (String.length message) limit

    let assert_valid_diagnostic ~ctxt ~path message =
        let text = Cbor_notation.of_string message in
        assert_valid_text ~ctxt ~path ~file:"diagnostic" text

    let each (Case c as case) = c.name >:: fun ctxt ->
        let path = path_to_case c.name in
        let message = assert_valid_encoding ~ctxt ~path ~case in
        assert_valid_decoding ~ctxt ~case message;
        assert_valid_diagnostic ~ctxt ~path message

    let vector_case ~name ~element value =
        let length = Array.length value in
        let equal = Stdlib.( = )
        and printer v =
            let b = Buffer.create 16 in
            Buffer.add_char b '[';
            let each i = Buffer.add_string b @@ Printf.sprintf " %Ld;" i in
            Array.iter each v;
            if Array.length v > 0 then Buffer.add_char b ' ';
            Buffer.add_char b ']';
            Buffer.contents b
        and render =
            let open Cf_data_render in
            vector @@ primitive element
        and ingest =
            let open Cf_data_ingest in
            vector ~a:length ~b:length @@ primitive element
        in Case { name; value; equal; printer; render; ingest }

    let int64_vector =
        vector_case ~name:"int64-array" ~element:Cf_type.Int64 [|
            2L;
            -3L;
            500L;
            -800L;
            123456L;
            Int64.of_int32 Int32.min_int;
            0xaabb_ccdd_eeffL;
            Int64.min_int;
        |]

    module Int32_map = Cf_rbtree.Map.Create(Int32)

    let int32_table_case ~name ~element input =
        let key = Cf_type.Int32 and cmp = Int32.compare in
        let length = Array.length input in
        let value =
            let each (k, v) = k, Ucs_text.of_string v in
            Int32_map.of_seq @@ Seq.map each @@ Array.to_seq input
        and equal a b =
            let a = Int32_map.to_seq_incr a and b = Int32_map.to_seq_incr b in
            let each (ka, va) (kb, vb) =
                Int32.equal ka kb && Ucs_text.equal va vb
            in
            Cf_seq.eqf each a b
        and printer v =
            let b = Buffer.create 16 in
            Buffer.add_char b '[';
            let each (k, v) =
                let Ucs_text.Octets v = v in
                Buffer.add_string b @@ Printf.sprintf " %ld, \"%s\";" k v
            in
            Seq.iter each @@ Int32_map.to_seq_incr v;
            if not (Int32_map.empty v) then Buffer.add_char b ' ';
            Buffer.add_char b ']';
            Buffer.contents b
        and render =
            let open Cf_data_render in
            let index = new Cbor_encode.Render.deterministic_index key in
            let f v = Array.of_seq @@ Int32_map.to_seq_incr v in
            cast f @@ table index @@ primitive element
        and ingest =
            let open Cf_data_ingest in
            let index = new index ~cmp key in
            let f v = Int32_map.of_seq @@ Array.to_seq v in
            cast f @@ table ~a:length ~b:length index @@ primitive element
        in Case { name; value; equal; printer; render; ingest }

    let int32_text_table =
        let element = Ucs_type.Text in
        int32_table_case ~name:"int32-text-table" ~element [|
            10l, "ten";
            -20l, "minus twenty";
            500l, "five hundred";
            -800l, "minus eight hundred";
            Int32.max_int, "maximum 32-bit integer";
            Int32.min_int, "minimum 32-bit integer";
        |]

    let tagged_record =
        let module R = struct
            type t = R of {
                b: bool;
                i: int;
                s: string;
                f: float;
            }
        end in
        let open R in
        let name = "tagged-record"
        and value = R { b=true; i=5; s="octets"; f=0.5 }
        and equal = Stdlib.( = )
        and printer (R r) = Printf.sprintf "(%b, %d, %s, %f)" r.b r.i r.s r.f
        and render =
            let open Cf_data_render in
            let open Affix in
            let open Cbor_encode.Render in
            tagged 4000L @@ record [|
                primitive Cf_type.Unit $= ();
                primitive Cf_type.Bool $: (fun (R r) -> r.b);
                primitive Cf_type.Int $: (fun (R r) -> r.i);
                primitive Cf_type.String $: (fun (R r) -> r.s);
                primitive Cf_type.Float $: (fun (R r) -> r.f);
            |]
        and ingest =
            let open Cf_data_ingest in
            let open Affix in
            let open Cbor_decode.Ingest in
            let compose () b i s f = R { b; i; s; f}
            and group =
                !: (primitive Cf_type.Unit) @>
                !: (primitive Cf_type.Bool) @>
                !: (primitive Cf_type.Int) @>
                !: (primitive Cf_type.String) @>
                !: (primitive Cf_type.Float) @>
                nil
            in
            tagged 4000L @@ record group compose
        in Case { name; value; equal; printer; render; ingest }

    let opaque_case ~name ~pack ~unpack value =
        let equal = String.equal
        and printer s = Printf.sprintf "\"%s\"" @@ String.escaped s
        and render =
            let open Cf_data_render in
            cast pack @@ primitive Cf_type.Opaque
        and ingest =
            let open Cf_data_ingest in
            cast unpack @@ primitive Cf_type.Opaque
        in Case { name; value; equal; printer; render; ingest }

    let opaque_triple =
        let triple a b c = a, b, c in
        let name = "opaque-triple"
        and value = "true 9223372036854775807 0.250000" in
        let pack v =
            let b, i64, fp = Scanf.sscanf v "%b %Ld %f" triple in
            let b = Cf_type.witness Cf_type.Bool b in
            let i64 = Cf_type.witness Cf_type.Int64 i64 in
            let fp = Cf_type.witness Cf_type.Float fp in
            Cf_type.witness Cf_type.(Seq Opaque) @@ List.to_seq [ b; i64; fp ]
        and unpack v =
            let s = List.of_seq @@ Cf_type.(req (Seq Opaque) v) in
            let b = List.nth s 0 in
            let i64 = List.nth s 1 in
            let fp = List.nth s 2 in
            let b = Cf_type.(req Bool b) in
            let i64 = Cf_type.(req Int64 i64) in
            let fp = Cf_type.(req Float fp) in
            Printf.sprintf "%b %Ld %f" b i64 fp
        in opaque_case ~name ~pack ~unpack value

    let test = "data-model" >::: List.map each [
        int64_vector;
        int32_text_table;
        tagged_record;
        opaque_triple;
    ]
end

let all = "t" >::: [
    T_horizon.test;
    T_literals.test;
    T_integers.test;
    T_int64_edge.test;
    T_floats.test;
    T_strings.test;
    T_flyweight.test;
    T_basic_expect.test;
    T_advanced_expect.test;
    T_data_model.test;
]

let _ = run_test_tt_main all

(*--- End ---*)
