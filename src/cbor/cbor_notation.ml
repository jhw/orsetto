(*---------------------------------------------------------------------------*
  Copyright (C) 2020-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type 'a t = 'a Cf_emit.To_formatter.t

type frame = Frame of {
    index: int;
    limit: int;
    infix: char;
    close: char;
    depth: int;
}

let push ?(index = 0) ?(limit = (-1)) ?(infix = ',') ~close st =
    let depth = match st with Frame s :: _ -> s.depth | [] -> 0 in
    Frame { index; limit; infix; close; depth } :: st

let events =
    let rec unwind pp st =
        match st with
        | Frame _ :: st -> unwind pp st
        | [] -> []
    in
    let error pp msg st =
        Format.fprintf pp "* %s" msg;
        unwind pp st
    in
    let pp_infix pp (Frame s) =
        match s.infix with
        | ':' ->
            Format.pp_print_char pp s.infix;
            Format.pp_print_char pp ' ';
            succ s.index, ','
        | ',' when s.close = '!' ->
            assert (s.limit < 0);
            assert (s.depth = 1);
            if s.index > 0 then begin
                Format.pp_print_char pp s.infix;
                Format.pp_print_space pp ();
            end;
            succ s.index, ','
        | ',' ->
            if s.index > 0 && (s.limit < 0 || s.index < s.limit) then begin
                Format.pp_print_char pp s.infix;
                Format.pp_print_space pp ();
            end;
            if s.close = '}' then s.index, ':' else succ s.index, ','
        | c ->
            assert (c = '_');
            succ s.index, s.infix
    and pp_enter pp st =
        match st with
        | [] ->
            []
        | Frame s :: st0 ->
            match
                match s.close with
                | '}' -> Some '{'
                | ']' -> Some '['
                | ')' -> Some '('
                | c ->
                    assert (c = '!' && st0 = []);
                    None
            with
            | None ->
                assert (st0 = []);
                assert (s.depth = 0);
                Format.pp_open_hvbox pp 0;
                assert (not (Format.pp_over_max_boxes pp ()));
                Frame { s with depth = succ s.depth } :: st0
            | Some c ->
                assert (st0 != []);
                assert (s.index = 0);
                Format.pp_print_char pp c;
                if s.limit < 0 then begin
                    Format.pp_print_char pp '_';
                    Format.pp_print_break pp 1 2
                end
                else begin
                    Format.pp_print_break pp 0 2
                end;
                let depth = succ s.depth in
                Format.pp_open_hvbox pp 0;
                assert (not (Format.pp_over_max_boxes pp ()));
                assert (depth == List.length st0 + 1);
                Frame { s with depth } :: st0
    and pp_leave pp st =
        match st with
        | [] ->
            []
        | Frame s :: st ->
            if s.close <> '!' then begin
                assert (st <> []);
                assert (s.depth > 1);
                Format.pp_close_box pp ();
                Format.pp_print_cut pp ();
                Format.pp_print_char pp s.close;
                let depth = pred s.depth in
                let Frame s = List.hd st and st = List.tl st in
                assert (List.length st + 1 = depth);
                Frame { s with depth } :: st
            end
            else begin
                assert (st == []);
                assert (s.depth == 1);
                Format.pp_close_box pp ();
                []
            end
    in
    let rec affix pp st =
        match st with
        | [] ->
            assert (not true);
            []
        | _ :: [] ->
            st
        | s0 :: st0 ->
            let Frame s = s0 in
            if s.infix = ':' || s.limit < 0 || s.index < s.limit then begin
                let index, infix = pp_infix pp s0 in
                Frame { s with index; infix } :: st0
            end
            else begin
                let st = pp_leave pp st in
                (affix[@tailcall]) pp st
            end
    and final pp st =
        match st with
        | [] ->
            assert (not true);
            []
        | _ :: [] ->
            st
        | Frame s :: _ ->
            if s.infix = ':' || s.limit < 0 || s.index < s.limit then
                error pp "incomplete container" st
            else begin
                let st = pp_leave pp st in
                (final[@tailcall]) pp st
            end
    in
    let rec loop st pp evs =
        match evs () with
        | exception Cf_decode.Invalid (_, msg) ->
            let st = affix pp st in
            error pp (Printf.sprintf "invalid: %s" msg) st
        | exception Cf_decode.Incomplete _ ->
            let st = affix pp st in
            error pp "incomplete event" st
        | Seq.Nil ->
            final pp st
        | Seq.Cons (ev, evs) ->
            match ev with
            | Cbor_event.Null ->
                ev_null pp st evs
            | Cbor_event.Signal signal ->
                ev_signal pp st evs signal
            | Cbor_event.Boolean b ->
                ev_boolean pp st evs b
            | Cbor_event.Integer (sign, minor) ->
                ev_integer pp st evs sign minor
            | Cbor_event.Float (_, n) ->
                ev_float pp st evs n
            | Cbor_event.Octets s ->
                ev_octets pp st evs s
            | Cbor_event.Text s ->
                ev_text pp st evs s
            | Cbor_event.Array minor ->
                ev_array pp st evs minor
            | Cbor_event.Map minor ->
                ev_map pp st evs minor
            | Cbor_event.Tag minor ->
                ev_tag pp st evs minor
            | Cbor_event.Reserved number ->
                ev_reserved pp st evs number
            | Cbor_event.Undefined _ ->
                error pp "undefined coding" st
    and ev_null pp st evs =
        let st = affix pp st in
        Format.pp_print_string pp "null";
        loop st pp evs
    and ev_signal pp st evs signal =
        match signal with
        | `Text
        | `Octets ->
            let st = affix pp st in
            let st = pp_enter pp @@ push ~close:')' st in
            (loop[@tailcall]) st pp evs
        | `Array ->
            let st = affix pp st in
            let st = pp_enter pp @@ push ~close:']' st in
            (loop[@tailcall]) st pp evs
        | `Map ->
            let st = affix pp st in
            let st = pp_enter pp @@ push ~close:'}' st in
            (loop[@tailcall]) st pp evs
        | `Break ->
            (ev_break[@tailcall]) pp st evs
    and ev_break pp st evs =
        match st with
        | [] ->
            assert (not true);
            []
        | Frame s :: _ ->
            if s.infix = ':' then
                error pp "unexpected break" st
            else begin
                let st = pp_leave pp st in
                if s.limit >= 0 && s.index = s.limit then
                    ev_break pp st evs
                else
                    (loop[@tailcall]) st pp evs
            end
    and ev_boolean pp st evs b =
        let st = affix pp st in
        Format.pp_print_string pp (if b then "true" else "false");
        loop st pp evs
    and ev_integer pp st evs sign minor =
        let st = affix pp st in
        let Cbor_event.Minor n = minor in
        match sign with
        | `Positive ->
            Format.fprintf pp "%Lu" n;
            loop st pp evs
        | `Negative ->
            if n = Int64.max_int then begin
                Format.pp_print_string pp "-9223372036854775809";
                error pp "unsupported integer" st
            end
            else begin
                Format.pp_print_char pp '-';
                Format.fprintf pp "%Lu" (Int64.succ n);
                loop st pp evs
            end
    and ev_float pp st evs n =
        let st = affix pp st in begin
            if Cf_endian_core.is_nan n then
                Format.pp_print_string pp "NaN"
            else if classify_float n = FP_infinite then begin
                if n < 0.0 then Format.pp_print_char pp '-';
                Format.pp_print_string pp "Infinity"
            end
            else
                Format.pp_print_string pp begin
                    let s = Printf.sprintf "%.16g" n in
                    if String.(contains s '.' || contains s 'e')
                        then s
                        else s ^ ".0"
                end
        end;
        loop st pp evs
    and ev_octets pp st evs s =
        let st = affix pp st in
        let b64 = Cf_base64.Std.encode_string s in
        Format.fprintf pp "b64'%s'" b64;
        loop st pp evs
    and ev_text pp st evs s =
        let st = affix pp st in
        let len = String.length s in
        let sxr = Cf_decode.string_scanner s in
        let rec uc_loop () =
            let Cf_decode.Position pos = sxr#position in
            if pos < len then begin
                match sxr#scan Ucs_transport.UTF8.uchar_decode_scheme with
                | exception Cf_decode.Invalid (_, msg) ->
                    Some msg
                | exception Cf_decode.Incomplete _ ->
                    Some "incomplete UTF8 encoding"
                | uc ->
                    let () =
                        match Uchar.to_int uc with
                        | 0 -> Format.pp_print_string pp {|\0|}
                        | 8 -> Format.pp_print_string pp {|\b|}
                        | 9 -> Format.pp_print_string pp {|\t|}
                        | 10 -> Format.pp_print_string pp {|\n|}
                        | 11 -> Format.pp_print_string pp {|\v|}
                        | 12 -> Format.pp_print_string pp {|\f|}
                        | 13 -> Format.pp_print_string pp {|\r|}
                        | 34 -> Format.pp_print_string pp "\\\""
                        | 92 -> Format.pp_print_string pp "\\\\"
                        | code ->
                            if code > 0x1f && code < 0x7f then
                                Format.pp_print_char pp (Char.chr code)
                            else if code < 256 then
                                Format.fprintf pp "\\x%02x" code
                            else
                                Format.fprintf pp "\\u{%x}" code
                    in
                    uc_loop ()
            end
            else
                None
        in
        Format.pp_print_char pp '"';
        let ret = uc_loop () in
        Format.pp_print_char pp '"';
        match ret with
        | None -> loop st pp evs
        | Some msg -> error pp msg st
    and ev_array pp st evs minor =
        let st = affix pp st in
        match Cbor_event.minor_to_intopt minor with
        | Some limit when limit < Sys.max_array_length ->
            let st = pp_enter pp @@ push ~limit ~close:']' st in
            (loop[@tailcall]) st pp evs
        | Some _ | None ->
            Format.pp_print_char pp '[';
            let Cbor_event.Minor n = minor in
            let msg = Printf.sprintf "unsupported array length %Lu" n in
            error pp msg st
    and ev_map pp st evs minor =
        let st = affix pp st in
        match Cbor_event.minor_to_intopt minor with
        | Some limit ->
            let st = pp_enter pp @@ push ~limit ~close:'}' st in
            (loop[@tailcall]) st pp evs
        | None ->
            let Cbor_event.Minor n = minor in
            let msg = Printf.sprintf "unsupported map length %Lu" n in
            error pp msg st
    and ev_tag pp st evs minor =
        let st = affix pp st in
        match Cbor_event.minor_to_intopt minor with
        | Some number ->
            Format.pp_print_int pp number;
            let st = push ~infix:'_' ~limit:1 ~close:')' st in
            let st = pp_enter pp st in
            (loop[@tailcall]) st pp evs
        | None ->
            let Cbor_event.Minor n = minor in
            Format.fprintf pp "%Lu(" n;
            error pp "unsupported tag number" st
    and ev_reserved pp st evs number =
        let st = affix pp st in
        Format.fprintf pp "simple(%u)" number;
        loop st pp evs
    in
    let enter pp evs =
        let st = pp_enter pp @@ push ~close:'!' [] in
        let st = loop st pp evs in
        let st = pp_leave pp st in
        assert (st = [])
    in
    Cf_emit.F enter

let of_string s =
    Cf_decode.string_to_vals Cbor_decode.event s |>
    Cf_emit.To_formatter.to_string events

let of_slice s =
    Cf_decode.slice_to_vals Cbor_decode.event s |>
    Cf_emit.To_formatter.to_string events

(*--- End ---*)
