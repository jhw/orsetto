(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let uintreq n =
    if n < 0 then
        invalid_arg (__MODULE__ ^ ": non-negative integer required!")

let event =
    let module E = Cbor_event in
    let open Cf_endian_big.Unsafe in
    let open Cf_decode in
    let limit32 = Int64.(shift_left 1L 32) in
    let rdcode s pos =
        let Position i = pos in
        let octet = ldu8 s i in
        let major = octet lsr 5 and info = octet land 0x1f in
        let width =
            if info < 24 || (info == 31 && major >= 2 && major <> 6) then
                0
            else if info < 28 then
                1 lsl (info - 24)
            else
                invalid pos @@
                    Printf.(sprintf "unrecognized info part %u" info)
        in
        major, info, width
    in
    let rdminor s pos width =
        let Position i = pos in
        E.unsafe_minor begin
            match width with
            | 1 ->
                Int64.of_int @@ ldu8 s i
            | 2 ->
                Int64.of_int @@ ldu16 s i
            | 4 ->
                if Sys.int_size < 32 then begin
                    let v = ldi32 s i in
                    if v < 0l then
                        Int64.(add limit32 @@ of_int32 v)
                    else
                        Int64.of_int32 v
                end
                else
                    Int64.of_int @@ ldu32 s i
            | 8 ->
                ldi64 s i
            | _ ->
                assert (not true);
                0L
        end
    in
    let rdoctets s pos width minor =
        match E.minor_to_intopt minor with
        | None ->
            assert (not true);
            ""
        | Some length ->
            let Position i = pos in
            let start = i + width in
            String.sub s start length
    in
    let ckdone w2 need =
        let pos, sz = w2 in
        analyze sz need pos
    in
    let cktype w2 major minor need s pos =
        if major == 2 || major == 3 then begin
            match E.minor_to_intopt minor with
            | Some length when length <= Sys.max_string_length ->
                let pos', have = w2 and need = need + length in
                let analyze = analyze have need in
                if major == 3 then begin
                    let Position i = pos in
                    assert (String.length s >= i + length);
                    let sl = Cf_slice.of_substring s i (i + length) in
                    try Ucs_transport.UTF8.validate_slice sl with
                    | Invalid (Position adj, msg) ->
                        invalid (advance adj pos) msg
                end;
                analyze pos'
            | (None | Some _)  ->
                invalid pos "exceeded maximum string length"
        end
        else if major == 4 || major == 5 then begin
            match E.minor_to_intopt minor with
            | Some length when length <= Sys.max_array_length ->
                ckdone w2 need
            | (None | Some _) ->
                invalid pos "exceeded maximum array length"
        end
        else
            ckdone w2 need
    in
    let ck s pos sz =
        let w2 = pos, sz in
        let major, info, width = rdcode s pos in
        let need = 1 + width and pos = advance 1 pos in
        let Size have = sz in
        if have < need || (info == 31 && (major >= 2 && major <> 6)) then
            ckdone w2 need
        else if major == 7 && width > 2 then begin
            let minor = E.int_to_minor info and pos = advance width pos in
            cktype w2 major minor need s pos
        end
        else if width > 0 then begin
            let minor = rdminor s pos width and pos = advance width pos in
            cktype w2 major minor need s pos
        end
        else begin
            let minor = E.int_to_minor info in
            cktype w2 major minor need s pos
        end
    and rd pos s =
        let major, info, width = rdcode s pos in
        let pos = advance 1 pos in
        let minor =
            if width > 0 then rdminor s pos width else E.int_to_minor info
        in
        if major < 7 then begin
            if major == 0 then
                E.integer `Positive minor
            else if major == 1 then
                E.integer `Negative minor
            else if major == 2 then begin
                if info == 31 then
                    E.signal `Octets
                else
                    E.octets (rdoctets s pos width minor)
            end
            else if major == 3 then begin
                if info == 31 then
                    E.signal `Text
                else
                    E.text (rdoctets s pos width minor)
            end
            else if major == 4 then
                if info == 31 then E.signal `Array else E.array minor
            else if major == 5 then
                if info == 31 then E.signal `Map else E.map minor
            else if major == 6 then begin
                E.tag minor
            end
            else begin
                assert (not true);
                let Position i = pos in
                E.unsafe_undefined (String.sub s (pred i) (succ width))
            end
        end
        else begin
            match info with
            | 20 ->
                E.boolean false
            | 21 ->
                E.boolean true
            | 22 ->
                E.null
            | 23 ->
                let Position i = pos in
                E.unsafe_undefined (String.sub s (pred i) 1)
            | 24 ->
                let E.Minor n = minor in
                if Int64.compare n 0x20L < 0 || Int64.compare n 0xFFL > 0 then
                    invalid pos "invalid encoding of simple value"
                else
                    E.unsafe_reserved @@ Int64.to_int n
            | 25 ->
                let Position i = pos in
                E.float ~precision:`Half @@
                    Cf_endian_core.of_fp16_bits @@
                    ldu16 s i
            | 26 ->
                let Position i = pos in
                E.float ~precision:`Single @@ Int32.float_of_bits @@ ldi32 s i
            | 27 ->
                let Position i = pos in
                E.float ~precision:`Double @@ Int64.float_of_bits @@ ldi64 s i
            | 31 ->
                E.signal `Break
            | _ ->
                assert (info < 28); (* invariant: see rdcode *)
                E.unsafe_reserved info
        end
    in
    scheme 1 ck rd

module Annot = struct
    module Basis = struct
        module Symbol = Cbor_event
        let symbol_type = Cbor_type.Event

        let advance pos ev =
            Cf_decode.advance (Cbor_event.width_octets ev) pos
    end

    include Cf_annot.Coded.Create(Basis)
end

module S = Cf_scan.Create(Annot.Scan_basis)

let of_slice p sl =
    S.of_seq p @@ Array.to_seq @@ Array.of_seq @@
    Cf_decode.slice_to_vals event sl

let of_string p s =
    S.of_seq p @@ Array.to_seq @@ Array.of_seq @@
    Cf_decode.string_to_vals event s

open S.Affix

let null = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Null -> Some ()
    | _ -> None
end

let break = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Signal `Break -> Some ()
    | _ -> None
end

let start s =
    let s = (s : [< `Octets | `Text | `Array | `Map] :> Cbor_event.signal) in
    S.tok begin [@warning "-4"] fun event ->
        match event with
        | Cbor_event.Signal s' when s = s' -> Some ()
        | _ -> None
    end

let boolean = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Boolean b -> Some b
    | _ -> None
end

let integer = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Integer (s, m) -> begin
        match Cbor_event.minor_to_intopt m with
        | Some n -> Some (match s with `Positive -> n | `Negative -> pred (-n))
        | None -> None
      end
    | _ ->
        None
end


(* TODO(ORS-102): expose this function *)

(*
let _raw_integer = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.(Integer (s, m)) -> Some (s, m)
    | _ -> None
end
*)

let int64_apply_sign_ s m =
    match s with
    | `Positive -> m
    | `Negative -> Int64.pred (Int64.neg m)

let signed_int64 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.(Integer (s, Minor m)) ->
        let i = int64_apply_sign_ s m in begin
            match s with
            | `Positive when i >= 0L -> Some i
            | `Negative when i < 0L -> Some i
            | _ -> None
        end
    | _ ->
        None
end

let min32_ = Int64.of_int32 Int32.min_int
let max32_ = Int64.of_int32 Int32.max_int

let signed_int32 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.(Integer (s, Minor m)) ->
        let i = int64_apply_sign_ s m in begin
            match s with
            | `Positive when i >= 0L && i <= max32_ -> Some (Int64.to_int32 i)
            | `Negative when i < 0L && i >= min32_ -> Some (Int64.to_int32 i)
            | _ -> None
        end
    | _ ->
        None
end

let int32 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Integer (s, Cbor_event.Minor m) ->
        let n64 = int64_apply_sign_ s m in
        if Int64.compare min32_ n64 <= 0 && Int64.compare n64 max32_ <= 0 then
            Some (s, Int64.to_int32 n64)
        else
            None
    | _ ->
        None
end

let int64 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Integer (s, Cbor_event.Minor m) ->
        Some (s, int64_apply_sign_ s m)
    | _ ->
        None
end

let float = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Float (_, n) -> Some n
    | _ -> None
end

let tag = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Tag (Cbor_event.Minor n) -> Some n
    | _ -> None
end

let octets1 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Octets s -> Some s
    | _ -> None
end

let text1 = S.tok begin [@warning "-4"] fun event ->
    match event with
    | Cbor_event.Text s -> Some s
    | _ -> None
end

let straux_def pstrl a b =
    let* strl = pstrl in
    let n = String.length @@ Annot.dn strl in
    if n < a || n > b then S.nil else S.return strl

let octets_def = straux_def octets1
let text_def = straux_def text1

let straux_vis pstrl (a, b, strs) =
    if b < 0 then
        S.nil
    else begin
        let* strl = pstrl in
        let str = Annot.dn strl in
        let n = String.length str in
        S.return (a - n, b - n, str :: strs)
    end

let octets_vis = straux_vis octets1
let text_vis = straux_vis text1

let octets_indef a0 b0 =
    let* synl = start `Octets in
    let* a, b, strs = S.vis octets_vis (a0, b0, []) in
    if a > 0 || b < 0 then
        S.nil
    else begin
        let* finl = break in
        let strs = List.rev strs in
        let buf = Buffer.create (b0 - b) in
        List.iter (Buffer.add_string buf) strs;
        S.return @@ Annot.span synl finl @@ Buffer.contents buf
    end

let text_indef =
    (*--- TODO(ORD-30):
        Ucs_text can provide an optimized text concatenation function that
        recognizes character composition at string boundaries.
      ---*)
    let nfd = Some (module Ucs_normal.NFD : Ucs_normal.Profile) in
    let apply_nf nf1 nf2 s =
        let text = Ucs_text.Unsafe.of_string s in
        match nf1 with
        | None -> text
        | Some _ -> Ucs_text.normalize ?nf:nf2 text
    in
    let add_segment b (Ucs_text.Octets s) =
        Buffer.add_string b s
    in
    let enter ?nf a0 b0 =
        let* synl = start `Text in
        let* a, b, strs = S.vis text_vis (a0, b0, []) in
        if a > 0 || b < 0 then
            S.nil
        else begin
            let* finl = break in
            let strs = List.rev_map (apply_nf nf nfd) strs in
            let buffer = Buffer.create (b0 - b) in
            List.iter (add_segment buffer) strs;
            S.return @@ Annot.span synl finl @@ apply_nf nf nf @@
                Buffer.contents buffer
        end
    in
    enter

let octets ?(a = 0) ?(b = Sys.max_string_length) () =
    uintreq a;
    if b < a || b > Sys.max_string_length then
        invalid_arg (__MODULE__ ^ ".octets: invalid maximum octets length.");
    S.alt [ octets_def a b; octets_indef a b ]

let text ?nf ?(a = 0) ?(b = Sys.max_string_length) () =
    uintreq a;
    if b < a || b > Sys.max_string_length then
        invalid_arg (__MODULE__ ^ ".text: invalid maximum octets length.");
    let indef = text_indef ?nf a b in
    let def =
        let* strl = text_def a b in
        S.return @@ Annot.map Ucs_text.Unsafe.of_string strl
    in
    S.alt [ def; indef ]

module Group = struct
    type syn = Indef | Def of int

    let synl_to_ab synl =
        match Annot.dn synl with
        | Indef -> None, None
        | Def n -> let v = Some n in v, v

    let ck_synl ?a ?b ~group synl =
        match Annot.dn synl with
        | Indef ->
            S.return synl
        | Def n ->
            match a, b with
            | Some a, _ when n < a -> S.fail @@ group ^ " too small"
            | _, Some b when n > b -> S.fail @@ group ^ " too large"
            | _, _ -> S.return synl

    let array_start =
        let indefinite =
            let+ p = start `Array in
            Annot.mv Indef p
        and definite =
            let* minorl =
                S.tok begin[@warning "-4"] function
                    | Cbor_event.Array minor -> Some minor
                    | _ -> None
                end
            in
            match Cbor_event.minor_to_intopt @@ Annot.dn minorl with
            | None -> S.fail "array too large"
            | Some n -> S.return @@ Annot.mv (Def n) minorl
        in
        let either = S.alt [ definite; indefinite ] in
        let enter ?a ?b () =
            let* synl = either in
            ck_synl ?a ?b ~group:"array" synl
        in
        enter

    let map_start =
        let indefinite =
            let+ p = start `Map in
            Annot.mv Indef p
        and definite =
            let* minorl =
                S.tok begin[@warning "-4"] function
                    | Cbor_event.Map minor -> Some minor
                    | _ -> None
                end
            in
            match Cbor_event.minor_to_intopt @@ Annot.dn minorl with
            | None -> S.fail "map too large"
            | Some n -> S.return @@ Annot.mv (Def n) minorl
        in
        let either = S.alt [ definite; indefinite ] in
        let enter ?a ?b () =
            let* synl = either in
            ck_synl ?a ?b ~group:"map" synl
        in
        enter

    let finish synl finl =
        match Annot.dn synl with
        | Indef -> S.or_fail "break expected" break
        | Def _ -> S.return finl

    let element =
        let v_scalar =
            S.tok begin[@warning "-4"] fun event ->
                match event with
                | Cbor_event.Null
                | Cbor_event.Boolean _
                | Cbor_event.Integer _
                | Cbor_event.Float _
                | Cbor_event.Octets _
                | Cbor_event.Text _
                | Cbor_event.Reserved _ ->
                    Some ()
                | _ ->
                    None
            end
        in
        let v_array ?b fix =
            let* synl = array_start ?b () in
            let a, b = synl_to_ab synl in
            let finl = Annot.mv () synl in
            let visitor _ = fix in
            let* finl = S.vis ?a ?b visitor finl in
            let+ finl = finish synl finl in
            Annot.span synl finl ()
        and v_map ?b fix =
            let* synl = map_start ?b () in
            let a, b = synl_to_ab synl in
            let finl = Annot.mv () synl in
            let visitor _ = let* _ = fix in fix in
            let* finl = S.vis ?a ?b visitor finl in
            let+ finl = finish synl finl in
            Annot.span synl finl ()
        and v_tagged fix =
            let* _ = tag in
            fix
        and v_indef_octets ?b () =
            let* p0 = start `Octets in
            let visitor _ =
                let+ vl = octets () in
                Annot.mv () vl
            in
            let* p1 = S.vis ?b visitor p0 in
            let+ p1 = finish (Annot.mv Indef p0) p1 in
            Annot.span p0 p1 ()
        and v_indef_text ?b () =
            let* p0 = start `Text in
            let visitor _ =
                let+ vl = text () in
                Annot.mv () vl
            in
            let* p1 = S.vis ?b visitor p0 in
            let+ p1 = finish (Annot.mv Indef p0) p1 in
            Annot.span p0 p1 ()
        in
        let rec v_any limit depth =
            if depth < 1000 then begin
                S.opt v_scalar >>= function
                | Some vl ->
                    S.return vl
                | None ->
                    let fix = v_any limit (succ depth) and b = limit in
                    let open! Cf_seqmonad.Infix in
                    S.altz @@ Cf_seqmonad.eval begin
                        let* _ = Cf_seqmonad.one @@ v_array ?b fix in
                        let* _ = Cf_seqmonad.one @@ v_map ?b fix in
                        let* _ = Cf_seqmonad.one @@ v_tagged fix in
                        let* _ = Cf_seqmonad.one @@ v_indef_octets ?b () in
                        Cf_seqmonad.one @@ v_indef_text ?b ()
                    end
            end
            else
                S.fail "nested too deeply"
        in
        let enter limit =
            let* mark = S.cur in
            let+ annot = v_any limit 0 in
            Annot.mv mark annot
        in
        enter
end

module Opaque = struct
    type mode = Mode of {
        strings: [ `Text | `String ]
    }

    let mode =
        let or_text opt =
            match opt with
            | Some strings -> (strings :> [ `Text | `String ])
            | None -> `Text
        in
        let enter ?strings () =
            let strings = or_text strings in
            Mode { strings }
        in
        enter

    let value =
        let v_null = S.ntyp Cf_type.Unit null in
        let v_boolean = S.ntyp Cf_type.Bool boolean in
        let v_integer = S.ntyp Cf_type.Int integer in
        let v_int32 =
            let* i32l = signed_int32 in
            let opaq = Cf_type.(witness Int32 @@ Annot.dn i32l) in
            S.return @@ Annot.mv opaq i32l
        in
        let v_int64 =
            let* i64l = signed_int64 in
            let opaq = Cf_type.(witness Int64) @@ Annot.dn i64l in
            S.return @@ Annot.mv opaq i64l
        in
        let v_float = S.ntyp Cf_type.Float float in
        let v_octets = S.ntyp Cf_type.String @@ octets () in
        let v_text (Mode m) =
            match m.strings with
            | `Text ->
                S.ntyp Ucs_type.Text @@ text ()
            | `String ->
                let* vl = text () in
                let Ucs_text.Octets s = Annot.dn vl in
                let v = Cf_type.(witness String) s in
                S.return @@ Annot.mv v vl
        in
        let v_array fix =
            let* synl = Group.array_start () in
            let a, b = Group.synl_to_ab synl in
            let* vls = S.seq ?a ?b fix in
            let vsl = Annot.collect vls in
            let+ finl = Group.finish synl (Annot.mv () vsl) in
            let vs = List.to_seq @@ Annot.dn vsl in
            Annot.span synl finl @@ Cf_type.(witness (Seq Opaque) vs)
        in
        let v_field fix =
            let* kl = fix in
            let+ vl = fix in
            Annot.(span kl vl (dn kl, dn vl))
        in
        let v_map fix =
            let* synl = Group.array_start () in
            let a, b = Group.synl_to_ab synl in
            let* kvls = S.seq ?a ?b @@ v_field fix in
            let kvsl = Annot.collect kvls in
            let+ finl = Group.finish synl (Annot.mv () kvsl) in
            let kvs = List.to_seq @@ Annot.dn kvsl in
            let v = Cf_type.(witness (Seq (Pair (Opaque, Opaque))) kvs) in
            Annot.span synl finl v
        in
        let v_tagged fix =
            let* nl = tag in
            let* vl = fix in
            let n = Annot.dn nl and v = Annot.dn vl in
            let v = Cf_type.(witness (Cbor_type.Tag Opaque) (n, v)) in
            S.return @@ Annot.span nl vl v
        in
        let v_reserved =
            S.ntyp Cbor_type.Reserved @@ S.tok begin [@warning "-4"] fun ev ->
                match ev with
                | Cbor_event.Reserved int -> Some int
                | _ -> None
            end
        in
        let v_scalar mode =
            S.altz @@ Array.to_seq @@ [|
                v_integer; v_text mode; v_octets; v_boolean; v_float; v_null;
                v_int32; v_int64; v_reserved
            |]
        in
        let rec fix scalar depth =
            if depth < 1000 then begin
                S.opt scalar >>= function
                | Some v ->
                    S.return v
                | None ->
                    let fix = fix scalar (succ depth) in
                    S.alt [ v_array fix; v_map fix; v_tagged fix ]
            end
            else
                S.fail "nested too deeply"
        in
        let mode0 = mode () in
        let enter ?(mode = mode0) () =
            let scalar = v_scalar mode in
            fix scalar 0
        in
        enter

    type _ Cf_type.nym += With_mode: mode -> Cf_type.opaque Cf_type.nym

    let model m = Cf_data_ingest.primitive @@ With_mode m
end

module Ingest = struct
    module D = Cf_data_ingest

    type (_, _) D.control += Tagged: int64 -> ('a, 'a) D.control

    module Basis = struct
        type symbol = Cbor_event.t
        type position = Cf_decode.position
        type 'k frame = Frame of Group.syn * D.occurs

        module Form = struct
            include Annot
            type 'a t = 'a Annot.form
        end

        module Scan = S

        let primitive: type v. v Cf_type.nym -> v Form.t Scan.t = fun nym ->
            match nym with
            | Cf_type.Unit -> null
            | Cf_type.Bool -> boolean
            | Cf_type.Int -> integer
            | Cf_type.Int32 -> signed_int32
            | Cf_type.Int64 -> signed_int64
            | Cf_type.Float -> float
            | Ucs_type.Text -> text ()
            | Cf_type.String -> octets ()
            | Cf_type.Opaque -> Opaque.value ()
            | Opaque.With_mode mode -> Opaque.value ~mode ()
            | _ -> invalid_arg (__MODULE__ ^ ": primitive undefined for CBOR!")

        let control:
            type a b. a Form.t S.t -> (a, b) D.control -> b Form.t S.t
            = fun scan control ->
                match control with
                | D.Cast f ->
                    Scan.cast f scan
                | Tagged n ->
                    let* nl = tag in
                    if Int64.equal n (Form.dn nl) then begin
                        let+ vl = S.or_fail "expected tagged value" scan in
                        Form.(span nl vl @@ dn vl)
                    end
                    else
                        S.nil
                | _ ->
                    invalid_arg
                        (__MODULE__ ^ ": unrecognized CBOR control variant!")

        let memo = Group.element None

        let visit _container frame each init =
            let Frame (_, D.Occurs { a; b }) = frame in
            S.vis ?a ?b each init

        let start =
            let ckframe occurs synl =
                let D.Occurs { a; b } = occurs in
                match Form.dn synl with
                | Group.Indef ->
                    S.return @@ Form.mv (Frame (Group.Indef, occurs)) synl
                | Group.Def n as syn ->
                    match a, b with
                    | _, Some b when n > b -> S.fail "CBOR group too large"
                    | Some a, _ when n < a -> S.fail "CBOR group too small"
                    | _ ->
                        let occurs = D.occurs ~a:n ~b:n () in
                        S.return @@ Form.mv (Frame (syn, occurs)) synl
            in
            let enter container occurs =
                match container with
                | (`Variant _ | `Structure _ | `Table _) ->
                    let* synl = Group.map_start () in
                    ckframe occurs synl
                | (`Record | `Vector) ->
                    let* synl = Group.array_start () in
                    ckframe occurs synl
            in
            enter

        let finish frame =
            let Frame (syn, _) = frame in
            match syn with
            | Group.Def _ -> S.(cur >>: pos)
            | Group.Indef -> break

        let pair _container kscan vscan =
            let* kl = kscan in
            let+ vl = vscan in
            Form.(span kl vl @@ (dn kl, dn vl))
    end

    include D.Create(Basis)

    let tagged n m = D.control m @@ Tagged n
end

let group ?a ?b kind scan =
    let* synl =
        match kind with
        | `Array -> Group.array_start ?a ?b ()
        | `Map -> Group.map_start ?a ?b ()
    in
    let a, b = Group.synl_to_ab synl in
    let* save = S.cur in
    let* mems =
        S.seq ?a ?b @@
        match kind with
        | `Array ->
            Group.element b
        | `Map ->
            let* _ = Group.element b in
            let* v = Group.element b in
            S.return v
    in
    let finl =
        match mems with
        | [] -> Annot.mv () synl
        | s -> Annot.mv () @@ List.(hd @@ rev s)
    in
    let* _ = S.mov save in
    let* ret = scan in
    let+ finl = Group.finish synl finl in
    Annot.span synl finl ret

(*--- BEGIN OBSOLESCENT ---*)

let value = Opaque.value ()

module Record_obsolescent = struct
    module type Basis = sig
        module Index: Cf_relations.Order
        val index: Index.t S.t
    end

    module Int_basis = struct
        module Index = Cf_relations.Int
        let index = integer >>: Annot.dn
    end

    module String_basis = struct
        module Index = String

        let index =
            let* sl = text () in
            let Ucs_text.Octets s = Annot.dn sl in
            S.return s
    end

    module type Profile = Cf_record_scan.Profile
       with type 'a form := 'a Annot.form
        and type 'a t := 'a S.t

    module Aux(B: Basis) = struct
        type symbol = Cbor_event.t
        type position = Cf_decode.position

        module Form = struct
            include Annot
            type 'a t = 'a Annot.form
        end

        module Index = B.Index
        module Content = Cbor_type

        module Scan = struct
            include S

            let index = B.index
            let preval = None
            let chain = None
        end
    end

    module Create(B: Basis) = struct
        module A = Aux(B)
        include Cf_record_scan.Create(A)

        let scan schema =
            let a, b = range schema in
            (group ~a ~b `Map @@ scan schema) >>: Annot.dn
    end
end

module Record = Record_obsolescent
module Int_map = Record_obsolescent.Create(Record_obsolescent.Int_basis)
module String_map = Record_obsolescent.Create(Record_obsolescent.String_basis)

(*--- END OBSOLESCENT ---*)

include S

(*--- End ---*)
