(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Concise Binary Object Representation (CBOR) flyweight implementation. *)

(** {6 Overview}

    This module provides flyweight encoding and decoding of Concise Binary
    Object Representation (CBOR) structured data.

    Use the flyweight implementation when values are not very large, streaming
    is not required, integer and floating point values do not exceed the limits
    of representation for OCaml primitive [int] and [float] types, lengths of
    octet arrays and Unicode texts do not exceed the limits of the OCaml
    [string] type, no special handling is required for standard CBOR tags, no
    Unicode normalization is required, and the order/uniqueness properties of
    key-value pairs in maps is not required to be deterministic.

    The encoder never emits indefinite-length sequence events. The decoder can
    scan them and reconstruct fixed-length sequences from them accordingly.
*)

(** {6 Types} *)

(** The private type of flyweight CBOR values. *)
type t = private
    | Null
    | Undefined of string
    | Boolean of bool
    | Integer of int
    | Float of float
    | Octets of string
    | Text of string
    | Array of t list
    | Map of (t * t) list
    | Tag of int * t

(** Equality *)
val equal: t -> t -> bool

(** Comparison *)
val compare: t -> t -> int

(** {6 Constructors}

    The functions below guard construction of flyweight values to respect the
    invariants of the CBOR encoding, e.g. tag values must be non-negative
    integers, text strings {i should} comprise valid UTF-8 encodings, an
    "undefined" value can only be constructed by decoders, et cetera.
*)

(** The distinguished "null" value. *)
val null: t

(** Use [boolean b] to make a boolean value with [b]. *)
val boolean: bool -> t

(** Use [integer i] to make an integer value with [i]. *)
val integer: int -> t

(** Use [float n] to make a floating-point value with [n]. *)
val float: float -> t

(** Use [octets s] to make an octet sequence value with [s]. *)
val octets: string -> t

(** Use [text s] to make a Unicode text value with [b]. If [s] is not valid
    UTF-8 and [~unsafe:()] is not used, then [Cf_decode.Invalid] is raised.
*)
val text: ?unsafe:unit -> string -> t

(** Use [array s] to make an array value with [s]. *)
val array: t list -> t

(** Use [map s] to make a map value with [s]. *)
val map: (t * t) list -> t

(** Use [tag n v] to tag the value [v] with tag number [n]. *)
val tag: int -> t -> t

(** {6 Decoding and Encoding} *)

(** The decoder for flyweight CBOR values. *)
val decoder: t Cf_decode.scheme

(** The encoder for flyweight CBOR values. *)
val encoder: t Cf_encode.scheme

(*--- End ---*)
