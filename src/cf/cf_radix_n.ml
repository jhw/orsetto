(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type check = Digit | Skip | Pad | Invalid
type req = Must | Should

type basis = Basis of {
    pad: (req * char) option;
    check: char -> check;
    decode: char -> int;
    encode: int -> char;
    radix: int;
}

type 'n aux = Aux of {
    pad: (req * char) option;
    check: char -> check;
    width: int;
    digits: int;
    octets: int;
    init: 'n;
    of_octet: 'n -> char -> 'n;
    of_digit: 'n -> char -> 'n;
    of_pad: 'n -> 'n;
    to_octet: 'n -> int -> char;
    to_digit: 'n -> int -> char;
    wordstr: 'n -> string
}

let auxfail n =
    n |> Printf.sprintf "Cf_radix_n: radix %u words too large." |> invalid_arg

let aux0 (Basis b) =
    let pad = b.pad and check = b.check in
    let decode = b.decode and encode = b.encode in
    let init = 0
    and of_octet w c = (w lsl 8) lor (Char.code c)
    and to_octet w ws = (w lsr ws) land 0xff |> Char.chr
    and to_digit mask w ws = (w lsr ws) land mask |> encode
    in
    let wordstr n = Printf.sprintf "0x%X" n in
    match b.radix with
    | 16 ->
        let width = 4 and digits = 2 and octets = 1 in
        let of_pad w = w lsl width in
        let of_digit w c = (of_pad w) lor (decode c) in
        let to_digit = to_digit 0xf in
        Aux {
            pad; check; width; octets; digits; init; of_octet; of_pad;
            of_digit; to_octet; to_digit; wordstr
        }
    | 32 when Sys.int_size > 31 ->
        let width = 5 and digits = 8 and octets = 5 in
        let of_pad w = w lsl width in
        let of_digit w c = (of_pad w) lor (decode c) in
        let to_digit = to_digit 0x1f in
        Aux {
            pad; check; width; octets; digits; init; of_octet; of_pad;
            of_digit; to_octet; to_digit; wordstr
        }
    | 64 ->
        let width = 6 and digits = 4 and octets = 3 in
        let of_pad w = w lsl width in
        let of_digit w c = (of_pad w) lor (decode c) in
        let to_digit = to_digit 0x3f in
        Aux {
            pad; check; width; octets; digits; init; of_octet; of_pad;
            of_digit; to_octet; to_digit; wordstr
        }
    | r ->
        auxfail r

let aux64 (Basis b) =
    let pad = b.pad and check = b.check in
    let decode = b.decode and encode = b.encode in
    let init = 0L
    and of_octet w c =
        Int64.(logor (Char.code c |> of_int) (shift_left w 8))
    and to_octet w ws =
        Int64.(logand (shift_right_logical w ws) 0xffL |> to_int) |> Char.chr
    and to_digit mask w ws =
        Int64.(logand (shift_right_logical w ws) mask |> to_int) |> encode
    in
    let wordstr n = Printf.sprintf "0x%LX" n in
    match b.radix with
    | 32 ->
        let width = 5 and digits = 8 and octets = 5 in
        let of_pad w = Int64.(shift_left w width) in
        let of_digit w c = Int64.(logor (decode c |> of_int) (of_pad w)) in
        let to_digit = to_digit 0x1fL in
        Aux {
            pad; check; width; octets; digits; init; of_octet; of_pad;
            of_digit; to_octet; to_digit; wordstr
        }
    | r ->
        auxfail r

exception Error

let decode_seq_aux =
    let push tin =
        match tin with
        | None -> None
        | Some (ti, tn) -> Some (succ ti, tn)
    and tin0 = function
        | None ->
            None
        | Some n ->
            if n < 0 then invalid_arg "Cf_radix_n: length < 0";
            Some (0, n)
    and ofwp ~aux wp =
        let Aux a = aux in
        if wp > 0 && wp < a.digits then
            (a.digits - wp) * a.width / 8
        else
            a.octets
    in
    let rec loop ~aux ?tin ~w ~wp ~ws s =
        match s () with
        | Seq.Nil -> (unpad[@tailcall]) ~aux ?tin ~w ~ws ~wp wp
        | Seq.Cons (hd, tl) -> (check[@tailcall]) ~aux ?tin ~w ~wp ~ws ~tl hd
    and unpad ~aux ?tin ~w ~ws ~wp wn =
        let Aux a = aux in
        if wn < a.digits then begin
            if wn > 0 then begin
                match a.pad with
                | Some (Must, _) ->
                    raise Error
                | (Some (Should, _) | None) ->
                    let w = a.of_pad w and ws = ws + a.width in
                    (unpad[@tailcall]) ~aux ?tin ~w ~ws ~wp (pred wp)
            end
            else begin
                let wi = 0 and wn = ofwp ~aux wp in
                (finish[@tailcall]) ~aux ?tin ~w ~wi ~ws ~wn ()
            end
        end
        else
            Seq.Nil
    and check ~aux ?tin ~w ~wp ~ws ~tl c =
        let Aux a = aux in
        match a.check c with
        | Digit -> (digit[@tailcall]) ~aux ?tin ~w ~wp ~ws ~tl c
        | Skip -> (loop[@tailcall]) ~aux ?tin ~w ~wp ~ws tl
        | Pad -> (startpad[@tailcall]) ~aux ?tin ~w ~wp ~ws tl
        | Invalid -> raise Error
    and digit ~aux ?tin ~w ~wp ~ws ~tl c =
        let Aux a = aux in
        let w = a.of_digit w c and ws = ws + a.width and wp = pred wp in
        if wp > 0 then
            (loop[@tailcall]) ~aux ?tin ~w ~wp ~ws tl
        else
            (octet[@tailcall]) ~aux ?tin ~w ~wi:0 ~ws ~tl ()
    and octet ~aux ?tin ~w ~wi ~ws ~tl () =
        let Aux a = aux in
        if wi < a.octets then begin
            let ws = ws - 8 and wi = succ wi in
            let c = a.to_octet w ws and tin = push tin in
            Seq.Cons (c, octet ~aux ?tin ~w ~wi ~ws ~tl)
        end
        else
            (loop[@tailcall]) ~aux ?tin ~w:a.init ~wp:a.digits ~ws:0 tl
    and startpad ~aux ?tin ~w ~wp ~ws tl =
        let Aux a = aux in
        if a.pad = None || not (wp > 0 && wp < a.digits) then raise Error;
        let wn = ofwp ~aux wp and wp = pred wp in
        let w = a.of_pad w and ws = ws + a.width in
        (nextpad[@tailcall]) ~aux ?tin ~w ~wp ~wn ~ws tl
    and nextpad ~aux ?tin ~w ~wp ~wn ~ws tl =
        let Aux a = aux in
        match tl () with
        | Seq.Nil ->
            if wp > 0 then
                (unpad[@tailcall]) ~aux ?tin ~w ~ws ~wp wn
            else
                (finish[@tailcall]) ~aux ?tin ~w ~wi:0 ~wn ~ws ()
        | Seq.Cons (hd, tl) ->
            match a.check hd with
            | Pad when wp > 0 && wp < a.digits ->
                let w = a.of_pad w and ws = ws + a.width and wp = pred wp in
                (nextpad[@tailcall]) ~aux ?tin ~w ~wp ~wn ~ws tl
            | (Digit | Skip | Pad | Invalid) ->
                raise Error
    and finish ~aux ?tin ~w ~wi ~wn ~ws () =
        let Aux a = aux in
        if wi < wn then begin
            let wi = succ wi and ws = ws - 8 in
            let c = a.to_octet w ws and tin = push tin in
            Seq.Cons (c, finish ~aux ?tin ~w ~wi ~wn ~ws)
        end
        else begin
            match tin with
            | Some (ti, tn) when ti < tn -> raise Error
            | (Some _ | None) -> Seq.Nil
        end
    in
    let enter ~aux ?n s () =
        let Aux a = aux in
        (loop[@tailcall]) ~aux ?tin:(tin0 n) ~w:a.init ~wp:a.digits ~ws:0 s
    in
    enter

exception Break_insert of int * string

let encode_seq_aux =
    let ws0 (Aux a) = a.octets * 8 in
    let validate_brk ~aux brk =
        let Aux a = aux in
        match brk with
        | None -> ()
        | Some (bn, bs) ->
            if bn < 1 then
                invalid_arg "Cf_radix_n: invalid break interval! (n < 1)";
            let visit i c =
                if a.check c <> Skip then
                    invalid_arg @@ Printf.sprintf
                        "Cf_radix_n: invalid break character! (index %u)" i
            in
            String.iteri visit bs
    in
    let rec loop ~aux ?pad ?brk ~bi ~w ~wi ~ws tl =
        let Aux a = aux in
        match tl () with
        | Seq.Nil ->
            let wp = (a.octets - wi) * 8 / a.width in
            (finish[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi ~wp ()
        | Seq.Cons (hd, tl) ->
            (octet[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi ~ws ~tl hd
    and octet ~aux ?pad ?brk ~bi ~w ~wi ~ws ~tl c =
        let Aux a = aux in
        let w = a.of_octet w c and ws = ws + 8 and wi = succ wi in
        if wi < a.octets then
            (loop[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi ~ws tl
        else
            (digit[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi:0 ~wp:0 ~ws ~tl ()
    and digit ~aux ?pad ?brk ~bi ~w ~wi ~wp ~ws ~tl () =
        let Aux a = aux in
        if wi < a.digits - wp then begin
            match isbreak bi brk with
            | exception (Break_insert (bn, bs)) ->
                (insbreak[@tailcall]) ~aux ?pad ~w ~wi ~wp ~ws ~tl bs 0 bn ()
            | bi ->
                let wi = succ wi and ws = ws - a.width in
                let c = a.to_digit w ws in
                Seq.Cons (c, digit ~aux ?pad ?brk ~bi ~w ~wi ~wp ~ws ~tl)
        end
        else if wp > 0 && wi < a.digits then begin
            match pad with
            | None ->
                Seq.Nil
            | Some (_, c) ->
                match isbreak bi brk with
                | exception (Break_insert (bn, bs)) ->
                    (insbreak[@tailcall]) ~aux ?pad ~w ~wi ~wp ~ws ~tl bs 0 bn
                        ()
                | bi ->
                    let wi = succ wi and wp = pred wp in
                    let ws = 0 and tl = Seq.empty in
                    Seq.Cons (c, digit ~aux ?pad ?brk ~bi ~w ~wi ~wp ~ws ~tl)
        end
        else
            (loop[@tailcall]) ~aux ?pad ?brk ~bi ~w:a.init ~wi:0 ~ws:0 tl
    and isbreak bi = function
        | Some (bn, bs) ->
            let bi = succ bi in
            if bi > bn then raise (Break_insert (bn, bs));
            bi
        | None ->
            bi
    and insbreak ~aux ?pad ~w ~wi ~wp ~ws ~tl bs bi bn () =
        if bi < String.length bs then begin
            let c = String.unsafe_get bs bi and bi = succ bi in
            Seq.Cons (c, insbreak ~aux ?pad ~w ~wi ~wp ~ws ~tl bs bi bn)
        end
        else begin
            let brk = Some (bn, bs) in
            (digit[@tailcall]) ~aux ?pad ?brk ~bi:0 ~w ~wi ~wp ~ws ~tl ()
        end
    and finish ~aux ?pad ?brk ~bi ~w ~wi ~wp () =
        let Aux a = aux in
        if pad = None || wi = 0 then
            Seq.Nil
        else if wi < a.octets then begin
            let wi = succ wi and w = a.of_octet w '\000' in
            (finish[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi ~wp ()
        end
        else begin
            let ws = ws0 aux and tl = Seq.empty in
            (digit[@tailcall]) ~aux ?pad ?brk ~bi ~w ~wi:0 ~wp ~ws ~tl ()
        end
    in
    let enter ~aux ?brk ?np s () =
        validate_brk ~aux brk;
        let Aux a = aux in
        let canpad = match np with None -> true | Some () -> false in
        let pad =
            match a.pad with
            | Some (Must, _) ->
                if canpad then
                    a.pad
                else
                    invalid_arg "Cf_radix_n: pad required!"
            | Some (Should, _) ->
                if canpad then a.pad else None
            | None ->
                None
        in
        (loop[@tailcall]) ~aux ?pad ?brk ~bi:0 ~w:a.init ~wi:0 ~ws:0 s
    in
    enter

let decode_string_aux ~aux ?n s =
    let z = decode_seq_aux ~aux ?n @@ String.to_seq s in
    try
        Some begin
            match n with
            | None -> Buffer.(contents @@ of_seq z)
            | Some n -> String.of_seq @@ Cf_seq.limit n z
        end
    with
    | Error ->
        None

let decode_slice_aux ~aux ?n s =
    let n0 = Cf_slice.length s in
    let s = Cf_slice.(Cf_seq.of_substring s.vector s.start n0) in
    let z = decode_seq_aux ~aux ?n s in
    try
        Some begin
            match n with
            | None -> Buffer.(contents @@ of_seq z)
            | Some n -> String.of_seq @@ Cf_seq.limit n z
        end
    with
    | Error ->
        None

let encode_string_aux ~aux ?brk ?np s =
    let Aux a = aux in
    let n = (String.length s + a.octets - 1) / a.octets * a.digits in
    let n =
        match brk with
        | None -> n
        | Some (bn, bs) -> n + ((String.length bs) * ((n - 1) / bn))
    in
    String.of_seq @@ Cf_seq.limit n @@ encode_seq_aux ~aux ?brk ?np @@
        String.to_seq s

let encode_slice_aux ~aux ?brk ?np s =
    let Aux a = aux in
    let n0 = Cf_slice.length s in
    let n = (n0 + a.octets - 1) / a.octets * a.digits in
    let n =
        match brk with
        | None -> n
        | Some (bn, bs) -> n + ((String.length bs) * ((n - 1) / bn))
    in
    let s = Cf_slice.(Cf_seq.of_substring s.vector s.start n0) in
    String.of_seq @@ Cf_seq.limit n @@ encode_seq_aux ~aux ?brk ?np s

module type Profile = sig
    val basis: basis

    val decode_seq: ?n:int -> char Seq.t -> char Seq.t
    val decode_string: ?n:int -> string -> string option
    val decode_slice: ?n:int -> string Cf_slice.t -> string option

    val encode_seq:
        ?brk:(int * string) -> ?np:unit -> char Seq.t -> char Seq.t

    val encode_string: ?brk:(int * string) -> ?np:unit -> string -> string
    val encode_slice:
        ?brk:(int * string) -> ?np:unit -> string Cf_slice.t -> string
end

let create basis =
    let Basis b = basis in
    if Sys.int_size > 31 || b.radix <> 32 then begin
        let aux = aux0 basis in
        let module M = struct
            let basis = basis
            let decode_seq = decode_seq_aux ~aux
            let decode_string = decode_string_aux ~aux
            let decode_slice = decode_slice_aux ~aux
            let encode_seq = encode_seq_aux ~aux
            let encode_string = encode_string_aux ~aux
            let encode_slice = encode_slice_aux ~aux
        end in
        (module M : Profile)
    end
    else begin
        let aux = aux64 basis in
        let module M = struct
            let basis = basis
            let decode_seq = decode_seq_aux ~aux
            let decode_string = decode_string_aux ~aux
            let decode_slice = decode_slice_aux ~aux
            let encode_seq = encode_seq_aux ~aux
            let encode_string = encode_string_aux ~aux
            let encode_slice = encode_slice_aux ~aux
        end in
        (module M : Profile)
    end

(*--- End ---*)
