(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type 'i cmp = Cmp of ('i -> int) [@@ocaml.unboxed]

type ('i, 'r) ret = Ret of {
    none: unit -> 'r;
    some: 'i -> 'r;
}

module type Basis = sig
    include Cf_relations.Order

    val succ: t -> t
    val pred: t -> t
    val center: t -> t -> t
end

module type Profile = sig
    type t

    val find: (t, 'r) ret -> t cmp -> t -> t -> 'r
    val search: t cmp -> t -> t -> t option
    val require: t cmp -> t -> t -> t
end

module Char_basis = struct
    type t = char
    let compare = Char.compare

    let succ c = Char.chr (succ (Char.code c))
    let pred c = Char.chr (pred (Char.code c))

    let center lo hi =
        let lo = Char.code lo and hi = Char.code hi in
        Char.chr ((lo + hi) / 2);
end

module Int_basis = struct
    type t = int
    let compare = Int.compare
    let succ = succ
    let pred = pred
    let center lo hi = (lo + hi) / 2
end

module Float_basis = struct
    type t = float
    let compare = Float.compare
    let succ = Float.succ
    let pred = Float.pred
    let center lo hi = (lo +. hi) /. 2.0
end

module Create(B: Basis) = struct
    let rec find ret sel lo hi =
        (* NOTE WELL: zero allocations *)
        let Ret r = ret in
        let d = B.compare lo hi in
        if d > 0 then
            (r.none[@tailcall]) ()
        else
            let m = if d <> 0 then B.center lo hi else lo in
            let Cmp f = sel in
            let x = f m in
            if x < 0 then
                (find[@tailcall]) ret sel (B.succ m) hi
            else if x > 0 then
                (find[@tailcall]) ret sel lo (B.pred m)
            else
                (r.some[@tailcall]) m

    let search =
        let none () = None and some m = Some m in
        find (Ret { none; some })

    let require =
        let none () = raise Not_found and some m = m in
        find (Ret { none; some })
end

module Int = Create(Int_basis)
module Char = Create(Char_basis)
module Float = Create(Float_basis)

(*--- End ---*)
