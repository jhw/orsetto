(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Printf

module type Unsafe = sig
    val descript: string

    val lds8: string -> int -> int
    val ldu8: string -> int -> int
    val lds16: string -> int -> int
    val ldu16: string -> int -> int
    val lds32: string -> int -> int
    val ldu32: string -> int -> int
    val lds64: string -> int -> int
    val ldu64: string -> int -> int
    val ldi32: string -> int -> int32
    val ldi64: string -> int -> int64

    val ldi32_boxed: string -> int -> int32
    val ldi64_boxed: string -> int -> int64

    val sts8: int -> bytes -> int -> unit
    val stu8: int -> bytes -> int -> unit
    val sts16: int -> bytes -> int -> unit
    val stu16: int -> bytes -> int -> unit
    val sts32: int -> bytes -> int -> unit
    val stu32: int -> bytes -> int -> unit
    val sts64: int -> bytes -> int -> unit
    val stu64: int -> bytes -> int -> unit
    val sti32: int32 -> bytes -> int -> unit
    val sti64: int64 -> bytes -> int -> unit

    val sti32_boxed: int32 -> bytes -> int -> unit
    val sti64_boxed: int64 -> bytes -> int -> unit
end

module type Safe = sig
    val descript: string

    val lds8: int Cf_decode.scheme
    val ldu8: int Cf_decode.scheme
    val lds16: int Cf_decode.scheme
    val ldu16: int Cf_decode.scheme
    val lds32: int Cf_decode.scheme
    val ldu32: int Cf_decode.scheme
    val lds64: int Cf_decode.scheme
    val ldu64: int Cf_decode.scheme

    val ldi32: int32 Cf_decode.scheme
    val ldi64: int64 Cf_decode.scheme

    val ldf16: float Cf_decode.scheme
    val ldf32: float Cf_decode.scheme
    val ldf64: float Cf_decode.scheme

    val sts8: int Cf_encode.scheme
    val stu8: int Cf_encode.scheme
    val sts16: int Cf_encode.scheme
    val stu16: int Cf_encode.scheme
    val sts32: int Cf_encode.scheme
    val stu32: int Cf_encode.scheme
    val sts64: int Cf_encode.scheme
    val stu64: int Cf_encode.scheme

    val sti32: int32 Cf_encode.scheme
    val sti64: int64 Cf_encode.scheme

    val stf16: float Cf_encode.scheme
    val stf32: float Cf_encode.scheme
    val stf64: float Cf_encode.scheme
end

let is_valid_int n min max = (n >= min && n <= max)

let min32_si =
    if Sys.int_size < 32 then min_int else Int32.to_int Int32.min_int

let max32_si =
    if Sys.int_size < 32 then max_int else Int32.to_int Int32.max_int

let max32_ui = if Sys.int_size < 32 then max_int else pred (1 lsl 32)

let is_signed_int32 =
    if Sys.int_size < 32 then begin
        let min = Int32.of_int min_int and max = Int32.of_int max_int in
        let enter n = Int32.compare n min >= 0 && Int32.compare n max <= 0 in
        enter
    end
    else begin
        let enter _ = true in
        enter
    end

let is_unsigned_int32 =
    if Sys.int_size < 32 then begin
        let max32 = Int32.of_int max_int in
        let enter n =
            Int32.compare n Int32.zero >= 0 && Int32.compare n max32 <= 0
        in
        enter
    end
    else begin
        let enter n = Int32.compare n Int32.zero >= 0 in
        enter
    end

let is_signed_int64 =
    let min64 = Int64.of_int min_int and max64 = Int64.of_int max_int in
    let enter n = Int64.compare n min64 >= 0 && Int64.compare n max64 <= 0 in
    enter

let is_unsigned_int64 =
    let max64 = Int64.of_int max_int in
    let enter n =
        Int64.compare n Int64.zero >= 0 && Int64.compare n max64 <= 0
    in
    enter

let no_invalid sz _ i v = Cf_encode.analyze i sz v

let req_valid_int p n min max =
    if not (is_valid_int n min max) then begin
        Cf_encode.invalid p begin
            sprintf "%s: signed value[%d] invalid in range[%d,%d]." __MODULE__
                n min max
        end
    end

let req_valid_uint p n max =
    if not (is_valid_int n 0 max) then begin
        Cf_encode.invalid p begin
            sprintf "%s: unsigned value[%d] invalid in range[0,%u]." __MODULE__
                n max
        end
    end

let req_valid_s8 p i n =
    req_valid_int p n (-0x80) 0x7f;
    Cf_encode.analyze i 1 n

let req_valid_u8 p i n =
    req_valid_uint p n 0xff;
    Cf_encode.analyze i 1 n

let req_valid_s16 p i n =
    req_valid_int p n (-0x8000) 0x7fff;
    Cf_encode.analyze i 2 n

let req_valid_u16 p i n =
    req_valid_uint p n 0xffff;
    Cf_encode.analyze i 2 n

let req_valid_s32 p i n =
    req_valid_int p n min32_si max32_si;
    Cf_encode.analyze i 4 n

let req_valid_u32 p i n =
    req_valid_uint p n max32_ui;
    Cf_encode.analyze i 4 n

let req_valid_u64 p i n =
    req_valid_int p n 0 max_int;
    Cf_encode.analyze i 8 n

let ck_signed_int32 p n =
    if not (is_signed_int32 n) then begin
        Cf_decode.invalid p begin
            sprintf "%s: value[%ld] invalid in range[%d,%d]." __MODULE__ n
                min_int max_int
        end
    end;
    Int32.to_int n

let ck_unsigned_int32 p n =
    if not (is_unsigned_int32 n) then begin
        Cf_decode.invalid p begin
            sprintf "%s: value[%ld] invalid in range[%d,%d]." __MODULE__ n 0
                max_int
        end
    end;
    Int32.to_int n

let ck_signed_int64 p n =
    if not (is_signed_int64 n) then begin
        Cf_decode.invalid p begin
            sprintf "%s: value[%Ld] invalid in range[%d,%d]." __MODULE__ n
                min_int max_int
        end
    end;
    Int64.to_int n

let ck_unsigned_int64 p n =
    if not (is_unsigned_int64 n) then begin
        Cf_decode.invalid p begin
            sprintf "%s: value[%Ld] invalid in range[%d,%d]." __MODULE__ n 0
                max_int
        end
    end;
    Int64.to_int n

module Unsafe_octet = struct
    let ldu8 s i = int_of_char (String.unsafe_get s i)
    let lds8 s i = let n = ldu8 s i in if n > 127 then n - 256 else n
    let sts8 n s i = Bytes.unsafe_set s i (Char.chr (0xff land n))
    let stu8 n s i = Bytes.unsafe_set s i (Char.chr n)
end

exception Imprecise

let f_mask = 0x000f_ffff_ffff_ffffL
let e_mask = 0x7ff0_0000_0000_0000L
let s_mask = 0x8000_0000_0000_0000L

let is_nan n =
    let open Int64 in
    let bits = bits_of_float n in
    let f_bits = logand bits f_mask and e_bits = logand bits e_mask in
    f_bits > 0L && e_bits = e_mask

let of_fp16_bits n =
    let fraction = n land 0x3ff in
    let exponent = (n lsr 10) land 0x1f in
    let sign = n land 0x8000 <> 0 in
    let value =
        match exponent with
        | 31 -> if fraction > 0 then nan else infinity
        | 0 -> if fraction > 0 then ldexp (float fraction) (-24) else 0.0
        | exponent -> ldexp (float @@ fraction + 1024) (exponent - 25)
    in
    if sign && (fraction = 0 || exponent <> 31) then ~-. value else value

let to_fp16_bits_unsafe =
    let f_of_u64 n = Int64.(logand n f_mask) in
    let e_of_u64 n = Int64.(shift_right_logical (logand n e_mask) 52) in
    let is_neg n =
        Int64.(shift_right_logical (logand n s_mask) 63 |> to_int) <> 0
    in
    let enter n =
        let n64 = Int64.bits_of_float n in
        let e11 = e_of_u64 n64 |> Int64.to_int in
        let f52 = f_of_u64 n64 in
        let e5 =
            match e11 with
            | 0b000_0000_0000 -> 0b0_0000
            | 0b111_1111_1111 -> 0b1_1111
            | v ->
                match v - 1023 with
                | v when v < (-15) -> 0
                | v when v > 15 -> 31
                | v -> v + 15
        in
        let f =
            if e5 < 31 then begin
                if e5 > 0 then
                    (* normal *)
                    Int64.(shift_right_logical f52 42 |> to_int)
                else if e11 >= 999 then begin
                    (* subnormal *)
                    let ex = e11 - 999 in
                    let f10 =
                        Int64.(shift_right_logical f52 (52 - ex) |> to_int)
                    in
                    (1 lsl ex) lor f10
                end
                else
                    (* zero *)
                    0
            end
            else if f52 <> 0L then
                0x200 (* NaN *)
            else
                0 (* Infinity *)
        in
        let s = if is_neg n64 then 0x8000 else 0 in
        let e = e5 lsl 10 in
        s lor e lor f
    in
    enter

let to_fp32_bits_unsafe = Int32.bits_of_float

let to_fp16_bits n =
    let bits = to_fp16_bits_unsafe n in
    if n <> of_fp16_bits bits && not (is_nan n) then raise Imprecise;
    bits

let to_fp32_bits n =
    let bits = Int32.bits_of_float n in
    if n <> Int32.float_of_bits bits && not (is_nan n) then raise Imprecise;
    bits

module Safe(U: Unsafe) = struct
    module D = Cf_decode
    module E = Cf_encode

    let descript = U.descript

    let rds32 =
        let unboxed (D.Position i) s = U.lds32 s i in
        let boxed (D.Position i as p) s = U.ldi32 s i |> ck_signed_int32 p in
        if Sys.int_size > 31 then unboxed else boxed

    let rdu32 =
        let unboxed (D.Position i) s = U.ldu32 s i in
        let boxed (D.Position i as p) s = U.ldi32 s i |> ck_unsigned_int32 p in
        if Sys.int_size > 31 then unboxed else boxed

    let rds64 (D.Position i as p) s = U.ldi64 s i |> ck_signed_int64 p
    let rdu64 (D.Position i as p) s = U.ldi64 s i |> ck_unsigned_int64 p

    let rdf16 (D.Position i) s = U.ldu16 s i |> of_fp16_bits
    let rdf32 (D.Position i) s = U.ldi32 s i |> Int32.float_of_bits
    let rdf64 (D.Position i) s = U.ldi64 s i |> Int64.float_of_bits

    let ldu8 = D.valid_fixed_width 1 U.ldu8
    let lds8 = D.valid_fixed_width 1 U.lds8
    let ldu16 = D.valid_fixed_width 2 U.ldu16
    let lds16 = D.valid_fixed_width 2 U.lds16
    let ldu32 = D.fixed_width 4 rdu32
    let lds32 = D.fixed_width 4 rds32
    let ldu64 = D.fixed_width 8 rdu64
    let lds64 = D.fixed_width 8 rds64
    let ldi32 = D.valid_fixed_width 4 U.ldi32
    let ldi64 = D.valid_fixed_width 8 U.ldi64
    let ldf16 = D.fixed_width 2 rdf16
    let ldf32 = D.fixed_width 4 rdf32
    let ldf64 = D.fixed_width 8 rdf64

    let wrf16 v b i = U.stu16 (to_fp16_bits v) b i
    let wrf32 v b i = U.sti32 (to_fp32_bits v) b i
    let wrf64 v b i = U.sti64 (Int64.bits_of_float v) b i

    let sts8 = E.scheme 1 req_valid_s8 U.sts8
    let stu8 = E.scheme 1 req_valid_u8 U.stu8
    let sts16 = E.scheme 2 req_valid_s16 U.sts16
    let stu16 = E.scheme 2 req_valid_u16 U.stu16
    let sts32 = E.scheme 4 req_valid_s32 U.sts32
    let stu32 = E.scheme 4 req_valid_u32 U.stu32
    let sts64 = E.scheme 8 (no_invalid 8) U.sts64
    let stu64 = E.scheme 8 req_valid_u64 U.stu64
    let sti32 = E.scheme 4 (no_invalid 4) U.sti32
    let sti64 = E.scheme 8 (no_invalid 8) U.sti64
    let stf16 = E.scheme 2 (no_invalid 2) wrf16
    let stf32 = E.scheme 4 (no_invalid 4) wrf32
    let stf64 = E.scheme 8 (no_invalid 8) wrf64
end

(*--- End ---*)
