# src/cf - The Orsetto Core Foundation

Orsetto has minimal external dependencies beyond the OCaml standard library.
Accordingly, general purpose data structures and miscellaneous utilities not
found in the standard library are provided to all the other components by the
modules in this component.

The following modules are intended to comprise public interfaces:

- *Cf_annot* -- Annotation systems for locating scanner productions.
- *Cf_base16* -- The "base16" encoding (RFC 4648).
- *Cf_base32* -- The "base32" and "base32hex" encodings (RFC 4648).
- *Cf_base64* -- The "base64" and "base64url" encodings (RFC 4648).
- *Cf_bsearch* -- Abstraction of binary search algorithm.
- *Cf_bsearch_data* -- Sets and maps implemented with binary search tables.
- *Cf_chain_scan* -- Parsing sequences of elements separated by delimiters.
- *Cf_clockface* -- Clock time formats.
- *Cf_cmonad* -- Continuation monads.
- *Cf_data_ingest* -- Ingesting data according to an abstract model.
- *Cf_data_render* -- Rendering data according to an abstract model.
- *Cf_decode* -- Simple octet structure decoders.
- *Cf_deque* -- Near real-time functional double-ended queues.
- *Cf_dfa* -- Functional composition of deterministic finite automata.
- *Cf_disjoint_interval* -- Binary search tables with disjoint interval keys.
- *Cf_emit* -- Functional emitter/formatter combinators.
- *Cf_encode* -- Simple octet structure encoders.
- *Cf_endian* -- Byte-order sensitive binary formatted data.
- *Cf_gregorian* -- Gregorian calendar dates.
- *Cf_journal* -- A basic interface to diagnostic logging.
- *Cf_leap_seconds* -- Handling of the leap second archive.
- *Cf_lex_scan* -- Lexical analyzers for languages with regular subsets.
- *Cf_monad* -- Functors for generating interfaces with monad operators.
- *Cf_number_scan* -- Parsing textual representations of numeric types.
- *Cf_rbtree* -- Sets and maps implemented with red-black binary trees.
- *Cf_relations* -- Equivalence and total order relations on types.
- *Cf_regx* -- A basic regular expression engine for 8-bit ASCII texts.
- *Cf_sbheap* -- Priority queues implemented with skew-binomial heaps.
- *Cf_scan* -- Functional scanner/parser combinators.
- *Cf_scmonad* -- State-continuation monads.
- *Cf_seq* -- Functional progressive sequences (like Stdlib.Seq).
- *Cf_seqmonad* -- Sequence comprehension monad.
- *Cf_slice* -- Substrings and vector slices.
- *Cf_slice_big* -- Slices of Stdlib.Bigarray vectors.
- *Cf_smonad* -- State monads.
- *Cf_stdtime* -- Date/time stamps.
- *Cf_structure_scan* -- Parsing structured sequences of key/value pairs.
- *Cf_tai64* -- The TAI64 timestamp format.
- *Cf_tai64n* -- The TAI64N timestamp format (nanosecond resolution).
- *Cf_type* -- Extensible runtime type indications and equality constraints.
- *Cf_uri* -- Decomposition of Uniform Resource Identifier (URI) values.

The following modules are internal to Orsetto, typically used as inclusions in
the public modules, and no commitment to their separate stability is offered.

- *Cf_endian_core* -- Partial implementation of *Cf_endian* safe operations.
- *Cf_endian_big* -- Implementation of *Cf_endian.BE*.
- *Cf_endian_little* -- Implementation of *Cf_endian.LE*.
- *Cf_index_node* -- Index nodes for *Cf_rbtree* and *Cf_sbheap*.
- *Cf_monad_core* -- Partial implementation of the *Cf_monad* module.
- *Cf_radix_n* -- Implementation of Radix-N formats, e.g. Base32, Base64. etc.

The remaining modules are obsolescent and preferable alternatives are provided.

- *Cf_record_scan* -- Parsing structured sequences of key/value pairs.
