(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let validate_ab ~a ?b fn =
    let msg = Printf.sprintf "%s.%s: %s" __MODULE__ fn in
    if a < 0 then msg "a < 0" |> invalid_arg;
    match b with
    | None -> ()
    | Some b -> if b < a then msg "b < a" |> invalid_arg

type ctrl = [ `Non | `Opt | `Req ]

module type Basis = sig
    type symbol
    type position
    type +'a form

    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form
end

module type Profile = sig
    type symbol
    type +'a form
    type mark
    type +'a t

    type chain = private Chain: {
        separator: unit t;
        header: ctrl;
        trailer: ctrl;
        fail: (mark -> 'x) option;
    } -> chain

    val mk: ?xf:(mark -> 'x) -> ?a:[< ctrl ] -> ?b:[< ctrl ] -> 'r t -> chain
    val sep: ?xf:(mark -> 'x) -> [< ctrl ] -> 'r -> 'r t -> 'r t
    val sep0: ?xf:(mark -> 'x) -> [< ctrl ] -> unit t -> bool t
    val vis: c:chain -> ?a:int -> ?b:int -> ('r -> 'r t) -> 'r -> 'r t
    val seq: c:chain -> ?a:int -> ?b:int -> 'r t -> 'r list t
end

module Create(B: Basis) = struct
    module S = B.Scan
    open S.Affix

    type mark = S.mark
    type 'r t = 'r S.t

    type chain = Chain: {
        separator: unit t;
        header: ctrl;
        trailer: ctrl;
        fail: (mark -> 'x) option;
    } -> chain

    let mk =
        let mkctrl = function None -> `Non | Some v -> (v :> ctrl) in
        let enter ?xf ?a ?b p =
            let header = mkctrl a and trailer = mkctrl b in
            let separator = S.disregard p in
            let fail = xf in
            Chain { separator; header; trailer; fail }
        in
        enter

    let sep ?xf c r p =
        match (c :> ctrl) with
        | `Non -> S.return r
        | `Opt -> begin
            S.opt p >>= function
            | None -> S.return r
            | Some r -> S.return r
          end
        | `Req -> S.reqf ?xf p

    let sep0 ?xf c p =
        match (c :> ctrl) with
        | `Non -> S.return false
        | `Opt -> let* vopt = S.opt p in S.return (vopt <> None)
        | `Req -> let* _ = S.reqf ?xf p in S.return true

    type 'a w = Work of {
        min: int;
        max: int option;
        chain: chain;
        scan: 'a -> 'a t;
    }

    let vis0 =
        let empty (Chain c) =
            match c.header, c.trailer with
            | `Non, ctrl -> ctrl
            | `Opt, `Non -> `Opt
            | `Opt, ctrl -> ctrl
            | `Req, _ -> `Req
        in
        let nextw w0 =
            let Work w = w0 in
            let scan v =
                let Chain c = w.chain in
                let* _ = c.separator in
                w.scan v
            in
            Work { w with scan }
        in
        let first w0 v =
            let Work w = w0 in
            let Chain c = w.chain in
            let* prefixed = sep0 ?xf:c.fail c.header c.separator in
            let p = w.scan v in
            if prefixed then S.reqf ?xf:c.fail p else p
        and tail (Chain c) v =
            let+ _ = sep0 ?xf:c.fail c.trailer c.separator in v
        in
        let rec loop i w0 v =
            let Work w = w0 in
            match w.max with
            | Some n when i >= n ->
                tail w.chain v
            | (None | Some _) ->
                S.opt (w.scan v) >>= function
                | Some v -> loop (succ i) w0 v
                | None -> tail w.chain v
        in
        let enter w0 v =
            let Work w = w0 in
            match w.max with
            | Some 0 ->
                let Chain c = w.chain in
                let ctrl = empty w.chain in
                let* _ = sep0 ?xf:c.fail ctrl c.separator in
                S.return v
            | (None | Some _) ->
                if w.min > 0 then
                    first w0 v >>= loop 1 (nextw w0)
                else begin
                    S.opt (first w0 v) >>= function
                    | None -> tail w.chain v
                    | Some v -> loop 1 (nextw w0) v
                end
        in
        enter

    let vis ~c ?(a = 0) ?b f v =
        validate_ab ~a ?b "vis";
        let w0 = Work { min = a; max = b; chain = c; scan = f } in
        vis0 w0 v

    let seq =
        let push p tl = let* hd = p in S.return (hd :: tl) in
        let enter ~c ?(a = 0) ?b p =
            validate_ab ~a ?b "seq";
            let w0 = Work { min = a; max = b; chain = c; scan = push p } in
            vis0 w0 [] >>: List.rev
        in
        enter
end

module ASCII = struct
    module Aux = struct
        type symbol = char
        type position = Cf_scan.Simple.position
        type 'a form = 'a

        module Scan = Cf_scan.ASCII
    end

    include Create(Aux)
end

(*--- End ---*)
