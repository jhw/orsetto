(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Unary = struct
    module type Basis = sig
        type +'r t

        val return: 'r -> 'r t
        val bind: 'a t -> ('a -> 'b t) -> 'b t

        val mapping: [
            | `Default
            | `Special of ('a t -> f:('a -> 'b) -> 'b t)
        ]

        val product: [
            | `Default
            | `Special of ('a t -> 'b t -> ('a * 'b) t)
        ]
    end

    module type Affix = sig
        type +'r t

        val ( >>: ): 'a t -> ('a -> 'b) -> 'b t
        val ( >>= ): 'a t -> ('a -> 'b t) -> 'b t
        val ( let+ ): 'a t -> ('a -> 'b) -> 'b t
        val ( and+ ): 'a t -> 'b t -> ('a * 'b) t
        val ( let* ): 'a t -> ('a -> 'b t) -> 'b t
        val ( and* ): 'a t -> 'b t -> ('a * 'b) t
    end

    module type Profile = sig
        type +'r t

        val return: 'r -> 'r t
        val bind: 'a t -> ('a -> 'b t) -> 'b t
        val map: 'a t -> f:('a -> 'b) -> 'b t
        val product: 'a t -> 'b t -> ('a * 'b) t

        module Affix: Affix with type 'r t := 'r t

        val disregard: 'r t -> unit t
        module Infix = Affix
    end

    module Mk_affix(B: Basis) = struct
        let map =
            let ret = B.return and ( >>= ) = B.bind in
            match B.mapping with
            | `Default -> let def m ~f = m >>= fun a -> ret (f a) in def
            | `Special def -> def

        let product =
            let ret = B.return and ( >>= ) = B.bind in
            match B.product with
            | `Default ->
                let def a b =
                    a >>= fun r1 ->
                    b >>= fun r2 ->
                    ret (r1, r2)
                in def
            | `Special def ->
                def

        let ( >>: ) m f = map m ~f
        let ( >>= ) = B.bind
        let ( let+ ) = ( >>: )
        let ( and+ ) = product
        let ( let* ) = B.bind
        let ( and* ) = product
    end

    module Create(B: Basis) = struct
        module Affix = Mk_affix(B)

        let return = B.return
        let bind = B.bind
        let map = Affix.map
        let product = Affix.product

        let disregard m = let f _ = () in Affix.map m ~f
        module Infix = Affix
    end

    module type Infix = Affix
end

module Binary = struct
    module type Basis = sig
        type ('m, +'r) t

        val return: 'r -> ('m, 'r) t
        val bind: ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t

        val mapping: [
            | `Default
            | `Special of (('m, 'a) t -> f:('a -> 'b) -> ('m, 'b) t)
        ]

        val product: [
            | `Default
            | `Special of (('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t)
        ]
    end

    module type Affix = sig
        type ('m, +'r) t

        val ( >>: ): ('m, 'a) t -> ('a -> 'b) -> ('m, 'b) t
        val ( >>= ): ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t
        val ( let+ ): ('m, 'a) t -> ('a -> 'b) -> ('m, 'b) t
        val ( and+ ): ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t
        val ( let* ): ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t
        val ( and* ): ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t
    end

    module type Profile = sig
        type ('m, +'r) t

        val return: 'r -> ('m, 'r) t
        val bind: ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t
        val map: ('m, 'a) t -> f:('a -> 'b) -> ('m, 'b) t
        val product: ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t

        module Affix: Affix with type ('m, 'r) t := ('m, 'r) t

        val disregard: ('m, 'r) t -> ('m, unit) t
        module Infix = Affix
    end

    module Mk_affix(B: Basis) = struct
        let map =
            let ret = B.return and ( >>= ) = B.bind in
            match B.mapping with
            | `Default -> let def m ~f = m >>= fun a -> ret (f a) in def
            | `Special def -> def

        let product =
            let ret = B.return and ( >>= ) = B.bind in
            match B.product with
            | `Default ->
                let def a b =
                    a >>= fun r1 ->
                    b >>= fun r2 ->
                    ret (r1, r2)
                in def
            | `Special def ->
                def

        let ( >>: ) m f = map m ~f
        let ( >>= ) = B.bind
        let ( let+ ) = ( >>: )
        let ( and+ ) = product
        let ( let* ) = B.bind
        let ( and* ) = product
    end

    module Create(B: Basis) = struct
        module Affix = Mk_affix(B)

        let return = B.return
        let bind = B.bind
        let map = Affix.map
        let product = Affix.product

        let disregard m = let f _ = () in Affix.map m ~f
        module Infix = Affix
    end

    module type Infix = Affix
end

module Trinary = struct
    module type Basis = sig
        type ('p, 'q, +'r) t

        val return: 'r -> ('p, 'q, 'r) t
        val bind: ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t

        val mapping: [
            | `Default
            | `Special of
                (('p, 'q, 'a) t -> f:('a -> 'b) -> ('p, 'q, 'b) t)
        ]

        val product: [
            | `Default
            | `Special of (('p, 'q, 'a) t -> ('p, 'q, 'b) t ->
                ('p, 'q, 'a * 'b) t)
        ]
    end

    module type Affix = sig
        type ('p, 'q, +'r) t

        val ( >>: ): ('p, 'q, 'a) t -> ('a -> 'b) -> ('p, 'q, 'b) t
        val ( >>= ): ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t
        val ( let+ ): ('p, 'q, 'a) t -> ('a -> 'b) -> ('p, 'q, 'b) t
        val ( let* ):
            ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t
        val ( and+ ): ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t
        val ( and* ): ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t
    end

    module type Profile = sig
        type ('p, 'q, +'r) t

        val return: 'r -> ('p, 'q, 'r) t
        val bind: ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t
        val map: ('p, 'q, 'a) t -> f:('a -> 'b) -> ('p, 'q, 'b) t
        val product: ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t

        module Affix: Affix with type ('p, 'q, 'r) t := ('p, 'q, 'r) t
        module Infix = Affix

        val disregard: ('p, 'q, 'r) t -> ('p, 'q, unit) t
    end

    module Mk_affix(B: Basis) = struct
        let map =
            let ret = B.return and ( >>= ) = B.bind in
            match B.mapping with
            | `Default -> let def m ~f = m >>= fun a -> ret (f a) in def
            | `Special def -> def

        let product =
            let ret = B.return and ( >>= ) = B.bind in
            match B.product with
            | `Default ->
                let def a b =
                    a >>= fun r1 ->
                    b >>= fun r2 ->
                    ret (r1, r2)
                in def
            | `Special def ->
                def

        let ( >>: ) m f = map m ~f
        let ( >>= ) = B.bind
        let ( let+ ) = ( >>: )
        let ( and+ ) = product
        let ( let* ) = B.bind
        let ( and* ) = product
    end

    module Create(B: Basis) = struct
        module Affix = Mk_affix(B)

        let return = B.return
        let bind = B.bind
        let map = Affix.map
        let product = Affix.product

        let disregard m = let f _ = () in Affix.map m ~f
        module Infix = Affix
    end

    module type Infix = Affix
end

(*--- End ---*)
