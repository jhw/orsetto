(*---------------------------------------------------------------------------*
  Copyright (C) 2011-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Form = sig
    type +'a t

    val imp: 'a -> 'a t
    val dn: 'a t -> 'a
    val mv: 'a -> 'b t -> 'a t
    val span: 'a t -> 'b t -> 'c -> 'c t
end

module type Basis = sig
    module Symbol: Cf_relations.Equal
    module Form: Form

    type position
    type iota

    val init: ?start:position -> Symbol.t -> iota
    val next: iota -> Symbol.t -> iota
    val sym: iota -> Symbol.t
    val term: iota -> Symbol.t Form.t
end

module type Profile = sig

    type +'r t
    include Cf_monad.Unary.Profile with type +'r t := 'r t

    val nil: 'r t
    val fin: bool t

    type mark
    type +'a form

    val cur: mark t
    val mov: mark -> unit t
    val pos: mark -> unit form

    type symbol

    val any: symbol form t
    val one: symbol -> symbol form t
    val sat: (symbol -> bool) -> symbol form t
    val ign: (symbol -> bool) -> unit form t
    val tok: (symbol -> 'r option) -> 'r form t

    val ntyp: 'r Cf_type.nym -> 'r form t -> Cf_type.opaque form t
    val dflt: 'r -> 'r form t -> 'r form t
    val opt: 'r t -> 'r option t
    val vis: ?a:int -> ?b:int -> ('r -> 'r t) -> 'r -> 'r t
    val seq: ?a:int -> ?b:int -> 'r t -> 'r list t
    val alt: 'r t list -> 'r t
    val altz: 'r t Seq.t -> 'r t

    type exn += Bad_syntax of string form
    val fail: string -> 'r t
    val or_fail: string -> 'r t -> 'r t

    val err: ?x:exn -> unit -> 'r t
    val errf: ?xf:(mark -> 'x) -> unit -> 'r t
    val req: ?x:exn -> 'r t -> 'r t
    val reqf: ?xf:(mark -> 'x) -> 'r t -> 'r t
    val cast: ('a -> 'b) -> 'a form t -> 'b form t
    val ck: 'r t -> ('r, exn) result t
    val sync: 'r t -> 'r option t

    type position
    val lift: ?start:position -> 'r t -> symbol Seq.t -> 'r Seq.t
    val of_seq: ?start:position -> 'r t -> symbol Seq.t -> 'r

    module Affix: sig
        include module type of Affix

        val ( ?. ): symbol -> symbol form t
        val ( ?/ ): 'r t -> 'r option t
        val ( ?+ ): 'r t -> ('r * 'r list) t
        val ( ?* ): 'r t -> 'r list t
        val ( ?^ ): 'r t list -> 'r t
        val ( ?^~ ): 'r t Seq.t -> 'r t
    end
end

let validate_ab ~a ?b fn =
    let msg = Printf.sprintf "%s.%s: %s" __MODULE__ fn in
    if a < 0 then msg "a < 0" |> invalid_arg;
    match b with
    | None -> ()
    | Some b -> if b < a then msg "b < a" |> invalid_arg

module Create(B: Basis) = struct
    type iota = B.iota

    type 'r t = iota Seq.t -> ('r * iota Seq.t) option

    module Basis = struct
        type nonrec 'r t = 'r t

        let return a s = Some (a, s)
        let bind m f s = match m s with None -> None | Some (r, s) -> f r s
        let product = `Default
        let mapping = `Default
    end

    include Cf_monad.Unary.Create(Basis)

    let nil _ = None
    let fin s = Some (Cf_seq.empty s, s)

    type 'a form = 'a B.Form.t
    module Form = B.Form

    type mark = Mark of iota Seq.t [@@caml.unboxed]

    let cur s = Some (Mark s, s)
    let mov (Mark s) _ = Some ((), s)

    let pos (Mark s) =
        match s () with
        | Seq.Nil -> Form.imp ()
        | Seq.Cons (i, _) -> B.term i |> Form.mv ()

    let any s =
        match s () with
        | Seq.Cons (i, s) -> Some (B.term i, s)
        | Seq.Nil -> None

    let one v0 s =
        match s () with
        | Seq.Cons (i, s) ->
            let v = B.sym i in
            if v0 == v || B.Symbol.equal v0 v then Some (B.term i, s) else None
        | Seq.Nil ->
            None

    let sat f s =
        match s () with
        | Seq.Cons (i, s) when f (B.sym i) -> Some (B.term i, s)
        | (Seq.Cons _ | Seq.Nil) -> None

    let ign =
        let rec loop syn fin f s =
            match s () with
            | Seq.Nil ->
                Some (Form.span syn fin (), s)
            | Seq.Cons (i, s') ->
                let v = B.term i in
                if B.sym i |> f then
                    (loop[@tailcall]) syn v f s'
                else
                    Some (Form.span syn v (), s)
        in
        let enter f s =
            match s () with
            | Seq.Nil ->
                Some (Form.imp (), Seq.empty)
            | Seq.Cons (i, s') ->
                let v = B.term i in
                if B.sym i |> f then
                    (loop[@tailcall]) v v f s'
                else
                    Some (Form.mv () v, s)
        in
        enter

    let tok f s =
        match s () with
        | Seq.Nil ->
            None
        | Seq.Cons (i, s) ->
            match f (B.sym i) with
            | Some r -> Some (Form.mv r (B.term i), s)
            | None -> None

    let ntyp (type n) (nym : n Cf_type.nym) (p : n form t) s =
        match p s with
        | Some (vl, s) -> Some (Form.(mv (Cf_type.witness nym (dn vl)) vl), s)
        | None -> None

    let dflt v p s =
        match p s with None -> Some (Form.imp v, s) | some -> some

    let opt p s =
        Some (match p s with None -> None, s | Some (r, s) -> Some r, s)

    let rec vis0 w i r s =
        let a, b, f = w in
        match b with
        | Some b when i == b ->
            Some (r, s)
        | (None | Some _) ->
            match f r s with
            | None ->
                if i < a then None else Some (r, s)
            | Some (r, s) ->
                (vis0[@tailcall]) w (succ i) r s

    let vis ?(a = 0) ?b f v =
        validate_ab ~a ?b "vis";
        vis0 (a, b, f) 0 v

    let seq =
        let open Affix in
        let push p tl = let* hd = p in return @@ hd :: tl in
        let enter ?(a = 0) ?b p =
            validate_ab ~a ?b "seq";
            vis0 (a, b, push p) 0 [] >>: List.rev
        in
        enter

    let rec alt ps s =
        match ps with
        | [] -> None
        | hd :: tl ->
            match hd s with
            | None -> alt tl s
            | Some _ as r -> r

    let rec altz ps s =
        match ps () with
        | Seq.Nil -> None
        | Seq.Cons (hd, tl) ->
            match hd s with
            | None -> altz tl s
            | Some _ as r -> r

    exception Bad_syntax of string form

    let fail msg s = Bad_syntax (Form.mv msg (pos (Mark s))) |> raise

    let or_fail msg p s =
        match p s with
        | None -> fail msg s
        | Some _ as r -> r

    let err ?(x = Not_found) () _ = raise x

    let errf =
        let aux _ = raise Not_found in
        fun ?(xf = aux) () s -> let m = Mark s in let _ = xf m in aux s

    let req ?x p s = match p s with None -> err ?x () s | Some _ as r -> r

    let reqf ?xf p s =
        match p s with
        | None -> errf ?xf () s
        | Some _ as r -> r

    let cast f p s =
        let _ = s () in
        match p s with
        | None ->
            None
        | Some (rl, s) ->
            match f (Form.dn rl) with
            | exception Not_found -> None
            | exception (Failure s) -> raise (Bad_syntax (Form.mv s rl))
            | v -> Some (Form.mv v rl, s)

    let ck p s =
        try
            match p s with
            | None -> None
            | Some (r, s) -> Some (Ok r, s)
        with
        | x ->
            Some (Error x, s)

    let rec sync p s =
        match p s with
        | Some (r, s) ->
            Some (Some r, s)
        | None ->
            match s () with
            | Seq.Cons (_, s) -> sync p s
            | Seq.Nil -> Some (None, s)

    let annot =
        let rec loop iota tl () =
            match tl () with
            | Seq.Nil ->
                Seq.Nil
            | Seq.Cons (hd, tl) ->
                let iota = B.next iota hd in
                Seq.Cons (iota, loop iota tl)
        in
        let enter ?start tl () =
            match tl () with
            | Seq.Nil ->
                Seq.Nil
            | Seq.Cons (hd, tl) ->
                let iota = B.init ?start hd in
                Seq.Cons (iota, loop iota tl)
        in
        enter

    let lift =
        let rec loop p tl () =
            match p tl with
            | None -> Seq.Nil
            | Some (r, tl) -> Seq.Cons (r, loop p tl)
        in
        let enter ?start p s = annot ?start s |> loop p |> Cf_seq.persist in
        enter

    let of_seq ?start p s =
        match p @@ annot ?start s with
        | Some (r, s) when Cf_seq.empty s -> r
        | (Some _ | None) -> raise Not_found

    module Affix = struct
        include Affix

        let ( ?. ) = one
        let ( ?/ ) = opt
        let ( ?* ) p = seq p
        let ( ?+ ) p = product p (seq p)
        let ( ?^ ) = alt
        let ( ?^~ ) = altz
    end
end

module Simple = struct
    type position

    module type Basis =
        Basis with type position = position and type 'a Form.t = 'a

    module type Profile =
        Profile with type position := position and type 'a form := 'a

    module Form = struct
        type 'a t = 'a

        let imp v = v
        let dn v = v
        let mv v _ = v
        let span _ _ v = v
    end

    module Basis(S: Cf_relations.Equal) = struct
        module Symbol = S
        module Form = Form

        type iota = S.t
        type nonrec position = position

        let sym v = v
        let init ?start:_ v = v
        let next _ v = v
        let term v = v
    end

    module Create(S: Cf_relations.Equal) = struct
        include Create(Basis(S))

        let pos (Mark _) = ()

        let any s =
            match s () with
            | Seq.Nil -> None
            | Seq.Cons (v, s) -> Some (v, s)

        let one v0 s =
            match s () with
            | Seq.Cons (v, s) ->
                if v0 == v || S.equal v0 v then Some (v, s) else None
            | Seq.Nil ->
                None

        let sat f s =
            match s () with
            | Seq.Cons (v, s) when f v -> Some (v, s)
            | (Seq.Cons _ | Seq.Nil) -> None

        let rec ign f s =
            match s () with
            | Seq.Nil ->
                Some ((), s)
            | Seq.Cons (v, s') ->
                if f v then (ign[@tailcall]) f s' else Some ((), s)

        let tok f s =
            match s () with
            | Seq.Nil ->
                None
            | Seq.Cons (v, s) ->
                match f v with
                | Some r -> Some (r, s)
                | None -> None

        let lift =
            let rec loop p tl () =
                match p tl with
                | None -> Seq.Nil
                | Some (r, tl) -> Seq.Cons (r, loop p tl)
            in
            let enter ?start:_ p s = loop p s |> Cf_seq.persist in
            enter
    end
end

module Staging = struct
    module type Basis = sig
        type symbol
        type position

        module Token: Cf_relations.Equal
        module Form: Form

        module Scan: Profile
           with type symbol := symbol
            and type position := position
            and type 'a form := 'a Form.t

        val lexer: Token.t Form.t Scan.t
    end

    module type Profile = sig
        type token
        include Profile with type symbol := token

        type symbol
        val lift_staged: ?start:position -> 'r t -> symbol Seq.t -> 'r Seq.t
        val of_seq_staged: ?start:position -> 'r t -> symbol Seq.t -> 'r
    end

    module Create(B: Basis) = struct
        module Aux = struct
            module Symbol = B.Token
            module Form = B.Form

            type position = B.position
            type iota = B.Token.t B.Form.t

            let sym = Form.dn

            let init ?start:_ _ = assert false
            let next _ _ = assert false
            let term _ = assert false
        end

        include Create(Aux)

        let pos (Mark s) =
            match s () with
            | Seq.Nil -> Form.imp ()
            | Seq.Cons (i, _) -> Form.mv () i

        let any s =
            match s () with
            | Seq.Nil -> None
            | Seq.Cons (i, s) -> Some (i, s)

        let one i0 s =
            match s () with
            | Seq.Cons (i, s) when B.Token.equal i0 (Form.dn i) -> Some (i, s)
            | (Seq.Cons _ | Seq.Nil) -> None

        let sat f s =
            match s () with
            | Seq.Cons (i, s) when f (Form.dn i) -> Some (i, s)
            | (Seq.Cons _ | Seq.Nil) -> None

        let ign =
            let rec loop syn fin f s =
                match s () with
                | Seq.Nil ->
                    Some (Form.span syn fin (), s)
                | Seq.Cons (i, s') ->
                    if f (Form.dn i) then
                        (loop[@tailcall]) syn i f s'
                    else
                        Some (Form.span syn i (), s)
            in
            let enter f s =
                match s () with
                | Seq.Nil ->
                    Some (Form.imp (), Seq.empty)
                | Seq.Cons (i, s') ->
                    if f (Form.dn i) then
                        (loop[@tailcall]) i i f s'
                    else
                        Some (Form.mv () i, s)
            in
            enter

        let tok f s =
            match s () with
            | Seq.Nil ->
                None
            | Seq.Cons (i, s) ->
                match f (Form.dn i) with
                | Some r -> Some (Form.mv r i, s)
                | None -> None

        let fail msg s = Bad_syntax (Form.mv msg (pos (Mark s))) |> raise

        let or_fail msg p s =
            match p s with
            | None -> fail msg s
            | Some _ as r -> r

        let lift_staged =
            let rec loop p tl () =
                match p tl with
                | exception B.Scan.Bad_syntax xl ->
                    Bad_syntax xl |> raise
                | Some (r, tl) ->
                    Seq.Cons (r, loop p tl)
                | None ->
                    match tl () with
                    | Seq.Cons (i, _) ->
                        Bad_syntax (Form.mv "unrecognized symbol" i) |> raise
                    | Seq.Nil ->
                        Seq.Nil
            in
            let enter ?start p s =
                B.Scan.lift ?start B.lexer s |> loop p |> Cf_seq.persist
            in
            enter

        let of_seq_staged ?start p s =
            match B.Scan.lift ?start B.lexer s |> p with
            | exception B.Scan.Bad_syntax xl -> Bad_syntax xl |> raise
            | Some (r, s) when Cf_seq.empty s -> r
            | (Some _ | None) -> raise Not_found
    end
end

module ASCII = struct
    include Simple.Create(Char)

    let one c0 s =
        match s () with
        | Seq.Cons (c, s) when c == c0 -> Some (c0, s)
        | (Seq.Cons _ | Seq.Nil) -> None

    let rws, rwsl  =
        let ckl = function '\009' | '\032' -> true | _ -> false in
        let ck = function
            | '\009'..'\013' | '\032' | '\133' -> true
            | _ -> false
        in
        let rec loop s =
            match s () with
            | Seq.Cons (c, s) when ck c ->
                (loop[@tailcall]) s
            | (Seq.Cons _ | Seq.Nil as s0) ->
                let s () = s0 in
                Some ((), s)
        in
        let enter ck s =
            match s () with
            | Seq.Cons (c, s) when ck c ->
                (loop[@tailcall]) s
            | Seq.Cons _
            | Seq.Nil ->
                None
        in
        enter ck, enter ckl

    let ows = opt rws
    let owsl = opt rwsl

    let lit k x =
        let klen = String.length k in
        let rec loop i s =
            if i < klen then
                match s () with
                | Seq.Cons (hd, tl) when String.unsafe_get k i = hd ->
                    loop (succ i) tl
                | Seq.Cons _
                | Seq.Nil ->
                    None
            else
                Some (x, s)
        in
        loop 0

    let fmt =
        let next u () =
            match !u () with
            | Seq.Nil -> raise End_of_file
            | Seq.Cons (hd, tl) -> u := tl; hd
        in
        let enter fstr rf s =
            let u = ref s in
            let b = next u |> Scanf.Scanning.from_function in
            let r = Scanf.bscanf b fstr rf in
            let s = !u in
            Some (r, s)
        in
        enter

    let escape up dn s =
        match s () with
        | Seq.Nil ->
            None
        | Seq.Cons (c, s) ->
            if Char.equal c up then begin
                match s () with
                | Seq.Nil ->
                    None
                | Seq.Cons (c, s) ->
                    if Char.equal c up || Char.equal c dn then
                        Some (c, s)
                    else
                        None
            end
            else
                None

    let quoted =
        let rec loop esc syn b s =
            match esc s with
            | Some (c, s) ->
                Buffer.add_char b c;
                (loop[@tailcall]) esc syn b s
            | None ->
                match s () with
                | Seq.Nil ->
                    None
                | Seq.Cons (c, s) ->
                    if not (Char.equal c syn) then begin
                        Buffer.add_char b c;
                        (loop[@tailcall]) esc syn b s
                    end
                    else
                        Some (Buffer.contents b, s)
        in
        let enter ?esc syn s =
            match s () with
            | Seq.Nil ->
                None
            | Seq.Cons (c, s) ->
                if Char.equal c syn then begin
                    let b = Buffer.create 0 in
                    let esc =
                        match esc with
                        | None -> escape '\\' syn
                        | Some p -> p
                    in
                    (loop[@tailcall]) esc syn b s
                end
                else
                    None
        in
        enter

    let of_string p s = of_seq p @@ String.to_seq s
end

(*--- End ---*)
