(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional skew-binomial heaps. *)

(** {6 Overview}

    This module implements functional heaps and priority queues based on
    skew-binomial heaps. The underlying algorithm can be found in Chris
    Okasaki's {{:http://www.cs.cmu.edu/~rwh/theses/okasaki.pdf}Ph.D. thesis}.
    These data structures have O(1) cost in space and time for most operations,
    including [merge].
*)

(** {6 Interfaces} *)

(** Interfaces to persistent functional heap data structures. *)
module Heap: sig

    (** The module type of functional persistent heaps. *)
    module type Profile = sig

        (* The type of elements. *)
        type element

        (** The heap type. *)
        type t

        (** The empty heap. *)
        val nil: t

        (** Use [one e] to create a heap containing the single element [e]. *)
        val one: element -> t

        (** Use [empty h] to test whether [h] is empty. *)
        val empty: t -> bool

        (** Use [size h] to compute the number of elements in [h]. Runs in
            O(n) time and O(log N) space.
        *)
        val size: t -> int

        (** Use [head h] to obtain the element on the top of the heap [h].
            Raises [Not_found] if the heap is empty.
        *)
        val head: t -> element

        (** Use [tail h] to obtain the heap produced by discarding the element
            at the top of [h].  If [h] is the empty heap, then the empty heap
            is returned.
        *)
        val tail: t -> t

        (** Use [pop h] to obtain the head and the tail of a heap [h] in one
            operation.  Returns [None] if the [h] is empty.
        *)
        val pop: t -> (element * t) option

        (** Use [put e h] to obtain a new heap that is the result of inserting
            the element [e] into the heap [h].
        *)
        val put: element -> t -> t

        (** Use [merge h1 h2] to obtain a new heap that is the result of
            merging all the elements of [h1] and [h2] into a single heap.
        *)
        val merge: t -> t -> t

        (** Use [of_seq s] to construct a heap from a sequence of elements.
            Evaluates the whole sequence. Runs in O(n) time and O(1) space.
        *)
        val of_seq: element Seq.t -> t

        (** Use [to_seq h] to produce a sequence of the elements in [h] from
            top to bottom.
        *)
        val to_seq: t -> element Seq.t
    end

    (** A functor to create a [Heap] module. *)
    module Create(E: Cf_relations.Order): Profile with type element := E.t
end

(** Interfaces to persistent functional priority queue data structures. *)
module PQueue: sig

    (** The module type of functional persistent priority queues. *)
    module type Profile = sig

        (** The priorities type. *)
        type priority

        (** The priority queue type. *)
        type +'a t

        (** The empty priority queue. *)
        val nil: 'a t

        (** Use [one kv] to create a priority queue containing the single
            priority-value pair [kv].
        *)
        val one: (priority * 'a) -> 'a t

        (** Use [empty q] to test whether [q] is empty. *)
        val empty: 'a t -> bool

        (** Use [size q] to compute the number of elements in [q]. Runs in
            O(n) time and O(log N) space.
        *)
        val size: 'a t -> int

        (** Use [head q] to obtain the key-value pair on the top of [q]. Raises
            [Not_found] if [q] is empty.
        *)
        val head: 'a t -> (priority * 'a)

        (** Use [tail q] to obtain the priority queue produced by discarding
            the element at the top of [q].  If [q] is the empty queue, then the
            empty queue is returned.
        *)
        val tail: 'a t -> 'a t

        (** Use [pop q] to obtain the head and the tail of a priority queue [q]
            in one operation.  Returns [None] if [q] is empty.
        *)
        val pop: 'a t -> ((priority * 'a) * 'a t) option

        (** Use [put kv q] to obtain a new priority queue that is the result of
            inserting the key-value pair [kv] into [q].
        *)
        val put: (priority * 'a) -> 'a t -> 'a t

        (** Use [merge q1 q2] to obtain a new priority queue that is the result
            of merging all the elements of [q1] and [q2] into a single queue.
        *)
        val merge: 'a t -> 'a t -> 'a t

        (** Use [of_seq s] to construct a priority queue from a sequence of
            key-value pairs. Evaluates the whole sequence. Runs in O(n) time
            and O(1) space.
        *)
        val of_seq: (priority * 'a) Seq.t -> 'a t

        (** Use [to_seq q] to produce a sequence of the key-value pairs in [q]
            from top to bottom.
        *)
        val to_seq: 'a t -> (priority * 'a) Seq.t
    end

    (** A functor to create a [PQueue] module. *)
    module Create(K: Cf_relations.Order): Profile with type priority := K.t
end

(*--- End ---*)
