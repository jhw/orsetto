(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type size = Size of int [@@ocaml.unboxed]
type position = Position of int [@@ocaml.unboxed]

let uintreq n =
    if n < 0 then invalid_arg (__MODULE__ ^ ": non-negative integer required!")

let overflow () =
    failwith (__MODULE__ ^ ": octet sequence length overflow!")

let addsize na nb =
    if Sys.max_string_length - na < nb then overflow ();
    na + nb

let mulsize n x =
    if x > 0 && Sys.max_string_length / x < n then overflow ();
    x * n

let addpos (Position i) n = Position (i + n)

type 'x analysis = Analysis of int * 'x

exception Aux_incomplete of int
exception Aux_invalid of position * string

let invalid i m = raise (Aux_invalid (i, m))

let advance adj (Position i) =
    uintreq adj;
    Position (i + adj)

let ckwindow (Size have) need =
    uintreq have;
    uintreq need;
    if need > have then raise (Aux_incomplete (need - have)) else need

let analyze have need =
    let adj = ckwindow have need in
    fun x -> Analysis (adj, x)

type +'v scheme = Scheme: {
    sz: int;
    ck: string -> position -> size -> 'x analysis;
    rd: 'x -> string -> 'v;
  } -> 'v scheme

let scheme sz ck rd =
    uintreq sz;
    Scheme { sz; ck; rd }

let nil =
    let a = Analysis (0, ()) in
    let sz = 0 and ck _ _ _ = a and rd () _ = () in
    Scheme { sz; ck; rd }

let pos =
    let sz = 0 and ck _ p _ = Analysis (0, p) and rd p _ = p in
    Scheme { sz; ck; rd }

let any =
    let sz = 1 in
    let ck _ (Position i) _ = Analysis (1, i) in
    let rd i s = String.unsafe_get s i in
    Scheme { sz; ck; rd }

let sat =
    let sz = 1 in
    let ck f s (Position i as i0) need =
        let _ = ckwindow need 1 in
        let c = String.unsafe_get s i in
        if f c then
            Analysis (1, c)
        else
            invalid i0 (__MODULE__ ^ ".sat: unrecognized octet")
    in
    let rd c _ = c in
    let enter f =
        let ck = ck f in
        Scheme { sz; ck; rd }
    in
    enter

let lit =
    let rec ckloop s0 i0 n0 s (Position i) n =
        if i0 < n0 then begin
            let c0 = String.unsafe_get s0 i0 and c = String.unsafe_get s i in
            if c <> c0 then
                invalid (Position i) (__MODULE__ ^ ".lit: unrecognized octet");
            (ckloop[@tailcall]) s0 (succ i0) n0 s (Position (succ i)) n
        end
        else
            Analysis (n0, ())
    in
    let rd _ _ = () in
    let enter s0 =
        let sz = String.length s0 in
        let ck = ckloop s0 0 sz in
        Scheme { sz; ck; rd }
    in
    enter

let opaque sz =
    uintreq sz;
    let ck _ (Position i) _ = Analysis (sz, i) in
    let rd i b = String.sub b i sz in
    Scheme { sz; ck; rd }

let fixed_width sz rd =
    let ck _ p n = analyze n sz p in
    Scheme { sz; ck; rd }

let valid_fixed_width sz rd =
    let ck _ (Position i) n = analyze n sz i and rd p s = rd s p in
    Scheme { sz; ck; rd }

let ign (Scheme s) =
    let rd _ _ = () in
    Scheme { s with rd }

let opt (Scheme s) =
    let sz = 0 in
    let ck ws i n =
        match s.ck ws i n with
        | exception Aux_invalid _ -> Analysis (0, None)
        | Analysis (i, x) -> Analysis (i, Some x)
    in
    let rd xo ws = match xo with None -> None | Some x -> Some (s.rd x ws) in
    Scheme { sz; ck; rd }

let pair (Scheme sa) (Scheme sb) =
    let na = sa.sz and nb = sb.sz in
    let sz = addsize na nb in
    let ck ws wi wn =
        let Analysis (na, xa) = sa.ck ws wi wn in
        let Position wi = wi and Size wn = wn in
        let wi = Position (addsize wi na) and wn = Size (wn - na) in
        let _ = ckwindow wn sb.sz in
        let Analysis (nb, xb) = sb.ck ws wi wn in
        let n = addsize na nb and x = xa, xb in
        Analysis (n, x)
    and rd (xa, xb) b =
        sa.rd xa b, sb.rd xb b
    in
    Scheme { sz; ck; rd }

let triple (Scheme sa) (Scheme sb) (Scheme sc) =
    let na = sa.sz and nb = sb.sz and nc = sc.sz in
    let sz = na |> addsize nb |> addsize nc in
    let ck ws wi wn =
        let Analysis (na, xa) = sa.ck ws wi wn in
        let Position wi = wi and Size wn = wn in
        let wi = Position (addsize wi na) and wn = Size (wn - na) in
        let _ = ckwindow wn (sb.sz + sc.sz) in
        let Analysis (nb, xb) = sb.ck ws wi wn in
        let Position wi = wi and Size wn = wn in
        let wi = Position (addsize wi nb) and wn = Size (wn - nb) in
        let _ = ckwindow wn sc.sz in
        let Analysis (nc, xc) = sc.ck ws wi wn in
        let n = addsize na @@ addsize nb nc and x = xa, xb, xc in
        Analysis (n, x)
    and rd (xa, xb, xc) b =
        sa.rd xa b, sb.rd xb b, sc.rd xc b
    in
    Scheme { sz; ck; rd }

let vec =
    let rec ckloop ck n0 xs sn ws wi wn =
        if sn > 0 then begin
            let sn = pred sn in
            let Analysis (n, x) = ck ws wi wn in
            let Position wi = wi in
            let wi = Position (addsize wi n) in
            let n0 = addsize n0 n and xs = x :: xs in
            (ckloop[@tailcall]) ck n0 xs sn ws wi wn
        end
        else
            Analysis (n0, List.to_seq xs)
    in
    let rec rdloop rd vs xs b =
        match xs () with
        | Seq.Nil ->
            Array.of_list vs
        | Seq.Cons (x, xs) ->
            let vs = rd x b :: vs in
            (rdloop[@tailcall]) rd vs xs b
    in
    let enter sn (Scheme s) =
        uintreq sn;
        let sz = mulsize s.sz sn
        and ck = ckloop s.ck 0 [] sn
        and rd = rdloop s.rd [] in
        Scheme { sz; ck; rd }
    in
    enter

let seq =
    let rec ckloop w4 n0 sz0 xs ws wi wn =
        let sz, ck, na, nb = w4 in
        if n0 < nb then begin
            if sz > 0 && n0 > na then begin
                let _ = ckwindow wn sz in
                ()
            end;
            match ck ws wi wn with
            | exception (Aux_invalid _ as x) ->
                if n0 >= na then ckfin sz0 xs else raise x
            | Analysis (n, x) ->
                let Position wi = wi in
                let wi = Position (addsize wi n) in
                let sz0 = addsize sz0 n and xs = x :: xs in
                (ckloop[@tailcall]) w4 (succ n0) sz0 xs ws wi wn
        end
        else
            ckfin sz0 xs
    and ckfin sz0 xs = Analysis (sz0, List.to_seq xs) in
    let rec rdloop rd vs xs b =
        match xs () with
        | Seq.Nil -> vs
        | Seq.Cons (x, xs) -> (rdloop[@tailcall]) rd (rd x b :: vs) xs b
    in
    let enter ?(a = 0) ?(b = max_int) (Scheme s) =
        uintreq a;
        uintreq b;
        if a > b then invalid_arg (__MODULE__ ^ ".seq: min > max");
        let sz = mulsize s.sz a
        and ck = ckloop (s.sz, s.ck, a, b) 0 0 []
        and rd = rdloop s.rd [] in
        Scheme { sz; ck; rd }
    in
    enter

let map f (Scheme s) =
    let ck w p n =
        let Analysis (d, x) = s.ck w p n in
        let v = s.rd x w in
        Analysis (d, f p v)
    in
    let rd x _ = x in
    Scheme { s with ck; rd }

let ntyp n s =
    let f _ v = Cf_type.witness n v in
    map f s

module Monad_basis = struct
    type 'r t = 'r scheme

    let return =
        let sz = 0 and ck _ _ _ = Analysis (0, ()) in
        let enter v =
            let rd () _ = v in
            Scheme { sz; ck; rd }
        in
        enter

    let bind (Scheme sa) f =
        let sz = sa.sz and rd = ( @@ ) in
        let ck ws wi wn =
            let Analysis (na, xa) = sa.ck ws wi wn in
            let Scheme sb = f (sa.rd xa ws) in
            let wi = Position (let Position wi = wi in addsize wi na) in
            let wn = Size (let Size wn = wn in wn - na) in
            let _ = ckwindow wn sb.sz in
            let Analysis (nb, xb) = sb.ck ws wi wn in
            Analysis (addsize na nb, sb.rd xb)
        in
        Scheme { sz; ck; rd }

    let product = `Default (* `Special pair *)
    let mapping = `Default
end

module Monad = struct
    type 'r t = 'r scheme
    include Cf_monad.Unary.Create(Monad_basis)
end

let slice0 = Cf_slice.of_string ""

exception Incomplete of string Cf_slice.t * int
exception Invalid of position * string

class scanner ?(start = Position 0) () = object(self)
    val mutable slice_ = slice0 (* the current working slice *)
    val mutable cursor_ = 0     (* index into slice_ to start decoding *)
    val mutable window_ = 0     (* index into slice_ to stop decoding *)
    val mutable start_ = start  (* position of first octet in slice_ *)

    (* set the working slice *)
    method private work slice =
        let open Cf_slice in
        slice_ <- slice;
        cursor_ <- slice.start;
        window_ <- slice.limit

    (* get the remaining octets in the working slice *)
    method private octets =
        let open Cf_slice in
        if cursor_ < slice_.start || window_ > slice_.limit then
            failwith (__MODULE__ ^ ".scanner: invalid window!")
        else if cursor_ > slice_.start || window_ < slice_.limit then
            of_substring slice_.vector cursor_ window_
        else
            slice_

    method private advance n = cursor_ <- cursor_ + n
    method private incomplete n : unit = raise (Incomplete (self#octets, n))

    method private invalid: 'a. position -> string -> 'a = fun p m ->
        let Position i = p in
        if i < cursor_ || i > window_ then
            failwith (__MODULE__ ^ ".scheme: invalid slice index!");
        raise (Invalid (addpos start_ i, m))

    method private acquire:
        'x. int -> (string -> position -> size -> 'x analysis) -> 'x analysis =
        fun sz ck ->
            let open Cf_slice in
            let rec loop () =
                let have = Size (window_ - cursor_) in
                try
                    let _ = ckwindow have sz in
                    ck slice_.vector (Position cursor_) have
                with
                | Aux_incomplete n ->
                    self#incomplete n;
                    if cursor_ < slice_.start || window_ > slice_.limit ||
                        cursor_ > window_
                    then
                        failwith (__MODULE__ ^ ".scanner: class invariant!");
                    (loop[@tailcall]) ()
                | Aux_invalid (i, m) ->
                    self#invalid i m
            in
            let Analysis (n, _) as a = loop () in
            if n < sz || cursor_ + n > window_ then
                failwith (__MODULE__ ^ ".scanner: validation error!");
            a

    method scan: 'v. 'v scheme -> 'v = fun (Scheme s) ->
        let open Cf_slice in
        let Analysis (n, x) = self#acquire s.sz s.ck in
        let v =
            try s.rd x slice_.vector with
            | Aux_invalid (i, m) ->
                self#invalid i m
            | Aux_incomplete _ ->
                failwith (__MODULE__ ^ ".scanner: analysis too late!")
        in
        if n > 0 then self#advance n;
        v

    method position =
        let Position start = start_ in
        Position (start + cursor_ - slice_.Cf_slice.start)
end

let slice_scanner ?start w = object(self)
    inherit scanner ?start ()

    initializer
        self#work w
end

let string_scanner ?start s = slice_scanner ?start (Cf_slice.of_string s)

class virtual window_scanner ?start ?limit () = object(self)
    inherit scanner ?start ()

    val mutable octets_ = Bytes.empty

    method private virtual ingest: bytes -> int -> int -> int

    method! private advance n =
        let Position i = start_ in
        start_ <- Position (i + n);
        window_ <- cursor_

    method! private incomplete n =
        let maxwindow = slice_.Cf_slice.limit and window = window_ + n in
        if window > maxwindow then begin
            let extend =
                match limit with
                | Some lim when n > lim -> raise (Incomplete (self#octets, n))
                | (Some _ | None) -> window - maxwindow
            in
            let copy = Bytes.extend octets_ 0 extend in
            slice_ <- Bytes.unsafe_to_string copy |> Cf_slice.of_string;
            octets_ <- copy
        end;
        let ni = self#ingest octets_ window_ n in
        if ni > 0 then
            window_ <- window_ + ni
        else
            raise (Incomplete (self#octets, n))

    initializer
        match limit with
        | Some lim -> uintreq lim
        | None -> ()
end

let chars_scanner ?start ?limit seq = object
    inherit window_scanner ?start ?limit ()

    val mutable seq_: char Seq.t = seq

    method private ingest octets pos n =
        let rec loop i s =
            if i < n then begin
                match s () with
                | Seq.Cons (c, s) ->
                    Bytes.unsafe_set octets (pos + i) c;
                    seq_ <- s;
                    (loop[@tailcall]) (succ i) s
                | Seq.Nil ->
                    i
            end
            else
                n
        in
        loop 0 seq_
end

let channel_scanner ?start ?limit c = object
    inherit window_scanner ?start ?limit ()
    method private ingest octets pos n = input c octets pos n
end

let rec scanner_to_vals s sxr () =
    let sxrb = (sxr :> scanner) in
    match
        try Some (sxrb#scan s) with
        | Incomplete (ss, _) as x ->
            if Cf_slice.length ss = 0 then None else raise x
    with
    | None -> Seq.Nil
    | Some v -> Seq.Cons (v, scanner_to_vals s sxr)

let incomplete_slice_aux need ws wi wn =
    let slice = Cf_slice.of_substring ws wi (wi + wn) in
    raise (Incomplete (slice, need))

let from_slice_aux adj ws wi wn (Scheme s) =
    if s.sz > wn then
        incomplete_slice_aux (s.sz - wn) ws wi wn
    else
        let Analysis (d, x) =
            try s.ck ws (Position wi) (Size wn) with
            | Aux_incomplete n ->
                incomplete_slice_aux n ws wi wn
            | Aux_invalid (pos, msg) ->
                raise (Invalid (addpos pos adj, msg))
        in
        let v =
            try s.rd x ws with
            | Aux_invalid (p, msg) -> raise (Invalid (addpos p adj, msg))
        in
        v, wi + d, wn - d

let rec to_vals_aux adj sch ws wi wn () =
    match from_slice_aux adj ws wi wn sch with
    | exception (Incomplete (s, _) as x) ->
        if Cf_slice.length s = 0 then Seq.Nil else raise x
    | (v, wi, wn) ->
        Seq.Cons (v, to_vals_aux adj sch ws wi wn)

let string_to_vals ?start sch str =
    let adj = match start with None -> 0 | Some (Position i) -> i in
    to_vals_aux adj sch str 0 (String.length str)

let slice_to_vals ?start sch w =
    let open Cf_slice in
    let adj = match start with None -> 0 | Some (Position i) -> i in
    to_vals_aux (adj - w.start) sch w.vector w.start (length w)

let chars_to_vals ?start ?limit sch seq =
    scanner_to_vals sch (chars_scanner ?start ?limit seq)

let of_scanner length scheme sxr =
    let sxr = (sxr :> scanner) in
    try
        let v = sxr#scan scheme in
        let Position n = sxr#position in
        if length = n then Some v else None
    with
    | Invalid _
    | Incomplete _ ->
        None

let of_slice scheme octets =
    let length = Cf_slice.length octets in
    slice_scanner octets |> of_scanner length scheme

let of_string scheme octets =
    Cf_slice.of_string octets |> of_slice scheme

(*--- End ---*)
