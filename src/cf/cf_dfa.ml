(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Dispatch = sig
    type event
    type 'a t

    val create: (event -> 'a option) -> 'a t
    val dispatch: event -> 'a t -> 'a
end

module type Basis = sig
    module Event: Cf_relations.Equal
    module Dispatch: Dispatch with type event = Event.t
end

module Aux = struct
    module type Char_dispatch = Dispatch with type event = char
    module type Int_dispatch = Dispatch with type event = int

    module Memo(E: Cf_relations.Order) = struct
        type event = E.t

        module Set = Cf_rbtree.Set.Create(E)
        module Map = Cf_rbtree.Map.Create(E)

        type 'a t = Memo of {
            mutable none: Set.t;
            mutable some: 'a Map.t;
            edge: event -> 'a option;
        }

        let create f = Memo { none = Set.nil; some = Map.nil; edge = f }

        let dispatch ev (Memo m) =
            if Set.member ev m.none then raise Not_found;
            try Map.require ev m.some with Not_found ->
            match m.edge ev with
            | None ->
                m.none <- Set.put ev m.none;
                raise Not_found
            | Some v ->
                m.some <- Map.replace (ev, v) m.some;
                v
    end

    module Eager = struct
        module type Basis_core = sig
            module Event: Cf_relations.Equal

            module Core: Cf_disjoint_interval.Core.Profile
               with type limit := Event.t

            type vector

            module Map: Cf_bsearch_data.Map.Profile
               with type search := Event.t
                and type key := Event.t Cf_disjoint_interval.t
                and type Unsafe.index := int
                and type Unsafe.vector := vector
                and type 'a Unsafe.content := 'a array
        end

        module Char_basis_core = struct
            module Event = Char
            module Core = Cf_disjoint_interval.Core.Of_char
            type vector = string
            module Map = Cf_disjoint_interval.Map.Of_char
        end

        module Int_basis_core = struct
            module Event = Cf_relations.Int
            module Core = Cf_disjoint_interval.Core.Of_int
            type vector = int array
            module Map = Cf_disjoint_interval.Map.Of_int
        end

        module type Basis = sig
            include Basis_core
            val eager: Event.t Seq.t
        end

        module Eager(B: Basis) = struct
            type event = B.Event.t

            type 'a t = 'a B.Map.t

            let create f =
                let g ev =
                    match f ev with None -> None | Some r -> Some (ev, r)
                in
                Seq.filter_map g B.eager |> B.Core.lift2 (==) |> B.Map.of_seq

            let dispatch = B.Map.require
        end

        let of_chars s =
            let module B = struct include Char_basis_core let eager = s end in
            (module Eager(B) : Char_dispatch)

        let of_ints s =
            let module B = struct include Int_basis_core let eager = s end in
            (module Eager(B) : Int_dispatch)
    end
end

module type Regular = sig
    type event
    type term

    val nil: term
    val one: event -> term
    val sat: (event -> bool) -> term
    val cat2: term -> term -> term
    val cats: term Seq.t -> term
    val alt2: term -> term -> term
    val alts: term Seq.t -> term
    val opt: term -> term
    val star: term -> term
    val seq: ?a:int -> ?b:int -> term -> term

    type 'r fin
    val fin: term -> 'a -> 'a fin
end

module type Machine = sig
    type event
    type 'r fin
    type 'r t
    val create: 'r fin Seq.t -> 'r t
    val copy: 'r t -> 'r t
    val reset: 'r t -> unit
    val advance: 'r t -> event -> unit
    val rejected: 'r t -> bool
    val accepted: 'r t -> bool
    val finish: 'r t -> 'r
end

module Edge = struct
    include Cf_relations.Int
    module Set = Cf_rbtree.Set.Create(Cf_relations.Int)
    module Map = Cf_rbtree.Map.Create(Cf_relations.Int)
end

module Node = struct
    type t = Edge.t array

    let of_edges v = Array.of_seq @@ Edge.Set.to_seq_incr v

    module Order = struct
        type nonrec t = t

       let compare a1 a2 =
            let n1 = Array.length a1 and n2 = Array.length a2 in
            let n = min n1 n2 in
            let rec loop i =
                if i < n then begin
                    let v1 = Array.unsafe_get a1 i in
                    let v2 = Array.unsafe_get a2 i in
                    let d = Edge.compare v1 v2 in
                    if d = 0 then loop (succ i) else d
                end
                else
                    if n < n1 then (-1) else if n < n2 then 1 else 0
            in
            loop 0
    end

    module Map = Cf_rbtree.Map.Create(Order)
end

external identity: 'a -> 'a = "%identity"

module Core(B: Basis) = struct
    type event = B.Event.t
    type 'a dispatch = 'a B.Dispatch.t

    class virtual satisfier = object
        val state_ = Edge.Set.nil

        method virtual transit: event -> Edge.Set.t -> Edge.Set.t

        method state = state_
        method push u = {< state_ = Edge.Set.union state_ u >}
    end

    let equiv ev0 = object
        inherit satisfier

        method transit ev u =
            if ev == ev0 || B.Event.equal ev ev0 then
                Edge.Set.union state_ u
            else
                u
    end

    let filter f = object
        inherit satisfier
        method transit ev u = if f ev then Edge.Set.union state_ u else u
    end

    type 'r q =
        | Q_ready of satisfier
        | Q_accept of 'r

    let qtransit edges0 event edges edge =
        match Edge.Map.require edge edges0 with
        | Q_ready sat -> sat#transit event edges
        | Q_accept _ -> edges

    let qaccept =
        let f = function Q_ready _ -> None | Q_accept f -> Some f in
        let enter edges node =
            let g edge = Edge.Map.require edge edges in
            match Array.to_seq node |> Seq.map g |> Cf_seq.searchmap f with
            | None -> None
            | Some (accept, _) -> Some accept
        in
        enter

    type 'r y = Y of {
        counter: Edge.t;
        first: Edge.Set.t;
        last: Edge.Set.t;
        follow: 'r q Edge.Map.t -> 'r q Edge.Map.t;
    }

    let ynil i =
        let u = Edge.Set.nil in
        Y { counter = i; first = u; last = u; follow = identity; }

    let ycons q i =
        let u = Edge.Set.one i and follow = Edge.Map.replace (i, q) in
        Y { counter = succ i; first = u; last = u; follow }

    let yfold ya yb u =
        let f m i =
            let Y y = yb in
            let q =
                match Edge.Map.require i m with
                | Q_ready sat -> Q_ready (sat#push y.first)
                | Q_accept _ as q -> q
            in
            Edge.Map.replace (i, q) m
        in
        let s = Edge.Set.to_seq_incr (let Y y = ya in y.last) in
        Seq.fold_left f u s

    let ycat2 na nb (Y ya as ya0) (Y yb as yb0) =
        let first = if na then Edge.Set.union ya.first yb.first else ya.first
        and last = if nb then Edge.Set.union ya.last yb.last else yb.last
        and follow m = ya.follow m |> yb.follow |> yfold ya0 yb0 in
        Y { yb with first; last; follow }

    let ystar (Y y as y0) =
        let follow m = y.follow m |> yfold y0 y0 in
        Y { y with follow }

    let yalt2 (Y a) (Y b) =
        let counter = max a.counter b.counter in
        let first = Edge.Set.union a.first b.first in
        let last = Edge.Set.union a.last b.last in
        let follow m = m |> a.follow |> b.follow in
        Y { counter; first; last; follow }

    let yalts =
        let counter n (Y y) = max n y.counter in
        let first u (Y y) = Edge.Set.union u y.first in
        let last u (Y y) = Edge.Set.union u y.last in
        let follow m (Y y) = y.follow m in
        let enter s =
            let counter = List.fold_left counter 0 s in
            let first = List.fold_left first Edge.Set.nil s in
            let last = List.fold_left last Edge.Set.nil s in
            let follow m = List.fold_left follow m s in
            Y { counter; first; last; follow }
        in
        enter

    type term = Term of { null: bool; cons: 'r. Edge.t -> 'r y }

    let ready sat =
        let cons i = ycons (Q_ready sat) i in
        Term { null = false; cons }

    let nil = Term { null = true; cons = ynil }

    let one e = ready (equiv e)
    let sat f = ready (filter f)

    let cat2 (Term a) (Term b) =
        let null = a.null && b.null in
        let cons i =
            let ya = a.cons i in
            let yb = let Y y = ya in b.cons y.counter in
            (ycat2[@tailcall]) a.null b.null ya yb
        in
        Term { null; cons }

    let cats =
        let is_null (Term t) = t.null in
        let rec loop n y s =
            match s () with
            | Seq.Nil ->
                y
            | Seq.Cons (Term t, s) ->
                let y = ycat2 n t.null y (let Y y = y in t.cons y.counter) in
                let n = n && t.null in
                (loop[@tailcall]) n y s
        in
        let enter s =
            let null = Cf_seq.has_all is_null s in
            let cons i = (loop[@tailcall]) true (ynil i) s in
            Term { null; cons }
        in
        enter

    let alt2 (Term a) (Term b) =
        let null = a.null || b.null in
        let cons i =
            let ya = a.cons i in
            let yb = (let Y y = ya in b.cons y.counter) in
            yalt2 ya yb
        in
        Term { null; cons }

    let alts =
        let is_not_null (Term t) = not t.null in
        let rec loop ys i s =
            match s () with
            | Seq.Nil ->
                (yalts[@tailcall]) (List.rev ys)
            | Seq.Cons (Term t, s) ->
                let y = t.cons i in
                (loop[@tailcall]) (y :: ys) (let Y y = y in y.counter) s
        in
        let enter s =
            let null = not @@ Cf_seq.has_all is_not_null s in
            let cons i = loop [] i s in
            Term { null; cons }
        in
        enter

    let opt t = alt2 nil t

    let star (Term t) =
        let cons i = ystar (t.cons i) in
        Term { null = true; cons }

    let seq =
        let valid ~a ?b () =
            let msg = Printf.sprintf "%s.seq: %s" __MODULE__ in
            if a < 0 then msg "a < 0" |> invalid_arg;
            match b with
            | None -> ()
            | Some b -> if b < a then msg "b < a" |> invalid_arg
        in
        let optpred = function None -> None | Some v -> Some (pred v) in
        let rec loop ~a ?b t0 t =
            if a > 0 then begin
                let a = pred a and b = optpred b and t0 = cat2 t0 t in
                (loop[@tailcall]) ~a ?b t0 t
            end
            else begin
                match b with
                | None ->
                    cat2 t0 (star t)
                | Some b ->
                    if b > 0 then begin
                        let b = pred b and t0 = cat2 t0 t in
                        (loop[@tailcall]) ~a:0 ~b t0 t
                    end
                    else
                        t0
            end
        in
        let enter ?(a = 0) ?b t =
            valid ~a ?b ();
            let a, t0 = if a > 0 then pred a, t else a, nil in
            (loop[@tailcall]) ~a ?b t0 t
        in
        enter

    type 'r fin = Fin of { cons: Edge.t -> 'r y } [@@ocaml.unboxed]

    let fin (Term t) v =
        let cons i =
            let ya = t.cons i in
            let yb = let Y y = ya in ycons (Q_accept v) y.counter in
            (ycat2[@tailcall]) t.null true ya yb
        in
        Fin { cons }

    type 'r state = State of {
        accept: 'r option;
        next: 'r state Lazy.t dispatch;
    }

    type 'r machine = Machine of {
        mutable memo: 'r state Node.Map.t;
        trans: 'r q Edge.Map.t;
    }

    let state =
        let rec loop (Machine m as m0) node =
            let accept = qaccept m.trans node in
            let next = B.Dispatch.create (follow m0 node) in
            let s = State { accept; next } in
            m.memo <- Node.Map.replace (node, s) m.memo;
            s
        and follow (Machine m as m0) node ev =
            let f = qtransit m.trans ev in
            let edges = Array.fold_left f Edge.Set.nil node in
            if Edge.Set.empty edges then
                None
            else
                Some (lazy begin
                    let node = Node.of_edges edges in
                    try Node.Map.require node m.memo with
                    | Not_found -> loop m0 node
                end)
        in
        let rec unify ys i s =
            match s () with
            | Seq.Nil ->
                yalts (List.rev ys)
            | Seq.Cons (Fin r, s) ->
                let y = r.cons i in
                (unify[@tailcall]) (y :: ys) (let Y y = y in y.counter) s
        in
        let enter s =
            let Y y = unify [] 0 s in
            let memo = Node.Map.nil and trans = y.follow Edge.Map.nil in
            let m = Machine { memo; trans } and node = Node.of_edges y.first in
            (loop[@tailcall]) m node
        in
        enter

    type 'r t = DFA of {
        mutable cursor: 'r state Lazy.t;
        mutable rejected: bool;
        start: 'r state Lazy.t
    }

    let create s =
        let start = lazy (state s) in
        DFA { cursor = start; rejected = false; start }

    let reset (DFA w) =
        w.rejected <- false;
        w.cursor <- w.start

    let copy (DFA w) =
        DFA { w with cursor = w.start; rejected = false }

    let advance (DFA w) ev =
        if w.rejected then begin
            let msg =
                Printf.sprintf "%s.advance: already rejected!" __MODULE__
            in
            failwith msg
        end;
        let State s = Lazy.force w.cursor in
        match B.Dispatch.dispatch ev s.next with
        | exception Not_found -> w.rejected <- true
        | state -> w.cursor <- state

    let accepted (DFA w) =
        let State s = Lazy.force w.cursor in
        match s.accept with None -> false | Some _ -> true

    let rejected (DFA w) = w.rejected

    let finish (DFA w) =
        let State s = Lazy.force w.cursor in
        match s.accept with Some v -> v | None -> raise Not_found
end

module type Affix = sig
    type event
    type term
    type 'r fin

    val ( !: ): event -> term
    val ( !^ ): (event -> bool) -> term

    val ( !? ): term -> term
    val ( !* ): term -> term
    val ( !+ ): term -> term
    val ( $| ): term -> term -> term
    val ( $& ): term -> term -> term

    val ( $= ): term -> 'r -> 'r fin
end

module Mk_affix(R: Regular) = struct
    open R

    let ( !: ) = one
    let ( !^ ) = sat
    let ( $| ) = alt2
    let ( $& ) = cat2
    let ( !? ) = opt
    let ( !* ) = star

    let ( !+ ) t = cat2 t (star t)

    let ( $= ) = fin
end

module type Profile = sig
    include Regular
    include Machine with type event := event and type 'r fin := 'r fin

    module Affix: Affix
       with type event := event
        and type term := term
        and type 'r fin := 'r fin
end

module Create(B: Basis) = struct
    module Core = Core(B)
    module Affix = Mk_affix(Core)
    include Core
end

(*--- End ---*)
