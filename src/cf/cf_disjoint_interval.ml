(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type 'a t = T of { a: 'a; b: 'a }

module Core = struct
    module type Profile = sig
        type limit

        val compare: limit t -> limit t -> int

        val lift: limit Seq.t -> limit t Seq.t

        val lift2:
            ('a -> 'a -> bool) -> (limit * 'a) Seq.t ->
            (limit t * 'a) Seq.t
    end

    module Create(B: Cf_bsearch.Basis) = struct
        let compare (T t1) (T t2) =
            let d = B.compare t1.a t2.a in
            if d <> 0 then d else B.compare t1.b t2.b

        let lift =
            let rec loop a b s () =
                match s () with
                | Seq.Nil ->
                    let p = T { a; b } in
                    Seq.(Cons (p, empty))
                | Seq.Cons (i, s) ->
                    if B.compare (B.succ b) i <> 0 then
                        let p = T { a; b } in
                        Seq.Cons (p, loop i i s)
                    else
                        (loop[@tailcall]) a i s ()
            in
            let enter s () =
                match s () with
                | Seq.Nil -> Seq.Nil
                | Seq.Cons (a, s) -> (loop[@tailcall]) a a s ()
            in
            enter

        let lift2 =
            let rec loop f v a b s () =
                match s () with
                | Seq.Nil ->
                    let p = T { a; b }, v in
                    Seq.(Cons (p, empty))
                | Seq.Cons (p, s) ->
                    let i, vi = p in
                    if not (f v vi) || B.compare (B.succ b) i <> 0 then
                        let p = T { a; b }, v in
                        Seq.Cons (p, loop f vi i i s)
                    else
                        (loop[@tailcall]) f v a i s ()
            in
            let enter f s () =
                match s () with
                | Seq.Nil ->
                    Seq.Nil
                | Seq.Cons (p, s) ->
                    let a, v = p in
                    (loop[@tailcall]) f v a a s ()
            in
            enter
    end

    module Of_char = Create(Cf_bsearch.Char_basis)
    module Of_int = Create(Cf_bsearch.Int_basis)
end

module Vector = struct
    let last_aux len v =
        let n = len v in
        if n > 0 then
            n / 2 - 1
        else
            invalid_arg (__MODULE__ ^ ": empty vector!")

    let project_aux len get s i =
        let i = i * 2 in
        let j = succ i in
        if j < len s then
            T { a = get s i; b = get s j }
        else
            invalid_arg (__MODULE__ ^ ": index out of bounds!")

    module Of_char = struct
        module Element = struct
            include Core.Create(Cf_bsearch.Char_basis)
            type nonrec t = char t
        end

        type element = char t
        type index = int
        type t = string

        module Basis = Cf_bsearch_data.Vector.Int_basis

        let nil = ""
        let empty s = String.length s = 0
        let first = 0
        let last = last_aux String.length

        let project = project_aux String.length String.unsafe_get

        let of_seq =
            let rec loop b s =
                match s () with
                | Seq.Nil ->
                    ()
                | Seq.Cons (T e, s) ->
                    Buffer.add_char b e.a;
                    Buffer.add_char b e.b;
                    (loop[@tailcall]) b s
            in
            let enter s =
                let b = Buffer.create 32 in
                loop b s;
                Buffer.contents b
            in
            enter

        let to_seq v =
            Cf_seq.range 0 (last v) |>
            Seq.map (project v)
    end

    module Of_search(B: Cf_bsearch.Basis) = struct
        module Element = struct
            include Core.Create(B)
            type nonrec t = B.t t
        end

        type element = B.t t
        type index = int
        type nonrec t = B.t array

        module Basis = Cf_bsearch_data.Vector.Int_basis

        let nil = [| |]
        let empty s = Array.length s = 0
        let first = 0
        let last v = last_aux Array.length v

        let project s = project_aux Array.length Array.unsafe_get s

        let of_seq =
            let rec loop w s =
                match s () with
                | Seq.Nil ->
                    List.rev !w
                | Seq.Cons (T e, s) ->
                    w := e.b :: e.a :: !w;
                    (loop[@tailcall]) w s
            in
            let enter s = Array.of_list (loop (ref []) s) in
            enter

        let to_seq v =
            Cf_seq.range 0 (last v) |>
            Seq.map (project v)
    end
end

module Table = struct
    let aux_compare compare key (T interval) =
        let da = compare key interval.a in
        if da < 0 then
            da
        else begin
            let db = compare key interval.b in
            if db > 0 then
                db
            else
                0
        end

    module Order_basis(B: Cf_bsearch.Basis) = struct
        module Vector = Vector.Of_search(B)
        module Search = B

        let xcompare = aux_compare B.compare
    end

    module Char_basis = struct
        module Vector = Vector.Of_char
        module Search = Char

        let xcompare = aux_compare Char.compare
    end
end

module Set = struct
    module Create(B: Cf_bsearch.Basis) =
        Cf_bsearch_data.Set.Of_basis(Table.Order_basis(B))

    module Of_char = Cf_bsearch_data.Set.Of_basis(Table.Char_basis)
    module Of_int = Create(Cf_bsearch.Int_basis)
    module Of_float = Create(Cf_bsearch.Float_basis)
end

module Map = struct
    module Order_basis(B: Cf_bsearch.Basis) = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.Order_basis(B)
        module Content = Cf_bsearch_data.Map.Aux.Of_array
    end

    module Create(B: Cf_bsearch.Basis) =
        Cf_bsearch_data.Map.Of_basis(Order_basis(B))

    module Char_basis = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.Char_basis
        module Content = Cf_bsearch_data.Map.Aux.Of_array
    end

    module Of_char = Cf_bsearch_data.Map.Of_basis(Char_basis)
    module Of_int = Create(Cf_bsearch.Int_basis)
    module Of_float = Create(Cf_bsearch.Float_basis)
end

(*--- End ---*)
