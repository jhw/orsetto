/*---------------------------------------------------------------------------*
  Copyright (c) 2003-2022, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/

#include "cf_tai64_aux.h"

#include <sys/types.h>

#include <math.h>
#include <stdint.h>
#include <time.h>

#define INVALID_ARGUMENT(S)     (caml_invalid_argument("Cf_tai64." S))

static int cf_tai64_op_compare(value v1, value v2);
static intnat cf_tai64_op_hash(value v);
static void cf_tai64_op_serialize(value v, uintnat* z32, uintnat* z64);
static uintnat cf_tai64_op_deserialize(void* buffer);

static const value* cf_tai64_range_error_exn = 0;
static const value* cf_tai64_label_error_exn = 0;

static value cf_tai64_epoch_val = Val_unit;
static value cf_tai64_first_val = Val_unit;
static value cf_tai64_last_val = Val_unit;
static value cf_tai64_mjd_epoch_val = Val_unit;

int cf_tai64_current_offset_from_utc = 10; /* Without leap seconds archive! */

void cf_tai64_range_error(void)
{
    CAMLparam0();
    CAMLlocal1(exnVal);

    if (!cf_tai64_range_error_exn) {
        cf_tai64_range_error_exn =
            caml_named_value("Cf_tai64.Range_error");
        if (!cf_tai64_range_error_exn)
            INVALID_ARGUMENT("Range_error exception unavailable in primitive.");
    }

    exnVal = caml_alloc_small(1, 0);
    caml_initialize(&Field(exnVal, 0), *cf_tai64_range_error_exn);
    caml_raise(exnVal);

    CAMLreturn0;
}

void cf_tai64_label_error(void)
{
    CAMLparam0();
    CAMLlocal1(exnVal);

    if (!cf_tai64_label_error_exn) {
        cf_tai64_label_error_exn =
            caml_named_value("Cf_tai64.Label_error");
        if (!cf_tai64_label_error_exn)
            INVALID_ARGUMENT("Label_error exception unavailable in primitive.");
    }

    exnVal = caml_alloc_small(1, 0);
    caml_initialize(&Field(exnVal, 0), *cf_tai64_label_error_exn);
    caml_raise(exnVal);

    CAMLreturn0;
}

static struct custom_operations cf_tai64_op = {
    .identifier = "uri://conjury.org/orsetto/cf_tai64",
    .finalize = custom_finalize_default,
    .compare = cf_tai64_op_compare,
    .hash = cf_tai64_op_hash,
    .serialize = cf_tai64_op_serialize,
    .deserialize = cf_tai64_op_deserialize,
    .compare_ext = custom_compare_ext_default,
};

static int cf_tai64_op_compare(value v1, value v2)
{
    CAMLparam2(v1, v2);

    const Cf_tai64_t* v1Ptr;
    const Cf_tai64_t* v2Ptr;
    int result;

    v1Ptr = Cf_tai64_val(v1);
    v2Ptr = Cf_tai64_val(v2);

    if (v2Ptr->s > v1Ptr->s)
        result = 1;
    else if (v1Ptr->s > v2Ptr->s)
        result = -1;
    else
        result = 0;

    CAMLreturn(result);
}

static intnat cf_tai64_op_hash(value v)
{
    CAMLparam1(v);
    uint64_t s = Cf_tai64_val(v)->s;
    uint32_t lo = (uint32_t) s, hi = (uint32_t) (s >> 32);
    CAMLreturn((intnat) hi ^ lo);
}

static void cf_tai64_op_serialize(value v, uintnat* z32, uintnat* z64)
{
    caml_serialize_int_8(Cf_tai64_val(v)->s);
    *z32 = *z64 = 8;
}

static uintnat cf_tai64_op_deserialize(void* data)
{
    ((Cf_tai64_t *) data)->s = caml_deserialize_sint_8();
    return 8;
}

/*---
  Allocate an initialized structure
  ---*/
extern value cf_tai64_alloc(const Cf_tai64_t* tai64Ptr)
{
    value result;

    result = caml_alloc_custom(&cf_tai64_op, sizeof *tai64Ptr, 0, 1);
    *Cf_tai64_val(result) = *tai64Ptr;
    return result;
}

/*---
  Compare primitive
  ---*/
CAMLprim value cf_tai64_equal(value v1, value v2)
{
    CAMLparam2(v1, v2);
    int result;

    const Cf_tai64_t* v1Ptr;
    const Cf_tai64_t* v2Ptr;

    v1Ptr = Cf_tai64_val(v1);
    v2Ptr = Cf_tai64_val(v2);

    result = (v1Ptr->s == v2Ptr->s);
    CAMLreturn(Val_bool(result));
}

/*---
  Compare primitive
  ---*/
CAMLprim value cf_tai64_compare(value v1, value v2)
{
    CAMLparam2(v1, v2);
    int dt;

    dt = cf_tai64_op_compare(v1, v2);
    CAMLreturn(Val_int(dt));
}

/*---
  Set to current time

    1972-01-01 00:00:00 UTC was 1972-01-01 00:00:10 TAI
    Unix time_t values include all currently defined leap seconds
  ---*/
extern void cf_tai64_update(Cf_tai64_t* tai64Ptr)
{
    uint64_t epoch;

    epoch = CF_TAI64_UNIX_EPOCH;
    epoch += cf_tai64_current_offset_from_utc;
    tai64Ptr->s = epoch + ((uint64_t) time(0));
}

/*---
  Allocate and set to current time
  ---*/
CAMLprim value cf_tai64_now(value unit)
{
    CAMLparam0();
    CAMLlocal1(result);
    Cf_tai64_t x;

    (void) unit;
    cf_tai64_update(&x);
    result = cf_tai64_alloc(&x);

    CAMLreturn(result);
}

/*---
  Allocate global value of TAI64 epoch
  ---*/
CAMLprim value cf_tai64_epoch(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64_epoch_val);
}

/*---
  Allocate global value of the first representable TAI64
  ---*/
CAMLprim value cf_tai64_first(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64_first_val);
}

/*---
  Allocate global value of the last representable TAI64
  ---*/
CAMLprim value cf_tai64_last(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64_last_val);
}

/*---
  Allocate global value of the Modified Julian Date (MJD) epoch
  ---*/
CAMLprim value cf_tai64_mjd_epoch(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64_mjd_epoch_val);
}

/*---
  Set the current offset from UTC to TAI
  ---*/
CAMLprim void cf_tai64_set_current_offset_from_utc(value v)
{
    CAMLparam1(v);
    cf_tai64_current_offset_from_utc = Int_val(v);
    CAMLreturn0;
}

/*---
  Convert a floating point Unix.time result to TAI
  ---*/
CAMLprim value cf_tai64_of_unix_time(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);

    static const double cf_tai64_unix_limit[2] = {
        -((double)CF_TAI64_UNIX_EPOCH),
        ((double)(CF_TAI64_UNIX_EPOCH << 1) - CF_TAI64_UNIX_EPOCH - 1),
    };

    Cf_tai64_t tai64;
    double x;

    x = ceil(Double_val(v));
    x += (double) cf_tai64_current_offset_from_utc;

    if (x < cf_tai64_unix_limit[0] || x > cf_tai64_unix_limit[1])
        cf_tai64_range_error();

    tai64.s = CF_TAI64_UNIX_EPOCH + ((int64_t) x);
    result = cf_tai64_alloc(&tai64);

    CAMLreturn(result);
}

/*---
  Convert a TAI value to a floating point Unix.time result
  ---*/
CAMLprim value cf_tai64_to_unix_time(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);
    double x;
    uint64_t epoch;

    epoch = CF_TAI64_UNIX_EPOCH;
    epoch += cf_tai64_current_offset_from_utc;

    uint64_t s = Cf_tai64_val(v)->s;

    x = (s < epoch) ? -(double) (epoch - s) : (double) (s - epoch);
    result = caml_copy_double(x);

    CAMLreturn(result);
}

/*---
  Convert a string containing a TAI label into the corresponding value
  ---*/
CAMLprim value cf_tai64_of_label(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);
    Cf_tai64_t tai64;
    int i;
    uint64_t x;

    if (caml_string_length(v) != 8) cf_tai64_label_error();
    for (i = 0, x = 0; i < 8; ++i) x = (x << 8) | Byte_u(v, i);
    tai64.s = x;
    result = cf_tai64_alloc(&tai64);

    CAMLreturn(result);
}

/*---
  Convert a TAI value to a Caml string containing its label
  ---*/
CAMLprim value cf_tai64_to_label(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);
    uint64_t x;
    int i;

    result = caml_alloc_string(8);
    x = Cf_tai64_val(v)->s;
    for (i = 7, x = Cf_tai64_val(v)->s; i >= 0; --i, x >>= 8)
        Byte_u(result, i) = (unsigned char) x;

    CAMLreturn(result);
}

/*---
  Addition of integer
  ---*/
CAMLprim value cf_tai64_add_int(value tai64Val, value dtVal)
{
    CAMLparam2(tai64Val, dtVal);
    CAMLlocal1(result);
    Cf_tai64_t x;

    const uint64_t s = Cf_tai64_val(tai64Val)->s;
    const int dt = Int_val(dtVal);

    if (dt > 0 && s > UINT64_MAX - dt)
        cf_tai64_range_error();
    else if (dt < 0 && s < (uint64_t) -dt)
        cf_tai64_range_error();

    x.s = s + dt;
    result = cf_tai64_alloc(&x);

    CAMLreturn(result);
}

/*---
  Addition of int32
  ---*/
CAMLprim value cf_tai64_add_int32(value tai64Val, value dt32Val)
{
    CAMLparam2(tai64Val, dt32Val);
    CAMLlocal1(result);
    Cf_tai64_t x;

    const uint64_t s = Cf_tai64_val(tai64Val)->s;
    const int32_t dt = Int32_val(dt32Val);

    if (dt > 0 && s > UINT64_MAX - dt)
        cf_tai64_range_error();
    else if (dt < 0 && s < (uint64_t) -dt)
        cf_tai64_range_error();

    x.s = Cf_tai64_val(tai64Val)->s + dt;
    result = cf_tai64_alloc(&x);

    CAMLreturn(result);
}

/*---
  Addition of int64
  ---*/
CAMLprim value cf_tai64_add_int64(value tai64Val, value dt64Val)
{
    CAMLparam2(tai64Val, dt64Val);
    CAMLlocal1(result);
    Cf_tai64_t x;

    const uint64_t s = Cf_tai64_val(tai64Val)->s;
    const int64_t dt = Int64_val(dt64Val);

    if (dt > 0 && s > UINT64_MAX - dt)
        cf_tai64_range_error();
    else if (dt < 0 && s < (uint64_t) -dt)
        cf_tai64_range_error();

    x.s = Cf_tai64_val(tai64Val)->s + dt;
    result = cf_tai64_alloc(&x);

    CAMLreturn(result);
}

/*---
  Subtraction of tai64 values
  ---*/
CAMLprim value cf_tai64_sub(value v1, value v2)
{
    CAMLparam2(v1, v2);
    int64_t dt;

    dt = Cf_tai64_val(v1)->s - Cf_tai64_val(v2)->s;

    CAMLreturn(caml_copy_int64(dt));
}

/*---
  Initialization primtive
  ---*/
CAMLprim value cf_tai64_init(value unit)
{
    static const Cf_tai64_t epoch = { CF_TAI64_UNIX_EPOCH };
    static const Cf_tai64_t first = { 0ULL };
    static const Cf_tai64_t last = { (CF_TAI64_UNIX_EPOCH << 1) - 1 };
    static const Cf_tai64_t mjd_epoch = { CF_TAI64_MJD_EPOCH };

    (void) unit;
    caml_register_custom_operations(&cf_tai64_op);

    caml_register_global_root(&cf_tai64_epoch_val);
    cf_tai64_epoch_val = cf_tai64_alloc(&epoch);

    caml_register_global_root(&cf_tai64_first_val);
    cf_tai64_first_val = cf_tai64_alloc(&first);

    caml_register_global_root(&cf_tai64_last_val);
    cf_tai64_last_val = cf_tai64_alloc(&last);

    caml_register_global_root(&cf_tai64_mjd_epoch_val);
    cf_tai64_mjd_epoch_val = cf_tai64_alloc(&mjd_epoch);

    return Val_unit;
}

/*--- $File: src/cf/cf_tai64_aux.c $ ---*/
