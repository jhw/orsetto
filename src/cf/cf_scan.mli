(*---------------------------------------------------------------------------*
  Copyright (C) 2011-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional LL(x) parsing with monadic combinators. *)

(** {6 Overview}

    This module implements functional left-shift/left-reduce parser combinators
    using a state-exception monad over a sequence of input symbols. The input
    symbol sequence is the state, and the recognized production is either a
    returned value [Some v], or [None] if the input stream is not recognized.
    Composing a grammar production from a sequence of other productions is done
    by using the bind operator. Backtracking assumes the input stream is
    confluently persistent.

    The input is conceptually sequence of {i symbol} value, attributed in the
    {i iota} type with stream position. Accordingly, productions are returned
    wrapped in a {i 'a form} type that conceptually represents the value and
    the span of positions where it was recognized in the input. However, a
    simplified module signature is provided where {i type iota = symbol} and
    {i type 'a form = 'a}.

    Syntax errors can be raised with arbitrary OCaml exceptions. Exceptions can
    be caught, and various functions are available for error recovery.
*)

(** {6 Interface} *)

(** Define a module of this type in a scanner basis. It defines the functions
    required by the basic scanner to relate produced values and their
    corresponding {i 'a form}.

    The {Cf_annot_scan} module extends this interface.
*)
module type Form = sig

    (** A value conceptually annotated with its position in the input. *)
    type +'a t

    (** Scanners use [imp value] to create a value form with a distinguished
        implicit position, i.e. not located in the input stream.
    *)
    val imp: 'a -> 'a t

    (** Scanners use [dn form] to get the value wrapped in [form]. *)
    val dn: 'a t -> 'a

    (** Scanners use [mv value form] to make a form containing [value]
        attributed with the same position as [form].
    *)
    val mv: 'a -> 'b t -> 'a t

    (** Scanners use [span a b value] to make a form containing [value]
        attributed to the span of positions from [a] to [b].
    *)
    val span: 'a t -> 'b t -> 'c -> 'c t
end

(** Define a module of this type as the basis of a scanner. *)
module type Basis = sig

    (** The equivalence relation on input symbols. *)
    module Symbol: Cf_relations.Equal

    (** The signature of production forms. *)
    module Form: Form

    (** An input stream position. *)
    type position

    (** An input symbol conceptually attributed with its position. *)
    type iota

    (** Scanners use [init symbol] to make the first {i iota} in the input
        stream. If [~start] is provided, then it specifies the starting
        position in the stream of the result.
    *)
    val init: ?start:position -> Symbol.t -> iota

    (** Scanners use [next cursor symbol] to make the {i iota} comprising
        [symbol] at the position immediately following [cursor].
    *)
    val next: iota -> Symbol.t -> iota

    (** Scanners use [sym iota] to get the {i symbol} contained in [iota]. *)
    val sym: iota -> Symbol.t

    (** Scanners use [term iota] to produce a terminal {i form} comprising
        the {i symbol} contained in [iota] attributed with its position.
    *)
    val term: iota -> Symbol.t Form.t
end

(** The signature of a basic scanner. {b Note Well}: names are short to
    economize on souce code size in writing grammars by hand.
*)
module type Profile = sig

    (** The monad type. *)
    type +'r t
    include Cf_monad.Unary.Profile with type +'r t := 'r t

    (** A scanner that never produces any value. *)
    val nil: 'r t

    (** A scanner that returns [true] at the end of the input sequence,
        otherwise returns [false].
    *)
    val fin: bool t

    (** {4 Backtracking} *)

    (** The abstract type representing a resumption point in the input. *)
    type mark

    (** The type of a production form (erased by [Create()] below). *)
    type +'a form

    (** A scanner that produces a {i mark} captured at the current position. *)
    val cur: mark t

    (** Use [mov mark] to resume scanning at the captured [mark]. *)
    val mov: mark -> unit t

    (** Use [pos mark] to make a unit value attributed with the position of
        captured [mark]. If the mark was captured at the end of the stream,
        then the result is an implicit form.
    *)
    val pos: mark -> unit form

    (** {4 Terminal Scanner} *)

    (** The type of an input symbol (erased by [Create()] below). *)
    type symbol

    (** The universal symbol scanner. Recognizes any symbol in the input stream
        and produces its {i form}. Does not produce anything at the end of
        input.
    *)
    val any: symbol form t

    (** The literal symbol scanner. Use [one symbol] to make a scanner that
        recognizes [symbol] in the input stream and produces its {i form}.
    *)
    val one: symbol -> symbol form t

    (** The symbol satisfier scanner. Use [sat f] to make a scanner that
        recognizes any {i symbol} for which applying [f] returns [true] and
        produces its {i form}.
    *)
    val sat: (symbol -> bool) -> symbol form t

    (** The ignore scanner. Use [ign f] to make a scanner that scans the input
        while applying [f] to each {i symbol} returns [true], then produces a
        unit {i form} that annotates the span of ignored symbols. Produces an
        implicit unit form if the end of input has already been reached.
    *)
    val ign: (symbol -> bool) -> unit form t

    (** The symbolic token scanner. Use [tok f] to make a scanner that
        recognizes any {i symbol} for which applying [f] returns [Some v], then
        produces the {i form} of [v].
    *)
    val tok: (symbol -> 'r option) -> 'r form t

    (** {4 Scanner Composers} *)

    (** The opaque value form scanner composer. Use [ntyp n p] to make a
        scanner that encloses the value contained in the {i form} produced by
        [p] in an opaque value with the runtime type indicated by [n] and
        returns its {i form} in the same position.
    *)
    val ntyp: 'r Cf_type.nym -> 'r form t -> Cf_type.opaque form t

    (** The default value scanner. Use [dflt v p] to produce the output of [p]
        or the default value [v] with implicit annotation if [p] does not
        produce output.
    *)
    val dflt: 'r -> 'r form t -> 'r form t

    (** The optional scanner composer. Use [opt p] to make a scanner that
        produces either [Some v] if [p] produces [v] otherwise [None].
    *)
    val opt: 'r t -> 'r option t

    (** The visitor scanner composer. Use [vis ?a ?b f v] to compose a scanner
        that recognizes a sequence of elements in the input stream by applying
        a visitor function [f] at each element to obtain its scanner. The first
        element is visited with the initializer [v], and each following element
        is visited with the value returned by the preceding scanner.

        If [~a] is used, then it specifies the minimum number of elements to
        visit. If [~b] is used then it specifies the maximum number of elements
        to visit. Composition raises [Invalid_argument] if [a < 0] or [b < a].
    *)
    val vis: ?a:int -> ?b:int -> ('r -> 'r t) -> 'r -> 'r t

    (** The homogenous list scanner composer. Use [seq ?a ?b p] to create a new
        scanner that uses [p] to recognize and produce, in order, each element
        in a sequence of elements in the input stream.

        If [~a] is used, then it specifies the minimum number of elements that
        must be recognized and produced in the output. If [~b] is used then it
        specifies the maximum number of elements to recognize. Composition
        raises [Invalid_argument] if [a < 0] or [b < a].
    *)
    val seq: ?a:int -> ?b:int -> 'r t -> 'r list t

    (** The bounded multiple choice scanner. Use [alt ps] to create a scanner
        that produces the output from the first scanner [ps] that produces.
        If no scanner in [ps] produces output, then the resulting scanner
        does not produce.
    *)
    val alt: 'r t list -> 'r t

    (** The unbounded multiple choice scanner. Use [alt ps] to create a scanner
        that produces the output from the first scanner [ps] that produces. If
        no scanner in [ps] produces output, then the resulting scanner does not
        produce.
    *)
    val altz: 'r t Seq.t -> 'r t

    (** {4 Error Parsers} *)

    (** A distinguished syntax failure exception. *)
    type exn += Bad_syntax of string form

    (** Use [fail msg] to raise [Bad_syntax] with [msg] optionally annotated
        with the current position.
    *)
    val fail: string -> 'r t

    (** Use [or_fail msg p] to make a scanner that raises [Bad_syntax] with
        [msg] if [p] does not recognize its input. It may be convenient to call
        this function with a pipeline operator, i.e. [p |> or_fail "reasons"].
    *)
    val or_fail: string -> 'r t -> 'r t

    (** Use [err ~x ()] to make a scanner that raises [x]. If [?x] is not
        provided, then it raises [Not_found].
    *)
    val err: ?x:exn -> unit -> 'r t

    (** Use [errf ~xf ()] to make a scanner that captures a {i mark} and
        applies it to [xf] to raise an exception. If [?xf] is not provided,
        then raises [Not_found].
    *)
    val errf: ?xf:(mark -> 'x) -> unit -> 'r t

    (** Use [req ~x p] to make a scanner that either produces the output of [p]
        or raises [x]. If [p] does not produce and [?x] is not provided, then
        it raises [Not_found].
    *)
    val req: ?x:exn -> 'r t -> 'r t

    (** Use [reqf ~xf p] to make a scanner that either produces the output of
        [p] or captures a {i mark} at the current input and applies it to [xf]
        to raise an exception. If [?xf] is not provided, then raises
        [Not_found].
    *)
    val reqf: ?xf:(mark -> 'x) -> 'r t -> 'r t

    (** The conversion scanner. Use [cast f p] to create a scanner produces the
        result of applying [f] to the value recognized by [p]. If [f] raises
        [Not_found] then the resulting scanner does not produce. If [f] raises
        [Failure s] then it will be caught and [Bad_syntax] will be raised with
        the corresponding message decorated with the position.
    *)
    val cast: ('a -> 'b) -> 'a form t -> 'b form t

    (** The error check scanner. Use [ck p] to create a new scanner that either
        produces either [Ok v] if [p] produces [v] or [Error x] if scanning the
        input with [p] raises the exception [x].
    *)
    val ck: 'r t -> ('r, exn) result t

    (** The error recovery scanner. Use [sync p] to scan the input with [p]
        until it produces or reaches the end of input. Produces [Some v] if [p]
        ever produces [v], otherwise produces [None] if the end of input is
        reached without [p] producing a value.
    *)
    val sync: 'r t -> 'r option t

    (** {4 Elaboration} *)

    (** The type of an input position. *)
    type position

    (** Use [lift p s] to map [s] into a persistent sequence of the values
        produced by [p]. If [~start] is provided, then it specifies the
        starting position of the first symbol in [s].
    *)
    val lift: ?start:position -> 'r t -> symbol Seq.t -> 'r Seq.t

    (** Use [of_seq p s] to parse [s] with [p] and return the result. Raises
        [Not_found] if [p] does not recognize the entire sequence of [s]. If
        [~start] is provided, then it specifies the starting position of the
        first symbol in [s].
    *)
    val of_seq: ?start:position -> 'r t -> symbol Seq.t -> 'r

    (** Combinator operators *)
    module Affix: sig

        (** Include the monad affix operators. *)
        include module type of Affix

        (** The prefix operator version of the [one] composer. *)
        val ( ?. ): symbol -> symbol form t

        (** The prefix operator version of the [opt] composer. *)
        val ( ?/ ): 'r t -> 'r option t

        (** A prefix operator that works like the [seq ~a:1 p] composer. *)
        val ( ?+ ): 'r t -> ('r * 'r list) t

        (** The prefix operator version of the [seq] composer. *)
        val ( ?* ): 'r t -> 'r list t

        (** The prefix operator version of the [alt] composer. *)
        val ( ?^ ): 'r t list -> 'r t

        (** The prefix operator version of the [altz] composer. *)
        val ( ?^~ ): 'r t Seq.t -> 'r t
    end
end

(** Use [Create(B)] to create a basic scanner on the basis of [B]. *)
module Create(B: Basis): Profile
   with type symbol := B.Symbol.t
    and type 'a form := 'a B.Form.t
    and type position := B.position

(* {4 Simple Symbol Streams}

    This module provides a simplified version of the basic scanner that does
    not annotate its input symbols with position.
*)
module Simple: sig

    (** The distinguished simple position type (abstract). *)
    type position

    (** The specialized basis signature of simple scanners. *)
    module type Basis =
        Basis with type position = position and type 'a Form.t = 'a

    (** The specialized signature of simple scanner modules. *)
    module type Profile =
        Profile with type position := position and type 'a form := 'a

    (** The distinguished production form for undecorated values. *)
    module Form: Form with type 'a t = 'a

    (** Use [Basis(S)] to make the simple scanner basis for a symbol. *)
    module Basis(S: Cf_relations.Equal): Basis with type Symbol.t = S.t

    (** Use [Create(S)] to make a simple scanner module. *)
    module Create(S: Cf_relations.Equal): Profile with type symbol := S.t
end

(** {4 Two-stage Parsers}

    This module provides support for a two-stage parsers, where the first stage
    recognizes regular grammar tokens, and the second stage recognizes more
    complex grammar forms.
*)
module Staging: sig

    (** The basis signature for second stage scanners. *)
    module type Basis = sig

        (** The type of the scanner symbol. *)
        type symbol

        (** The type of the scanner position. *)
        type position

        (** The equivalence type for grammar tokens. *)
        module Token: Cf_relations.Equal

        (** The scanner production form (common to both stages). *)
        module Form: Form

        (** The basic scanner module used for recognizing grammar tokens. *)
        module Scan: Profile
           with type symbol := symbol
            and type position := position
            and type 'a form := 'a Form.t

        (** The scanner that recognizes grammar tokens. *)
        val lexer: Token.t Form.t Scan.t
    end

    (** The signature of second-stage scanner modules. *)
    module type Profile = sig

        (** The symbol type of the second stage is a grammer token. *)
        type token
        include Profile with type symbol := token

        (** The basic symbol type. *)
        type symbol

        (** Use [lift_staged p s] to lift [s] into the first stage token
            sequence, then map it into a persistent sequence of the values
            produced by [p]. If [~start] is provided, then it specifies the
            starting position of the first symbol in [s].
        *)
        val lift_staged:
            ?start:position -> 'r t -> symbol Seq.t -> 'r Seq.t

        (** Use [of_seq_staged p s] to lift [s] into the first stage token
            sequence, then parse it with [p], and return the result. Raises
            [Not_found] if [p] does not recognize the entire sequence of [s].
            If [~start] is provided, then it specifies the starting position of
            the first symbol in [s].
        *)
        val of_seq_staged: ?start:position -> 'r t -> symbol Seq.t -> 'r
    end

    (** Use [Create(B)] to make a second-stage parser with [B]. *)
    module Create(B: Basis): Profile
       with type token := B.Token.t
        and type symbol := B.symbol
        and type position := B.position
        and type 'a form := 'a B.Form.t
end

(* {4 For 8-bit Extended ASCII} *)

(** This module provides simple scanners for analyzing texts encoded in
    8bit-extended ASCII text encodings, e.g. ISO 8859 Latin, without any
    concept of stream position.
*)
module ASCII: sig

    (** Include the basic scanner for simple ASCII character sequences. *)
    include Simple.Profile with type symbol := char

    (** The required white space parser, which recognizes a sequence of one or
        more white space characters.
    *)
    val rws: unit t

    (** The required line-breaking white space parser, which recognizes a
        sequence of one or more white space characters excluding line breaks.
    *)
    val rwsl: unit t

    (** The optional white space parser, which recognizes a sequence of zero or
        more white space characters. It is an abbreviation of [opt rws].
    *)
    val ows: unit option t

    (** The optional line-breaking white space parser, which recognizes a
        sequence of zero or more white space characters excluding line breaks.
        It is an abbreviation of [opt rwsl].
    *)
    val owsl: unit option t

    (** Use [lit s obj] to obtain a parser on character input sequences that
        produces the output [obj] when it recognizes the literal [s] in the
        input.
    *)
    val lit: string -> 'r -> 'r t

    (** Use [fmt s c] to make a parser that uses the standard [Scanf] library
        to parse the input according to the format [s], applies the recognized
        values to the receiver [c], and produces the result. Exceptions raised
        by the [Scanf] library functions are not intercepted.

        {b Note Well}: The [Scanf] library is not intended for heavy-duty
        lexical analysis. In particular, a field width should be used in the
        last format specifier of [s] to prevent the over-consumption of input
        stream characters. For example, [fmt "%d" c] will consume the 'x'
        character in the input stream "1000x" while [fmt "%4d" c] will not.
    *)
    val fmt:
        ('a, Scanf.Scanning.in_channel, 'b, 'c -> 'r, 'a -> 'd, 'd) format6 ->
        'c -> 'r t

    (** Use [escape up dn] to create a minimal escape parser that recognizes
        [up] followed either by [dn] or another [up]. The character following
        the first [up] is returned.
    *)
    val escape: char -> char -> char t

    (** The quoted string parser. Use [quoted c] to recognize a string
        delimited by the character [c]. Use the [~esc] argument to specify an
        escape parser. The default escape parser is equivalent to
        [~esc:(escape '\\' c)].
    *)
    val quoted: ?esc:char t -> char -> string t

    (** Use [of_string p s] to parse [s] with [p] and return the result. Raises
        [Not_found] if [p] does not recognize the entire text of [s].
    *)
    val of_string: 'a t -> string -> 'a
end

(*--- End ---*)
