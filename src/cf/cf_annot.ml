(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Meta = struct
    type style = [ `Concise | `Diagnostic ]

    let or_concise s =
        match s with None -> `Concise | Some s -> (s :> style)

    let or_diagnostic s =
        match s with None -> `Diagnostic | Some s -> (s :> style)

    module type Basis = sig
        type symbol
        type position
        type fields

        val default_fields: fields list
        val symbol_type: symbol Cf_type.nym

        val of_opaque_position:
            ?style:[< style ] -> ?fields:fields list -> Cf_type.opaque ->
            position

        val to_opaque_position:
            ?style:[< style ] -> ?fields:fields list -> position ->
            Cf_type.opaque
    end

    let opaque_unit = Cf_type.(witness Unit) ()
    let array_nym = Cf_type.(Seq Opaque)
    let map_nym = Cf_type.(Seq (Pair (Opaque, Opaque)))

    module Field_map = Cf_rbtree.Map.Create(String)

    let of_field (k, v) = Cf_type.(req String) k, v
    let to_field (k, v) = Cf_type.(witness String) k, v
end

module type Basis = sig
    module Position: Cf_relations.Order
    module Symbol: Cf_relations.Equal

    module Meta: Meta.Basis
        with type position := Position.t
         and type symbol := Symbol.t

    val default_initial_position: Position.t
    val advance: Position.t -> Symbol.t -> Position.t
end

module type Profile = sig
    type symbol
    type position
    type iota = private Iota of { symbol: symbol; position: position }
    type span = private Span of { start: position; limit: position }
    type +'a form = private Form of { value: 'a; span: span option }

    module Scan_basis: Cf_scan.Basis
       with type Symbol.t = symbol
        and type iota = iota
        and type position = position
        and type 'a Form.t = 'a form

    include Cf_scan.Form with type 'a t := 'a form

    val up: ?span:span -> 'a -> 'a form
    val map: ('a -> 'b) -> 'a form -> 'b form
    val join: ('a -> 'b -> 'c) -> 'a form -> 'b form -> 'c form
    val collect: 'a form list -> 'a list form
    val force: 'a Lazy.t form -> 'a form
    val to_iotas: ?start:position -> symbol Seq.t -> iota Seq.t

    module Meta: sig
        type fields

        val of_opaque_iota:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (Cf_type.opaque -> symbol) -> Cf_type.opaque -> iota

        val of_opaque_span:
            ?style:[< Meta.style ] -> ?fields:fields list -> Cf_type.opaque ->
            span

        val of_opaque_form:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (Cf_type.opaque -> 'a) -> Cf_type.opaque -> 'a form

        val to_opaque_iota:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (symbol -> Cf_type.opaque) -> iota -> Cf_type.opaque

        val to_opaque_span:
            ?style:[< Meta.style ] -> ?fields:fields list -> span ->
            Cf_type.opaque

        val to_opaque_form:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            ('a -> Cf_type.opaque) -> 'a form -> Cf_type.opaque
    end
end

module Create(B: Basis) = struct
    type symbol = B.Symbol.t
    type position = B.Position.t
    type iota = Iota of { symbol: symbol; position: position }
    type span = Span of { start: position; limit: position }
    type 'a form = Form of { value: 'a; span: span option }

    let imp value = Form { value; span = None }
    let dn (Form r) = r.value
    let mv value (Form r) = Form {r with value }

    let minpos = Cf_relations.min (module B.Position)
    let maxpos = Cf_relations.max (module B.Position)

    let span (Form a) (Form b) value =
        let span =
            match a.span, b.span with
            | None, span
            | span, None ->
                span
            | Some (Span a), Some (Span b) ->
                let start = minpos a.start b.start in
                let limit = maxpos a.limit b.limit in
                Some (Span { start; limit })
        in
        Form { value; span }

    module Scan_basis = struct
        module Symbol = B.Symbol

        type position = B.Position.t
        type nonrec iota = iota

        let init ?start:(position = B.default_initial_position) symbol =
            Iota { symbol; position }

        let next (Iota i) symbol =
            Iota { symbol; position = B.advance i.position i.symbol }

        let sym (Iota i) = i.symbol

        let term (Iota i) =
            let value = i.symbol and start = i.position in
            let limit = B.advance start value in
            let span = Some (Span { start; limit }) in
            Form { value; span  }

        module Form = struct
            type 'v t = 'v form
            let imp = imp
            let dn = dn
            let mv = mv
            let span = span
        end
    end

    let up ?span value = Form { value; span }

    let map f (Form v) = Form { v with value = f v.value }

    let join f (Form a) (Form b) =
        let value = f a.value b.value in
        match a.span, b.span with
        | None, span
        | span, None ->
            Form { value; span }
        | Some (Span a), Some (Span b) ->
            let start = minpos a.start b.start in
            let limit = maxpos a.limit b.limit in
            let span = Some (Span { start; limit }) in
            Form { value; span }

    let collect =
        let rec loop s a b = function
            | [] -> let f _ _ = List.rev s in join f a b
            | hd :: tl -> loop (dn hd :: s) a hd tl
        in
        let enter = function
            | [] -> imp []
            | hd :: [] -> mv [ dn hd ] hd
            | a :: b :: tl -> loop [ dn b; dn a ] a b tl
        in
        enter

    let force a = map Lazy.force a

    let to_iotas =
        let rec loop pos tl () =
            match tl () with
            | Seq.Nil ->
                Seq.Nil
            | Seq.Cons (hd, tl) ->
                let tl = loop (B.advance pos hd) tl in
                let hd = Iota { symbol = hd; position = pos } in
                Seq.Cons (hd, tl)
        in
        let enter ?start:(pos = B.default_initial_position) tl = loop pos tl in
        enter

    module Meta = struct
        type fields = B.Meta.fields

        let or_fields opt =
            match opt with None -> B.Meta.default_fields | Some fields -> fields

        let of_opaque_concise_iota fields unpack value =
            let elements = List.of_seq @@ Cf_type.req Meta.array_nym value in
            if List.length elements <> 2 then raise Cf_type.Type_error;
            let symbol = unpack @@ List.nth elements 0 in
            let position =
                B.Meta.of_opaque_position ~style:`Concise ~fields @@
                List.nth elements 1
            in
            Iota { symbol; position }

        let of_opaque_diagnostic_iota fields unpack value =
            let map =
                Meta.Field_map.of_seq @@ Seq.map Meta.of_field @@
                    Cf_type.req Meta.map_nym value
            in
            let symbol, position =
                try
                    let symbol = Meta.Field_map.require "symbol" map in
                    let position = Meta.Field_map.require "position" map in
                    symbol, position
                with
                | Not_found ->
                     raise Cf_type.Type_error
            in
            let Cf_type.Witness (v0, nym0) = symbol in
            let Cf_type.Witness (v1, nym1) = position in
            let symbol = unpack @@ Cf_type.(witness nym0) v0 in
            let position =
                B.Meta.of_opaque_position ~style:`Diagnostic
                    ~fields @@ Cf_type.(witness nym1) v1
            in
            Iota { symbol; position }

        let of_opaque_iota ?style ?fields unpack value =
            match or_fields fields with
            | [] ->
                let symbol = unpack value in
                let position =
                    B.Meta.of_opaque_position ?style ~fields:[]
                        Meta.opaque_unit
                in
                Iota { symbol; position }
            | fields ->
                match Meta.or_concise style with
                | `Diagnostic ->
                    of_opaque_diagnostic_iota fields unpack value
                | `Concise ->
                    of_opaque_concise_iota fields unpack value

        let of_opaque_concise_span fields value =
            let elements = List.of_seq @@ Cf_type.req Meta.array_nym value in
            if List.length elements <> 2 then raise Cf_type.Type_error;
            let f = B.Meta.of_opaque_position ~style:`Concise ~fields in
            let positions = List.map f elements in
            let start = List.nth positions 0 in
            let limit = List.nth positions 1 in
            Span { start; limit }

        let of_opaque_diagnostic_span fields value =
            let map =
                Meta.Field_map.of_seq @@ Seq.map Meta.of_field @@
                    Cf_type.req Meta.map_nym value
            in
            let size = Meta.Field_map.size map in
            if size <> 2 then raise Cf_type.Type_error;
            let start, limit =
                try
                    let start = Meta.Field_map.require "start" map in
                    let limit = Meta.Field_map.require "limit" map in
                    start, limit
                with
                | Not_found ->
                    raise Cf_type.Type_error
            in
            let Cf_type.Witness (v0, nym0) = start in
            let Cf_type.Witness (v1, nym1) = limit in
            let start =
                B.Meta.of_opaque_position ~style:`Diagnostic ~fields @@
                Cf_type.(witness nym0) v0
            and limit =
                B.Meta.of_opaque_position ~style:`Diagnostic ~fields @@
                Cf_type.(witness nym1) v1
            in
            Span { start; limit }

        let of_opaque_span ?style ?fields value =
            match or_fields fields with
            | [] ->
                let pos =
                    B.Meta.of_opaque_position ?style ~fields:[]
                        Meta.opaque_unit
                in
                Span { start = pos; limit = pos }
            | fields ->
                match Meta.or_concise style with
                | `Diagnostic ->
                    of_opaque_diagnostic_span fields value
                | `Concise ->
                    of_opaque_concise_span fields value

        let of_opaque_concise_form fields unpack value =
            let elements = List.of_seq @@ Cf_type.req Meta.array_nym value in
            let length = List.length elements in
            if length <> 2 then raise Cf_type.Type_error;
            let value = unpack @@ List.nth elements 0 in
            let span =
                let element = List.nth elements 1 in
                if Cf_type.(ck Unit) element then
                    None
                else
                    Some (of_opaque_concise_span fields element)
            in
            Form { value; span }

        let of_opaque_diagnostic_form fields unpack value =
            let map =
                Meta.Field_map.of_seq @@ Seq.map Meta.of_field @@
                    Cf_type.req Meta.map_nym value
            in
            let size = Meta.Field_map.size map in
            if size <> 2 then raise Cf_type.Type_error;
            let value, span =
                try
                    let value = Meta.Field_map.require "value" map in
                    let span = Meta.Field_map.require "span" map in
                    value, span
                with
                | Not_found ->
                    raise Cf_type.Type_error
            in
            let Cf_type.Witness (v0, nym0) = value in
            let Cf_type.Witness (v1, nym1) = span in
            let value = unpack @@ Cf_type.(witness nym0) v0 in
            let span =
                match nym1 with
                | Cf_type.Unit ->
                    None
                | _ ->
                    Some begin
                        of_opaque_span ~style:`Diagnostic ~fields @@
                        Cf_type.(witness nym1) v1
                    end
            in
            Form { value; span }

        let of_opaque_form ?style ?fields unpack value =
            match or_fields fields with
            | [] ->
                Form { value = unpack value; span = None }
            | fields ->
                match Meta.or_concise style with
                | `Diagnostic ->
                    of_opaque_diagnostic_form fields unpack value
                | `Concise ->
                    of_opaque_concise_form fields unpack value

        let to_opaque_iota ?style ?fields pack (Iota i) =
            let fields = or_fields fields in
            let symbol = pack i.symbol in
            match fields with
            | [] ->
                symbol
            | fields ->
                let position =
                    B.Meta.to_opaque_position ?style ~fields i.position
                in
                match Meta.or_concise style with
                | `Concise ->
                    Cf_type.witness Meta.array_nym @@ List.to_seq [
                        symbol;
                        position;
                    ]
                | `Diagnostic ->
                    Cf_type.witness Meta.map_nym @@ Seq.map Meta.to_field @@
                        List.to_seq [
                            "symbol", symbol;
                            "position", position;
                        ]

        let to_opaque_span ?style ?fields span =
            match or_fields fields with
            | [] ->
                Meta.opaque_unit
            | fields ->
                let Span s = span in
                let start = B.Meta.to_opaque_position ?style ~fields s.start in
                let limit = B.Meta.to_opaque_position ?style ~fields s.limit in
                match Meta.or_concise style with
                | `Concise ->
                    Cf_type.witness Meta.array_nym @@ List.to_seq [
                        start;
                        limit
                    ]
                | `Diagnostic ->
                    Cf_type.witness Meta.map_nym @@ Seq.map Meta.to_field @@
                        List.to_seq [
                            "start", start;
                            "limit", limit;
                        ]

        let to_opaque_form ?style ?fields pack (Form loc) =
            let fields = or_fields fields in
            let value = pack loc.value in
            match fields with
            | [] ->
                value
            | fields ->
                let span =
                    match loc.span with
                    | None -> Meta.opaque_unit
                    | Some span -> to_opaque_span ?style ~fields span
                in
                match Meta.or_concise style with
                | `Concise ->
                    Cf_type.witness Meta.array_nym @@ List.to_seq [
                        value;
                        span
                    ]
                | `Diagnostic ->
                    Cf_type.witness Meta.map_nym @@ Seq.map Meta.to_field @@
                        List.to_seq [
                            "value", value;
                            "span", span;
                        ]
    end
end

module Coded = struct
    let pos0 =
        let sxr = new Cf_decode.scanner () in
        sxr#position

    module Meta_basis = struct
        type fields = [ `Offsets ]
        let default_fields = `Offsets :: []

        let of_opaque_position ?style:_ ?fields:_ value =
            let n = Cf_type.(req Int) value in
            Cf_decode.advance n pos0

        let to_opaque_position ?style:_ ?fields:_ pos =
            let Cf_decode.Position p = pos in
            Cf_type.(witness Int) p
    end

    module type Basis = sig
        module Symbol: Cf_relations.Equal
        val symbol_type: Symbol.t Cf_type.nym
        val advance: Cf_decode.position -> Symbol.t -> Cf_decode.position
    end

    module type Profile = Profile with type position := Cf_decode.position

    module Create(B: Basis) = struct
        module Aux = struct
            module Symbol = B.Symbol

            module Position = struct
                type t = Cf_decode.position

                let compare (Cf_decode.Position a) (Cf_decode.Position b) =
                    Cf_relations.Int.compare a b
            end

            let default_initial_position =
                let sxr = new Cf_decode.scanner () in
                sxr#position

            let advance = B.advance

            module Meta = struct
                include Meta_basis
                let symbol_type = B.symbol_type
            end
        end

        include Create(Aux)
    end
end

module Textual = struct
    module Serial = struct
        type t = int64

        let equal = Int64.equal

        let min_delta = Int64.div Int64.min_int 2L
        let max_delta = Int64.div Int64.max_int 2L

        let fail () = failwith (__MODULE__ ^ ".Serial: overflow!")

        let compare a b =
            let d = Int64.sub b a in
            if Int64.equal d 0L then
                0
            else if d > 0L then begin
                if d > max_delta then fail ();
                (-1)
            end
            else begin
                if d < min_delta then fail ();
                (1)
            end

        let zero = 0L
        let succ = Int64.succ
    end

    type line = Line of {
        number: int;
        stream: string;
        octets: int64;
        crpred: bool;
    }

    type position = Position of {
        serial: Serial.t;
        line: line;
        column: int;
        lnadj: int64;
    }

    module Meta_basis = struct
        type fields = [ `Serial | `Stream | `Offsets | `Lines ]
        let default_fields = [ `Serial; `Stream; `Offsets; `Lines ]

        let default_line = Line {
            number = (-1);
            stream = "";
            octets = (-1L);
            crpred = false;
        }

        let default_position = Position {
            serial = (-1L);
            line = default_line;
            column = 0;
            lnadj = 0L;
        }

        module Field_map = Cf_rbtree.Map.Create(String)

        let map_require key map =
            try Field_map.require key map with
            | Not_found -> raise Cf_type.Type_error

        let int_or_int64 v =
            let open Cf_type in
            if ck Int64 v then
                req Int64 v
            else if ck Int v then
                Int64.of_int @@ req Int v
            else
                raise Cf_type.Type_error

        let rec of_opaque_concise_line line seq tl =
            match tl with
            | [] ->
                if not (Cf_seq.empty seq) then raise Cf_type.Type_error;
                line
            | hd :: tl ->
                let Line ln = line in
                let seq, line =
                    match seq (), hd with
                    | _, `Serial ->
                        seq, line
                    | Seq.Nil, _ ->
                        raise Cf_type.Type_error
                    | Seq.Cons (v, seq), `Stream ->
                        let stream = Cf_type.(req String) v in
                        seq, Line { ln with stream }
                    | Seq.Cons (v, seq), `Offsets ->
                        let octets = int_or_int64 v in
                        seq, Line { ln with octets }
                    | Seq.Cons (v0, seq), `Lines ->
                        match seq () with
                        | Seq.Nil ->
                            raise Cf_type.Type_error
                        | Seq.Cons (v1, seq) ->
                            let number = Cf_type.(req Int) v0 in
                            let crpred = Cf_type.(req Bool) v1 in
                            seq, Line { ln with number; crpred }
                in
                of_opaque_concise_line line seq tl

        let rec of_opaque_diagnostic_line line map tl =
            match tl with
            | [] ->
                line
            | hd :: tl ->
                let Line ln = line in
                let line =
                    match hd with
                    | `Serial ->
                        line
                    | `Stream ->
                        let field = map_require "stream" map in
                        let stream = Cf_type.(req String) field in
                        Line { ln with stream }
                    | `Offsets ->
                        let field = map_require "octets" map in
                        let octets = int_or_int64 field in
                        Line { ln with octets }
                    | `Lines ->
                        let field0 = map_require "number" map in
                        let field1 = map_require "crpred" map in
                        let number = Cf_type.(req Int) field0 in
                        let crpred = Cf_type.(req Bool) field1 in
                        Line { ln with number; crpred }
                in
                of_opaque_diagnostic_line line map tl

        let rec of_opaque_concise_position fields position seq tl =
            match tl with
            | [] ->
                if not (Cf_seq.empty seq) then raise Cf_type.Type_error;
                position
            | hd :: tl ->
                let Position p = position in
                let seq, position =
                    match seq (), hd with
                    | _, `Stream ->
                        seq, position
                    | Seq.Nil, _ ->
                        raise Cf_type.Type_error
                    | Seq.Cons (v, seq), `Serial ->
                        let serial = int_or_int64 v in
                        seq, Position { p with serial }
                    | Seq.Cons (v, seq), `Offsets ->
                        let lnadj = int_or_int64 v in
                        seq, Position { p with lnadj }
                    | Seq.Cons (v0, seq), `Lines ->
                        match seq () with
                        | Seq.Nil ->
                            raise Cf_type.Type_error
                        | Seq.Cons (v1, seq) ->
                             let v0 = Cf_type.(req Meta.array_nym) v0 in
                            let line =
                                of_opaque_concise_line p.line v0 fields
                            in
                            let column = Cf_type.(req Int) v1 in
                            seq, Position { p with line; column }
                in
                of_opaque_concise_position fields position seq tl

        let rec of_opaque_diagnostic_position fields position map tl =
            match tl with
            | [] ->
                position
            | hd :: tl ->
                let Position p = position in
                let position =
                    match hd with
                    | `Serial ->
                        let field = map_require "serial" map in
                        let serial = int_or_int64 field in
                        Position { p with serial }
                    | `Stream ->
                        position
                    | `Offsets ->
                        let field = map_require "lnadj" map in
                        let lnadj = int_or_int64 field in
                        Position { p with lnadj }
                    | `Lines ->
                        let field0 = map_require "column" map in
                        let field1 = map_require "line" map in
                        let line =
                            let map =
                                Field_map.of_seq @@ List.to_seq @@
                                    List.rev_map Meta.of_field @@
                                    Cf_seq.reverse @@
                                    Cf_type.req Meta.map_nym field0
                            in
                            of_opaque_diagnostic_line default_line map fields
                        in
                        let column = Cf_type.(req Int) field1 in
                        Position { p with line; column }
                in
                of_opaque_diagnostic_position fields position map tl

        let of_opaque_position ?style ?(fields = default_fields) value =
            let position = default_position in
            match Meta.or_concise style with
            | `Concise ->
                let seq = Cf_type.req Meta.array_nym value in
                of_opaque_concise_position fields position seq fields
            | `Diagnostic ->
                let map =
                    Field_map.of_seq @@ List.to_seq @@
                        List.rev_map Meta.of_field @@ Cf_seq.reverse @@
                        Cf_type.req Meta.map_nym value
                in
                of_opaque_diagnostic_position fields position map fields

        let rec to_opaque_concise_line values line tl =
            match tl with
            | [] ->
                Cf_type.witness Meta.array_nym @@ List.(to_seq @@ rev values)
            | hd :: tl ->
                let Line ln = line in
                let values =
                    match hd with
                    | `Serial ->
                        values
                    | `Stream ->
                        Cf_type.(witness String) ln.stream :: values
                    | `Offsets ->
                        Cf_type.(witness Int64) ln.octets :: values
                    | `Lines ->
                        let crpred = Cf_type.(witness Bool) ln.crpred in
                        let number = Cf_type.(witness Int) ln.number in
                        crpred :: number :: values
                in
                to_opaque_concise_line values line tl

        let rec to_opaque_diagnostic_line values line tl =
            match tl with
            | [] ->
                Cf_type.witness Meta.map_nym @@
                    List.(to_seq @@ rev_map Meta.to_field values)
            | hd :: tl ->
                let Line ln = line in
                let values =
                    match hd with
                    | `Serial ->
                        values
                    | `Stream ->
                        let value = Cf_type.(witness String) ln.stream in
                        ("stream", value) :: values
                    | `Offsets ->
                        let value = Cf_type.(witness Int64) ln.octets in
                        ("octets", value) :: values
                    | `Lines ->
                        let crpred = Cf_type.(witness Bool) ln.crpred in
                        let number = Cf_type.(witness Int) ln.number in
                        ("crpred", crpred) :: ("number", number) :: values
                in
                to_opaque_diagnostic_line values line tl

        let rec to_opaque_concise_position fields values position tl =
            match tl with
            | [] ->
                Cf_type.witness Meta.array_nym @@ List.(to_seq @@ rev values)
            | hd :: tl ->
                let Position p = position in
                let values =
                    match hd with
                    | `Serial ->
                        Cf_type.(witness Int64) p.serial :: values
                    | `Stream ->
                        values
                    | `Offsets ->
                        Cf_type.(witness Int64) p.lnadj :: values
                    | `Lines ->
                        let line = to_opaque_concise_line [] p.line fields in
                        let column = Cf_type.(witness Int) p.column in
                        column :: line :: values
                in
                to_opaque_concise_position fields values position tl

        let rec to_opaque_diagnostic_position fields values position tl =
            match tl with
            | [] ->
                Cf_type.witness Meta.map_nym @@
                    List.(to_seq @@ rev_map Meta.to_field values)
            | hd :: tl ->
                let Position p = position in
                let values =
                    match hd with
                    | `Serial ->
                        let value = Cf_type.(witness Int64) p.serial in
                        ("serial", value) :: values
                    | `Stream ->
                        values
                    | `Offsets ->
                        let value = Cf_type.(witness Int64) p.lnadj in
                        ("lnadj", value) :: values
                    | `Lines ->
                        let line =
                            to_opaque_diagnostic_line [] p.line fields
                        in
                        let column = Cf_type.(witness Int) p.column in
                        ("line", line) :: ("column", column) :: values
                in
                to_opaque_diagnostic_position fields values position tl

        let to_opaque_position ?style ?(fields = default_fields) position =
            match Meta.or_concise style with
            | `Concise ->
                to_opaque_concise_position fields [] position fields
            | `Diagnostic ->
                to_opaque_diagnostic_position fields [] position fields
    end

    let initial_position stream =
        let ln0 = Line { number = 1; stream; octets = 0L; crpred = false } in
        Position { serial = Serial.zero; line = ln0; column = 1; lnadj = 0L }

    module type Basis = sig
        module Symbol: Cf_relations.Equal
        val symbol_type: Symbol.t Cf_type.nym
        val advance: position -> Symbol.t -> position
    end

    module type Profile = sig
        include Profile
            with type position := position
             and type Meta.fields = [ `Serial | `Stream | `Offsets | `Lines ]

        val emit_form: Format.formatter -> 'a form -> unit
    end

    module Create(B: Basis) = struct
        module Aux = struct
            module Symbol = B.Symbol
            module Position = struct
                type t = position

                let compare (Position a) (Position b) =
                    Serial.compare a.serial b.serial
            end

            let default_initial_position = initial_position ""
            let advance = B.advance

            module Meta = struct
                include Meta_basis
                let symbol_type = B.symbol_type
            end
        end

        include Create(Aux)

        let emit_span pp (Position p0) (Position p1) =
            let Line l0 = p0.line and Line l1 = p1.line in
            let n0 = l0.stream and n1 = l1.stream in
            let ln0 = l0.number and ln1 = l1.number in
            let c0 = p0.column and c1 = p1.column in
            if n0 <> n1 then
                Format.fprintf pp "%s:%u:%u-%s:%u:%u" n0 ln0 c0 n1 ln1 c1
            else if ln0 < ln1 then
                Format.fprintf pp "%s::%u:%u-%u:%u" n0 ln0 c0 ln1 c1
            else if c0 < c1 then
                Format.fprintf pp "%s:%u::%u-%u" n0 ln0 c0 c1
            else
                Format.fprintf pp "%s:%u:%u" n0 ln0 c0

        let emit_form pp (Form v) =
            match v.span with
            | None -> Format.pp_print_string pp "<implicit>"
            | Some (Span s) -> emit_span pp s.start s.limit
    end

    module ASCII = struct
        module Basis = struct
            module Symbol = Char

            let symbol_type = Cf_type.Char

            let advance (Position p) c =
                let Line ln = p.line in
                let lnadj = Int64.succ p.lnadj in
                let line, column, lnadj =
                    let cn = Char.code c in
                    if ln.crpred && cn = 10 then begin
                        let octets = Int64.succ ln.octets and crpred = false in
                        Line { ln with octets; crpred }, 1, 0L
                    end
                    else begin [@warning "-4"]
                        match cn with
                        | (10 | 13) ->
                            let number = succ ln.number in
                            let octets = Int64.add ln.octets lnadj in
                            let crpred = (cn = 13) in
                            Line { ln with number; octets; crpred }, 1, 0L
                        | 8 ->
                            let column = (((p.column - 1) / 8) + 1) * 8 + 1 in
                            p.line, column, lnadj
                        | _ ->
                            p.line, succ p.column, lnadj

                    end
                in
                let serial = Serial.succ p.serial in
                Position { serial; line; column; lnadj }
        end

        include Create(Basis)
    end

    module Unicode = struct
        module type Basis = sig
            val size_of_uchar: Uchar.t -> int
            val is_grapheme_base: Uchar.t -> bool
        end

        module type Profile = Profile with type symbol := Uchar.t

        module Aux(B: Basis) = struct
            module Symbol = Uchar

            let symbol_type = Cf_type.Uchar

            let advance (Position p) c =
                let Line ln = p.line in
                let cp = Uchar.to_int c in
                let w = B.size_of_uchar c |> Int64.of_int in
                let lnadj = Int64.add p.lnadj w in
                let line, column, lnadj =
                    if ln.crpred && cp = 10 then begin
                        let octets = Int64.add ln.octets w and crpred = false in
                        Line { ln with octets; crpred }, 1, 0L
                    end
                    else begin [@warning "-4"]
                        match cp with
                        | (10 | 13 | 133 | 2028 | 2029) ->
                            let number = succ ln.number in
                            let octets = Int64.add ln.octets lnadj in
                            let crpred = (cp = 13) in
                            Line { ln with number; octets; crpred }, 1, 0L
                        | 8 ->
                            let column = (((p.column - 1) / 8) + 1) * 8 + 1 in
                            p.line, column, lnadj
                        | _ ->
                            let column =
                                if B.is_grapheme_base c then
                                    succ p.column
                                else
                                    p.column
                            in
                            p.line, column, lnadj

                    end
                in
                let serial = Serial.succ p.serial in
                Position { serial; line; column; lnadj }
        end

        module Create(B: Basis) = Create(Aux(B))
    end
end

(*--- End ---*)
