(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** The state-continuation monad and its operators. *)

(** {6 Overview}

    The state-continuation monad is the composition of the continuation monad
    and the state monad: a function for passing intermediate results from
    continuation context to continuation context with an encapsulated state
    value at each stage.
*)

(** {6 Types} *)

(** The state-continuation monad type. *)
type ('s, 'x, 'r) t = ('s -> 'x, 'r) Cf_cmonad.t
module Basis: Cf_monad.Trinary.Basis with type ('s, 'x, 'r) t := ('s, 'x, 'r) t
include Cf_monad.Trinary.Profile with type ('s, 'x, 'r) t := ('s, 'x, 'r) t

(** {6 Operators} *)

(** A monad that returns [unit] and performs no operation. *)
val nil: ('s, 'x, unit) t

(** Use [init x] to produce a monad that discards the current intermediate
    context and passes [x] into the continuation.
*)
val init: 'x -> ('s, 'x, 'r) t

(** Use [cont f] to produce a monad that applies [f] to the current
    intermediate context and passes the result into the continuation.
*)
val cont: ('x -> 'x) -> ('s, 'x, unit) t

(** A monad that returns the encapsulate state. *)
val load: ('s, 'x, 's) t

(** Use [store s] to produce a monad with [s] as the encapsulated state. *)
val store: 's -> ('s, 'x, unit) t

(** Use [modify f] to produce a monad that applies [f] to the encapsulated
    state to obtain a new state value.
*)
val modify: ('s -> 's) -> ('s, 'x, unit) t

(** Use [field f] to produce a monad that applies [f] to the encapsulated state
    and returns the result.
*)
val field: ('s -> 'r) -> ('s, 'x, 'r) t

(** Use [downC m s] to lower [m] initialized with state [s] into a continuation
    monad that returns the final state.
*)
val downC: ('s, 'x, unit) t -> 's -> ('x, 's) Cf_cmonad.t

(** Use [downS m x] to lower [m] initialized with context [x] into a state
    monad that returns the final context without modifying the encapsulated
    state.
*)
val downS: ('s, 'x, unit) t -> 'x -> ('s, 'x) Cf_smonad.t

(** Use [liftC m] to lift a stateless continuation monad [m] into a
    state-continuation monad.
*)
val liftC: ('x, 'r) Cf_cmonad.t -> ('s, 'x, 'r) t

(** Use [liftS m] to lift a state monad [m] into a state-continuation monad. *)
val liftS: ('s, 'r) Cf_smonad.t -> ('s, 'x, 'r) t

(** Use [eval m s] to evaluate the state-continuation monad [m] with initial
    state [s] to produce a function from an initial context to a final context.
*)
val eval: ('s, 'x, unit) t -> 's -> 'x -> 'x

(** Use [bridge x f m] to lower the state-continuation monad [m], then bridge
    with the initial context [x] and a map function [f], and finally lift the
    result to another state-contination monad, one that evaluates [m] using the
    state bridged from the result monad.
*)
val bridge: 'x -> ('x -> 'y) -> ('s, 'x, unit) t -> ('s, 'y, unit) t

(*--- End ---*)
