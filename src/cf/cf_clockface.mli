(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Civil time in the 24-hour digital clock face notation. *)

(** {6 Overview} *)

(** This module implements record types inhabited by 24-hour digital clock face
    indications. It also provides related functions and types to facilitate
    conversion to and from various formats for data interchange.
*)

(** The private type inhabited by 24-hour civil time values. *)
type time = private Time of {
    hour: int;      (** 0 .. 23 *)
    minute: int;    (** 0 .. 59 *)
    second: int;    (** 0 .. 59, 60 if leap second *)
}

(** Use [create ~hour ~minute ~second] to create a 24-hour clock time value
    according to the [~hour], [~minute] and [~second] parameters. Raises
    [Invalid_argument] if the time specified is not a valid clock time. A clock
    time with [second = 60] is only valid when [hour = 23] and [minute = 59]
    (as leap seconds may appear only at the end of an day, according to
    standard).
*)
val create: hour:int -> minute:int -> second:int -> time

(** Use [to_seconds t] to convert the time to an integer number of seconds
    since midnight (without counting any preceding leap seconds).
*)
val to_seconds: time -> int

(**/**)
val unsafe_create: int -> int -> int -> time

(*--- End ---*)
