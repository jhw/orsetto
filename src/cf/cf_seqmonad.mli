(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Monad functions for functional progressive sequences. *)

(** {6 Overview} *)

(** This module augments the core monad interfaces with functions that
    facilitate monad functions that operate on sequences of monad values. It
    also provides a distinguished monad that encapsulates a sequence.
*)

(** {6 Functor Module}

    This module defines monad functors that produce modules with functions that
    operate on sequences of monads.
*)
module Functor: sig

    (** The unary monad support module type. *)
    module type Unary = sig

        (** The monad type. *)
        type +'r t

        (** Use [collect s] to bind in sequence every monad value in the
            finite sequence [s] and collect all the returned values. Returns
            [(n, s)] where [n] is the number of values collected and [s] is the
            list of values in reverse order, i.e. from last collected to first
            collected. Never returns and exhausts all memory if [s] never
            terminates.
        *)
        val collect: 'r t Seq.t -> (int * 'r list) t

        (** Use [serial s] to bind in sequence every monad value in the
            sequence [s].
        *)
        val serial: unit t Seq.t -> unit t
    end

    (** The binary monad support module type. *)
    module type Binary = sig

        (** The monad type. *)
        type ('m, +'r) t

        (** Use [collect s] to bind in sequence every monad value in the
            finite sequence [s] and collect all the returned values. Returns
            [(n, s)] where [n] is the number of values collected and [s] is the
            list of values in reverse order, i.e. from last collected to first
            collected. Never returns and exhausts all memory if [s] never
            terminates.
        *)
        val collect: ('m, 'r) t Seq.t -> ('m, int * 'r list) t

        (** Use [serial s] to bind in sequence every monad value in the
            sequence [s].
        *)
        val serial: ('m, unit) t Seq.t -> ('m, unit) t
    end

    (** The trinary monad support module type. *)
    module type Trinary = sig

        (** The monad type. *)
        type ('p, 'q, +'r) t

        (** Use [collect s] to bind in sequence every monad value in the
            finite sequence [s] and collect all the returned values. Returns
            [(n, s)] where [n] is the number of values collected and [s] is the
            list of values in reverse order, i.e. from last collected to first
            collected. Never returns and exhausts all memory if [s] never
            terminates.
        *)
        val collect: ('p, 'q, 'r) t Seq.t -> ('p, 'q, int * 'r list) t

        (** Use [serial s] to bind in sequence every monad value in the
            sequence [s].
        *)
        val serial: ('p, 'q, unit) t Seq.t -> ('p, 'q, unit) t
    end

    (** Aliases and inclusions. *)
    module Core = Cf_monad_core

    (** The unary monad support composer. *)
    module Unary(B: Core.Unary.Basis):
        Unary with type 'r t = 'r B.t

    (** The binary monad support composer. *)
    module Binary(B: Core.Binary.Basis):
        Binary with type ('m, 'r) t = ('m, 'r) B.t

    (** The trinary monad support composer. *)
    module Trinary(B: Core.Trinary.Basis):
        Trinary with type ('p, 'q, 'r) t = ('p, 'q, 'r) B.t
end

(** {6 Sequence Monad} *)

(** The type of a monad encapsulating a sequence. *)
type ('m, +'r) t

(** Module inclusions *)
open Cf_monad_core.Binary
module Basis: Basis with type ('m, 'r) t := ('m, 'r) t
include Profile with type ('m, 'r) t := ('m, 'r) t
include Functor.Binary with type ('m, 'r) t := ('m, 'r) t

(** Use [one v] to produce the value [v] in the encapsulated sequence. *)
val one: 'm -> ('m, unit) t

(** Use [all s] to produce each value consumed from [s] in the encapsulated
    sequence.
*)
val all: 'm Seq.t -> ('m, unit) t

(** Use [eval m] to convert [m] into its encapsulated sequence. *)
val eval: ('m, unit) t -> 'm Seq.t

(*--- End ---*)
