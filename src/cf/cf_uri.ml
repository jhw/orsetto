(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary containers, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary container must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type host = ..

type host +=
    | H_name of string
    | H_ipv4 of { address: string }
    | H_ipv6 of { address: string; zoneid: string option }
    | H_ip_future of { version: int; address: string }

type authority = Authority of {
    user: string option;
    host: host;
    port: int option;
}

class basis ?authority ~absolute ?(path = []) ?query () =
    let absolute = absolute || (authority <> None) in
    object
        method authority: authority option = authority
        method absolute: bool = absolute
        method path: string list = path
        method query: string option = query
    end

class type absolute = object
    inherit basis
    method scheme: string
end

class relative ?authority ~absolute ?path ?query ?fragment () = object
    inherit basis ?authority ~absolute ?path ?query ()
    method fragment: string option = fragment
end

class generic ~scheme ?authority ~absolute ?path ?query ?fragment () = object
    inherit relative ?authority ~absolute ?path ?query ?fragment ()
    method scheme: string = scheme
end

type +'a container = Object of 'a constraint 'a = #basis [@@unboxed]

type t = generic container

type reference =
    | Generic of generic container
    | Relative of relative container

let int_of_A = (int_of_char 'A') - 10
let int_of_a = (int_of_char 'a') - 10
let int_of_0 = int_of_char '0'

let int_of_hexdigit c =
    let n0 =
        match c with
        | 'A'..'F' -> int_of_A
        | 'a'..'f' -> int_of_a
        | '0'..'9' -> int_of_0
        | _ -> assert (not true); 0
    in
    (int_of_char c) - n0

let hexdigit_of_int n =
    assert (n >= 0 && n < 16);
    char_of_int (n + (if n > 9 then int_of_A else int_of_0))

let ch_is_unreserved c =
    match c with
    | ('A'..'Z' | 'a'..'z' | '0'..'9' | '-' | '.' | '_' | '~') -> true
    | _ -> false

let ch_is_sub_delim c =
    match c with
    | ('!' | '$' | '&' | '\'' | '(' | ')' | '*' | '+' | ',' | ';' | '=') ->
        true
    | c ->
        ch_is_unreserved c

let ch_is_hexdigit c =
    match c with
    | ('0'..'9' | 'A'..'F' | 'a'..'f') -> true
    | _ -> false

let ch_is_alpha c = match c with 'A'..'Z' | 'a'..'z' -> true | _ -> false
let ch_is_digit c = match c with '0'..'9' -> true | _ -> false

let percent_decode =
    let failure m =
        failwith @@ Printf.sprintf "%s.percent_decode: %s!" __MODULE__ m
    in
    let rec loop tl () =
        match tl () with
        | Seq.Nil ->
            Seq.Nil
        | Seq.Cons (hd, tl) ->
            if hd <> '%' then
                Seq.Cons (hd, loop tl)
            else
                (digits[@tailcall]) 0 2 tl
    and digits cp i tl =
        if cp > 0 then begin
            match tl () with
            | Seq.Nil ->
                failure "unexpected termination"
            | Seq.Cons (hd, tl) ->
                if not (ch_is_hexdigit hd) then failure "not a hex digit";
                let cp = (cp lsl 4) lor (int_of_hexdigit hd) and i = pred i in
                (digits[@tailcall]) cp i tl
        end
        else
            Seq.Cons (Char.chr cp, loop tl)
    in
    loop

let percent_encode =
    let rec loop allow tl () =
        match tl () with
        | Seq.Nil ->
            Seq.Nil
        | Seq.Cons (hd, tl) ->
            if ch_is_unreserved hd || allow hd then
                Seq.Cons (hd, loop allow tl)
            else begin
                let w = allow, tl and n = int_of_char hd in
                Seq.Cons ('%', digits w 2 n)
            end
    and digits w i n () =
        if i > 0 then begin
            let hd = hexdigit_of_int @@ n land 0xf in
            let tl = digits w (pred i) (n lsr 4) in
            Seq.Cons (hd, tl)
        end
        else begin
            let allow, tl = w in
            (loop[@tailcall]) allow tl ()
        end
    in
    let allow0 _ = false in
    let enter ?(allow = allow0) s () = (loop[@tailcall]) allow s () in
    enter

let resolve_dot_segments =
    let rec loop ~stack absolute = function
        | [] ->
            List.rev stack
        | hd :: tl ->
            let stack =
                if hd = "." then begin
                    if tl = [] then
                        if not absolute && stack = [] then
                            (* [relative] "." -> "." *)
                            [ hd ]
                        else
                            (* "p/." -> "p/" *)
                            "" :: stack
                    else
                        (* "p1/./p2" -> "p1/p2" *)
                        stack
                end
                else if hd = ".." then begin
                    match stack with
                    | []
                    | ".." :: _ ->
                        if not absolute then
                            (* [relative] ".."  -> ".." *)
                            (* [relative] "../.."  -> "../.." *)
                            hd :: stack
                        else begin
                            (* [absolute] ".." -> "" *)
                            assert absolute;
                            []
                        end
                    | _ :: stack ->
                        if tl = [] then
                            if not absolute && stack = [] then
                                (* [relatve] "s/.." -> "." *)
                                [ "." ]
                            else
                                (* "p/s/.." -> "p/" *)
                                "" :: stack
                        else
                            (* "a/../b" -> "b" *)
                            stack
                end
                else
                    hd :: stack
            in
            (loop[@tailcall]) ~stack absolute tl
    in
    loop ~stack:[]

module Emit = struct
    let allow_username = function
        | ':' -> true
        | c -> ch_is_sub_delim c

    let allow_segment = function
        | (':' | '@') -> true
        | c -> ch_is_sub_delim c

    let allow_query_fragment = function
        | ('/' | '?') -> true
        | c -> allow_segment c

    let b_encode ?allow b text =
        Buffer.add_string b @@ Cf_seq.string_pipe (percent_encode ?allow) text

    let b_ipv6_zoneid b = function
        | None ->
            ()
        | Some id ->
            Buffer.add_string b "%25";
            (b_encode[@tailcall]) b id

    let b_host b = function
        | H_name name ->
            b_encode ~allow:ch_is_sub_delim b name
        | H_ipv4 ip ->
            Buffer.add_string b ip.address
        | H_ipv6 ip ->
            Buffer.add_char b '[';
            Buffer.add_string b ip.address;
            b_ipv6_zoneid b ip.zoneid;
            Buffer.add_char b ']'
        | H_ip_future ip ->
            Buffer.add_string b "[v";
            Buffer.add_string b @@ Printf.sprintf "%x" ip.version;
            Buffer.add_char b '.';
            Buffer.add_string b ip.address;
            Buffer.add_char b ']'
        | _ ->
            assert (not true)

    let b_userinfo b = function
        | None ->
            ()
        | Some name ->
            b_encode ~allow:allow_username b name;
            Buffer.add_char b '@'

    let b_port b = function
        | None ->
            ()
        | Some port ->
            Buffer.add_char b ':';
            Buffer.add_string b @@ Printf.sprintf "%d" port

    let b_authority b (Authority a) =
        b_userinfo b a.user;
        b_host b a.host;
        (b_port[@tailcall]) b a.port

    let rec b_segment_list ~sep b = function
        | [] ->
            ()
        | hd :: tl ->
            if sep then Buffer.add_char b '/';
            b_encode ~allow:allow_segment b hd;
            (b_segment_list[@tailcall]) b ~sep:true tl

    let b_hierarchy ~abs b uri =
        let uri = (uri :> basis) in
        match uri#authority with
        | Some a ->
            Buffer.add_string b "//";
            b_authority b a;
            (b_segment_list[@tailcall]) ~sep:true b uri#path
        | None ->
            if abs then Buffer.add_char b '/';
            (b_segment_list[@tailcall]) ~sep:false b uri#path

    let b_query_fragment ~pfx b = function
        | None ->
            ()
        | Some text ->
            Buffer.add_char b pfx;
            (b_encode[@tailcall]) ~allow:allow_query_fragment b text

    let b_generic b uri =
        let Object uri = (uri :> generic container) in
        Buffer.add_string b uri#scheme;
        Buffer.add_char b ':';
        b_hierarchy ~abs:uri#absolute b uri;
        b_query_fragment ~pfx:'?' b uri#query;
        (b_query_fragment[@tailcall]) ~pfx:'#' b uri#fragment

    let b_relative b uri =
        let Object uri = (uri :> relative container) in
        b_hierarchy ~abs:uri#absolute b uri;
        b_query_fragment ~pfx:'?' b uri#query;
        (b_query_fragment[@tailcall]) ~pfx:'#' b uri#fragment

    let b_reference b = function
        | Generic uri -> (b_generic[@tailcall]) b uri
        | Relative uri -> (b_relative[@tailcall]) b uri
end

module Scan = struct
    module S = Cf_scan.ASCII
    module NS = Cf_number_scan.ASCII
    module LS = Cf_lex_scan.ASCII
    module DFA = LS.DFA

    open LS.Affix
    open S.Affix

    let x_hyphen = !:'-'
    let x_plus = !: '+'
    let x_dot = !:'.'
    let x_colon = !:':'
    let x_atsign = !:'@'
    let x_percent = !:'%'
    let x_slash = !:'/'
    let x_question = !:'?'

    let x_alpha = !^ch_is_alpha
    let x_digit = !^ch_is_digit
    let x_hexdigit = !^ch_is_hexdigit
    let x_unreserved = !^ch_is_unreserved
    let x_sub_delim = !^ch_is_sub_delim

    let x_pct_encoded = x_percent $& x_hexdigit $& x_hexdigit

    let x_scheme =
        x_alpha $& !*(x_alpha $| x_digit $| x_dot $| x_plus $| x_hyphen)

    let x_userinfo =
        !*(x_unreserved $| x_pct_encoded $| x_sub_delim $| x_colon)

    let x_reg_name = !*(x_unreserved $| x_pct_encoded $| x_sub_delim)

    let x_dec_octet_10_99 =
        !^(function '1'..'9' -> true | _ -> false) $& x_digit

    let x_dec_octet_100_199 = !:'1' $& x_digit $& x_digit

    let x_dec_octet_200_249 =
        !:'2' $& !^(function '0'..'4' -> true | _ -> false) $& x_digit

    let x_dec_octet_250_255 =
        !:'2' $& !:'5' $& !^(function '0'..'5' -> true | _ -> false)

    let x_dec_octet =
        x_digit $| x_dec_octet_10_99 $| x_dec_octet_100_199 $|
            x_dec_octet_200_249 $| x_dec_octet_250_255

    let x_ipv4_address =
        x_dec_octet $& x_dot $& x_dec_octet $& x_dot $& x_dec_octet $& x_dot $&
            x_dec_octet

    let x_h16 = x_hexdigit $& !?(x_hexdigit $& !?(x_hexdigit $& !?x_hexdigit))
    let x_ls32 = (x_h16 $& x_colon $& x_h16) $| x_ipv4_address

    let x_h16_colon = x_h16 $& x_colon

    let x_opt_h16_colon = !? x_h16_colon
    let x_rep_h16_colon n = DFA.seq ~a:n ~b:n x_h16_colon

    let x_h16_pre1 = !? x_h16
    let x_h16_pre2 = x_opt_h16_colon $& x_h16_pre1
    let x_h16_pre3 = x_opt_h16_colon $& x_h16_pre2
    let x_h16_pre4 = x_opt_h16_colon $& x_h16_pre3
    let x_h16_pre5 = x_opt_h16_colon $& x_h16_pre4
    let x_h16_pre6 = x_opt_h16_colon $& x_h16_pre5
    let x_h16_pre7 = x_opt_h16_colon $& x_h16_pre6

    let x_2colon = !:':' $& !:':'

    let x_ipv6_address_c1 =
        x_rep_h16_colon 6 $& x_ls32

    let x_ipv6_address_c2 =
        x_2colon $& x_rep_h16_colon 5 $& x_ls32

    let x_ipv6_address_c3 =
        x_h16_pre1 $& x_2colon $& x_rep_h16_colon 4 $& x_ls32

    let x_ipv6_address_c4 =
        x_h16_pre2 $& x_2colon $& x_rep_h16_colon 3 $& x_ls32

    let x_ipv6_address_c5 =
        x_h16_pre3 $& x_2colon $& x_rep_h16_colon 2 $& x_ls32

    let x_ipv6_address_c6 =
        x_h16_pre4 $& x_2colon $& x_h16_colon $& x_ls32

    let x_ipv6_address_c7 =
        x_h16_pre5 $& x_2colon $& x_ls32

    let x_ipv6_address_c8 =
        x_h16_pre6 $& x_2colon $& x_h16

    let x_ipv6_address_c9 =
        x_h16_pre7 $& x_2colon

    let x_ipv6_address = DFA.alts @@ Array.to_seq [|
        x_ipv6_address_c1;
        x_ipv6_address_c2;
        x_ipv6_address_c3;
        x_ipv6_address_c4;
        x_ipv6_address_c5;
        x_ipv6_address_c6;
        x_ipv6_address_c7;
        x_ipv6_address_c8;
        x_ipv6_address_c9;
    |]

    let x_zoneid = !+ (x_unreserved $| x_pct_encoded)

    let x_ip_future_address = !+(x_unreserved $| x_sub_delim $| x_colon)

    let x_pchar_no_colon =
        x_unreserved $| x_pct_encoded $| x_sub_delim $| x_atsign

    let x_pchar = x_pchar_no_colon $| x_colon

    let x_segment = !* x_pchar
    let x_segment_nz = !+ x_pchar
    let x_segment_nz_nc = !+ x_pchar_no_colon

    let x_query_fragment = !*(x_pchar $| x_slash $| x_question)

    let p_pct_encoded t =
        let* lexeme = LS.simple t in
        S.return @@ Buffer.contents @@ Buffer.of_seq @@ percent_decode @@
            String.to_seq lexeme

    let p_pct_encoded_lc t =
        let* lexeme = LS.simple t in
        S.return @@ Buffer.contents @@ Buffer.of_seq @@ percent_decode @@
            Seq.map Char.lowercase_ascii @@ String.to_seq lexeme

    let p_scheme = p_pct_encoded_lc x_scheme
    let p_userinfo = p_pct_encoded x_userinfo
    let p_reg_name = p_pct_encoded_lc x_reg_name

    let p_hostname =
        let* name = p_reg_name in
        S.return @@ H_name name

    let p_ipv4_address = LS.simple x_ipv4_address

    let p_ipv4_address_h =
        let* address = p_ipv4_address in
        S.return @@ H_ipv4 { address }

    let p_ipv6_address = LS.simple x_ipv6_address

    (** NOTE WELL:

        RFC 3986 says that syntax normalization of the "host" part is
        case-insensitive.

        RFC 4007 does *not* say that IPv6 address <zone_id> parts are
        case-insensitive, and RFC 6874 does not address the question of how to
        syntax-normalize the "zoneid" part of an IPv6 address with a zone
        identifier suffix.

        Accordingly, we do not normalize the zone identifier to lowercase as we
        do with all the rest of the components of the "host" part.
    *)
    let p_zoneid = p_pct_encoded x_zoneid

    let p_ipv6_address_z =
        let* address = p_ipv6_address in
        let* zoneid = ?/ (let* _ = S.lit "%25" () in p_zoneid) in
        S.return @@ H_ipv6 { address; zoneid }

    let p_int_of_hexdigits =
        NS.special @@ Cf_number_scan.(ctrl ~opt:[ O_radix hexadecimal ] R_int)

    let p_int_of_digits =
        NS.special @@ Cf_number_scan.(ctrl ~opt:[ O_radix decimal ] R_int)

    let p_ip_future_address = LS.simple x_ip_future_address

    let p_ip_future =
        let* _ = ?.'v' in
        let* version = p_int_of_hexdigits in
        let* _ = ?.'.' in
        let* address = p_ip_future_address in
        S.return @@ H_ip_future { version; address }

    let p_ip_literal =
        let* _ = ?.'[' in
        let* host = S.alt [ p_ip_future; p_ipv6_address_z ] in
        let* _ = ?.']' in
        S.return host

    (* NOTE WELL: RFC 3986 requires to match IPv4 Address before Host Name. *)
    let p_host = S.alt [ p_ip_literal; p_ipv4_address_h; p_hostname ]

    let p_authority =
        let* user =
            ?/ begin
                let* userinfo = p_userinfo in
                let* _ = ?.'@' in
                S.return userinfo
            end
        in
        let* host = p_host in
        let* port = ?/ (let* _ = ?.':' in p_int_of_digits) in
        S.return @@ Authority { user; host; port }

    let p_segment = p_pct_encoded x_segment
    let p_segment_nz = p_pct_encoded x_segment_nz
    let p_segment_nz_nc = p_pct_encoded x_segment_nz_nc

    let p_path_abempty =
        let* s = ?* (let* _ = ?.'/' in p_segment) in
        S.return (s <> [], s)

    let p_path_absolute =
        let* _ = ?.'/' in
        let* optpath =
            ?/ begin
                let* hd = p_segment_nz in
                let* _, tl = p_path_abempty in
                S.return (hd :: tl)
            end
        in
        let path = match optpath with None -> [] | Some path -> path in
        S.return (true, path)

    let p_path_noscheme =
        let* hd = p_segment_nz_nc in
        let* _, tl = p_path_abempty in
        S.return (false, hd :: tl)

    let p_path_rootless =
        let* hd = p_segment_nz in
        let* _, tl = p_path_abempty in
        S.return (false, hd :: tl)

    let p_path_empty = S.return (false, [])

    let p_authority_abempty =
        let* _ = ?.'/' in
        let* _ = ?.'/' in
        let* authority = p_authority in
        let* absolute, path = p_path_abempty in
        S.return (Some authority, absolute, path)

    let p_absolute_hier_aux =
        let* absolute, path =
            S.alt [
                p_path_absolute;
                p_path_rootless;
                p_path_empty;
            ]
        in
        S.return (None, absolute, path)

    let p_relative_hier_aux =
        let* absolute, path =
            S.alt [
                p_path_absolute;
                p_path_noscheme;
                p_path_empty;
            ]
        in
        S.return (None, absolute, path)

    let p_absolute_hier = S.alt [ p_authority_abempty; p_absolute_hier_aux ]
    let p_relative_hier = S.alt [ p_authority_abempty; p_relative_hier_aux ]

    let p_query_fragment = p_pct_encoded x_query_fragment

    let p_query = let* _ = ?.'?' in p_query_fragment
    let p_fragment = let* _ = ?.'#' in p_query_fragment

    let p_generic_container =
        let* scheme = p_scheme in
        let* _ = ?.':' in
        let* authority, absolute, path = p_absolute_hier in
        let* query = ?/ p_query in
        let* fragment = ?/ p_fragment in
        let path = resolve_dot_segments absolute path in
        let c =
            new generic ~scheme ?authority ~absolute ~path ?query ?fragment ()
        in
        S.return @@ Object c

    let p_relative_container =
        let* authority, absolute, path = p_relative_hier in
        let* query = ?/ p_query in
        let* fragment = ?/ p_fragment in
        let path = resolve_dot_segments absolute path in
        let c = new relative ?authority ~absolute ~path ?query ?fragment () in
        S.return @@ Object c

    let p_reference =
        let generic =
            let* uri = p_generic_container in
            S.return @@ Generic uri
        and relative =
            let* rel = p_relative_container in
            S.return @@ Relative rel
        in
        S.alt [ generic; relative ]
end

let scan_registry_name = Scan.p_reg_name
let scan_ipv4_address = Scan.p_ipv4_address
let scan_ipv6_address = Scan.p_ipv6_address
let scan_host = Scan.p_host
let scan_authority = Scan.p_authority
let scan_generic = Scan.p_generic_container
let scan_reference = Scan.p_reference

let emit_host = Cf_emit.F Emit.b_host
let emit_authority = Cf_emit.F Emit.b_authority
let emit_generic = Cf_emit.F Emit.b_generic
let emit_reference = Cf_emit.F Emit.b_reference

let of_string s =
    try Scan.S.of_string scan_generic s with
    | Not_found ->
        invalid_arg @@ Printf.sprintf "%s.of_string: <%s>" __MODULE__ s

let to_string uri =
    let b = Buffer.create 1 in
    Emit.b_generic b uri;
    Buffer.contents b

let merge_paths base rel =
    let Object base = (base :> absolute container) in
    let Object rel = (rel :> relative container) in
    match List.rev base#path with
    | [] ->
        assert (not true);
        if base#absolute then true, rel#path else false, (".." :: rel#path)
    | _ :: tl ->
        let absolute = base#absolute || rel#absolute in
        let path =
            resolve_dot_segments absolute @@ List.rev @@
                List.rev_append rel#path tl
        in
        absolute, path

let resolve_aux base' rel' =
    let Object base = (base' :> absolute container) in
    let Object rel = (rel' :> relative container) in
    let scheme = base#scheme and fragment = rel#fragment in
    match rel#authority with
    | Some authority ->
        let path = rel#path and query = rel#query in
        Object begin
            new generic ~scheme ~authority ~absolute:true ~path ?query
            ?fragment ()
        end
    | None ->
        let authority = base#authority in
        let absolute, path, query =
            if rel#absolute then
                true, rel#path, rel#query
            else begin
                if rel#path = [] then begin
                    let query =
                        match rel#query with
                        | None -> base#query
                        | q -> q
                    in
                    base#absolute, base#path, query
                end
                else begin
                    let absolute, path = merge_paths base' rel' in
                    absolute, path, rel#query
                end
            end
        in
        Object begin
            new generic ~scheme ?authority ~absolute ~path ?query ?fragment ()
        end

let resolve base = function
    | Generic gen -> gen
    | Relative rel -> resolve_aux base rel

(*--- End ---*)
