(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2021, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type t

exception Range_error
exception Label_error

external equal: t -> t -> bool = "cf_tai64_equal" [@@noalloc]
external compare: t -> t -> int = "cf_tai64_compare" [@@noalloc]
external now: unit -> t = "cf_tai64_now"
external epoch_: unit -> t = "cf_tai64_epoch"
external first_: unit -> t = "cf_tai64_first"
external last_: unit -> t = "cf_tai64_last"
external mjd_epoch_: unit -> t = "cf_tai64_mjd_epoch"

external set_current_offset_from_utc:
    int -> unit = "cf_tai64_set_current_offset_from_utc" [@@noalloc]

external to_unix_time: t -> float = "cf_tai64_to_unix_time"
external of_unix_time: float -> t = "cf_tai64_of_unix_time"
external to_label: t -> string = "cf_tai64_to_label"
external of_label: string -> t = "cf_tai64_of_label"
external add: t -> int -> t = "cf_tai64_add_int"
external add_int32: t -> int32 -> t = "cf_tai64_add_int32"
external add_int64: t -> int64 -> t = "cf_tai64_add_int64"
external sub: t -> t -> int64 = "cf_tai64_sub"

external init_: unit -> unit = "cf_tai64_init"
let _ = Callback.register_exception "Cf_tai64.Range_error" Range_error
let _ = Callback.register_exception "Cf_tai64.Label_error" Label_error
let _ = init_ ()

let epoch = epoch_ ()
let first = first_ ()
let last = last_ ()
let mjd_epoch = mjd_epoch_ ()

(*--- End ---*)
