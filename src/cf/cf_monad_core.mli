(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Core functions for monad operations. *)

(** {6 Overview} *)

(** This module defines the core functor modules and module types used in the
    [Cf_monad] interface. These definitions are used in [Cf_seq] to make the
    functor modules and module types that work with monad sequences, and those
    are in turn incorporated into the final [Cf_monad] interface.
*)

(** {6 Unary Monad} *)
module Unary: sig

    (** Define a module of type [Basis] to make a monad with a single type
        parameter for its return type.
    *)
    module type Basis = sig

        (** The abstract type of monad with a parameter for its return type. *)
        type +'r t

        (** Define [return a] to apply [a] to the bound function. *)
        val return: 'r -> 'r t

        (** Define [bind m f] to apply the value returned by [m] to the
            function [f] to obtain the result monad.
        *)
        val bind: 'a t -> ('a -> 'b t) -> 'b t

        (** Define [mapping] to select the `map` operator to be either the
            default [fun a -> return (f a)] or as some other special map
            function.
        *)
        val mapping: [
            | `Default
            | `Special of ('a t -> f:('a -> 'b) -> 'b t)
        ]

        (** Define [product] to select the `product` operator to be either
            [fun a b -> bind a (fun r1 -> bind b (fun r2 -> return (r1, r2)))]
            or some other special function.
        *)
        val product: [
            | `Default
            | `Special of ('a t -> 'b t -> ('a * 'b) t)
        ]
    end

    (** A module type that defines the affix monad operators. *)
    module type Affix = sig

        (** The abstract type of monad with a parameter for its return type. *)
        type +'r t

        (** Use [m >>: f] as a convenient way to express [map m ~f]. *)
        val ( >>: ): 'a t -> ('a -> 'b) -> 'b t

        (** Use [m >>= f] as a convenient way to express [bind m f]. *)
        val ( >>= ): 'a t -> ('a -> 'b t) -> 'b t

        (** The conventional binding operators. *)
        val ( let+ ): 'a t -> ('a -> 'b) -> 'b t
        val ( and+ ): 'a t -> 'b t -> ('a * 'b) t
        val ( let* ): 'a t -> ('a -> 'b t) -> 'b t
        val ( and* ): 'a t -> 'b t -> ('a * 'b) t
    end

    (** A module type that defines the core monad functions. *)
    module type Profile = sig

        (** The abstract type of monad with a parameter for its return type. *)
        type +'r t

        (** Use [return a] to apply the binding to [a]. *)
        val return: 'r -> 'r t

        (** Use [bind m f] to bind [f] to the value returned by [m]. *)
        val bind: 'a t -> ('a -> 'b t) -> 'b t

        (** Use [map m ~f] to return the result of applying [f] to the value
            returned by [m].
        *)
        val map: 'a t -> f:('a -> 'b) -> 'b t

        (** Use [product a b] to return the monoidal product of [a] and [b]. *)
        val product: 'a t -> 'b t -> ('a * 'b) t

        (** Open [Affix] to include the affix monad operators. *)
        module Affix: Affix with type 'r t := 'r t

        (** Use [disregard m] to ignore the value returned by [m] and apply the
            unit value to the bound function.
        *)
        val disregard: 'r t -> unit t

        (** Deprecated module alias. *)
        module Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
    end

    (** Use [Create(B)] to make the core monad functions for the basis
        module [B].
    *)
    module Create(B: Basis): Profile with type 'r t := 'r B.t

    (** Deprecated module type alias. *)
    module type Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
end

(** {6 Binary Monad} *)
module Binary: sig

    (** Define a module of type [Basis] to make a monad with two type
        parameters, a phantom type and a return type.
    *)
    module type Basis = sig

        (** The abstract type of monad with two parameters, a phantom type and
            a return type.
        *)
        type ('m, +'r) t

        (** Define [return a] to apply [a] to the bound function. *)
        val return: 'r -> ('m, 'r) t

        (** Define [bind m f] to apply the value returned by [m] to the
            function [f] to obtain the result monad.
        *)
        val bind: ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t

        (** Define [mapping] to define the `map` operator either as the default
            [fun a -> return (f a)] or as some other special map function.
        *)
        val mapping: [
            | `Default
            | `Special of (('m, 'a) t -> f:('a -> 'b) -> ('m, 'b) t)
        ]

        (** Define [product] to select the `product` operator to be either
            [fun a b -> bind a (fun r1 -> bind b (fun r2 -> return (r1, r2)))]
            or some other special function.
        *)
        val product: [
            | `Default
            | `Special of (('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t)
        ]
    end

    (** A module type that defines the affix monad operators. *)
    module type Affix = sig

        (** The abstract type of monad with two parameters, a phantom type and
            a return type.
        *)
        type ('m, +'r) t

        (** Use [m >>: f] as a convenient way to express [map m f]. *)
        val ( >>: ): ('m, 'a) t -> ('a -> 'b) -> ('m, 'b) t

        (** Use [m >>= f] as a convenient way to express [bind m f]. *)
        val ( >>= ): ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t

        (** The conventional binding operators. *)
        val ( let+ ): ('m, 'a) t -> ('a -> 'b) -> ('m, 'b) t
        val ( and+ ): ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t
        val ( let* ): ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t
        val ( and* ): ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t
    end

    (** A module type that defines the core monad functions. *)
    module type Profile = sig

        (** The abstract type of monad with two parameters, a phantom type and
            a return type.
        *)
        type ('m, +'r) t

        (** Use [return a] to apply the binding to [a]. *)
        val return: 'r -> ('m, 'r) t

        (** Use [bind m f] to bind [f] to the value returned by [m]. *)
        val bind: ('m, 'a) t -> ('a -> ('m, 'b) t) -> ('m, 'b) t

        (** Use [map m ~f] to return the result of applying [f] to the value
            returned by [m].
        *)
        val map: ('m, 'a) t -> f:('a -> 'b) -> ('m, 'b) t

        (** Use [product a b] to return the monoidal product of [a] and [b]. *)
        val product: ('m, 'a) t -> ('m, 'b) t -> ('m, 'a * 'b) t

        (** Open [Affix] to include the affix monad operators. *)
        module Affix: Affix with type ('m, 'r) t := ('m, 'r) t

        (** Use [disregard m] to ignore the value returned by [m] and apply the
            unit value to the bound function.
        *)
        val disregard: ('m, 'r) t -> ('m, unit) t

        (** Deprecated module alias. *)
        module Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
    end

    (** Use [Create(B)] to make the core monad functions for the basis
        module [B].
    *)
    module Create(B: Basis): Profile with type ('m, 'r) t := ('m, 'r) B.t

    (** Deprecated module type alias. *)
    module type Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
end

(** {6 Trinary Monad} *)
module Trinary: sig

    (** Define a module of type [Basis] to make a monad with three type
        parameters, two phantom types and a return type.
    *)
    module type Basis = sig

        (** The abstract type of monad with three parameters, two phantom types
            and a return type.
        *)
        type ('p, 'q, +'r) t

        (** Define [return a] to apply [a] to the bound function. *)
        val return: 'r -> ('p, 'q, 'r) t

        (** Define [bind m f] to apply the value returned by [m] to the
            function [f] to obtain the result monad.
        *)
        val bind: ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t

        (** Define [mapping] to define the `map` operator either as the default
            [fun a -> return (f a)] or as some other special map function.
        *)
        val mapping: [
            | `Default
            | `Special of
                (('p, 'q, 'a) t -> f:('a -> 'b) -> ('p, 'q, 'b) t)
        ]

        (** Define [product] to select the `product` operator to be either
            [fun a b -> bind a (fun r1 -> bind b (fun r2 -> return (r1, r2)))]
            or some other special function.
        *)
        val product: [
            | `Default
            | `Special of (('p, 'q, 'a) t -> ('p, 'q, 'b) t ->
                ('p, 'q, 'a * 'b) t)
        ]
    end

    (** A module type that defines the affix monad operators. *)
    module type Affix = sig

        (** The abstract type of monad with three parameters, two phantom types
            and a return type.
        *)
        type ('p, 'q, +'r) t

        (** Use [m >>: f] as a convenient way to express [map m f]. *)
        val ( >>: ): ('p, 'q, 'a) t -> ('a -> 'b) -> ('p, 'q, 'b) t

        (** Use [m >>= f] as a convenient way to express [bind m f]. *)
        val ( >>= ): ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t

        (** The conventional binding operators. *)
        val ( let+ ): ('p, 'q, 'a) t -> ('a -> 'b) -> ('p, 'q, 'b) t
        val ( let* ):
            ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t
        val ( and+ ): ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t
        val ( and* ): ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t
    end

    (** A module type that defines the core monad functions. *)
    module type Profile = sig

        (** The abstract type of monad with three parameters, two phantom types
            and a return type.
        *)
        type ('p, 'q, +'r) t

        (** Use [return a] to apply the binding to [a]. *)
        val return: 'r -> ('p, 'q, 'r) t

        (** Use [bind m f] to bind [f] to the value returned by [m]. *)
        val bind: ('p, 'q, 'a) t -> ('a -> ('p, 'q, 'b) t) -> ('p, 'q, 'b) t

        (** Use [map m ~f] to return the result of applying [f] to the value
            returned by [m].
        *)
        val map: ('p, 'q, 'a) t -> f:('a -> 'b) -> ('p, 'q, 'b) t

        (** Use [product a b] to return the monoidal product of [a] and [b]. *)
        val product: ('p, 'q, 'a) t -> ('p, 'q, 'b) t -> ('p, 'q, 'a * 'b) t

        (** Open [Affix] to include the affix monad operators. *)
        module Affix: Affix with type ('p, 'q, 'r) t := ('p, 'q, 'r) t

        (** Use [disregard m] to ignore the value returned by [m] and apply the
            unit value to the bound function.
        *)
        val disregard: ('p, 'q, 'r) t -> ('p, 'q, unit) t

        (** Deprecated module alias. *)
        module Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
    end

    (** Use [Create(B)] to make the core monad functions for the basis
        module [B].
    *)
    module Create(B: Basis): Profile
        with type ('p, 'q, 'r) t := ('p, 'q, 'r) B.t

    (** Deprecated module type alias. *)
    module type Infix = Affix [@@caml.alert deprecated "Use Affix instead."]
end

(*--- End ---*)
