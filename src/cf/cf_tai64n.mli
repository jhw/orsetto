(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2018, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Computations with the Temps Atomique International (TAI) timescale. *)

(** {6 Overview}

    This module (and its cognate {!Cf_tai64}) defines an abstract type and
    associated functions for computations with values representing epochs in
    the Temps Atomique International (TAI) timescale.  Values are represented
    internally with the TAI64 format defined by Dan Bernstein, and support
    precision to the nearest nanosecond.

    Functions are provided that:
    - acquire the current time in TAI64N format.
    - compare, add and subtract values.
    - convert between TAI64N values and a portable external format called
      the "TAI64N label", which is essentially an array of twelve octets.
    - convert between TAI64N values and the float values returned by the
      [Unix.gettimeofday] function.

    Constants are also provided that define the boundaries of valid TAI64N
    representations.

    {b Warning:} This implementation obtains the current time of day using the
    POSIX [gettimeofday()] function, which returns a value based on the UTC
    timescale (but with leap seconds "elided" in a way that makes conversions
    between POSIX time, Standard Time and TAI a perilous undertaking).  See the
    {!Cf_stdtime} module for detail
*)

(** {6 Types} *)

(** Abstract values of TAI64N type *)
type t

(** {6 Functions} *)

(** Equivalence and total order relations *)
include Cf_relations.Std with type t := t

(** Returns the current time in TAI64N, obtained by reading the current time
    from the POSIX [gettimeofday()] function, and adjusting for leap
    seconds.  (Currently, the leap seconds table is hardcoded into the library,
    and the most recent leap second announcement was for Dec 31, 1998.)
*)
val now: unit -> t

(** The earliest TAI epoch representable in the TAI64N format.  The TAI64N
    label is [00000000 00000000 00000000].
*)
val first: t

(** The latest TAI epoch representable in the TAI64N format.  The TAI64N label
    is [7fffffff ffffffff 3b9ac9ff].
*)
val last: t

(** Use [compose s ns] to compose a TAI64N value from a TAI64 value [s] and an
    offset of [ns] nanoseconds.  Raises [Invalid_argument] if the number of
    nanoseconds is not less than 10{^12}.
*)
val compose: Cf_tai64.t -> int -> t

(** Use [decompose x] to separate the TAI64N value [x] into a TAI64 value and
    an offset in nanoseconds.
*)
val decompose: t -> Cf_tai64.t * int

(** Converts a TAI64 value to a value consistent with the result of calling the
    [Unix.gettimeofday] function.
*)
val to_unix_time: t -> float

(** Converts a value consistent with the result of calling the
    [Unix.gettimeofday] function into a TAI64N value.
*)
val of_unix_time: float -> t

(** Returns a string of 12 octets containing the TAI64N label corresponding to
    the TAI64N value of its argument.
*)
val to_label: t -> string

(** Interprets the argument as a TAI64N label and returns the corresponding
    TAI64N value.  Raises [Cf_tai64.Label_error] if the label is not a valid
    TAI64N label.
*)
val of_label: string -> t

(** Add seconds to a TAI64N value.  Raises [Oni_cf_tai64.Range_error] if the
    result is not a valid TAI64N value.
*)
val add: t -> float -> t

(** Subtract one TAI64N value from another.  [sub t0 t1] returns the number of
    seconds before [t0] that [t1] denotes.
*)
val sub: t -> t -> float

(*--- End ---*)
