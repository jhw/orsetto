(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Position annotation systems. *)

(** {6 Overview}

    This module defines an interface for composing systems that annotate
    scanned or decoded values with the span of positions in the input stream
    where the represented value was found. Specializations of position
    information are provided for streams analyzed with {Cf_decode} as well as
    streams comprising lines of text.

    A distinguished module is provided for annotating streams comprising texts
    encoded in 8-bit ASCII.
*)

(** {6 Interface} *)

(** Submodule comprising types for structured interchange of position data. *)
module Meta: sig

    (** Style for annotation data structures, either {i `Concise}, i.e. fields
        identified by array index, or {i `Diagnostic}, i.e. fields presented
        as pairs comprising a field name string and the field value.
    *)
    type style = [ `Concise | `Diagnostic ]

    (** Use [or_concise] with an optional style argument to select the
        {`Concise} style as the default.
    *)
    val or_concise: [< style ] option -> style

    (** Use [or_diagnostic] with an optional style argument to select the
        {`Diagnostic} style as the default.
    *)
    val or_diagnostic: [< style ] option -> style

    (** Parameters for configuring structured interchange of annotations. *)
    module type Basis = sig

        (** The basis symbol type. *)
        type symbol

        (** The position type. *)
        type position

        (** Relevant portions of position types are identified by this type. *)
        type fields

        (** If a list of fields is not provided to the meta formatter, then
            this [default_fields] is used.
        *)
        val default_fields: fields list

        (** The witness for the symbol type. *)
        val symbol_type: symbol Cf_type.nym

        (** Annotation profiles use [of_opaque_position v] to translate [v]
            into a position value provided it was decoded from an interchange
            language representation produced by [to_opaque_position] above.

            The [~style] parameter controls whether and how record and variant
            names should be decoded from the opaque value.

            The [~fields] parameters facilitates controlling which portions of
            the position record are required to be present in the decoded
            value. Default values for fields not required should be used if not
            present in the decoded value.

            Raises [Cf_type.Type_error] if the type witnessed by [v] is wrong
            according to style and fields required. Raises [Failure] if the
            witness is correct but the underlying value is unsound.
        *)
        val of_opaque_position:
            ?style:[< style ] -> ?fields:fields list -> Cf_type.opaque ->
            position

        (** Annotation profiles use [to_opaque_position p] to translate [p]
            into an opaque value ready to encode in an interchange language.

            The [~style] parameter controls whether and how record and variant
            names are encoded in the opaque value.

            The [~fields] parameter facilitates controlling which portions of
            the position record are to be included in the serialized octets.
        *)
        val to_opaque_position:
            ?style:[< style ] -> ?fields:fields list -> position ->
            Cf_type.opaque
    end
end

(** The basis signature for creating an annotation system. *)
module type Basis = sig

    (** The total order of positions. *)
    module Position: Cf_relations.Order

    (** The equivalence relation on symbols. *)
    module Symbol: Cf_relations.Equal

    (** The specification of structural metadata. *)
    module Meta: Meta.Basis
        with type position := Position.t
         and type symbol := Symbol.t

    (** The default initial position. *)
    val default_initial_position: Position.t

    (** Scanners use [advance pos sym] to make the position immediately after
        [pos] when occupied by [sym].
    *)
    val advance: Position.t -> Symbol.t -> Position.t
end

(** The signture of an annotation system specialized on a basis module. *)
module type Profile = sig

    (** The basis symbol type. *)
    type symbol

    (** The basis position type. *)
    type position

    (** The type of an input event, comprising a symbol and its position. *)
    type iota = private Iota of { symbol: symbol; position: position }

    (** Representation of a span of positions, comprising the pair of a) the
        position of the first symbol in the span, and b) the position of the
        symbol immediately following the last symbol in the span. Accordingly,
        if both the {i start} and {i limit} fields have the same serial number,
        then the span is empty, comprising no symbols.
    *)
    type span = private Span of { start: position; limit: position }

    (** A value annotated with its span of text in the input stream. *)
    type +'a form = private Form of { value: 'a; span: span option }

    (** A basis module for constructing a {Cf_scan} scanner. *)
    module Scan_basis: Cf_scan.Basis
       with type Symbol.t = symbol
        and type iota = iota
        and type position = position
        and type 'a Form.t = 'a form

    (** Inclusion of the {Cf_scan} production form signature. *)
    include Cf_scan.Form with type 'a t := 'a form

    (** Use [up v] to lift [v] into a form. Use the optional [~span] argument
        to specify an explicit span of locations. Otherwise, this function is
        equivalent to [imp v].
    *)
    val up: ?span:span -> 'a -> 'a form

    (** Use [map f v] to apply [f] to the value enclosed in [v] to make the
        result annotated with the same position as [v].
    *)
    val map: ('a -> 'b) -> 'a form -> 'b form

    (** Use [join f a b] to apply [f] to the values enclosed in [a] and [b] to
        make the result annotated with the span that covers the overlap of the
        positions of both [a] and [b].
    *)
    val join: ('a -> 'b -> 'c) -> 'a form -> 'b form -> 'c form

    (** Use [collect s] to collect all the values enclosed in the forms [s] to
        a single list annotated with the span that overlaps all the positions
        in [s].
    *)
    val collect: 'a form list -> 'a list form

    (** Use [force z] to force the lazy value enclosed in [z]. *)
    val force: 'a Lazy.t form -> 'a form

    (** Use [to_iotas s] to lift [s] into a sequence of {i iota} values. Use
        [~start] to provide an explicit starting position.
    *)
    val to_iotas: ?start:position -> symbol Seq.t -> iota Seq.t

    (** A submodule to facilitate structural interchange of metadata. *)
    module Meta: sig

        (** Relevant portions of a position indicator. *)
        type fields

        (** The [~style] and [~fields] parameters for all the functions below
            are used as described in {Meta} to control what fields are encoded
            in a position and how they are to be decoded.

            All the [of_opaque_xxxxx] function raise [Cf_type.Type_error] if
            the type of [v] is not witnessed correctly for the style and fields
            specified. Otherwise, raises [Failure] if the encapsulated value is
            witnessed by the correct type, but the encoded value is not a valid
            representation of a position.
        *)

        (** Use [of_opaque_iota f v] to translate [v] into an {i iota} value
            provided it was decoded from an interchange language representation
            of it produced by [to_opaque_iota] below. Applies [f] to the symbol
            encapsulated in [v] to compose the result.
        *)
        val of_opaque_iota:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (Cf_type.opaque -> symbol) -> Cf_type.opaque -> iota

        (** Use [of_opaque_span v] to translate [v] into a {i span} value
            provided it was decoded from an interchange language representation
            of it produced by [to_opaque_span] below.
        *)
        val of_opaque_span:
            ?style:[< Meta.style ] -> ?fields:fields list -> Cf_type.opaque ->
            span

        (** Use [of_opaque_form f v] to translate [v] into a {i form} value
            provided it was decoded from an interchange language representation
            of it produced by [to_opaque_form] below. The value encapsulated in
            the form is unpacked by applying [v].
        *)
        val of_opaque_form:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (Cf_type.opaque -> 'a) -> Cf_type.opaque -> 'a form

        (** Use [to_opaque_iota f i] to translate [i] into an opaque value ready
            to encode in an interchange language. The symbol encapsulated by in
            the iota is packed by applying [f].
        *)
        val to_opaque_iota:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            (symbol -> Cf_type.opaque) -> iota -> Cf_type.opaque

        (** Use [to_opaque_span s] to translate [s] into an opaque value ready
            to encode in an interchange language.
        *)
        val to_opaque_span:
            ?style:[< Meta.style ] -> ?fields:fields list -> span ->
            Cf_type.opaque

        (** Use [to_opaque_form f loc] to translate [loc] into an opaque value
            ready to encode in an interchange language.  The value encapsulated
            in the form is packed by applying [f].
        *)
        val to_opaque_form:
            ?style:[< Meta.style ] -> ?fields:fields list ->
            ('a -> Cf_type.opaque) -> 'a form -> Cf_type.opaque
    end
end

(** Use [Create(B)] to make an annotation system module for the symbols defined
    in [B].
*)
module Create(B: Basis): Profile
   with type symbol := B.Symbol.t
    and type position := B.Position.t
    and type Meta.fields := B.Meta.fields

(** A submodule containing specializations for use with {Cf_decode}. *)
module Coded: sig

    (** The basis signature for {Cf_decode} specializations. *)
    module type Basis = sig

        (** The equivalence class of decoded symbols. *)
        module Symbol: Cf_relations.Equal

        (** The witness for the symbol type. *)
        val symbol_type: Symbol.t Cf_type.nym

        (** Scanners use [advance pos sym] to make the position immediately
            after [pos] when occupied by [sym].
        *)
        val advance: Cf_decode.position -> Symbol.t -> Cf_decode.position
    end

    (** The signature of annotation systems specialized for {Cf_decode}. *)
    module type Profile = Profile with type position := Cf_decode.position

    (** Use [Create(B)] to make an annotation system module for the symbols
        defined in [B] and decoded with {Cf_decode}.
    *)
    module Create(B: Basis): Profile with type symbol := B.Symbol.t
end

(** A submodule containing specializations for textual representations. *)
module Textual: sig

    (** The signature of a serial number type, c.f. RFC 1982. *)
    module Serial: sig

        (** Serial numbers are not technically totally ordered. Some
            comparisons of serial numbers at excessively large distances can
            raise [Failure].
        *)
        include Cf_relations.Std

        (** The disintiguished initial serial number. *)
        val zero: t

        (** Annotation systems use [succ n] to make the successor of [n]. *)
        val succ: t -> t
    end

    (** Position of the start of a line of text. *)
    type line = private Line of {
        number: int;
        stream: string;
        octets: int64;
        crpred: bool;
    }

    (** Position of a coded symbol. *)
    type position = private Position of {
        serial: Serial.t;
        line: line;
        column: int;
        lnadj: int64;
    }

    (** Use [initial_position s] to obtain initial position in stream [s]. *)
    val initial_position: string -> position

    (** The basis signature of text annotation systems. *)
    module type Basis = sig

        (** The equivalence class of text symbols. *)
        module Symbol: Cf_relations.Equal

        (** The witness for the symbol type. *)
        val symbol_type: Symbol.t Cf_type.nym

        (** Scanners use [advance pos sym] to make the position immediately
            after [pos] when occupied by [sym].
        *)
        val advance: position -> Symbol.t -> position
    end

    (** The signature of annotation systems specialized for text. *)
    module type Profile = sig

        (** Include the common signature specialized for textual positions.

            The following table explains the field categories for structural
            metadata interchange:

            - {`Serial}: the [.serial] field.
            - {`Stream}: the [.stream] field.
            - {`Offsets}: the [.octets] and [.lnadj] fields.
            - {`Lines}: the [.number] and [.column] fields.
        *)
        include Profile
            with type position := position
             and type Meta.fields = [ `Serial | `Stream | `Offsets | `Lines ]

        (** Use [emit_form pp vl] to print the span of [vl] with [pp] in a
            mostly readable format.
        *)
        val emit_form: Format.formatter -> 'a form -> unit
    end

    (** Use [Create(B)] to make an annotation system module for texts with
        symbols defined in [B].
    *)
    module Create(B: Basis): Profile with type symbol := B.Symbol.t

    (** A distinguished annotation system for 8-bit ASCII text. *)
    module ASCII: Profile with type symbol := char

    (** A submodule defining a functor for creating annotations systems for
        Unicode texts with an abstraction of the transport form.
    *)
    module Unicode: sig

        (** The signature of a basis module. *)
        module type Basis = sig

            (** The annotation system uses [size_of_uchar uc] to get the number
                of octets required by the transport form to encode [uc].
            *)
            val size_of_uchar: Uchar.t -> int

            (** The annotation system uses [is_grapheme_base uc] to test
                whether [uc] is a grapheme base code, and introduces a new
                column in the text.
            *)
            val is_grapheme_base: Uchar.t -> bool
        end

        (** The signature of a Unicode text annotation system. *)
        module type Profile = Profile with type symbol := Uchar.t

        (** Use [Create(B)] to create an annotation system for Unicode texts
            encoded in the transport form [T].
        *)
        module Create(B: Basis): Profile
    end
end

(*--- End ---*)
