(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Slices of basic vector types, i.e. arrays, byte sequences and string. *)

(** {6 Preamble} *)
open Bigarray

(** {6 Interface} *)

(** Use [of_array1 a] to make a slice that references all of [a]. *)
val of_array1: ('a, 'b, 'c) Array1.t -> ('a, 'b, 'c) Array1.t Cf_slice.t

(** Use [of_subarray1 a start limit] to make a slice that references the part
    of [a] that starts at index [start] and is limited at index [limit].
*)
val of_subarray1:
    ('a, 'b, 'c) Array1.t -> int -> int -> ('a, 'b, 'c) Array1.t Cf_slice.t

(** Use [to_array1 a] to make a new array from the elements in [a]. *)
val to_array1: ('a, 'b, 'c) Array1.t Cf_slice.t -> ('a, 'b, 'c) Array1.t

(*--- End ---*)
