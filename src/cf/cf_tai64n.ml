(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2018, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type t

external equal: t -> t -> bool = "cf_tai64n_compare"
external compare: t -> t -> int = "cf_tai64n_compare"
external now: unit -> t = "cf_tai64n_now"
external first_: unit -> t = "cf_tai64n_first"
external last_: unit -> t = "cf_tai64n_last"
external compose: Cf_tai64.t -> int -> t = "cf_tai64n_compose"
external decompose: t -> Cf_tai64.t * int = "cf_tai64n_decompose"
external to_unix_time: t -> float = "cf_tai64n_to_unix_time"
external of_unix_time: float -> t = "cf_tai64n_of_unix_time"
external to_label: t -> string = "cf_tai64n_to_label"
external of_label: string -> t = "cf_tai64n_of_label"
external add: t -> float -> t = "cf_tai64n_add"
external sub: t -> t -> float = "cf_tai64n_sub"

;;
external init_: unit -> unit = "cf_tai64_init";;
init_ ();;

let first = first_ ()
let last = last_ ()

(*--- End ---*)
