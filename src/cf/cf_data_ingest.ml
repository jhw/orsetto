(*---------------------------------------------------------------------------*
  Copyright (C) 2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let invalid fn detail =
    invalid_arg @@ Printf.sprintf "%s.%s: %s" __MODULE__ fn detail

type 'k pair = [
    | `Table of 'k Cf_type.nym
    | `Structure of 'k Cf_type.nym
    | `Variant of 'k Cf_type.nym
]

type 'k container = [ `Vector | `Record | 'k pair ]

type occurs = Occurs of { a: int option; b: int option }

type element

type (_, _) control = ..
type (_, _) control += Cast: ('a -> 'b) -> ('a, 'b) control

type ('s, 'p) entry =
    | Required: ('s, 's) entry
    | Optional: ('s, 's option) entry
    | Default: 's -> ('s, 's) entry

type 'v model =
    | Primitive: 'v Cf_type.nym -> 'v model
    | Control: 'a model * ('a, 'b) control -> 'b model
    | Defer: 'v model Lazy.t -> 'v model
    | Choice: 'v model array -> 'v model
    | Vector: occurs * 'v model -> 'v array model
    | Table: occurs * 'k indexed * 'v model -> ('k * 'v) array model
    | Record: occurs * (element, 'f, 'v) group * 'f -> 'v model
    | Structure:
        occurs * 'k indexed * ('k indexed, 'f, 'v) group * 'f -> 'v model
    | Variant: 'k indexed * ('k * 'v model) array -> 'v model

and ('g, 'f, 'v) group =
    | Return: ('g, 'v, 'v) group
    | Bind: ('g, 'p) bind * ('g, 'f, 'v) group -> ('g, 'p -> 'f, 'v) group

and ('g, 'p) bind =
    | Sequential: ('s, 'p) entry * 's model -> (element, 'p) bind
    | Indexed: ('s, 'p) entry * 'k * 's model -> ('k indexed, 'p) bind

and 'k indexed = <
    'k Cf_relations.Extensible.order;
    nym: 'k Cf_type.nym;
    model: 'k model;
>

module Occurs = struct
    let one = let v = Some 1 in Occurs { a = v; b = v }
    let any = Occurs { a = None; b = None }

    let validate ?a ?b fn =
        match a, b with
        | None, None -> any
        | Some a, _ when a < 0 -> invalid fn "a < 0"
        | _, Some b when b < 0 -> invalid fn "b < 0"
        | Some a, Some b when a > b -> invalid fn "a > b"
        | Some 0, b -> Occurs { a = None; b }
        | a, b -> Occurs { a; b }

    let of_group =
        let count: type s p. int -> (s, p) entry -> int = fun n entry ->
            match entry with
            | Required -> succ n
            | (Optional | Default _) -> n
        in
        let rec loop:
            type k f r. int -> int -> (k, f, r) group -> occurs =
            fun a b group ->
                match group with
                | Return ->
                    let a' = if a > 0 then Some a else None in
                    let b' = if a = b && b > 0 then a' else Some b in
                    Occurs { a = a'; b = b' }
                | Bind (bind, group) ->
                    let b = succ b in
                    let a =
                        match bind with
                        | Sequential (entry, _) -> count a entry
                        | Indexed (entry, _, _) -> count a entry
                    in
                    (loop[@tailcall]) a b group
        in
        let enter group = loop 0 0 group in
        enter
end

let occurs ?a ?b () = Occurs.validate ?a ?b "occurs"

let nil = Return
let bind b g = Bind (b, g)

class ['key] index ?cmp ?model nym = object(_:'self)
    inherit ['key] Cf_relations.Extensible.order as order
    constraint 'self = < 'key indexed; .. >

    val nym_: 'key Cf_type.nym = nym

    val model_ =
        match model with None -> Primitive nym | Some model -> model

    method nym = nym_
    method model = model_

    method! compare a b =
        match cmp with
        | None -> order#compare a b
        | Some f -> f a b
end

module Element = struct
    let required model = Sequential (Required, model)
    let optional model = Sequential (Optional, model)
    let default model dvalue = Sequential (Default dvalue, model)
end

module Field = struct
    let required key model = Indexed (Required, key, model)
    let optional key model = Indexed (Optional, key, model)
    let default key model dvalue = Indexed (Default dvalue, key, model)
end

let primitive nym = Primitive nym

let control m c = Control (m, c)
let cast f m = control m @@ Cast f
let defer m = Defer m

let choice models =
    let n = Array.length models in
    if n < 1 then invalid "choice" "Array.length < 1";
    if n = 1 then Array.unsafe_get models 0 else Choice (Array.copy models)

let vector ?a ?b model =
    let occurs = Occurs.validate ?a ?b "vector" in
    Vector (occurs, model)

let table ?a ?b index model =
    let occurs = Occurs.validate ?a ?b "table" in
    let index = (index :> 'key index) in
    Table (occurs, index, model)

let record group compose =
    let occurs = Occurs.of_group group in
    Record (occurs, group, compose)

let structure index group compose =
    let occurs = Occurs.of_group group in
    let index = (index :> 'key index) in
    Structure (occurs, index, group, compose)

let variant index cases =
    let index = (index :> 'key index) in
    Variant (index, cases)

module Affix = struct
    let ( !: ) = Element.required
    let ( !? ) = Element.optional
    let ( $= ) = Element.default

    let ( %: ) = Field.required
    let ( %? ) = Field.optional
    let ( %= ) k (m, v) = Field.default k m v

    let ( /= ) m f = cast f m
    let ( @> ) = bind
end

module type Basis = sig
    type symbol
    type position
    type 'k frame

    module Form: Cf_scan.Form

    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a Form.t

    val primitive: 'a Cf_type.nym -> 'a Form.t Scan.t
    val control: 'a Form.t Scan.t -> ('a, 'b) control -> 'b Form.t Scan.t
    val start: 'k container -> occurs -> 'k frame Form.t Scan.t
    val visit: 'k container -> 'k frame -> ('r -> 'r Scan.t) -> 'r -> 'r Scan.t
    val finish: 'k frame -> unit Form.t Scan.t

    val pair:
        'k pair -> 'k Form.t Scan.t -> 'v Form.t Scan.t ->
        ('k * 'v) Form.t Scan.t

    val memo: Scan.mark Form.t Scan.t
end

module type Profile = sig
    module Basis: Basis
    module Form: module type of Basis.Form
    module Scan: module type of Basis.Scan

    val scan: 'a model -> 'a Form.t Scan.t
end

module Create(B: Basis) = struct
    module Form = B.Form
    module Scan = B.Scan

    module Aux = struct
        type ('g, 'f, 'v) group =
            | Return:
                ('g, 'v, 'v) group
            | Bind:
                ('g, 'p) bind * ('g, 'f, 'v) group -> ('g, 'p -> 'f, 'v) group

        and ('g, 'p) bind =
            | Sequential:
                ('s, 'p) entry * 's Form.t Scan.t Lazy.t -> (element, 'p) bind
            | Indexed:
                ('s, 'p) entry * 'k * 's Form.t Scan.t Lazy.t ->
                ('k indexed, 'p) bind
    end

    open Scan.Affix

    let p_finish p0l p1l =
        let+ p2l =
            B.finish @@ Form.dn p0l |>
                Scan.or_fail "expected end of data group"
        in
        Form.(span p0l (span p1l p2l ()) ())

    let rec p_model: type v. v model -> v Form.t Scan.t = fun model ->
        match model with
        | Primitive nym ->
            B.primitive nym
        | Control (model, control) ->
            B.control (p_model model) control
        | Defer model ->
            p_model @@ Lazy.force model
        | Choice models ->
            B.Scan.altz @@ Cf_seq.persist @@ Seq.map p_model @@
                Array.to_seq models
        | Vector (occurs, model) ->
            p_vector occurs model
        | Table (occurs, index, model) ->
            p_table occurs index model
        | Record (occurs, compose, group) ->
            p_record occurs compose group
        | Structure (occurs, index, compose, group) ->
            p_structure occurs index compose group
        | Variant (index, cases) ->
            p_variant index cases

    and p_vector:
        type v. occurs -> v model -> v array Form.t Scan.t
        = fun occurs model ->
            let vscan = lazy (p_model model) in
            let each (_, vlsr) =
                let+ vl = Lazy.force vscan in
                Form.mv () vl, vl :: vlsr
            in
            let* p0l = B.start `Vector occurs in
            let init = Form.mv () p0l, [] in
            let* p1l, vlsr = B.visit `Vector (Form.dn p0l) each init in
            let+ finl = p_finish p0l p1l in
            Form.mv (Array.of_list @@ List.rev_map Form.dn vlsr) finl

    and p_table:
        type k v. occurs -> k index -> v model -> (k * v) array Form.t Scan.t
        = fun occurs index model ->
            let module Order =
                (val Cf_relations.Extensible.ordering index :
                Cf_relations.Order with type t = k)
            in
            let module Set = Cf_rbtree.Set.Create(Order) in
            let tag = `Table index#nym in
            let kvscan =
                lazy (B.pair tag (p_model index#model) (p_model model))
            in
            let each (_tail, keys, pairs) =
                let* kvl = Lazy.force kvscan in
                let k, _ as kv = Form.dn kvl in
                if Set.member k keys then
                    Scan.fail "duplicate key in table"
                else begin
                    let tail = Form.mv () kvl in
                    let keys = Set.put k keys in
                    let pairs = kv :: pairs in
                    Scan.return (tail, keys, pairs)
                end
            in
            let* p0l = B.start tag occurs in
            let init = Form.mv () p0l, Set.nil, [] in
            let* p1l, _keys, pairs = B.visit tag (Form.dn p0l) each init in
            let+ finl = p_finish p0l p1l in
            Form.mv (Array.of_list @@ List.rev pairs) finl

    and p_record:
        type f v. occurs -> (element, f, v) group -> f -> v Form.t Scan.t
        = fun occurs group compose ->
            let rec mapg:
                type f v. (element, f, v) group -> (element, f, v) Aux.group
                = fun g ->
                    match g with
                    | Return ->
                        Aux.Return
                    | Bind (b, g) ->
                        let Sequential (e, m) = b in
                        let bind = Aux.Sequential (e, lazy (p_model m)) in
                        Aux.Bind (bind, mapg g)
            in
            let group = mapg group in
            let each markls =
                let+ markl = B.memo in
                markl :: markls
            in
            let* p0l = B.start `Record occurs in
            let* markls = B.visit `Record (Form.dn p0l) each [] in
            let p1l =
                match markls with
                | [] -> Form.mv () p0l
                | hd :: _ -> Form.mv () hd
            in
            let* finl = p_finish p0l p1l in
            let* save = Scan.cur in
            let marks = List.rev_map Form.dn markls in
            let* ret = p_element_group marks group compose in
            let+ _ = Scan.mov save in
            Form.mv ret finl

    and p_element_group:
        type f v. Scan.mark list -> (element, f, v) Aux.group -> f -> v Scan.t
        = fun marks group compose ->
            match group with
            | Aux.Return ->
                Scan.return compose
            | Aux.Bind (bind, group) ->
                let Aux.Sequential (entry, scan) = bind in
                match entry with
                | Required ->
                    if marks = [] then
                        Scan.fail "required element not present"
                    else begin
                        let* _ = Scan.mov @@ List.hd marks in
                        let* vl =
                            Scan.or_fail "element not recognized" @@
                            Lazy.force scan
                        in
                        let marks = List.tl marks in
                        let compose = compose @@ Form.dn vl in
                        p_element_group marks group compose
                    end
                | Optional ->
                    if marks = [] then
                        p_element_group [] group @@ compose None
                    else begin
                        let* _ = Scan.mov @@ List.hd marks in
                        let* vlopt = Scan.opt @@ Lazy.force scan in
                        match vlopt with
                        | None ->
                            p_element_group marks group @@ compose None
                        | Some vl ->
                            let marks = List.tl marks in
                            let compose = compose @@ Some (Form.dn vl) in
                            p_element_group marks group compose
                    end
                | Default dvalue ->
                    if marks = [] then
                        p_element_group [] group @@ compose dvalue
                    else begin
                        let* _ = Scan.mov @@ List.hd marks in
                        let* vlopt = Scan.opt @@ Lazy.force scan in
                        match vlopt with
                        | None ->
                            p_element_group marks group @@ compose dvalue
                        | Some vl ->
                            let marks = List.tl marks in
                            let compose = compose @@ Form.dn vl in
                            p_element_group marks group compose
                    end

    and p_structure:
        type k f v. occurs -> k index -> (k index, f, v) group -> f ->
            v Form.t Scan.t
        = fun occurs index group compose ->
            let rec mapg:
                type k f v. (k index, f, v) group -> (k index, f, v) Aux.group
                = fun g ->
                    match g with
                    | Return ->
                        Aux.Return
                    | Bind (b, g) ->
                        let Indexed (e, k, m) = b in
                        let scan =
                            lazy begin
                                Scan.or_fail "field value unrecognized" @@
                                p_model m
                            end
                        in
                        let bind = Aux.Indexed (e, k, scan) in
                        Aux.Bind (bind, mapg g)
            in
            let group = mapg group in
            let module Order =
                (val Cf_relations.Extensible.ordering index :
                Cf_relations.Order with type t = k)
            in
            let module Map = Cf_rbtree.Map.Create(Order) in
            let tag = `Structure index#nym and kscan = p_model index#model in
            let kvscan = B.pair tag kscan B.memo in
            let each (_tail, fields) =
                let* kvl = kvscan in
                let k, _ as kv = Form.dn kvl in
                if Map.member k fields then
                    Scan.fail "duplicate key in structure"
                else begin
                    let tail = Form.mv () kvl in
                    let fields = Map.replace kv fields in
                    Scan.return (tail, fields)
                end
            in
            let* _ = Scan.return () in
            let* p0l = B.start tag occurs in
            let init = Form.mv () p0l, Map.nil in
            let* p1l, fields = B.visit tag (Form.dn p0l) each init in
            let* finl = p_finish p0l p1l in
            let* save = Scan.cur in
            let* ret = p_indexed_group Map.extract fields group compose in
            let+ _ = Scan.mov save in
            Form.mv ret finl

    and p_indexed_group:
        type m k f v. (k -> m -> Scan.mark * m) -> m ->
            (k index, f, v) Aux.group -> f -> v Scan.t
        = fun extract fields group compose ->
            match group with
            | Aux.Return ->
                Scan.return compose
            | Aux.Bind (bind, group) ->
                let Aux.Indexed (entry, key, scan) = bind in
                match entry with
                | Required ->
                    begin
                        match extract key fields with
                        | exception Not_found ->
                            Scan.fail "required field not present"
                        | mark, fields ->
                            let* _ = Scan.mov mark in
                            let* vl = Lazy.force scan in
                            let compose = compose @@ Form.dn vl in
                            p_indexed_group extract fields group compose
                    end
                | Optional ->
                    begin
                        match extract key fields with
                        | exception Not_found ->
                            let compose = compose None in
                            p_indexed_group extract fields group compose
                        | mark, fields ->
                            let* _ = Scan.mov mark in
                            let* vl = Lazy.force scan in
                            let compose = compose @@ Some (Form.dn vl) in
                            p_indexed_group extract fields group compose
                    end
                | Default dvalue ->
                    begin
                        match extract key fields with
                        | exception Not_found ->
                            let compose = compose dvalue in
                            p_indexed_group extract fields group compose
                        | mark, fields ->
                            let* _ = Scan.mov mark in
                            let* vl = Lazy.force scan in
                            let compose = compose @@ Form.dn vl in
                            p_indexed_group extract fields group compose
                    end

    and p_variant:
        type k v. k index -> (k * v model) array -> v Form.t Scan.t
        = fun index cases ->
            let module Order =
                (val Cf_relations.Extensible.ordering index :
                Cf_relations.Order with type t = k)
            in
            let module Map = Cf_bsearch_data.Map.Create(Order) in
            let each (k, m) = k, lazy (p_model m) in
            let cases = Map.of_seq @@ Seq.map each @@ Array.to_seq cases in
            let tag = `Variant index#nym in
            let kscan = p_model index#model in
            let vret = Scan.return @@ Form.imp () in
            let kvscan =
                Scan.or_fail "variant content required" @@
                B.pair tag kscan vret
            in
            let* p0l = B.start tag Occurs.one in
            let* kvl = kvscan in
            let k, _ = Form.dn kvl in
            match Map.search k cases with
            | None ->
                Scan.fail "unrecognized variant case"
            | Some scan ->
                let scan = Lazy.force scan in
                let* vl = Scan.or_fail "variant content type error" scan in
                let+ p1l = p_finish p0l vl in
                Form.(span p0l p1l @@ dn vl)

    let scan = p_model
end

(*--- End ---*)
