(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type ('s, 'x, 'r) t = ('s -> 'x, 'r) Cf_cmonad.t
    (* ('r -> 's -> 'x) -> 's -> 'x *)
    (* 's -> (('s * 'r) -> 'x) -> 'x *)

module Basis = struct
    type nonrec ('s, 'x, 'r) t = ('s, 'x, 'r) t

    let return r f s = (f[@tailcall]) r s
    let bind m f g s = (m[@tailcall]) (fun r -> (f[@tailcall]) r g) s

    let product = `Default
    let mapping = `Default
end

include Cf_monad.Trinary.Create(Basis)

let nil f s = (f[@tailcall]) () s
let init x _ _ = x
let cont c f x = (c[@tailcall]) (f () x)
let load f s = (f[@tailcall]) s s
let store s f _ = (f[@tailcall]) () s
let modify c f s = (f[@tailcall]) () (c s)
let field r f s = (f[@tailcall]) (r s) s
let downC m s f = (m[@tailcall]) (fun () -> f) s
let downS m x s = s, m (fun () _ -> x) s
let liftC m f g = (m[@tailcall]) (fun r -> f r g)
let liftS m f s = let s, r = m s in (f[@tailcall]) r s
let eval m s x = (m[@tailcall]) (fun () _ -> x) s
let bridge x b m _ s = b (m (fun () _ -> x) s)

(*--- End ---*)
