(*---------------------------------------------------------------------------*
  Copyright (C) 2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Parsers for sequences of delimited elements. *)

(** {6 Overview} *)

(** This module provides scanners specialized for parsing {i chains}, i.e.
    sequences of grammar elements separated by a terminal delimiter, and
    optionally preceded and/or succeeded by a delimiter.
*)

(** {6 Interface} *)

(** The type of a leading/trailing delimiter control. *)
type ctrl = [ `Non | `Opt | `Req ]

(** The basis signature of a chain scanning module. *)
module type Basis = sig

    (** The type of the scanner symbol. *)
    type symbol

    (** The type of the scanner position. *)
    type position

    (** The type constructor of the scanner form. *)
    type +'a form

    (** The basic scanner module. *)
    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form
end

(** The signature of a chain scanning module. *)
module type Profile = sig

    (** The type of the scanner symbol. *)
    type symbol

    (** The type constructor of the scanner form. *)
    type +'a form

    (** The type of the scanner mark. *)
    type mark

    (** The type of the scanner. *)
    type +'a t

    (** The type of a chain discipline. *)
    type chain = private Chain: {
        separator: unit t;
        header: ctrl;
        trailer: ctrl;
        fail: (mark -> 'x) option;
    } -> chain

    (** Use [mk separator] to make a chain discipline with [separator] to scan
        for delimiters. Use [~a] and/or [~b] to control how leading/trailing
        delimiters are processed (respectively). For example, use [~b:`Opt] to
        specify that a trailing delimiter is optionally permitted.
    *)
    val mk: ?xf:(mark -> 'x) -> ?a:[< ctrl ] -> ?b:[< ctrl ] -> 'r t -> chain

    (** Use [sep c r p] to make a parser for separator [p] with optionality
        control [c].

        If [c] is [`Non] then no input is consumed and [r] is returned.

        If [c] is [`Opt] then returns either the value recognized by [p], or
        [r] if no value is recognized.

        If [c] is [`Req] then acts as [reqf ?xf p].
    *)
    val sep: ?xf:(mark -> 'x) -> [< ctrl ] -> 'r -> 'r t -> 'r t

    (** Use [sep0 c p] to make a parser for separator [p] with optionality
        control [c], in the case that [p] returns the unit type.

        If [c] is [`Non] then no input is consumed and [false] is returned.

        If [c] is [`Opt] then returns a boolean value indicating whether [p]
        recognized a separator.

        If [c] is [`Req] then acts as [reqf ?xf p >>= fun () -> return true].
    *)
    val sep0: ?xf:(mark -> 'x) -> [< ctrl ] -> unit t -> bool t

    (** Use [vis ~c ?a ?b f v] to compose a parser that recognizes a sequence
        of symbols in the input stream, delimited according to the chain
        discipline [~c], by applying a visitor function [f] at each sequence
        point to obtain its parser. The first sequence point is visited with
        the initializer [v].

        If [~a] is used, then it specifies the minimum number of elements in
        the sequence. If [~b] is used then it specifies the maximum number of
        elements in the sequence. Composition raises [Invalid_argument] if
        [a < 0] or [b < a].
    *)
    val vis: c:chain -> ?a:int -> ?b:int -> ('r -> 'r t) -> 'r -> 'r t

    (** Use [seq ?a ?b p] to create a new parser that recognizes a sequence of
        symbols in the input stream with [p], delimited according to the chain
        discipline [~c].

        If [~a] is used, then it specifies the minimum number of elements in
        the sequence. If [~b] is used then it specifies the maximum number of
        elements in the sequence. Composition raises [Invalid_argument] if
        [a < 0] or [b < a].
    *)
    val seq: c:chain -> ?a:int -> ?b:int -> 'r t -> 'r list t
end

(** Use [Create(B)] to make a chain scanning module for [B]. *)
module Create(B: Basis): Profile
   with type symbol := B.symbol
    and type 'a form := 'a B.form
    and type mark := B.Scan.mark
    and type 'a t := 'a B.Scan.t

(** A distinguished chain scanner for simple ASCII character symbols. *)
module ASCII: Profile
   with type symbol := char
    and type 'a form := 'a
    and type mark := Cf_scan.ASCII.mark
    and type 'a t := 'a Cf_scan.ASCII.t

(*--- End ---*)
