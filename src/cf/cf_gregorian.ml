(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2018, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type date = Date of {
    year: int64;
    month: int;
    day: int;
}

type cjd = CJD of int [@@unboxed]
type mjd = MJD of int64 [@@unboxed]

let m_invalid s = invalid_arg (__MODULE__ ^ s)
let m_fail s = failwith (__MODULE__ ^ s)

let mod64 n d = Int64.(let r = rem n d in if r < 0L then add r d else r)

let of_mjd =
    let rec loop year day =
        if day >= 146097 then
            (loop[@tailcall]) (Int64.succ year) (day - 146097)
        else
            year, day
    in
    let adjleap a b year day =
        if day = a then
            Int64.(add year 3L), b
        else
            Int64.(add year (of_int (day / b))), day mod b
    in
    let enter (MJD mjd) =
        let year = Int64.(div mjd 146097L) in
        let day = (mod64 mjd 146097L |> Int64.to_int) + 678881 in
        let year, day = loop year day in
        let year = Int64.(mul year 4L) in
        let year, day = adjleap 146096 36524 year day in
        let year = Int64.(mul (add (mul year 25L) (of_int (day / 1461))) 4L) in
        let day = day mod 1461 in
        let year, day = adjleap 1460 365 year day in
        let day = day * 10 in
        let month =  (day + 5) / 306 in
        let day = (day + 5) mod 306 in
        let day = day / 10 in
        let year, month =
            if month >= 10 then
                Int64.succ year, month - 10
            else
                year, month + 2
        in
        let month = succ month and day = succ day in
        Date {year; month; day }
    in
    enter

let to_mjd_unsafe =
    let mtab = [|
        0L; 31L; 61L; 92L; 122L; 153L;
        184L; 214L; 245L; 275L; 306L; 337L
    |] in
    let x365 = [| 0L; 365L; 730L; 1095L |] in
    let x36524 = [| 0L; 36524L; 73048L; 109572L |] in
    let auxd d y = Int64.(add d (mul 146097L (div y 400L))) in
    let enter y m d =
        let d = auxd Int64.(sub (of_int d) 678882L) y in
        let m = pred m in
        let y = Int64.to_int (mod64 y 400L) in
        let y, m = if m >= 2 then y, m - 2 else pred y, m + 10 in
        let y = (y + (m / 12)) mod 400 and m = m mod 12 in
        let d = Int64.(add d Array.(unsafe_get mtab m)) in
        let d = auxd d Int64.(of_int y) in
        let y, d = if y < 0 then y + 400, Int64.(sub d 146097L) else y, d in
        let d = Int64.(add d Array.(unsafe_get x365 (y land 3))) in
        let y = y lsr 2 in
        let d = Int64.(add d (of_int (1461 * (y mod 25)))) in
        let y = (y / 25) land 3 in
        let d = Int64.(add d Array.(unsafe_get x36524 y)) in
        MJD d
    in
    enter

let is_leap_year y = mod64 y 4L = 0L && mod64 y 100L <> 0L || mod64 y 400L = 0L
let md_nonleap_ = [| 31; 28; 31; 30; 31; 30; 31; 31; 30; 31; 30; 31 |]
let md_leap_ = [| 31; 29; 31; 30; 31; 30; 31; 31; 30; 31; 30; 31 |]

let is_valid ~year:y ~month:m ~day:d =
    m >= 1 && m <= 12 &&
    d >= 1 && d <= 31 &&
    begin
        let md = if is_leap_year y then md_leap_ else md_nonleap_ in
        d <= Array.unsafe_get md (pred m)
    end

let create ~year ~month ~day =
    if month < 1 || month > 12 then
        m_invalid ".create: month 1..12";
    if day < 1 || day > 31 then
        m_invalid ".create: day 1..31";
    let md = if is_leap_year year then md_leap_ else md_nonleap_ in
    if day > Array.unsafe_get md (pred month) then
        m_invalid ".create: not gregorian date";
    Date { year; month; day }

let of_cjd (CJD n) = MJD Int64.(sub (of_int n) 2400001L) |> of_mjd

let Date min_cjd = of_cjd (CJD min_int)
let Date max_cjd = of_cjd (CJD max_int)

let in_cjd_range (Date d) =
    let y = d.year and m = d.month and d = d.day in begin
        y >= min_cjd.year && y <= max_cjd.year &&
        not (y = min_cjd.year &&
            (m < min_cjd.month || m = min_cjd.month && d < min_cjd.day)) &&
        not (y = max_cjd.year &&
            (m > max_cjd.month || m = max_cjd.month && d > max_cjd.day))
    end

let to_cjd_unsafe year month day =
    let MJD d = to_mjd_unsafe year month day in
    Int64.(to_int (add d 2400001L))

let to_cjd date =
    if not (in_cjd_range date) then m_fail ".to_cjd: integer overflow";
    CJD (let Date d = date in to_cjd_unsafe d.year d.month d.day)

let Date min_mjd = of_mjd (MJD Int64.min_int)
let Date max_mjd = of_mjd (MJD Int64.max_int)

let in_mjd_range y m d =
    is_valid ~year:y ~month:m ~day:d &&
    begin
        y >= min_mjd.year && y <= max_mjd.year &&
        not (y = min_mjd.year &&
            (m < min_mjd.month || m = min_mjd.month && d < min_mjd.day)) &&
        not (y = max_mjd.year &&
            (m > max_mjd.month || m = max_mjd.month && d > max_mjd.day))
    end

let rec dayloop day =
    if day >= 146097 then (dayloop[@tailcall]) (day - 146097) else day

let day_of_week (Date d) =
    if not (in_mjd_range d.year d.month d.day) then
        m_fail ".day_of_week: computing julian day.";
    let MJD mjd = to_mjd_unsafe d.year d.month d.day in
    let xd = (mod64 mjd 146097L |> Int64.to_int) + 678881 |> dayloop in
    let wd = (xd + 3) mod 7 in
    if wd > 0 then wd else 7

let day_of_year (Date d) =
    if not (in_mjd_range d.year d.month d.day) then
        m_fail ".day_of_year: computing julian day.";
    let MJD mjd = to_mjd_unsafe d.year d.month d.day in
    let day = (mod64 mjd 146097L |> Int64.to_int) + 678881 |> dayloop in
    let day = if day = 146096 then 36524 else day mod 36524 in
    let day = day mod 1461 in
    let yd = if day < 306 then 1 else 0 in
    let day = if day = 1460 then 365 else day mod 365 in
    let yd = yd + day in
    let day = day * 10 in
    let month = (day + 5) / 306 in
    if month >= 10 then yd - 306 else yd + 59

let week_number =
    let last_year (Date d) = Date { d with year = Int64.pred d.year } in
    let end_of_year (Date d) = Date { d with month = 12; day = 31 } in
    let week_number_aux d = (day_of_year d - day_of_week d + 10) / 7 in
    let enter d =
        let w = week_number_aux d in
        let y =
            let Date d = d in
            d.year
        in
        if w < 1 then
            Int64.pred y, d |> last_year |> end_of_year |> week_number_aux
        else if w > 52 then
            Int64.succ y, d |> end_of_year |> week_number_aux
        else
            y, w
    in
    enter

(*--- End ---*)
