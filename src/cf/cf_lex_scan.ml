(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Buffer = sig
    type symbol
    type lexeme
    type t

    val create: unit -> t
    val reset: t -> unit
    val advance: t -> symbol -> unit
    val lexeme: t -> lexeme
end

module type Basis = sig
    type symbol and position and lexeme and +'a form

    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form

    module Form: Cf_scan.Form with type 'a t := 'a form

    module DFA: Cf_dfa.Profile with type event := symbol

    module Buffer: Buffer with type symbol := symbol and type lexeme := lexeme

    val string_to_term: string -> DFA.term
end

module type Profile = sig
    type symbol
    type lexeme
    type +'a form
    type +'a t

    module DFA: Cf_dfa.Regular with type event := symbol

    val string_to_term: string -> DFA.term
    val simple: DFA.term -> lexeme form t

    type 'a rule
    val rule: DFA.term -> (lexeme form -> 'a) -> 'a rule
    val analyze: 'a rule Seq.t -> 'a t

    module Affix: sig
        include Cf_dfa.Affix
           with type event := symbol
            and type term := DFA.term
            and type 'r fin := 'r DFA.fin

        val ( !$ ): string -> DFA.term
        val ( ?$$ ): string -> lexeme form t
        val ( $$> ): string -> (lexeme form -> 'a) -> 'a rule
    end
end

external identity: 'a -> 'a = "%identity"

module Create(B: Basis) = struct
    module DFA = B.DFA

    module S = B.Scan
    module Q = B.Buffer

    open S.Affix

    type lexeme = B.lexeme
    type 'a form = 'a B.form
    type 'a rule = (lexeme form -> 'a) DFA.fin

    let string_to_term = B.string_to_term
    let rule = DFA.fin

    let analyze =
        let finish w q up =
            match DFA.finish w with
            | exception Not_found ->
                DFA.reset w;
                Q.reset q;
                S.nil
            | f ->
                DFA.reset w;
                let lexeme = Q.lexeme q in
                Q.reset q;
                up lexeme |> f |> S.return
        in
        let rec loop w q p0 p1 =
            let* eos = S.fin in
            if eos then begin
                B.Form.span p0 p1 |> finish w q
            end
            else begin
                let* mark = S.cur and* iota = S.any in
                let sym = B.Form.dn iota in
                DFA.advance w sym;
                if DFA.rejected w then begin
                    let* _ = S.mov mark in
                    finish w q @@ B.Form.span p0 p1
                end
                else begin
                    Q.advance q sym;
                    (loop[@tailcall]) w q p0 iota
                end
            end
        in
        let enter rules =
            let w = DFA.create rules and q = Q.create () in
            let* eos = S.fin in
            if eos then
                finish w q @@ B.Form.imp
            else begin
                let* mark = S.cur and* iota = S.any in
                let sym = B.Form.dn iota in
                DFA.advance w sym;
                if DFA.rejected w then begin
                    let* _ = S.mov mark in
                    finish w q @@ B.Form.span iota iota
                end
                else begin
                    Q.advance q sym;
                    (loop[@tailcall]) w q iota iota
                end
            end
        in
        enter

    let simple term = rule term identity |> Seq.return |> analyze

    module Affix = struct
        include DFA.Affix

        let ( !$ ) = B.string_to_term
        let ( ?$$ ) s = simple (B.string_to_term s)
        let ( $$> ) s f = rule (B.string_to_term s) f
    end
end

module ASCII = struct
    module Aux = struct
        type symbol = char
        type position = Cf_scan.Simple.position
        type lexeme = string
        type 'a form = 'a

        module Scan = Cf_scan.ASCII
        module Form = Cf_scan.Simple.Form
        module DFA = Cf_regx.DFA

        module Buffer = struct
            type t = Buffer.t
            let create () = Buffer.create 0
            let reset = Buffer.reset
            let lexeme = Buffer.contents
            let advance = Buffer.add_char
        end

        let string_to_term = DFA.string_to_term
    end

    include Create(Aux)
end

(*--- End ---*)
