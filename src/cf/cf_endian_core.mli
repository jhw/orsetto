(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Utility functions for safe operations with byte-order sensitive data. *)

(** {6 Overview}

    Ancillary internal functions used to check the validity of numeric quantity
    and octet sequence subrange parameters in the [Cf_endian] interface.
*)

(** {6 Signatures} *)

(** The functions in this module offer fast unsafe operations that provide
    correct behavior only when the parameters are valid, i.e. when the position
    and length are valid for the string or byte sequence, and when integer and
    float quantities can converted between representations without error.
*)
module type Unsafe = sig
    val descript: string

    val lds8: string -> int -> int
    val ldu8: string -> int -> int
    val lds16: string -> int -> int
    val ldu16: string -> int -> int
    val lds32: string -> int -> int
    val ldu32: string -> int -> int
    val lds64: string -> int -> int
    val ldu64: string -> int -> int
    val ldi32: string -> int -> int32
    val ldi64: string -> int -> int64

    val ldi32_boxed: string -> int -> int32
    val ldi64_boxed: string -> int -> int64

    val sts8: int -> bytes -> int -> unit
    val stu8: int -> bytes -> int -> unit
    val sts16: int -> bytes -> int -> unit
    val stu16: int -> bytes -> int -> unit
    val sts32: int -> bytes -> int -> unit
    val stu32: int -> bytes -> int -> unit
    val sts64: int -> bytes -> int -> unit
    val stu64: int -> bytes -> int -> unit
    val sti32: int32 -> bytes -> int -> unit
    val sti64: int64-> bytes -> int -> unit

    val sti32_boxed: int32 -> bytes -> int -> unit
    val sti64_boxed: int64 -> bytes -> int -> unit
end

(** This module type defines octet encoding and decoding schemes for the
    various integer and floating point types.
*)
module type Safe = sig

    (** A Latin-1 string describing the endian mode, e.g. ["LE"] for
        little-endian or ["BE"] for big-endian.
    *)
    val descript: string

    (** Use [r#scan lds8] to read one octet with [r] as a signed 8-bit integer
        in the range [-128] to [127].
    *)
    val lds8: int Cf_decode.scheme

    (** Use [r#scan ldu8] to read one octet with [r] as an unsigned 8-bit
        integer in the range [0] to [255].
    *)
    val ldu8: int Cf_decode.scheme

    (** Use [r#scan lds16] to read two octets with [r] as a signed 16-bit
        integer in the range [-32768] to [32767].
    *)
    val lds16: int Cf_decode.scheme

    (** Use [r#scan ldu16] to read two octets with [r] as an unsigned 16-bit
        integer in the range [0] to [65535].
    *)
    val ldu16: int Cf_decode.scheme

    (** Use [r#scan lds32] to read four octets with [r] as a signed 32-bit
        integer in the range [-2147483648] to [2147483647]. Raises
        [Cf_decode.Invalid] where [Sys.int_length < 32] and the value cannot be
        represented by the OCaml [int] type.
    *)
    val lds32: int Cf_decode.scheme

    (** Use [r#scan ldu32] to read four octets with [r] as an unsigned 32-bit
        integer in the range [0] to [4294967295]. Raises [Cf_decode.Invalid] if
        the value cannot be represented by the OCaml [int] type.
    *)
    val ldu32: int Cf_decode.scheme

    (** Use [r#scan lds64] to read eight octets with [r] as a signed 64-bit
        integer in the range [-9223372036854775808] to [9223372036854775807].
        Raises [Cf_decode.Invalid] if the value cannot be represented by the
        OCaml [int] type.
    *)
    val lds64: int Cf_decode.scheme

    (** Use [r#scan ldu64] to read eight octets with [r] as an unsigned 64-bit
        integer in the range [0] to [18446744073709551615]. Raises
        [Cf_decode.Invalid] if the value cannot be represented by the OCaml
        [int] type.
    *)
    val ldu64: int Cf_decode.scheme

    (** Use [r#scan ldi32] to read four octets with [r] as a signed 32-bit
        integer in the range [-2147483648] to [2147483647].
    *)
    val ldi32: int32 Cf_decode.scheme

    (** Use [r#scan ldi32] to read eight octets with [r] as a signed 64-bit
        integer in the range [-9223372036854775808] to [9223372036854775807].
    *)
    val ldi64: int64 Cf_decode.scheme

    (** Use [r#scan ldf16] to read two octets with [r] as a binary16 format
        IEEE 754 floating point number.
    *)
    val ldf16: float Cf_decode.scheme

    (** Use [r#scan ldf32] to read four octets with [r] as a binary32 format
        IEEE 754 floating point number.
    *)
    val ldf32: float Cf_decode.scheme

    (** Use [r#scan ldf64] to read eight octets with [r] as a binary64 format
        IEEE 754 floating point number.
    *)
    val ldf64: float Cf_decode.scheme

    (** Use [w#emit sts8] to write with [w] a signed 8-bit integer as one
        octet. Raises [Invalid_argument] if the value to emit is not in the
        range [-128] to [127].
    *)
    val sts8: int Cf_encode.scheme

    (** Use [w#emit stu8] to write with [w] an unsigned 8-bit integer as one
        octet. Raises [Invalid_argument] if the value to emit is not in the
        range [0] to [255].
    *)
    val stu8: int Cf_encode.scheme

    (** Use [w#emit sts16] to write with [w] a signed 16-bit integer as two
        octets. Raises [Invalid_argument] if the value to emit is not in the
        range [-32768] to [32767].
    *)
    val sts16: int Cf_encode.scheme

    (** Use [w#emit stu16] to write with [w] an unsigned 16-bit integer as two
        octets. Raises [Invalid_argument] if the value to emit is not in the
        range [0] to [65535].
    *)
    val stu16: int Cf_encode.scheme

    (** Use [w#emit sts32] to write with [w] a signed 32-bit integer as four
        octets. Raises [Invalid_argument] if the value to emit is not in the
        range [-32768] to [32767].
    *)
    val sts32: int Cf_encode.scheme

    (** Use [w#emit stu32] to write with [w] an unsigned 32-bit integer as
        four octets. Raises [Invalid_argument] if the value to emit is not in
        the range [0] to [65535].
    *)
    val stu32: int Cf_encode.scheme

    (** Use [w#emit sts64] to write with [w] a signed 64-bit integer as eight
        octets. Raises [Invalid_argument] if the value to emit is not in the
        range [-9223372036854775808] to [9223372036854775807].
    *)
    val sts64: int Cf_encode.scheme

    (** Use [w#emit stu64] to write with [w] an unsigned 64-bit integer as
        eight octets. Raises [Invalid_argument] if the value to emit is not in
        the range [0] to [18446744073709551615].
    *)
    val stu64: int Cf_encode.scheme

    (** Use [w#emit sti32] to write with [w] a signed 32-bit integer as four
        octets.
    *)
    val sti32: int32 Cf_encode.scheme

    (** Use [w#emit sti64] to write with [w] a signed 64-bit integer as eight
        octets.
    *)
    val sti64: int64 Cf_encode.scheme

    (** Use [w#emit stf16] to write with [w] a floating point number as the
        four octets comprising a binary16 format IEEE 754 number. Raises
        [Imprecise] if the conversion truncates the fractional part or
        overflows the exponent part. Conversion of NaN always results in a
        signaling NaN.
    *)
    val stf16: float Cf_encode.scheme

    (** Use [w#emit stf32] to write with [w] a floating point number as the
        four octets comprising a binary32 format IEEE 754 number. Raises
        [Imprecise] if the conversion truncates the fractional part or
        overflows the exponent part. Conversion of NaN always results in a
        signaling NaN.
    *)
    val stf32: float Cf_encode.scheme

    (** Use [w#emit stf64] to write with [w] a floating point number as the
        eight octets comprising a binary64 format IEEE 754 number.
    *)
    val stf64: float Cf_encode.scheme
end

(** {6 Modules} *)

(** Include this module to include operations single-octet types. *)
module Unsafe_octet: sig

    (** Use [lds8 s i] to get the signed 8-bit integer at [i] in [s]. *)
    val lds8: string -> int -> int

    (** Use [ldu8 s i] to get the unsigned 8-bit integer at [i] in [s]. *)
    val ldu8: string -> int -> int

    (** Use [sts8 n b i] to set the octet at [i] in [b] to [n]. The prodedure
        is not defined for integers [-128 <= n <= 127].
    *)
    val sts8: int -> bytes -> int -> unit

    (** Use [sts8 n b i] to set the octet at [i] in [b] to [n]. The prodedure
        is not defined for integers [0 <= n <= 255].
    *)
    val stu8: int -> bytes -> int -> unit
end

(** Use this functor to construct safe encoders and decoders. *)
module Safe(U: Unsafe): Safe

(** Exception raised when conversion to binary16 or binary32 truncates the
    fractional part or overflows the exponent.
*)
exception Imprecise

(** Use [is_nan n] to test whether [n] is not a number. *)
val is_nan: float -> bool

(** Use [of_fp16_bits n] to convert a binary16 format integer representation of
    IEEE-754 "half-precision" floating point number.
*)
val of_fp16_bits: int -> float

(** Use [to_fp16_bits n] to convert a floating point number into its binary16
    format integer representation of IEEE-754 "half-precision" floating point.
    Raises [Imprecise] if conversion truncates the fractional part or overflows
    the exponent.
*)
val to_fp16_bits: float -> int

(** Use [to_fp32_bits n] to convert a floating point number into its binary32
    format integer representation of IEEE-754 "single-precision" floating
    point. Raises [Imprecise] if conversion truncates the fractional part or
    overflows the exponent.
*)
val to_fp32_bits: float -> int32

(** Use [to_fp16_bits n] to convert a floating point number into its binary16
    format integer representation of IEEE-754 "half-precision" floating point.
    Fractional bits beyond the first ten are truncated. If the exponent is out
    of range, then infinity is returned. If [n = nan] then result is the
    distinguished binary16 form of a signaling NaN.
*)
val to_fp16_bits_unsafe: float -> int

(** Use [to_fp32_bits n] to convert a floating point number into its binary32
    format integer representation of IEEE-754 "single-precision" floating
    point. Fractional bits beyond the first ten are truncated. If the exponent
    is out of range, then infinity is returned. If [n = nan] then result is the
    distinguished binary32 form of a signaling NaN.
*)
val to_fp32_bits_unsafe: float -> int32

(*--- End ---*)
