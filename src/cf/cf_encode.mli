(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Simple octet-stream encoders. *)

(** {6 Overview}

    This module provides for safely encoding structured data in octet streams
    according to schemes composed with functional combinators.

    To encode an octet stream progressively, make the appropriate [emitter]
    class object [exr] for the octet sink, then iteratively apply encoding
    schemes and values of corresponding type to the [exr#emit] method to encode
    them as octets to the sink.

    An encoding scheme is represented internally as a 3-tuple comprising a) the
    minimum number of octets [sz] required in the octet stream to represent a
    value of the encoded type, b) a structural analysis function [ck] described
    further below, and c) an encoder function [wr] that puts octets into a
    slice when provided with the structural analysis from the [ck] function.

    Combinators are provided for composing more useful complex encoding schemes
    from simpler schemes. For example, use [pair sa sb] to compose a scheme
    that encodes [(va, vb)] where [sa] encodes [va] and [sb] encodes [vb].

    The structural analysis function [ck] in an encoding scheme is applied to
    the current position in the stream, the current number of available octets
    in the encoding buffer, and the value [v] to encode. If the [ck] finds that
    [v] has no valid encoding, then an exception must be raised. Otherwise, it
    returns an analysis result that includes the number of octets required to
    encode the value. An internal exception is uses to signal to the emitter
    that a valid encoding may yet be found if the working slice is extended
    with more octets to initialize.

    The encoding function [wr] in an encoding scheme is applied to the octet
    vector [s] and cursor [i] in the working slice, and the structural analysis
    [x] returned by the [ck] function (described above), to encode the value in
    [s] starting at index [i]. The emitter ensures sufficient number of octets
    are available.

    Either encoder function may use [invalid msg] (see below) to signal a fault
    to the emitter when a value has no valid encoding. It causes the emitter to
    raise the [Invalid] exception.

    When encoding encounters the end of a finite octet stream, and the encoding
    scheme structural analysis result indicates that more octets are required,
    the emitting method raises the [Incomplete] exception.
*)

(** {6 Utility} *)

(** Private representation of the size requirement of an encoded value. *)
type size = private Size of int [@@ocaml.unboxed]

(** Private representation of the stream position of an encoded value. *)
type position = private Position of int [@@ocaml.unboxed]

(** {6 Validation} *)

(** Emitting may raise [Invalid (i, s)] in the [ck] function of an encoding
    scheme with the index [i] provided, which indiciates the position in the
    form where a value cannot be encoded, and diagnostic message [s] to
    describe the validation error.
*)
type exn += private Invalid of position * string

(** Emitters may raise [Incomplete n] when [n] additional octets are required
    in their working slice to emit the encoded value.
*)
type exn += private Incomplete of int

(** Private representation of an analysis result, comprising a non-negative
    count of the encoded octets and the structural analysis value.
*)
type 'x analysis = private Analysis of int * 'x

(** Use [analyze have need x] to make a structural analysis result, where
    [have] is the size of the current slice of working octets, [need] is the
    total number of octets in the analyzed structure of octets required in the
    working slice to encode the next value, and [x] is the structural analysis
    of the octets, which is provided to the [wr] function in the scheme.

    Using [analyze have need], i.e. without applying [x], raises [Incomplete]
    if [need > have], otherwise it returns with a unary constructor for the
    structural analysis.

    Raises [Invalid_argument] if either [have] or [need] is negative. An emitter
    raises [Failure] if an analysis result indicates that [need] is less than
    the minimum required octets for the encoding scheme.
*)
val analyze: size -> int -> 'x -> 'x analysis

(** Use [invalid p m] in the [ck] function of an encoding scheme with a
    position [p] in the octet stream (provided to the [ck] function), and a
    diagnostic message [m], to signal a validation error in encoding by raising
    an internal exception caught by the [emitter] class (see below).
*)
val invalid: position -> string -> 'a

(** Use [advance i pos] in check functions to advance [pos] by [i] positions.
    Raises [Invalid_argument] if [i < 0].
*)
val advance: int -> position -> position

(** {6 Schemes} *)

(** The type of an encoding scheme for values of the associated type. *)
type -'v scheme

(** Use [scheme sz ck wr] to make an encoding scheme that applies [ck pos n v]
    to analyze the proposal to encode [v] in no more than [n] octets, where
    [n >= sz]. If [ck] returns normally, then the scheme applies [wr b i x],
    where [x] is the analysis result returned normally by [ck], to emit at [i]
    in [b] the octets corresponding to the encoding of [v]. The [ck] function
    should call [invalid] (see above) if [v] cannot be encoded.
*)
val scheme:
    int -> (position -> size -> 'v -> 'x analysis) ->
    ('x -> bytes -> int -> unit) -> 'v scheme

(** The nil scheme. Ignores value and emits no octets. *)
val nil: 'v scheme

(** The any octet scheme. Emits exactly one octet. *)
val any: char scheme

(** The opaque string scheme. Emits the octets from its value. *)
val opaque: string scheme

(** The opaque string slice scheme. Emits the octets from its value. *)
val subopaque: string Cf_slice.t scheme

(** Use [buffer f] to make a scheme that applies [f] to a fresh buffer for each
    value and emits the buffer content.
*)
val buffer: (Buffer.t -> 'v -> unit) -> 'v scheme

(** {4 Composers} *)

(** Use [opt s] to make an encoding scheme for optional values, which
    encode [Some v] according to [s], and which encode [None] as an empty octet
    sequence.
*)
val opt: 'v scheme -> 'v option scheme

(** Use [pair a b] to make an encoding scheme for pairs of values, which
    encodes its first value according to [a] and its second value according to
    [b]. The [ck] function for each is called once by the [ck] function in the
    resulting scheme. Likewise, the [wr] function in each is called once in the
    [wr] function of the resulting scheme.
*)
val pair: 'a scheme -> 'b scheme -> ('a * 'b) scheme

(** Use [triple a b c] to make an encoding scheme for 3-tuples of values, which
    encodes its first value according to [a], its second value according to
    [b], and its third value according to [c]. The [ck] function for each is
    called once by the [ck] function in the resulting scheme. Likewise, the
    [wr] function in each is called once in the [wr] function of the resulting
    scheme.
*)
val triple: 'a scheme -> 'b scheme -> 'c scheme -> ('a * 'b * 'c) scheme

(** Use [seq s] to make an encoding scheme to emit each value in a sequence
    according to [s]. Use [~a] to specify a minimum number of elements to
    encode. Use [~b] to specify a maximum number of elements to encode. Raises
    [Invalid_argument] if [a] is less than zero, [b] is less than [a]. The [ck]
    function raises [Invalid] if the sequence has fewer then [a] elements.
*)
val seq: ?a:int -> ?b:int -> 'v scheme -> 'v Seq.t scheme

(** Use [map f s] to make an encoding scheme that applies [f] to its value and
    encodes the result according to [s]. The function [f] may call [invalid] if
    the map is not injective. Note well: [f] is applied during the check phase
    of [s], and it may be applied multiple times with the same value if the
    scheme requires more octets that the emitter has available.
*)
val map: (position -> 'a -> 'b) -> 'b scheme -> 'a scheme

(** {4 Monad} *)

(** Use this monad to compose encoding schemes where intermediate values
    emitted earlier in the octet stream are used to select encoding schemes for
    the values emitted later in the stream.

    For example, the [pair] scheme composer above is equivalent to this:

        {[let pair sa sb = eval begin fun (va, vb) ->
            seal sa va >>= fun () ->
            seal sb vb
        end]}
*)
module Monad: sig
    include Cf_monad.Unary.Profile with
        type +'r t = private (unit scheme, 'r) Cf_seqmonad.t

    (** Use [seal s v] to enclose the encoding of [v] with [s] before the
        values encoded in the monad bound to the result.
    *)
    val seal: 'a scheme -> 'a -> unit t

    (** Use [eval f] to encapsulate the monad into a scheme that applies [f] to
        the emitted value to obtain its sequence of sealed schemes. If a
        positive integer [~sz] is provided, then it specifies the minimum size
        of any emitted value.
    *)
    val eval: ?sz:int -> ('a -> unit t) -> 'a scheme
end

(** {4 Abstract Data Renderers} *)

(** Use the [Create(B: Basis)] functor in this module as a simpler method to
    create a data renderer profile that can make encoding schemes from abstract
    data models.
*)
module Render: sig

    (** Use a model of this simpler signature to create a data renderer that
        specificall produces encoding schemes.
    *)
    module type Basis = sig

        (** Same as [Cf_data_render.Basis.primitive]. *)
        val primitive: 'a Cf_type.nym -> 'a scheme

        (** Same as [Cf_data_render.Basis.control]. *)
        val control: [
            | `Default
            | `Special of
                'a scheme -> ('a, 'b) Cf_data_render.control -> 'b scheme
        ]

        (** Same as [Cf_data_render.Basis.pair]. *)
        val pair: [
            | `Default
            | `Special of
                'k Cf_data_render.pair -> unit Monad.t -> unit Monad.t ->
                unit Monad.t
        ]

        (** Same as [Cf_data_render.Basis.sequence]. *)
        val sequence: [
            | `Default
            | `Special of
                'k Cf_data_render.container -> unit Monad.t Seq.t ->
                unit Monad.t
        ]
    end

    (** Use [Create(B)] to make a data renderer that produces encoding schemes
        according to the protocol defined in [B].
    *)
    module Create(B: Basis):
        Cf_data_render.Profile with type 'a scheme := 'a scheme

    (** Use [basis_control m c] to transform the encoding scheme [m] to a new
        scheme according to the rendering control [c]. Raises
        [Invalid_argument] if [c] has an unrecognized extensible variant tag,
        i.e. not one of the tags defined in {Cf_data_render}.
    *)
    val basis_control:
        'a scheme -> ('a, 'b) Cf_data_render.control -> 'b scheme
end

(** {6 Emitters} *)

(** Use [required_size s v] to check whether [v] can be encoded by [s]. If so,
    then returns the length of the encoded octets. Otherwise, raises [Invalid].
*)
val required_size: 'v scheme -> 'v -> size

(** Use [to_string s v] to make a string comprising the octets that encode [v]
    according to [s].
*)
val to_string: 'v scheme -> 'v -> string

(** The class of octet stream emitters. Use [new emitter ())] to make a basic
    emitter object that can progressively encode values into the working slice.
    Likewise, use [inherit emitter ())] to derive a subclass that implements
    more refined behavior by overriding private methods to manipulate the
    working slice and the cursor position. Use the optional [~start] parameter
    to initialize the starting position counter to a number other than zero.
    (See documentation below for the various private members.)
*)
class emitter: ?start:position -> unit -> object

    (** The working slice. *)
    val mutable slice_: bytes Cf_slice.t

    (** The cursor position. *)
    val mutable cursor_: int

    (** Stream position of slice. *)
    val mutable start_: position

    (** Use [self#work slice] to set [slice] as the working slice and set
        [cursor_] to the start of [slice].
    *)
    method private work: bytes Cf_slice.t -> unit

    (** A successful emission calls [self#advance n] to signal that a value was
        encoded as [n] octets in the working slice at the cursor position.

        The basis implementation is simply [cursor_ <- cursor_ + n].
    *)
    method private advance: int -> unit

    (** If emitting a value requires at least [n] more octets to encode than
        are currently available in the working slice after the cursor position,
        the basis implementation of [self#expand] applies [self#incomplete n]
        as necessary to update the working slice and cursor position until the
        working slice is large enough.

        The basis implementation raises [Incomplete].
    *)
    method private incomplete: int -> unit

    (** To reserve space at the cursor position in the working slice to store
        [v] as octets according to [s].

        To accomplish this it applies the [ck] function in [v] with the current
        position, the working slice and [v] to obtain the `check` result for
        the scheme. If [n] more octets are required, then [self#incomplete n]
        is applied and [self#allocate s v] is applied tail recursively. If [n]
        octets are available, then the size and an unsafe write function is
        returned for the [emit] method to use.

        No exceptions raised in any of the private methods in [exr] or the
        functions in [s] are caught.
    *)
    method private allocate:
        'v. 'v scheme -> 'v -> int * (bytes -> int -> unit)

    (** When an encoding scheme calls the [invalid p m] function (see above),
        the emitter catches the internal exception and invokes this method to
        raise the corresponding [Invalid (p, m)] accordingly.
    *)
    method private invalid: 'a. position -> string -> 'a

    (** Use [exr#emit s v] to store [v] as octets according to [s] at the
        cursor position in the working slice.

        To accomplish this, it first obtains the allocation size [n] and
        validates [v]. In the [Fix] case, the size is taken from the [sz] field
        in [s] and validation is done by applying the [ck] function in [s]. In
        the [Var] case, validation is performed and the size is computed by
        applying the [ck] function in [s]. Next, if [n > 0], it applies
        [self#allocate n], then applies the [wr] function, and finally applies
        [self#advance n].

        No exceptions raised in any of the private methods in [exr] or the
        functions in [s] are caught.
    *)
    method emit: 'v. 'v scheme -> 'v -> unit

    (** Use [exr#position] to get the total number of valid octets ever
        emitted by [exr].
    *)
    method position: position
end

(** Use [bytes_emitter ?start b] to make an emitter that encodes values
    progressively to the byte array [b] and raises [Incomplete] when the
    remaining octets in bytes array are insufficient to encode a value. Use the
    [~start] parameter to initialize the starting position of the first octet
    in [b] to a number other than zero.
*)
val bytes_emitter: ?start:position -> bytes -> emitter

(** Use [slice_scanner ?start sl] to make an emitter that encodes values
    progressively to the byte array slice [sl] and raises [Incomplete] when the
    remaining octets in the slice are insufficient to encode a value. Use the
    [~start] parameter to initialize the starting position of the first octet
    in [sl] to a number other than zero.
*)
val slice_emitter: ?start:position -> bytes Cf_slice.t -> emitter

(** The class of synchronous emitters, which contains a working slice of octets
    and optionally raises [Failure] if the size requirement for any particular
    emitted value is larger than [limit]. Objects of this class are constructed
    by the functions below, e.g. [buffer_emitter], [channel_emitter], etc.
*)
class sync_emitter: ?start:position -> ?limit:int -> unit -> emitter

(** Use [buffer_emitter ?start ?limit b] to make an emitter that adds its
    encoded octets to the buffer [b] every time its [emit] method is called.
    Use the optional [~limit] to make the [emit] method raise [Failure] if the
    size requirement for any particular emitted value is larger than [limit].

    Emitters of this class never raise the [Incomplete] exception.
*)
val buffer_emitter: ?start:position -> ?limit:int -> Buffer.t -> emitter

(** Use [channel_emitter ?limit c] to make an emitter that outputs its encoded
    octets to the channel [c] every time its [emit] method is called. Use
    [~limit] to make the [emit] method raise [Failure] if the size requirement
    for an emitted value is larger than [limit].

    Emitters of this class never raise the [Incomplete] exception.
*)
val channel_emitter: ?start:position -> ?limit:int -> out_channel -> emitter

(** Values of this class type are returned by the [framer] function below. *)
class type framer = object
    inherit emitter

    (** Use [exr#frame] to get the immutable contents of the current working
        slice and begin using a new working slice in subsequent applications
        of the [emit] method.
    *)
    method frame: string Cf_slice.t
end

(** Use [framer ?start ?limit] to create a framing emitter. *)
val framer: ?start:position -> ?limit:int -> unit -> framer

(*--- End ---*)
