(*---------------------------------------------------------------------------*
  Copyright (C) 2005-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Core = struct
    module Basis = struct
        module Event = Char

        let d =
            Cf_seq.range 0 255 |> Seq.map Char.chr |> Cf_dfa.Aux.Eager.of_chars

        module Dispatch = (val d : Cf_dfa.Aux.Char_dispatch)
    end

    include Cf_dfa.Core(Basis)
end

module Affix = Cf_dfa.Mk_affix(Core)

let esc_prefix = '\\'

module Scan = struct
    module DFA = Core
    open Affix

    let invalid msg =
        invalid_arg (Printf.sprintf "%s: %s" __MODULE__ msg)

    module S = struct
        include Cf_scan.ASCII
        include Cf_number_scan.ASCII

        open Affix
        open Cf_number_scan

        let code10 =
            let opt = [ O_leading; O_width 2] in
            let number = special @@ ctrl ~opt R_int in
            let enter prefix =
                let* n = number in
                let n = prefix + n in
                if n > 255 then nil else return @@ Char.chr n
            in
            enter

        let code16 =
            let opt = [ O_leading; O_radix hexadecimal; O_width 2] in
            let* n = special @@ ctrl ~opt R_int in
            return @@ Char.chr n
    end

    open S.Affix

    module CMap = Cf_bsearch_data.Map.Of_char

    let is_alpha = function 'A'..'Z' | 'a' .. 'z' -> true | _ -> false
    let is_digit = function '0'..'9' -> true | _ -> false
    let is_alnum c = is_digit c || is_alpha c
    let ck_white x = function '\009'..'\013' | '\032' -> x | _ -> not x

    let c_control =
        let f = function
            | '@'..'_' | 'a'..'z' -> true
            | _ -> false
        in
        let* c = S.sat f in
        let n = Char.code c in
        let n = if n >= 97 then n - 96 else n - 64 in
        S.return @@ Char.chr n

    let s_esc1 = Array.to_seq [|
        't', S.return '\x09';
        'r', S.return '\x0D';
        'n', S.return '\x0A';
        'x', S.code16;
        '0', S.code10 0;
        '1', S.code10 100;
        '2', S.code10 200;
        'c', c_control;
        esc_prefix, S.return esc_prefix
    |]

    let s_escf = Array.to_seq [|
        'a', is_alpha;
        'd', is_digit;
        'i', is_alnum;
        's', ck_white true;
        'w', ck_white false;
    |]

    module CClass = struct
        let c_raw = S.sat (function '-' | ']' -> false | _ -> true)

        let c_esc1 =
            let s = Seq.return (']', S.return ']') in
            let m = CMap.of_seq @@ Cf_seq.concat s s_esc1 in
            let* _ = S.one esc_prefix in
            let* c = S.any in
            match CMap.search c m with
            | Some scan -> scan
            | None -> S.nil

        let c_single = ?^~(Array.to_seq [| c_esc1; c_raw |])

        let sat_range =
            let* a = c_single in
            let* _ = ?.'-' in
            let* b = c_single in
            let a = Char.code a and b = Char.code b in
            if a > b then invalid "invalid range class";
            S.return @@ fun c -> let c = Char.code c in not (c < a || c > b)

        let sat_single = c_single >>: Char.equal

        let sat_escape =
            let m = CMap.of_seq s_escf in
            let* _ = ?.esc_prefix in
            let* c = S.any in
            match CMap.search c m with
            | Some sat -> S.return sat
            | None -> S.nil

        let sat_hyphen = S.one '-' >>: Char.equal

        let atom = ?^~ begin
            Array.to_seq [| sat_hyphen; sat_escape; sat_range; sat_single |]
        end

        let optcarat =
            let* c = ?/ ?.'^' in
            S.return (match c with Some _ -> true | None -> false)

        let term =
            let* _ = ?.'[' in
            let* negated = optcarat in
            let* hd, tl = ?+ atom in
            let* _ = ?.']' in
            let f c =
                let v = List.exists ((|>) c) (hd :: tl) in
                if negated then not v else v
            in
            S.return @@ DFA.sat f
    end

    let symbol =
        let f = function
            | '?' | '*' | '+' | '(' | ')' | '|' | '[' | ']' | '$' | '^'
            | '\x00'..'\x1f'
            | '\x7f'..'\xff' -> false
            | _ -> true
        in
        S.sat f >>: DFA.one

    let escape =
        let up0 c = c, S.return @@ DFA.one c in
        let up1 (c, scan) = c, scan >>: DFA.one in
        let upf (c, sat) = c, S.return @@ DFA.sat sat in
        let s0 =
            String.to_seq ".?+*[]()|^$" |> Seq.map up0 |> Cf_seq.reverse
        in
        let s1 = Seq.map up1 s_esc1 |> Cf_seq.reverse in
        let s2 = Seq.map upf s_escf |> Cf_seq.reverse in
        let m = CMap.of_seq @@ List.to_seq @@ List.rev @@ (s2 @ s1 @ s0) in
        let* _ = ?.esc_prefix in
        let* c = S.any in
        match CMap.search c m with
        | Some scan -> scan
        | None -> S.nil

    let dot =
        let f c = c <> '\n' in
        let* _ = ?.'.' in
        S.return @@ DFA.sat f

    let simple = ?^~(Array.to_seq [| escape; dot; CClass.term; symbol |])

    let postfix =
        let question x = let* _ = ?.'?' in S.return !?x in
        let plus x = let* _ = ?.'+' in S.return !+x in
        let star x = let* _ = ?.'*' in S.return !*x in
        let ps = Array.to_seq [| question; plus; star; S.return |] in
        let enter x = ?^~(Seq.map (fun f -> f x) ps) in
        enter

    let xcats fix =
        let* hd, tl = ?+(?^[ simple; fix ] >>= postfix) in
        S.return @@ DFA.cats @@ List.to_seq @@ hd :: tl

    let xalts fix =
        let fix = xcats fix in
        let* hd = fix in
        let* tl = ?* (let* _ = ?.'|' in fix) in
        S.return @@ DFA.alts @@ List.to_seq @@ hd :: tl

    let rec xgroup () =
        let* _ = ?. '(' in
        let* x = xalts @@ xgroup () in
        let* _ = ?. ')' in
        S.return x

    let term = xcats @@ xgroup ()
end

module DFA = struct
    include Core

    let invalid fn s =
        invalid_arg @@ Printf.sprintf
            "%s.%s: invalid regular expression, \"%s\"" __MODULE__ fn s

    let string_to_term s =
        try Scan.(S.of_string term s)
        with Not_found -> invalid "string_to_term" s

    let chars_to_term s =
        try Scan.(S.of_seq term s) with
        | Not_found ->
            let b = Buffer.of_seq s in
            invalid "chars_to_term" (Buffer.contents b)

    module Affix = struct
        include Affix

        let ( !$ ) = string_to_term
        let ( $$= ) s r = fin (string_to_term s) r
    end
end

type t = Regx of { mk: unit -> unit DFA.t }
open DFA.Affix

let of_dfa_term term =
    let mk () = DFA.create @@ List.to_seq [ term $= () ]in
    Regx { mk }

let of_chars s =
    match DFA.chars_to_term s with
    | exception (Failure msg) -> invalid_arg msg
    | term -> of_dfa_term term

let of_string s =
    match DFA.string_to_term s with
    | exception (Failure msg) -> invalid_arg msg
    | term -> of_dfa_term term

let test =
    let rec loop w i n s =
        if DFA.rejected w then
            false
        else if i < n then begin
            let c = String.unsafe_get s i in
             DFA.advance w c;
            (loop[@tailcall]) w (succ i) n s
        end
        else
            DFA.accepted w
    in
    let enter (Regx r) s =
        let w = r.mk () and n = String.length s in
        (loop[@tailcall]) w 0 n s
    in
    enter

let contains =
    let rec loop w n s i j =
        if j < n then begin
            if DFA.rejected w then begin
                DFA.reset w;
                let i = succ i in
                (loop[@tailcall]) w n s i i
            end
            else begin
                let c = String.unsafe_get s j in
                DFA.advance w c;
                (loop[@tailcall]) w n s i (succ j)
            end
        end
        else
            DFA.accepted w
    in
    let enter (Regx r) s =
        let w = r.mk ()  and n = String.length s in
        (loop[@tailcall]) w n s 0 0
    in
    enter

let search =
    let rec loop w s0 s i j =
        match s () with
        | Seq.Cons (c, s) ->
            if DFA.rejected w then begin
                if DFA.accepted w then
                    Some (i, j)
                else begin
                    DFA.reset w;
                    let i = succ i and s = Cf_seq.tail s0 in
                    (loop[@tailcall]) w s s i i
                end
            end
            else begin
                DFA.advance w c;
                (loop[@tailcall]) w s0 s i (succ j)
            end
        | Seq.Nil ->
            if DFA.accepted w then Some (i, j) else None
    in
    let enter (Regx r) s = (loop[@tailcall]) (r.mk ()) s s 0 0 in
    enter

let split =
    let open Cf_slice in
    let rec loop w v n a b i () =
        if i < n then begin
            if DFA.rejected w then begin
                if DFA.accepted w then begin
                    DFA.reset w;
                    let hd = of_substring v a b in
                    let tl = loop w v n i i i in
                    Seq.Cons (hd, tl)
                end
                else begin
                    DFA.reset w;
                    let b = succ b in
                    (loop[@tailcall]) w v n a b b ()
                end
            end
            else begin
                let c = String.unsafe_get v i in
                DFA.advance w c;
                (loop[@tailcall]) w v n a b (succ i) ()
            end
        end
        else if DFA.accepted w then
            Seq.Cons (of_substring v a b, Seq.empty)
        else
            Seq.Nil
    in
    let enter (Regx r) s =
        let w = r.mk () and v = s.vector and n = s.limit and i = s.start in
        loop w v n i i i
    in
    enter

let quote =
    let special = Printf.sprintf "%c.?*+()|[]^$" esc_prefix in
    let rec loop b s i n =
        if i < n then begin
            let c = String.unsafe_get s i in
            if String.contains special c then Buffer.add_char b esc_prefix;
            Buffer.add_char b c;
            (loop[@tailcall]) b s (succ i) n
        end
    in
    let enter s =
        let n = String.length s in
        let b = Buffer.create n in
        loop b s 0 n;
        Buffer.contents b
    in
    enter

let unquote =
    let rec loop esc b s i n =
        if i < n then begin
            let c = String.unsafe_get s i in
            let i = succ i in
            let esc = not esc && c = esc_prefix && i < n in
            if not esc then Buffer.add_char b c;
            (loop[@tailcall]) esc b s i n
        end
    in
    let enter s =
        let n = String.length s in
        let b = Buffer.create n in
        loop false b s 0 n;
        Buffer.contents b
    in
    enter

(*--- End ---*)
