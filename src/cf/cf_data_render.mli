(*---------------------------------------------------------------------------*
  Copyright (C) 2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Rendering data according to an abstract model. *)

(** {6 Overview}

    This module facilitates the rendering of data conforming to an abstract
    model using any interchange language provided with a basis module defining
    the emitter type and some basic emitters and emitter composers.

    Data models are functionally composable. Optional affix convenience
    operators are provide an indiomatic syntax.

    With a fully composed data models and the render specialization of an
    interchange language, you produce an emitter that produces output of
    well-typed values according to the corresponding model.

    A data model represents either the "primitive" type of a scalar value or
    a "composed" type of a group of values.

    A primitive model represents an OCaml value identified by a [Cf_type.nym].
    Some nyms are conventionally accepted by every interchange language in the
    Orsetto distribution:

    - [Cf_type.Unit]
        A type with exactly one value, e.g. the JSON "null" value.

    - [Cf_type.Bool]
        A boolean value type.

    - [Cf_type.Int]
        An integer value in the range that an OCaml {int} can represent.

    - [Cf_type.Int32]
        An integer value in the range that an OCaml {int32} can represent.

    - [Cf_type.Int64]
        An integer value in the range that an OCaml {int64} can represent.

    - [Cf_type.Float]
        An IEEE 754 floating point value in the range that an OCaml {float} can
        represent.

    - [Cf_type.String]
        An octet string, not necessarily associated with a particular encoding.

    - [Cf_type.Opaque]
        An opaque value conventionally representing any scalar or group value
        in the underlying interchange language. (Note well: opaque values may
        contain interchange language specific elements that cannot expressed in
        other interchange languages.)

    A group model takes one of the following forms:

    - Vector
        A sequence of zero, one or more elements all of with the same model.

    - Table
        A sequence of zero, one or more pairs, comprising a domain model and a
        codomain model, where the domain model corresponds to a totally
        ordered type and no two pairs in the sequence share the same domain
        value.

    - Record
        A sequence of zero, one or more elements, each of which may have a
        different model indicated by its position in the sequence.

    - Structure
        A sequence of zero, one or more pairs, comprising an index model and a
        field model, where the index model corresponds to a totally ordered
        type, the field model is indicated by the value of the index, and no
        two pairs in the sequence share the same index value.

    - Variant
        Exactly one pair, comprising an index model and a field mode, where the
        field model is indicated by the value of the index.
*)

(** {6 Generalized Data Models}

    The types and values in this section facilitate the composition of abstract
    data models for use in constructing emitters with interchange languages
    for which a render specialization is defined.
*)

(** The type constructor ['v model] represents the rendering of ['v] values
    encoded to some interchange language.
*)
type 'v model

(** Use [primitive nym] to compose a model that corresponds to all the values
    of the type associated with [nym].
*)
val primitive: 'v Cf_type.nym -> 'v model

(** Use [cast f m] to compose a model that corresponds to the values produced
    by applying [f] to rendered values for the model [m].
*)
val cast: ('b -> 'a) -> 'a model -> 'b model

(** Use [default v m] to compose a model for rendered optional values in the
    model [m] where [v] is output when the rendered value is [None].
*)
val default: 'a -> 'a model -> 'a option model

(** Use [defer m] to compose a model by forcing [m] as needed. This is useful
    for composing recursive data models to be used in the array arguments of
    the [choice] and [variant] model constructors (see below).
*)
val defer: 'a model Lazy.t -> 'a model

(** Use [choice f s] to compose a model that applies [f] to the rendered value
    to select the index of the model in [s] used to produce the output.
*)
val choice: ('v -> int) -> 'v model array -> 'v model

(** Use [vector m] to compose a model that corresponds to a variable length
    array of values produced by the emitter for the model [m].
*)
val vector: 'v model -> 'v array model

(** Use [new index nym] to create an index object for the type indicated by
    [nym] required for composing table, structure and variant group models. Use
    the optional [~cmp] argument to specify a concrete comparator function to
    use instead of [Stdlib.( = )]. Use the optional [~model] argument to
    specify the model explicitly, otherwise the primitive model for [nym] is
    used for the domain or index in the compoed group model.
*)
class ['key] index:
    ?cmp:('key -> 'key -> int) -> ?model:'key model -> 'key Cf_type.nym ->
    object
        (** The total order relation for the key. *)
        inherit ['key] Cf_relations.Extensible.order

        (** The type nym for the key. *)
        method nym: 'key Cf_type.nym

        (** The render model for the key. *)
        method model: 'key model
    end

(** Use [table k v] to compose a model that corresponds to a variable length
    array of pairs comprising a domain value of the type described by the
    index [k] and a codomain value of the type described by the model [v].
    Use the optional [~unsorted] argument to specify that the emitter does not
    sort the rendered array.
*)
val table: ?unsorted:unit -> 'k #index -> 'v model -> ('k * 'v) array model

(** The abstract type parameter for group elements where the position in the
    sequence indicates the model for the element.
*)
type element

(** The type constructor [\['g, 'p\] bind] represents the model of a specific
    element in a group with fields or elements of disparate type. In the case
    where type ['g] is [element], the type ['p] corresponds to the type
    indicated by the position in the sequence. In the case where type ['g] is
    ['k index], the type ['p] corresponds to the type indicated by its index
    value.
*)
type ('g, 'v) bind

(** A submodule containing composers for elements indexed by position. *)
module Element: sig

    (** Use [required m f] to compose a group element where a required output
        value that matches the model [m] is obtained by applying the projector
        [f] to the rendered value.
    *)
    val required: 'r model -> ('v -> 'r) -> (element, 'v) bind

    (** Use [optional m f] to compose a group element where an optional output
        value that matches the model [m] is obtained by applying the projector
        [f] to the rendered value. If the projector returns [None] then no
        element is output.
    *)
    val optional: 'r model -> ('v -> 'r option) -> (element, 'v) bind

    (** Use [constant m v] to compose a group element where a required output
        value [v] that matches the model [m] is produced regardless of the
        rendered value.
    *)
    val constant: 'r model -> 'r -> (element, 'v) bind
end

(** Use [record s] to compose a model that corresponds to variable length array
    of elements corresponding to the projection of the rendered value with all
    the element binders in [s] in sequence.
*)
val record: (element, 'v) bind array -> 'v model

(** A submodule containing composers for elements indexed by some totally
    ordered domain type.
*)
module Field: sig

    (** Use [required k v f] to compose a group field where a required output
        value with key [k] and value matching the model [v] obtained by
        applying the projector [f] to the rendered value.
    *)
    val required: 'k -> 'r model -> ('v -> 'r) -> ('k index, 'v) bind

    (** Use [optional k v f] to compose a group field where an optional output
        value with key [k] and value matching the model [v] obtained by
        applying the projector [f] to the rendered value. If the projector
        returns [None] then no field with key [k] is output.
    *)
    val optional: 'k -> 'r model -> ('v -> 'r option) -> ('k index, 'v) bind

    (** Use [constant k v def] to compose a group field where a required output
        value with key [k] and value [def] matching the model [v] is produced
        regardless of the rendered value.
    *)
    val constant: 'k -> 'r model -> 'r -> ('k index, 'v) bind
end

(** Use [structure d s] to compose a model that corresponds to variable length
    array of pairs comprising a key of the type described by the index [d] and
    a value of a type indicated by the field in [s] associated with the key.
    Use the optional [~unsorted] argument to specify that the emitter does not
    sort the rendered array by their keys.
*)
val structure:
    ?unsorted:unit -> 'k #index -> ('k index, 'v) bind array -> 'v model

(** Use [variant d f s] to compose a model that corresponds to a pair
    comprising a key of the type described by the index [d] obtained by
    applying [f] to the rendered value, and a value of a type indicated by, and
    a value obtained according to the field bind associated with the key.
*)
val variant:
    'k #index -> ('v -> 'k) -> ('k * (element, 'v) bind) array -> 'v model

(** A submodule containing affix operators the provide a convenient and
    idiosyncratic syntax for composing record and structure models.
*)
module Affix: sig

    (** Use [m $: f] as an abbreviation of [Element.required m f]. *)
    val ( $: ): 'r model -> ('v -> 'r) -> (element, 'v) bind

    (** Use [m $? f] as an abbreviation of [Element.optional m f]. *)
    val ( $? ): 'r model -> ('v -> 'r option) -> (element, 'v) bind

    (** Use [m $= v] as an abbreviation of [Element.constant m v]. *)
    val ( $= ): 'r model -> 'r -> (element, 'v) bind

    (** Use [m %: f] as an abbreviation of [Field.required m f]. *)
    val ( %: ): ('k * 'r model) -> ('v -> 'r) -> ('k index, 'v) bind

    (** Use [m %? f] as an abbreviation of [Field.optional m f]. *)
    val ( %? ): ('k * 'r model) -> ('v -> 'r option) -> ('k index, 'v) bind

    (** Use [m %= v] as an abbreviation of [Field.constant m v]. *)
    val ( %= ): ('k * 'r model) -> 'r -> ('k index, 'v) bind

    (** Use [m /= f] as an abbreviation of [cast f m] *)
    val ( /= ): 'a model -> ('b  -> 'a) -> 'b model
end

(** {6 Specialization}

    Usage of generalized data models with a specific interchange language
    entails composing a render specialization for it. The types and values in
    this section are provided for that purpose.
*)

(** The extensible control type. Facilitates composing data models with
    features extended by the render specialization for unique features of the
    interchange language. A value of type [('a, 'b) control] represents a
    conversion of a model of type ['a model] to a model of type ['b model] as a
    side effect of any additional constraints applied.
*)
type (_, _) control = ..

(** A control value [Cast f] represents the operation of applying [f] to the
    rendered value to produce the value rendered by the model under control.
*)
type (_, _) control += private Cast: ('b -> 'a) -> ('a, 'b) control

(** A control value [Default v] represents the conversion of an optional
    rendered value into a required value by using [v] in the event the rendered
    value is [None].
*)
type (_, _) control += private Default: 'a -> ('a, 'a option) control

(** Use [control m c] to compose a new model comprising the application of [c]
    to the model [m].
*)
val control: 'a model -> ('a, 'b) control -> 'b model

(** Values of type ['k pair] are used to describe the structure of pair
    elements in sequences that comprise group data models indexed by values of
    type ['k].
*)
type 'k pair = [
    | `Table of 'k Cf_type.nym
    | `Structure of 'k Cf_type.nym
    | `Variant of 'k Cf_type.nym
]

(** Values of type ['k container] are used to describe the structure of
    sequences that comprise group data models.
*)
type 'k container = [ `Vector | `Record | 'k pair ]

(** Define a module of type [Basis] to create a render specialization. *)
module type Basis = sig

    (** The general type of an emitter of rendered values. *)
    type -'v scheme

    (** The emitted compiler uses [primitive nym] to create an emitter for the
        primitive type named by [nym].
    *)
    val primitive: 'v Cf_type.nym -> 'v scheme

    (** The emitter compiler uses [control e c] to compose an emitter
        representing the application of control [c] to the value output by [e].
    *)
    val control: 'a scheme -> ('a, 'b) control -> 'b scheme

    (** The general type of an encapusated emission. *)
    type packet

    (** The emitter compiler uses [delegate f] to compose a scheme that applies
        [f] to the rendered value to make an encapsulated emission.
    *)
    val delegate: ('v -> packet) -> 'v scheme

    (** The emitter compiler uses [packet e v] to produce an encapsulated
        emission of [v] using the emitter [e].
    *)
    val packet: 'v scheme -> 'v -> packet

    (** The emitter compiler uses [pair g a b] to combine the encapsulated
        emissions of [a] and [b] according to the structural descriptor [g].
    *)
    val pair: 'k pair -> packet -> packet -> packet

    (** The emitter compiler uses [sequence g s] to combine the encapsulated
        emissions in [s] according to the structure descriptor [g].
    *)
    val sequence: 'k container -> packet Seq.t -> packet
end

(** The module type of render specializations. *)
module type Profile = sig

    (** The type of a specialized emitter of rendered values. *)
    type -'v scheme

    (** Use [scheme m] to compile a specialized emitter for the model [m]. *)
    val scheme: 'v model -> 'v scheme
end

(** Use [Create(B)] to create a render specialization from the basis [B]. *)
module Create(B: Basis): Profile with type 'v scheme := 'v B.scheme

(*--- End ---*)
