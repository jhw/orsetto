(*---------------------------------------------------------------------------*
  Copyright (C) 2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional lexical analyzers. *)

(** {6 Overview}

    This module implements functional combinators for parsing languages that
    comprise sequences of regular tokens recognized by a deterministic finite
    automaton, using the {Cf_regx} module.
*)

(** {6 Interface} *)

(** Define a module with this signature to represent an imperative interface
    to a buffer that appends symbols in sequence until a lexeme is recognized.
*)
module type Buffer = sig

    (** The type of a symbol. *)
    type symbol

    (** The type of a lexeme. *)
    type lexeme

    (** The type of a buffer. *)
    type t

    (** Scanners use [create ()] to make a buffer. *)
    val create: unit -> t

    (** Scanners use [reset b] to reset [b] to contain an empty lexeme. *)
    val reset: t -> unit

    (** Scanners use [advance b c] to append [c] to [b]. *)
    val advance: t -> symbol -> unit

    (** Scanners use [lexeme b] to extract the lexeme from [b]. *)
    val lexeme: t -> lexeme
end

(** Define a module with this signature to specialize the lexical analyzer. *)
module type Basis = sig

    (** The type of a symbol. *)
    type symbol

    (** The type of a position. *)
    type position

    (** The type of a lexeme. *)
    type lexeme

    (** The type of a scanner form constructor. *)
    type +'a form

    (** The basis scanner used by the lexical analyzer. *)
    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form

    (** The scanner production form. *)
    module Form: Cf_scan.Form with type 'a t := 'a form

    (** The deterministic finite automaton for the symbol type. *)
    module DFA: Cf_dfa.Profile with type event := symbol

    (** The lexical buffer module. *)
    module Buffer: Buffer with type symbol := symbol and type lexeme := lexeme

    (** Scanners use [string_to_term s] to make a DFA term that represents the
        regular expression denoted by [s].
    *)
    val string_to_term: string -> DFA.term
end

(** The signature of a lexical analyzing scanner. *)
module type Profile = sig

    (** The type of a symbol. *)
    type symbol

    (** The type of a lexeme. *)
    type lexeme

    (** The type of a scanner form constructor. *)
    type +'a form

    (** The type of the basis scanner. *)
    type +'a t

    (** The regular syntax terms. *)
    module DFA: Cf_dfa.Regular with type event := symbol

    (** Use [string_to_term s] to make a DFA term that represents the regular
        expression denoted by [s].
    *)
    val string_to_term: string -> DFA.term

    (** Use [simple t] to make a scanner that produces lexemes matching [t]. *)
    val simple: DFA.term -> lexeme form t

    (** The type of a lexical analyzer rule. *)
    type 'a rule

    (** Use [rule t f] to make a rule to apply lexemes that match [t] to the
        production function [f] to make the value produced by the analyzer.
    *)
    val rule: DFA.term -> (lexeme form -> 'a) -> 'a rule

    (** Use [analyze s] to make a lexical analyzing scanner that produces the
        annotated value from the first rule in [s] that matches the longest
        lexeme accepted by the DFA.
    *)
    val analyze: 'a rule Seq.t -> 'a t

    (** This module contains useful affix operators. *)
    module Affix: sig

        (** Include the affix operators for the DFA module. *)
        include Cf_dfa.Affix
           with type event := symbol
            and type term := DFA.term
            and type 'r fin := 'r DFA.fin

        (** Use [!$ s] as [string_to_term s]. *)
        val ( !$ ): string -> DFA.term

        (** Use [?$$ s] as [simple (string_to_form s)]. *)
        val ( ?$$ ): string -> lexeme form t

        (** Use [t $$> f] as [rule (string_to_form s) f]. *)
        val ( $$> ): string -> (lexeme form -> 'a) -> 'a rule
    end
end

(** Use [Create(B)] to make a lexical analyzing scanner. *)
module Create(B: Basis): Profile
   with type symbol := B.symbol
    and type lexeme := B.lexeme
    and type 'a t := 'a B.Scan.t
    and type 'a form := 'a B.form

(** A distinguished lexical analyzer for simple ASCII character symbols. *)
module ASCII: Profile
   with type symbol := char
    and type lexeme := string
    and type 'a t := 'a Cf_scan.ASCII.t
    and type 'a form := 'a

(*--- End ---*)
