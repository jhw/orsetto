(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Simple octet-stream decoders. *)

(** {6 Overview}

    This module provides for safely decoding structured data in octet streams
    according to schemes composed with functional combinators.

    To decode an octet stream progressively, make the appropriate [scanner]
    class object [sxr] for the octet source, then iteratively apply decoding
    schemes to the [sxr#scan] method to produce decoded values from the
    consumed octets.

    A decoding scheme is represented internally as a 3-tuple comprising a) the
    number [sz] of octets required in the working slice to perform structural
    analysis, b) a structural analysis function [ck] described further below,
    and c) a validating decoder function [rd] that produces values when
    provided with the structural analysis returned by the [ck] function.

    Combinators are provided for composing more useful complex decoding schemes
    from simpler schemes. For example, use [pair sa sb] to compose a scheme
    that decodes [(va, vb)] where [sa] decodes [va] and [sb] decodes [vb].

    The structural analysis function [ck] in a decoding scheme is applied to
    the working slice of octets [w] to analyze the structure in preparation for
    scanning. If the [ck] function finds [w] does not yet contain the structure
    of an encoded value, then an exception must be raised. An internal
    exception is used to signal to the scanner that a valid structure may yet
    be found if the working slice is extended further with more octets to
    analyze.

    The validating decoder function [rd] in a decoding scheme is applied to the
    octet vector [s] in the working slice, and the structural analysis [x]
    returned by the [ck] function (described above) to decode and validate the
    indicated octets in [s].

    Either decoder function may use [invalid msg] (see below) to signal a fault
    to the scanner when a value cannot be decoded. It causes the scanner to
    raise the [Invalid] exception.

    When scanning encounters the end of a finite octet stream, and the decoding
    scheme structural analysis result indicates that more octets are required,
    the scanning method raises the [Incomplete] exception.
*)

(** {6 Utility} *)

(** Private representation of the size requirement of a decoded value. *)
type size = private Size of int [@@ocaml.unboxed]

(** Private representation of the stream position of a decoded value. *)
type position = private Position of int [@@ocaml.unboxed]

(** {6 Validation} *)

(** Scanning raises [Incomplete (s, n)] when a finite octet stream terminates
    and at least [n] more octets are required in the working slice [s] before a
    value can be decoded.
*)
type exn += private Incomplete of string Cf_slice.t * int

(** Scanning raises [Invalid (p, s)] to indicate the stream position [p] of the
    first octet encountered where decoding is not valid, with a diagnostic
    message [s] to describe the validation error.
*)
type exn += private Invalid of position * string

(** Private representation of an analysis result, comprising a non-negative
    count of the encoded octets and the structural analysis value.
*)
type 'x analysis = private Analysis of int * 'x

(** Use [analyze have need x] to make a structural analysis result, where
    [have] is the size of the current slice of working octets, [need] is the
    total number of octets from the analyzed structure of octets in the working
    slice that decode to the next value, and [x] is the structural analysis of
    the octets, which is provided to the [rd] function in the scheme.

    Using [analyze have need], i.e. without applying [x], raises [Incomplete]
    if [need > have], otherwise it returns with a unary constructor for the
    structural analysis.

    Raises [Invalid_argument] if either [have] or [need] is negative. A scanner
    raises [Failure] if an analysis result indicates that [need] is less than
    the minimum required octets for the decoding scheme.
*)
val analyze: size -> int -> 'x -> 'x analysis

(** Use [invalid p m] in a decoding scheme function with the position [p] in
    the octet vector where the first invalid octet is located, and
    a diagnostic message [m], to signal a validation error in decoding by
    raising an internal exception caught by the [scanner] class (see below).
*)
val invalid: position -> string -> 'a

(** Use [advance i pos] in check functions to advance [pos] by [i] positions.
    Raises [Invalid_argument] if [i < 0].
*)
val advance: int -> position -> position

(** {6 Schemes} *)

(** The type of a decoding scheme for values of the associated type. *)
type +'v scheme

(** Use [scheme sz ck rd] to make a decoding scheme for values that require
    [sz] or more octets to encode, are validated by applying [ck s p n] for the
    [n] octets in [s] starting at [p] to produce an analysis [x], then by
    applying [rd s x] for decoding the octets in [s] according to [x] as the
    scanned value.

    Raises [Invalid_argument] if [sz < 0].
*)
val scheme:
    int -> (string -> position -> size -> 'x analysis) ->
    ('x -> string -> 'v) -> 'v scheme

(** The nil scheme. Scans no octets. *)
val nil: unit scheme

(** The position scheme. Scans no octets, produces the current position. *)
val pos: position scheme

(** The any octet scheme. Scans exactly one octet. *)
val any: char scheme

(** Use [sat f] to make a decoding scheme that scans exactly one octet and
    produces it if it satisfies the predicate [f], otherwise raises [Invalid].
*)
val sat: (char -> bool) -> char scheme

(** Use [lit s] to make a decoding scheme that scans a sequence of octets
    required to be equal to [s].
*)
val lit: string -> unit scheme

(** Use [opaque n] to make a decoding scheme that scans [n] octets to produce
    a string value comprising those octets.

    Raises [Invalid_argument] if [n < 0].
*)
val opaque: int -> string scheme

(** Use [fixed_width sz rd] to make a decoding scheme for values that can be
    decoded from some sequences of exactly [sz] octets by applying [rd] to the
    working slice vector and its start index. The [rd] function may use
    [invalid pos s] where no valid decoding of the sequence exists.
*)
val fixed_width: int -> (position -> string -> 'v) -> 'v scheme

(** Use [valid_fixed_width sz rd] to make a decoding scheme for values that
    always can be decoded from any sequence of exactly [sz] octets by applying
    [rd] to the working slice vector and its start index. Compare with
    [fixed_width] which provides an index of type [position] suitable for use
    with the [invalid] function.
*)
val valid_fixed_width: int -> (string -> int -> 'v) -> 'v scheme

(** {4 Composers} *)

(** Use [ign s] to make a decoding scheme that scans a value according to [s]
    and ignores the octets comprising it.
*)
val ign: 'v scheme -> unit scheme

(** Use [opt s] to make a decoding scheme for optional values, i.e. if octets
    scanned are valid for [s], then decodes [Some v] according to [s], else if
    the octets are not valid, then decodes [None].
*)
val opt: 'v scheme -> 'v option scheme

(** Use [pair a b] to make a decoding scheme for pairs of values, which decodes
    its first value according to [a] and its second according to [b].
*)
val pair: 'a scheme -> 'b scheme -> ('a * 'b) scheme

(** Use [triple a b c] to make a decoding scheme for triples of values, which
    decodes its first value according to [a], its second according to [b], and
    its third value according to [c].
*)
val triple: 'a scheme -> 'b scheme -> 'c scheme -> ('a * 'b * 'c) scheme

(** Use [vec n s] to make a decoding scheme for fixed-length vectors of [n]
    values, with each element decoded according to [s].

    Raises [Invalid_argument] if [n < 0].
*)
val vec: int -> 'v scheme -> 'v array scheme

(** Use [seq s] to make a deconding scheme for variable-length sequences of
    values decoded according to [s]. Use [~a] to specify a minimum number of
    elements to decode. Use [~b] to specify a maximum number of elements to
    decode. Stops decoding if encounters an invalid element according to [s].
    Raises [Invalid_argument] if [a] is less than zero or [b] is less than [a].
*)
val seq: ?a:int -> ?b:int -> 'v scheme -> 'v list scheme

(** Use [map f s] to make a decoding scheme that applies [f] to each value
    decoded according to [s] to map the value into its corresponding type. The
    function [f] may call [invalid] if the map is not injective.
*)
val map: (position -> 'a -> 'b) -> 'a scheme -> 'b scheme

(** Use [ntyp n s] to create decoding scheme that encloses the value decoded by
    [s] in an untyped value with the type indicated by [n]. It is a convenient
    and efficient shortcut for [map (fun _ -> Cf_type.witness n) s].
*)
val ntyp: 't Cf_type.nym -> 't scheme -> Cf_type.opaque scheme

(** {4 Monad} *)

(** Use this monad to compose decoding schemes where intermediate values
    scanned earlier in the octet stream are used to select decoding schemes for
    the values scanned later in the stream.
 *)
module Monad: Cf_monad.Unary.Profile with type 'r t = 'r scheme

(** {6 Scanners} *)

(** Use [of_string octets scheme] to decode the octets [octets] according to
    the scheme [scheme], to return either [Some v], if [s] comprises the
    complete encoding of [v], or otherwise returns [None].
*)
val of_string: 'v scheme -> string -> 'v option

(** Use [of_slice octets scheme] to decode the octets [octets] according to
    the scheme [scheme], to return either [Some v], if [s] comprises the
    complete encoding of [v], or otherwise returns [None].
*)
val of_slice: 'v scheme -> string Cf_slice.t -> 'v option

(** The class of imperative octet stream scanners. Use [new scanner ()] to make
    a basic scanner object that can progressively decode values from a working
    slice of octets. Use [inherit scanner ()] to derive a subclass that
    implements more refined behavior by overriding private methods to
    manipulate the working slice and the cursor position. Use the [~start]
    parameter to initialize the starting position counter to a number other
    than zero. (See documentation below for the various private members.)
*)
class scanner: ?start:position -> unit -> object

    (** The working slice. *)
    val mutable slice_: string Cf_slice.t

    (** The cursor index into the working slice. *)
    val mutable cursor_: int

    (** The window index into the working slice. *)
    val mutable window_: int

    (** Position of slice start. *)
    val mutable start_: position

    (** Use [self#work slice] to set [slice] as the working slice and set
        [cursor_] to the start of [slice].
    *)
    method private work: string Cf_slice.t -> unit

    (** Use [self#octets] to make a slice comprising the octets after the
        cursor index in the working slice.
    *)
    method private octets: string Cf_slice.t

    (** A successful scan calls [self#advance n] to signal that a value was
        decoded from the [n] octets in the working slice at the cursor. The
        basis implementation increments both [cursor_] and [start_] by [n]
        octets.
    *)
    method private advance: int -> unit

    (** If scanning a value requires at least [n] more octets to validate than
        are currently available in the working slice after the cursor, the
        basis implementation of [self#scan] applies [self#incomplete n] as
        necessary to update the working slice and cursor position until the
        working slice is large enough to decode a valid result.

        The basis implementation raises [Incomplete]. A derived class may
        extend the current working slice, and also shift already consumed
        octets from the head of the working slice, provided that it adjusts the
        start position accordingly.
    *)
    method private incomplete: int -> unit

    (** When preparing the working slice to decode according to [s], the base
        implementation of [self#scan] applies [self#acquire sz ck] to acquire
        enough octets in the working slice after the check cursor to use safely
        with the [rd] function in [s].

        To accomplish this, it progressively calls [self#incomplete] as
        necessary while there are not enough octets in the working slice.

        First, while [sz] is more than the length of the working slice, or if
        [Incomplete] is raised by applying [ck start_ self#octets] indicating a
        decoded length more than the length of the working slice, then
        [self#incomplete n] is applied to give the subclass a method of
        extending the working slice with [n] more octets of input, then if no
        exception is raised, the above procedure is repeated. Otherwise,
        returns the analysis result of the [ck] function.

        The internal signaling exceptions for incomplete structure and
        validation error must never be raised to callers of this method.

        Raises [Failure] if applying [ck] returns an analysis result with a
        decoding length that falls outside the working slice provided.
    *)
    method private acquire:
        'x. int -> (string -> position -> size -> 'x analysis) -> 'x analysis

    (** When a decoding scheme calls the [invalid i m] function (see above),
        the scanner catches the internal exception and invokes this method to
        locate and validate the stream position [p] corresponding to [i] and
        raise [Invalid (p, m)] accordingly. Raises [Failure] if [i] is not an
        index into the current working slice.
    *)
    method private invalid: 'a. position -> string -> 'a

    (** Use [sxr#scan s] to decode according to [s] zero or more octets at the
        read cursor in the working slice.

        To accomplish this, it first applies [self#acquire s] to acquire enough
        working octets to decode a value and either returns [Analysis (n, x)]
        or raises an exception left for the caller to catch. The [rd] function
        in the decoding scheme is applied to the string value in the working
        slice and the intermediate result [x] to obtain the validated result of
        the scan. If no exception is raised by the [rd] function, then applies
        [self#advance n] and returns the scan result.

        No exception raised by the [self#acquire] or [self#advance] methods, or
        raised by the [rd] function in the decoding scheme, is caught. The
        [Invalid] exception is provided for decoding schemes to signal that no
        valid value can be decoded in the [rd] function.
    *)
    method scan: 'v. 'v scheme -> 'v

    (** Use [sxr#position] to get the total number of valid octets ever
        scanned by [sxr].
    *)
    method position: position
end

(** Use [string_scanner ?start s] to make a scanner that decodes values
    progressively from the string [s] and raises [Incomplete] when the
    remaining octets in the string are insufficient to decode a value. Use the
    [~start] parameter to initialize the starting position to a number other
    than zero.
*)
val string_scanner: ?start:position -> string -> scanner

(** Use [slice_scanner ?start sl] to make a scanner that decodes values
    progressively from the string slice [sl] and raises [Incomplete] when the
    remaining octets in the slice are insufficient to decode a value. Use the
    [~start] parameter to initialize the starting position to a number other
    than zero.
*)
val slice_scanner: ?start:position -> string Cf_slice.t -> scanner

(** Use [chars_scanner ?limit s] to make a scanner that decodes values
    progressively from the character sequence [s].  Use [~limit] to make the
    [scan] method raise [Failure] if the size requirement for a scanned value
    is larger than [limit]. Raises [Incomplete] when the end of the sequence is
    reached and the remaining octets are insufficient to decode a value. Use
    the [~start] parameter to initialize the starting position to a number
    other than zero.
*)
val chars_scanner: ?start:position -> ?limit:int -> char Seq.t -> scanner

(** Use [channel_scanner ?limit c] to make a scanner that decodes values
    progressively from the input channel [c].  Use [~limit] to make the [scan]
    method raise [Failure] if the size requirement for a scanned value is
    larger than [limit]. Raises [Incomplete] when the end of the file is
    reached and the remaining octets are insufficient to decode a value. Use
    the [~start] parameter to initialize the starting position to a number
    other than zero.
*)
val channel_scanner: ?start:position -> ?limit:int -> in_channel -> scanner

(** {6 Streaming Decode} *)

(** Use [scanner_to_vals sxr s] to make a contextually limited volatile
    sequence enclosing [sxr], which produces each progressive application of
    [sxr#scan s]. The sequence terminates when the scan method raises
    [Incomplete] with an empty working slice.
*)
val scanner_to_vals: 'v scheme -> #scanner -> 'v Seq.t

(** Use [string_to_vals ?start sch str] to make a concurrently persistent
    sequence of values decoded progressively from [str] according to [sch].
    Consuming raises [Invalid] or [Incomplete] as necessary. The sequence
    terminates if no values can be decoded according to [sch] with an empty
    slice and the end of [str] is reached. If [~start] is provided then it
    specifies the starting position of the first octet in [str]. Otherwise, the
    starting position is zero.
*)
val string_to_vals: ?start:position -> 'v scheme -> string -> 'v Seq.t

(** Use [slice_to_vals ?start sch sl] to make a concurrently persistent
    sequence of values decoded progressively from [sl] according to [sch].
    Consuming raises [Invalid] or [Incomplete] as necessary. The sequence
    terminates if no values can be decoded according to [sch] with an empty
    slice and the end of [sl] is reached. If [~start] is provided then it
    specifies the starting position of the first octet in [sl]. Otherwise, the
    starting position is zero
*)
val slice_to_vals:
    ?start:position -> 'v scheme -> string Cf_slice.t -> 'v Seq.t

(** Use [chars_to_vals ?start ?limit sch seq] as an abbreviation for
    [scanner_to_vals sch (chars_scanner ?start ?limit seq)].
*)
val chars_to_vals:
    ?start:position -> ?limit:int -> 'v scheme -> char Seq.t -> 'v Seq.t

(*--- End ---*)
