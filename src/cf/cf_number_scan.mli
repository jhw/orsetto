(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Parser combinators for numeric types. *)

(** {6 Overview}

    This module implements functional combinators for parsing common numeric
    types in typical textual presentations.
*)

(** {6 Types and Constants}

    In this section various utility types are defined for use in specializing
    the general number scanning engine.
*)

(** The type of available radixes. *)
type radix = private Radix of int [@@caml.unboxed]

(** The type of a scanned digit value *)
type digit = Digit of int [@@caml.unboxed]

(** The binary (base 2) radix. *)
val binary: radix

(** The octal (base 8) radix. *)
val octal: radix

(** The decimal (base 10) radix. *)
val decimal: radix

(** The hexadecimal (base 16) radix. *)
val hexadecimal: radix

(** The type qualifier for integer scanning options. *)
type iopt

(** The type qualifier for floating point scanning options. *)
type fopt

(** The generalized algebraic data type (GADT) representing scanning options
    qualified by option type.
*)
type _ opt =
    | O_radix: radix -> iopt opt
    | O_leading: iopt opt
    | O_signed: iopt opt
    | O_width: int -> 'a opt
    | O_scientific: fopt opt

(** The generalized algebraic data type (GADT) representing return value types
    qualified by option type.
*)
type (_, _) ret =
    | R_int: (int, iopt) ret
    | R_int32: (int32, iopt) ret
    | R_int64: (int64, iopt) ret
    | R_native: (nativeint, iopt) ret
    | R_float: (float, fopt) ret

(** The abstract type representing scanner controllers comprising a return type
    and the option qualifiers.
*)
type 'r ctrl

(** Use [ctrl ?opt ret] to make a scanner control that for the return type
    represented by [ret]. If [~opt] is used then it specifies a list of
    options for scanning the input stream accordingly.
*)
val ctrl: ?opt:'x opt list -> ('r, 'x) ret -> 'r ctrl

(** {6 Functor} *)

(** Define a module of this type to specialize the number scanner for specific
    underlying symbol types.
*)
module type Basis = sig

    (** The symbol type. *)
    type symbol

    (** The position type. *)
    type position

    (** The value form type. *)
    type +'a form

    (** The basic scanner module for the symbol type. *)
    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form

    (** The scanner production form. *)
    module Form: Cf_scan.Form with type 'a t := 'a form

    (** Calls [digitsym base sym] to convert [sym] to its digit value under
        [base]. If [sym] is not a digit under the radix [base], then returns
        a digit less than zero.
    *)
    val digitsym: radix -> symbol -> digit

    (** Tokenizing function for recognizing a positive or negative sign.
        Returns either 1 or (-1) to represent the sign recognized.
    *)
    val signtok: symbol -> int option

    (** Satisfier for decimla point characters. *)
    val pointsat: symbol -> bool

    (** Satisfier for exponent symbols in scientific notation. *)
    val expsat: symbol -> bool
end

(** The signature of a general number scanner module. *)
module type Profile = sig

    (** The input type of the scanner form. *)
    type +'a form

    (** The input type of the basic scanner. *)
    type +'a t

    (** Use [special ctrl] to make a scanner that recognizes input and returns
        a result according to [ctrl].
    *)
    val special: 'r ctrl -> 'r form t

    (** The integer scanner. Recognizes an OCaml integer in decimal format
        without any leading zeros.
    *)
    val signed_int: int form t

    (** The 32-bit integer scanner. Recognizes a 32-bit integer in decimal
        format without any leading zeros.
    *)
    val signed_int32: int32 form t

    (** The 64-bit integer scanner. Recognizes a 64-bit integer in decimal
        format without any leading zeros.
    *)
    val signed_int64: int64 form t

    (** The native integer scanner. Recognizes a native system integer in
        decimal format without any leading zeros.
    *)
    val signed_native: nativeint form t

    (** The simple floating point number scanner. Recognizes an OCaml floating
        point number without any exponent part, i.e. a simple integer part
        without leading zeros, optionally followed by a decimal point and a
        fraction part.
    *)
    val simple_float: float form t

    (** The scientific notation floating point number scanner. Recognizes an
        OCaml floating point number that may be in scientific notation, i.e. an
        integer part, optional followed by a decimal point and a fraction part,
        optionally followed again by an exponent part.
    *)
    val scientific_float: float form t
end

(** Use [Create(B)] to specialize the number scanner according to [B]. *)
module Create(B: Basis): Profile
   with type 'a t := 'a B.Scan.t
    and type 'a form := 'a B.form

(** A distinguished number scanner for simple ASCII character symbols. *)
module ASCII: Profile
   with type 'a t := 'a Cf_scan.ASCII.t
    and type 'a form := 'a

(*--- End ---*)
