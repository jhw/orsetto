/*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/

#include "cf_endian_little_aux.h"

#include <stdio.h>

/*---------------------------------------------------------------------------*
  module Unsafe
 *---------------------------------------------------------------------------*/

CAMLprim value
cf_endian_lds16l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    int16_t n = (int16_t) cf_endian_ld16l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim value
cf_endian_ldu16l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint16_t n = cf_endian_ld16l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim value
cf_endian_lds32l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    int32_t n = (int32_t) cf_endian_ld32l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim value
cf_endian_ldu32l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint32_t n = cf_endian_ld32l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim value
cf_endian_lds64l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    int64_t n = (int64_t) cf_endian_ld64l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim value
cf_endian_ldu64l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint64_t n = cf_endian_ld64l(p);
    CAMLreturn(Val_int(n));
}

CAMLprim uint32_t
cf_endian_ldi32l_unsafe_fast(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    CAMLreturn(cf_endian_ld32l(p));
}

CAMLprim value
cf_endian_ldi32l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    uint32_t n = cf_endian_ldi32l_unsafe_fast(vOctets, vPos);
    CAMLreturn(caml_copy_int32(n));
}

CAMLprim uint64_t
cf_endian_ldi64l_unsafe_fast(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    const uint8_t* p = (const uint8_t*) String_val(vOctets) + Long_val(vPos);
    CAMLreturn(cf_endian_ld64l(p));
}

CAMLprim value
cf_endian_ldi64l_unsafe(value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    uint64_t n = cf_endian_ldi64l_unsafe_fast(vOctets, vPos);
    CAMLreturn(caml_copy_int64(n));
}

CAMLprim void
cf_endian_sti16l_unsafe(value vInt, value vOctets, value vPos)
{
    CAMLparam3(vInt, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint16_t n = Long_val(vInt);
    cf_endian_st16l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sts32l_unsafe(value vInt, value vOctets, value vPos)
{
    CAMLparam3(vInt, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    int32_t n = Long_val(vInt);
    cf_endian_st32l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_stu32l_unsafe(value vInt, value vOctets, value vPos)
{
    CAMLparam3(vInt, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint32_t n = Long_val(vInt);
    cf_endian_st32l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sts64l_unsafe(value vInt, value vOctets, value vPos)
{
    CAMLparam3(vInt, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    int64_t n = Long_val(vInt);
    cf_endian_st64l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_stu64l_unsafe(value vInt, value vOctets, value vPos)
{
    CAMLparam3(vInt, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint64_t n = Long_val(vInt);
    cf_endian_st64l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sti32l_unsafe_fast(uint32_t n, value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    cf_endian_st32l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sti32l_unsafe(value vInt32, value vOctets, value vPos)
{
    CAMLparam3(vInt32, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint32_t n = (uint32_t) Int32_val(vInt32);
    cf_endian_st32l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sti64l_unsafe_fast(uint64_t n, value vOctets, value vPos)
{
    CAMLparam2(vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    cf_endian_st64l(p, n);
    CAMLreturn0;
}

CAMLprim void
cf_endian_sti64l_unsafe(value vInt64, value vOctets, value vPos)
{
    CAMLparam3(vInt64, vOctets, vPos);
    uint8_t* p = (uint8_t*) String_val(vOctets) + Long_val(vPos);
    uint64_t n = (uint64_t) Int64_val(vInt64);
    cf_endian_st64l(p, n);
    CAMLreturn0;
}

/*--- $File: src/cf/cf_endian_little_aux.c $ ---*/
