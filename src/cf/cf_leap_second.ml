(*---------------------------------------------------------------------------*
  $Change$
  Copyright (C) 2021-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(*---
  This data is copied manually copied from a "recent" copy of the U.S. NIST
  leap seconds archive.

    <https://www.ietf.org/timezones/data/leap-seconds.list>
    <ftp://ftp.nist.gov/pub/time/leap-seconds.list>

  TODO(ORS-100): Generate this automatically from a downloaded copy.
  ---*)
let embedded_ =
    3676924800L,    (* Updated:  8 Jul 2016 *)
    3865363200L,    (* Expires: 28 Jun 2022 *)
    [
        2272060800L, 10;    (*  1 Jan 1972 *)
        2287785600L, 11;    (*  1 Jul 1972 *)
        2303683200L, 12;    (*  1 Jan 1973 *)
        2335219200L, 13;    (*  1 Jan 1974 *)
        2366755200L, 14;    (*  1 Jan 1975 *)
        2398291200L, 15;    (*  1 Jan 1976 *)
        2429913600L, 16;    (*  1 Jan 1977 *)
        2461449600L, 17;    (*  1 Jan 1978 *)
        2492985600L, 18;    (*  1 Jan 1979 *)
        2524521600L, 19;    (*  1 Jan 1980 *)
        2571782400L, 20;    (*  1 Jul 1981 *)
        2603318400L, 21;    (*  1 Jul 1982 *)
        2634854400L, 22;    (*  1 Jul 1983 *)
        2698012800L, 23;    (*  1 Jul 1985 *)
        2776982400L, 24;    (*  1 Jan 1988 *)
        2840140800L, 25;    (*  1 Jan 1990 *)
        2871676800L, 26;    (*  1 Jan 1991 *)
        2918937600L, 27;    (*  1 Jul 1992 *)
        2950473600L, 28;    (*  1 Jul 1993 *)
        2982009600L, 29;    (*  1 Jul 1994 *)
        3029443200L, 30;    (*  1 Jan 1996 *)
        3076704000L, 31;    (*  1 Jul 1997 *)
        3124137600L, 32;    (*  1 Jan 1999 *)
        3345062400L, 33;    (*  1 Jan 2006 *)
        3439756800L, 34;    (*  1 Jan 2009 *)
        3550089600L, 35;    (*  1 Jul 2012 *)
        3644697600L, 36;    (*  1 Jul 2015 *)
        3692217600L, 37;    (*  1 Jan 2017 *)
    ]

type entry = Entry of {
    epoch: Cf_tai64.t;
    offset: int;
}

type archive = Archive of {
    current: int;
    updated: Cf_tai64.t;
    expires: Cf_tai64.t;
    history: entry list;
}

module Archive = struct
    let create_from_nist_data =
        let ntp_epoch =
            Cf_tai64.(add_int64 first @@ Int64.of_string "0x3fffffff7c55818a")
        in
        let of_ntp_timestamp utc_offset ntp_timestamp =
            Cf_tai64.(add (add_int64 ntp_epoch ntp_timestamp) utc_offset)
        in
        let rec loop current adjust history = function
            | (_, next) :: tl when current = next ->
                (loop[@tailcall]) current adjust history tl
            | (leap, next) :: tl ->
                let epoch = of_ntp_timestamp adjust leap and offset = current in
                let history = Entry { epoch; offset } :: history in
                let adjust = adjust + (next - current) in
                (loop[@tailcall]) next adjust history tl
            | [] ->
                current, history
        in
        let enter updated expires history =
            let current, history = loop 10 0 [] history in
            let expires = of_ntp_timestamp current expires in
            let updated = of_ntp_timestamp current updated in
            Cf_tai64.set_current_offset_from_utc current;
            Archive { current; updated; expires; history }
        in
        enter

    let singleton =
        let updated, expires, history = embedded_ in
        ref (create_from_nist_data updated expires history)

    let load_from_nist_file =
        let rec loop w3 cin =
            match Stdlib.input_line cin with
            | exception End_of_file ->
                let updated, expires, history = w3 in
                updated, expires, List.rev history
            | line ->
                if String.length line < 1 then
                    loop w3 cin
                else if line.[0] = '#' then begin
                    if String.length line < 2 then
                        loop w3 cin
                    else if line.[1] = '$' then begin
                        let _, expires, history = w3 in
                        let updated = Scanf.sscanf line "#$ %Lu" Fun.id in
                        loop (updated, expires, history) cin
                    end
                    else if line.[1] = '@' then begin
                        let updated, _, history = w3 in
                        let expires = Scanf.sscanf line "#@ %Lu" Fun.id in
                        loop (updated, expires, history) cin
                    end
                    else
                        loop w3 cin
                end
                else begin
                    let updated, expires, history = w3 in
                    let epoch, offset =
                        Scanf.sscanf line "%Lu %d" (fun leap adj -> leap, adj)
                    in
                    let w3 = updated, expires, (epoch, offset) :: history in
                    loop w3 cin
                end
        in
        let enter filename =
            let cin = Stdlib.open_in filename in
            let updated, expires, history = loop (0L, 0L, []) cin in
            Stdlib.close_in cin;
            let archive = create_from_nist_data updated expires history in
            let Archive current = !singleton and Archive next = archive in
            if next.current < 10 || next.expires < current.expires then
                failwith "Cf_leap_second.load_from_nist_file: invalid data!";
            singleton := archive
        in
        enter
end

let current () = !Archive.singleton

let load_from_nist_file = Archive.load_from_nist_file

let search =
    let rec loop mark offset = function
        | Entry hd :: tl when Cf_tai64.compare hd.epoch mark < 0 ->
            (loop[@tailcall]) mark hd.offset tl
        | Entry hd :: _ when Cf_tai64.compare hd.epoch mark = 0 ->
            true, offset
        | Entry _ :: _
        | [] ->
            false, offset
    in
    fun mark ->
        let Archive a = current () in
        let leap, offset = loop mark a.current a.history in
        leap, offset

(*--- $File$ ---*)
