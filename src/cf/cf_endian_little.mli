(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Utility functions for little-endian byte order. *)

(** {6 Overview}

    Primitive functions for ingesting and rendering integers and floating
    point numbers to and from subranges of strings and byte sequences using
    the "little-endian" explicit octet ordering discipline.
*)

(** Module inclusion *)
include Cf_endian_core.Safe

(** The functions in this module offer fast unsafe operations that provide
    correct behavior only when the parameters are valid, i.e. when the position
    and length are valid for the string or byte sequence, and when integer and
    float quantities are valid for the specific external representation.
*)
module Unsafe: sig
    include module type of Cf_endian_core.Unsafe_octet

    val descript: string

    external lds16: string -> int -> int =
        "cf_endian_lds16l_unsafe" [@@noalloc]

    external ldu16: string -> int -> int =
        "cf_endian_ldu16l_unsafe" [@@noalloc]

    external lds32: string -> int -> int =
        "cf_endian_lds32l_unsafe" [@@noalloc]

    external ldu32: string -> int -> int =
        "cf_endian_ldu32l_unsafe" [@@noalloc]

    external lds64: string -> int -> int =
        "cf_endian_lds64l_unsafe" [@@noalloc]

    external ldu64: string -> int -> int =
        "cf_endian_ldu64l_unsafe" [@@noalloc]

    external ldi32: string -> int -> (int32 [@unboxed]) =
        "cf_endian_ldi32l_unsafe" "cf_endian_ldi32l_unsafe_fast" [@@noalloc]

    external ldi64: string -> int -> (int64 [@unboxed]) =
        "cf_endian_ldi64l_unsafe" "cf_endian_ldi64l_unsafe_fast" [@@noalloc]

    external ldi32_boxed: string -> int -> int32 = "cf_endian_ldi32l_unsafe"
    external ldi64_boxed: string -> int -> int64 = "cf_endian_ldi64l_unsafe"

    external sts16: int -> bytes -> int -> unit =
        "cf_endian_sti16l_unsafe" [@@noalloc]

    external stu16: int -> bytes -> int -> unit =
        "cf_endian_sti16l_unsafe" [@@noalloc]

    external sts32: int -> bytes -> int -> unit =
        "cf_endian_sts32l_unsafe" [@@noalloc]

    external stu32: int -> bytes -> int -> unit =
        "cf_endian_stu32l_unsafe" [@@noalloc]

    external sts64: int -> bytes -> int -> unit =
        "cf_endian_sts64l_unsafe" [@@noalloc]

    external stu64: int -> bytes -> int -> unit =
        "cf_endian_stu64l_unsafe" [@@noalloc]

    external sti32: (int32 [@unboxed]) -> bytes -> int -> unit =
        "cf_endian_sti32l_unsafe" "cf_endian_sti32l_unsafe_fast" [@@noalloc]

    external sti64: (int64 [@unboxed]) -> bytes -> int -> unit =
        "cf_endian_sti64l_unsafe" "cf_endian_sti64l_unsafe_fast" [@@noalloc]

    external sti32_boxed: int32 -> bytes -> int -> unit =
        "cf_endian_sti32l_unsafe" [@@noalloc]

    external sti64_boxed: int64 -> bytes -> int -> unit =
        "cf_endian_sti64l_unsafe" [@@noalloc]
end

(*--- End ---*)
