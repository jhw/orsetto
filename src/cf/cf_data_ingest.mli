(*---------------------------------------------------------------------------*
  Copyright (C) 2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Ingesting data according to an abstract model. *)

(** {6 Overview}

    This module facilitates the ingestion of data conforming to an abstract
    model using any interchange language provided with a basis module defining
    the scanner type and some basic scanners and scanner composers.

    Data models are functionally composable. Optional affix convenience
    operators are provide an indiomatic syntax.

    With a fully composed data models and the ingest specialization of an
    interchange language, you produce a scanner that recognizes well-typed
    values according to the corresponding model.

    A data model represents either the "primitive" type of a scalar value or
    a "composed" type of a group of values.

    A primitive model represents an OCaml value identified by a [Cf_type.nym].
    Some nyms are conventionally accepted by every interchange language in the
    Orsetto distribution:

    - [Cf_type.Unit]
        A type with exactly one value, e.g. the JSON "null" value.

    - [Cf_type.Bool]
        A boolean value type.

    - [Cf_type.Int]
        An integer value in the range that an OCaml {int} can represent.

    - [Cf_type.Int32]
        An integer value in the range that an OCaml {int32} can represent.

    - [Cf_type.Int64]
        An integer value in the range that an OCaml {int64} can represent.

    - [Cf_type.Float]
        An IEEE 754 floating point value in the range that an OCaml {float} can
        represent.

    - [Cf_type.String]
        An octet string, not necessarily associated with a particular encoding.

    - [Cf_type.Opaque]
        An opaque value conventionally representing any scalar or group value
        in the underlying interchange language. (Note well: opaque values may
        contain interchange language specific elements that cannot expressed in
        other interchange languages.)

    A group model takes one of the following forms:

    - Vector
        A sequence of zero, one or more elements all of with the same model.

    - Table
        A sequence of zero, one or more pairs, comprising a domain model and a
        codomain model, where the domain model corresponds to a totally
        ordered type and no two pairs in the sequence share the same domain
        value.

    - Record
        A sequence of zero, one or more elements, each of which may have a
        different model indicated by its position in the sequence.

    - Structure
        A sequence of zero, one or more pairs, comprising an index model and a
        field model, where the index model corresponds to a totally ordered
        type, the field model is indicated by the value of the index, and no
        two pairs in the sequence share the same index value.

    - Variant
        Exactly one pair, comprising an index model and a field mode, where the
        field model is indicated by the value of the index.
*)

(** {6 Generalized Data Models}

    The types and values in this section facilitate the composition of abstract
    data models for use in constructing scanners with interchange languages
    for which an ingest specialization is defined.
*)

(** The type constructor ['v model] represents the ingestion of ['v] values
    decoded from some interchange language.
*)
type 'v model

(** Use [primitive nym] to compose a model that corresponds to all the values
    of the type associated with [nym].
*)
val primitive: 'v Cf_type.nym -> 'v model

(** Use [cast f m] to compose a model that corresponds to the values produced
    by applying [f] to the values recognized by scanners for the model [m]. If
    applying [f] raises [Not_found], then the scanner recognizes no value. If
    [f] raises [Failure], then the exception is converted into a [Bad_syntax]
    exception from the scanner.
*)
val cast: ('a -> 'b) -> 'a model -> 'b model

(** Use [defer m] to compose a model by forcing [m] as needed. This is useful
    for composing recursive data models to be used in the array arguments of
    the [choice] and [variant] model constructors (see below).
*)
val defer: 'v model Lazy.t -> 'v model

(** Use [choice a] to compose a model that corresponds to the values produced
    by the first scanner in the array of scanners corresponding to the models
    in [a].
*)
val choice: 'v model array -> 'v model

(** Use [vector m] to compose a model that corresponds to a variable length
    array of values recognized by the scanner for the model [m]. Use the
    optional [~a] and [~b] arguments to constrain the length of the vector to a
    minimum of [a] and/or a maximum of [b].
*)
val vector: ?a:int -> ?b:int -> 'v model -> 'v array model

(** Use [new index nym] to create an index object for the type indicated by
    [nym] required for composing table, structure and variant group models. Use
    the optional [~cmp] argument to specify a concrete comparator function to
    use instead of [Stdlib.( = )]. Use the optional [~model] argument to
    specify the model explicitly, otherwise the primitive model for [nym] is
    used for the domain or index in the compoed group model.
*)
class ['key] index:
    ?cmp:('key -> 'key -> int) -> ?model:'key model -> 'key Cf_type.nym ->
    object('self)
        (** The total order relation for the key. *)
        inherit ['key] Cf_relations.Extensible.order

        (** The type nym for the key. *)
        method nym: 'key Cf_type.nym

        (** The ingest model for the key. *)
        method model: 'key model
    end

(** Use [table k v] to compose a model that corresponds to a variable length
    array of pairs comprising a domain value of the type described by the
    index [k] and a codomain value of the type described by the model [v].
    Use the optional [~a] and [~b] arguments to constrain the length of the
    table to a minimum of [a] and/or a maximum of [b].
*)
val table: ?a:int -> ?b:int -> 'k #index -> 'v model -> ('k * 'v) array model

(** The abstract type parameter for group elements where the position in the
    sequence indicates the model for the element.
*)
type element

(** The type constructor [\['g, 'f, 'v\] group] represents the composition of
    a group with fields of disparate model in each element on the sequence. The
    ['g] parameter is either [element] for record groups or ['k index] for the
    other cases. The ['f] parameter indicates the type of the composer function
    that produces the final value from its component parts. The ['v] parameter
    indicates the type of the value produced.
*)
type ('g, 'f, 'v) group

(** The type constructor [\['g, 'p\] bind] represents the model of a specific
    element in a group with fields or elements of disparate type. In the case
    where type ['g] is [element], the type ['p] corresponds to the type
    indicated by the position in the sequence. In the case where type ['g] is
    ['k index], the type ['p] corresponds to the type indicated by its index
    value.
*)
type ('g, 'p) bind

(** The empty group that comprises the composition of a value. *)
val nil: ('g, 'v, 'v) group

(** Use [bind b g] to prepend the group element [b] to the group [g]. *)
val bind: ('g, 'p) bind -> ('g, 'f, 'r) group -> ('g, 'p -> 'f, 'r) group

(** A submodule containing composers for elements indexed by position. *)
module Element: sig

    (** Use [required m] to compose a group element that requires a value
        matching the model [m] to be present in the sequence at the position at
        which it is bound in the group.
    *)
    val required: 'v model -> (element, 'v) bind

    (** Use [optional m] to compose a group element that admits an optional
        value matching the model [m] if present in the sequence at the position
        at which it is bound in the group.
    *)
    val optional: 'v model -> (element, 'v option) bind

    (** Use [default m v] to compose a group element that admits an optional
        value matching the model [m] if present in the sequence at the position
        at which it is bound in the group, and if not present, then the scanner
        produces the default [v] for the element.
    *)
    val default: 'v model -> 'v -> (element, 'v) bind
end

(** Use [record g f] to compose a data model for a record consisting of the
    elements described by [g] and the composer [f], which is applied to the
    values scanned in the sequence to produce the result.
*)
val record: (element, 'f, 'v) group -> 'f -> 'v model

(** A submodule containing composers for elements indexed by some totally
    ordered domain type.
*)
module Field: sig

    (** Use [required k m] to compose a group element that requires a value
        matching the model [m] to be indexed in the sequence with [k].
    *)
    val required: 'k -> 'v model -> ('k index, 'v) bind

    (** Use [optional k m] to compose a group element that admits an optional
        value matching the model [m] if it is indexed in the sequence with [k].
    *)
    val optional: 'k -> 'v model -> ('k index, 'v option) bind

    (** Use [default k m v] to compose a group element that admits an optional
        value matching the model [m] if it is indexed in the sequence with [k],
        and if it is not present, then the scanner produces the default [v] for
        the element.
    *)
    val default: 'k -> 'v model -> 'v -> ('k index, 'v) bind
end

(** Use [structure i g f] to compose a data model for a structure consisting of
    the fields indexed by [i] as described by [g], with the composer [f], which
    is applied to the values scanned in the sequence to produce the result.
*)
val structure: 'k #index -> ('k index, 'f, 'v) group -> 'f -> 'v model

(** Use [variant i s] to compose a data model for a variant indexed by [i] with
    cases described by [s]. Raises [Invalid_argument] if more than one pair
    [k, m] in [s] has the same value of [k]. The model [m] describes how the
    field value is scanned in the case where the index value is [k].
*)
val variant: 'k #index -> ('k * 'v model) array -> 'v model

(** A submodule containing affix operators the provide a convenient and
    idiosyncratic syntax for composing record and structure models.
*)
module Affix: sig

    (** Use [!: m] as an abbreviation of [Element.required m]. *)
    val ( !: ): 'v model -> (element, 'v) bind

    (** Use [!? m] as an abbreviation of [Element.optional m]. *)
    val ( !? ): 'v model -> (element, 'v option) bind

    (** Use [m $= v] as an abbreviation of [Element.default m v]. *)
    val ( $= ): 'v model -> 'v -> (element, 'v) bind

    (** Use [k %: m] as an abbreviation of [Field.required k m]. *)
    val ( %: ): 'k -> 'v model -> ('k index, 'v) bind

    (** Use [k %? m] as an abbreviation of [Field.default k m]. *)
    val ( %? ): 'k -> 'v model -> ('k index, 'v option) bind

    (** Use [k %= (m, v)] as an abbreviation of [Field.default k m v]. *)
    val ( %= ): 'k -> ('v model * 'v) -> ('k index, 'v) bind

    (** Use [m /= f] as an abbreviation of [cast f m] *)
    val ( /= ): 'a model -> ('a  -> 'b) -> 'b model

    (** Use [b @> g] as an abbreviation of [bind b g]. Note well: this operator
        is right-associative to facilitate an idiosyncratic usage. Group
        expressions like [b1 @> b2 @> b3 @> nil] comprise a triple of three
        elements in the sequence that reads from left to right.
    *)
    val ( @> ): ('g, 'p) bind -> ('g, 'f, 'r) group -> ('g, 'p -> 'f, 'r) group
end

(** {6 Specialization}

    Usage of generalized data models with a specific interchange language
    entails composing an ingest specialization for it. The types and values in
    this section are provided for that purpose.
*)

(** The extensible control type. Facilitates composing data models with
    features extended by the ingest specialization for unique features of the
    interchange language. A value of type [('a, 'b) control] represents a
    conversion of a model of type ['a model] to a model of type ['b model] as a
    side effect of any additional constraints applied.
*)
type (_, _) control = ..

(** A control value [Cast f] represents the operation of applying [f] to a
    scanned value to produce that value admitted by the model.
*)
type (_, _) control += private Cast: ('a -> 'b) -> ('a, 'b) control

(** Use [control m c] to compose a new model comprising the application of [c]
    to the model [m].
*)
val control: 'a model -> ('a, 'b) control -> 'b model

(** Values of type ['k pair] are used to describe the structure of pair
    elements in sequences that comprise group data models indexed by values of
    type ['k].
*)
type 'k pair = [
    | `Table of 'k Cf_type.nym
    | `Structure of 'k Cf_type.nym
    | `Variant of 'k Cf_type.nym
]

(** Values of type ['k container] are used to describe the structure of
    sequences that comprise group data models.
*)
type 'k container = [ `Vector | `Record | 'k pair ]

(** Values of type [occurs] describe the optional minimum and maximum number
    of elements contained by a group data model.
*)
type occurs = private Occurs of { a: int option; b: int option }

(** Use [occurs ()] to construct a value of type [occurs] with validated
    content. Use [~a] to specify a non-negative minimum number of elements.
    Use [~b] to specify a non-negative maximum number of elements. Raises
    [Invalid_argument] if either [a] or [b] is negative, or if [a > b].
*)
val occurs: ?a:int -> ?b:int -> unit -> occurs

(** Define a module of type [Basis] to create an ingest specialization. *)
module type Basis = sig

    (** The interchange language scanner symbol type. *)
    type symbol

    (** The interchange language scanner position type. *)
    type position

    (** Encapsulation of a sequence comprising a group of kind ['k]. *)
    type 'k frame

    (** The form module for interchange language scanners. *)
    module Form: Cf_scan.Form

    (** The interchange language scanner module. *)
    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a Form.t

    (** The scanner compiler uses [primitive n] to obtain a scanner for the
        primitive type named by [n].
    *)
    val primitive: 'a Cf_type.nym -> 'a Form.t Scan.t

    (** The scanner compiler uses [control s c] to compose a scanner
        representing the application of control [c] to the value recognized by
        the scanner [s].
    *)
    val control: 'a Form.t Scan.t -> ('a, 'b) control -> 'b Form.t Scan.t

    (** The scanner compiler uses [start c q] to compose a scanner that
        recognizes the start of a sequence of kind [c] with length limited by
        [q] and produces an abstract frame value annotated with the location
        of the start in the input stream.
    *)
    val start: 'k container -> occurs -> 'k frame Form.t Scan.t

    (** The scanner compiler uses [visit c w f v] to compose a scanner that
        visits with [f] every element in a sequence of kind [c] in the context
        [w]. The first element is visited with the initializer [v], and each
        following element is visited with the value returned by the preceding
        scanner.
    *)
    val visit: 'k container -> 'k frame -> ('r -> 'r Scan.t) -> 'r -> 'r Scan.t

    (** The scanner compiler uses [finish w] to compose a scanner that
        recognizes the end of the sequence that started upon producing [w].
    *)
    val finish: 'k frame -> unit Form.t Scan.t

    (** The scanner compiler uses [pair c k v] to compose a scanner that
        recognizes a pair of kind [c] comprising a first item recognized by [k]
        and a second item recognized by [v].
    *)
    val pair:
        'k pair -> 'k Form.t Scan.t -> 'v Form.t Scan.t ->
        ('k * 'v) Form.t Scan.t

    (** The scanner compiler uses [memo] to obtain an annotated scanner mark
        at the beginning of the next element in a sequence, advancing the
        cursor to one position past the end of the element.
    *)
    val memo: Scan.mark Form.t Scan.t
end

(** The module type of ingest specializations. *)
module type Profile = sig

    (** The basis used to construct the profile. *)
    module Basis: Basis

    (** An alias to the basis scanner form. *)
    module Form: module type of Basis.Form

    (** An alias to the specialized scanner module. *)
    module Scan: module type of Basis.Scan

    (** Use [scan m] to compile the model [m] into a specialized scanner. *)
    val scan: 'a model -> 'a Form.t Scan.t
end

(** Use [Create(B)] to create an ingest specialization from the basis [B]. *)
module Create(B: Basis): Profile
   with module Basis := B
    and module Form := B.Form
    and module Scan := B.Scan

(*--- End ---*)
