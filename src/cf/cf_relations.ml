(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Order = sig
    type t
    val compare: t -> t -> int
end

let min (type t) (module M: Order with type t = t) (a : t) (b : t) : t =
    if M.compare a b > 0 then b else a

let max (type t) (module M: Order with type t = t) (a : t) (b : t) : t =
    if M.compare a b > 0 then a else b

module type Equal = sig
    type t
    val equal: t -> t -> bool
end

module type Std = sig
    type t
    val compare: t -> t -> int
    val equal: t -> t -> bool
end

module Int: (Std with type t = int) = struct
    type t = int

    let compare a b =
        let d = b - a in
        if d < 0 then 1 else if d > 0 then (-1) else 0

    let equal (a : int) (b : int) = (a == b)
end

module Extensible = struct
    class ['a] order = object

        (** Define to compare [a] and [b]. *)
        method compare: 'a -> 'a -> int = Stdlib.compare
    end

    let ordering (type a) obj =
        let _ = (obj :> a order) in
        let module M = struct type t = a let compare = obj#compare end in
        (module M: Order with type t = a)

    class ['a] equal = object
        method equal: 'a -> 'a -> bool = Stdlib.(=)
    end

    let equality (type a) obj =
        let _ = (obj :> a equal) in
        let module M = struct type t = a let equal = obj#equal end in
        (module M: Equal with type t = a)

    class ['a] std = object
        inherit ['a] order
        inherit ['a] equal
    end

    let std (type a) obj =
        let _ = (obj :> a std) in
        let
            module M = struct
                type t = a
                let compare = obj#compare
                let equal = obj#equal
            end
        in
        (module M: Std with type t = a)
end

(*--- End ---*)
