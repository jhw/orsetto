(*---------------------------------------------------------------------------*
  Copyright (C) 2005-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Regular expression parsing, search and matching. *)

(** {6 Overview}

    This module implements simple regular expression parsing, search and
    matching in pure Objective Caml for 8-bit extended ASCII text.

    Use any of the following constructions in regular expressions:
    - [\n     ] Matches LF ("newline") character.
    - [\t     ] Matches TAB character.
    - [\r     ] Matches RETURN character.
    - [\a     ] Matches an alphabetical character.
    - [\d     ] Matches a decimal digit character.
    - [\i     ] Matches an alphanumerical character.
    - [\s     ] Matches a TAB, LF, VT, FF, CR or SPACE (whitespace) character.
    - [\w     ] Matches a character other than a whitespace character.
    - [\xNN   ] Matches the character with hexadecimal code [NN].
    - [\DDD   ] Matches the character with decimal code [DDD], where DDD is a
        three digit number between [000] and [255].
    - [\c_    ] Matches the control character corresponding to the subsequent
        printable character, e.g. [\cA] is CONTROL-A, and [\c\[] is ESCAPE.
    - [.      ] Matches any character except newline.
    - [*      ] (postfix) Matches the preceding expression, zero, one or
        several times in sequence.
    - [+      ] (postfix) Matches the preceding expression, one or several
        times in sequence.
    - [?      ] (postfix) Matches the preceding expression once or not at all.
    - [[..]   ] Character set.  Ranges are denoted with ['-'], as in [[a-z]].
        An initial ['^'], as in [[^0-9]], complements the set.  Special
        characters in the character set syntax may be included in the set by
        escaping them with a backtick, e.g. [[`^```\]]] is a set containing
        three characters: the carat, the backtick and the right bracket
        characters.
    - [(..|..)] Alternatives.  Matches one of the expressions between the
        parentheses, which are separated by vertical bar characters.
    - [\_     ] Escaped special character.  The special characters are ['\\'],
        ['.'], ['*'], ['+'], ['?'], ['('], ['|'], [')'], ['\['].
*)

(** {6 Interface} *)

(** The deterministic finite automata on octet character symbols. *)
module DFA: sig
    include Cf_dfa.Regular with type event := char
    include Cf_dfa.Machine with type event := char and type 'r fin := 'r fin

    (** Use [string_to_term s] to make a term representing the regular
        expression in [s]. Raises [Invalid_argument] if [s] is not a valid
        regular expression.
    *)
    val string_to_term: string -> term

    (** Use [chars_to_term s] to make a term representing the regular
        expression parsed from the characters in [s]. Raises [Invalid_argument]
        if [s] does not comprise a valid regular expression.
    *)
    val chars_to_term: char Seq.t -> term

    (** This module extends the usual set of affix operators. *)
    module Affix: sig
        include Cf_dfa.Affix
           with type event := char
            and type term := term
            and type 'a fin := 'a fin

        (** Using [!:s] is the same as [string_to_term s]. *)
        val ( !$ ): string -> term

        (** Using [s $$= v] is the same as [fin (string_to_term s) v]. *)
        val ( $$= ): string -> 'a -> 'a fin
    end
end

(** The type of a compiled regular expression. *)
type t

(** Use [of_string s] to make a regular expression denoted by [s]. Raises
    [Invalid_argment] if [s] does not denote a valid regular expression.
*)
val of_string: string -> t

(** Use [of_chars s] to make a regular expression denoted by the characters
    in [s]. Raises [Invalid_argment] if the characters do not denote a valid
    regular expression.
*)
val of_chars: char Seq.t -> t

(** Use [of_dfa_term s] to make a regular expression for recognizing the
    language term [s].
*)
val of_dfa_term: DFA.term -> t

(** Use [test r s] to test whether [r] recognizes [s]. Returns [true] if all
    the characters in [s] are not rejected and the DFA reaches at least one
    final state, otherwise returns [false].
*)
val test: t -> string -> bool

(** Use [contains r s] to test whether [r] recognizes any substring of [s]. *)
val contains: t -> string -> bool

(** Use [search r s] to search with [r] in a confluently persistent sequence
    [s] for the first accepted subsequence. Returns [None] if [s] does not
    contain a matching subsequence. Otherwise, returns [Some (start, limit)]
    where [start] is the index of the first matching subsequence, and [limit]
    is the index after the end of the longest matching subsequence.
*)
val search: t -> char Seq.t -> (int * int) option

(** Use [split r s] to split [s] into a sequence of slices comprising the
    substrings in [s] that are separated by disjoint substrings matching [r],
    which are found by searching from left to right. If [r] does not match any
    substring in [s], then a sequence containing just [s] is returned, even if
    [s] is an empty slice.
*)
val split: t -> string Cf_slice.t -> string Cf_slice.t Seq.t

(** Use [quote s] to make a copy of [s] by converting all the special
    characters into escape sequences.
*)
val quote: string -> string

(** Use [unquote s] to make a copy of [s] by converting all the escape
    sequences into ordinary characters.
*)
val unquote: string -> string

(*--- End ---*)
