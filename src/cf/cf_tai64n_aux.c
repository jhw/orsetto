/*---------------------------------------------------------------------------*
  Copyright (c) 2003-2022, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/

#include "cf_tai64n_aux.h"

#include <errno.h>
#include <math.h>
#include <sys/time.h>

#define INVALID_ARGUMENT(S)     (caml_invalid_argument("Cf_tai64n." S))

static int cf_tai64n_op_compare(value v1, value v2);
static intnat cf_tai64n_op_hash(value v);
static void cf_tai64n_op_serialize(value v, uintnat* z32, uintnat* z64);
static uintnat cf_tai64n_op_deserialize(void* buffer);

static value cf_tai64n_first_val = Val_unit;
static value cf_tai64n_last_val = Val_unit;

static struct custom_operations cf_tai64n_op = {
    .identifier = "uri://conjury.org/orsetto/cf_tai64n",
    .finalize = custom_finalize_default,
    .compare = cf_tai64n_op_compare,
    .hash = cf_tai64n_op_hash,
    .serialize = cf_tai64n_op_serialize,
    .deserialize = cf_tai64n_op_deserialize,
    .compare_ext = custom_compare_ext_default,
};

static int cf_tai64n_op_compare(value v1, value v2)
{
    CAMLparam2(v1, v2);

    const Cf_tai64n_t* v1Ptr;
    const Cf_tai64n_t* v2Ptr;
    int result;

    v1Ptr = Cf_tai64n_val(v1);
    v2Ptr = Cf_tai64n_val(v2);

    if (v2Ptr->s > v1Ptr->s)
        result = 1;
    else if (v1Ptr->s > v2Ptr->s)
        result = -1;
    else if (v2Ptr->ns > v1Ptr->ns)
        result = 1;
    else if (v1Ptr->ns > v2Ptr->ns)
        result = -1;
    else
        result = 0;

    CAMLreturn(result);
}

static intnat cf_tai64n_op_hash(value v)
{
    CAMLparam1(v);
    uint64_t s = Cf_tai64n_val(v)->s;
    uint32_t ns = Cf_tai64n_val(v)->ns;
    uint32_t lo = (uint32_t) s, hi = (uint32_t) (s >> 32);
    CAMLreturn((intnat) hi ^ lo ^ ns);
}

static void cf_tai64n_op_serialize(value v, uintnat* z32, uintnat* z64)
{
    caml_serialize_int_8(Cf_tai64n_val(v)->s);
    caml_serialize_int_4(Cf_tai64n_val(v)->ns);
    *z32 = *z64 = 12;
}

static uintnat cf_tai64n_op_deserialize(void* data)
{
    Cf_tai64n_t* p = (Cf_tai64n_t*) data;
    p->s = caml_deserialize_sint_8();
    p->ns = caml_deserialize_sint_4();
    return 12;
}

/*---
  Compare primitive
  ---*/
CAMLprim value cf_tai64n_equal(value v1, value v2)
{
    CAMLparam2(v1, v2);
    int result;

    const Cf_tai64n_t* v1Ptr;
    const Cf_tai64n_t* v2Ptr;

    v1Ptr = Cf_tai64n_val(v1);
    v2Ptr = Cf_tai64n_val(v2);

    result = (v1Ptr->s == v2Ptr->s && v1Ptr->ns == v2Ptr->ns);
    CAMLreturn(Val_bool(result));
}

/*---
  Compare primitive
  ---*/
CAMLprim value cf_tai64n_compare(value v1, value v2)
{
    CAMLparam2(v1, v2);
    int dt;

    dt = cf_tai64n_op_compare(v1, v2);
    CAMLreturn(Val_int(dt));
}

/*---
  Allocate an initialized structure
  ---*/
extern value cf_tai64n_alloc(const Cf_tai64n_t* tai64nPtr)
{
    value result;

    result = caml_alloc_custom(&cf_tai64n_op, sizeof *tai64nPtr, 0, 1);
    *Cf_tai64n_val(result) = *tai64nPtr;
    return result;
}

/*---
  Set to current time

    1972-01-01 00:00:00 UTC was 1972-01-01 00:00:10 TAI
    Unix time values include all currently defined leap seconds
  ---*/
extern void cf_tai64n_update(Cf_tai64n_t* tai64nPtr)
{
    uint64_t epoch;
    struct timeval tv;
    struct timezone tz;

    if (gettimeofday(&tv, &tz))
        caml_unix_error(errno, "gettimeofday", Nothing);

    epoch = CF_TAI64_UNIX_EPOCH;
    epoch += cf_tai64_current_offset_from_utc;
    tai64nPtr->s = epoch + ((uint64_t) tv.tv_sec);
    tai64nPtr->ns = tv.tv_usec * 1000;
}

/*---
  Allocate and set to current time
  ---*/
CAMLprim value cf_tai64n_now(value unit)
{
    CAMLparam0();
    CAMLlocal1(result);
    (void) unit;

    Cf_tai64n_t x;

    cf_tai64n_update(&x);
    result = cf_tai64n_alloc(&x);

    CAMLreturn(result);
}

/*---
  Allocate global value of the first representable TAI64N
  ---*/
CAMLprim value cf_tai64n_first(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64n_first_val);
}

/*---
  Allocate global value of the last representable TAI64N
  ---*/
CAMLprim value cf_tai64n_last(value unit)
{
    CAMLparam0();
    (void) unit;
    CAMLreturn(cf_tai64n_last_val);
}

/*---
    external compose: Cf_tai64.t -> int -> t = "cf_tai64n_compose"
  ---*/
CAMLprim value cf_tai64n_compose(value tai64Val, value nsVal)
{
    CAMLparam2(tai64Val, nsVal);
    CAMLlocal1(resultVal);

    Cf_tai64_t* tai64Ptr;
    uint32_t ns;
    Cf_tai64n_t tai64n;

    tai64Ptr = Cf_tai64_val(tai64Val);
    ns = (uint32_t) Int_val(nsVal);
    if (ns > 999999999) INVALID_ARGUMENT("compose: ns > 10^9");

    tai64n.s = tai64Ptr->s;
    tai64n.ns = ns;

    resultVal = cf_tai64n_alloc(&tai64n);
    CAMLreturn(resultVal);
}

/*---
    external decompose: t -> Cf_tai64.t * int =
         "cf_tai64n_decompose"
  ---*/
CAMLprim value cf_tai64n_decompose(value tai64nVal)
{
    CAMLparam1(tai64nVal);
    CAMLlocal3(resultVal, tai64Val, nsVal);
    Cf_tai64_t tai64;

    tai64.s = Cf_tai64n_val(tai64nVal)->s;
    tai64Val = cf_tai64_alloc(&tai64);
    nsVal = Val_int(Cf_tai64n_val(tai64nVal)->ns);

    resultVal = caml_alloc_small(2, 0);
    caml_initialize(&Field(resultVal, 0), tai64Val);
    caml_initialize(&Field(resultVal, 1), nsVal);

    CAMLreturn(resultVal);
}

/*---
  Convert a floating point Unix.time result to TAI
  ---*/
CAMLprim value cf_tai64n_of_unix_time(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);

    static const double cf_tai64_unix_limit[2] = {
        -((double)CF_TAI64_UNIX_EPOCH),
        ((double)(CF_TAI64_UNIX_EPOCH << 1) - CF_TAI64_UNIX_EPOCH - 1),
    };

    Cf_tai64n_t tai64n;
    double x, y;

    y = (uint64_t) modf(Double_val(v), &x);
    x += (double) cf_tai64_current_offset_from_utc;
    if (x < cf_tai64_unix_limit[0] || x > cf_tai64_unix_limit[1])
        cf_tai64_range_error();

    tai64n.s = CF_TAI64_UNIX_EPOCH + ((uint64_t) x);
    tai64n.ns = (uint32_t) (y * 1E9);
    result = cf_tai64n_alloc(&tai64n);

    CAMLreturn(result);
}

/*---
  Convert a TAI value to a floating point Unix.time result
  ---*/
CAMLprim value cf_tai64n_to_unix_time(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);

    double x, y;
    uint64_t epoch;

    epoch = CF_TAI64_UNIX_EPOCH;
    epoch += cf_tai64_current_offset_from_utc;

    x = (double) (Cf_tai64n_val(v)->s - epoch);
    y = ((double) Cf_tai64n_val(v)->ns) * 1E-9;

    result = caml_copy_double(x + y);

    CAMLreturn(result);
}

/*---
  Convert a string containing a TAI64N label into the corresponding value
  ---*/
CAMLprim value cf_tai64n_of_label(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);

    Cf_tai64n_t tai64n;
    int i;
    uint64_t x;
    uint32_t y;

    if (caml_string_length(v) != 12) cf_tai64_label_error();
    for (i = 0, x = 0; i < 8; ++i) x = (x << 8) | Byte_u(v, i);
    for (i = 8, y = 0; i < 12; ++i) y = (y << 8) | Byte_u(v, i);
    tai64n.s = x;
    tai64n.ns = y;
    result = cf_tai64n_alloc(&tai64n);

    CAMLreturn(result);
}

/*---
  Convert a TAI64N value to a Caml string containing its label
  ---*/
CAMLprim value cf_tai64n_to_label(value v)
{
    CAMLparam1(v);
    CAMLlocal1(result);

    uint64_t x;
    uint32_t y;
    int i;

    result = caml_alloc_string(12);
    x = Cf_tai64n_val(v)->s;
    for (i = 7; i >= 0; --i, x >>= 8)
        Byte_u(result, i) = (unsigned char) x;
    y = Cf_tai64n_val(v)->ns;
    for (i = 11; i >= 8; --i, y >>= 8)
        Byte_u(result, i) = (unsigned char) y;

    CAMLreturn(result);
}

/*---
  Addition of float
  ---*/
CAMLprim value cf_tai64n_add(value tai64nVal, value dtVal)
{
    CAMLparam2(tai64nVal, dtVal);
    CAMLlocal1(result);

    Cf_tai64n_t tai64n;
    double zInt, zFrac;
    int64_t x;
    int32_t y, ns;

    zFrac = modf(Double_val(dtVal), &zInt);
    x = (int64_t) zInt;
    y = (int32_t) (zFrac * 1E9);

    tai64n.s = Cf_tai64n_val(tai64nVal)->s + x;
    ns = (int32_t) Cf_tai64n_val(tai64nVal)->ns + y;
    if (ns < 0) {
        ns += 1000000000;
        tai64n.s -= 1;
    }
    else if (ns >= 1000000000) {
        ns -= 1000000000;
        tai64n.s += 1;
    }

    if (tai64n.s >= (CF_TAI64_UNIX_EPOCH << 1))
        cf_tai64_range_error();
    tai64n.ns = (uint32_t) ns;
    result = cf_tai64n_alloc(&tai64n);
    CAMLreturn(result);
}

/*---
  Subtraction of TAI64N values
  ---*/
CAMLprim value cf_tai64n_sub(value v1, value v2)
{
    CAMLparam2(v1, v2);
    CAMLlocal1(resultVal);

    double dt;
    dt = ((int64_t) Cf_tai64n_val(v1)->s) -
        ((int64_t) Cf_tai64n_val(v2)->s);
    dt += (((int32_t) Cf_tai64n_val(v1)->ns) -
        ((int32_t) Cf_tai64n_val(v2)->ns)) * 1E-9;

    resultVal = caml_copy_double(dt);
    CAMLreturn(resultVal);
}

/*---
  Initialization primtive
  ---*/
CAMLprim value cf_tai64n_init(value unit)
{
    static const Cf_tai64n_t first = { 0ULL, 0UL };
    static const Cf_tai64n_t last = {
        (CF_TAI64_UNIX_EPOCH << 1) - 1, 999999999UL
    };

    (void) unit;
    caml_register_custom_operations(&cf_tai64n_op);

    caml_register_global_root(&cf_tai64n_first_val);
    cf_tai64n_first_val = cf_tai64n_alloc(&first);

    caml_register_global_root(&cf_tai64n_last_val);
    cf_tai64n_last_val = cf_tai64n_alloc(&last);

    return Val_unit;
}

/*--- $File: src/cf/cf_tai64n_aux.c $ ---*/
