(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type radix = Radix of int [@@caml.unboxed]
type digit = Digit of int [@@caml.unboxed]

let binary = Radix 2
let octal = Radix 8
let decimal = Radix 10
let hexadecimal = Radix 16

type iopt
type fopt

type _ opt =
    | O_radix: radix -> iopt opt
    | O_leading: iopt opt
    | O_signed: iopt opt
    | O_width: int -> 'a opt
    | O_scientific: fopt opt

type (_, _) ret =
    | R_int: (int, iopt) ret
    | R_int32: (int32, iopt) ret
    | R_int64: (int64, iopt) ret
    | R_native: (nativeint, iopt) ret
    | R_float: (float, fopt) ret

module type Natural = sig
    type t

    val zero: t
    val neg: t -> t
    val digit: digit -> t
    val check: b:t -> d:t -> v:t -> bool
    val push: b:t -> d:t -> v:t -> t
end

module Z_int: Natural with type t = int = struct
    type t = int

    let zero = 0
    let neg = ( ~- )
    let digit (Digit d) = d

    let check ~b ~d ~v =
        if v > 0 then
            v > (max_int - d) / b
        else
            v < (min_int + d) / b

    let push ~b ~d ~v = v * b + (if v < 0 then (-d) else d)
end

module Z_int32: Natural with type t = int32 = struct
    type t = int32

    let zero = 0l
    let neg = Int32.neg
    let digit (Digit d) = Int32.of_int d

    let check ~b ~d ~v =
        if v > zero then
            v > Int32.(div (sub Int32.max_int d) b)
        else
            v < Int32.(div (add Int32.max_int d) b)

    let push ~b ~d ~v =
        let d = if v < zero then neg d else d in
        Int32.(add d (mul v b))
end

module Z_int64: Natural with type t = int64 = struct
    type t = int64

    let zero = 0L
    let neg = Int64.neg
    let digit (Digit d) = Int64.of_int d

    let check ~b ~d ~v =
        if v > zero then
            v > Int64.(div (sub Int64.max_int d) b)
        else
            v < Int64.(div (add Int64.max_int d) b)

    let push ~b ~d ~v =
        let d = if v < zero then neg d else d in
        Int64.(add d (mul v b))
end

module Z_native: Natural with type t = nativeint = struct
    type t = nativeint

    let zero = 0n
    let neg = Nativeint.neg
    let digit (Digit d) = Nativeint.of_int d

    let check ~b ~d ~v =
        if v > zero then
            v > Nativeint.(div (sub Nativeint.max_int d) b)
        else
            v < Nativeint.(div (add Nativeint.max_int d) b)

    let push ~b ~d ~v =
        let d = if v < zero then neg d else d in
        Nativeint.(add d (mul v b))
end

module type Floating_point = sig
    type t
    val of_string_opt: string -> t option
end

module Q_float: Floating_point with type t = float = struct
    type t = float

    let of_string_opt s =
        (* OCaml 4.07 --> Float.of_string_opt s *)
        try Some (float_of_string s) with Failure _ -> None
end

type _ ctrl =
    | C_integer: {
        nat: (module Natural with type t = 't);
        radix: radix;
        leading: bool;
        signed: bool;
        width: int;
      } -> 't ctrl
    | C_float: {
        fp: (module Floating_point with type t = 't);
        scientific: bool;
        width: int;
      } -> 't ctrl

let rec xradix = function [@warning "-4"]
    | O_radix b :: _ -> b
    | _ :: opts -> (xradix[@tailcall]) opts
    | [] -> Radix 10

let rec xwidth = function [@warning "-4"]
    | O_width n :: _ -> n
    | _ :: opts -> (xwidth[@tailcall]) opts
    | [] -> (-1)

let xiopts opts =
    let radix = xradix opts in
    let leading = List.mem O_leading opts in
    let signed = List.mem O_signed opts in
    let width = xwidth opts in
    radix, leading, signed, width

let ctrl:
    type r x. ?opt:x opt list -> (r, x) ret -> r ctrl
    = fun ?opt:(opts = []) ret ->
        match ret with
        | R_int ->
            let nat = (module Z_int : Natural with type t = r) in
            let radix, leading, signed, width = xiopts opts in
            C_integer { nat; radix; leading; signed; width }
        | R_int32 ->
            let nat = (module Z_int32 : Natural with type t = r) in
            let radix, leading, signed, width = xiopts opts in
            C_integer { nat; radix; leading; signed; width }
        | R_int64 ->
            let nat = (module Z_int64 : Natural with type t = r) in
            let radix, leading, signed, width = xiopts opts in
            C_integer { nat; radix; leading; signed; width }
        | R_native ->
            let nat = (module Z_native : Natural with type t = r) in
            let radix, leading, signed, width = xiopts opts in
            C_integer { nat; radix; leading; signed; width }
        | R_float ->
            let fp = (module Q_float : Floating_point with type t = r) in
            let scientific = List.mem O_scientific opts in
            let width = xwidth opts in
            C_float { fp; scientific; width }

module type Basis = sig
    type symbol and position and +'a form

    module Scan: Cf_scan.Profile
       with type symbol := symbol
        and type position := position
        and type 'a form := 'a form

    module Form: Cf_scan.Form with type 'a t := 'a form

    val digitsym: radix -> symbol -> digit
    val signtok: symbol -> int option
    val pointsat: symbol -> bool
    val expsat: symbol -> bool
end

module type Profile = sig
    type +'a t
    type +'a form

    val special: 'r ctrl -> 'r form t

    val signed_int: int form t
    val signed_int32: int32 form t
    val signed_int64: int64 form t
    val signed_native: nativeint form t
    val simple_float: float form t
    val scientific_float: float form t
end

module Create(B: Basis) = struct
    open B.Scan
    open Affix

    module Form = B.Form

    let digit b =
        let* c = any in
        let d0 = B.digitsym b @@ Form.dn c in
        let Digit d = d0 in
        if d < 0 then nil else return @@ Form.mv d0 c

    let optsign = opt @@ tok B.signtok
    let expsym = sat B.expsat
    let pointsym = sat B.pointsat

    let special (type r) (ctrl : r ctrl) =
        match ctrl with
        | C_integer k ->
            let module N = (val k.nat : Natural with type t = r) in
            let ifsign = if k.signed then optsign else return None in
            let* signlopt = ifsign in
            let* dl = digit k.radix in
            let sf, synl =
                match signlopt with
                | None -> 0, Form.mv () dl
                | Some signl -> Form.dn signl, Form.span signl dl ()
            in
            let d0 = Form.dn dl in
            let Digit d = d0 in
            if d = 0 && sf <> 0 then
                nil
            else if k.leading || d <> 0 then begin
                let Radix b = k.radix in
                let b = N.digit @@ Digit b in
                let rec next i v finl =
                    if k.width < 0 || i < k.width then begin
                        let* dlopt = opt (digit k.radix) in
                        match dlopt with
                        | Some dl ->
                            let d = N.digit @@ Form.dn dl in
                            if N.check ~b ~d ~v then
                                finish v finl
                            else begin
                                let i = succ i and v = N.push ~b ~d ~v in
                                (next[@tailcall]) i v dl
                            end
                        | None ->
                            finish v finl
                    end
                    else
                        finish v finl
                and finish v finl =
                    return @@ Form.span synl finl v
                in
                let i = if sf <> 0 then 2 else 1 in
                let v = N.digit d0 in
                let v = if sf < 0 then N.neg v else v in
                (next[@tailcall]) i v dl
            end
            else
                return @@ Form.mv N.zero dl
        | C_float k ->
            let* _ = return () in
            let b = Buffer.create (if k.width < 0 then 0 else k.width) in
            let ck i = k.width < 0 || i < k.width in
            let sign i =
                optsign >>= function
                | None ->
                    return (i, None)
                | Some signl ->
                    let sign = Form.dn signl in
                    if sign <> 0 then begin
                        Buffer.add_char b (if sign < 0 then '-' else '+');
                        return (succ i, Some signl)
                    end
                    else
                        return (i, None)
            in
            let one_digit i =
                let* dl = digit decimal in
                let Digit d = Form.dn dl in
                Buffer.add_char b @@ Char.chr (d + 48);
                return (Form.mv (succ i) dl)
            in
            let rec more_digits il =
                let i = Form.dn il in
                if ck i then begin
                    opt (one_digit i) >>= function
                    | None -> return @@ Form.mv i il
                    | Some il -> (more_digits[@tailcall]) il
                end
                else
                    return @@ Form.mv i il
            in
            let int_part i =
                let* i, signlopt = sign i in
                let* il = one_digit i in
                let synl =
                    match signlopt with
                    | None -> Form.mv () il
                    | Some signl -> Form.mv () signl
                in
                let* il = more_digits il in
                return (synl, il)
            in
            let fraction_opt il =
                let* _ = pointsym in
                Buffer.add_char b '.';
                let* il = one_digit @@ succ @@ Form.dn il in
                (more_digits[@tailcall]) il
            in
            let fraction il =
                opt (fraction_opt il) >>= function
                | None -> return il
                | Some il -> return il
            in
            let exponent_opt il =
                let* _ = expsym in
                Buffer.add_char b 'e';
                (int_part[@tailcall]) (succ @@ Form.dn il)
            in
            let exponent il =
                if k.scientific then begin
                    opt @@ exponent_opt il >>= function
                    | None -> return il
                    | Some (_, il) -> return il
                end
                else
                    return il
            in
            let* synl, il = int_part 0 in
            let* il = fraction il in
            let* il = exponent il in
            let i = Form.dn il in
            assert (ck (pred i));
            let module N = (val k.fp : Floating_point with type t = r) in
            let bytes = Buffer.contents b in
            match N.of_string_opt bytes with
            | None -> nil
            | Some v -> return @@ Form.span synl il v

    let signed_int = special @@ ctrl ~opt:[ O_signed ] R_int
    let signed_int32 = special @@ ctrl ~opt:[ O_signed ] R_int32
    let signed_int64 = special @@ ctrl ~opt:[ O_signed ] R_int64
    let signed_native = special @@ ctrl ~opt:[ O_signed ] R_native
    let simple_float = special @@ ctrl R_float
    let scientific_float = special @@ ctrl ~opt:[ O_scientific ] R_float
end

module ASCII = struct
    module Aux = struct
        type symbol = char
        type position = Cf_scan.Simple.position
        type 'a form = 'a

        module Scan = Cf_scan.ASCII
        module Form = Cf_scan.Simple.Form

        let digitsym b c = begin [@warning "-4"]
            match b, Char.code c with
            | Radix 16, c when c >= 48 && c <= 57 -> Digit (c - 48)
            | Radix  n, c when c >= 48 && c < 48 + n -> Digit (c - 48)
            | Radix 16, c when c >= 65 && c <= 70 -> Digit (c - 55)
            | Radix 16, c when c >= 97 && c <= 102 -> Digit (c - 87)
            | _, _ -> Digit (-1)
        end

        let signtok c = begin [@warning "-4"]
            match c with
            | '+' -> Some 1
            | '-' -> Some (-1)
            | _ -> None
        end

        let pointsat c = (c == '.')
        let expsat c = (c == 'e' || c == 'E')
    end

    include Create(Aux)
end

(*--- End ---*)
