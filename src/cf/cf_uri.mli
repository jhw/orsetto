(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** General syntax of the Uniform Resource Identifier (URI) *)

(** {6 Overview}

    This module implements operations on the general syntax of Uniform Resource
    Identifiers (URI) according to RFC 3986, as well as convenience functions
    for manipulating their components. Some functions are also provided for
    conversion to and from strings for presentation.
*)

(** {6 Types}

    The private types below comprise the structure of generic URI values in
    syntax-normalized form (see RFC 3986, section 6.2.2).
*)

(** The extensible sum type of "host" components of hierarchical URI's. Values
    of the cases of this type defined in this module are syntax-normalized. In
    particular, the ASCII letter characters present in "scheme" and "host"
    components are always lower case (except for the 'zoneid' field in IPv6
    addresses).
*)
type host = ..

(** Host name (RFC 3986). *)
type host += private H_name of string

(** IPv4 address literal (RFC 3986) *)
type host += private H_ipv4 of { address: string }

(** IPv6 address literal with optional zone identifier (RFC 3986,6874) *)
type host += private H_ipv6 of { address: string; zoneid: string option }

(** Future IP address literal (RFC 3986) *)
type host += private H_ip_future of { version: int; address: string }

(** The private type of "authority" components in hierarchical identifiers. *)
type authority = private Authority of {
    user: string option;
    host: host;
    port: int option;
}

(** The virtual class type inherited by all URI subtypes. *)
class type virtual basis = object
    method authority: authority option
    method absolute: bool
    method path: string list
    method query: string option
end

(** The class type of relative URI references. *)
class type relative = object
    inherit basis
    method fragment: string option
end

(** The class type of absolute URI bases. *)
class type absolute = object
    inherit basis
    method scheme: string
end

(** The class type of generic URI. *)
class type generic = object
    inherit absolute
    inherit relative
end

(** The private higher-order type of URI object containers. *)
type +'a container = private Object of 'a constraint 'a = #basis [@@unboxed]

(** The monomorphic type alias for generic URI objects. *)
type t = generic container

(** The private type of URI references. *)
type reference = private
    | Generic of generic container
    | Relative of relative container

(** {6 Scanners}

    These functions comprise a parser for the generic URI syntax defined in
    RFC 3986 (updated by errata and by RFC 6874).

    Fields containing percent-encoded text are decoded first, then other syntax
    normalization functions are applied, e.g. case normalization, path segment
    list normalization, et cetera.
*)

(** Use [scan_registry_name] to scan the "registry name" part of a host. *)
val scan_registry_name: string Cf_scan.ASCII.t

(** Use [scan_ipv4_address] to scan an IPv4 address literal. *)
val scan_ipv4_address: string Cf_scan.ASCII.t

(** Use [scan_ipv6_address] to scan an IPv6 address literal. *)
val scan_ipv6_address: string Cf_scan.ASCII.t

(** Use [scan_host] to scan the "host" part of a URI authority. *)
val scan_host: host Cf_scan.ASCII.t

(** Use [scan_authority] to scan the "authority" part of a URI hierarchy. *)
val scan_authority: authority Cf_scan.ASCII.t

(** Use [scan_generic] to scan a generic URI. *)
val scan_generic: t Cf_scan.ASCII.t

(** Use [scan_reference] to scan a URI reference. *)
val scan_reference: reference Cf_scan.ASCII.t

(** Use [of_string s] to decompose [s] into its URI structural value. Raises
    [Invalid_argument] if [s] is not valid generic URI syntax.
*)
val of_string: string -> t

(** {6 Emitters}

    These functions comprise a formatter that conforms to the generic URI
    syntax defined in RFC 3986 (updated by errata and by RFC 6874).

    Unreserved characters are not percent-encoded. Reserved characters are only
    percent-encoded when the fields in which they appear are delimited by them.
*)

(** Use [emit_host b name] to emit the "host" part of a URI authority. *)
val emit_host: host Cf_emit.To_buffer.t

(** Use [emit_authority b authority] to emit the "authority" part of a URI
    hierarchy.
*)
val emit_authority: authority Cf_emit.To_buffer.t

(** Use [emit_generic b uri] to emit a generic URI. *)
val emit_generic: t Cf_emit.To_buffer.t

(** Use [emit_reference b reference] to emit a URI reference. *)
val emit_reference: reference Cf_emit.To_buffer.t

(** Use [to_string uri] to compose [uri] into its textual form. *)
val to_string: t -> string

(** {6 Miscellaneous Functions} *)

(** Use [resolve_aux b r] to resolve [r] according to the base [b]. *)
val resolve_aux: #absolute container -> #relative container -> t

(** Use [resolve b r] to resolve [r] according to the base [b]. *)
val resolve: t -> reference -> t

(** Use [percent_decode s] to transform the sequence [s] by decoding all the
    "percent-encoded" characters. Evaluation raises [Failure] if a ['%']
    character appears without two hexadecimal digits immediately following.
*)
val percent_decode: char Seq.t -> char Seq.t

(** Use [percent_encode ?allow s] to transform the sequence [s] into the
    equivalent "percent-encoded" sequence. Use [~allow] to provide a function
    that returns [true] for those reserved characters to be left decoded in the
    resulting sequence. Unreserved characters are never percent encoded.
*)
val percent_encode: ?allow:(char -> bool) -> char Seq.t -> char Seq.t

(*--- End ---*)
