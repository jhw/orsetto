(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Monad operations. *)

(** {6 Overview}

    This module defines functors and module types for use in programming with
    monad operations. Provide a module [B] with a signature conforming to one
    of the [Basis] types, i.e. unary, binary or trinary, then use the
    corresponding [Create(B)] functor to compose a module with affix operators,
    syntax extension signatures, and conventional utility functions.
*)

(** {6 Module Aliases} *)
module Core = Cf_monad_core

(** {5 Unary Monad}

    A unary monad is a type with a single return parameter, which encapsulates
    a value of monomorphic type.
*)
module Unary: sig

    (** Use [module B: Basis = struct ... end] to define a module [B] that
        implements the core type and functions of a monad.
    *)
    module type Basis = Core.Unary.Basis

    (** The signature of modules produced by the [Create(B: Basis)] functor. *)
    module type Profile = sig

        (** The abstract type of a monad. *)
        type +'r t

        (** Module inclusions from [Cf_monad_core] and [Cf_seqmonad]. *)
        include Core.Unary.Profile with type 'r t := 'r t
        include Cf_seqmonad.Functor.Unary with type 'r t := 'r t
    end

    (** Use [module M = Create(B)] to create a module that implements all the
        conventional interfaces of a monad.
    *)
    module Create(B: Basis): Profile with type 'r t := 'r B.t
end

(** {5 Binary Monad}

    A binary monad is a type with a single return parameter, which encapsulates
    a value of polymorphic type with one parameter.
*)
module Binary: sig

    (** Use [module B: Basis = struct ... end] to define a module [B] that
        implements the core type and functions of a monad.
    *)
    module type Basis = Core.Binary.Basis

    (** The signature of modules produced by the [Create(B: Basis)] functor. *)
    module type Profile = sig

        (** The abstract type of a monad. *)
        type ('m, +'r) t

        (** Module inclusions from [Cf_monad_core] and [Cf_seqmonad]. *)
        include Core.Binary.Profile with type ('m, 'r) t := ('m, 'r) t
        include Cf_seqmonad.Functor.Binary
            with type ('m, 'r) t := ('m, 'r) t
    end

    (** Use [module M = Create(B)] to create a module that implements all the
        conventional interfaces of a monad.
    *)
    module Create(B: Basis): Profile with type ('m, 'r) t := ('m, 'r) B.t
end

(** {5 Trinary Monad}

    A trinary monad is a type with a single return parameter, which encapsulates
    a value of polymorphic type with two parameters.
*)
module Trinary: sig

    (** Use [module B: Basis = struct ... end] to define a module [B] that
        implements the core type and functions of a monad.
    *)
    module type Basis = Core.Trinary.Basis

    (** The signature of modules produced by the [Create(B: Basis)] functor. *)
    module type Profile = sig

        (** The abstract type of a monad. *)
        type ('p, 'q, +'r) t

        (** Module inclusions from [Cf_monad_core] and [Cf_seqmonad]. *)
        include Core.Trinary.Profile
            with type ('p, 'q, 'r) t := ('p, 'q, 'r) t

        include Cf_seqmonad.Functor.Trinary
            with type ('p, 'q, 'r) t := ('p, 'q, 'r) t
    end

    (** Use [module M = Create(B)] to create a module that implements all the
        conventional interfaces of a monad.
    *)
    module Create(B: Basis): Profile
        with type ('p, 'q, 'r) t := ('p, 'q, 'r) B.t
end

(*--- End ---*)
