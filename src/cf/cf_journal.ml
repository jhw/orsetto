(*---------------------------------------------------------------------------*
  Copyright (C) 2004-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Profile = sig
    type priority

    class type ['level] prioritizer = object
        method code: 'level -> priority
        method tag: 'level -> string
    end

    class ['level] event:
        'level #prioritizer -> 'level -> string ->
        object
            method prioritizer: 'level prioritizer
            method level: 'level
            method message: string
        end

    class type ['event] archiver = object
        constraint 'event = 'level #event
        method emit: 'event -> unit
    end

    class virtual ['archiver] agent:
        'level #prioritizer -> 'level -> 'archiver list ->
        object
            constraint 'event = 'level #event
            constraint 'archiver = 'event #archiver

            val mutable archivers_: 'archiver list
            val mutable limit_: priority

            method virtual private event: 'level -> string -> 'event
            method setlimit: 'level -> unit
            method enabled: 'level -> bool

            method private put:
                'a 'b. 'level -> ('event -> 'b) ->
                ('a, unit, string, string, string, 'b) format6 -> 'a
        end
end

(*
let nullf_ fmt =
    let fmt = string_of_format fmt in
    let len = String.length fmt in
    let rec eatfmt i =
        if i >= len then
            Obj.magic ()
        else
            match String.unsafe_get fmt i with
            | '%' -> Printf.scan_format fmt i sF aF tF fF
            | ch -> eatfmt (succ i)
    and sF _ i = eatfmt i
    and aF _ _ i = eatfmt i
    and tF _ i = eatfmt i
    and fF i = eatfmt i
    in
    eatfmt 0
*)

module Create(P: Cf_relations.Order) = struct
    module Priority = P

    class type ['level] prioritizer = object
        method code: 'level -> Priority.t
        method tag: 'level -> string
    end

    class ['level] event prioritizer level message =
        let prioritizer = (prioritizer :> 'level prioritizer) in
        object
            method prioritizer = prioritizer
            method level: 'level = level
            method message: string = message
        end

    class type ['event] archiver = object
        constraint 'event = 'level #event
        method emit: 'event -> unit
    end

    class virtual ['archiver] agent prioritizer limit archivers =
        let _ = (prioritizer :> 'level prioritizer) in
        let _ = (archivers :> 'archiver list) in
        object(self:'self)
            constraint 'event = 'level #event
            constraint 'archiver = 'event #archiver

            val mutable archivers_ = archivers
            val mutable limit_ = prioritizer#code limit

            method virtual private event: 'level -> string -> 'event

            method setlimit limit = limit_ <- prioritizer#code limit

            method enabled limit =
                Priority.compare (prioritizer#code limit) limit_ >= 0

            method private put:
                type a b. 'level -> ('event -> b) ->
                (a, unit, string, string, string, b) format6 -> a
                = fun level cont ->
                    let f message =
                        let e = self#event level message in
                        if self#enabled level then
                            List.iter (fun j -> j#emit e) archivers_;
                        cont e
                    in
                    Printf.ksprintf f
        end
end

module Basic = struct
    include Create(Cf_relations.Int)

    type invalid = [ `Invalid ]
    type fail = [ `Fail ]
    type error = [ `Error ]
    type warn = [ `Warn ]
    type notice = [ `Notice ]
    type info = [ `Info ]
    type debug = [ `Debug ]

    type basic = [ invalid | fail | error | warn | notice | info | debug ]
    type enable = [ `None | `All ]
    type level = [ basic | enable ]
end

class ['level] basic_prioritizer =
    let invalid_ = "INVALID" in
    let fail_ = "FAIL" in
    let error_ = "ERROR" in
    let warn_ = "WARN" in
    let notice_ = "NOTICE" in
    let info_ = "INFO" in
    let debug_ = "DEBUG" in
    let unknown_ = "UNKNOWN" in
    object(_:'self)
        constraint 'self = 'level #Basic.prioritizer
        constraint 'level = [> Basic.level ]

        method code = function
            | `All -> max_int
            | `Invalid -> 7000
            | `Fail -> 6000
            | `Error -> 5000
            | `Warn -> 4000
            | `Notice -> 3000
            | `Info -> 2000
            | `Debug -> 1000
            | (`None | _ as c) -> assert (c = `None); min_int

        method tag = function
            | `Invalid -> invalid_
            | `Fail -> fail_
            | `Error -> error_
            | `Warn -> warn_
            | `Notice -> notice_
            | `Info -> info_
            | `Debug -> debug_
            | _ -> unknown_
    end

class ['event] basic_channel_archiver channel = object
    constraint 'self = 'event #Basic.archiver
    constraint 'level = [> Basic.level ]
    constraint 'event = 'level #Basic.event

    method channel = channel

    method emit e =
        let _ = (e :> 'event) in
        let n = e#level in
        let p = e#prioritizer in
        if (p#code `Fail) - (p#code e#level) > 0 then begin
            let tag = p#tag n in
            let m = e#message in
            Printf.fprintf channel "%s: %s\n" tag m;
            flush channel
        end
end

class virtual ['archiver] basic_agent prioritizer limit archivers =
    let _ = (prioritizer :> 'level basic_prioritizer) in
    let _ = (limit : 'level) in
    let _ = (archivers : 'archiver list) in
    object(self)
        constraint 'level = [> Basic.level ]
        constraint 'event = 'level #Basic.event
        constraint 'archiver = 'event #Basic.archiver
        inherit ['archiver] Basic.agent prioritizer limit archivers as super

        method! private put:
            'a 'b. 'level -> ('event -> 'b) ->
            ('a, unit, string, 'b) format4 -> 'a = super#put

        method invalid:
            'a 'b. ('a, unit, string, string, string, 'b) format6 -> 'a =
            self#put `Invalid (fun x -> invalid_arg x#message)

        method fail:
            'a 'b. ('a, unit, string, string, string, 'b) format6 -> 'a =
            self#put `Fail (fun x -> failwith x#message)

        method error:
            'a. ('a, unit, string, string, string, unit) format6 -> 'a =
            self#put `Error ignore

        method warn:
            'a. ('a, unit, string, string, string, unit) format6 -> 'a =
            self#put `Warn ignore

        method notice:
            'a. ('a, unit, string, string, string, unit) format6 -> 'a =
            self#put `Notice ignore

        method info:
            'a. ('a, unit, string, string, string, unit) format6 -> 'a =
            self#put `Info ignore

        method debug:
            'a. ('a, unit, string, string, string, bool) format6 -> 'a =
            self#put `Debug (fun _ -> true)
    end

let basic_prioritizer = new basic_prioritizer

let basic_stdout_archiver =
    new basic_channel_archiver Stdlib.stdout

let basic_stderr_archiver =
    new basic_channel_archiver Stdlib.stderr

class basic_stdio_agent archiver =
    object
        inherit [Basic.level Basic.event basic_channel_archiver] basic_agent
            basic_prioritizer `Notice [archiver]

        method private event = new Basic.event basic_prioritizer
    end

type t = Basic.level Basic.event Basic.archiver basic_agent

let stdout = new basic_stdio_agent basic_stdout_archiver
let stderr = new basic_stdio_agent basic_stderr_archiver

(*--- End ---*)
