(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Parser combinators for record types. *)

(** {6 Overview}

    This module provides functional combinators for parsing records comprising
    lists of indexed fields, where an {i index} is a value of totally ordered
    type, and the associated value has a type that depends on the value of the
    index.

    {b Theory of Operation}: Define a {i Basis} that provides the scanners for
    field indexes and separators. Use that to {i Create()} a profile of the
    record scanner in the syntax of each interchange language. Its users may
    then define a {i schema} for each application record to scan, and use them
    to define scanners that first produce a {i pack} by scanning according to
    the schema, then unpack the scanned records from the pack into application
    data structures.
*)

(** {6 Interface} *)

(** Define a module of this signature according to the syntax of record types
    for a given structural interchange language.
*)

(** The basis signature of a record scanning module. *)
module type Basis = sig

    (** The symbol type. *)
    type symbol

    (** The position type. *)
    type position

    (** Total order for the index type. *)
    module Index: Cf_relations.Order

    (** Type nym equivalence for opaque values. *)
    module Content: Cf_type.Form

    (** The type constructor of the scanner production form. *)
    module Form: Cf_scan.Form

    (** A module comprising a basic scanner and the scanner functions specific
        to the field separator and index/value separator.
    *)
    module Scan: sig

        (** Include the basic scanner module. *)
        include Cf_scan.Profile
           with type symbol := symbol
            and type position := position
            and type 'a form := 'a Form.t

        (** Scan the index in a field. *)
        val index: Index.t t

        (** Define [Some p] if there is an index/value separator. *)
        val preval: unit t option

        (** Parameters for constructing a field chain discipline. *)
        open Cf_chain_scan
        val chain: (ctrl * ctrl * unit t) option
    end
end

(** The signature of a record scanning module. *)
module type Profile = sig

    (** The record index type. *)
    type index

    (** The type constructor of the scanner production form. *)
    type +'a form

    (** The type of the scanner. *)
    type +'a t

    (** Field descriptor type. A {i schema} contains a map of indexes to values
        of this type, which indicate whether the field is {i required} or
        {i optional}, along with a description of the type of the field value
        and a scanner for values of that type.
    *)
    type field =
        | Required: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
            (** A field of the named type is required. *)
        | Optional: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
            (** A field of the named type is optional. *)
        | Default:
            { nym: 'a Cf_type.nym; scan: 'a form t; default: 'a } -> field
            (** A field of the named type is optional with a default value. *)

    (** The type of a schema *)
    type schema

    (** Use [schema fields] to make a record schema for scanning records
        according to [fields]. If [~ignore:(n, p)] is provided, then as many as
        [n] unrecognized fields are scanned according to [p] and their values
        are discarded.
    *)
    val schema: ?ignore:(int * unit t) -> (index * field) list -> schema

    (** Use [range schema] to get the minimum and maximum number of fields that
        can be processed by scanning with [schema].
    *)
    val range: schema -> int * int

    (** The type of a packed record produced by scanning with a schema. *)
    type pack

    (** Use [scan schema] to scan a record according to [schema] producing a
        {i pack} value containing all the values in the record.
    *)
    val scan: schema -> pack t

    (** Use [unpack pack index nym] to unpack from [pack] the value for the
        field named by [index] with the type [nym].

        Note well: fields with {i Optional} type in the {i schema} are packed
        with the corresponding [Cf_type.Option] type nym, and the unpacked
        value is either [Some v] if the field is present in the scanned record,
        or [None] if the field was not present.
    *)
    val unpack: pack -> index -> 'a Cf_type.nym -> 'a form
end

(** Use [Create(B)] to make a record scanning module with basis [B]. *)
module Create(B: Basis): Profile
   with type index := B.Index.t
    and type 'a form := 'a B.Form.t
    and type 'a t := 'a B.Scan.t

(*--- End ---*)
