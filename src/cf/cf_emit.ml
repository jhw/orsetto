(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type ('a, 'b) t = F of ('a -> 'b -> unit) [@@caml.unboxed]

let apply (F aux) a b = aux a b

let nil = let f _ _ = () in F f

let seal (F aux) v = F (fun out () -> aux out v)
let defer f = let aux out v = let F f = f v in f out () in F aux
let map f (F aux) = F (fun out v -> aux out (f v))

let coded sch =
    let f exr v =
        let exr = (exr :> Cf_encode.emitter) in
        exr#emit sch v
    in
    F f

let pair ?(sep = nil) (F f1) (F f2) =
    let aux out (v1, v2) =
        f1 out v1;
        let F f = sep in f out ();
        f2 out v2
    in
    F aux

let opt ?(none = nil) (F f) =
    let aux out = function
        | None -> let F f = none in f out ()
        | Some v -> f out v
    in
    F aux

let seq =
    let rec loop w3 tl =
        match tl () with
        | Seq.Nil ->
            ()
        | Seq.Cons (hd, tl) ->
            let out, sep, item = w3 in
            sep out ();
            item out hd;
            (loop[@tailcall]) w3 tl
    in
    let start (F sep) (F item) out tl =
        match tl () with
        | Seq.Nil ->
            ()
        | Seq.Cons (hd, tl) ->
            item out hd;
            (loop[@tailcall]) (out, sep, item) tl
    in
    let enter ?(sep = nil) item = F (start sep item) in
    enter

let group =
    let rec loop w2 tl =
        match tl () with
        | Seq.Nil ->
            ()
        | Seq.Cons (F hd, tl) ->
            let out, sep = w2 in
            sep out ();
            hd out ();
            (loop[@tailcall]) w2 tl
    in
    let start (F sep) tl out () =
        match tl () with
        | Seq.Nil ->
            ()
        | Seq.Cons (F hd, tl) ->
            hd out ();
            (loop[@tailcall]) (out, sep) tl
    in
    let enter ?(sep = nil) boxes = F (start sep boxes) in
    enter

let encl ~a:(F fa) ~b:(F fb) (F f) =
    let aux out v =
        fa out ();
        f out v;
        fb out ()
    in
    F aux

module Render = struct
    module type Output = sig type channel end

    module type Basis = sig
        type channel

        val primitive: 'a Cf_type.nym -> (channel, 'a) t

        val control: [
            | `Default
            | `Special of
                (channel, 'a) t -> ('a, 'b) Cf_data_render.control ->
                (channel, 'b) t
        ]

        val pair:
            'k Cf_data_render.pair -> (channel, unit) t ->
            (channel, unit) t -> (channel, unit) t

        val sequence:
            'k Cf_data_render.container -> (channel, unit) t Seq.t ->
            (channel, unit) t
    end

    let basis_control:
        type a b c. (a, b) t -> (b, c) Cf_data_render.control -> (a, c) t
        = fun scheme control ->
            match control with
            | Cf_data_render.Cast f ->
                map f scheme
            | Cf_data_render.Default v ->
                map (Option.value ~default:v) scheme
            | _ ->
                invalid_arg @@ __MODULE__ ^ ".Render: invalid control variant"

    module Create(Out: Output)(B: Basis with type channel := Out.channel)
        = struct
            module Aux = struct
                type channel = Out.channel

                type 'v scheme = (channel, 'v) t
                type packet = (channel, unit) t

                let packet = seal
                let delegate = defer

                include B

                let control =
                    match control with
                    | `Default -> basis_control
                    | `Special f -> f
            end

            include Cf_data_render.Create(Aux)
        end
end

module To_buffer = struct
    type nonrec 'a t = (Buffer.t, 'a) t

    let char = F Buffer.add_char
    let string = F Buffer.add_string
    let bytes = F Buffer.add_bytes

    let string_slice =
        let f buf ss =
            let s, i, n = Cf_slice.(ss.vector, ss.start, length ss) in
            Buffer.add_substring buf s i n
        in
        F f

    let bytes_slice =
        let f buf ss =
            let b, i, n = Cf_slice.(ss.vector, ss.start, length ss) in
            Buffer.add_subbytes buf b i n
        in
        F f

    let utf_8_uchar = F Buffer.add_utf_8_uchar
    let utf_16be_uchar = F Buffer.add_utf_16be_uchar
    let utf_16le_uchar = F Buffer.add_utf_16le_uchar

    let substitute sub =
        let f buf s = Buffer.add_substitute buf sub s in
        F f

    let buffer = F Buffer.add_buffer

    let channel n =
        let f buf cin = Buffer.add_channel buf cin n in
        F f

    let to_string (F aux) v =
        let b = Buffer.create 0 in
        aux b v;
        Buffer.contents b

    module Render = struct
        module Output = struct type channel = Buffer.t end

        module Create(B: Render.Basis with type channel := Buffer.t) =
            Render.Create(Output)(B)
    end
end

module To_formatter = struct
    type nonrec 'a t = (Format.formatter, 'a) t

    let char = F Format.pp_print_char
    let string = F Format.pp_print_string
    let int = F Format.pp_print_int
    let float = F Format.pp_print_float
    let bool = F Format.pp_print_bool

    let break offset =
        let aux pp nspaces = Format.pp_print_break pp nspaces offset in
        F aux

    let space = F Format.pp_print_space
    let cut = F Format.pp_print_cut

    let boxf openf (F inner) indent pp v =
        openf pp indent;
        inner pp v;
        Format.pp_close_box pp ()

    let box indent inner = F (boxf Format.pp_open_box inner indent)
    let hbox inner = F (boxf Format.pp_open_hbox inner ())
    let vbox indent inner = F (boxf Format.pp_open_vbox inner indent)
    let hvbox indent inner = F (boxf Format.pp_open_hvbox inner indent)
    let hovbox indent inner = F (boxf Format.pp_open_hovbox inner indent)

    let ch_lbracket = seal char '['
    let ch_lbrace = seal char '{'
    let ch_rbrace = seal char '}'
    let ch_rbracket = seal char ']'
    let ch_lparen = seal char '('
    let ch_rparen = seal char ')'
    let ch_quote = seal char '"'

    let brace inner = encl ~a:ch_lbrace ~b:ch_rbrace inner
    let bracket inner = encl ~a:ch_lbracket ~b:ch_rbracket inner
    let paren inner = encl ~a:ch_lparen ~b:ch_rparen inner
    let quote inner = encl ~a:ch_quote ~b:ch_quote inner

    let string_as n =
        let aux pp s = Format.pp_print_as pp n s in
        F aux

    let seq m = seq ~sep:cut m
    let group m = group ~sep:cut m

    let to_string (F aux) v =
        let b = Buffer.create 0 in
        let pp = Format.formatter_of_buffer b in
        aux pp v;
        Format.pp_print_flush pp ();
        Buffer.contents b

    let to_channel oc (F aux) v =
        let pp = Format.formatter_of_out_channel oc in
        aux pp v;
        Format.pp_print_flush pp ()

    module Render = struct
        module Output = struct type channel = Format.formatter end

        module Create(B: Render.Basis with type channel := Format.formatter) =
            Render.Create(Output)(B)
    end
end

(*--- End ---*)
