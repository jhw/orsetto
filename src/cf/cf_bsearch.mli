(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Binary search algorithm and data structures. *)

(** {6 Overview}

    This module implements generalized binary search functions along with a
    functorial interface that generalizes static sets and maps implemented as
    vectors in multiplicative binary search order. A specialization is provided
    for character sets and maps implemented with strings as the vector.
*)

(** {6 Types} *)

(** A comparator function for use with the [search] function (see below). *)
type 'i cmp = Cmp of ('i -> int) [@@ocaml.unboxed]

(** A return finalization configuration for use with the [search] function (see
    below).
*)
type ('i, 'r) ret = Ret of {
    none: unit -> 'r;
    some: 'i -> 'r;
}

(** Use [module B: Basis = struct ... end] to define a type [t] and associated
    functions that provide the operations on it required for binary searching
    in the range between two values of type [t].
*)
module type Basis = sig

    (** Basis values are totally ordered. *)
    include Cf_relations.Order

    (** Search uses [succ c] to find the successor of [c]. *)
    val succ: t -> t

    (** Search uses [pred c] to find the predecessor of [c]. *)
    val pred: t -> t

    (** Search uses [center a b] to find the center between [a] and [b]. *)
    val center: t -> t -> t
end

(** The distinguished basis for OCaml [char] values. *)
module Char_basis: Basis with type t = char

(** The distinguished basis for OCaml [int] values. *)
module Int_basis: Basis with type t = int

(** The distinguished basis for OCaml [float] values. *)
module Float_basis: Basis with type t = float

(** The signature of the module returned by the [Create(B)] functor. *)
module type Profile = sig

    (** Abstract type of a binary search cursor. *)
    type t

    (** Use [find r f a b] to search for the cursor [c] between [a] and [b]
        inclusive, for which applying the ordering function [f] returns zero.
        If the cursor is found in the search, then [r.some] is applied to it.
        Otherwise, [r.none ()] is applied.
    *)
    val find: (t, 'r) ret -> t cmp -> t -> t -> 'r

    (** Use [search f a b] to find [Some c] between [a] and [b] inclusive for
        which applying the ordering function [f] returns zero. Otherwise, if
        no such cursor exists, then returns [None].
    *)
    val search: t cmp -> t -> t -> t option

    (** Use [opt f a b] to find the cursor between [a] and [b] inclusive for
        which applying the ordering function [f] returns zero. Otherwise, if
        no such cursor exists, then raises [Not_found].
    *)
    val require: t cmp -> t -> t -> t
end

(** {6 General Interface} *)

(** Use [Create(B)] to make a module that provides binary searches using the
    index type defined with the basis [B].
*)
module Create(B: Basis): Profile with type t := B.t

(** The profile of binary searches with [char] type index. *)
module Char: Profile with type t := char

(** The profile of binary searches with [int] type index. *)
module Int: Profile with type t := int

(** The profile of binary searches with [float] type index. *)
module Float: Profile with type t := float

(*--- End ---*)
