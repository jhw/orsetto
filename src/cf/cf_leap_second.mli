(*---------------------------------------------------------------------------*
  $Change$
  Copyright (C) 2021-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Handling the leap second archive *)

(** {6 Overview}

    This module defines a data structure representing the history of leap
    seconds published in Bulletin C by the International Earth Rotation and
    Reference Systems Service (IERS). This data is used in converting between
    the Coordinated Universal Time (UTC) and Temps Atomique International (TAI)
    timescales. Bulletin C is published every six months and whenever necessary
    and a machine readable archive of the historical record is provided as a
    public service by the United States National Institute of Standards and
    Technology (NIST) and the Internet Engineering Task Force (IETF) at the
    following locations:

        <https://www.ietf.org/timezones/data/leap-seconds.list>
        <ftp://ftp.nist.gov/pub/time/leap-seconds.list>

    The data intrinsically inclued in the current implementation expires on
    December 28, 2021. Use the [load_from_nist_file] function to use a more
    recent update of the leap second archive.
*)

(** {6 Types} *)

(** An entry in the archive comprising the TAI epoch of an announced leap
    second and the new offset from UTC to TAI in seconds after the leap.
*)
type entry = private Entry of {
    epoch: Cf_tai64.t;
    offset: int;
}

(** A record of known leap seconds in reverse chronological order. *)
type archive = private Archive of {
    current: int;
    updated: Cf_tai64.t;
    expires: Cf_tai64.t;
    history: entry list;
}

(** {6 Values} *)

(** Use [current ()] to obtain the current leap second archive. *)
val current: unit -> archive

(** Use [load_from_nist_file filename] to override the internal archive data
    with the content of [filename], a file copied from the data published by
    NIST and IETF. Raise an exception if the file cannot be read or a parsing
    error arises.
*)
val load_from_nist_file: string -> unit

(** Use [search t] to search the leap second archive with [t] to find out
    whether it appears in the archive as an announcement of a leap second.
    The integer returned is the offset from UTC to TAI in seconds immediately
    after [t].
*)
val search: Cf_tai64.t -> bool * int

(*--- $File$ ---*)
