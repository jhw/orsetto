(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** The state monad and its operators. *)

(** {6 Overview}

    The state monad represents the computation of transtions on its
    encapsulated state. It can be used as a basic element of a deterministic
    finite automaton.
*)

(** {6 Types} *)

(** The state monad type. *)
type ('s, 'r) t = 's -> 's * 'r
module Basis: Cf_monad.Binary.Basis with type ('s, 'r) t := ('s, 'r) t
include Cf_monad.Binary.Profile with type ('s, 'r) t := ('s, 'r) t

(** {6 Operators} *)

(** A monad that returns [unit] and performs no operation. *)
val nil: ('s, unit) t

(** A monad that returns the encapsulate state. *)
val load: ('s, 's) t

(** Use [store s] to produce a monad with [s] as the value of its encapsulated
    state.
*)
val store: 's -> ('s, unit) t

(** Use [modify f] to produce a monad that applies [f] to the encapsulated
    state to obtain a new state value, and which returns the unit value as its
    result when evaluated.
*)
val modify: ('s -> 's) -> ('s, unit) t

(** Use [field f] to produce a monad that returns the result of applying [f] to
    the value of the encapsulated state.
*)
val field: ('s -> 'r) -> ('s, 'r) t

(** Use [eval m s] to evaluate the monad, which produces a function from an
    initial state [s] to a final state.
*)
val eval: ('s, unit) t -> 's -> 's

(*--- End ---*)
