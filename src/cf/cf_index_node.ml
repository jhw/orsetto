(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Profile = sig
    type index
    type +'a t

    val cons: index -> 'a -> 'a t
    val index: 'a t -> index
    val obj: 'a t -> 'a

    val icompare: index -> 'a t -> int
    val compare: 'a t -> 'a t -> int
end

module Unary(E: Cf_relations.Order) = struct
    type index = E.t
    type 'a t = index

    let cons i _ = i
    let index i = i
    let obj _ = assert false

    let icompare = E.compare
    let compare = E.compare
end

module Binary(K: Cf_relations.Order) = struct
    type index = K.t
    type 'a t = index * 'a

    let cons k v = k, v
    let index (k, _) = k
    let obj (_, v) = v

    let icompare x (y, _) = K.compare x y
    let compare (x, _) (y, _) = K.compare x y
end

(*--- End ---*)
