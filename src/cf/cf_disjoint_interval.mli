(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Data structures founded on vectors of sorted disjoint intervals. *)

(** {6 Overview}

    This module implements set and map data structures using multiplicative
    binary search data structures where keys correspond to table elements that
    comprise disjoint intervals. Distinguished instances are provided for sets
    and maps where elements are disjoint intervals of [char] and [int] types.
    These are useful for fast cache-friendly instances of ASCII and Unicode
    character sets and maps.
*)

(** {6 Types and Signatures} *)

(** A private type representing a disjoint interval. *)
type 'a t = private T of { a: 'a; b: 'a }

(** Modules with core functions for use with disjoint intervals. *)
module Core: sig

    (** The signature of modules representing disjoint intervals. *)
    module type Profile = sig

        (** The type of interval limit. *)
        type limit

        (** Total order of disjoint intervals. *)
        val compare: limit t -> limit t -> int

        (** Use [lift s] to lift a sequence of values into a sequence of
            disjoint intervals.
        *)
        val lift: limit Seq.t -> limit t Seq.t

        (** Use [lift2 f s] to lift a sequence of key/value pairs into a
            sequence of interval/value pairs. Applies [f] to test values for
            equivalence. The value of the first key in the interval is used
            for all keys in the interval.
        *)
        val lift2:
            ('a -> 'a -> bool) -> (limit * 'a) Seq.t ->
            (limit t * 'a) Seq.t
    end

    (** Use [Create(B)] to make a module representing a disjoint interval. *)
    module Create(B: Cf_bsearch.Basis): Profile with type limit := B.t

    (** A distinguished module for disjoint intervals of [char] values. *)
    module Of_char: module type of Create(Cf_bsearch.Char_basis)

    (** A distinguished module for disjoint intervals of [int] values. *)
    module Of_int: module type of Create(Cf_bsearch.Int_basis)
end [@@caml.alert deprecated "Private use only!"]

(** Modules of distinguished set implementations. *)
module Set: sig

    (** Use [Create(B)] to make a set using disjoint intervals of a given
        binary searchable type.
    *)
    module Create(B: Cf_bsearch.Basis): Cf_bsearch_data.Set.Profile
       with type search := B.t
        and type element := B.t t
        and type Unsafe.index := int
        and type Unsafe.vector := B.t array

    (** A character set using disjoint intervals. *)
    module Of_char: Cf_bsearch_data.Set.Profile
       with type search := char
        and type element := char t
        and type Unsafe.index := int
        and type Unsafe.vector := string

    (** An integer set using disjoint intervals. *)
    module Of_int: module type of Create(Cf_bsearch.Int_basis)

    (** An floating point number set using disjoint intervals. *)
    module Of_float: module type of Create(Cf_bsearch.Float_basis)
end

(** Modules of distinguished map implementations. *)
module Map: sig

    (** Use [Create(B)] to make a map using disjoint intervals of a given
        binary searchable type.
    *)
    module Create(B: Cf_bsearch.Basis): Cf_bsearch_data.Map.Profile
       with type search := B.t
        and type key := B.t t
        and type Unsafe.index := int
        and type Unsafe.vector := B.t array
        and type 'a Unsafe.content := 'a array

    (** A map using character disjoint interval keys. *)
    module Of_char: Cf_bsearch_data.Map.Profile
       with type search := char
        and type key := char t
        and type Unsafe.index := int
        and type Unsafe.vector := string
        and type 'a Unsafe.content := 'a array

    (** An integer map using disjoint intervals. *)
    module Of_int: module type of Create(Cf_bsearch.Int_basis)

    (** An floating point number map using disjoint intervals. *)
    module Of_float: module type of Create(Cf_bsearch.Float_basis)
end

(*--- End ---*)
