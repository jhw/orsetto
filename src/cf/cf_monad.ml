(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Core = Cf_monad_core

module Unary = struct
    module type Basis = Core.Unary.Basis

    module type Profile = sig
        type +'r t

        include Core.Unary.Profile with type 'r t := 'r t
        include Cf_seqmonad.Functor.Unary with type 'r t := 'r t
    end

    module Create(B: Basis) = struct
        include Core.Unary.Create(B)
        include Cf_seqmonad.Functor.Unary(B)
    end
end

module Binary = struct
    module type Basis = Core.Binary.Basis

    module type Profile = sig
        type ('m, +'r) t

        include Core.Binary.Profile with type ('m, 'r) t := ('m, 'r) t
        include Cf_seqmonad.Functor.Binary
            with type ('m, 'r) t := ('m, 'r) t
    end

    module Create(B: Basis) = struct
        include Core.Binary.Create(B)
        include Cf_seqmonad.Functor.Binary(B)
    end
end

module Trinary = struct
    module type Basis = Core.Trinary.Basis

    module type Profile = sig
        type ('p, 'q, +'r) t

        include Core.Trinary.Profile
            with type ('p, 'q, 'r) t := ('p, 'q, 'r) t

        include Cf_seqmonad.Functor.Trinary
            with type ('p, 'q, 'r) t := ('p, 'q, 'r) t
    end

    module Create(B: Basis) = struct
        include Core.Trinary.Create(B)
        include Cf_seqmonad.Functor.Trinary(B)
    end
end

(*--- End ---*)
