/*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/

#ifndef _CF_ENDIAN_LITTLE_P_H
#define _CF_ENDIAN_LITTLE_P_H

#include "cf_endian_aux.h"

#ifdef ARCH_BIG_ENDIAN

#define CF_ENDIAN_L16_OF_NAT(C) CF_ENDIAN_SWAP16(C)
#define CF_ENDIAN_L32_OF_NAT(C) CF_ENDIAN_SWAP32(C)
#define CF_ENDIAN_L64_OF_NAT(C) CF_ENDIAN_SWAP64(C)

#define CF_ENDIAN_L16_TO_NAT(C) CF_ENDIAN_SWAP16(C)
#define CF_ENDIAN_L32_TO_NAT(C) CF_ENDIAN_SWAP32(C)
#define CF_ENDIAN_L64_TO_NAT(C) CF_ENDIAN_SWAP64(C)

#else // !defined(ARCH_BIG_ENDIAN)

#define CF_ENDIAN_L16_OF_NAT(C) (C)
#define CF_ENDIAN_L32_OF_NAT(C) (C)
#define CF_ENDIAN_L64_OF_NAT(C) (C)

#define CF_ENDIAN_L16_TO_NAT(C) (C)
#define CF_ENDIAN_L32_TO_NAT(C) (C)
#define CF_ENDIAN_L64_TO_NAT(C) (C)

#endif

static inline uint16_t cf_endian_ld16l_aligned(const uint16_t* p)
{
    uint16_t n = *p;
    return CF_ENDIAN_L16_TO_NAT(n);
}

static inline uint32_t cf_endian_ld32l_aligned(const uint32_t* p)
{
    uint32_t n = *p;
    return CF_ENDIAN_L32_TO_NAT(n);
}

static inline uint64_t cf_endian_ld64l_aligned(const uint64_t* p)
{
    uint64_t n = *p;
    return CF_ENDIAN_L64_TO_NAT(n);
}

static inline void cf_endian_st16l_aligned(uint16_t* p, uint16_t n)
{
    *p = CF_ENDIAN_L16_OF_NAT(n);
}

static inline void cf_endian_st32l_aligned(uint32_t* p, uint32_t n)
{
    *p = CF_ENDIAN_L32_OF_NAT(n);
}

static inline void cf_endian_st64l_aligned(uint64_t* p, uint64_t n)
{
    *p = CF_ENDIAN_L64_OF_NAT(n);
}

static inline uint16_t cf_endian_ld16l_copy(const void* p)
{
    union CF_ENDIAN_ALIASING(16) u;
    memcpy(u.b, p, sizeof u.n);
    return CF_ENDIAN_L16_TO_NAT(u.n);
}

static inline uint32_t cf_endian_ld32l_copy(const void* p)
{
    union CF_ENDIAN_ALIASING(32) u;
    memcpy(u.b, p, sizeof u.n);
    return CF_ENDIAN_L32_TO_NAT(u.n);
}

static inline uint64_t cf_endian_ld64l_copy(const void* p)
{
    union CF_ENDIAN_ALIASING(64) u;
    memcpy(u.b, p, sizeof u.n);
    return CF_ENDIAN_L64_TO_NAT(u.n);
}

static inline void cf_endian_st16l_copy(void* p, uint16_t n)
{
    n = CF_ENDIAN_L16_OF_NAT(n);
    memcpy(p, &n, sizeof n);
}

static inline void cf_endian_st32l_copy(void* p, uint32_t n)
{
    n = CF_ENDIAN_L32_OF_NAT(n);
    memcpy(p, &n, sizeof n);
}

static inline void cf_endian_st64l_copy(void* p, uint64_t n)
{
    n = CF_ENDIAN_L64_OF_NAT(n);
    memcpy(p, &n, sizeof n);
}

static inline uint16_t cf_endian_ld16l(const void* p)
{
    bool aligned = cf_endian_is_aligned(p, 16);
    return aligned
        ? cf_endian_ld16l_aligned(p)
        : cf_endian_ld16l_copy(p);
}

static inline uint32_t cf_endian_ld32l(const void* p)
{
    bool aligned = cf_endian_is_aligned(p, 32);
    return aligned
        ? cf_endian_ld32l_aligned(p)
        : cf_endian_ld32l_copy(p);
}

static inline uint64_t cf_endian_ld64l(const void* p)
{
    bool aligned = cf_endian_is_aligned(p, 64);
    return aligned
        ? cf_endian_ld64l_aligned(p)
        : cf_endian_ld64l_copy(p);
}

static inline void cf_endian_st16l(void* p, uint16_t n)
{
    cf_endian_is_aligned(p, 16)
        ? cf_endian_st16l_aligned(p, n)
        : cf_endian_st16l_copy(p, n);
}

static inline void cf_endian_st32l(void* p, uint32_t n)
{
    cf_endian_is_aligned(p, 32)
        ? cf_endian_st32l_aligned(p, n)
        : cf_endian_st32l_copy(p, n);
}

static inline void cf_endian_st64l(void* p, uint64_t n)
{
    cf_endian_is_aligned(p, 64)
        ? cf_endian_st64l_aligned(p, n)
        : cf_endian_st64l_copy(p, n);
}

#endif /* defined(_CF_ENDIAN_LITTLE_P_H) */

/*--- $File: src/cf/cf_endian_little_aux.h $ ---*/
