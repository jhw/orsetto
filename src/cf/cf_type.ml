(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type (_, _) eq = Eq: ('a, 'a) eq

let refl = Eq
let symm (type a b) (Eq : (a, b) eq) = (Eq : (b, a) eq)
let trans (type a b c) (Eq : (a, b) eq) (Eq : (b, c) eq) = (Eq : (a, c) eq)
let coerce (type a b) (Eq : (a, b) eq) (v : a) = (v : b)

type _ nym = ..

type opaque = Witness: 'a * 'a nym -> opaque

let witness (type a) (n : a nym) (v : a) = Witness (v, n)

type _ nym += Unit: unit nym
type _ nym += Bool: bool nym
type _ nym += Char: char nym
type _ nym += Uchar: Uchar.t nym
type _ nym += String: string nym
type _ nym += Bytes: bytes nym
type _ nym += Int: int nym
type _ nym += Int32: int32 nym
type _ nym += Int64: int64 nym
type _ nym += Float: float nym
type _ nym += Option: 'a nym -> 'a option nym
type _ nym += Pair: 'a nym  * 'b nym -> ('a * 'b) nym
type _ nym += Seq: 'a nym -> 'a Seq.t nym
type _ nym += Opaque: opaque nym

exception Type_error

class horizon = object(self)
    method equiv: type a b. a nym -> b nym -> (a, b) eq = fun a b ->
        match[@warning "-4"] a, b with
        | String, String -> Eq
        | Int, Int -> Eq
        | Bool, Bool -> Eq
        | Opaque, Opaque -> Eq
        | Unit, Unit -> Eq
        | Int64, Int64 -> Eq
        | Int32, Int32 -> Eq
        | Seq a, Seq b ->
            let Eq = self#equiv a b in Eq
        | Pair (a1, a2), Pair (b1, b2) ->
            let Eq = self#equiv a1 b1 in
            let Eq = self#equiv a2 b2 in
            Eq
        | Option a, Option b ->
            let Eq = self#equiv a b in Eq
        | Float, Float -> Eq
        | Char, Char -> Eq
        | Uchar, Uchar -> Eq
        | Bytes, Bytes -> Eq
        | _ -> raise Type_error
end

module type Form = sig
    val req: 'a nym -> opaque -> 'a
    val opt: 'a nym -> opaque -> 'a option
    val eq: 'a nym -> 'b nym -> ('a, 'b) eq
    val ck: 'a nym -> opaque -> bool
end

let form r =
    let r = (r :> horizon) in
    let module M = struct
        let req (type t) (a : t nym) (Witness (v, b)) =
            let eq = r#equiv b a in coerce eq v

        let opt (type t) (a : t nym) (Witness (v, b)) =
            match r#equiv b a with
            | exception Type_error -> None
            | eq -> Some (coerce eq v)

        let eq (type a) (type b) (a : a nym) (b : b nym) : (a, b) eq =
            r#equiv a b

        let ck (type t) (a : t nym) (Witness (_, b)) =
            try let Eq = r#equiv b a in true with Type_error -> false
    end
    in (module M : Form)

include (val form (new horizon) : Form)

(*--- End ---*)
