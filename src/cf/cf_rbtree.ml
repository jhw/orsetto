(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type 'a t =
    | Z
    | R of 'a * 'a t * 'a t
    | B of 'a * 'a t * 'a t

module Core(N: Cf_index_node.Profile) = struct
    type nonrec 'a t = 'a N.t t

    (*
    type 'a ic = IC_o | IC_l of 'a N.t | IC_r of 'a N.t

    let invariant_key_compare_ x = function
        | IC_o ->
            ()
        | IC_r y when N.compare x y > 0 ->
            ()
        | IC_l y when N.compare x y < 0 ->
            ()
        | _ ->
            failwith "key out of order"

    let invariant_print_aux_ =
        let rec loop (ic : 'a ic) = function
            | Z ->
                Format.printf "Z@\n";
                0
            | R (_, R (_, _, _), _) ->
                Format.printf "@[<2>R@\nR@\n";
                failwith "red node has red child"
            | R (x, a, R (_, _, _)) ->
                Format.printf "@[<2>R@\n";
                let _ = loop (IC_l x) a in
                Format.printf "R@\n";
                failwith "red node has red child"
            | R (x, a, b) ->
                Format.printf "@[<2>R@\n";
                invariant_key_compare_ x ic;
                let a = loop (IC_l x) a in
                let b = loop (IC_r x) b in
                Format.printf "a=%d,b=%d@]@\n" a b;
                begin
                    match a, b with
                    | h1, h2 when h1 = h2 -> h1
                    | _ -> failwith "imbalanced black height"
                end
            | B (x, a, b) ->
                Format.printf "@[<2>B@\n";
                invariant_key_compare_ x ic;
                let a = loop (IC_l x) a in
                let b = loop (IC_r x) b in
                Format.printf "a=%d,b=%d@]@\n" a b;
                begin
                    match a, b with
                    | h1, h2 when h1 = h2 -> h1 + 1
                    | _ -> failwith "imbalanced black height"
                end
        in
        fun u ->
            Format.printf "| >invariant:@\n  @[<2>";
            match try `Okay (loop IC_o u) with x -> `Error x with
            | `Okay h ->
                Format.printf "@]@\n| <invariant (height=%d)@." h;
                flush stdout;
                true
            | `Error x ->
                Format.printf "!!!@.";
                flush stdout;
                raise x

    let invariant_noprint_aux_ =
        let rec loop = function
            | Z ->
                0
            | R (_, R (_, _, _), _)
            | R (_, _, R (_, _, _)) ->
                failwith "red node with red child"
            | R (_, a, b) ->
                begin
                    match loop a, loop b with
                    | h1, h2 when h1 = h2 -> h1
                    | _ -> failwith "imbalanced black height"
                end
            | B (_, a, b) ->
                begin
                    match loop a, loop b with
                    | h1, h2 when h1 = h2 -> h1 + 1
                    | _ -> failwith "imbalanced black height"
                end
        in
        fun u ->
            try ignore (loop u); true with Failure _ -> false
            (* ignore (loop u); true *)
            (* try ignore (loop u); true with Failure _ as x -> false *)

    let invariant_aux_ = invariant_noprint_aux_
    *)

    let nil = Z
    let one n = B (n, Z, Z)

    let empty = function Z -> true | R _ | B _ -> false

    let rec size = function
        | Z ->
            0
        | R (_, a, b)
        | B (_, a, b) ->
            succ (size a + size b)

    let rec min = function
        | Z ->
            raise Not_found
        | R (x, Z, _)
        | B (x, Z, _) ->
            x
        | R (_, y, _)
        | B (_, y, _) ->
            (min[@tailcall]) y

    let rec max = function
        | Z ->
            raise Not_found
        | R (x, _, Z)
        | B (x, _, Z) ->
            x
        | R (_, _, y)
        | B (_, _, y) ->
            (max[@tailcall]) y

    let rec require key = function
        | Z ->
            raise Not_found
        | (R (n, a, b) | B (n, a, b)) ->
            let d = N.icompare key n in
            if d = 0 then
                n
            else
                (require[@tailcall]) key (if d < 0 then a else b)

    let search key tree =
        try Some (require key tree) with Not_found -> None

    let rec member key = function
        | Z ->
            false
        | (R (n, a, b) | B (n, a, b)) ->
            let d = N.icompare key n in
            if d = 0 then
                true
            else
                (member[@tailcall]) key (if d < 0 then a else b)

    let l_balance_ z n1 n2 =
        match n1, n2 with
        | R (y, R (x, a, b), c), d
        | R (x, a, R (y, b, c)), d ->
            R (y, B (x, a, b), B (z, c, d))
        | (Z | R _ | B _), _ ->
            B (z, n1, n2)

    let r_balance_ z n1 n2 =
        match n1, n2 with
        | a, R (y, b, R (x, c, d))
        | a, R (x, R (y, b, c), d) ->
            R (y, B (z, a, b), B (x, c, d))
        | _, (Z | R _ | B _) ->
            B (z, n1, n2)

    let rec replace_aux_ x = function
        | Z ->
            R (x, Z, Z)
        | R (y, a, b) ->
            let d = N.compare x y in
            if d < 0 then
                R (y, replace_aux_ x a, b)
            else if d > 0 then
                R (y, a, replace_aux_ x b)
            else
                R (x, a, b)
        | B (y, a, b) ->
            let d = N.compare x y in
            if d < 0 then
                l_balance_ y (replace_aux_ x a) b
            else if d > 0 then
                r_balance_ y a (replace_aux_ x b)
            else
                B (x, a, b)

    let force_black_ = function
        | R (n, a, b) -> B (n, a, b)
        | (B _ | Z as u) -> u

    let replace x u =
        let u = force_black_ (replace_aux_ x u) in
        (* assert (invariant_aux_ u); *)
        u

    let l_repair_ = function
        | R (x, B (y, a, b), c) ->
            l_balance_ x (R (y, a, b)) c, false
        | B (x, B (y, a, b), c) ->
            l_balance_ x (R (y, a, b)) c, true
        | B (x, R (y, a, B (z, b, c)), d) ->
            B (y, a, l_balance_ x (R (z, b, c)) d), false
        | (Z | R _ | B _) ->
            assert (not true);
            Z, false

    let r_repair_ = function
        | R (x, a, B (y, b, c)) ->
            r_balance_ x a (R (y, b, c)), false
        | B (x, a, B (y, b, c)) ->
            r_balance_ x a (R (y, b, c)), true
        | B (x, a, R (y, B (z, b, c), d)) ->
            B (y, r_balance_ x a (R (z, b, c)), d), false
        | (Z | R _ | B _) ->
            assert (not true);
            Z, false

    let dup_color_ x a b = function
        | Z -> assert (not true); R (x, a, b)
        | R _ -> R (x, a, b)
        | B _ -> B (x, a, b)

    let rec extract_min_ = function
        | Z
        | B (_, Z, B _) ->
            assert (not true);
            extract_min_ Z
        | B (x, Z, Z) ->
            Z, x, true
        | B (x, Z, R (y, a, b)) ->
            B (y, a, b), x, false
        | R (x, Z, a) ->
            a, x, false
        | (R (x, a, b) | B (x, a, b)) as n ->
            let a, m, r = extract_min_ a in
            let n = dup_color_ x a b n in
            if r then
                let n, r = r_repair_ n in n, m, r
            else
                n, m, false

    let cons_r_ x a b = R (x, a, b)
    let cons_b_ x a b = B (x, a, b)

    let ifxz_r_ y a = a, false, y

    let ifxz_b_ y = function
        | R (z, a, b) -> B (z, a, b), false, y
        | (Z | B _ as u) -> u, true, y

    let rec extract_aux_ k = function
        | Z -> raise Not_found
        | B (y, a, b) -> (extract_aux_flip_[@tailcall]) cons_b_ ifxz_b_ k y a b
        | R (y, a, b) -> (extract_aux_flip_[@tailcall]) cons_r_ ifxz_r_ k y a b

    and extract_aux_flip_ cons ifxz k y a b =
        let d = N.icompare k y in
        if d < 0 then begin
            let a, r, v = extract_aux_ k a in
            let n = cons y a b in
            let n, r = if r then r_repair_ n else n, false in
            n, r, v
        end
        else if d > 0 then begin
            let b, r, v = extract_aux_ k b in
            let n = cons y a b in
            let n, r = if r then l_repair_ n else n, false in
            n, r, v
        end
        else if b = Z then begin
            (ifxz[@tailcall]) y a
        end
        else begin
            let b, z, d = extract_min_ b in
            let n = cons z a b in
            let n, r = if d then l_repair_ n else n, false in
            n, r, y
        end

    let extract k u =
        let u, _, v = extract_aux_ k u in
        (* assert (invariant_aux_ u); *)
        v, u

    let ifdz_r_ a = a, false
    let ifdz_b_ = function
        | R (z, a, b) -> B (z, a, b), false
        | (Z | B _ as u) -> u, true

    let rec delete_aux_ k = function
        | Z -> raise Not_found
        | B (y, a, b) -> (delete_aux_flip_[@tailcall]) cons_b_ ifdz_b_ k y a b
        | R (y, a, b) -> (delete_aux_flip_[@tailcall]) cons_r_ ifdz_r_ k y a b

    and delete_aux_flip_ cons ifdz k y a b =
        let d = N.icompare k y in
        if d > 0 then begin
            let b, r = delete_aux_ k b in
            let n = cons y a b in
            if r then l_repair_ n else n, false
        end
        else if d < 0 then begin
            let a, r = delete_aux_ k a in
            let n = cons y a b in
            if r then r_repair_ n else n, false
        end
        else match b with
        | Z ->
            (ifdz[@tailcall]) a
        | (R _ | B _ as b) ->
            let b, z, d = extract_min_ b in
            let n = cons z a b in
            if d then l_repair_ n else n, false

    let delete k u =
        try
            let u, _ = delete_aux_ k u in
            (* assert (invariant_aux_ u); *)
            u
        with
        | Not_found ->
            u

    let rec insert_aux_ x = function
        | Z ->
            R (x, Z, Z), None
        | R (y, a, b) ->
            let d = N.compare x y in
            if d < 0 then begin
                let a, xopt = insert_aux_ x a in
                R (y, a, b), xopt
            end
            else if d > 0 then begin
                let b, xopt = insert_aux_ x b in
                R (y, a, b), xopt
            end
            else
                R (x, a, b), Some y
        | B (y, a, b) ->
            let d = N.compare x y in
            if d < 0 then begin
                let a, xopt = insert_aux_ x a in
                l_balance_ y a b, xopt
            end
            else if d > 0 then begin
                let b, xopt = insert_aux_ x b in
                r_balance_ y a b, xopt
            end
            else begin
                R (x, a, b), Some y
            end

    let insert x u =
        let u, xopt = insert_aux_ x u in
        let u = force_black_ u in
        (* assert (invariant_aux_ u); *)
        u, xopt

    let rec modify key f = function
        | Z ->
            raise Not_found
        | (B (x, a, b) | R (x, a, b)) as u ->
            let d = N.icompare key x in
            if d < 0 then
                (dup_color_[@tailcall]) x (modify key f a) b u
            else if d > 0 then
                (dup_color_[@tailcall]) x a (modify key f b) u
            else begin
                let x = N.cons key (f (N.obj x)) in
                (dup_color_[@tailcall]) x a b u
            end

    let rec of_seq_aux_ u s =
        match s () with
        | Seq.Cons (x, s) -> (of_seq_aux_[@tailcall]) (replace x u) s
        | Seq.Nil -> u

    let of_seq s = (of_seq_aux_[@tailcall]) nil s

    type 'a digit =
        | Y of 'a t * 'a N.t
        | X of 'a t * 'a N.t * 'a t * 'a N.t

    let rec accum_incr_ n x = function
        | [] -> [ Y (n, x) ]
        | Y (m, y) :: t -> X (m, y, n, x) :: t
        | X (m, y, p, z) :: t -> Y (n, x) :: (accum_incr_ (B (y, m, p)) z t)

    let rec accum_decr_ n x = function
        | [] -> [ Y (n, x) ]
        | Y (m, y) :: t -> X (n, x, m, y) :: t
        | X (p, z, m, y) :: t -> Y (n, x) :: (accum_decr_ (B (y, p, m)) z t)

    let rec final_incr_ acc = function
        | [] -> acc
        | Y (m, y) :: t -> final_incr_ (B (y, m, acc)) t
        | X (m, y, p, z) :: t -> final_incr_ (B (y, m, R (z, p, acc))) t

    let rec final_decr_ acc = function
        | [] -> acc
        | Y (m, y) :: t -> final_decr_ (B (y, acc, m)) t
        | X (p, z, m, y) :: t -> final_decr_ (B (y, R (z, acc, p), m)) t

    let of_seq_incr =
        let rec loop last acc s =
            match s () with
            | Seq.Cons (hd, tl) ->
                if N.compare last hd > 0 then begin
                    let hd = replace hd (final_incr_ Z acc) in
                    (of_seq_aux_[@tailcall]) hd tl
                end
                else
                    (loop[@tailcall]) hd (accum_incr_ Z hd acc) tl
            | Seq.Nil ->
                (final_incr_[@tailcall]) Z acc
        in
        fun s ->
            match s () with
            | Seq.Nil -> Z
            | Seq.Cons (hd, tl) -> (loop[@tailcall]) hd [ Y (Z, hd) ] tl

    let of_seq_decr =
        let rec loop last acc s =
            match s () with
            | Seq.Cons (hd, tl) ->
                if N.compare last hd < 0 then begin
                    let hd = replace hd (final_decr_ Z acc) in
                    (of_seq_aux_[@tailcall]) hd tl
                end
                else
                    (loop[@tailcall]) hd (accum_decr_ Z hd acc) tl
            | Seq.Nil ->
                (final_incr_[@tailcall]) Z acc
        in
        fun s ->
            match s () with
            | Seq.Nil -> Z
            | Seq.Cons (hd, tl) -> (loop[@tailcall]) hd [ Y (Z, hd) ] tl

    let rec stack_min_ i = function
        | Z ->
            i
        | R (e, a, b)
        | B (e, a, b) ->
            (stack_min_[@tailcall]) ((e, b) :: i) a

    let rec stack_max_ i = function
        | Z ->
            i
        | R (e, a, b)
        | B (e, a, b) ->
            (stack_max_[@tailcall]) ((e, a) :: i) b

    let to_seq_aux_ =
        let rec loop f s () =
            match s with
            | (e, u) :: s ->
                let s = match u with Z -> s | (R _ | B _) -> f s u in
                Seq.Cons (e, loop f s)
            | [] ->
                Seq.Nil
        in
        fun f g u ->
            loop f (f g u)

    let to_seq_incr u = (to_seq_aux_[@tailcall]) stack_min_ [] u
    let to_seq_decr u = (to_seq_aux_[@tailcall]) stack_max_ [] u

    let rec to_seq_nearest_incr_aux_ key w u () =
        match u with
        | Z ->
            (to_seq_aux_[@tailcall]) stack_min_ w Z ()
        | R (n, a, b)
        | B (n, a, b) ->
            let d = N.icompare key n in
            if d = 0 then
                Seq.Cons (n, to_seq_aux_ stack_min_ w b)
            else
                let w, n = if d < 0 then (n, b) :: w, a else w, b in
                (to_seq_nearest_incr_aux_[@tailcall]) key w n ()

    let rec to_seq_nearest_decr_aux_ key w u () =
        match u with
        | Z ->
            (to_seq_aux_[@tailcall]) stack_max_ w Z ()
        | R (n, a, b)
        | B (n, a, b) ->
            let d = N.icompare key n in
            if d = 0 then
                Seq.Cons (n, to_seq_aux_ stack_max_ w a)
            else
                let w, n = if d > 0 then (n, a) :: w, b else w, a in
                (to_seq_nearest_decr_aux_[@tailcall]) key w n ()

    let to_seq_nearest_incr key u =
        (to_seq_nearest_incr_aux_[@tailcall]) key [] u

    let to_seq_nearest_decr key u =
        (to_seq_nearest_decr_aux_[@tailcall]) key [] u
end

module Set = struct
    module type Profile = sig
        type element
        type t

        val nil: t
        val one: element -> t
        val min: t -> element
        val max: t -> element
        val empty: t -> bool
        val size: t -> int
        val member: element -> t -> bool
        val compare: t -> t -> int
        val subset: t -> t -> bool
        val disjoint: t -> t -> bool
        val put: element -> t -> t
        val clear: element -> t -> t
        val union: t -> t -> t
        val diff: t -> t -> t
        val intersect: t -> t -> t

        val of_seq: element Seq.t -> t
        val of_seq_incr: element Seq.t -> t
        val of_seq_decr: element Seq.t -> t
        val to_seq_incr: t -> element Seq.t
        val to_seq_decr: t -> element Seq.t
        val to_seq_nearest_decr: element -> t -> element Seq.t
        val to_seq_nearest_incr: element -> t -> element Seq.t
    end

    module Create(E: Cf_relations.Order) = struct
        module Core = Core(Cf_index_node.Unary(E))

        type t = E.t Core.t

        let nil = Core.nil
        let one = Core.one

        let empty = Core.empty
        let size = Core.size
        let min = Core.min
        let max = Core.max
        let member = Core.member

        let put = Core.replace
        let clear = Core.delete

        let compare s0 s1 =
            Cf_seq.cmpf E.compare (Core.to_seq_incr s0) (Core.to_seq_incr s1)

        let put_swap_ s x = (Core.replace[@tailcall]) x s
        let clear_swap_ s x = (Core.delete[@tailcall]) x s
        let member_swap_ s x = (Core.member[@tailcall]) x s

        (*---
          The [diff], [intersect] and [union] functions may be more efficient
          if implemented more like the functions in the standard OCaml library
          [Set] module.  The key function there is [join], which would have to
          be rewritten for red-black trees.  The [split] and [concat] functions
          are implemented on top of [join].  The [diff], [intersect] and
          [union] functions are implemented on top of [join], [split] and
          [concat].  If [join] is fast enough on RB-trees, then our efficiency
          should compare well with the standard library.

          Hint: implement [join_aux_ ltree lheight v rheight rtree] and assume
          that [lheight] is the black-height of the [ltree] node and [rheight]
          is the black-height of the [rtree] node; then implement [join] as a
          wrapper on [join_aux_] that first computes the black-hight of [ltree]
          and [rtree].
          ---*)

        let union s0 s1 = Core.to_seq_incr s1 |> Seq.fold_left put_swap_ s0
        let diff s0 s1 = Core.to_seq_incr s1 |> Seq.fold_left clear_swap_ s0

        let intersect s0 s1 =
            Core.to_seq_incr s1 |>
            Seq.filter (member_swap_ s0) |>
            Core.of_seq_incr

        let rec subset s1 s2 =
            match s1, s2 with
            | Z, _ ->
                true
            | _, Z ->
                false
            | (R (x1, a1, b1) | (B (x1, a1, b1))),
              (R (x2, a2, b2) | (B (x2, a2, b2))) ->
                let dx = E.compare x1 x2 in
                if dx = 0 then
                    subset a1 a2 && subset b1 b2
                else if dx < 0 then
                    subset (B (x1, a1, Z)) a2 && subset b1 s2
                else
                    subset (B (x1, Z, b1)) b2 && subset a1 s2

        let rec disjoint s1 s2 =
            match s1, s2 with
            | Z, _
            | _, Z ->
                true
            | (R (x1, a1, b1) | (B (x1, a1, b1))),
              (R (x2, a2, b2) | (B (x2, a2, b2))) ->
                let dx = E.compare x1 x2 in
                if dx = 0 then
                    false
                else if dx < 0 then
                    disjoint (B (x1, a1, Z)) a2 && disjoint b1 s2
                else
                    disjoint (B (x1, Z, b1)) b2 && disjoint a1 s2

        let of_seq = Core.of_seq
        let of_seq_incr = Core.of_seq_incr
        let of_seq_decr = Core.of_seq_decr

        let to_seq_incr = Core.to_seq_incr
        let to_seq_decr = Core.to_seq_decr
        let to_seq_nearest_incr = Core.to_seq_nearest_incr
        let to_seq_nearest_decr = Core.to_seq_nearest_decr
    end
end

module Map = struct
    module type Profile = sig
        type index
        type +'a t

        val nil: 'a t
        val one: (index * 'a) -> 'a t
        val empty: 'a t -> bool
        val size: 'a t -> int
        val min: 'a t -> (index * 'a)
        val max: 'a t -> (index * 'a)
        val member: index -> 'a t -> bool
        val search: index -> 'a t -> 'a option
        val require: index -> 'a t -> 'a
        val insert: (index * 'a) -> 'a t -> 'a t * 'a option
        val replace: (index * 'a) -> 'a t -> 'a t
        val modify: index -> ('a -> 'a) -> 'a t -> 'a t
        val extract: index -> 'a t -> 'a * 'a t
        val delete: index -> 'a t -> 'a t

        val of_seq: (index * 'a) Seq.t -> 'a t
        val of_seq_incr: (index * 'a) Seq.t -> 'a t
        val of_seq_decr: (index * 'a) Seq.t -> 'a t
        val to_seq_incr: 'a t -> (index * 'a) Seq.t
        val to_seq_decr: 'a t -> (index * 'a) Seq.t
        val to_seq_nearest_decr: index -> 'a t -> (index * 'a) Seq.t
        val to_seq_nearest_incr: index -> 'a t -> (index * 'a) Seq.t
    end

    module Create(K: Cf_relations.Order) = struct
        module Node = Cf_index_node.Binary(K)
        module Core = Core(Node)

        type 'a t = 'a Core.t

        let nil = Core.nil
        let one = Core.one

        let empty = Core.empty
        let size = Core.size
        let min = Core.min
        let max = Core.max
        let member = Core.member

        let search key u =
            match Core.search key u with
            | Some n -> Some (Node.obj n)
            | None -> None

        let require key u = Node.obj (Core.require key u)

        let extract key u =
            let n, u = Core.extract key u in
            Node.obj n, u

        let insert key u =
            let u, nopt = Core.insert key u in
            let nopt =
                match nopt with
                | None -> None
                | Some n -> Some (Node.obj n)
            in
            u, nopt

        let delete = Core.delete
        let replace = Core.replace
        let modify = Core.modify

        let of_seq = Core.of_seq
        let of_seq_incr = Core.of_seq_incr
        let of_seq_decr = Core.of_seq_decr

        let to_seq_incr = Core.to_seq_incr
        let to_seq_decr = Core.to_seq_decr
        let to_seq_nearest_incr = Core.to_seq_nearest_incr
        let to_seq_nearest_decr = Core.to_seq_nearest_decr
    end
end

(*--- End ---*)
