(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Cf_radix_n

let std =
    let pad = Some (Must, '\061')
    and check = function
        | '\065'..'\090' -> Digit
        | '\097'..'\122' -> Digit
        | '\048'..'\057' -> Digit
        | '\043' -> Digit
        | '\047' -> Digit
        | '\061' -> Pad
        | _ -> Invalid
    and decode c =
        let i = Char.code c in
        match c with
        | '\065'..'\090' -> i - 65
        | '\097'..'\122' -> i - 71
        | '\048'..'\057' -> i + 4
        | '\043' -> 62
        | '\047' -> 63
        | _ -> assert (not true); 0
    and encode i =
        Char.chr begin
            match i with
            | _ when i < 26 -> i + 65
            | _ when i < 52 -> i + 71
            | _ when i < 62 -> i - 4
            | 62 -> 43
            | 63 -> 47
            | _ -> assert (not true); 0
        end
    in
    let radix = 64 in
    Basis { pad; check; decode; encode; radix }

let mime =
    let Basis b = std in
    let check c =
        match b.check c with
        | Invalid -> Skip
        | (Digit | Pad | Skip as ck) -> ck
    in
    Basis { b with check }

let url =
    let pad = Some (Should, '\061')
    and check = function
        | '\065'..'\090' -> Digit
        | '\097'..'\122' -> Digit
        | '\048'..'\057' -> Digit
        | '\043' -> Digit
        | '\047' -> Digit
        | '\061' -> Pad
        | _ -> Invalid
    and decode c =
        let i = Char.code c in
        match c with
        | '\065'..'\090' -> i - 65
        | '\097'..'\122' -> i - 71
        | '\048'..'\057' -> i + 4
        | '\045' -> 62
        | '\095' -> 63
        | _ -> assert (not true); 0
    and encode i =
        Char.chr begin
            match i with
            | _ when i < 26 -> i + 65
            | _ when i < 52 -> i + 71
            | _ when i < 62 -> i - 4
            | 62 -> 45
            | 63 -> 95
            | _ -> assert (not true); 0
        end
    in
    let radix = 64 in
    Basis { pad; check; decode; encode; radix }

module Std = (val create std : Profile)
module Mime = (val create mime : Profile)
module Url = (val create url : Profile)

(*--- End ---*)
