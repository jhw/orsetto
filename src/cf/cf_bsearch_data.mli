(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Data structures founded on sorted vectors. *)

(** {6 Overview}

    This module implements some useful data structures, e.g. sets and maps, on
    the foundation of a general abstraction of a vector initialized with
    totally ordered values in sorted order.
*)

(** {6 Signatures} *)

(** This module contains the signature of a random-access vector containing
    data accessible at constant order cost for any index. Distinguished
    instances of the signature include [char] type values in [string] type
    vectors and [int] type values in [int array] vectors.

    Note: this interface is provided so that a {Bigarray} can be a vector, but
    the implementation of that is not provided here.
*)
module Vector: sig

    (** The signature of vector basis modules. *)
    module type Basis = sig
        include Cf_bsearch.Basis

        (** Data structures use [expand c] to multiple an index by two. *)
        val expand: t -> t

        (** Data structures use [adjust ~lim ~rev c] to quickly decrement [c]
            by the difference between [lim] and [rev] if [c] is a successor of
            [lim].
        *)
        val adjust: lim:t -> rev:t -> t -> t
    end

    (** A distinguished basis module for vectors with integer indices. *)
    module Int_basis: Basis with type t = int

    (** The signature of vector implementation modules. *)
    module type Profile = sig

        (** Type of vector index. *)
        type index

        (** Type of vector element. *)
        type element

        (** Signture of vector basis with type erased. *)
        module Basis: Basis with type t := index

        (** Signature of totally ordered vector element type. *)
        module Element: Cf_relations.Order with type t = element

        (** Abstract type of a vector of elements *)
        type t

        (** Data structures use this distinguished empty vector. *)
        val nil: t

        (** Data structures use [empty v] to test whether [v] contains any
            elements.
        *)
        val empty: t -> bool

        (** Data structures use [first] as the index of the first element. *)
        val first: index

        (** Data strutures use [last v] to get the index of the last element of
            [v]. This function may raise [Invalid_argument] only if [v] is
            empty.
        *)
        val last: t -> index

        (** Data structures use [project v i] to get the element in [v] at
            index [i]. Data structures may raise [Invalid_argument] only if [i]
            is not a valid index of [v].
        *)
        val project: t -> index -> element

        (** Data structures use [of_seq s] to make a vector with elements
            consumed from [s], with the head of [s] at the first index, and
            the element immediately preceding the end of [s] at the last index.
        *)
        val of_seq: element Seq.t -> t

        (** Data structures use [to_seq v] to make a confluently persistent
            sequence of elements from [v], beginning at the first element and
            ending after the last element.
        *)
        val to_seq: t -> element Seq.t
    end

    (** Use [Create(E)] to compose a vector using an array. *)
    module Create(E: Cf_relations.Order): Profile
        with type index = int
        and type element = E.t
        and type t = E.t array

    (** Use [Of_char] for string vectors. *)
    module Of_char: Profile
        with type index = int
        and type element = char
        and type t = string

    (** Use [Of_int] for integer array vectors. *)
    module Of_int: module type of Create(Stdlib.Int)

    (** Use [Of_string] for string array vectors. *)
    module Of_string: module type of Create(Stdlib.String)
end

(** This module contains the signature of a search table comprising a vector of
    data sorted in multiplicative binary search order and an ancillary vector
    of adjustments for searching the final rank of the tree. Distinguished
    instances are provided for table searchable by [char], [int] and [string]
    values.
*)
module Table: sig

    (** The signature of a table basis module. *)
    module type Basis = sig

        (** The total order of search keys. *)
        module Search: Cf_relations.Order

        (** The implementation module for the underlying vector. *)
        module Vector: Vector.Profile

        (** The [find] function uses this to compare a search key with a vector
            element when iterating the vector.
        *)
        val xcompare: Search.t -> Vector.element -> int
    end

    (** A table basis module *)
    module Char_basis: Basis
       with type Search.t = char
        and type Vector.element = char
        and type Vector.index = int
        and type Vector.t = string
        [@@ocaml.alert deprecated "Private use only!"]

    (** Use [Order_basis(R)] to compose a table basis for a total order. *)
    module Order_basis(R: Cf_relations.Order): Basis
       with type Search.t = R.t
        and type Vector.element = R.t
        and type Vector.index = int
        and type Vector.t = R.t array

    (** A table basis module *)
    module Int_basis: module type of Order_basis(Stdlib.Int)
        [@@ocaml.alert deprecated "Private use only!"]

    (** A table basis module *)
    module String_basis: module type of Order_basis(Stdlib.String)
        [@@ocaml.alert deprecated "Private use only!"]

    (** The signature of table implementation modules. *)
    module type Profile = sig

        (** The type of a search parameter. *)
        type search

        (** The type of the underlying vector index. *)
        type index

        (** The type of the underlying vector element. *)
        type element

        (** Abstract type containing internal structure of table. *)
        type t

        (** A distinguished empty table. *)
        val nil: t

        (** Use [empty t] to test if [t] is an empty table. *)
        val empty: t -> bool

        (** Use [of_seq s] to construct a table from the sequence of vector
            elements [s].
        *)
        val of_seq: element Seq.t -> t

        (** Use [find ret key tab] to search for [key] in [tab] and use the
            binary search return adapter [ret] to construct the result.
        *)
        val find: (index, 'r) Cf_bsearch.ret -> search -> t -> 'r

        (** Use [member key tab] to check if [key] is to be found in [tab]. *)
        val member: search -> t -> bool

        (** Use [search key tab] to return [Some index] for [key] if it can be
            found in [tab] and [None] otherwise.
        *)
        val search: search -> t -> index option

        (** Use [require key tab] to return [index] for [key] if it can be
            found in [tab]. Raises [Not_found] if [key] is not in [tab].
        *)
        val require: search -> t -> index

        (** Use [to_seq tab] to make a sequence of the elements in the
            underlying vector. These are presented in binary search order, not
            monotonically increasing order.
        *)
        val to_seq: t -> element Seq.t

        (** Unsafe interfaces depend on the internal structure of tables. *)
        module Unsafe: sig
            type vector

            type event = private
                | E_index of index
                | E_adjust of { lim: index; rev: index }

            val import: vector -> index array -> t
            val export: t -> vector * index array
            val events: index -> index -> event Seq.t
        end
    end

    (** Use [Create(B)] to create an instance of a table module. *)
    module Create(B: Basis): Profile
       with type search := B.Search.t
        and type index := B.Vector.index
        and type element := B.Vector.element
        and type Unsafe.vector := B.Vector.t

    (** A distinguished instance of tables of [char] type elements. *)
    module Of_char: Profile
       with type search := char
        and type index := int
        and type element := char
        and type Unsafe.vector := string
        [@@ocaml.alert deprecated "Private use only!"]

    (** A distinguished instance of tables of [int] type elements. *)
    module Of_int: Profile
       with type search := int
        and type index := int
        and type element := int
        and type Unsafe.vector := int array
        [@@ocaml.alert deprecated "Private use only!"]

    (** A distinguished instance of tables of [string] type elements. *)
    module Of_string: Profile
       with type search := string
        and type index := int
        and type element := string
        and type Unsafe.vector := string array
        [@@ocaml.alert deprecated "Private use only!"]
end

(** This module contains the signature of an immutable set constructed from a
    binary search table of containing elements of the set. Distinguished
    instances are provided for tables of [char] and [int] type values.
*)
module Set: sig

    (** The signature of set implementation modules. *)
    module type Profile = sig

        (** The abstract type of a set. *)
        type t

        (** The type of a search key *)
        type search

        (** The type of a set element *)
        type element

        (** A distinguished empty set. *)
        val nil: t

        (** Use [empty u] to test if [u] is an empty set. *)
        val empty: t -> bool

        (** Use [of_seq s] to create a set from the unordered sequence of
            elements [s].
        *)
        val of_seq: element Seq.t -> t

        (** Use [member v u] to test whether [v] is a member of [u]. *)
        val member: search -> t -> bool

        (** Unsafe interfaces depend on the internal structure of sets. *)
        module Unsafe: sig
            type index and vector
            val import: vector -> index array -> t
            val export: t -> vector * index array
        end
    end

    (** Use [Of_basis(B)] to create an instance of a set module. *)
    module Of_basis(T: Table.Basis): Profile
       with type search := T.Search.t
        and type element := T.Vector.element
        and type Unsafe.index := T.Vector.index
        and type Unsafe.vector := T.Vector.t

    (** A simplified constructor for any total order. *)
    module Create(R: Cf_relations.Order): Profile
       with type search := R.t
        and type element := R.t
        and type Unsafe.index := int
        and type Unsafe.vector := R.t array

    (** A distinguished instance for character sets. *)
    module Of_char: Profile
       with type search := char
        and type element := char
        and type Unsafe.index := int
        and type Unsafe.vector := string

    (** A distinguished instance for integer sets. *)
    module Of_int: module type of Create(Stdlib.Int)

    (** A distinguished instance for string sets. *)
    module Of_string: module type of Create(Stdlib.String)
end

(** This module contains the signature of an immutable map constructed from a
    binary search table of containing key elements of the map and an co-domain
    array of the co-domain elements. Distinguished instances are provided for
    maps from [char] and [int] type values.
*)
module Map: sig

    (** This module contains the signature of an ancillary array for storage of
        the co-domain elements of a map structure.
    *)
    module Aux: sig

        (** The signature of a co-domain content array. *)
        module type Profile = sig

            (** The type of an co-domain array. *)
            type 'a t

            (** The index type. *)
            type index

            (** A distinguished empty array. *)
            val nil: 'a t

            (** Use [empty v] to test whether [v] is an empty array. *)
            val empty: 'a t -> bool

            (** Use [of_seq s] to make an array from the elements consumed from
                [s].
            *)
            val of_seq: 'a Seq.t -> 'a t

            (** Use [project v i] to obtain the element of [v] at index [i]. *)
            val project: 'a t -> index -> 'a
        end

        (** A distinguished instance of a module for the ['a array] type. *)
        module Of_array:
            Profile with type 'a t = 'a array and type index := int
    end

    (** The signature of a map basis module. *)
    module type Basis = sig

        (** The total order of the table and co-domain array index type. *)
        module Index: Cf_relations.Order

        (** The basis of the underlying table module. *)
        module Table: Table.Basis with type Vector.index = Index.t

        (** The implementation of the underlying co-domain content array. *)
        module Content: Aux.Profile with type index := Index.t
    end

    (** A distinguished instance of the map basis for character maps. *)
    module Char_basis: Basis
       with type Index.t = int
        and type Table.Search.t = char
        and type Table.Vector.element = char
        and type Table.Vector.t = string
        and type 'a Content.t = 'a array
        [@@caml.alert deprecated "Private use only!"]

    (** A distinguished instance of the map basis for integer maps. *)
    module Int_basis: Basis
       with type Index.t = int
        and type Table.Search.t = int
        and type Table.Vector.element = int
        and type Table.Vector.t = int array
        and type 'a Content.t = 'a array
        [@@caml.alert deprecated "Private use only!"]

    (** A distinguished instance of the map basis for string maps. *)
    module String_basis: Basis
       with type Index.t = int
        and type Table.Search.t = string
        and type Table.Vector.element = string
        and type Table.Vector.t = string array
        and type 'a Content.t = 'a array
        [@@caml.alert deprecated "Private use only!"]

    (** The signature of a map implementation module. *)
    module type Profile = sig

        (** The abstract type of a map. *)
        type 'a t

        (** The type of a search key *)
        type search

        (** The type of an index key *)
        type key

        (** A distinguished empty map. *)
        val nil: 'a t

        (** Use [empty m] to test if [m] is an empty map. *)
        val empty: 'a t -> bool

        (** Use [of_seq s] to make a map by consuming the unordered sequence
            of domain and co-domain values [s].
        *)
        val of_seq: (key * 'a) Seq.t -> 'a t

        (** Use [member k m] to test whether [k] is in the domain of [m]. *)
        val member: search -> 'a t -> bool

        (** Use [search k m] to find the co-domain [Some v] of [k] in [m].
            Returns [None] if [k] is not in the domain of [m].
        *)
        val search: search -> 'a t -> 'a option

        (** Use [require k m] to find the co-domain of [k] in [m]. Raises
            [Not_found] if [k] is not in the domain of [m].
        *)
        val require: search -> 'a t -> 'a

        (** Unsafe interfaces depend on the internal structure of maps. *)
        module Unsafe: sig
            type index and vector and 'a content
            val import: vector -> index array -> 'a content -> 'a t
            val export: 'a t -> vector * index array * 'a content
        end
    end

    (** Use [Of_basis(B)] to create an instance of a map module. *)
    module Of_basis(B: Basis): Profile
       with type search := B.Table.Search.t
        and type key := B.Table.Vector.element
        and type Unsafe.index := B.Index.t
        and type Unsafe.vector := B.Table.Vector.t
        and type 'a Unsafe.content := 'a B.Content.t

    (** A simplified constructor for any total order. *)
    module Create(R: Cf_relations.Order): Profile
       with type search := R.t
        and type key := R.t
        and type Unsafe.index := int
        and type Unsafe.vector := R.t array
        and type 'a Unsafe.content := 'a array

    (** A distinguished instance for character maps. *)
    module Of_char: Profile
       with type search := char
        and type key := char
        and type Unsafe.index := int
        and type Unsafe.vector := string
        and type 'a Unsafe.content := 'a array

    (** A distinguished instance for integer maps. *)
    module Of_int: module type of Create(Stdlib.Int)

    (** A distinguished instance for string maps. *)
    module Of_string: module type of Create(Stdlib.String)
end

(*--- End ---*)
