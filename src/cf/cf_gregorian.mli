(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2018, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Civil dates in the proleptic Gregorian calendar. *)

(** {6 Overview} *)

(** This module implements a record type inhabited by proleptic Gregorian
    calendar dates. It also provides related functions and types to facilitate
    conversion to and from various formats for data interchange.

    Days, weeks, and months are numbered according to ISO 8601. Days of the
    month are represented by integers from 1 to 31.  Months of the year are
    represented by integers from 1 to 12.  Days of the week are represented by
    integers from 1 to 7, where 1 is Monday and 7 is Sunday. Days of the year
    are numbered from 1 to 366, where only leap years contain day number 366.
    Weeks of the year are numbered from 1 to 53.

    Additional, an ancillary private type is also provided here to facilitate
    conversions between a useful subset of dates and their corresponding
    Chronological Julian Day (CJD) numbers. These functions are useful for
    adjustments of dates by intervals measured in integral numbers of days.
    The first Chronological Julian Day, numbered zero, was 24 Nov 4714 BCE.
*)

(** {6 Types} *)

(** The private type inhabited by dates in a useful subset of the proleptic
    Gregorian calendar.
*)
type date = private Date of {
    year: int64;    (** Astonomical year number. *)
    month: int;     (** Months from January = 1 to December = 12 *)
    day: int;       (** Days 1 to 31. *)
}

(** The type inhabited by Chronological Julian Day numbers, which correspond to
    a useful subset of dates in the Gregorian calendar, including those dates
    that are accepted by the Internet time stamp format (RFC 3339).
*)
type cjd = CJD of int [@@unboxed]

(** {6 Functions} *)

(** Use [create ~year ~month ~day] to create a date according to the values of
    [~year], [~month] and [~day].  Raises [Invalid_argument] if the [month] or
    [day] values are not valid for [year] in the proleptic Gregorian calendar.
*)
val create: year:int64 -> month:int -> day:int -> date

(** Using [is_valid ~year ~month ~day] returns [true] if [year], [month] and
    [day] arguments specify a valid proleptic Gregorian date.
*)
val is_valid: year:int64 -> month:int -> day:int -> bool

(** Use [day_of_week d] to compute the day of the week on which [d] occurs.
    Days of the week are numbered according to ISO 8601, i.e. from 1 to 7,
    starting on Monday and ending on Sunday.
*)
val day_of_week: date -> int

(** Use [day_of_year day] to compute the ordinal day of the year on which [d]
    occurs according to ISO 8601, i.e. from 1 to 366.
*)
val day_of_year: date -> int

(** Use [week_number d] to compute the year and week number in which [d]
    occurs. Weeks are numbered according to ISO 8601, i.e. from 1 to 53.
*)
val week_number: date -> int64 * int

(** [to_cjd d] computes the Chronological Julian Day number for [d].

    Gregorian dates between 12 Aug 2937947 BCE and 27 Feb 2935093 CE inclusive
    are convertible to Chronological Julian Day numbers on platforms where the
    Ocaml integer is a 31-bit integer. Accordingly, [Failure] is raised if the
    Chronological Julian Day corresponding to the date is outside the range
    that can be represented with an integer.
*)
val to_cjd: date -> cjd

(** Using [is_cjd_range d] returns [true] if [d] is within the range of dates
    that can be represented by a Chronological Julian Day (CJD) number.
*)
val in_cjd_range: date -> bool

(** [of_cjd cjd] returns the Gregorian date that corresponds to [cjd]. *)
val of_cjd: cjd -> date

(**/**)

type mjd = MJD of int64 [@@unboxed]

val of_mjd: mjd -> date
val in_mjd_range: int64 -> int -> int -> bool
val to_mjd_unsafe: int64 -> int -> int -> mjd

(*--- End ---*)
