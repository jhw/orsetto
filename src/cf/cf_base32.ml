(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Cf_radix_n

let std =
    let radix = 32
    and pad = Some (Must, '\061')
    and check = function
        | '\097'..'\122' -> Digit
        | '\065'..'\090' -> Digit
        | '\050'..'\055' -> Digit
        | '\061' -> Pad
        | _ -> Invalid
    and decode c =
        let i = Char.code c in
        match c with
        | '\097'..'\122' -> i - 97
        | '\065'..'\090' -> i - 65
        | '\050'..'\055' -> i - 24
        | _ -> assert (not true); 0
    and encode i =
        Char.chr begin
            match i with
            | _ when i < 26 -> i + 65
            | _ when i < 32 -> i + 24
            | _ -> assert (not true); 0
        end
    in
    Basis { pad; check; decode; encode; radix }

let hex =
    let radix = 32
    and pad = Some (Must, '\061')
    and check = function
        | '\097'..'\122' -> Digit
        | '\065'..'\086' -> Digit
        | '\048'..'\057' -> Digit
        | '\061' -> Pad
        | _ -> Invalid
    and decode c =
        let i = Char.code c in
        match c with
        | '\097'..'\122' -> i - 87
        | '\065'..'\086' -> i - 55
        | '\048'..'\057' -> i - 48
        | _ -> assert (not true); 0
    and encode i =
        Char.chr begin
            match i with
            | _ when i < 10 -> i + 48
            | _ when i < 32 -> i + 55
            | _ -> assert (not true); 0
        end
    in
    Basis { pad; check; decode; encode; radix }

module Std = (val create std : Profile)
module Hex = (val create hex : Profile)

(*--- End ---*)
