(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type 'a t = 'a r list and 'a r = R of int * 'a * 'a list * 'a t

module Core(N: Cf_index_node.Profile) = struct
    let nil = []
    let one x =  [ R (0, x, [], []) ]

    let empty h = (h = [])

    let size =
        let rec loop = function hd :: tl -> (aux hd + loop tl) | [] -> 0
            and aux (R (_, _, xs, ts)) = 1 + List.length xs + loop ts
        in
        loop

    let rank_ (R (r, _, _, _)) = r
    let root_ (R (_, x, _, _)) = x

    let link_ t1 t2 =
        let R (r, x1, xs1, c1) = t1
        and R (_, x2, xs2, c2) = t2
        in
        let r = succ r in
        if N.compare x1 x2 < 0
            then R (r, x1, xs1, t2 :: c1)
            else R (r, x2, xs2, t1 :: c2)

    let skew_link_ x t1 t2 =
        let R (r, y, ys, c) = link_ t1 t2 in
        if N.compare x y < 0
            then R (r, x, y :: ys, c)
            else R (r, y, x :: ys, c)

    let rec insert_tree_ t = function
        | [] ->
            [ t ]
        | hd :: tl as ts ->
            if rank_ t < rank_ hd
                then t :: ts
                else insert_tree_ (link_ t hd) tl

    let rec merge_trees_ ts1 ts2 =
        match ts1, ts2 with
        | _, [] -> ts1
        | [], _ -> ts2
        | t1 :: ts1', t2 :: ts2' ->
            if rank_ t1 < rank_ t2 then
                t1 :: merge_trees_ ts1' ts2
            else if rank_ t2 < rank_ t1 then
                t2 :: merge_trees_ ts1 ts2'
            else
                insert_tree_ (link_ t1 t2) (merge_trees_ ts1' ts2')

    let normalize_ = function
        | [] -> []
        | hd :: tl -> insert_tree_ hd tl

    let put x = function
        | t1 :: t2 :: tail as ts ->
            if rank_ t1 = rank_ t2
                then skew_link_ x t1 t2 :: tail
                else R (0, x, [], []) :: ts
        | ts ->
            R (0, x, [], []) :: ts

    let merge ts1 ts2 = merge_trees_ (normalize_ ts1) (normalize_ ts2)

    let rec shift_tree_ = function
        | [] -> raise Not_found
        | x :: [] -> x
        | hd :: tl ->
            let hd' = shift_tree_ tl in
            let x = root_ hd and y = root_ hd' in
            if N.compare x y < 0 then hd else hd'

    let head ts = root_ (shift_tree_ ts)

    let rec remove_tree_ = function
        | [] -> raise Not_found
        | x :: [] -> x, []
        | hd :: tl ->
            let hd', tl' = remove_tree_ tl in
            let x = root_ hd in
            let y = root_ hd' in
            if N.compare x y < 0
                then hd, tl
                else hd', hd :: tl'

    let rec tail_loop_ ts = function
        | x :: xs' -> tail_loop_ (put x ts) xs'
        | [] -> ts

    let tail ts =
        let R (_, _, xs, ts1), ts2 = remove_tree_ ts in
        tail_loop_ (merge (List.rev ts1) ts2) xs

    let pop ts = try Some (head ts, tail ts) with Not_found -> None

    let of_seq =
        let rec loop a s =
            match s () with
            | Seq.Cons (hd, tl) -> loop (put hd a) tl
            | Seq.Nil -> a
        in
        fun s ->
            loop nil s

    let rec to_seq h () =
        try Seq.Cons (head h, to_seq (tail h)) with Not_found -> Seq.Nil
end

module Heap = struct
    module type Profile = sig
        type element

        type t

        val nil: t
        val one: element -> t
        val empty: t -> bool
        val size: t -> int
        val head: t -> element
        val tail: t -> t
        val pop: t -> (element * t) option
        val put: element -> t -> t
        val merge: t -> t -> t

        val of_seq: element Seq.t -> t
        val to_seq: t -> element Seq.t
    end

    module Create(E: Cf_relations.Order) = struct
        module Node = Cf_index_node.Unary(E)
        type nonrec t = E.t t
        include Core(Node)
    end
end

module PQueue = struct
    module type Profile = sig
        type priority
        type +'a t

        val nil: 'a t
        val one: (priority * 'a) -> 'a t
        val empty: 'a t -> bool
        val size: 'a t -> int
        val head: 'a t -> (priority * 'a)
        val tail: 'a t -> 'a t
        val pop: 'a t -> ((priority * 'a) * 'a t) option
        val put: (priority * 'a) -> 'a t -> 'a t
        val merge: 'a t -> 'a t -> 'a t

        val of_seq: (priority * 'a) Seq.t -> 'a t
        val to_seq: 'a t -> (priority * 'a) Seq.t
    end

    module Create(K: Cf_relations.Order) = struct
        module Node = Cf_index_node.Binary(K)
        type nonrec 'a t = 'a Node.t t
        include Core(Node)
    end
end

(*--- End ---*)
