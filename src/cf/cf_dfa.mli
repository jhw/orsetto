(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional composition of lazy deterministic finite automata. *)

(** {6 Overview}

    This module implements operators for functional composition of lazy
    deterministic finite automata (DFA). A DFA is computationally more
    efficient at recognizing regular grammars than a non-deterministic finite
    automaton, at the cost of requiring exponentially more space for states.
    Lazy computation and memorization of DFA states usually requires space
    comparable with direct NFA elaboration, at the cost of some additional
    computation required when new DFA states are encountered.
*)

(** {6 Interface} *)

(** This signature provides the abstraction necessary to represent an event
    dispatch, which is part of the DFA state record. It creates a dispatch for
    each new DFA state computed. It conceptually represents a lazily-evaluated
    map of events to state transitions.

    Some pre-defined options are provided in the [Aux] module.
*)
module type Dispatch = sig

    (** The event type. *)
    type event

    (** The dispatch type. *)
    type 'a t

    (** The DFA uses [create f] to make an event dispatch that returns
        transitions computed by applying [f] to an event.
    *)
    val create: (event -> 'a option) -> 'a t

    (** The DFA uses [dispatch e d] to find the transition in [d] for [e]. *)
    val dispatch: event -> 'a t -> 'a
end

(** This signature is a the basis for creating a DFA module. *)
module type Basis = sig

    (** The equivalence relation on events. *)
    module Event: Cf_relations.Equal

    (** The dispatch signature. *)
    module Dispatch: Dispatch with type event = Event.t
end

(** This module contains some pre-defined dispatch modules. *)
module Aux: sig

    (** The dispatch signatures for {i char} events. *)
    module type Char_dispatch = Dispatch with type event = char

    (** The dispatch signatures for {i int} events. *)
    module type Int_dispatch = Dispatch with type event = int

    (** Use [Memo(E)] with a totally ordered event type to make a dispatch that
        memorizes the results of computing transitions as events require them.
        Transitions are only computed for events not previously required.
    *)
    module Memo(E: Cf_relations.Order): Dispatch with type event = E.t

    (** This module contains functions for creating dispatch modules that are
        eager about pre-computing some frequently used events and storing their
        transitions in a {Cf_disjoint_interval.Map} object for faster and more
        cache-friendly dispatch.
    *)
    module Eager: sig

        (** Use [of_chars s] to make a dispatch module for [char] events that
            eagerly computes the transitions for all the characters in [s].
        *)
        val of_chars: char Seq.t -> (module Char_dispatch)

        (** Use [of_chars s] to make a dispatch module for [int] events that
            eagerly computes the transitions for all the integers in [s].
        *)
        val of_ints: int Seq.t -> (module Int_dispatch)
    end
end

(** The signature of the core functions of the DFA engine. *)
module type Regular = sig

    (** The basis event type (erased when created). *)
    type event

    (** The type of a regular language term. *)
    type term

    (** The empty term, representing no events. *)
    val nil: term

    (** Use [one v] to make a term representing the single occurrence of [v]. *)
    val one: event -> term

    (** Use [sat f] to make a term representing any event that satisfies the
        predicate [f].
    *)
    val sat: (event -> bool) -> term

    (** Use [cat2 a b] to make a term representing [a] and [b] concatenated. *)
    val cat2: term -> term -> term

    (** Use [cats s] to make a term representing the concatenation of all the
        terms of [s] in sequential order.
    *)
    val cats: term Seq.t -> term

    (** Use [alt2 a b] to make a term representing the occurrence of either [a]
        or [b].
    *)
    val alt2: term -> term -> term

    (** Use [alts s] to make a term representing the occurrence of any of the
        alternatives in [s].
    *)
    val alts: term Seq.t -> term

    (** Use [opt t] to make a term representing either zero or one occurences
        of [t].
    *)
    val opt: term -> term

    (** Use [star t] to make a term representing the Kleene star of [t], i.e.
        zero, one or more occurrences of [t].
    *)
    val star: term -> term

    (** Use [seq t] to make a term representing a sequence of occurrences of
        [t].

        If [~a] is used, then it specifies the minimum number of occurrences in
        the recognized sequence. If [~b] is used then it specifies the maximum
        number of occurrences in the recognized sequence. Composition raises
        [Invalid_argument] if [a < 0] or [b < a].

    *)
    val seq: ?a:int -> ?b:int -> term -> term

    (** The type of a final state. *)
    type 'r fin

    (** Use [fin t r] to make [t] into a final state identified by [r] *)
    val fin: term -> 'a -> 'a fin
end

module type Machine = sig

    (** The basis event type (erased when created). *)
    type event

    (** The type of a final state (erased when created). *)
    type 'r fin

    (** The type of automatons parameterized by final identifier type. An
        imperative programming interface is provided below for elaboration.
    *)
    type 'r t

    (** Use [create s] to make a new DFA with all the final states of [s]. *)
    val create: 'r fin Seq.t -> 'r t

    (** Use [copy dfa] to copy the internal state of [dfa] into a new
        machine, ready to advance from the same point.
    *)
    val copy: 'r t -> 'r t

    (** Use [reset dfa] to reset the internal state of [dfa] to its initial
        state, ready to advance on new events.
    *)
    val reset: 'r t -> unit

    (** Use [advance dfa event] to advance [dfa] with [event]. Raises [Failure]
        if [dfa] has rejected an event previously.
    *)
    val advance: 'r t -> event -> unit

    (** Use [finished dfa] to check whether [dfa] has rejected an event and
        and may not be advanced any further.
    *)
    val rejected: 'r t -> bool

    (** Use [accepted dfa] to check whether [dfa] has reached a final state.
        A DFA may still be further advanced after reaching one of its final
        states the first time.
    *)
    val accepted: 'r t -> bool

    (** Use [finish dfa] to get the final value [dfa]. Raise [Not_found] if
        no final value is available, i.e. either because it has rejected an
        event or it has not yet reached an accepting state.
    *)
    val finish: 'r t -> 'r
end

(** Use [Core(B)] to make a DFA with basis [B]. *)
module Core(B: Basis): sig
    include Regular with type event = B.Event.t
    include Machine with type event := event and type 'r fin := 'r fin
end

(** The signature of additional affix operators for composing terms. *)
module type Affix = sig

    (** These types are copied from the core module. *)
    type event and term and 'r fin

    (** Use [!: event] as [one event]. *)
    val ( !: ): event -> term

    (** Use [!^ f] as [sat f]. *)
    val ( !^ ): (event -> bool) -> term

    (** Use [!? term] as [opt term] *)
    val ( !? ): term -> term

    (** Use [!* term] as [star term] *)
    val ( !* ): term -> term

    (** Use [!+ term] as [seq ~a:1 term] *)
    val ( !+ ): term -> term

    (** Use [a $| b] as [alt2 a b] *)
    val ( $| ): term -> term -> term

    (** Use [a $& b] as [cat2 a b] *)
    val ( $& ): term -> term -> term

    (** Use [term $= v] as [fin term v] *)
    val ( $= ): term -> 'r -> 'r fin
end

(** Use [Mk_affix(R)] to make the optional affix operators for [R]. *)
module Mk_affix(R: Regular): Affix
   with type event := R.event
    and type term := R.term
    and type 'r fin := 'r R.fin

(** The DFA signature including the affix operators. *)
module type Profile = sig
    include Regular
    include Machine with type event := event and type 'r fin := 'r fin

    module Affix: Affix
       with type event := event
        and type term := term
        and type 'r fin := 'r fin
end

(** Use [Create(B)] to make the DFA module for [B] including its optional affix
    operators.
*)
module Create(B: Basis): Profile with type event := B.Event.t

(*--- End ---*)
