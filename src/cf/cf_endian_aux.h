/*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/

#ifndef _CF_ENDIAN_AUX_H
#define _CF_ENDIAN_AUX_H

#include "cf_common_aux.h"

#include <memory.h>
#include <stdbool.h>
#include <stdint.h>

#define CF_ENDIAN_SWAP16(C)                               \
    ((uint16_t)                                           \
     ((((uint16_t) (C) & 0xffU) << 8)                   | \
      (((uint16_t) (C) & 0xff00U) >> 8)))

#define CF_ENDIAN_SWAP32(C)                               \
    ((uint32_t)                                           \
     ((((uint32_t) (C) & 0xffUL) << 24)                 | \
      (((uint32_t) (C) & 0xff00UL) << 8)                | \
      (((uint32_t) (C) & 0xff0000UL) >> 8)              | \
      (((uint32_t) (C) & 0xff000000UL) >> 24)))

#define CF_ENDIAN_SWAP64(C)                               \
    ((uint64_t)                                           \
     ((((uint64_t) (C) & 0xffUL) << 56)                 | \
      (((uint64_t) (C) & 0xff00UL) << 40)               | \
      (((uint64_t) (C) & 0xff0000UL) << 24)             | \
      (((uint64_t) (C) & 0xff000000UL) << 8)            | \
      (((uint64_t) (C) & 0xff00000000UL) >> 8)          | \
      (((uint64_t) (C) & 0xff0000000000UL) >> 24)       | \
      (((uint64_t) (C) & 0xff000000000000UL) >> 40)     | \
      (((uint64_t) (C) & 0xff00000000000000UL) >> 56)))

static inline uint16_t cf_endian_swap16_unboxed(uint16_t u16)
{
    return CF_ENDIAN_SWAP16(u16);
}

static inline uint32_t cf_endian_swap32_unboxed(uint32_t u32)
{
    return CF_ENDIAN_SWAP32(u32);
}

static inline uint64_t cf_endian_swap64_unboxed(uint64_t u64)
{
    return CF_ENDIAN_SWAP64(u64);
}

#define CF_ENDIAN_ALIASING(BITS) \
    { uint##BITS##_t n; uint8_t b[sizeof(uint##BITS##_t)]; }

static inline bool cf_endian_is_aligned(const void* p, unsigned int bits)
{
    return ((uintptr_t) p % (bits / 8) == 0);
}

#endif /* defined(_CF_ENDIAN_AUX_H) */

/*--- $File: src/cf/cf_endian_aux.h $ ---*/
