(*---------------------------------------------------------------------------*
  Copyright (C) 2003-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional deque. *)

(** {6 Overview}

    A functional persistent double-ended catenable deque, with O{_ avg}(1) cost
    for every operation.  Internally, this is a recursive data structure with
    height O(log N). (Note: if the deque is not the product of concatenation,
    then it is a pure data structure. Concatenation entails a lazy evaluation
    of the recursive join.)
*)

(** {6 Type} *)

(** The abstract type of a deque. *)
type +'a t

(** {6 Interface} *)

(** The empty deque. *)
val nil: 'a t

(** Create a deque containing one element. *)
val one: 'a -> 'a t

(** Returns [true] if the deque is the empty deque. *)
val empty: 'a t -> bool

(** Functions for operations on one of the two ends of a deque. *)
module type Direction = sig

    (** [push x d] adds the element [x] to the end of the deque [d].  The
        average cost is constant.  Worst-case running time is O(log N), which
        happens once in every N operations.
    *)
    val push: 'a -> 'a t -> 'a t

    (** [pop q] returns [None] if [q] is the empty deque, otherwise it returns
        [Some (x, d')] where [x] is the element on the end of the deque, and
        [q'] is the remainder of [q] with the element [x] removed.  The average
        cost is constant.  Worst-case running time is O(log N), which happens
        once in every N operations.
    *)
    val pop: 'a t -> ('a * 'a t) option

    (** [head q] returns the element at the end of the deque [q].  Raises
        [Not_found] if the deque is empty.
    *)
    val head: 'a t -> 'a

    (** [tail q] is discards the element at the end of the deque [q].  Raises
        [Not_found] if the deque is empty.
    *)
    val tail: 'a t -> 'a t

    (** Use [of_seq s] to make a deque by consuming all the elements in the
        sequence [s]. The head of the deque is the first element consumed
        and the tail is the final element consumed.
    *)
    val of_seq: 'a Seq.t -> 'a t

    (** [to_seq q] returns a sequence that produces the elements of the deque in
        order from head to tail by making successive calls to [pop q].
    *)
    val to_seq: 'a t -> 'a Seq.t
end

(** Operations on the left end of a deque. *)
module A: Direction

(** Operations on the right end of a deque. *)
module B: Direction

(** [catenate q1 q2] returns a new deque composed by joining the right end of
    [q1] to the left end of [q2].  The average cost is constant.
*)
val catenate: 'a t -> 'a t -> 'a t

(*--- End ---*)
