(*---------------------------------------------------------------------------*
  Copyright (C) 2003-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open OUnit2

(*
Gc.set {
    (Gc.get ()) with
    (* Gc.verbose = 0x3ff; *)
    Gc.verbose = 0x14;
};;

Gc.create_alarm begin fun () ->
    let min, pro, maj = Gc.counters () in
    Printf.printf "[Gc] minor=%f promoted=%f major=%f\n" min pro maj;
    flush stdout
end
*)

let jout = Cf_journal.stdout

let _ =
    Random.self_init ();
    jout#setlimit `Debug;
    Printexc.record_backtrace true;
    Cf_leap_second.load_from_nist_file "leap-seconds.list";
    Unix.putenv "TZ" "UTC"

let assert_formatted f =
    let pp = Format.str_formatter in
    Buffer.clear Format.stdbuf;
    f pp;
    Format.pp_print_flush pp ();
    assert_string (Buffer.contents Format.stdbuf)

module type T_signature = sig
    val test: test
end

module T_horizon: T_signature = struct
    type bottom
    type 'a test = Test of 'a

    type _ Cf_type.nym +=
        | Extend: unit test Cf_type.nym
        | Container: 'a Cf_type.nym -> 'a test Cf_type.nym
        | Bottom: bottom Cf_type.nym

    let horizon = object(self)
        inherit Cf_type.horizon as cf

        method! equiv:
            type a b. a Cf_type.nym -> b Cf_type.nym -> (a, b) Cf_type.eq
                = fun a b ->
            match[@warning "-4"] a, b with
            | Extend, Extend ->
                Cf_type.Eq
            | Container a, Container b ->
                let Cf_type.Eq = self#equiv a b in
                Cf_type.Eq
            | _ ->
                cf#equiv a b
    end

    module Form = (val Cf_type.form horizon : Cf_type.Form)

    open Cf_type
    open! Form

    let test_relations = "relations" >:: begin fun ctxt ->
        let _ =
            match eq Bottom Unit with
            | exception Type_error ->
                ()
            | exception x ->
                logf ctxt `Error "exn %s %s"
                    (Printexc.to_string x) (Printexc.get_backtrace ());
                assert_failure "eq Bottom Unit -> exn != Type_error"
            | _ ->
                assert_failure "eq Bottom Unit -> eq!"
        in
        let eq1 = eq Unit Unit in
        let eq2 = symm eq1 in
        let eq3 = trans eq1 eq2 in
        coerce eq3 ()
    end

    type case = Case: {
        name: string;
        nym: 'v nym;
        equal: 'v -> 'v -> bool;
        value: 'v;
        typerr: opaque list;
    } -> case

    let of_case (Case c) = c.name >:: begin fun ctxt ->
        let v = witness c.nym c.value in
        assert_bool "Cf_type.ck -> true" @@ ck c.nym v;
        assert_bool "Cf_type.ck -> false" @@ not @@ ck Bottom v;
        let _ =
            match opt c.nym v with
            | Some value ->
                assert_equal ~ctxt ~msg:"opt v -> Some" ~cmp:c.equal
                    c.value value
            | None ->
                assert_failure "opt v -> !Some"
        in
        let _ =
            match opt Bottom v with
            | Some _ -> assert_failure "opt v Bottom -> Some"
            | None -> ()
        in
        assert_equal ~ctxt ~msg:"req v = c.value" ~cmp:c.equal
            c.value (req c.nym v);
        let _ =
            match req Bottom v with
            | exception Type_error ->
                ()
            | exception x ->
                logf ctxt `Error "exn %s %s"
                    (Printexc.to_string x) (Printexc.get_backtrace ());
                assert_failure "req v Bottom -> exn?"
            | _ ->
                assert_failure "req v Bottom -> value!"
        in
        let f notv =
            assert_bool "ck notv -> true!" @@ not (ck c.nym notv)
        in
        List.iter f c.typerr
    end

    let test_witness = "witness" >::: List.map of_case [
        Case {
            name = "unit";
            nym = Cf_type.Unit;
            equal = (fun () () -> true);
            value = ();
            typerr = [];
        };

        Case {
            name = "bool";
            nym = Cf_type.Bool;
            equal = Bool.equal;
            value = true;
            typerr = [];
        };

        Case {
            name = "char";
            nym = Cf_type.Char;
            equal = Char.equal;
            value = '?';
            typerr = [];
        };

        Case {
            name = "uchar";
            nym = Cf_type.Uchar;
            equal = Uchar.equal;
            value = Uchar.of_int 1024;
            typerr = [];
        };

        Case {
            name = "string";
            nym = Cf_type.String;
            equal = String.equal;
            value = "test";
            typerr = [];
        };

        Case {
            name = "bytes";
            nym = Cf_type.Bytes;
            equal = Bytes.equal;
            value = Bytes.of_string "test";
            typerr = [];
        };

        Case {
            name = "int";
            nym = Cf_type.Int;
            equal = Int.equal;
            value = 77;
            typerr = [];
        };

        Case {
            name = "int32";
            nym = Cf_type.Int32;
            equal = Int32.equal;
            value = 0x7fff_ffffl;
            typerr = [];
        };

        Case {
            name = "int64";
            nym = Cf_type.Int64;
            equal = Int64.equal;
            value = 0x7fff_ffff_0123_4567L;
            typerr = [];
        };

        Case {
            name = "float";
            nym = Cf_type.Float;
            equal = Float.equal;
            value = Float.pi;
            typerr = [];
        };

        Case {
            name = "int-option";
            nym = Cf_type.(Option Int);
            equal = Option.equal Int.equal;
            value = Some 9;
            typerr = [ witness (Option Bottom) None ];
        };

        Case {
            name = "string/int-list";
            nym = Cf_type.(Pair (String, Seq Int));
            equal = begin fun (a1, a2) (b1, b2) ->
                String.equal a1 b1 && Cf_seq.eqf Int.equal a2 b2
            end;
            value = "foo", List.to_seq [ 2; 5; 7 ];
            typerr = [
                witness (Pair (String, Unit)) ("", ());
                witness (Option Bottom) None;
                witness (Pair (Unit, Seq Int)) ((), Seq.empty);
                witness (Pair (String, Seq Bottom)) ("foo", Seq.empty);
            ];
        };

        Case {
            name = "opaque";
            nym = Cf_type.Opaque;
            equal = ( == );
            value = witness Opaque @@ witness Unit ();
            typerr = [];
        };

        Case {
            name = "extend";
            nym = Extend;
            equal = (fun (Test ()) (Test ()) -> true);
            value = Test ();
            typerr = [
                witness (Container Char) (Test 'a');
                witness Int 5;
                witness (Seq Unit) Seq.empty;
            ];
        };

        Case {
            name = "container";
            nym = Container Int;
            equal = (fun (Test a) (Test b) -> Int.equal a b);
            value = Test 53;
            typerr = [
                witness Extend (Test ());
                witness (Container String) (Test "ok");
                witness (Seq Char) (Seq.return 'a');
            ];
        };
    ]

    let test = "type" >::: [
        test_witness;
        test_relations;
    ]
end

module T_annot: T_signature = struct
    open Cf_annot

    module T_meta = struct
        open Meta

        let t_basis = "basis" >:: fun ctxt ->
            assert_equal ~ctxt ~msg:"or_concise None"
                `Concise (or_concise None);
            assert_equal ~ctxt ~msg:"or_diagnostic None"
                `Diagnostic (or_diagnostic None)

        module Basis = struct
            module Position = Int
            module Symbol = Char

            module Meta = struct
                type fields = All
                let default_fields = [ All ]

                let symbol_type = Cf_type.Char

                let of_opaque_position ?style ?(fields = default_fields) v =
                    let style = or_concise style in
                    let open Cf_type in
                    match fields with
                    | [] ->
                        let _ = req Unit v in
                        (-1)
                    | _ :: _ ->
                        match style with
                        | `Concise ->
                            let pos = req Int v in
                            if pos < 0 then failwith "unsound position";
                            pos
                        | `Diagnostic ->
                            let name, pos = req (Pair (String, Int)) v in
                            if name <> "pos" then failwith "unsound field";
                            if pos < 0 then failwith "unsound position";
                            pos

                let to_opaque_position ?style ?(fields = default_fields) p =
                    let style = or_concise style in
                    let open Cf_type in
                    match fields with
                    | [] ->
                        witness Unit ()
                    | _ :: _ ->
                        match style with
                        | `Concise ->
                            witness Int p
                        | `Diagnostic ->
                            witness (Pair (String, Int)) ("pos", p)
            end

            let default_initial_position = 0
            let advance i _ = succ i
        end

        module Annot = Create(Basis)

        let assert_equal_positions ~ctxt a b =
            assert_equal ~ctxt ~msg:"position" ~cmp:Int.equal a b

        let assert_equal_spans ~ctxt (Annot.Span a) (Annot.Span b) =
            assert_equal ~ctxt ~msg:"start" a.start b.start;
            assert_equal ~ctxt ~msg:"limit" a.limit b.limit

        let t_fields ?style style_name = style_name >:: fun ctxt ->
            let _ =
                match style with
                | None -> ()
                | Some style ->
                    assert_equal ~ctxt ~msg:"or_concise style"
                        style (or_concise @@ Some style);
                    assert_equal ~ctxt ~msg:"or_diagnostic style"
                        style (or_diagnostic @@ Some style);
            in
            let iotas =
                Array.of_seq @@ Annot.to_iotas ?start:None @@
                    String.to_seq "test"
            in
            let pack = Cf_type.(witness Char) in
            let unpack = Cf_type.(req Char) in
            let ckiota i0 =
                let opaque = Annot.Meta.to_opaque_iota ?style pack i0 in
                let i1 = Annot.Meta.of_opaque_iota ?style unpack opaque in
                let Annot.Iota a = i0 and Annot.Iota b = i1 in
                assert_equal ~ctxt ~msg:"iota.symbol equal" ~cmp:Char.equal
                    a.symbol b.symbol;
                assert_equal_positions ~ctxt a.position b.position
            in
            Array.iter ckiota iotas;
            let forms = Array.map Annot.Scan_basis.term iotas in
            let ckform loc0 =
                let v = Annot.Meta.to_opaque_form ?style pack loc0 in
                let loc1 = Annot.Meta.of_opaque_form ?style unpack v in
                let Annot.Form a = loc0 and Annot.Form b = loc1 in
                assert_equal ~ctxt ~msg:"form.value equal" ~cmp:Char.equal
                    a.value b.value
            in
            Array.iter ckform forms;
            let Annot.Form form = Array.get forms 0 in
            let sp0 = Option.get form.span in
            let v = Annot.Meta.to_opaque_span ?style sp0 in
            let sp1 = Annot.Meta.of_opaque_span ?style v in
            assert_equal_spans ~ctxt sp0 sp1;
            let f0 = Annot.imp 'x' in
            let v = Annot.Meta.to_opaque_form ?style pack f0 in
            let f1 = Annot.Meta.of_opaque_form ?style unpack v in
            let Annot.Form a = f0 and Annot.Form b = f1 in
            assert_equal ~ctxt ~msg:"form.value equal" ~cmp:Char.equal
                a.value b.value

        let t_no_fields ?style style_name = style_name >:: fun ctxt ->
            let pack = Cf_type.(witness Char) in
            let unpack = Cf_type.(req Char) in
            let forms =
                Array.of_seq @@
                    Seq.map Annot.Scan_basis.term @@
                    Annot.to_iotas ?start:None @@ String.to_seq "x"
            in
            let f0 = Array.get forms 0 in
            let v = Annot.Meta.to_opaque_form ?style ~fields:[] pack f0 in
            let f1 = Annot.Meta.of_opaque_form ?style ~fields:[] unpack v in
            let Annot.Form a = f0 and Annot.Form b = f1 in
            assert_equal ~ctxt ~msg:"form.value equal" ~cmp:Char.equal
                a.value b.value;
            let sp0 = Option.get a.span in
            let v = Annot.Meta.to_opaque_span ?style ~fields:[] sp0 in
            let sp1 = Annot.Meta.of_opaque_span ?style ~fields:[] v in
            let Annot.Span sp = sp1 in
            assert_equal ~ctxt ~msg:"sp.start" (-1) sp.start;
            assert_equal ~ctxt ~msg:"sp.limit" (-1) sp.limit

        let test = "meta" >::: [
            t_basis;
            "with-fields" >::: [
                t_fields "default";
                t_fields ~style:`Concise "concise";
                t_fields ~style:`Diagnostic "diagnostic";
            ];
            "no-fields" >::: [
                t_no_fields "default";
                t_no_fields ~style:`Concise "concise";
                t_no_fields ~style:`Diagnostic "diagnostic";
            ]
         ]
    end

    let test = "annot" >::: [
        T_meta.test;
    ]
end

module T_channel_codec: T_signature = struct
    type case = Case: {
        name: string;
        limit: int option;
        write: Cf_encode.emitter -> unit;
        read: Cf_decode.scanner -> 'v;
        validate: ctxt:test_ctxt -> 'v -> unit
    } -> case

    let c_abc =
        let name = "abc"
        and limit = Some 16
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any 'a';
            w#emit Cf_encode.any 'b';
            w#emit Cf_encode.any 'c'
        and read (r : Cf_decode.scanner) =
            Cf_decode.opaque 3 |> r#scan
        and validate ~ctxt v =
            let printer s = s in
            assert_equal ~ctxt ~cmp:String.equal ~printer "abc" v
        in
        Case { name; limit; write; read; validate }

    let c_abc_lim1 =
        let name = "abc-lim1"
        and limit = Some 2
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any 'a';
            w#emit Cf_encode.any 'b';
            w#emit Cf_encode.any 'c'
        and read (r : Cf_decode.scanner) =
            let a = r#scan Cf_decode.any in
            let b = r#scan Cf_decode.any in
            let c = r#scan Cf_decode.any in
            a, b, c
        and validate ~ctxt (a, b, c) =
            let printer = Printf.sprintf "%c" in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' a;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'b' b;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'c' c
        in
        Case { name; limit; write; read; validate }

    let c_empty =
        let name = "empty"
        and limit = Some 16
        and write (_ : Cf_encode.emitter) = ()
        and read (r : Cf_decode.scanner) =
            match r#scan Cf_decode.any with
            | exception Cf_decode.Incomplete (ss, n) ->
                `Incomplete (Cf_slice.length ss, n)
            | c ->
                `Scanned c
        and validate ~ctxt v =
            match v with
            | `Scanned c ->
                Printf.sprintf "`Scanned '%c'" c |> assert_failure
            | `Incomplete (len, win) ->
                let cmp = Stdlib.( = ) and printer = Printf.sprintf "%d" in
                let msg = "`Incomplete (len, _)" in
                assert_equal ~ctxt ~cmp ~printer ~msg 0 len;
                let msg = "`Incomplete (_, win)" in
                assert_equal ~ctxt ~cmp ~printer ~msg 1 win
        in
        Case { name; limit; write; read; validate }

    let c_sat =
        let name = "lit"
        and limit = Some 1
        and write (w : Cf_encode.emitter) = w#emit Cf_encode.any '0'
        and read (r : Cf_decode.scanner) =
            r#scan @@ Cf_decode.sat @@ fun c ->
                let n = Char.code c in
                n >= Char.code '0' && n <= Char.code '9'
        and validate ~ctxt c =
            let printer = Printf.sprintf "%c" in
            assert_equal ~ctxt ~cmp:Char.equal ~printer '0' c
        in
        Case { name; limit; write; read; validate }

    let c_lit =
        let name = "lit"
        and limit = Some 5
        and write (w : Cf_encode.emitter) = w#emit Cf_encode.opaque "zzyzx"
        and read (r : Cf_decode.scanner) = r#scan @@ Cf_decode.lit "zzyzx"
        and validate ~ctxt:_ () = () in
        Case { name; limit; write; read; validate }

    let c_ign =
        let name = "lit"
        and limit = Some 5
        and write (w : Cf_encode.emitter) = w#emit Cf_encode.opaque "zzyzx"
        and read (r : Cf_decode.scanner) =
            r#scan @@ Cf_decode.(ign @@ opaque 5)
        and validate ~ctxt:_ () = () in
        Case { name; limit; write; read; validate }

    let c_opt =
        let name = "opt"
        and limit = None
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any '+';
            w#emit Cf_encode.any '-'
        and read (r : Cf_decode.scanner) =
            let scheme = Cf_decode.(opt @@ sat @@ (==) '+') in
            let v1 = r#scan scheme in
            let v2 = r#scan scheme in
            v1, v2
        and validate ~ctxt (v1, v2) =
            match v1, v2 with
            | Some c, None ->
                let printer = Printf.sprintf "%c" in
                assert_equal ~ctxt ~cmp:Char.equal ~printer '+' c
            | None, _ ->
                assert_failure "v1=None"
            | _, Some _ ->
                assert_failure "v2=Some"
        in
        Case { name; limit; write; read; validate }

    let c_pair =
        let name = "pair"
        and limit = Some 3
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.(pair any any) ('a', 'b');
            w#emit Cf_encode.any 'c';
        and read (r : Cf_decode.scanner) =
            let a = r#scan Cf_decode.any in
            let b, c = r#scan Cf_decode.(pair any any) in
            a, b, c
        and validate ~ctxt (a, b, c) =
            let printer = Printf.sprintf "%c" in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' a;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'b' b;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'c' c;
        in
        Case { name; limit; write; read; validate }

    let c_triple =
        let name = "triple"
        and limit = Some 4
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.(triple any any any) ('a', 'b', 'c');
            w#emit Cf_encode.any 'd';
        and read (r : Cf_decode.scanner) =
            let a = r#scan Cf_decode.any in
            let b, c, d = r#scan Cf_decode.(triple any any any) in
            a, b, c, d
        and validate ~ctxt (a, b, c, d) =
            let printer = Printf.sprintf "%c" in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' a;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'b' b;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'c' c;
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'd' d;
        in
        Case { name; limit; write; read; validate }

    let c_vec =
        let name = "vec"
        and limit = None
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any 'a';
            w#emit Cf_encode.any 'b';
            w#emit Cf_encode.any 'c';
        and read (r : Cf_decode.scanner) =
            r#scan @@ Cf_decode.(vec 3 any)
        and validate ~ctxt v =
            let printer = Printf.sprintf "%c" in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' v.(0);
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'b' v.(1);
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'c' v.(2);
        in
        Case { name; limit; write; read; validate }

    let c_seq =
        let name = "seq"
        and limit = None
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.(seq any) @@ String.to_seq "abc"
        and read (r : Cf_decode.scanner) =
            r#scan @@ Cf_decode.(seq ~a:3 ~b:3 any)
        and validate ~ctxt v =
            let printer = Printf.sprintf "%c" in
            let v = String.of_seq @@ List.to_seq v in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' v.[0];
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'b' v.[1];
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'c' v.[2];
        in
        Case { name; limit; write; read; validate }

    let c_map =
        let name = "map"
        and limit = None
        and write (w : Cf_encode.emitter) =
            let f _ = Char.chr in
            w#emit Cf_encode.(map f any) 65
        and read (r : Cf_decode.scanner) =
            let f _ = Char.code in
            r#scan Cf_decode.(map f any)
        and validate ~ctxt n =
            let printer = Printf.sprintf "%d" in
            assert_equal ~ctxt ~cmp:Int.equal ~printer 65 n
        in
        Case { name; limit; write; read; validate }

    let c_ntyp =
        let name = "ntyp"
        and limit = None
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any 'a'
        and read (r : Cf_decode.scanner) =
            r#scan Cf_decode.(ntyp Cf_type.Char any)
        and validate ~ctxt v =
            let printer = Printf.sprintf "%c" in
            let c = Cf_type.(req Char v) in
            assert_equal ~ctxt ~cmp:Char.equal ~printer 'a' c
        in
        Case { name; limit; write; read; validate }

    let c_scanner_to_vals =
        let name = "scanner_to_vals"
        and limit = None
        and write (w : Cf_encode.emitter) =
            w#emit Cf_encode.any 'a';
            w#emit Cf_encode.any 'b';
            w#emit Cf_encode.any 'c';
        and read (r : Cf_decode.scanner) =
            String.of_seq @@ Cf_decode.(scanner_to_vals any r)
        and validate ~ctxt s = assert_equal ~ctxt ~cmp:String.equal "abc" s in
        Case { name; limit; write; read; validate }

    let case (Case c) = c.name >:: fun ctxt ->
        let fname, cout = Filename.open_temp_file "orsetto-cf-test" c.name in
        let w = Cf_encode.channel_emitter ?limit:c.limit cout in
        c.write (w :> Cf_encode.emitter);
        close_out cout;
        let cin = open_in fname in
        let r = Cf_decode.channel_scanner ?limit:c.limit cin in
        let v = c.read (r :> Cf_decode.scanner) in
        close_in cin;
        Sys.remove fname;
        c.validate ~ctxt v

    let errors = "errors" >:: fun _ctxt ->
        assert_raises
            ~msg:"expecting invalid argument"
            (Invalid_argument "Cf_decode: non-negative integer required!")
            begin fun () ->
                Cf_decode.opaque (-1)
            end;
        assert_raises
            ~msg:"pair size overflow"
            (Failure "Cf_decode: octet sequence length overflow!")
            begin fun () ->
                let a = Cf_decode.opaque Sys.max_string_length in
                Cf_decode.pair a a
            end;
        assert_raises
            ~msg:"position add overflow"
            (Invalid_argument "Cf_decode: non-negative integer required!")
            begin fun () ->
                let w = new Cf_decode.scanner () in
                Cf_decode.advance (-1) w#position
            end;
        assert_bool "invalid satisifier" begin
            let ck _ p _ = Cf_decode.invalid p "test" in
            let rd _ _ = () in
            let s = Cf_decode.scheme 0 ck rd in
            let w = new Cf_decode.scanner () in
            try
                w#scan s;
                false
            with
            | Cf_decode.(Invalid (Position 0, "test")) ->
                true
        end;
        assert_bool "can read nil" begin
            let w = new Cf_decode.scanner () in
            w#scan Cf_decode.nil;
            true
        end;
        assert_bool "can read pos" begin
            let w = new Cf_decode.scanner () in
            let p = w#scan Cf_decode.pos in
            let Cf_decode.Position i = p in
            i == 0
        end

    let test = "channel-codec" >::: [
        errors;
        case c_empty;
        case c_abc;
        case c_abc_lim1;
        case c_sat;
        case c_lit;
        case c_ign;
        case c_opt;
        case c_pair;
        case c_triple;
        case c_vec;
        case c_seq;
        case c_map;
        case c_ntyp;
        case c_scanner_to_vals;
    ]
end

module T_endian: T_signature = struct
    module Positive(E: Cf_endian.Profile) = struct
        open E

        let run _ =
            let b = Buffer.create 8 in
            let w = Cf_encode.buffer_emitter b in
            w#emit stu8 0xF9;
            w#emit stu16 40000;
            w#emit stu32 0x3abcdef0;
            w#emit stu64 max_int;
            w#emit sts8 0x7A;
            w#emit sts16 (-100);
            w#emit sts32 (-120000);
            w#emit sts64 min_int;
            w#emit sti32 Int32.min_int;
            w#emit sti64 Int64.(lognot zero);
            let r =
                Buffer.contents b |> Cf_decode.string_scanner ?start:None
            in
            assert_bool "ldu8" (r#scan ldu8 = 0xF9);
            assert_bool "ldu16" (r#scan ldu16 = 40000);
            assert_bool "ldu32" (r#scan ldu32 = 0x3abcdef0);
            assert_bool "ldu64" (r#scan ldu64 = max_int);
            assert_bool "lds8"  (r#scan lds8 = 0x7A);
            assert_bool "lds16" (r#scan lds16 = (-100));
            assert_bool "lds32" (r#scan lds32 = (-120000));
            assert_bool "lds64" (r#scan lds64 = min_int);
            assert_bool "ldi32" (r#scan ldi32 = Int32.min_int);
            assert_bool "ldi64" (r#scan ldi64 = Int64.(lognot zero));
            assert true
    end

    module Positive_big = Positive(Cf_endian.BE)
    module Positive_little = Positive(Cf_endian.LE)
    module Positive_system = Positive(Cf_endian.SE)

    let do_fp16 ctxt =
        for i = 0 to 1024 do
            let fp = Cf_endian_core.of_fp16_bits i in
            let u64 = Int64.bits_of_float fp in
            let msg =
                Printf.sprintf "fp16 round trip %u -> %.16g [%08Lx]"
                i fp u64
            in
            let u16 = Cf_endian_core.to_fp16_bits_unsafe fp in
            let printer = Printf.sprintf "%04x" in
            assert_equal ~ctxt ~msg ~printer i u16
        done

    let test = "endian" >::: [
        "big" >:: Positive_big.run;
        "little" >:: Positive_little.run;
        "system" >:: Positive_system.run;
        "fp16" >:: do_fp16;
    ]
end

module T_stdtime: T_signature = struct
(*
3fffe33e1b840309 365  Sun -1000000-12-31 23:59:59 +0000
3fffe33e1b84030a 000  Mon -999999-01-01 00:00:00 +0000
3ffffff1868b8409 364  Fri -1-12-31 23:59:59 +0000
3ffffff1868b840a 000  Sat 0-01-01 00:00:00 +0000
3ffffff1886e1757 000  Mon 1-01-01 01:01:01 +0000
3ffffff8df7db65f 000  Wed 1000-01-01 12:04:37 +0000
3ffffffec03dbf89 364  Tue 1799-12-31 23:59:59 +0000
3fffffff7c558189 364  Sun 1899-12-31 23:59:59 +0000
3fffffff7c55818a 000  Mon 1900-01-01 00:00:00 +0000
3fffffffffffffff 364  Wed 1969-12-31 23:59:49 +0000
4000000000000000 364  Wed 1969-12-31 23:59:50 +0000
4000000000000009 364  Wed 1969-12-31 23:59:59 +0000
400000000000000a 000  Thu 1970-01-01 00:00:00 +0000
400000000000000b 000  Thu 1970-01-01 00:00:01 +0000
4000000003c2670a 000  Sat 1972-01-01 00:00:00 +0000
4000000004b25808 181  Fri 1972-06-30 23:59:58 +0000
4000000004b25809 181  Fri 1972-06-30 23:59:59 +0000
4000000004b2580a 181  Fri 1972-06-30 23:59:60 +0000
4000000004b2580b 182  Sat 1972-07-01 00:00:00 +0000
4000000004b2580c 182  Sat 1972-07-01 00:00:01 +0000
4000000030e7241b 364  Sun 1995-12-31 23:59:58 +0000
4000000030e7241c 364  Sun 1995-12-31 23:59:59 +0000
4000000030e7241d 364  Sun 1995-12-31 23:59:60 +0000
4000000030e7241e 000  Mon 1996-01-01 00:00:00 +0000
4000000030e7241d 364  Sun 1995-12-31 23:59:60 +0000
4000000030e7241e 000  Mon 1996-01-01 00:00:00 +0000
4000000030e7241f 000  Mon 1996-01-01 00:00:01 +0000
4000000030e72420 000  Mon 1996-01-01 00:00:02 +0000
4000000033b8489d 180  Mon 1997-06-30 23:59:59 +0000
4000000033b8489e 180  Mon 1997-06-30 23:59:60 +0000
4000000033b8489f 181  Tue 1997-07-01 00:00:00 +0000
4000000033df0226 210  Wed 1997-07-30 08:57:43 +0000
4000000034353637 275  Fri 1997-10-03 18:14:48 +0000
4000000037d77955 251  Thu 1999-09-09 09:09:09 +0000
40000000386d439f 364  Fri 1999-12-31 23:59:59 +0000
40000000386d43a0 000  Sat 2000-01-01 00:00:00 +0000
40000000386d43a1 000  Sat 2000-01-01 00:00:01 +0000
4000000038bb0c1f 058  Mon 2000-02-28 23:59:59 +0000
4000000038bc5d9f 059  Tue 2000-02-29 23:59:59 +0000
4000000038bc5da0 060  Wed 2000-03-01 00:00:00 +0000
400000003a4fc89f 365  Sun 2000-12-31 23:59:59 +0000
40000000f4875017 000  Fri 2100-01-01 17:42:15 +0000
40000001b09f1217 000  Wed 2200-01-01 17:42:15 +0000
40000007915fc517 000  Wed 3000-01-01 17:42:15 +0000
4000003afff53a97 000  Sat 10000-01-01 17:42:15 +0000
40001ca4f3758a1f 364  Fri 999999-12-31 23:59:59 +0000
40001ca4f3758a20 000  Sat 1000000-01-01 00:00:00 +0000
*)

    module L1 = struct
        open Cf_stdtime
        module YMD = Cf_gregorian
        module HMS = Cf_clockface

        let x = [
            0x4000000003c2670aL, begin
                let date = YMD.create ~year:1972L ~month:1 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:0 in
                create ~date ~time ~offset:0
            end;

            0x4000000004b25809L, begin
                let date = YMD.create ~year:1972L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:59 in
                create ~date ~time ~offset:0
            end;

            0x4000000004b2580bL, begin
                let date = YMD.create ~year:1972L ~month:7 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:0 in
                create ~date ~time ~offset:0
            end;

            0x4000000004b2580aL, begin
                let date = YMD.create ~year:1972L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:60 in
                create ~date ~time ~offset:0
            end;

            0x4000000004b2580cL, begin
                let date = YMD.create ~year:1972L ~month:7 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:1 in
                create ~date ~time ~offset:0
            end;

            0x4000000005a4ec0aL, begin
                let date = YMD.create ~year:1972L ~month:12 ~day:31 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:59 in
                create ~date ~time ~offset:0
            end;

            0x4000000005a4ec0bL, begin
                let date = YMD.create ~year:1972L ~month:12 ~day:31 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:60 in
                create ~date ~time ~offset:0
            end;

            0x4000000005a4ec0cL, begin
                let date = YMD.create ~year:1973L ~month:1 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:0 in
                create ~date ~time ~offset:0
            end;

            0x4000000005a4ec0dL, begin
                let date = YMD.create ~year:1973L ~month:1 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:1 in
                create ~date ~time ~offset:0
            end;

            0x4000000030e7241bL, begin
                let date = YMD.create ~year:1995L ~month:12 ~day:31 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:58 in
                create ~date ~time ~offset:0
            end;

            0x4000000030e7241cL, begin
                let date = YMD.create ~year:1995L ~month:12 ~day:31 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:59 in
                create ~date ~time ~offset:0
            end;

            0x400000004fef9321L, begin
                let date = YMD.create ~year:2012L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:59 in
                create ~date ~time ~offset:0
            end;

            0x400000004fef9322L, begin
                let date = YMD.create ~year:2012L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:60 in
                create ~date ~time ~offset:0
            end;

            0x400000004fef9323L, begin
                let date = YMD.create ~year:2012L ~month:7 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:0 in
                create ~date ~time ~offset:0
            end;

            0x4000000055932da2L, begin
                let date = YMD.create ~year:2015L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:59 in
                create ~date ~time ~offset:0
            end;

            0x4000000055932da3L, begin
                let date = YMD.create ~year:2015L ~month:6 ~day:30 in
                let time = HMS.create ~hour:23 ~minute:59 ~second:60 in
                create ~date ~time ~offset:0
            end;

            0x4000000055932da4L, begin
                let date = YMD.create ~year:2015L ~month:7 ~day:1 in
                let time = HMS.create ~hour:0 ~minute:0 ~second:0 in
                create ~date ~time ~offset:0
            end;
        ]

        let wdayname = function
            | 1 -> "Mon"
            | 2 -> "Tue"
            | 3 -> "Wed"
            | 4 -> "Thu"
            | 5 -> "Fri"
            | 6 -> "Sat"
            | 7 -> "Sun"
            | _ -> assert (not true); "???"

        let tsprintf (UTC j as t) =
            let module G = Cf_gregorian in
            let G.Date ymd = j#date in
            let G.MJD mjd = G.to_mjd_unsafe ymd.year ymd.month ymd.day in
            let d = G.create ~year:ymd.year ~month:ymd.month ~day:ymd.day in
            let wday = d |> G.day_of_week |> wdayname in
            let inet = localize t |> local_to_inet in
            Printf.sprintf "{ mjd=%Ld %s %s }" mjd wday inet

        let y ctxt (i, j) =
            let module G = Cf_gregorian in
            let module C = Cf_clockface in
            let j = local_to_utc j in
            let i = Cf_tai64.add_int64 Cf_tai64.first i in
            let i' = to_tai64 j in
            (*
            Printf.printf "TAI epoch+%Lx [%Lx], %s\n"
                (Cf_tai64.sub i Cf_tai64.first) (Cf_tai64.sub i' Cf_tai64.first)
                (tsprintf j);
            flush stdout;
            *)
            let UTC j as j0 = j and UTC j' as j0' = of_tai64 i in
            if
                let G.Date ymd = j#date and C.Time hms = j#time in
                let G.Date ymd' = j'#date and C.Time hms' = j'#time in
                ymd.year <> ymd'.year ||
                ymd.month <> ymd'.month ||
                ymd.day <> ymd'.day ||
                hms.hour <> hms'.hour ||
                hms.minute <> hms'.minute ||
                hms.second <> hms'.second
            then
                assert_failure ("ck" ^ tsprintf j0 ^ " tai64" ^ tsprintf j0');
            if i <> i' then begin
                let n = Cf_tai64.sub i' Cf_tai64.first in
                assert_failure (Printf.sprintf "0x%016Lx" n)
            end;
            let i1 =
                localize j0 |> local_to_inet |> inet_to_local |>
                local_to_utc |> to_tai64
            in
            assert_equal ~ctxt ~msg:"stdtime conversion" i1 i
    end

    let run ctxt = List.iter (L1.y ctxt) L1.x

    let test = "stdtime" >:: run
end

module T_seq: T_signature = struct
    open Cf_seq

    let (!$) s = head s, tail s

    let run _ctxt =
        let s = of_substring "abc" 0 3 in
        assert_bool "has_all" (not (has_all ((==) 'x') s));
        assert_bool "has_some" (has_some ((==) 'a') s);
        let ch, s = !$ s in
        if ch <> 'a' then assert_failure "next 0";
        let _ = !$ s in
        let ch, s = !$ s in
        if ch <> 'b' then assert_failure "next 1";
        let _ = !$ s in
        let _ = !$ s in
        let ch, s = !$ s in
        if ch <> 'c' then assert_failure "next 2";
        try ignore (!$ s); assert_failure "terminal" with Not_found -> ()

    let test = "cas" >:: run
end

module T_monad: T_signature = struct
    module State_monad = struct
        open Cf_smonad
        open Affix

        let rec count n =
            if n > 0 then begin
                let* s = load in
                store @@ succ s >>= fun () ->
                count (pred n)
            end
            else
                nil

        let run _ =
            assert (eval (count 100000) 100000 == 200000);
    end

    module Continuation_monad = struct
        open Cf_cmonad
        open Affix

        let equal_x s1 s2 = List.(Cf_seq.equal (to_seq s1) (to_seq s2))

        let clear = init [ 10 ]
        let push n = cont (fun s -> n :: s)

        let rec count n =
            if n > 9 then begin
                clear >>= fun () ->
                return ()
            end
            else if n > 0 then begin
                push n >>= fun () ->
                count (succ n)
            end
            else begin
                count (succ n)
            end

        let run _ =
            let calc = eval (count 0) [] in
            let expect = [ 1; 2; 3; 4; 5; 6; 7; 8; 9; 10 ] in
            assert (equal_x calc expect)
    end

    module Sequence_monad = struct
        open Cf_seqmonad
        open Affix

        let equal_x s1 s2 = List.(Cf_seq.equal (to_seq s1) (to_seq s2))

        let rec count n =
            if n > 4 then
                all (Cf_seq.range 0 (pred n) |> Seq.map (fun i -> n - i))
            else if n > 0 then begin
                one n >>= fun () ->
                count (succ n)
            end
            else
                count 1

        let run _ =
            let calc = List.of_seq @@ eval @@ count (-200) in
            let expect = [ 1; 2; 3; 4; 5; 4; 3; 2; 1 ] in
            assert (equal_x calc expect)
    end

    let test =
        "monad" >::: [
            "state" >:: State_monad.run;
            "continuation" >:: Continuation_monad.run;
            "sequence" >:: Sequence_monad.run;
        ]
end

module T_deque: T_signature = struct
    module Q = Cf_deque

    (*
    let aux _msg _i _ (* q *) =
        assert begin
            (* jout#debug "%s i=%d q=%s\n" msg i (sprint string_of_int q) *)
            (* jout#debug "%s i=%d\n" msg i *)
            true
        end

    let modaux msg modulo i q =
        if ((i / modulo) * modulo) = i then aux msg i q
    *)

    let test1 pop push =
        let n = 1024 in
        let rec loop1 i q =
            (* aux "+" i q; *)
            if i < n then loop1 (succ i) (push i q) else loop2 0 q
        and loop2 i q =
            (* aux "-" i q; *)
            if i < n then begin
                match pop q with
                | None ->
                    assert_failure "[1] pop: None"
                | Some (j, q) ->
                    if i <> j then
                        assert_failure (Printf.sprintf "[1] i <> j j=%d" j);
                    loop2 (succ i) q
            end
            else
                match pop q with
                | None -> ()
                | Some _ -> assert_failure "[1] pop: Some _"
        in
        loop1 0 Q.nil

    let test2 pop push ncat =
        let size = 10000 (* and m = 1 *) in
        let chunk = 100 in
        let rec cons i q = function
            | j when j > 0 ->
                (* modaux "+" m i q; *)
                cons (succ i) (push i q) (pred j)
            | _ ->
                i, q
        in
        let rec load i j z =
            let i, j, q0, q1 =
                if z > chunk then begin
                    let z = z / chunk in
                    let z = if z < chunk then chunk else z in
                    let i, j, q0 = load i j z in
                    let i, j, q1 = load i j z in
                    i, j, q0, q1
                end
                else begin
                    assert (z = chunk);
                    let x = Random.int z in
                    let x = if x > j then j else x in
                    let j = j - x in
                    let i, q0 = cons i Q.nil x in
                    let y = Random.int z in
                    let y = if y > j then j else y in
                    let j = j - y in
                    let i, q1 = cons i Q.nil y in
                    i, j, q0, q1
                end
            in
            let q' = ncat q0 q1 in
            (* aux "%" i q'; *)
            if j > 0 && z > chunk then begin
                let i, j, q = load i j z in
                let q' = ncat q' q in
                (* aux "%" i q'; *)
                i, j, q'
            end
            else
                i, j, q'
        in
        let rec unload i q =
            (* modaux "-" m i q; *)
            if i < size then begin
                match pop q with
                | None ->
                    assert_failure "[2] pop: None"
                | Some (j, q) ->
                    if i <> j then begin
                        let msg = Printf.sprintf "[2] i <> j i=%d j=%d" i j in
                        assert_failure msg
                    end;
                    unload (succ i) q
            end
            else
                match pop q with
                | None -> ()
                | Some _ -> assert_failure "[2] pop: Some _"
        in
        let i, j, q = load 0 size size in
        if i <> size then assert_failure "[2] load: i <> size";
        if j <> 0 then assert_failure "[2] load: j <> 0";
        unload 0 q

    let test =
        let open Q in
        let test_1AB _ = test1 A.pop B.push in
        let test_1BA _ = test1 B.pop A.push in
        let test_2AB _ = test2 A.pop B.push catenate in
        let test_2BA _ = test2 B.pop A.push (fun q2 q1 -> catenate q1 q2) in
        "deque" >::: [
            "AB" >:: test_1AB;
            "BA" >:: test_1BA;
            "AB.cat" >:: test_2AB;
            "BA.catf" >:: test_2BA;
        ]
end

module T_bsearch: T_signature = struct
    module S = Cf_bsearch.Int

    let prime1to23 = [| 1; 2; 3; 5; 7; 11; 13; 17; 19; 23 |]

    let test_arr search ctxt =
        let fin = Array.length prime1to23 - 1 in
        let valid i = not (i < 0 || i > fin) in
        let cmp n i =
            assert_equal ~ctxt ~msg:"out of range" (valid i) true;
            let x = Array.get prime1to23 i in
            Stdlib.compare x n
        in
        let f v =
            let expect = Array.mem v prime1to23 in
            let cmp = Cf_bsearch.Cmp (cmp v) in
            match search cmp 0 fin with
            | None ->
                assert_equal ~ctxt ~msg:"expected Some" expect false
            | Some i ->
                assert_equal ~ctxt ~msg:"out of range" (valid i) true;
                let x = Array.get prime1to23 i in
                assert_equal ~ctxt ~msg:"expected None" expect true;
                assert_equal ~ctxt ~msg:"search error" v x
        in
        Seq.iter f (Cf_seq.range 0 24)

    let test_opt ctxt = test_arr S.search ctxt

    let test_req ctxt =
        let search cmp lo hi =
            try Some (S.require cmp lo hi) with Not_found -> None
        in
        test_arr search ctxt

    let ck_quick = "AaEeIiOoUu0123456789"

    let ck_nonwhite =
        let f n =
            match char_of_int n with
            | ' ' | '\t' | '\n' | '\011' | '\012' -> None
            | c -> Some c
        in
        String.of_seq @@ Seq.filter_map f @@ Cf_seq.range 0 255

    module U = Cf_bsearch_data.Set.Of_char

    let test_set ckstr ctxt =
        let u = String.to_seq ckstr |> U.of_seq in
        for i = 0 to 255 do
            let c = Char.chr i in
            let expected = String.contains ckstr c in
            let found = U.member c u in
            let msg = if expected then "expected" else "not expected" in
            let msg = Printf.sprintf "%s code %u" msg i in
            assert_equal ~ctxt ~msg expected found
        done

    module U2 = Cf_disjoint_interval.Set.Of_char
    module RBU = Cf_rbtree.Set.Create(Cf_bsearch.Char_basis)

    let test_set2 ckstr ctxt =
        let u =
            String.to_seq ckstr |>
            RBU.of_seq |>
            RBU.to_seq_incr |>
            Cf_disjoint_interval.Core.Of_char.lift |>
            U2.of_seq
        in
        for i = 0 to 255 do
            let c = Char.chr i in
            let expected = String.contains ckstr c in
            let found = U2.member c u in
            let msg = if expected then "expected" else "not expected" in
            let msg = Printf.sprintf "%s code %u" msg i in
            assert_equal ~ctxt ~msg expected found
        done

    module M = Cf_bsearch_data.Map.Of_char

    let test_map ckstr ctxt =
        let keys = String.to_seq ckstr in
        let content = Seq.map Char.code keys in
        let m = Cf_seq.zip keys content |> M.of_seq in
        for i = 0 to 255 do
            let c = Char.chr i in
            let expected = String.contains ckstr c in
            let found =
                match M.search c m with
                | Some code ->
                    let msg =
                        Printf.sprintf "expected %u searched %u" i code
                    in
                    assert_equal ~ctxt ~msg code i;
                    true
                | None ->
                    false
            in
            let msg = if expected then "expected" else "not expected" in
            let msg = Printf.sprintf "%s code %u" msg i in
            assert_equal ~ctxt ~msg expected found
        done

    module M2 = Cf_disjoint_interval.Map.Of_char
    module RBM = Cf_rbtree.Map.Create(Cf_bsearch.Char_basis)

    let test_map2 ckstr ctxt =
        let keys = String.to_seq ckstr in
        let content = Seq.map Char.code keys in
        let eq (a : int) (b : int) = (a = b) in
        let m =
            Cf_seq.zip keys content |>
            RBM.of_seq |>
            RBM.to_seq_incr |>
            Cf_disjoint_interval.Core.Of_char.lift2 eq |>
            M2.of_seq
        in
        for i = 0 to 255 do
            let c = Char.chr i in
            let expected = String.contains ckstr c in
            let found =
                match M2.search c m with
                | Some code ->
                    let msg =
                        Printf.sprintf "expected %u searched %u" i code
                    in
                    assert_equal ~ctxt ~msg code i;
                    true
                | None ->
                    false
            in
            let msg = if expected then "expected" else "not expected" in
            let msg = Printf.sprintf "%s code %u" msg i in
            assert_equal ~ctxt ~msg expected found
        done

    let test =
        "bsearch" >::: [
            "opt" >:: test_opt;
            "req" >:: test_req;
            "set" >::: [
                "quick" >:: test_set ck_quick;
                "nonwhite" >:: test_set ck_nonwhite;
            ];
            "set2" >::: [
                "quick" >:: test_set2 ck_quick;
                "nonwhite" >:: test_set2 ck_nonwhite;
            ];
            "map" >::: [
                "quick" >:: test_map ck_quick;
                "nonwhite" >:: test_map ck_nonwhite;
            ];
            "map2" >::: [
                "quick" >:: test_map2 ck_quick;
                "nonwhite" >:: test_map2 ck_nonwhite;
            ];
        ]
end

module T_rbtree: T_signature = struct
    module Index = Cf_relations.Int

    module T_set = struct
        module S1 = Set.Make(Index)
        module S2 = Cf_rbtree.Set.Create(Index)

        let bound = 64
        let iterations = 512

        (*
        let print_list =
            let rec loop = function
                | [] -> ()
                | hd :: [] -> Printf.printf "%d" hd
                | hd :: tl -> Printf.printf "%d; " hd; loop tl
            in
            fun id r ->
                Printf.printf "%s [ " id;
                loop r;
                print_string " ]\n";
                flush stdout
        *)

        let rec loop n s1 s2 =
            let x = Random.int bound in
            let msg, s1, s2 =
                if S1.mem x s1 then
                    "remove failure", S1.remove x s1, S2.clear x s2
                else
                    "add failure", S1.add x s1, S2.put x s2
            in
            let n = pred n in
            let e1 = S1.elements s1 in
            let e2 = List.of_seq @@ S2.to_seq_incr s2 in
            (*
            print_list "e1" e1;
            print_list "e2" e2;
            *)
            if e1 <> e2 then assert_failure msg;
            if n > 0 then loop n s1 s2

        let test _ =
            loop iterations S1.empty S2.nil
    end

    module T_map = struct
        module M = Cf_rbtree.Map.Create(Index)

        let bprintnode b (k, v) = Printf.bprintf b " (%d, %d);" k v

        let loglist ctx s lst =
            let b = Buffer.create 40 in
            Printf.bprintf b "%s: [" s;
            List.iter (bprintnode b) lst;
            Printf.bprintf b " ]\n";
            logf ctx `Info "%s" (Buffer.contents b)

        (*
        let to_list tree =
            Cf_seq.fold (fun v (e, _) -> e :: v) [] (M.to_seq_decr tree)
        *)

        let testcontent ctx tree lst =
            loglist ctx "testcontent" lst;
            let c = List.of_seq @@ M.to_seq_incr tree in
            if c <> lst then begin
                loglist ctx "found" c;
                assert_failure "testcontent"
            end

        let testfind ctx tree key =
            let msg = Printf.sprintf "testfind %d" key in
            loglist ctx msg (List.of_seq @@ M.to_seq_incr tree);
            try
                let e = M.require key tree in
                assert (M.member key tree);
                if (e / 2) <> key then assert_failure "testfind key <> e / 2"
            with
            | Not_found ->
                assert (not (M.member key tree));
                assert_failure "testfind -> Not_found"

        let testextract ctx tree key =
            let msg = Printf.sprintf "testextract %d" key in
            loglist ctx msg (List.of_seq @@ M.to_seq_incr tree);
            try
                let e, nt = M.extract key tree in
                if (e / 2) <> key then
                    assert_failure "testextract -> e <> key";
                nt
            with
            | Not_found ->
                assert_failure "testextract -> Not_found"

        let test1 ctx =
            let t1, old = M.insert (1, 2) M.nil in
            if old <> None then assert_failure "t1 replaced a nil";
            testcontent ctx t1 [ 1, 2 ];
            testfind ctx t1 1;
            let d1 = testextract ctx t1 1 in
            testcontent ctx d1 [];
            let t2, old = M.insert (2, 4) t1 in
            if old <> None then assert_failure "t2 replaced a nil";
            testcontent ctx t2 [ 1, 2; 2, 4 ];
            testfind ctx t2 1;
            testfind ctx t2 2;
            let d1 = testextract ctx t2 1 in
            testcontent ctx d1 [ 2, 4 ];
            let d2 = testextract ctx t2 2 in
            testcontent ctx d2 [ 1, 2 ];
            let d12 = testextract ctx d1 2 in
            testcontent ctx d12 [];
            let d21 = testextract ctx d2 1 in
            testcontent ctx d21 [];
            let t3, old = M.insert (3, 6) t2 in
            if old <> None then assert_failure "t3 replaced a nil";
            testfind ctx t3 1;
            testfind ctx t3 2;
            testfind ctx t3 3;
            let d1 = testextract ctx t3 1 in
            testcontent ctx d1 [ 2, 4; 3, 6 ];
            let d2 = testextract ctx t3 2 in
            testcontent ctx d2 [ 1, 2; 3, 6 ];
            let d3 = testextract ctx t3 3 in
            testcontent ctx d3 [ 1, 2; 2, 4 ];
            testfind ctx d1 3;
            testfind ctx d1 2;
            testfind ctx d2 1;
            testfind ctx d2 3;
            testfind ctx d3 1;
            testfind ctx d3 2;
            let d12 = testextract ctx d1 2 in
            testcontent ctx d12 [ 3, 6 ];
            let d23 = testextract ctx d2 3 in
            testcontent ctx d23 [ 1, 2 ];
            let d31 = testextract ctx d3 1 in
            testcontent ctx d31 [ 2, 4 ];
            testfind ctx d12 3;
            testfind ctx d23 1;
            testfind ctx d31 2;
            let d123 = testextract ctx d12 3 in
            let d231 = testextract ctx d23 1 in
            let d312 = testextract ctx d31 2 in
            testcontent ctx d123 [];
            testcontent ctx d231 [];
            testcontent ctx d312 []

        let test2 _ =
            let m = M.replace (0, "zero") M.nil in
            let s = M.require 0 m in
            assert (s = "zero");
            assert (M.member 0 m)

        let nearest_succ key m =
            let c = M.to_seq_nearest_incr key m in
            match c () with
            | Seq.Nil ->
                raise Not_found
            | Seq.Cons (hd, _) ->
                hd

        let nearest_pred key m =
            let c = M.to_seq_nearest_decr key m in
            match c () with
            | Seq.Nil ->
                raise Not_found
            | Seq.Cons (hd, _) ->
                hd

        let test3 _ =
            let m = [
                1, "one";
                3, "three";
                5, "five";
                7, "seven";
                9, "nine";
                11, "eleven";
                13, "thirteen";
                15, "fifteen";
                17, "seventeen";
                19, "nineteen";
            ] in
            let m = List.to_seq m |> M.of_seq_incr in
            if nearest_succ 0 m <> (1, "one") then
                assert_failure "nearest_succ 0";
            if nearest_succ 1 m <> (1, "one") then
                assert_failure "nearest_succ 1";
            if nearest_succ 2 m <> (3, "three") then
                assert_failure "nearest_succ 2";
            if nearest_pred 20 m <> (19, "nineteen") then
                assert_failure "nearest_pred 20";
            if nearest_pred 19 m <> (19, "nineteen") then
                assert_failure "nearest_pred 19";
            if nearest_pred 18 m <> (17, "seventeen") then
                assert_failure "nearest_pred 18";
            ()
    end

    let test =
        "rbtree" >::: [
            "set" >:: T_set.test;
            "map" >::: [
                "1" >:: T_map.test1;
                "2" >:: T_map.test2;
                "3" >:: T_map.test3;
            ]
        ]
end

module T_data_render: T_signature = struct
    open Cf_data_render
    open Affix

    type case = Case: {
        name: string;
        model: 'v model;
        value: 'v;
        text: string;
    } -> case

    module Basis = struct
        type 'v scheme = Scheme of ('v -> string) [@@caml.unboxed]

        let primitive: type a. a Cf_type.nym -> a scheme = fun nym ->
            match nym with
            | Cf_type.Int -> Scheme Stdlib.string_of_int
            | Cf_type.Bool -> Scheme Stdlib.string_of_bool
            | Cf_type.String -> Scheme Fun.id
            | Cf_type.Unit -> Scheme (fun () -> "!")
            | _ -> invalid_arg "Primitive type not defined!"

        let control:
            type a b. a scheme -> (a, b) control -> b scheme
            = fun (Scheme f) control ->
                match control with
                | Cast g -> Scheme (fun a -> f @@ g a)
                | Default v -> Scheme (function None -> f v | Some v -> f v)
                | _ -> invalid_arg "Unrecognized control variant!"

        type packet = Packet of string [@@caml.unboxed]

        let delegate f = Scheme (fun v -> let Packet s = f v in s)
        let packet (Scheme s) v = Packet (s v)

        let sequence container packets =
            let start, separator, finish =
                match container with
                | `Vector -> '[', ' ', ']'
                | `Record -> '(', ' ', ')'
                | `Table _ -> '<', ',', '>'
                | `Structure _ -> '{', ',', '}'
                | `Variant _ -> '|', ' ', '|'
            in
            let packets = Array.of_seq packets in
            let b = Buffer.create 2 in
            Buffer.add_char b start;
            let size = Array.length packets in
            if size > 0 then begin
                let Packet s = packets.(0) in
                Buffer.add_string b s;
                if size > 1 then
                    for i = 1 to size - 1 do
                        Buffer.add_char b separator;
                        let Packet s = packets.(i) in
                        Buffer.add_string b s
                    done
            end;
            Buffer.add_char b finish;
            Packet (Buffer.contents b)

        let pair container (Packet first) (Packet second) =
            let separator =
                match container with
                | `Table _ -> '='
                | `Structure _ -> ':'
                | `Variant _ -> ' '
            in
            Packet (Printf.sprintf "%s%c%s" first separator second)
    end

    module R = Create(Basis)

    let string_index = new index Cf_type.String

    let of_case (Case c) = c.name >:: fun ctxt ->
        let Basis.Scheme f = R.scheme c.model in
        let text = f c.value in
        assert_equal ~ctxt ~msg:"incorrect render!" ~cmp:String.equal
            ~printer:Fun.id c.text text

    module P = struct
        let null = primitive Cf_type.Unit
        let integer = primitive Cf_type.Int
        let boolean = primitive Cf_type.Bool
        let text = primitive Cf_type.String
    end

    let test = "data-render" >::: List.map of_case [
        Case {
            name = "int";
            model = P.integer;
            value = 1;
            text = "1";
        };
        Case {
            name = "cast-char-to-int";
            model = cast Char.code @@ primitive Cf_type.Int;
            value = 'a';
            text = Printf.sprintf "%d" @@ Char.code 'a';
        };
        Case {
            name = "default-none";
            model = default 0 @@ primitive Cf_type.Int;
            value = None;
            text = "0";
        };
        Case {
            name = "default-some";
            model = default 0 @@ primitive Cf_type.Int;
            value = Some 5;
            text = "5";
        };
        Case {
            name = "vector0";
            model = vector @@ P.text;
            value = [||];
            text = "[]";
        };
        Case {
            name = "vector1";
            model = vector @@ P.integer;
            value = [| 23 |];
            text = "[23]";
        };
        Case {
            name = "vector2";
            model = vector @@ P.integer;
            value = [| 23; 45 |];
            text = "[23 45]";
        };
        Case {
            name = "vector3";
            model = vector @@ P.integer;
            value = [| 23; 45; 99 |];
            text = "[23 45 99]";
        };
        Case {
            name = "record0";
            model = record [| |];
            value = ();
            text = "()";
        };
        Case {
            name = "record1";
            model = record [| Element.required P.text Fun.id |];
            value = "foo";
            text = "(foo)";
        };
        Case {
            name = "record2";
            model =
                record [|
                    P.text $: Fun.id;
                    Element.optional P.integer (fun _ -> None);
                |];
            value = "bar";
            text = "(bar)";
        };
        Case {
            name = "record3";
            model =
                record [|
                    P.text $: (fun (s, _) -> s);
                    P.integer $? (fun _ -> None);
                    Element.constant P.boolean false;
                |];
            value = ("baz", 0);
            text = "(baz false)";
        };
        Case {
            name = "record4";
            model =
                record [|
                    P.text $: (fun (s, _) -> s);
                    P.integer $? (fun (_, i) -> Some i);
                    P.boolean $= true;
                |];
            value = ("baz", 5);
            text = "(baz 5 true)";
        };
        Case {
            name = "table0";
            model = table (new index Cf_type.Int) P.boolean;
            value = [||];
            text = "<>";
        };
        Case {
            name = "table1";
            model = table (new index Cf_type.Int) P.boolean;
            value = [| 0, false |];
            text = "<0=false>";
        };
        Case {
            name = "table2";
            model = table ~unsorted:() (new index Cf_type.Int) P.boolean;
            value = [| 1, true; 0, false |];
            text = "<1=true,0=false>";
        };
        Case {
            name = "table3";
            model = table (new index Cf_type.Int) P.boolean;
            value = [| 1, true; 0, false |];
            text = "<0=false,1=true>";
        };
        Case {
            name = "structure0";
            model = structure string_index [||];
            value = ();
            text = "{}";
        };
        Case {
            name = "structure1";
            model =
                structure string_index [|
                    Field.required "alfa" P.integer (fun () -> 0)
                |];
            value = ();
            text = "{alfa:0}";
        };
        Case {
            name = "structure2";
            model =
                structure string_index [|
                    ("foo", P.integer) %: Fun.id;
                    Field.optional "bar" P.text (fun _ -> None)
                |];
            value = 23;
            text = "{foo:23}";
        };
        Case {
            name = "structure3";
            model =
                structure ~unsorted:() string_index [|
                    ("first", P.integer) %: begin fun (`V (i, _)) -> i end;
                    ("second", P.boolean) %? begin fun (`V (_, b)) ->
                        if b then Some true else None
                    end;
                    Field.constant "constant" P.text "yes";
                |];
            value = `V (23, false);
            text = "{first:23,constant:yes}";
        };
        Case {
            name = "structure4";
            model =
                structure string_index [|
                    ("first", P.integer) %: (fun (i, _, _) -> i);
                    ("second", P.boolean) %? begin fun (_, b, _) ->
                        if b then Some true else None
                    end;
                    ("third", P.text) %: (fun (_, _, s) -> s);
                    ("constant", P.text) %= "yes";
                |];
            value = (56, true, "xray");
            text = "{constant:yes,first:56,second:true,third:xray}";
        };
        Case {
            name = "choice0";
            model = begin
                let index opt = match opt with None -> 0 | Some _ -> 1 in
                choice index [|
                    structure string_index [||];
                    structure string_index [| ("some", P.text) %? Fun.id |];
                |]
            end;
            value = None;
            text = "{}";
        };
        Case {
            name = "choice1";
            model = begin
                choice (function None -> 0 | Some _ -> 1) [|
                    structure string_index [||];
                    structure string_index [| ("some", P.text) %? Fun.id |];
                |]
            end;
            value = Some "foo";
            text = "{some:foo}";
        };
        Case {
            name = "variant1";
            model = begin
                let select v = if v mod 2 == 0 then "even" else "odd" in
                variant string_index select [|
                    "even", (P.integer $: Fun.id);
                    "odd", (P.integer $: Fun.id);
                |]
            end;
            value = 0;
            text = "|even 0|";
        };
        Case {
            name = "variant2";
            model = begin
                let select v = if v mod 2 == 0 then "even" else "odd" in
                variant string_index select [|
                    "even", (P.integer $: Fun.id);
                    "odd", (P.integer $: Fun.id);
                |]
            end;
            value = 1;
            text = "|odd 1|";
        };
        Case {
            name = "recursive-record";
            model = begin[@warning "-4"]
                let rec loop () =
                    let fix = defer @@ lazy (loop ()) in
                    record [|
                        (P.text $: fun (`R (a, _)) -> a);
                        (vector fix $: fun (`R (_, b)) -> Array.of_list b);
                    |]
                in
                loop ()
            end;
            value = `R ("foo", [`R ("bar", [])]);
            text = "(foo [(bar [])])"
        };
        Case {
            name = "recursive-variant";
            model = begin[@warning "-4"]
                let select v = match v with `Z -> "nil" | `C _ -> "cons" in
                let m_nil = P.null $: (fun _ -> ()) in
                let m_cons fix =
                    record [|
                        (fix $: fun (exp, _) -> exp);
                        (fix $: fun (_, exp) -> exp);
                    |] $: function
                        | `C (a, b) -> a, b
                        | `Z -> assert false
                in
                let rec loop () =
                    let fix = defer @@ lazy (loop ()) in
                    variant string_index select [|
                        "nil", m_nil;
                        "cons", m_cons fix;
                    |]
                in
                loop ()
            end;
            value = `C (`Z, `Z);
            text = "|cons (|nil !| |nil !|)|";
        };
    ]
end

module T_parser: T_signature = struct
    open Cf_scan.ASCII
    open Affix

    let digit =
        let zero = int_of_char '0' in
        let* ch = sat (function '0'..'9' -> true | _ -> false) in
        return ((int_of_char ch) - zero)

    let unumber =
        let rec to_int x = function
            | [] -> x
            | hd :: tl -> to_int (hd + (x * 10)) tl
        in
        let* d0, dn = ?+digit in
        return (to_int 0 (d0 :: dn))

    let plus = ?. '+'
    let minus = ?. '-'
    let mult = ?. '*'
    let div = ?. '/'
    let lparen = ?. '('
    let rparen = ?. ')' |> or_fail "expected ')'"

    let ifxaddsub = alt [ plus; minus ]
    let pfxsign = alt [ plus; minus; return ' ' ]

    let ifxmultdiv = alt [ mult; div ]

    let number =
        let* pfx = pfxsign and* n = unumber in
        return (if pfx == '-' then (-n) else n)

    type fval_t = Mult of int | Div of int

    let p_ifx_term term =
        let* ifx = ifxaddsub and* n = term in
        return @@ if ifx == '-' then (-n) else n

    let p_terms term =
        let* hd = term and* tl = ?* (p_ifx_term term) in
        return @@ List.fold_left ( + ) hd tl

    let p_subexpr term = alt [ p_terms term; term ]

    let p_ifx_factor factor =
        let* ifx = ifxmultdiv and* n = factor in
        return @@ if ifx == '*' then Mult n else Div n

    let p_factors =
        let f a = function Mult x -> a * x | Div x -> a / x in
        let enter factor =
            let* hd = factor and* tl = ?* (p_ifx_factor factor) in
            return @@ List.fold_left f hd tl
        in
        enter

    let p_term factor = alt [ p_factors factor; factor ]

    let p_enclosed term =
        let+ _ = lparen and* n = p_subexpr term and* _ = rparen in n

    let p_factor term = alt [ p_enclosed term; number ]

    let run =
        let obj = object(self)
            method expr = let* _ = return () in self#term
            method term = p_term self#factor
            method factor = p_factor self#expr
        end in
        let* e = p_subexpr obj#expr in
        let* eos = fin in
        if eos then return e else nil

    let positive = [
        "123",  123;
        "-256", -256;
        "2+3", 5;
        "8/(4-2)+1", 5;
    ]

    let t_pos s x _ =
        let x' = of_string run s in
        if x' <> x then begin
            let msg = Printf.sprintf "'%s' <> %d (%d)" s x x' in
            assert_failure msg
        end

    let test =
        let pos (s, x) = s >:: t_pos s x in
        let pos = List.of_seq @@ Seq.map pos @@ List.to_seq positive in
        "parser" >::: pos
end

module T_number_scan: T_signature = struct
    module S = struct
        include Cf_scan.ASCII
        include Cf_number_scan.ASCII
    end

    let ckint scan str num ctxt =
        let num' =
            try S.of_string scan str with
            | Not_found ->
                let msg = Printf.sprintf "not recognized: \"%s\"" str in
                assert_failure msg
        in
        let msg = Printf.sprintf "\"%s\" -> %d [not %d]" str num' num in
        assert_equal ~ctxt ~msg num num'

    let ckfloat scan str num ctxt =
        let num' =
            try S.of_string scan str with
            | Not_found ->
                let msg = Printf.sprintf "not recognized: \"%s\"" str in
                assert_failure msg
        in
        let msg = Printf.sprintf "\"%s\" -> %g [not %g]" str num' num in
        assert_equal ~ctxt ~msg num num'

    module L1 = struct
        let f n =
            let str = string_of_int n in
            str >:: ckint S.signed_int str n

        let test =
             "basic-int" >::: List.map f [
                0; 1; (-1); 10000; (-10000);
                (max_int - 1); max_int; (min_int + 1); min_int
            ]
    end

    module L2 = struct
        let f (s, n) =
            s >:: ckfloat S.scientific_float s n

        let test =
             "basic-float" >::: List.map f [
                "0.0", 0.0;
                "-0.0", (-0.0);
                "12e34", 12e34;
                "-0.23567e3", -0.23567e3;
                "777777", 777777.0;
                "-777777", -777777.0;
                "2.22044604925031308e-16", epsilon_float;
                "1.79769313486231571e+308", max_float;
                "2.22507385850720138e-308", min_float;
            ]
    end

    let test = "number-scan" >::: [
        L1.test;
        L2.test;
    ]
end

module T_data_ingest: T_signature = struct
    open Cf_data_ingest
    open Affix

    module S = Cf_scan.ASCII

    module Basis = struct
        type symbol = char
        type position = Cf_scan.Simple.position
        type 'k frame = Frame of char * char * occurs

        module Scan = S
        module Form = Cf_scan.Simple.Form

        open S.Infix
        module N = Cf_number_scan.ASCII
        module C = Cf_chain_scan.ASCII

        let p_int = N.signed_int

        let p_char =
            let* p0 = S.one '\'' in
            let* ch = S.any in
            let+ p1 = S.one '\'' in
            Form.span p0 p1 ch

        type delim = Delim of {
            start: char;
            next: char;
            finish: char;
        }

        let delim container =
            match container with
            | `Vector -> Delim { start = '['; next = ' '; finish = ']' }
            | `Record -> Delim { start = '('; next = ' '; finish = ')' }
            | `Table _ -> Delim { start = '<'; next = ','; finish = '>' }
            | `Structure _ -> Delim { start = '{'; next = ','; finish = '}' }
            | `Variant _ -> Delim { start = '|'; next = '='; finish = '|' }

        let chain next = C.mk (S.one next)

        let primitive: type a. a Cf_type.nym -> a Scan.t = fun nym ->
            match nym with
            | Cf_type.Int -> p_int
            | Cf_type.Char -> p_char
            | _ -> assert false

        let control:
            type a b. a S.t -> (a, b) control -> b S.t
            = fun scan control ->
                match control with
                | Cast f -> S.cast f scan
                | _ -> invalid_arg "Unrecognized control variant!"

        let visit container frame =
            let Frame (_, _, occurs) = frame in
            let Occurs { a; b } = occurs in
            let Delim d = delim container in
            C.vis ?a ?b ~c:(chain d.next)

        let start container occurs =
            let Delim d = delim container in
            let+ _ = S.one d.start in
            Frame (d.next, d.finish, occurs)

        let finish (Frame (_, fin, _)) =
            let+ _ = S.or_fail "close required" @@ S.one fin in ()

        let pair _pair kscan vscan =
            let* k = kscan in
            let* _ = S.or_fail "equal required" @@ S.one '=' in
            let+ v = S.or_fail "value entry required" @@ vscan in
            k, v

        let memo =
            let any_occurs = occurs () in
            let v_scalar =
                S.alt [
                    p_int >>: ignore;
                    p_char >>: ignore;
                ]
            in
            let v_start =
                S.alt [
                    start `Vector any_occurs;
                    start `Record any_occurs;
                    start (`Table (new index Cf_type.Unit)) any_occurs;
                    start (`Structure (new index Cf_type.Unit)) any_occurs;
                    start (`Variant (new index Cf_type.Unit)) any_occurs;
                ]
            in
            let v_field fix =
                let* _ = v_scalar in
                let* _ = S.or_fail "equal required" @@ S.one '=' in
                S.or_fail "field value required" @@ Lazy.force fix
            in
            let v_group fix =
                let* frame = v_start in
                let Frame (next, close, occurs) = frame in
                let Occurs { a; b } = occurs in
                let f _ =
                    match close with
                    | (')' | ']') -> Lazy.force fix
                    | _ -> v_field fix
                in
                let* _ = C.vis ?a ?b ~c:(chain next) f () in
                let+ _ = finish frame in
                ()
            in
            let rec loop () =
                let* vopt = S.opt v_scalar in
                match vopt with
                | Some _ -> S.return ()
                | None -> v_group @@ lazy (loop ())
            in
            let* mark = S.cur in
            let+ _ = loop () in
            mark
    end

    module Data = Create(Basis)

    type case = Case: {
        name: string;
        model: 'v model;
        value: 'v;
        equal: 'v -> 'v -> bool;
        show: 'v -> string;
        text: string;
    } -> case

    let of_case (Case c) = c.name >:: begin fun ctxt ->
        let scanner = Data.scan c.model in
        match S.of_string scanner c.text with
        | exception (S.Bad_syntax msg) ->
            assert_failure msg
        | value ->
            assert_equal ~ctxt ~msg:"scanner error"  ~cmp:c.equal
                ~printer:c.show c.value value
    end

    let show_pair _pair kshow vshow (k, v) = kshow k ^ "=" ^ vshow v

    let show_group container vshow s =
        let Basis.Delim d = Basis.delim container in
        let b = Buffer.create 8 in
        Buffer.add_char b d.start;
        let s = Array.to_list s in
        if s <> [] then begin
            Buffer.add_string b @@ vshow @@ List.hd s;
            let f v =
                Buffer.add_char b d.next;
                Buffer.add_string b @@ vshow v
            in
            List.iter f @@ List.tl s
        end;
        Buffer.add_char b d.finish;
        Buffer.contents b

    let char_index = new index Cf_type.Char
    let int_index = new index Cf_type.Int

    let test = "data-ingest" >::: List.map of_case [
        Case {
            name = "char";
            model = primitive Cf_type.Char;
            value = 'A';
            equal = Char.equal;
            show = Printf.sprintf "'%c'";
            text = "'A'";
        };

        Case {
            name = "int";
            model = primitive Cf_type.Int;
            value = 5;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "5";
        };

        Case {
            name = "empty-vector";
            model = vector ?a:None ?b:None @@ primitive Cf_type.Int;
            value = [||];
            equal = Stdlib.( = );
            show = show_group `Vector Stdlib.string_of_int;
            text = "[]";
        };

        Case {
            name = "vector-one";
            model = vector ~a:1 ~b:1 @@ primitive Cf_type.Int;
            value = [| 1 |];
            equal = Stdlib.( = );
            show = show_group `Vector Stdlib.string_of_int;
            text = "[1]";
        };

        Case {
            name = "vector-three";
            model = vector ?a:None ~b:3 @@ primitive Cf_type.Char;
            value = [| 'a'; 'b'; 'c' |];
            equal = Stdlib.( = );
            show = show_group `Vector @@ Printf.sprintf "'%c'";
            text = "['a' 'b' 'c']";
        };

        Case {
            name = "vector-four";
            model = vector ?a:None ?b:None @@ primitive Cf_type.Char;
            value = [| 'a'; 'b'; 'c'; 'd' |];
            equal = Stdlib.( = );
            show = show_group `Vector @@ Printf.sprintf "'%c'";
            text = "['a' 'b' 'c' 'd']";
        };

        Case {
            name = "table-empty";
            model = table ~b:0 int_index @@ primitive Cf_type.Char;
            value = [||];
            equal = Stdlib.( = );
            show = begin
                let tag = `Table int_index in
                let kshow = Stdlib.string_of_int in
                let vshow = Printf.sprintf "'%c'" in
                show_group tag @@ show_pair tag kshow vshow
            end;
            text = "<>";
        };

        Case {
            name = "table-one";
            model = table ~a:1 ~b:1 int_index @@ primitive Cf_type.Char;
            value = [|0, 'a'|];
            equal = Stdlib.( = );
            show = begin
                let tag = `Table int_index in
                let kshow = Stdlib.string_of_int in
                let vshow = Printf.sprintf "'%c'" in
                show_group tag @@ show_pair tag kshow vshow
            end;
            text = "<0='a'>";
        };

        Case {
            name = "table-two";
            model = table ~b:2 int_index @@ primitive Cf_type.Char;
            value = [|0, 'a'; 1, 'b'|];
            equal = Stdlib.( = );
            show = begin
                let tag = `Table int_index in
                let kshow = Stdlib.string_of_int in
                let vshow = Printf.sprintf "'%c'" in
                show_group tag @@ show_pair tag kshow vshow
            end;
            text = "<0='a',1='b'>";
        };

        Case {
            name = "choice-one";
            model = choice [| primitive Cf_type.Int |];
            value = 13;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "13";
        };

        Case {
            name = "choice-two";
            model =
                choice [|
                    primitive Cf_type.Int;
                    primitive Cf_type.Char /= Char.code;
                |];
            value = Char.code 'a';
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "'a'";
        };

        Case {
            name = "record-empty";
            model = record nil ();
            value = ();
            equal = Unit.equal;
            show = (fun () -> "()");
            text = "()";
        };

        Case {
            name = "record-one";
            model = begin
                let group = Element.required (primitive Cf_type.Char) @> nil in
                record group Char.code
            end;
            value = 65;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "('A')";
        };

        Case {
            name = "record-two";
            model = begin
                let group =
                    !: (primitive Cf_type.Int) @>
                    Element.optional (primitive Cf_type.Char) @>
                    nil
                and compose i copt =
                    match copt with
                    | None -> i
                    | Some c -> i + (Char.code c - 64)
                in
                record group compose
            end;
            value = 28;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "(25 'C')";
        };

        Case {
            name = "record-three";
            model = begin
                let group =
                    Element.default (primitive Cf_type.Char) 'A' @>
                    !: (primitive Cf_type.Int) @>
                    !? (primitive Cf_type.Int) @>
                    nil
                and compose x i jopt =
                    let n = Char.code x + i in
                    match jopt with
                    | None -> n
                    | Some j -> n + j
                in
                record group compose
            end;
            value = 75;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "(10)";
        };

        Case {
            name = "record-option";
            model = record ((primitive Cf_type.Char $= 'A') @> nil) Char.code;
            value = 65;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "()";
        };

        Case {
            name = "structure-empty";
            model = structure int_index nil ();
            value = ();
            equal = Unit.equal;
            show = (fun () -> "()");
            text = "{}";
        };

        Case {
            name = "structure-one";
            model = begin
                let group = Field.required 1 (primitive Cf_type.Char) @> nil in
                structure int_index group Char.code
            end;
            value = 88;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "{1='X'}";
        };

        Case {
            name = "structure-two";
            model = begin
                let group =
                    0 %: primitive Cf_type.Int @>
                    Field.optional 1 (primitive Cf_type.Char) @>
                    nil
                and compose i copt =
                    match copt with
                    | None -> i
                    | Some c -> i + (Char.code c - 64)
                in
                structure int_index group compose
            end;
            value = 28;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "{0=25,1='C'}";
        };

        Case {
            name = "structure-three";
            model = begin
                let group =
                    Field.default 0 (primitive Cf_type.Char) 'A' @>
                    1 %: primitive Cf_type.Int @>
                    2 %? primitive Cf_type.Int @>
                    nil
                and compose x i jopt =
                    let n = Char.code x + i in
                    match jopt with
                    | None -> n
                    | Some j -> n + j
                in
                structure int_index group compose
            end;
            value = 75;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "{1=10}";
        };

        Case {
            name = "variant-alfa";
            model =
                variant int_index [|
                    0, primitive Cf_type.Char;
                    1, cast Char.chr @@ primitive Cf_type.Int;
                |];
            value = 'A';
            equal = Char.equal;
            show = Printf.sprintf "'%c'";
            text = "|0='A'|";
        };

        Case {
            name = "variant-bravo";
            model =
                variant int_index [|
                    0, primitive Cf_type.Char;
                    1, primitive Cf_type.Int /= Char.chr;
                |];
            value = 'B';
            equal = Char.equal;
            show = Printf.sprintf "'%c'";
            text = "|1=66|";
        };

        Case {
            name = "recursive-record";
            model = begin
                let hd = primitive Cf_type.Int in
                let tl fix = vector fix in
                let rec loop () =
                    let fix = defer @@ lazy (loop ()) in
                    record (!: hd @> !? (tl fix) @> nil) begin fun a bopt ->
                        let b =
                            match bopt with
                            | None -> 0
                            | Some b -> List.fold_left (+) 0 @@ Array.to_list b
                        in
                        a + b
                    end
                in
                loop ()
            end;
            value = 6;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "(1 [(2 []) (3)])";
        };

        Case {
            name = "recursive-structure";
            model = begin
                let rec fix = lazy (loop ())
                and loop () =
                    let group =
                        'V' %: primitive Cf_type.Int @>
                        'N' %? defer fix @>
                        nil
                    and compose value next =
                        match next with
                        | None -> value
                        | Some total -> value + total
                    in
                    structure char_index group compose
                in
                Lazy.force fix
            end;
            value = 5;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "{'V'=3,'N'={'V'=2}}";
        };

        Case {
            name = "recursive-variant";
            model = begin
                let m_lit = primitive Cf_type.Int in
                let m_add fix =
                    record (!: fix @> !: fix @> nil) (fun a b -> a + b)
                in
                let rec loop () =
                    let fix = defer @@ lazy (loop ()) in
                    variant int_index [|
                        0, m_lit;
                        1, (m_add fix);
                    |]
                in
                loop ()
            end;
            value = 3;
            equal = Int.equal;
            show = Stdlib.string_of_int;
            text = "|1=(|0=1| |0=2|)|";
        };
    ]
end

module T_record_scan: T_signature = struct
    let p_identifier =
        Cf_lex_scan.ASCII.(string_to_term "[A-Za-z0-9]+" |> simple)

    module R_basis = struct
        type symbol = char
        type position = Cf_scan.Simple.position

        module Form = Cf_scan.Simple.Form
        module Index = String
        module Content = Cf_type

        module Scan = struct
            include Cf_scan.ASCII
            open Affix

            let index = p_identifier
            let preval = Some (one ':' >>: ignore)
            let fail _ = failwith (__MODULE__ ^ ": parsing error!")
            let chain = Some (`Opt, `Opt, one ',' >>: ignore)
        end
    end

    module S = struct
        include Cf_scan.ASCII
        include Cf_number_scan.ASCII
        include Cf_record_scan.Create(R_basis)
    end

    open S.Affix

    let p_bool = S.alt [
        S.(let* _ = one 't' in return true);
        S.(let* _ = one 'f' in return false);
    ]

    let schema = S.schema [
        "foo", S.Required {
            nym = Cf_type.Int;
            scan = S.signed_int;
        };

        "bar", S.Required {
            nym = Cf_type.Bool;
            scan = p_bool;
        };

        "baz", S.Optional {
            nym = Cf_type.String;
            scan = p_identifier;
        };

        "flox", S.Default {
            nym = Cf_type.Int;
            scan = S.signed_int;
            default = 0;
        };
    ]

    type record = Record of {
        foo: int;
        bar: bool;
        baz: string option;
        flox: int;
    }

    let p_record =
        let* m = S.scan schema in
        let flox = S.unpack m "flox" Cf_type.Int in
        let foo = S.unpack m "foo" Cf_type.Int in
        let baz = S.unpack m "baz" Cf_type.(Option String) in
        let bar = S.unpack m "bar" Cf_type.Bool in
        S.return @@ Record { foo; bar; baz; flox }

    let text = "bar:t,foo:12,baz:ikaikaika"

    let ck ctxt =
        match S.of_string p_record text with
        | exception Not_found ->
            assert_failure "T_record_scan.ck: failed!"
        | Record r ->
            let msg = Printf.sprintf "foo expect 12 parsed %d" r.foo in
            assert_equal ~ctxt ~msg r.foo 12;
            let msg = Printf.sprintf "bar expect 't' parsed %B" r.bar in
            assert_equal ~ctxt ~msg r.bar true;
            let xbaz = Some "ikaikaika" in
            let msg = {|baz expect (Some "ikaika")|} in
            assert_equal ~ctxt ~msg r.baz xbaz;
            let msg = Printf.sprintf "flox expect 0 parsed %d" r.flox in
            assert_equal ~ctxt ~msg r.flox 0

    let test =
        "record-scan" >:: ck
end

module T_radix_n: T_signature = struct
    let ckinput ~ctxt ~decode ?n text input =
        let n =
            match n with
            | None -> None
            | Some () -> Some (String.length text)
        in
        match decode ?n input with
        | Some text' ->
            let msg = Printf.sprintf {|decode: "%s" <> "%s"|} text text' in
            assert_equal ~ctxt ~msg text text'
        | None ->
            let msg = Printf.sprintf {|decode: "%s" -> error|} input in
            assert_failure msg

    let ckoutput ~ctxt ~encode text output =
        let output' = encode text in
        let msg = Printf.sprintf {|encode: "%s" <> "%s"|} output output' in
        assert_equal ~ctxt ~msg output output'

    let run rm tab ctxt =
        let module M = (val rm : Cf_radix_n.Profile) in
        let each (text, input, output) =
            ckinput ~ctxt ~decode:M.decode_string ?n:None text input;
            ckinput ~ctxt ~decode:M.decode_string ~n:() text input;
            ckoutput ~ctxt ~encode:(M.encode_string ?brk:None ?np:None)
                text output
        in
        List.iter each tab

    let base16 = [
        "", "", "";
        "f", "66", "66";
        "fo", "666F", "666F";
        "foo", "666F6F", "666F6F";
        "foob", "666F6F62", "666F6F62";
        "fooba", "666F6F6261", "666F6F6261";
        "foobar", "666F6F626172", "666F6F626172";
        "foobar", "666f6f626172", "666F6F626172";
    ]

    let base32 = [
        "", "", "";
        "f", "MY======", "MY======";
        "fo", "MZXQ====", "MZXQ====";
        "foo", "MZXW6===", "MZXW6===";
        "foob", "MZXW6YQ=", "MZXW6YQ=";
        "fooba", "MZXW6YTB", "MZXW6YTB";
        "foobar", "MZXW6YTBOI======", "MZXW6YTBOI======";
        "foobar", "mzxw6ytboi======", "MZXW6YTBOI======";
    ]

    let base32hex = [
        "", "", "";
        "f", "CO======", "CO======";
        "fo", "CPNG====", "CPNG====";
        "foo", "CPNMU===", "CPNMU===";
        "foob", "CPNMUOG=", "CPNMUOG=";
        "fooba", "CPNMUOJ1", "CPNMUOJ1";
        "foobar", "CPNMUOJ1E8======", "CPNMUOJ1E8======";
        "foobar", "cpnmuoj1e8======", "CPNMUOJ1E8======";
    ]

    let base64 = [
        "", "", "";
        "f", "Zg==", "Zg==";
        "fo", "Zm8=", "Zm8=";
        "foo", "Zm9v", "Zm9v";
        "foob", "Zm9vYg==", "Zm9vYg==";
        "fooba", "Zm9vYmE=", "Zm9vYmE=";
        "foobar", "Zm9vYmFy", "Zm9vYmFy";
    ]

    let test = "radix-n" >::: [
        "base16" >:: run (module Cf_base16.Std) base16;
        "base32" >:: run (module Cf_base32.Std) base32;
        "base32hex" >:: run (module Cf_base32.Hex) base32hex;
        "base64" >:: run (module Cf_base64.Std) base64;
    ]
end

module T_regx: T_signature = struct
    open Cf_regx.DFA.Affix

    module Quote = struct
        let qlist = [
            "abc", "abc";
            {|(a*b+c?.[\t +?]|d)|}, {|\(a\*b\+c\?\.\[\\t \+\?\]\|d\)|};
        ]

        let loop (a, b) =
            a >:: fun ctxt ->
                let s = Cf_regx.quote a in
                assert_equal ~ctxt ~msg:"quoted" s b;
                let s = Cf_regx.unquote b in
                let msg =
                    Printf.sprintf "unquote [produced %s] [expected %s]" s a
                in
                assert_equal ~ctxt ~msg s a

        let test = List.map loop qlist
    end

    module Recognize = struct
        let xlist = [

            "a", [ "a" ], [ "x"; "ax"; "" ];

            "a*", [ ""; "a"; "aaa" ], [ "x"; "ax"; "aaax" ];

            "abc", [ "abc" ], [ "xxxx" ];

            "[abc]+", [ "abccabc" ], [ "abcdabcd" ];

            {|\a[\i-.+]*|}, [ "http"; "hg+ssh-1" ], [ ""; "23"; "foo*bar" ];

            "ab+c*d?e", [ "abe"; "abbcde" ], [ "ae"; "acde"; "xxxx" ];

            "a(bc)+d", [ "abcd"; "abcbcd" ], [ "abc"; "abcabc"; "abcdd" ];

            "(a|b)*abb", [ "abababb"; "ababbabb" ], [ "ababab"; "cde" ];

            {|[_\a][_\i]*|}, [ "id0"; "_"; "long_id" ], [ "0"; "" ];

            {|[ \t]*\w+|}, [ "foo"; "\t  foo" ], [ "\n"; " "; "" ];

            {|[^abc]+|}, [ "def" ], [ ""; "abc"; "aabbcc" ];
        ]

        let message fn e s v =
            let fmt: ('a, 'b, 'c) format =
                match e with
                | true -> "%s of \"%s\" expected with \"%s\""
                | false -> "%s of \"%s\" not expected with \"%s\""
            in
            Printf.sprintf fmt fn s v

        let ck_regx ctxt s regx e v =
            let msg = message "recognition" e s v in
            assert_equal ~ctxt ~msg e (Cf_regx.test regx v)

        let ck_scan ctxt s scan e v =
            let msg = message "scanning" e s v in
            let e' =
                match Cf_scan.ASCII.of_string scan v with
                | exception Not_found -> false
                | _ -> true
            in
            assert_equal ~ctxt ~msg e e'

        let loop (s, yes, no) =
            s >:: fun ctxt ->
                let scan = Cf_lex_scan.ASCII.(string_to_term s |> simple) in
                List.iter (ck_scan ctxt s scan true) yes;
                List.iter (ck_scan ctxt s scan false) no;
                let regx = Cf_regx.of_dfa_term !$s in
                List.iter (ck_regx ctxt s regx true) yes;
                List.iter (ck_regx ctxt s regx false) no;
                assert true

        let test = List.map loop xlist
    end

    module Scan = struct
        open Cf_lex_scan.ASCII.Affix

        let rules () = List.to_seq [
            {|\a+\d+\a+|}   $$> (fun x -> `ADA x);
            {|\a\d+|}       $$> (fun x -> `AD x);
        ]

        let input = "x23y"

        let test ctxt =
            let scan = rules () |> Cf_lex_scan.ASCII.analyze in
            match Cf_scan.ASCII.of_string scan input with
            | `ADA s -> assert_equal ~ctxt ~msg:"not matched" s input
            | `AD _ -> assert_failure "not longest match"
    end

    let test =
        "regx" >::: [
            "quote" >::: Quote.test;
            "recognize" >::: Recognize.test;
            "scan" >:: Scan.test;
        ]
end

module T_uri: T_signature = struct
    open Cf_uri

    module T_text: T_signature = struct
        let tlist = [|
            "scheme:";
            "scheme:opaque";
            "scheme:/path/a/b/c";
            "scheme://authority";
            "scheme://authority/";
            "scheme://authority/path";
            "scheme://authority/path/a/b/c";
            "scheme://authority/path/a/b/c?query";
            "scheme://authority/path/a/b/c#fragment";
            "scheme://authority/path/a/b/c?query#fragment";
        |]

        let run ~ctxt s =
            match of_string s with
            | exception (Invalid_argument msg) ->
                assert_failure msg
            | uri ->
                let s' = to_string uri in
                let msg = Printf.sprintf "normal(%s) <> %s" s s' in
                assert_equal ~ctxt ~msg s s'

        let test =
            let f uri = uri >:: fun ctxt -> run ~ctxt uri in
            "text" >::: List.of_seq @@ Seq.map f @@ Array.to_seq tlist
    end

    module T_dotpath: T_signature = struct
        let rlist = [|
            "", "";
            "a", "a";
            ".", ".";
            "..", "..";
            "../a", "../a";
            "a/.", "a/";
            "a/..", ".";
            "a/./b", "a/b";
            "a/../b", "b";
            "/", "/";
            "/a", "/a";
            "/a/", "/a/";
            "/.", "/";
            "/./a", "/a";
            "/a/./b", "/a/b";
            "/a/..", "/";
            "/a/../b", "/b";
            "/a/..", "/";
        |]

        let run ~ctxt r0 r1 =
            let uref = Cf_scan.ASCII.of_string scan_reference r0 in
            let b = Buffer.create (String.length r0) in
            Cf_emit.apply emit_reference b uref;
            let r0' = Buffer.contents b in
            let msg = Printf.sprintf "dotpath(%s) -> %s <> %s" r0 r0' r1 in
            assert_equal ~ctxt ~msg r0' r1

        let test =
            let f (r0, r1) = r0 >:: fun ctxt -> run ~ctxt r0 r1 in
            "dotpath" >::: List.of_seq @@ Seq.map f @@ Array.to_seq rlist
    end

    module T_resolve: T_signature = struct
        let rlist = [|
            "g:h", "g:h";
            "g", "http://a/b/c/g";
            "./g", "http://a/b/c/g";
            "g/", "http://a/b/c/g/";
            "/g", "http://a/g";
            "//g", "http://g";
            "?y", "http://a/b/c/d;p?y";
            "g?y", "http://a/b/c/g?y";
            "#s", "http://a/b/c/d;p?q#s";
            "g#s", "http://a/b/c/g#s";
            "g?y#s", "http://a/b/c/g?y#s";
            ";x", "http://a/b/c/;x";
            "g;x", "http://a/b/c/g;x";
            "g;x?y#s", "http://a/b/c/g;x?y#s";
            "", "http://a/b/c/d;p?q";
            ".", "http://a/b/c/";
            "./", "http://a/b/c/";
            "..", "http://a/b/";
            "../", "http://a/b/";
            "../g", "http://a/b/g";
            "../..", "http://a/";
            "../../", "http://a/";
            "../../g", "http://a/g";
            "../../../g", "http://a/g";
            "../../../../g", "http://a/g";
            "/./g", "http://a/g";
            "/../g", "http://a/g";
            "/g.", "http://a/g.";
            "/.g", "http://a/.g";
            "/g..", "http://a/g..";
            "/..g", "http://a/..g";
            "./../g", "http://a/b/g";
            "./g/.", "http://a/b/c/g/";
            "g/./h", "http://a/b/c/g/h";
            "g;x=1/./y", "http://a/b/c/g;x=1/y";
            "g;x=1/../y", "http://a/b/c/y";
            "g?y/./x", "http://a/b/c/g?y/./x";
            "g?y/../x", "http://a/b/c/g?y/../x";
            "g#s/./x", "http://a/b/c/g#s/./x";
            "g#s/../x", "http://a/b/c/g#s/../x";
        |]

        let run ~ctxt r0 ck =
            let base = of_string "http://a/b/c/d;p?q" in
            let rel = Cf_scan.ASCII.of_string scan_reference r0 in
            let uri = resolve base rel |> to_string in
            let msg = Printf.sprintf "resolve(%s) -> %s <> %s" r0 uri ck in
            assert_equal ~ctxt ~msg uri ck

        let test =
            let f (r0, ck) = r0 >:: fun ctxt -> run ~ctxt r0 ck in
            "resolve" >::: List.of_seq @@ Seq.map f @@ Array.to_seq rlist
    end

    let test = "uri" >::: [
        T_text.test;
        T_dotpath.test;
        T_resolve.test;
    ]
end

module T_tai64: T_signature = struct
    type case = Case of {
        name: string;
        seconds: float;
        unix: Unix.tm;
        tai64: Cf_tai64.t;
    }

    let tai_show t = Printf.sprintf "0x%Lx" Cf_tai64.(sub t first)

    let tm_show tm =
        let open Unix in
        Printf.sprintf "%d-%02d-%02dT%02d:%02d:%02dZ"
            (tm.tm_year + 1900) (tm.tm_mon + 1) tm.tm_mday tm.tm_hour tm.tm_min
            tm.tm_sec

    let tm_create year month day hour minute second = {
        Unix.tm_isdst = false; tm_yday = 0; tm_wday = 0;
        tm_year = year - 1900;
        tm_mon = month - 1;
        tm_mday = day;
        tm_hour = hour;
        tm_min = minute;
        tm_sec = second;
    }

    let tm_equal a b =
        let open Unix in
        a.tm_year == b.tm_year &&
        a.tm_mon == b.tm_mon &&
        a.tm_mday == b.tm_mday &&
        a.tm_hour == b.tm_hour &&
        a.tm_min == b.tm_min &&
        a.tm_sec == b.tm_sec

    let tai64_create n = Cf_tai64.(add_int64 first n)

    let Cf_leap_second.Archive leaps = Cf_leap_second.current ()

    let unix_epoch = Cf_tai64.of_unix_time 0.0

    let offset_at_unix_seconds s =
        let _, offset =
            Cf_leap_second.search @@
            Cf_tai64.add_int64 unix_epoch @@
            Int64.pred @@
            Int64.of_float s
        in
        offset

    let cases = [
        Case {
            name = "1969-12-31T23:59:58";
            seconds = (-2.0);
            unix = tm_create 1969 12 31 23 59 58;
            tai64 = tai64_create 0x4000_0000_0000_0008L;
        };
        Case {
            name = "1970-01-01T00:00:00";
            seconds = 0.0;
            unix = tm_create 1970 1 1 0 0 0;
            tai64 = tai64_create 0x4000_0000_0000_000aL;
        };
        Case {
            name = "2022-01-02T04:49:10";
            seconds = 1641098950.0;
            unix = tm_create 2022 1 2 4 49 10;
            tai64 = tai64_create 0x4000_0000_61d1_2eebL;
        };
    ]

    let of_stdtime utc =
        let Cf_gregorian.Date date = utc#date in
        let Cf_clockface.Time time = utc#time in {
            Unix.tm_isdst = false; tm_yday = 0; tm_wday = 0;
            tm_year = Int64.(to_int (sub date.year 1900L));
            tm_mon = date.month - 1;
            tm_mday = date.day;
            tm_hour = time.hour;
            tm_min = time.minute;
            tm_sec = time.second;
        }

    let of_unix_time (Case c) = c.name >:: fun ctxt ->
        let unix_seconds, _ = Unix.mktime c.unix in
        let offset = offset_at_unix_seconds unix_seconds in
        let adjust = Float.of_int @@ leaps.current - offset in
        let tai_unix_seconds = unix_seconds -. adjust in
        assert_equal ~ctxt ~msg:"Unix.mktime" ~printer:(Printf.sprintf "%f")
            c.seconds unix_seconds;
        let t = Cf_tai64.of_unix_time tai_unix_seconds in
        assert_equal ~ctxt ~msg:"Cf_tai64.of_unix_time" ~cmp:Cf_tai64.equal
            ~printer:tai_show c.tai64 t

    let to_unix_time (Case c) = c.name >:: fun ctxt ->
        let tai_unix_seconds = Cf_tai64.to_unix_time c.tai64 in
        let offset = offset_at_unix_seconds tai_unix_seconds in
        let unix_seconds =
            tai_unix_seconds +. Float.of_int (leaps.current - offset)
        in
        let tm = Unix.gmtime unix_seconds in
        assert_equal ~ctxt ~msg:"Unix.to_unix_time" ~cmp:tm_equal
            ~printer:tm_show c.unix tm;
        let Cf_stdtime.UTC utc = Cf_stdtime.of_tai64 c.tai64 in
        let tm' = of_stdtime utc in
        assert_equal ~ctxt ~msg:"Cf_stdtime.of_tai64" ~cmp:tm_equal
            ~printer:tm_show tm' tm

    let test = "tai64" >::: [
        "of_unix_time" >::: List.map of_unix_time cases;
        "to_unix_time" >::: List.map to_unix_time cases;
    ]
end

let all = "t" >::: [
    T_horizon.test;
    T_annot.test;
    T_channel_codec.test;
    T_endian.test;
    T_stdtime.test;
    T_seq.test;
    T_monad.test;
    T_deque.test;
    T_bsearch.test;
    T_rbtree.test;
    T_data_render.test;
    T_parser.test;
    T_number_scan.test;
    T_data_ingest.test;
    T_record_scan.test;
    T_radix_n.test;
    T_regx.test;
    T_uri.test;
    T_tai64.test;
]

let _ = run_test_tt_main all

(*--- End ---*)
