(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Support logic for arbitrary Radix-N transcodings. *)

(** {6 Overview}

    This module provides the basis interface for Radix-16, -32 and -64
    encodings using arbitrary alphabets.

    Note well: the implementation comprises a record type and a function that
    returns a first-class module, rather than a functor, for internal reasons
    related to Radix-32 transformations requiring a working accumulator that
    is either [int] or [int64] depending on the value of [Sys.int_size].
*)

(** {6 Basis Types} *)

(** Values of this type are returned by [check] functions. *)
type check = Digit | Skip | Pad | Invalid

(** Values of this type signal whether pad characters are required or merely
    recommended.
*)
type req = Must | Should

(** Values of this type describe the form of the Radix-N transform. *)
type basis = Basis of {
    pad: (req * char) option;   (** Pad characters. *)
    check: char -> check;       (** Check the kind of character. *)
    decode: char -> int;        (** Return the digit for a character. *)
    encode: int -> char;        (** Return the character for a digit. *)
    radix: int;                 (** The radix of the transform. *)
}

(** {6 Composition} *)

(** The exception signaling a encoding error. *)
type exn += private Error

(** Modules of this type implement the various transcoding methods. *)
module type Profile = sig

    (** The basis of the transform. *)
    val basis: basis

    (** Use [decode_seq s] to decode the digits in [s] according to the rules
        for the transform. Evaluating the result raises [Error] if an error in
        the encoding is encountered. Optionally use [~n] to specify the
        required length of the decoded octets, raising [Error] if the end of
        [s] is not found immediately following the last digit in the encoding.
    *)
    val decode_seq: ?n:int -> char Seq.t -> char Seq.t

    (** Use [decode_string s] to decode the digits in [s] according to the
        rules for the transform. Returns [None] if an error is encountered.
        Optionally use [~n] to specify the required length of the decoded
        octets.
    *)
    val decode_string: ?n:int -> string -> string option

    (** Use [decode_slice s] to decode the digits in [s] according to the
        rules for the transform. Returns [None] if an error is encountered.
        Optionally use [~n] to specify the required length of the decoded
        octets.
    *)
    val decode_slice: ?n:int -> string Cf_slice.t -> string option

    (** Use [encode_seq s] to encode the octets in [s] according to the
        rules for the transform. Use [~brk:(n, s)] to insert [s] every [n]
        digits. Use [~np:()] to disable output of pad characters. If pad
        characters are mandatory for the transform, then using [~np:()] raises
        [Invalid_argument].
    *)
    val encode_seq:
        ?brk:(int * string) -> ?np:unit -> char Seq.t -> char Seq.t

    (** Use [encode_string s] to encode the octets in [s] according to the
        rules for the transform. Use [~brk:(bn, bs)] to insert [bs] every [bn]
        digits. Use [~np:()] to disable output of pad characters. If pad
        characters are mandatory for the transform, then using [~np:()] raises
        [Invalid_argument].
    *)
    val encode_string: ?brk:(int * string) -> ?np:unit -> string -> string

    (** Use [encode_slice s] to encode the octets in [s] according to the
        rules for the transform. Use [~brk:(bn, bs)] to insert [bs] every [bn]
        digits. Use [~np:()] to disable output of pad characters. If pad
        characters are mandatory for the transform, then using [~np:()] raises
        [Invalid_argument].
    *)
    val encode_slice:
        ?brk:(int * string) -> ?np:unit -> string Cf_slice.t -> string
end

(** Use [create b] to make a module that implements the encoding. *)
val create: basis -> (module Profile)

(*--- End ---*)
