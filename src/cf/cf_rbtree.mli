(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional red-black binary trees. *)

(** {6 Overview}

    This module implements functional sets and maps based on red-black binary
    trees.  This permits trees that can be used as an alternative to the [Set]
    and [Map] modules in the Ocaml standard library.  For many operations on
    sets and maps, red-black binary trees give better performance that the
    balanced trees in the standard library (though some applications may see
    better performance with the standard modules).
*)

(** {6 Interfaces} *)

(** An alternative to the [Set] module in the OCaml standard library. *)
module Set: sig

    (** The module type of functional sets. *)
    module type Profile = sig

        (** The element type. *)
        type element

        (** The set type *)
        type t

        (** The empty set. *)
        val nil: t

        (** Use [one e] to create a set with one element [v]. *)
        val one: element -> t

        (** Use [min u] to return the ordinally least element in [u]. Raises
            [Not_found] if [u] is empty.
        *)
        val min: t -> element

        (** Use [min u] to return the ordinally greatest element in [u]. Raises
            [Not_found] if [u] is empty.
        *)
        val max: t -> element

        (** Use [empty u] to test whether [u] is the empty set. *)
        val empty: t -> bool

        (** Use [size u] to compute the size of [u]. *)
        val size: t -> int

        (** Use [member e u] to test whether the element [e] is a member of the
            set [u].
        *)
        val member: element -> t -> bool

        (** Use [compare u1 u2] to compare the sequence of elements in [u1] and
            the sequence of elements in [u2] in order of increasing ordinality.
            Two sets are ordinally equal if the sequences of their elements are
            ordinally equal.
        *)
        val compare: t -> t -> int

        (** Use [subset u1 u2] to test whether [u1] is a subset of [u2]. *)
        val subset: t -> t -> bool

        (** Use [disjoint u1 u2] to test whether [u1] and [u2] are disjoint. *)
        val disjoint: t -> t -> bool

        (** Use [put e u] to obtain a new set produced by inserting the element
            [e] into [u].  If [s] already contains a member ordinally equal to
            [e] then it is replaced by [e] in the set returned.
        *)
        val put: element -> t -> t

        (** Use [clear e u] to obtain a new set produced by deleting the
            element in [u] ordinally equal to the element [e].  If there is no
            such element in the set, then the set is returned unchanged.
        *)
        val clear: element -> t -> t

        (** Use [union u1 u2] to obtain a new set from the union of [u1] and
            [u2].  Elements of the new set belonging to the intersection are
            copied from [u2].
        *)
        val union: t -> t -> t

        (** Use [diff u1 u2] to obtain a new set from the difference of [u1]
            and [u2].
        *)
        val diff: t -> t -> t

        (** Use [interset u1 u2] to obtain a new set from the intersection of
            [u1] and [u2].  All the elements in the new set are copied from
            [u2].
        *)
        val intersect: t -> t -> t

        (** Use [of_seq s] to consume a sequence [s] and compose a new set by
            inserting each value produced in the order produced.
        *)
        val of_seq: element Seq.t -> t

        (** Use [of_seq_incr s] to compose the set with elements in the
            sequence [s].  Runs in linear time if the sequence [c] is known to
            be in increasing order.  Otherwise, there is an additional linear
            cost beyond [of_seq s].
        *)
        val of_seq_incr: element Seq.t -> t

        (** Use [of_seq_decr s] to compose the set with elements in the
            sequence [s].  Runs in linear time if the sequence [c] is known to
            be in decreasing order.  Otherwise, there is an additional linear
            cost beyond [of_seq s].
        *)
        val of_seq_decr: element Seq.t -> t

        (** Use [to_seq_incr u] to produce the list of elements in [u] in order
            of increasing ordinality.
        *)
        val to_seq_incr: t -> element Seq.t

        (** Use [to_seq_decr u] to produce the list of elements in [u] in order
            of decreasing ordinality.
        *)
        val to_seq_decr: t -> element Seq.t

        (** Use [to_seq_nearest_decr e u] to obtain the element ordinally less
            than or equal to [e] in [u].  Raises [Not_found] if [u] is empty or
            all the keys are ordinally greater.
        *)
        val to_seq_nearest_decr: element -> t -> element Seq.t

        (** Use [to_seq_nearest_incr e u] to obtain the element ordinally
            greater than or equal to [e] in [u].  Raises [Not_found] if [u] is
            empty or all the keys are ordinally lesser.
        *)
        val to_seq_nearest_incr: element -> t -> element Seq.t
    end

    (** A functor to create a [Set] module. *)
    module Create(E: Cf_relations.Order): Profile with type element := E.t
end

(** An alternative to the [Map] module in the OCaml standard library. *)
module Map: sig

    (** The module type of functional maps. *)
    module type Profile = sig

        (** The index type. *)
        type index

        (** The map type. *)
        type +'a t

        (** The empty map. *)
        val nil: 'a t

        (** Use [one (k, v)] to create a map with one key [k] for value [v]. *)
        val one: (index * 'a) -> 'a t

        (** Use [empty m] to test whether [m] is the empty map. *)
        val empty: 'a t -> bool

        (** Use [size m] to count the number of keys in [m]. *)
        val size: 'a t -> int

        (** Use [min m] to obtain the key-value pair with the ordinally minimum
            key in [m].  Raises [Not_found] if [m] is empty.
        *)
        val min: 'a t -> (index * 'a)

        (** Use [max m] to obtain the key-value pair with the ordinally maximum
            key in [m].  Raises [Not_found] if [m] is empty.
        *)
        val max: 'a t -> (index * 'a)

        (** Use [member k m] to test whether [m] contains [k]. *)
        val member: index -> 'a t -> bool

        (** Use [search k m] to find [Some v] associated in the map [m] with
            key [k]. Returns [None] if [m] does not contain [k].
        *)
        val search: index -> 'a t -> 'a option

        (** Use [require k m] to find the value assocoated in the map [m] with
            the key [k]. Raises [Not_found] if [m] does not contain [k].
        *)
        val require: index -> 'a t -> 'a

        (** Use [insert p m] to insert the key-value pair [p] into [m],
            producing a new map with the inserted element and, if [k] is
            already present in [m], the value replaced by the insertion.
        *)
        val insert: (index * 'a) -> 'a t -> 'a t * 'a option

        (** Use [replace p m] to obtain a new map produced by inserting the
            key-value pair [p] into the map [m], replacing any existing pair
            associated to the same key.
        *)
        val replace: (index * 'a) -> 'a t -> 'a t

        (** Use [modify k f m] to obtain a new tree produced by replacing the
            value in [m] associated with [k] with the result of applying it to
            the continuation function [f].  Raises [Not_found] if [m] does not
            contain the key.
        *)
        val modify: index -> ('a -> 'a) -> 'a t -> 'a t

        (** Use [extract k m] to obtain the value associated with the key [k]
            in [m] and a new map with the key deleted from the map. Raises
            [Not_found] if the map does not contain the key.
        *)
        val extract: index -> 'a t -> 'a * 'a t

        (** Use [delete k m] to obtain a new map produced by deleting the key
            [k] from [m].  If [m] does not contain the key, then the function
            simply returns its argument.
        *)
        val delete: index -> 'a t -> 'a t

        (** Use [of_seq s] to consume a sequence [s] and compose a new map by
            inserting each key-value pair produced in the order produced.
        *)
        val of_seq: (index * 'a) Seq.t -> 'a t

        (** Use [of_seq_incr s] to compose the map with key-value pairs in the
            sequence [s].  Runs in linear time if the sequence [s] is known to
            be in increasing order.  Otherwise, there is an additional linear
            cost beyond [of_seq s].
        *)
        val of_seq_incr: (index * 'a) Seq.t -> 'a t

        (** Use [of_seq_decr s] to compose the map with key-value pairs in the
            sequence [s].  Runs in linear time if the sequence [s] is known to
            be in decreasing order.  Otherwise, there is an additional linear
            cost beyond [of_seq s].
        *)
        val of_seq_decr: (index * 'a) Seq.t -> 'a t

        (** Use [to_seq_incr m] to produce the list of key-value pairs in [m]
            in order of increasing key ordinality.
        *)
        val to_seq_incr: 'a t -> (index * 'a) Seq.t

        (** Use [to_seq_decr m] to produce the list of key-value pairs in [m]
            in order of decreasing ordinality.
        *)
        val to_seq_decr: 'a t -> (index * 'a) Seq.t

        (** Use [to_seq_nearest_decr k m] to obtain the key-value pair
            ordinally less than or equal to [k] in [m].  Raises [Not_found] if
            [m] is empty or all the keys are ordinally greater.
        *)
        val to_seq_nearest_decr: index -> 'a t -> (index * 'a) Seq.t

        (** Use [to_seq_nearest_incr k m] to obtain the key-value pair
            ordinally greater than or equal to [k] in [m].  Raises [Not_found]
            if [m] is empty or all the keys are ordinally greater.
        *)
        val to_seq_nearest_incr: index -> 'a t -> (index * 'a) Seq.t
    end

    (** A functor to create a [Map] module. *)
    module Create(K: Cf_relations.Order): Profile with type index := K.t
end

(*--- End ---*)
