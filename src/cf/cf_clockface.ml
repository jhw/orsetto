(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type time = Time of {
    hour: int;      (** 0 .. 23 *)
    minute: int;    (** 0 .. 59 *)
    second: int;    (** 0 .. 60 *)
}

let unsafe_create hour minute second = Time { hour; minute; second }

let create ~hour ~minute ~second =
    if hour < 0 || hour > 23 ||
        minute < 0 || minute > 59 ||
        second < 0 || second > 60 ||
        (second = 60 && not (hour = 23 && minute = 59))
    then
        invalid_arg (__MODULE__ ^ ".create_time: invalid 24-hour time");
    Time { hour; minute; second }

let to_seconds (Time t) = t.second + (t.minute * 60) + (t.hour * 3600)

(*--- End ---*)
