(*---------------------------------------------------------------------------*
  Copyright (C) 2016-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** The continuation monad and its operators. *)

(** {6 Overview} *)

(** A continuation monad represents a computation composed of stages that can
    be interrupted, resumed and rescheduled.  Because Objective Caml is an
    eager language programming in the continuation-passing style (CPS) can be
    simplified by the use of the continuation monad and its operators.
*)

(** {6 Types} *)

(** The continuation monad: a function for passing intermediate results from
    continuation context to continuation context.
*)
type ('x, 'a) t = ('a -> 'x) -> 'x
module Basis: Cf_monad.Binary.Basis with type ('x, 'a) t := ('x, 'a) t
include Cf_monad.Binary.Profile with type ('x, 'a) t := ('x, 'a) t

(** A monad that returns [unit] and performs no operation. *)
val nil: ('x, unit) t

(** Use [init x] to discard the current context and pass [x] into the
    continuation.
*)
val init: 'x -> ('x, 'a) t

(** Use [cont f] to apply [f] to the current context and pass the result into
    the continuation.
*)
val cont: ('x -> 'x) -> ('x, unit) t

(** Use [eval m] to evaluate the continuation monad, which produces a function
    from an initial context to a final context.
*)
val eval: ('x, unit) t -> 'x -> 'x

(** Use [bridge x f m] as a shortcut for [init (f (eval m x))]. *)
val bridge: 'x -> ('x -> 'y) -> ('x, unit) t -> ('y, unit) t

(*--- End ---*)
