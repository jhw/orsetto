(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module type Basis = sig
    type symbol
    type position

    module Index: Cf_relations.Order
    module Content: Cf_type.Form
    module Form: Cf_scan.Form

    module Scan: sig

        include Cf_scan.Profile
           with type symbol := symbol
            and type position := position
            and type 'a form := 'a Form.t

        val index: Index.t t
        val preval: unit t option

        open Cf_chain_scan
        val chain: (ctrl * ctrl * unit t) option
    end
end

module type Profile = sig
    type index
    type +'a form
    type +'a t

    type field =
        | Required: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
        | Optional: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
        | Default:
            { nym: 'a Cf_type.nym; scan: 'a form t; default: 'a } -> field

    type schema

    val schema: ?ignore:(int * unit t) -> (index * field) list -> schema
    val range: schema -> int * int

    type pack

    val scan: schema -> pack t
    val unpack: pack -> index -> 'a Cf_type.nym -> 'a form
end

module Create(B: Basis) = struct
    module S = struct
        include B.Scan

        module CB = struct
            include B
            type 'a form = 'a B.Form.t
        end

        include Cf_chain_scan.Create(CB)
    end

    type 'a form = 'a B.Form.t
    type 'a t = 'a S.t

    type field =
        | Required: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
        | Optional: { nym: 'a Cf_type.nym; scan: 'a form t } -> field
        | Default:
            { nym: 'a Cf_type.nym; scan: 'a form t; default: 'a } -> field

    module Map = Cf_rbtree.Map.Create(B.Index)
    type pack = Cf_type.opaque form Map.t

    type schema = Schema of {
        output: Cf_type.opaque form Map.t;
        fields: field Map.t;
        required: int;
        ignore: (int * unit t) option;
    }

    let range (Schema s) =
        let a = s.required in
        let b = a + (match s.ignore with None -> 0 | Some (n, _) -> n) in
        a, b

    open S.Affix

    let failmsg = Printf.sprintf "%s: required field" __MODULE__

    let errf () = S.fail failmsg
    let reqf p = S.or_fail failmsg p

    let schema =
        let invalid msg =
            Printf.sprintf "%s.schema: %s." __MODULE__ msg |> invalid_arg
        in
        let rec loop outm inpm required = function
            | [] ->
                outm, inpm, required
            | hd :: tl ->
                let key, field = hd in
                if Map.member key inpm then invalid_arg "duplicate key";
                let outm, required, field =
                    match field with
                    | Required f ->
                        let required = succ required in
                        let field = Required { f with scan = reqf f.scan } in
                        outm, required, field
                    | Optional f ->
                        let v = Cf_type.witness (Cf_type.Option f.nym) None in
                        let outm = Map.replace (key, B.Form.imp v) outm in
                        let field = Optional { f with scan = reqf f.scan } in
                        outm, required, field
                    | Default f ->
                        let v = Cf_type.witness f.nym f.default in
                        let outm = Map.replace (key, B.Form.imp v) outm in
                        let field = Default { f with scan = reqf f.scan } in
                        outm, required, field
                in
                let inpm = Map.replace (key, field) inpm in
                (loop[@tailcall]) outm inpm required tl
        in
        let enter ?ignore fields =
            let ignore =
                match ignore with
                | Some (n, _) when n <= 0 ->
                    invalid "~ignore:(n, _) with n <= 0"
                | ignore ->
                    ignore
            in
            let output, fields, required = loop Map.nil Map.nil 0 fields in
            Schema { output; fields; required; ignore }
        in
        enter

    let scan =
        let p_header =
            match S.chain with
            | None ->
                S.return false
            | Some (a, b, p) ->
                let S.Chain cr = S.mk ~a ~b p in
                S.sep0 ?xf:cr.fail cr.header cr.separator
        and p_trailer =
            match S.chain with
            | None ->
                S.return false
            | Some (a, b, p) ->
                let S.Chain cr = S.mk ~a ~b p in
                let ctrl = if cr.trailer == `Req then `Req else `Opt in
                let* rxsep = S.sep0 ?xf:cr.fail ctrl cr.separator in
                S.return @@ if cr.trailer == `Opt then false else rxsep
        and p_infix =
            match S.preval with
            | None -> S.return ()
            | Some p -> reqf p
        in
        let rec p_index rxsep schema =
            let Schema sr = schema in
            if rxsep || sr.required > 0 then begin
                let* index = reqf S.index in
                p_value index schema
            end
            else begin
                S.opt S.index >>= function
                | Some index -> p_value index schema
                | None -> S.return sr.output
            end
        and p_value index schema =
            let* _ = p_infix in
            let Schema sr = schema in
            match Map.extract index sr.fields with
            | exception Not_found ->
                p_nofield schema
            | field, fields ->
                match field with
                | Required fr ->
                    let* vl = fr.scan in
                    let v = Cf_type.witness fr.nym @@ B.Form.dn vl in
                    let value = B.Form.mv v vl in
                    let required = pred sr.required in
                    p_output required index value fields schema
                | Optional fr ->
                    let* vl = fr.scan in
                    let nym = Cf_type.(Option fr.nym) in
                    let v = Cf_type.witness nym @@ Some (B.Form.dn vl) in
                    let value = B.Form.mv v vl in
                    p_output sr.required index value fields schema
                | Default fr ->
                    let* vl = fr.scan in
                    let v = Cf_type.witness fr.nym @@ B.Form.dn vl in
                    let value = B.Form.mv v vl in
                    p_output sr.required index value fields schema
        and p_nofield schema =
            let Schema sr = schema in
            match sr.ignore with
            | Some (n, p) when n > 0 ->
                let schema = Schema { sr with ignore = Some (pred n, p) } in
                let* _ = p in
                let* rxsep = p_trailer in
                p_index rxsep schema
            | (None | Some _) ->
                errf ()
        and p_output required index value fields schema =
            let Schema sr = schema in
            let output = Map.replace (index, value) sr.output in
            let schema = Schema { sr with output; fields; required } in
            let* rxsep = p_trailer in
            p_index rxsep schema
        in
        let enter schema =
            let* rxsep = p_header in
            p_index rxsep schema
        in
        enter

    let unpack values index nym =
        match Map.search index values with
        | Some vl ->
            let v = B.(Content.req nym @@ Form.dn vl) in
            B.Form.mv v vl
        | None ->
            let msg = __MODULE__ ^ ".unpack: undefined index!" in
            invalid_arg msg
end

(*--- End ---*)
