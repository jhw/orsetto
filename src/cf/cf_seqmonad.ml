(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2020, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Functor = struct
    module type Unary = sig
        type +'r t

        val collect: 'r t Seq.t -> (int * 'r list) t
        val serial: unit t Seq.t -> unit t
    end

    module type Binary = sig
        type ('m, +'r) t

        val collect: ('m, 'r) t Seq.t -> ('m, int * 'r list) t
        val serial: ('m, unit) t Seq.t -> ('m, unit) t
    end

    module type Trinary = sig
        type ('p, 'q, +'r) t

        val collect: ('p, 'q, 'r) t Seq.t -> ('p, 'q, int * 'r list) t
        val serial: ('p, 'q, unit) t Seq.t -> ('p, 'q, unit) t
    end

    module Core = Cf_monad_core

    let rec collect ~basis ~n ~vs s =
        let return, ( let* ) = basis in
        match s () with
        | Seq.Nil ->
            return (n, vs)
        | Seq.Cons (hd, tl) ->
            let* v = hd in
            let n = succ n and vs = v :: vs in
            collect ~basis ~n ~vs tl

    let rec serial ~basis s =
        let return, ( let* ) = basis in
        match s () with
        | Seq.Nil -> return ()
        | Seq.Cons (hd, tl) -> let* _ = hd in serial ~basis tl

    module Unary(B: Core.Unary.Basis) = struct
        type 'r t = 'r B.t

        let basis = B.return, B.bind
        let collect ts = collect ~basis ~n:0 ~vs:[] ts
        let serial s = serial ~basis s
    end

    module Binary(B: Core.Binary.Basis) = struct
        type ('m, 'r) t = ('m, 'r) B.t

        let basis = B.return, B.bind
        let collect ts = collect ~basis ~n:0 ~vs:[] ts
        let serial s = serial ~basis s
    end

    module Trinary(B: Core.Trinary.Basis) = struct
        type ('p, 'q, 'r) t = ('p, 'q, 'r) B.t

        let basis = B.return, B.bind
        let collect ts = collect ~basis ~n:0 ~vs:[] ts
        let serial s = serial ~basis s
    end
end

module Basis = struct
    open Seq
    type ('v, 'a) t = ('a -> 'v node) -> 'v node

    let return r f = f r
    let bind m f s = m (fun r -> f r s)

    let product = `Default
    let mapping = `Default
end

module Binary = Cf_monad_core.Binary
include Binary.Create(Basis)
include Functor.Binary(Basis)

let one v f = Seq.Cons (v, f)

let all =
    let rec loop s f () =
        match s () with
        | Seq.Nil -> f ()
        | Seq.Cons (v, s) -> Seq.Cons (v, loop s f)
    in
    fun s f ->
        (loop[@tailcall]) s f ()

let eval m () = (m[@tailcall]) Seq.empty

(*--- End ---*)
