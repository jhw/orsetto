(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type size = Size of int [@@ocaml.unboxed]
type position = Position of int [@@ocaml.unboxed]

let uintreq n =
    if n < 0 then
        invalid_arg (__MODULE__ ^ ": non-negative integer required!")

let overflow () =
    failwith (__MODULE__ ^ ": octet sequence length overflow!")

let addsize na nb =
    if Sys.max_string_length - na < nb then overflow ();
    na + nb

let mulsize n x =
    assert (x > 0);
    if Sys.max_string_length / x < n then overflow ();
    x * n

let addpos (Position i) n = Position (i + n)

type 'x analysis = Analysis of int * 'x

exception Aux_incomplete of int
exception Aux_invalid of position * string

let invalid i m = raise (Aux_invalid (i, m))

let advance adj (Position i) =
    uintreq adj;
    Position (i + adj)

let ckwindow (Size have) need =
    uintreq have;
    uintreq need;
    if need > have then raise (Aux_incomplete (need - have)) else need

let analyze have need =
    let adj = ckwindow have need in
    fun x -> Analysis (adj, x)

type -'v scheme = Scheme: {
    sz: int;
    ck: position -> size -> 'v -> 'x analysis;
    wr: 'x -> bytes -> int -> unit;
} -> 'v scheme

let scheme sz ck wr =
    uintreq sz;
    Scheme { sz; ck; wr }

let nil =
    let a = Analysis (0, ()) in
    let sz = 0 and ck _ _ _ = a and wr _ _ _ = () in
    Scheme { sz; ck; wr }

let any =
    let sz = 1
    and ck _ n c = analyze n 1 c
    and wr x b i = Bytes.unsafe_set b i x in
    Scheme { sz; ck; wr }

let opaque =
    let sz = 0
    and ck _ n s = analyze n (String.length s) s
    and wr s b i = Bytes.blit_string s 0 b i (String.length s) in
    Scheme { sz; ck; wr }

let subopaque =
    let open Cf_slice in
    let sz = 0
    and ck _ n ss = analyze n (length ss) ss
    and wr ss b i = Bytes.blit_string ss.vector ss.start b i (length ss) in
    Scheme { sz; ck; wr }

let buffer f =
    let sz = 0
    and ck _ n v =
        let buf = Buffer.create 0 in
        f buf v;
        analyze n (Buffer.length buf) buf
    and wr buf b i =
        Buffer.blit buf 0 b i (Buffer.length buf)
    in
    Scheme { sz; ck; wr }

let opt (Scheme s) =
    let sz = 0
    and ck p n = function
        | None ->
            Analysis (0, None)
        | Some v ->
            let Analysis (n, v) = s.ck p n v in
            Analysis (n, Some v)
    and wr v b i = match v with None -> () | Some v -> s.wr v b i in
    Scheme { sz; ck; wr }

let pair (Scheme sa) (Scheme sb) =
    let sz = addsize sa.sz sb.sz in
    let ck p n0 (va, vb) =
        let n = n0 in
        let Analysis (na, va) = sa.ck p n va in
        let p = addpos p na in
        let Size n = n in
        let n = Size (n - na) in
        let _ = ckwindow n sb.sz in
        let Analysis (nb, vb) = sb.ck p n vb in
        let d = addsize na nb in
        Analysis (ckwindow n0 d, (va, na, vb))
    and wr (va, na, vb) b i =
        sa.wr va b i;
        sb.wr vb b (i + na)
    in
    Scheme { sz; ck; wr }

let triple (Scheme sa) (Scheme sb) (Scheme sc) =
    let sz = sa.sz |> addsize sb.sz |> addsize sc.sz in
    let ck p n0 (va, vb, vc) =
        let n = n0 in
        let Analysis (na, va) = sa.ck p n va in
        let p = addpos p na in
        let Size n = n in
        let n = Size (n - na) in
        let _ = ckwindow n sb.sz in
        let Analysis (nb, vb) = sb.ck p n vb in
        let p = addpos p nb in
        let Size n = n in
        let n = Size (n - nb) in
        let _ = ckwindow n sc.sz in
        let Analysis (nc, vc) = sc.ck p n vc in
        let d = na |> addsize nb |> addsize nc in
        Analysis (ckwindow n0 d, (va, na, vb, nb, vc))
    and wr (va, na, vb, nb, vc) b i =
        sa.wr va b i;
        sb.wr vb b (i + na);
        sc.wr vc b (i + na + nb)
    in
    Scheme { sz; ck; wr }

let seq =
    let rec ckloop w4 rn xsl xs p n vs =
        let sz, ck, a, b = w4 in
        match vs () with
        | Seq.Cons (v, vs) when xsl < b ->
            let _ = ckwindow n sz in
            let Analysis (d, v) = ck p n v in
            let rn = addsize rn d and p = addpos p d in
            let xs = (v, d) :: xs in
            let Size n = n in
            let n = Size (n - d) in
            let xsl = succ xsl in
            (ckloop[@tailcall]) w4 rn xsl xs p n vs
        | Seq.Cons _
        | Seq.Nil ->
            if xsl < a then invalid p (__MODULE__ ^ ".seq: too short.");
            Analysis (rn, xs)
    in
    let rec wrloop wr xs b i =
        match xs with
        | [] ->
            ()
        | (v, n) :: xs ->
            wr v b i;
            (wrloop[@tailcall]) wr xs b (i + n)
    in
    let enter ?(a = 0) ?(b = max_int) (Scheme s) =
        uintreq a;
        uintreq b;
        if a > b then invalid_arg (__MODULE__ ^ ".seq: min > max");
        let sz = if a > 0 && s.sz > 0 then mulsize s.sz a else 0
        and ck = ckloop (s.sz, s.ck, a, b) 0 0 []
        and wr xs b i = (wrloop[@tailcall]) s.wr (List.rev xs) b i in
        Scheme { sz; ck; wr }
    in
    enter

let map f (Scheme s) =
    let ck p i v = s.ck p i (f p v) and wr v b i = s.wr v b i in
    Scheme { s with ck; wr }

module Monad = struct
    module Basis = struct
        type 'r t = (unit scheme, 'r) Cf_seqmonad.t
        let return = Cf_seqmonad.Basis.return
        let bind = Cf_seqmonad.Basis.bind
        let product = Cf_seqmonad.Basis.product
        let mapping = Cf_seqmonad.Basis.mapping
    end

    type 'r t = 'r Basis.t
    include Cf_monad.Unary.Create(Basis)

    let seal (Scheme s) v =
        let ck p i () = s.ck p i v in
        Scheme { s with ck } |> Cf_seqmonad.one

    let eval =
        let rec ckloop rn xs p n bs =
            match bs () with
            | Seq.Nil ->
                let f () = List.rev xs in
                Analysis (rn, f)
            | Seq.Cons (Scheme s, bs) ->
                let _ = ckwindow n s.sz in
                let Analysis (d, v) = s.ck p n () in
                let p = addpos p d in
                let rn = addsize rn d in
                let n = Size (let Size n = n in n - d) in
                let xs = (d, s.wr v) :: xs in
                (ckloop[@tailcall]) rn xs p n bs
        in
        let rec wrloop b i = function
            | [] ->
                ()
            | (n, f) :: tl ->
                f b i;
                (wrloop[@tailcall]) b (i + n) tl
        in
        let enter ?(sz = 0) f =
            uintreq sz;
            let open Cf_seqmonad in
            let ck p i v = (ckloop[@tailcall]) 0 [] p i (eval @@ f v)
            and wr f b i = (wrloop[@tailcall]) b i (f ()) in
            Scheme { sz; ck; wr }
        in
        enter
end

module Render = struct
    module type Basis = sig
        val primitive: 'a Cf_type.nym -> 'a scheme

        val control: [
            | `Default
            | `Special of
                'a scheme -> ('a, 'b) Cf_data_render.control -> 'b scheme
        ]

        val pair: [
            | `Default
            | `Special of
                'k Cf_data_render.pair -> unit Monad.t -> unit Monad.t ->
                unit Monad.t
        ]

        val sequence: [
            | `Default
            | `Special of
                'k Cf_data_render.container -> unit Monad.t Seq.t ->
                unit Monad.t
        ]
    end

    let basis_control:
        type a b. a scheme -> (a, b) Cf_data_render.control -> b scheme
        = fun scheme control ->
            match control with
            | Cf_data_render.Cast f ->
                map (fun _ v -> f v) scheme
            | Cf_data_render.Default v ->
                let f _ opt = match opt with Some v -> v | None -> v in
                map f scheme
            | _ ->
                invalid_arg @@
                    __MODULE__ ^ ".Render.control: unrecognized control"

    module Create(B: Basis) = struct
        module Aux = struct
            include B

            type nonrec 'a scheme = 'a scheme

            let control =
                match B.control with
                | `Default -> basis_control
                | `Special f -> f

            type packet = unit Monad.t

            let packet = Monad.seal
            let delegate f = Monad.eval f

            open Monad.Affix

            let pair =
                let default _container first second =
                    let* () = first in
                    let+ () = second in
                    ()
                in
                match B.pair with
                | `Default -> default
                | `Special f -> f

            let sequence =
                let default _container packets = Monad.serial packets in
                match B.sequence with
                | `Default -> default
                | `Special f -> f
        end

        include Cf_data_render.Create(Aux)
    end
end

let to_string (Scheme s) v =
    match s.ck (Position 0) (Size Sys.max_string_length) v with
    | exception Aux_incomplete _ ->
        failwith (__MODULE__ ^ ".to_string: output too large!")
    | Analysis (n, v) ->
        let b = Bytes.create n in
        s.wr v b 0;
        Bytes.unsafe_to_string b

exception Incomplete of int
exception Invalid of position * string

let slice0 = Cf_slice.of_bytes Bytes.empty

let required_size (Scheme s) v =
    match s.ck (Position 0) (Size max_int) v with
    | exception Aux_incomplete n -> raise (Incomplete n)
    | exception Aux_invalid (p, m) -> raise (Invalid (p, m))
    | Analysis (n, _) -> Size n

class emitter ?(start = Position 0) () = object(self)
    val mutable slice_ = slice0
    val mutable cursor_ = 0
    val mutable start_ = start

    method private work slice =
        slice_ <- slice;
        cursor_ <- Cf_slice.(slice.start)

    method private advance n = cursor_ <- cursor_ + n
    method private incomplete n : unit = raise (Incomplete n)

    method private invalid: 'a. position -> string -> 'a = fun p m ->
        let open Cf_slice in
        let Position i = p in
        assert (i >= cursor_ && i < slice_.limit);
        raise (Invalid (addpos start_ i, m))

    method private allocate:
        'v. 'v scheme -> 'v -> int * (bytes -> int -> unit) = fun sch v ->
        let open Cf_slice in
        let Scheme s = sch in
        try
            let n = Size (slice_.limit - cursor_) in
            let _ = ckwindow n s.sz in
            let Analysis (d, v) = s.ck (Position cursor_) n v in
            d, s.wr v
        with
        | Aux_incomplete d ->
            self#incomplete d;
            (self#allocate[@tailcall]) sch v
        | Aux_invalid (p, m) ->
            self#invalid p m

    method emit: 'v. 'v scheme -> 'v -> unit = fun sch v ->
        let n, wr = self#allocate sch v in
        begin
            try wr slice_.Cf_slice.vector cursor_ with
            | Aux_invalid (pos, msg) ->
                raise (Invalid (pos, msg))
            | Aux_incomplete _ ->
                failwith (__MODULE__ ^ ".emitter: analysis too late!")
        end;
        self#advance n

    method position =
        let Position i = start_ in
        Position (i + cursor_ - slice_.Cf_slice.start)
end

let slice_emitter ?start s = object(self:_)
    inherit emitter ?start ()

    initializer
        self#work s
end

let bytes_emitter ?start b = Cf_slice.of_bytes b |> slice_emitter ?start

class sync_emitter ?start ?limit () = object(self)
    inherit emitter ?start ()

    method! private incomplete n =
        let open Cf_slice in
        if limit <> None then
            failwith (__MODULE__ ^ ".emitter: limit exceeded!");
        assert (n > 0);
        self#work (of_bytes (Bytes.create (slice_.limit + n)))

    initializer
        self#work begin
            match limit with
            | Some n ->
                uintreq n;
                Cf_slice.of_bytes (Bytes.create n)
            | None ->
                slice0
        end
end

class buffer_emitter ?start ?limit b = object
    inherit sync_emitter ?start ?limit ()

    method! private advance n =
        Buffer.add_subbytes b Cf_slice.(slice_.vector) 0 n
end

class channel_emitter ?start ?limit c = object
    inherit sync_emitter ?start ?limit ()

    method! private advance n =
        let s = Bytes.unsafe_to_string Cf_slice.(slice_.vector) in
        output_substring c s 0 n
end

let buffer_emitter = new buffer_emitter
let channel_emitter = new channel_emitter

class framer ?start ?limit () = object(self)
    inherit sync_emitter ?start ?limit ()

    method frame =
        let open Cf_slice in
        let s = Bytes.unsafe_to_string slice_.vector in
        let r = of_substring s 0 cursor_ in
        self#work slice0;
        r
end

let framer = new framer

(*--- End ---*)
