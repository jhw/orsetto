(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Index codes for tree structures. *)

(** This module contains the index node structure shared by tree structures
    like {Cf_rbtree} and {Cf_sbheap} where an index is paired with an indexed
    value.
*)

(** The signature of an index node module. *)
module type Profile = sig

    (** The type of an index value. *)
    type index

    (** The type of an index node. *)
    type +'a t

    (** Tree structures use [cons i v] to construct an index node. *)
    val cons: index -> 'a -> 'a t

    (** Tree structures use [index n] to get the index of [n]. *)
    val index: 'a t -> index

    (** Tree structure use [obj n] to get the codomain value of [n]. *)
    val obj: 'a t -> 'a

    (** Tree structures use [icompare i n] to compare [i] with the index value
        of [n].
    *)
    val icompare: index -> 'a t -> int

    (** Tree structures use [compare a b] to compare the index values of nodes
        [a] and [b] (without regard to the codomain values).
    *)
    val compare: 'a t -> 'a t -> int
end

(** Tree structures use [Unary(E)] to make an node module where [E.t] is the
    only value stored in the node. (The [obj] function asserts [false].)
*)
module Unary(E: Cf_relations.Order): Profile
    with type index = E.t and type +'a t = E.t

(** Tree structures use [Binary(K)] to make an index node where [K.t] is the
    index value associated to its codomain value by the node.
*)
module Binary(K: Cf_relations.Order): Profile
    with type index = K.t and type +'a t = K.t * 'a

(*--- End ---*)
