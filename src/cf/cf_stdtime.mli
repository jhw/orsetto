(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2022, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Conversions between Standard Time, UTC and TAI. *)

(** {6 Overview} *)

(** This module contains a type for representing values of standard time,
    defined as a reference from Coordinated Universal Time (UTC).  It also
    provides functions for converting values of standard time to and from
    TAI64 values (see {!Cf_tai64}).

    {b Note:} no facility is yet provided here for manipulating time with
    associated timezone attributes.  Converting arbitrary values of UTC time
    to and from local time is tricky, since daylight savings time rules vary
    greatly from locality to locality, and some are even subject to change
    without advance notice.

    {b Note:} The historical record of the divergence between TAI and UTC, from
    the creation of the UTC time scale on January 1, 1960 until the
    standardization of the leap second on January 1, 1972, is not preserved.
    During that time period, the offset between TAI and UTC often contained
    fractions of a second, and time authorities adjusted it frequently and
    irregularly. This implementation ignores that history, and the offset from
    TAI to UTC for all points in time prior to January 1, 1972 is ten seconds.

*)

(** {6 Types} *)

(** The class type inhabited by object types representing unqualified local
    time stamps, which comprise a Gregorian calendar date and a civil time
    synchronized with Coordinated Universal Time (UTC) without localizing time
    zone offset.
*)
class type unqualified = object
    method date: Cf_gregorian.date
    method time: Cf_clockface.time
end

(** The class type inhabited by object types representing time stamps qualified
    with an integral offset from local time to UTC in minutes.
*)
class type epoch = object
    inherit unqualified
    method offset: int
end

(** The private type inhabited by time stamps in UTC, i.e. with an explicit
    offset of +00:00.
*)
type utc = private UTC of unqualified [@@unboxed]

(** The private type inhabited by time stamps qualified with a local timezone
    offset.
*)
type local = private Local of epoch [@@unboxed]

(** {6 Functions} *)

(** Use [is_valid_utc ~date ~time] to check whether [date] and [time] comprise
    a valid point in time on the UTC time scale.

    {b Note:} The currently loaded leap second archive is consulted to validate
    the combination of [date] and [time]. The time value [23:59:60] is valid
    only when the archive contains a positive leap second at the adjust date,
    and the value [23:59:59] is not valid if the archive contains a negative
    leap second at[date].
*)
val is_valid_utc: date:Cf_gregorian.date -> time:Cf_clockface.time -> bool

(** Use [create ~date ~time ~offset] to create a time stamp comprising [~date],
    [~time] and ~[offset]. Raises [Invalid_argument] if [date] and [time],
    adjusted to UTC by [offset], do not comprise a valid point in time on the
    UTC time scale.
*)
val create:
    date:Cf_gregorian.date -> time:Cf_clockface.time -> offset:int -> local

(** Use [localize t] to convert the UTC time stamp [t] to a local time stamp.
    If [~offset] is used, then the civil time and calendar date are adjusted
    accordingly. Raises [Invalid_argument] if the adjustment to the calendar
    date would overflow the 64-bit integer representation of the astronomical
    year.
*)
val localize: ?offset:int -> utc -> local

(** Use [local_to_utc t] to convert [t] to the UTC. Raises [Invalid_argument]
    in the untypical case that time zone adjustment results in an overflow in
    the 64-bit integer arithetic for the Gregorian calendar year.
*)
val local_to_utc: local -> utc

(** {4 Conversions to RFC 3394 Internet Timestamp} *)

(** Use [local_to_inet t] to make a string representation of [t] in the
    Internet Timestamp format, c.f {{:https://tools.ietf.org/html/rfc3339:}
    RFC 3339}.
*)
val local_to_inet: local -> string

(** Use [inet_to_local s] to make a timestamp with the value extracted from [s]
    according to the Internet Timestamp format.
*)
val inet_to_local: string -> local

(** {4 Conversions to and from TAI64} *)

(** Use [of_tai64 t] to create the UTC time stamp corresponding to [t]. *)
val of_tai64: Cf_tai64.t -> utc

(** Using [in_tai64_range t] returns [true] if [t] is within the range of valid
    TAI64 epochs (see {!Cf_tai64}).
*)
val in_tai64_range: utc -> bool

(** Use [to_tai64 t] to create the TAI64 value corresponding to [t]. Raises
    [Invalid_argument] if [t] is outside the range of valid TAI64 values.
*)
val to_tai64: utc -> Cf_tai64.t

(**/**)
val to_tai64_unsafe: utc -> Cf_tai64.t

(*--- End ---*)
