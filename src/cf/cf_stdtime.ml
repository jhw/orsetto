(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2022, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open Cf_gregorian
open Cf_clockface

let invalid msg = invalid_arg (__MODULE__ ^ msg)

class unqualified ~date ~time = object
    method date: date = date
    method time: time = time
end

class epoch ~date ~time ~offset = object
    inherit unqualified ~date ~time
    method offset: int = offset
end

type utc = UTC of unqualified [@@unboxed]
type local = Local of epoch [@@unboxed]

let localize ?(offset = 0) (UTC t) =
    let date, time =
        if offset = 0 then
            t#date, t#time
        else begin
            let Date ymd = t#date and Time hms = t#time in
            let MJD mjd = to_mjd_unsafe ymd.year ymd.month ymd.day in
            let w = Int64.of_int (3600 * hms.hour + 60 * hms.minute) in
            let w = Int64.(add w (mul 86400L mjd)) in
            let off64 = Int64.(mul 60L (of_int offset)) in
            if
                (offset > 0 && w > Int64.sub Int64.max_int off64) ||
                (offset < 0 && w < Int64.add Int64.min_int (Int64.neg off64))
            then
                invalid ".to_utc: would overflow.";
            let w = Int64.(add w off64) in
            let date = of_mjd (MJD (Int64.(div w 86400L))) in
            let w = Int64.(rem w 86400L |> to_int) / 60 in
            let time = unsafe_create (w / 60) (w mod 60) hms.second in
            date, time
        end
    in
    Local (new epoch ~date ~time ~offset)

let local_to_utc (Local t) =
    let offset = t#offset in
    let t =
        if offset = 0 then
            t
        else begin
            let utc = UTC (t :> unqualified) in
            let Local t = localize ~offset utc in
            t
        end
    in
    UTC (t :> unqualified)

let leapsec_add_ =
    let rec loop mark hit offset = function
        | Cf_leap_second.Entry hd :: tl ->
            let mark' = Cf_tai64.add mark (hd.offset - 10) in
            let cmp = Cf_tai64.compare hd.epoch mark' in
            if cmp < 0 || (hit && cmp = 0) then
                (loop[@tailcall]) mark hit hd.offset tl
            else
                offset
        | [] ->
            offset
    in
    fun mark hit ->
        let Cf_leap_second.Archive a = Cf_leap_second.current () in
        let offset = loop mark hit a.current a.history in
        Cf_tai64.add mark (offset - 10)

let to_tai64_aux_ (Date ymd) (Time hms as time) =
    let s64 = Int64.of_int @@ to_seconds time in
    let MJD mjd = to_mjd_unsafe ymd.year ymd.month ymd.day in
    let s64 = Int64.(add s64 (mul mjd 86400L)) in
    let tai = Cf_tai64.(add_int64 mjd_epoch s64) in
    let hit = (hms.second == 60) in
    leapsec_add_ tai hit

let of_tai64 t =
    let leap, offset = Cf_leap_second.search t in
    let tadj = Cf_tai64.(sub t first) in
    let tadj = Int64.(add tadj (of_int (10 - offset))) in
    let mjd =
         (* if tadj <= 58486 before last TAI64 epoch *)
        if tadj <= 9223372036854717321L then
            Int64.(sub (div (add tadj 58486L) 86400L) 53375995543064L)
        else
            53375995624237L (* MJD on the day of the last TAI64 epoch *)
    in
    let date = MJD mjd |> of_mjd in
    let r = Int64.(rem (sub tadj 27914L) 86400L |> to_int) in
    let r = if r < 0 then r + 86400 else r in
    let second = if leap then 60 else r mod 60 and r = r / 60 in
    let minute = r mod 60 in
    let hour = r / 60 in
    let time = unsafe_create hour minute second in
    UTC (new unqualified ~date ~time)

let UTC min_tai64 = of_tai64 Cf_tai64.first
let UTC max_tai64 = of_tai64 Cf_tai64.last

let in_tai64_range_aux_ (Date d) (Time t) =
    let Date da = min_tai64#date and Date db = max_tai64#date in
    (d.year > da.year && d.year < db.year) ||
    (d.year = da.year && d.month > da.month) ||
    (d.year = db.year && d.month < db.month) ||
    (d.month = da.month && d.day > da.day) ||
    (d.month = db.month && d.day < db.day) || begin
        let Time ta = min_tai64#time and Time tb = max_tai64#time in
        (d.day = da.day && t.hour > ta.hour) ||
        (d.day = db.day && t.hour < tb.hour) ||
        (t.hour = ta.hour && t.minute > ta.minute) ||
        (t.hour = tb.hour && t.minute < tb.minute) ||
        (t.minute = ta.minute && t.second >= ta.second) ||
        (t.minute = tb.minute && t.second <= tb.second)
    end

let is_valid_utc ~date ~time =
    let Cf_clockface.Time hms = time in
    hms.hour < 23 ||
    hms.minute < 59 ||
    hms.second < 59 ||
    not (in_tai64_range_aux_ date time) || begin
        let leap, _ = Cf_leap_second.search @@ to_tai64_aux_ date time in
        match hms.second, leap with
        | 60, false
        | 59, true ->
            false
        | _ ->
            true
    end

let create ~date ~time ~offset =
    let Cf_clockface.Time hms = time in
    if hms.second > 58 then begin
        let UTC utc =
            local_to_utc @@
            localize ~offset @@
            UTC (new unqualified ~date ~time)
        in
        if not (is_valid_utc ~date:utc#date ~time:utc#time) then
            invalid ".create: invalid leap second!"
    end;
    Local (new epoch ~date ~time ~offset)

let local_to_inet (Local t) =
    let Date ymd = t#date and Time hms = t#time and tz = t#offset in
    let dir, tz = if tz < 0 then '-', (-tz) else '+', tz in
    Printf.sprintf "%04Ld-%02u-%02uT%02u:%02u:%02u%c%02u:%02u"
        ymd.year ymd.month ymd.day
        hms.hour hms.minute hms.second
        dir (tz / 60) (tz mod 60)

module Scan = struct
    include Cf_scan.ASCII
    include Cf_number_scan.ASCII
    open Cf_number_scan

    let int = ctrl ~opt:[ O_leading ] R_int |> special
    let int64 = ctrl ~opt:[ O_leading ] R_int64 |> special
end

let inet_to_local =
    let open Scan.Affix in
    let tok_dir c =
        match c with
        | '+' -> Some 1
        | '-' -> Some (-1)
        | _ -> None
    in
    let s_date =
        let* year = Scan.int64 in
        let* _ = Scan.one '-' in
        let* month = Scan.int in
        let* _ = Scan.one '-' in
        let* day = Scan.int in
        Scan.return @@ Cf_gregorian.create ~year ~month ~day
    and s_time =
        let* hour = Scan.int in
        let* _ = Scan.one ':' in
        let* minute = Scan.int in
        let* _ = Scan.one ':' in
        let* second = Scan.int in
        Scan.return @@ Cf_clockface.create ~hour ~minute ~second
    and s_offset =
        let* d = Scan.tok tok_dir in
        let* hour = Scan.int in
        let* _ = Scan.one ':' in
        let* minute = Scan.int in
        let Time t = Cf_clockface.create ~hour ~minute ~second:0 in
        Scan.return @@ d * (t.hour * 60 + t.minute)
    in
    let s_stamp =
        let* date = s_date in
        let* _ = Scan.one 'T' in
        let* time = s_time in
        let* offset = s_offset in
        Scan.return @@ create ~date ~time ~offset
    in
    let enter s =
        match Scan.of_string s_stamp s with
        | exception Not_found ->
            invalid ".inet_to_local: syntax error."
        | t ->
            t
    in
    enter

let in_tai64_range (UTC utc) = in_tai64_range_aux_ utc#date utc#time

let to_tai64 (UTC utc) =
    let date = utc#date and time = utc#time in
    if not (in_tai64_range_aux_ date time) then
        invalid ".to_tai64: out of TAI64 range";
    to_tai64_aux_ date time

let to_tai64_unsafe (UTC t) = to_tai64_aux_ t#date t#time

(*--- End ---*)
