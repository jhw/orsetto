(*---------------------------------------------------------------------------*
  Copyright (C) 2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

let invalid fn detail =
    invalid_arg @@ Printf.sprintf "%s.%s: %s" __MODULE__ fn detail

let failure fn detail =
    failwith @@ Printf.sprintf "%s.%s: %s" __MODULE__ fn detail

type 'k pair = [
    | `Table of 'k Cf_type.nym
    | `Structure of 'k Cf_type.nym
    | `Variant of 'k Cf_type.nym
]

type 'k container = [ `Vector | `Record | 'k pair ]

type (_, _, _, _) entry =
    | Required: ('k, 'p, 'e, 'e) entry
    | Optional: ('k, 'p, 'e, 'e option) entry

type element

type (_, _) control = ..

type (_, _) control += Cast: ('b -> 'a) -> ('a, 'b) control
type (_, _) control += Default: 'a -> ('a, 'a option) control

type 'k sort =
    | Unsorted
    | Sorted of { f: 'v. ('k * 'v) Seq.t -> ('k * 'v) Seq.t }

type _ model =
    | Primitive: 'v Cf_type.nym -> 'v model
    | Control: 'a model * ('a, 'b) control -> 'b model
    | Defer: 'v model Lazy.t -> 'v model
    | Choice: ('v -> int) * 'v model array -> 'v model
    | Vector: 'v model -> 'v array model
    | Table: 'k sort * 'k indexed * 'v model -> ('k * 'v) array model
    | Record: (element, 'v) bind array -> 'v model
    | Structure: 'k sort * 'k indexed * ('k indexed, 'v) bind array -> 'v model
    | Variant:
        'k indexed * ('v -> 'k) * ('k * (element, 'v) bind) array -> 'v model

and (_, _) bind =
    | Sequential:
        (element, 'v, 'e, 'r) entry * 'e model * ('v -> 'r) ->
        (element, 'v) bind
    | Indexed:
        ('k indexed, 'v, 'e, 'r) entry * 'k * 'e model * ('v -> 'r) ->
        ('k indexed, 'v) bind

and 'k indexed = <
    'k Cf_relations.Extensible.order;
    nym: 'k Cf_type.nym;
    model: 'k model;
>

let mksort arg compare =
    match arg with
    | Some () ->
        Unsorted
    | None ->
        let f s =
            let w = Array.of_seq s in
            Array.sort (fun (a, _) (b, _) -> compare a b) w;
            Array.to_seq w
        in
        Sorted { f }

let primitive nym = Primitive nym
let control m c = Control (m, c)
let defer m = Defer m
let default v m = control m @@ Default v
let cast f m = control m @@ Cast f

let choice index choices =
    let size = Array.length choices in
    if size = 0 then failwith @@ (__MODULE__ ^ ": empty choices array!");
    Choice (index, choices)

class ['key] index ?cmp ?model nym =
    let model = match model with None -> Primitive nym | Some m -> m in
    object(_:'self)
        inherit ['key] Cf_relations.Extensible.order as order
        constraint 'self = < 'key indexed; ..>

        val nym_: 'key Cf_type.nym = nym
        val model_ = model

        method nym = nym_
        method model = model_

        method! compare a b =
            match cmp with
            | None -> order#compare a b
            | Some f -> f a b
    end

module Element = struct
    let required model project = Sequential (Required, model, project)
    let optional model project = Sequential (Optional, model, project)
    let constant model field = required model (fun _ -> field)
end

module Field = struct
    let required index model project =
        Indexed (Required, index, model, project)

    let optional index model project =
        Indexed (Optional, index, model, project)

    let constant index model field =
        required index model (fun _ -> field)
end

let vector model = Vector model

let table ?unsorted domain codomain =
    Table (mksort unsorted domain#compare, (domain :> 'k index), codomain)

let record binds = Record binds

let structure ?unsorted domain binds =
    Structure (mksort unsorted domain#compare, (domain :> 'k index), binds)

let variant domain select cases =
    let size = Array.length cases in
    if size = 0 then failwith @@ (__MODULE__ ^ ": empty cases array!");
    Variant ((domain :> 'k index), select, cases)

module Affix = struct
    let ( $: ) = Element.required
    let ( $? ) = Element.optional
    let ( $= ) = Element.constant

    let ( %: ) (domain, model) project = Field.required domain model project
    let ( %? ) (domain, model) project = Field.optional domain model project
    let ( %= ) (domain, model) field = Field.constant domain model field

    let ( /= ) m f = cast f m
end

module type Basis = sig
    type -'v scheme

    val primitive: 'v Cf_type.nym -> 'v scheme
    val control: 'a scheme -> ('a, 'b) control -> 'b scheme

    type packet

    val delegate: ('v -> packet) -> 'v scheme
    val packet: 'v scheme -> 'v -> packet
    val pair: 'k pair -> packet -> packet -> packet
    val sequence: 'k container -> packet Seq.t -> packet
end

module type Profile = sig
    type -'v scheme

    val scheme: 'v model -> 'v scheme
end

module Create(B: Basis) = struct
    type 'v scheme = 'v B.scheme

    type 'v projector = Projector of ('v -> B.packet option) [@@caml.unboxed]

    let rec of_model: type v. v model -> v scheme = fun model ->
        match model with
        | Primitive nym ->
            B.primitive nym
        | Control (model, control) ->
            B.control (of_model model) control
        | Defer model ->
            of_model @@ Lazy.force model
        | Choice (index, choices) ->
            of_choice index choices
        | Vector model ->
            let packet = lazy (B.packet @@ of_model model) in
            B.delegate begin fun w ->
                B.sequence `Vector @@ begin
                    if Array.length w > 0 then
                        Seq.map (Lazy.force packet) @@ Array.to_seq w
                    else
                        Seq.empty
                end
            end
        | Table (sort, domain, codomain) ->
            let table = `Table domain#nym in
            let k_packet = B.packet @@ of_model domain#model in
            let v_packet = lazy (B.packet @@ of_model codomain) in
            B.delegate begin fun w ->
                B.sequence table @@ begin
                    if Array.length w > 0 then begin
                        let v_packet = Lazy.force v_packet in
                        let each (k, v) =
                            B.pair table (k_packet k) (v_packet v)
                        in
                        let s = Array.to_seq w in
                        Seq.map each begin
                            match sort with
                            | Unsorted -> s
                            | Sorted sort -> sort.f s
                        end
                    end
                    else
                        Seq.empty
                end
            end
        | Record binds ->
            of_record binds (Array.length binds - 1) []
        | Structure (sort, domain, binds) ->
            let tag = `Structure domain#nym in
            let scheme = of_model domain#model in
            let geti = Array.length binds - 1 in
            of_structure tag scheme sort domain binds geti []
        | Variant (index, select, cases) ->
            of_variant index select cases

    and of_choice:
        type v. (v -> int) -> v model array -> v scheme
        = fun index choices ->
            let schemes = Array.map (fun m -> lazy (of_model m)) choices in
            B.delegate begin fun v ->
                let i = index v in
                if i < 0 || i >= Array.length schemes then
                    failwith @@ (__MODULE__ ^ ": undefined choice index!");
                let scheme = Lazy.force (Array.unsafe_get schemes i) in
                B.packet scheme v
            end

    and of_record:
        type v. (element, v) bind array -> int -> v projector list -> v scheme
        = fun binds index projectors ->
            if index < 0 then begin
                let projectors = Array.of_list projectors in
                B.delegate begin fun v ->
                    let apply (Projector f) = f v in
                    B.sequence `Record @@ Seq.filter_map apply @@
                        Array.to_seq projectors
                end
            end
            else begin
                let bind = Array.unsafe_get binds index in
                let Sequential (entry, model, projector) = bind in
                let scheme = lazy (of_model model) in
                let projector =
                    match entry with
                    | Required ->
                        Projector begin fun v ->
                            Some (B.packet (Lazy.force scheme) @@ projector v)
                        end
                    | Optional ->
                        Projector begin fun v ->
                            match projector v with
                            | Some v -> Some (B.packet (Lazy.force scheme) v)
                            | None -> None
                        end
                in
                of_record binds (pred index) (projector :: projectors)
            end

    and of_structure:
        type i v. i pair -> i scheme -> i sort -> i index ->
            (i index, v) bind array -> int -> (i * v projector) list ->
            v scheme
        = fun tag i_scheme sort domain binds geti fields ->
            if geti < 0 then begin
                let fields = List.to_seq fields in
                let fields =
                    Array.of_seq @@ Seq.map snd begin
                        match sort with
                        | Unsorted -> fields
                        | Sorted sort -> sort.f fields
                    end
                in
                let tag = `Structure domain#nym in
                B.delegate begin fun v ->
                    let apply (Projector f) = f v in
                    B.sequence tag @@ Seq.filter_map apply @@
                        Array.to_seq fields
                end
            end
            else begin
                let bind = Array.unsafe_get binds geti in
                let Indexed (entry, index, model, projector) = bind in
                let e_scheme = lazy (of_model model) in
                let i_packet = B.packet i_scheme index in
                let projector =
                    match entry with
                    | Required ->
                        Projector begin fun v ->
                            let e_scheme = Lazy.force e_scheme in
                            let e_packet = B.packet e_scheme @@ projector v in
                            Some (B.pair tag i_packet e_packet)
                        end
                    | Optional ->
                        Projector begin fun v ->
                            match projector v with
                            | Some e ->
                                Some begin
                                    let e_scheme = Lazy.force e_scheme in
                                    let e_packet = B.packet e_scheme e in
                                    B.pair tag i_packet e_packet
                                end
                            | None ->
                                None
                        end
                in
                let geti = pred geti in
                let fields = (index, projector) :: fields in
                of_structure tag i_scheme sort domain binds geti fields
            end

    and of_variant:
        type k v. k index -> (v -> k) -> (k * (element, v) bind) array ->
            v scheme
        = fun domain select cases ->
            let k_scheme = of_model @@ domain#model in
            let module Order =
                (val Cf_relations.Extensible.ordering domain :
                Cf_relations.Order with type t = k)
            in
            let module Map = Cf_bsearch_data.Map.Create(Order) in
            let f (key, bind) =
                let Sequential (entry, model, projector) = bind in
                match entry with
                | Optional ->
                    invalid "scheme" "variant bind cannot be optional"
                | Required ->
                    let scheme = lazy (of_model model) in
                    let projector v =
                        let scheme = Lazy.force scheme and v = projector v in
                        B.packet scheme v
                    in
                    let packet = B.packet k_scheme key in
                    let field = packet, projector in
                    key, field
            in
            let cases = Map.of_seq @@ Seq.map f @@ Array.to_seq cases in
            B.delegate begin fun v ->
                match Map.search (select v) cases with
                | None ->
                    failure "scheme" "selected variant not recognized"
                | Some (k_packet, projector) ->
                    let v_packet = projector v in
                    let nym = domain#nym in
                    B.sequence (`Variant nym) @@ List.to_seq [
                        B.pair (`Variant nym) k_packet v_packet
                    ]
            end

    let scheme = of_model
end

(*--- End ---*)
