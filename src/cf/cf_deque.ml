(*---------------------------------------------------------------------------*
  Copyright (C) 2003-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(* A brief note about the algorithm in this module.

    This algorithm is an original invention, but it owes a few of its key
    insights to various algorithms published by Kaplan, Okasaki and Tarjan.

    The inventor makes the baseless claim that it is substantially more
    efficient and simpler than other algorithms with similar complexity. The
    claim is baseless because benchmarks and evaluation criteria have not been
    made to compare this algorithm formally with published algorithms. However,
    informal testing by the inventor strongly suggests that the claim merits
    further investigation.

    The inventor invites other scientists to collaborate in the development of
    a scholarly paper about this algorithm and its performance characteristics.

                                            --j h woodyatt <jhw@conjury.org>
*)

type 'a t =
    | Z
    (* An empty deque *)

    | Y of 'a
    (* A deque containing one element. *)

    | X of 'a * 'a
    (*
        An internal node of a deque that contains two elements.  Nodes of this
        variant constructor are only allowed to be the left or right subnode of
        a U node (see below) that is itself a middle subnode of a U node that
        does not have an X subnode in the corresponding position.
    *)

    | U of 'a t * ('a * 'a) t * 'a t
    (*
        A deque, or an internal node of a deque that contains three subnodes:
        1) a left node of single elements; 2) a middle node of element pairs;
        and 3) a right node of single elements.   The middle subnode is never
        a Z, X, U or W node.  The other subnodes are never U or W nodes.
    *)

    | W of 'a t * 'a t t Lazy.t * 'a t
    (*
        A deque composed by catenation.  It contains three subnodes: 1) a left
        deque; 2) a lazy deque of deques; and 3) a right deque.  The left and
        right subnodes are never Z or W nodes.
    *)

(*
    An invariant rule is applied to the tree structure in order to obtain the
    recursive slowdown effect necessary to achieve O(1) asymptotic complexity
    in all the operations.  The rule amounts to a requirement on the left and
    right subnodes in a stack of U nodes that may be mapped as digits in a
    redundant binary representation, i.e. Z, Y, and X nodes correspond to 0,
    1 and 2 digits respectively.

    In redundant binary representation, a number is represented with the 0, 1
    and 2 digits, with the rule demanding that when scanning the digits from
    most significant to least significant we always find at least one 0 digit
    after every 2 digit.

    The key insight to apply this invariant to the tree structure of a
    persistent functional deque comes from Kaplan and Tarjan [insert citation
    here].
*)

let nil = Z
let one x = Y x
let empty = function Z -> true | Y _ | X _ | U _ | W _ -> false

module type Push = sig
    val push: 'a -> 'a t -> 'a t
end

module rec A_push: Push = struct
    let rec push x = function
        | Y a ->
            U (Z, Y (x, a), Z)
        | U (Z, a, b) ->
            U (Y x, a, b)
        | U (Y a, b, y0) ->
            begin
                match b with
                | Y (b,c) ->
                    begin
                        match y0 with
                        | Z -> U (Y x, Y (a, b), Y c)
                        | Y d -> U (Y x, U (Z, Y ((a, b), (c, d)), Z), Z)
                        | (X _ | U _ | W _) -> assert (not true); Z
                    end
                | U (Y (b, c), Y ((d, e), (f, g)), Y (h, i)) when y0 = Z ->
                    U (Y x, U (Y (a, b), Y ((c, d), (e, f)), Y (g, h)), Y i)
                | U (b, c, d) ->
                    let y1 =
                        match b with
                        | X (b1, b2) ->
                            U (Y (x, a), A_push.push (b1, b2) c, d)
                        | Y b ->
                            begin
                                match c, d with
                                | Y (c, d), Z ->
                                    U (Y (x, a), Y (b, c), Y d)
                                | (Z | Y _ | X _ | U _ | W _), _ ->
                                    U (X ((x, a), b), c, d)
                            end
                        | Z ->
                            U (Y (x, a), c, d)
                        | (U _ | W _) ->
                            assert (not true);
                            Z
                    in
                    U (Z, y1, y0)
                | (Z | X _ | W _) ->
                    assert (not true);
                    Z
            end
        | W (a, b, c) ->
            W (push x a, b, c)
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            Y x
end

module rec B_push: Push = struct
    let rec push x = function
        | Y a ->
            U (Z, Y (a, x), Z)
        | U (a, b, Z) ->
            U (a, b, Y x)
        | U (y0, b, Y a) ->
            begin
                match b with
                | Y (c, b) ->
                    begin
                        match y0 with
                        | Z -> U (Y c, Y (b, a), Y x)
                        | Y d -> U (Z, U (Z, Y ((d, c), (b, a)), Z), Y x)
                        | (X _ | U _ | W _) -> assert (not true); Z
                    end
                | U (Y (i, h), Y ((g, f), (e, d)), Y (c, b)) when y0 = Z ->
                    U (Y i, U (Y (h, g), Y ((f, e), (d, c)), Y (b, a)), Y x)
                | U (d, c, b) ->
                    let y1 =
                        match b with
                        | X (b1, b2) ->
                            U (d, B_push.push (b1, b2) c, Y (a, x))
                        | Y b ->
                            begin
                                match d, c with
                                | Z, Y (d, c) ->
                                    U (Y d, Y (c, b), Y (a, x))
                                | (Z | Y _ | X _ | U _ | W _), _ ->
                                    U (d, c, X (b, (a, x)))
                            end
                        | Z ->
                            U (d, c, Y (a, x))
                        | (U _ | W _) ->
                            assert (not true);
                            Z
                    in
                    U (y0, y1, Z)
                | (Z | X _ | W _) ->
                    assert (not true);
                    Z
            end
        | W (a, b, c) ->
            W (a, b, push x c)
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            Y x
end

module type Pop = sig
    val pop: 'a t -> ('a * 'a t) option
end

module rec A_pop: Pop = struct
    let rec pop = function
        | Y x ->
            Some (x, Z)
        | U (Y x, a, b) ->
            let y =
                match a, b with
                | Y (a, b), X (c, d) -> U (Y a, Y (b, c), Y d)
                | (Z | Y _ | X _ | U _ | W _), _ -> U (Z, a, b)
            in
            Some (x, y)
        | U (Z, a, b) ->
            begin
                match a, b with
                | Y (x, a), b ->
                    let y =
                        match b with
                        | X (b, c) -> U (Y a, Y (b, c), Z)
                        | Y b -> U (Z, Y (a, b), Z)
                        | Z -> Y a
                        | (U _ | W _) -> assert (not true); Z
                    in
                    Some (x, y)
                | U (a, y2, y1), y0 ->
                    begin
                        match a with
                        | X ((x, a), b) ->
                            Some (x, U (Y a, U (Y b, y2, y1), y0))
                        | Y (x, a) ->
                            let y =
                                match y2, y1 with
                                | Y (b,c), X (d,e) -> U (Y b, Y (c,d), Y e)
                                | Y _, _ -> U (Z, y2, y1)
                                | U _, _ ->
                                    let (hd1,hd2), tl =
                                        match A_pop.pop y2 with
                                        | Some (hd, tl) -> hd, tl
                                        | None -> assert false
                                    in
                                    U (X (hd1,hd2), tl, y1)
                                | (Z | X _ | W _), _ ->
                                    assert (not true);
                                    Z
                            in
                            Some (x, U (Y a, y, y0))
                        | Z ->
                            let x, a, b =
                                match y2 with
                                | Y ((x, a), b) -> x, a, b
                                | (Z | X _ | U _ | W _) -> assert false
                            in
                            let y =
                                match y1, y0 with
                                | X (c, d), Y _ -> U (Z, Y (b, c), Y d)
                                | Y c, _ -> U (Z, Y (b, c), Z)
                                | Z, _ -> Y b
                                | (X _ | U _ | W _), _ -> assert (not true); Z
                            in
                            Some (x, U (Y a, y, y0))
                        | (U _ | W _) ->
                            assert (not true);
                            None
                    end
                | (Z | X _ | W _), _ ->
                    assert (not true);
                    None
            end
        | W (a, b, c) ->
            begin
                match pop a with
                | None ->
                    assert (not true);
                    None
                | Some (x, Z) ->
                    let b = Lazy.force b in
                    let y =
                        match A_pop.pop b with
                        | Some (y, b) -> W (y, Lazy.from_val b, c)
                        | None -> c
                    in
                    Some (x, y)
                | Some (x, (Y _ | X _ | U _ | W _ as a)) ->
                    Some (x, W (a, b, c))
            end
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            None
end

module rec B_pop: Pop = struct
    let rec pop = function
        | Y x ->
            Some (x, Z)
        | U (b, a, Y x) ->
            let y =
                match b, a with
                | X (d, c), Y (b, a) -> U (Y d, Y (c, b), Y a)
                | (Z | Y _ | X _ | U _ | W _), _ -> U (b, a, Z)
            in
            Some (x, y)
        | U (b, a, Z) ->
            begin
                match b, a with
                | b, Y (a, x) ->
                    let y =
                        match b with
                        | X (c, b) -> U (Z, Y (c, b), Y a)
                        | Y b -> U (Z, Y (b, a), Z)
                        | Z -> Y a
                        | (U _ | W _) -> assert (not true); Z
                    in
                    Some (x, y)
                | y0, U (y1, y2, a) ->
                    begin
                        match a with
                        | X (b, (a, x)) ->
                            Some (x, U (y0, U (y1, y2, Y b), Y a))
                        | Y (a, x) ->
                            let y =
                                match y1, y2 with
                                | X (e, d), Y (c, b) -> U (Y e, Y (d, c), Y b)
                                | _, Y _ -> U (y1, y2, Z)
                                | _, U _ ->
                                    let (hd1, hd2), tl =
                                        match B_pop.pop y2 with
                                        | Some (hd, tl) -> hd, tl
                                        | None -> assert false
                                    in
                                    U (y1, tl, X (hd1, hd2))
                                | _, (Z | X _ | W _) ->
                                    assert (not true);
                                    Z
                            in
                            Some (x, U (y0, y, Y a))
                        | Z ->
                            let b, a, x =
                                match y2 with
                                | Y (b, (a, x)) -> b, a, x
                                | (Z | X _ | U _ | W _) -> assert false
                            in
                            let y =
                                match y0, y1 with
                                | Y _, X (d, c) -> U (Y d, Y (c, b), Z)
                                | _, Y c -> U (Z, Y (c, b), Z)
                                | _, Z -> Y b
                                | _, (X _ | U _ | W _) -> assert (not true); Z
                            in
                            Some (x, U (y0, y, Y a))
                        | (U _ | W _) ->
                            assert (not true);
                            None
                    end
                | _, (Z | X _ | W _) ->
                    assert (not true);
                    None
            end
        | W (c, b, a) ->
            begin
                match pop a with
                | None ->
                    assert (not true);
                    None
                | Some (x, Z) ->
                    let y =
                        let b = Lazy.force b in
                        match B_pop.pop b with
                        | Some (y, b) -> W (c, Lazy.from_val b, y)
                        | None -> c
                    in
                    Some (x, y)
                | Some (x, (Y _ | X _ | U _ | W _ as a)) ->
                    Some (x, W (c, b, a))
            end
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            None
end

module type Head = sig
    val head: 'a t -> 'a
end

module A_head: Head = struct
    let rec head = function
        | Y x
        | U (Y x, _, _)
        | U (Z, Y (x, _), _)
        | U (Z, U (Y (x, _), _, _), _)
        | U (Z, U (X ((x, _), _), _, _), _)
        | U (Z, U (Z, Y ((x, _), _), _), _) ->
            x
        | W (a, _, _) ->
            (head[@tailcall]) a
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            raise Not_found
end

module B_head: Head = struct
    let rec head = function
        | Y x
        | U (_, _, Y x)
        | U (_, Y (_, x), Z)
        | U (_, U (_, _, Y (_, x)), Z)
        | U (_, U (_, _, X (_, (_, x))), Z)
        | U (_, U (_, Y (_, (_, x)), Z), Z) ->
            x
        | W (_, _, a) ->
            (head[@tailcall]) a
        | (Z | X _ | U _ as y) ->
            assert (y = Z);
            raise Not_found
end

module type Direction = sig
    include Push
    include Pop
    include Head

    val tail: 'a t -> 'a t
    val of_seq: 'a Seq.t -> 'a t
    val to_seq: 'a t -> 'a Seq.t
end

module MkDirection(M1: Push)(M2: Push)(M3: Pop)(M4: Head) : Direction = struct
    include M1
    include M3
    include M4

    let tail q = match pop q with Some (_, q) -> q | None -> Z

    let of_seq =
        let rec loop q s =
            match s () with
            | Seq.Nil -> q
            | Seq.Cons (hd, tl) -> loop (M2.push hd q) tl
        in
        fun s -> loop Z s

    let rec to_seq q () =
        match M3.pop q with
        | None -> Seq.Nil
        | Some (v, q) -> Seq.Cons (v, to_seq q)
end

module A = MkDirection(A_push)(B_push)(A_pop)(A_head)
module B = MkDirection(B_push)(A_push)(B_pop)(B_head)

module type Catenate = sig
    val catenate: 'a t -> 'a t -> 'a t
end

module rec C: Catenate = struct
    let catenate q1 q2 =
        match q1, q2 with
        | (Z, q | q, Z) -> q
        | Y a, x -> A.push a x
        | x, Y b -> B.push b x
        | (U _ as a), (U _ as b) ->
            W (a, Lazy.from_val Z, b)
        | (U _ as a), W (b, c, d) ->
            W (a, Lazy.from_val (A.push b (Lazy.force c)), d)
        | W (a, b, c), (U _ as d) ->
            W (a, Lazy.from_val (B.push c (Lazy.force b)), d)
        | W (a, b, c), W (d, e, f) ->
            let q1 = B.push c (Lazy.force b) and q2 = A.push d (Lazy.force e) in
            W (a, lazy (C.catenate q1 q2), f)
        | (X _, _ | _, X _) ->
            assert (not true); Z
end

include C

(*--- End ---*)
