(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module Vector = struct
    module type Basis = sig
        include Cf_bsearch.Basis

        val expand: t -> t
        val adjust: lim:t -> rev:t -> t -> t
    end

    module Int_basis = struct
        include Cf_bsearch.Int_basis

        let adjust ~lim ~rev i =
            if compare i lim < 0 then i else i - (lim - rev)

        let expand i = i * 2
    end

    module type Profile = sig
        type index
        type element

        module Basis: Basis with type t := index
        module Element: Cf_relations.Order with type t = element

        type t

        val nil: t
        val empty: t -> bool
        val first: index
        val last: t -> index
        val project: t -> index -> element
        val of_seq: element Seq.t -> t
        val to_seq: t -> element Seq.t
    end

    let last_aux len v =
        let n = len v in
        if n < 1 then invalid_arg (__MODULE__ ^ ": empty vector!");
        pred n

    module Create(E: Cf_relations.Order) = struct
        type index = int
        type element = E.t

        module Basis = Int_basis
        module Element = E

        type t = E.t array

        let nil = [| |]
        let empty v = Array.length v = 0
        let first = 0
        let last v = last_aux Array.length v
        let project = Array.get

        let of_seq = Array.of_seq
        let to_seq = Array.to_seq
    end

    module Of_char = struct
        type index = int
        type element = char

        module Basis = Int_basis
        module Element = Char

        type t = string

        let nil = ""
        let empty v = String.length v = 0
        let first = 0
        let last = last_aux String.length
        let project = String.get

        let of_seq = String.of_seq
        let to_seq = String.to_seq
    end

    module Of_int = Create(Stdlib.Int)
    module Of_string = Create(Stdlib.String)
end

module Table = struct
    module type Basis = sig
        module Search: Cf_relations.Order
        module Vector: Vector.Profile

        val xcompare: Search.t -> Vector.element -> int
    end

    module Mk_basis(V: Vector.Profile) = struct
        module Vector = V
        module Search = V.Element

        let xcompare = Search.compare
    end

    module Order_basis(R: Cf_relations.Order) = Mk_basis(Vector.Create(R))

    module Char_basis = Mk_basis(Vector.Of_char)
    module Int_basis = Mk_basis(Vector.Of_int)
    module String_basis = Mk_basis(Vector.Of_string)

    module type Profile = sig
        type search and index and element

        type t

        val nil: t
        val empty: t -> bool
        val of_seq: element Seq.t -> t
        val find: (index, 'r) Cf_bsearch.ret -> search -> t -> 'r
        val member: search -> t -> bool
        val search: search -> t -> index option
        val require: search -> t -> index
        val to_seq: t -> element Seq.t

        module Unsafe: sig
            type vector

            type event = private
                | E_index of index
                | E_adjust of { lim: index; rev: index }

            val import: vector -> index array -> t
            val export: t -> vector * index array
            val events: index -> index -> event Seq.t
        end
    end

    module Create(B: Basis) = struct
        module Vector = B.Vector
        module VB = Vector.Basis

        type t = Tab of {
            v: Vector.t;
            s: Vector.index array;
        }

        let nil = Tab { v = Vector.nil; s = [| |] }
        let empty (Tab t) = Vector.empty t.v

        let find =
            (* NOTE WELL: zero allocations. *)
            let rec loop ret i key tab =
                let Cf_bsearch.Ret r = ret and Tab t = tab in
                if VB.compare i (Vector.last t.v) <= 0 then begin
                    let q = Vector.project t.v i in
                    let d = B.xcompare key q in
                    if d <> 0 then begin
                        let i = VB.expand i in
                        let i = VB.succ i in
                        let i = if d > 0 then VB.succ i else i in
                        (next[@tailcall]) ret key tab i 0
                    end
                    else
                        (r.some[@tailcall]) i
                end
                else
                    r.none ()
            and next ret key tab i j =
                let Tab t = tab in
                if j < Array.length t.s then begin
                    let lim = Array.unsafe_get t.s j in
                    let rev = Array.unsafe_get t.s (j + 1) in
                    let i' = VB.adjust ~lim ~rev i in
                    if i == i' then
                        (loop[@tailcall]) ret i key tab
                    else
                        (next[@tailcall]) ret key tab i' (j + 2)
                end
                else
                    (loop[@tailcall]) ret i key tab
            in
            let enter ret key tab =
                let Tab t = tab and Cf_bsearch.Ret r = ret in
                if not (Vector.empty t.v) then
                    (loop[@tailcall]) ret Vector.first key tab
                else
                    (r.none[@tailcall]) ()
            in
            enter

        let member =
            let none () = false and some _ = true in
            Cf_bsearch.Ret { none; some } |> find

        let search =
            let none () = None and some v = Some v in
            Cf_bsearch.Ret { none; some } |> find

        let require =
            let module M = struct external some: 'a -> 'a = "%identity" end in
            let none () = raise Not_found and some = M.some in
            Cf_bsearch.Ret { none; some } |> find

        let to_seq (Tab t) = Vector.to_seq t.v

        module Unsafe = struct
            module Vector = Vector

            type op = Op_plus1 | Op_plus2 | Op_index of Vector.index

            type event =
                | E_index of Vector.index
                | E_adjust of { lim: Vector.index; rev: Vector.index }

            let mk_vector =
                let module Set = Cf_rbtree.Set.Create(Vector.Element) in
                fun s -> Set.of_seq s |> Set.to_seq_incr |> Vector.of_seq

            let x_table =
                let rec step i n =
                    if n > 0 then
                        (step[@tailcall]) (VB.succ i) (pred n)
                    else
                        i
                in
                let rec final n i oq () =
                    match Cf_deque.B.pop oq with
                    | None ->
                        Seq.Nil
                    | Some (op, oq) ->
                        match op with
                        | Op_plus1 ->
                            (final[@tailcall]) (succ n) i oq ()
                        | Op_plus2 ->
                            (final[@tailcall]) (n + 2) i oq ()
                        | Op_index j ->
                            if n > 0 then begin
                                let lim = step i n and rev = i in
                                let ev = E_adjust { lim; rev } in
                                Seq.Cons (ev, nextfin j i oq)
                            end
                            else
                                (nextfin[@tailcall]) j i oq ()
                and nextfin j i oq () =
                    let i = VB.succ i in
                    Seq.Cons (E_index j, final 0 i oq)
                in
                let rec balanced i oq iq () =
                    match Cf_deque.B.pop iq with
                    | None ->
                        (final[@tailcall]) 0 i oq ()
                    | Some (i2, iq) ->
                        let lo, hi = i2 in
                        let m = VB.center lo hi in
                        let oq, iq =
                            let da = VB.compare lo m in
                            let db = VB.compare m hi in
                            if da < 0 && db < 0 then begin
                                let a = VB.pred m in
                                let b = VB.succ m in
                                if Cf_deque.empty oq then begin
                                    let iq = Cf_deque.A.push (lo, a) iq in
                                    let iq = Cf_deque.A.push (b, hi) iq in
                                    oq, iq
                                end
                                else begin
                                    let oq =
                                        Cf_deque.A.push (Op_index lo) oq
                                    in
                                    let oq =
                                        Cf_deque.A.push (Op_index hi) oq
                                    in
                                    oq, iq
                                end
                            end
                            else begin
                                let oq =
                                    if da < 0 then
                                        Cf_deque.A.push (Op_index lo) oq |>
                                        Cf_deque.A.push Op_plus1
                                    else if db < 0 then
                                        Cf_deque.A.push Op_plus1 oq |>
                                        Cf_deque.A.push (Op_index hi)
                                    else
                                        Cf_deque.A.push Op_plus2 oq
                                in
                                oq, iq
                            end
                        in
                        let i = VB.succ i in
                        Seq.Cons (E_index m, balanced i oq iq)
                in
                let enter lo hi () =
                    let oq = Cf_deque.nil and iq = Cf_deque.one (lo, hi) in
                    (balanced[@tailcall]) Vector.first oq iq ()
                in
                enter

            let of_aux =
                let to_adj = function
                    | E_adjust a ->
                        Cf_seq.concat (Seq.return a.lim) (Seq.return a.rev)
                    | E_index _ ->
                        Seq.empty
                in
                let enter v s =
                    let s = Cf_seq.persist s in
                    let to_elt = function
                        | E_index i -> Some (Vector.project v i)
                        | E_adjust _ -> None
                    in
                    let tv = Vector.of_seq @@ Seq.filter_map to_elt s in
                    let ts = Array.of_seq @@ Seq.flat_map to_adj s in
                    Tab { v = tv; s = ts }
                in
                enter

            let import v s = Tab { v; s }
            let export (Tab t) = t.v, t.s

            let events = x_table
        end

        let of_seq s =
            let v = Unsafe.mk_vector s in
            if Vector.empty v then
                nil
            else
                Unsafe.x_table Vector.first (Vector.last v) |> Unsafe.of_aux v
    end

    module Of_char = Create(Char_basis)
    module Of_int = Create(Int_basis)
    module Of_string = Create(String_basis)
end

module Set = struct
    module type Profile = sig
        type t
        type search and element

        val nil: t
        val empty: t -> bool

        val of_seq: element Seq.t -> t
        val member: search -> t -> bool

        module Unsafe: sig
            type index and vector
            val import: vector -> index array -> t
            val export: t -> vector * index array
        end
    end

    module Of_basis(T: Table.Basis) = struct
        module Table = Table.Create(T)

        type t = Table.t

        let nil = Table.nil
        let empty = Table.empty

        let of_seq = Table.of_seq
        let member = Table.member

        module Unsafe = struct
            let import = Table.Unsafe.import
            let export (Table.Tab t) = t.v, t.s
        end
    end

    module Create(R: Cf_relations.Order) = Of_basis(Table.Order_basis(R))

    module Of_char = Of_basis(Table.Char_basis)
    module Of_int = Create(Stdlib.Int)
    module Of_string = Create(Stdlib.String)
end

module Map = struct
    module Aux = struct
        module type Profile = sig
            type 'a t
            type index

            val nil: 'a t
            val empty: 'a t -> bool
            val of_seq: 'a Seq.t -> 'a t
            val project: 'a t -> index -> 'a
        end

        module Of_array = struct
            type 'a t = 'a array

            let nil = [| |]
            let empty a = Array.length a = 0
            let of_seq s = Cf_seq.reverse s |> List.rev |> Array.of_list
            let project = Array.get
        end
    end

    module type Basis = sig
        module Index: Cf_relations.Order
        module Table: Table.Basis with type Vector.index = Index.t
        module Content: Aux.Profile with type index := Index.t
    end

    module Char_basis = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.Char_basis
        module Content = Aux.Of_array
    end

    module Int_basis = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.Int_basis
        module Content = Aux.Of_array
    end

    module String_basis = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.String_basis
        module Content = Aux.Of_array
    end

    module Order_basis(R: Cf_relations.Order) = struct
        module Index = Cf_bsearch.Int_basis
        module Table = Table.Order_basis(R)
        module Content = Aux.Of_array
    end

    module type Profile = sig
        type 'a t
        type search and key

        val nil: 'a t
        val empty: 'a t -> bool

        val of_seq: (key * 'a) Seq.t -> 'a t

        val member: search -> 'a t -> bool
        val search: search -> 'a t -> 'a option
        val require: search -> 'a t -> 'a

        module Unsafe: sig
            type index and vector and 'a content
            val import: vector -> index array -> 'a content -> 'a t
            val export: 'a t -> vector * index array * 'a content
        end
    end

    module Of_basis(B: Basis) = struct
        module Table = Table.Create(B.Table)
        module Content = B.Content

        type 'a t = Map of {
            keys: Table.t;
            content: 'a Content.t;
        }

        let nil = Map { keys = Table.nil; content = Content.nil }
        let empty (Map m) = Table.empty m.keys

        module RBMap = Cf_rbtree.Map.Create(Table.Vector.Element)

        let of_seq s =
            let s = RBMap.of_seq s |> RBMap.to_seq_incr |> Cf_seq.persist in
            let keys = Seq.map fst s |> Table.of_seq in
            if Table.empty keys then
                nil
            else begin
                let Table.Tab t = keys in
                let lo = Table.Vector.first and hi = Table.Vector.last t.v in
                let v = Seq.map snd s |> Content.of_seq in
                let f = function
                    | Table.Unsafe.E_index i -> Some (Content.project v i)
                    | Table.Unsafe.E_adjust _ -> None
                in
                let content =
                    Table.Unsafe.events lo hi |>
                    Seq.filter_map f |>
                    Content.of_seq
                in
                Map { keys; content }
            end

        let member k (Map m) = Table.member k m.keys

        let search k (Map m) =
            match Table.search k m.keys with
            | None -> None
            | Some i -> Some (Content.project m.content i)

        let require k (Map m) =
            Content.project m.content (Table.require k m.keys)

        module Unsafe = struct
            let import v s content =
                let keys = Table.Unsafe.import v s in
                Map { keys; content }

            let export (Map m) =
                let Table.Tab t = m.keys in
                t.v, t.s, m.content
        end
    end

    module Create(R: Cf_relations.Order) = Of_basis(Order_basis(R))

    module Of_char = Of_basis(Char_basis)
    module Of_int = Create(Stdlib.Int)
    module Of_string = Create(Stdlib.String)
end

(*--- End ---*)
