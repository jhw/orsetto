(*---------------------------------------------------------------------------*
  Copyright (c) 2003-2022, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Computations with the Temps Atomique International (TAI) timescale. *)

(** {6 Overview}

    This module defines an abstract type and associated functions for
    computations with values representing epochs in the Temps Atomique
    International (TAI) timescale.  Values are represented internally with the
    TAI64 format defined by Dan Bernstein, and support precision to the nearest
    second.

    Functions are provided that:
    - acquire the current time in TAI64 format.
    - compare, add and subtract values.
    - convert between TAI64 values and a portable external format called
      the "TAI64 label", which is essentially an array of eight octets.
    - convert between TAI64 values and the float values returned by the
      [Unix.time] function.

    Constants are also provided that define the boundaries of valid TAI64
    representations.

    {b Warning:} This implementation obtains the current time of day using the
    POSIX [time()] function, which returns a value based on the UTC timescale
    (but with leap seconds "elided" in a way that makes conversions between
    POSIX time, Standard Time and TAI a perilous undertaking).  See the
    {!Cf_stdtime} module for details.

    {b Warning!} The internal logic used here for adjusting the difference
    between the UTC time scale and TAI timescale due to leap seconds uses data
    that originates at the International Earth Rotation and Reference
    System Service, which determines whether and when to insert or delete leap
    seconds into the UTC timescale.  These announcements are made every six
    months in an IERS bulletin, and provided as a public service by the
    United States National Institute of Standards and Technology (NIST) and the
    Internet Engineering Task Force (IETF) at the following locations:

        <https://www.ietf.org/timezones/data/leap-seconds.list>
        <ftp://ftp.nist.gov/pub/time/leap-seconds.list>

    The data in the current implementation expires on December 28, 2021.
*)

(** {6 Types} *)

(** Abstract values of TAI64 type *)
type t

(** {6 Exceptions} *)

(** Result not representable in TAI64 format. *)
type exn += private Range_error

(** Input is not a valid TAI64 label *)
type exn += private Label_error

(** {6 Functions} *)

(** Equivalence and total order relations *)
include Cf_relations.Std with type t := t

(** Returns the current time in TAI64, obtained by reading the current time
    from the POSIX [time()] function, and adjusting for leap seconds.
*)
val now: unit -> t

(** The earliest TAI epoch representable in the TAI64 format, corresponding to
    16:14:36 on July 14, 146138509945 BCE (in the continuous TAI timescale,
    which diverges from UTC by not introducing leap seconds). The label is
    [0000000000000000].
*)
val first: t

(** The latest TAI epoch representable in the TAI64 format, corresponding to
    7:43:49 on June 19, 146138514283 CE (in the TAI timescale, which diverges
    from UTC by not introducing leap seconds). The label is [7fffffffffffffff].
*)
val last: t

(** Converts a TAI64 value to a value consistent with the result of calling the
    [Unix.gettimeofday] function. The output is not adjusted for leap seconds
    into UTC from TAI.
*)
val to_unix_time: t -> float

(** Converts a value consistent with the result of calling the [Unix.time]
    function into a TAI64 value. The input is assumed to be adjusted for leap
    seconds into TAI from UTC.
*)
val of_unix_time: float -> t

(** Returns a string of 8 octets containing the TAI64 label corresponding to
    the TAI64 value of its argument.
*)
val to_label: t -> string

(** Interprets the argument as a TAI64 label and returns the corresponding
    TAI64 value.  Raises [Label_error] if the label is invalid.
*)
val of_label: string -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add: t -> int -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add_int32: t -> int32 -> t

(** Add seconds to a TAI64 value.  Raises [Range_error] if the result is not
    a valid TAI64 value.
*)
val add_int64: t -> int64 -> t

(** Subtract one TAI64 value from another.  [sub t0 t1] returns the number of
    seconds before [t0] that [t1] denotes.
*)
val sub: t -> t -> int64

(**/**)

(** The TAI64 value defining the beginning of the TAI64 epoch, i.e. 00:00:10,
    01 Jan 1970 UTC.  The TAI64 label is [4000000000000000].
*)
val epoch: t

(** The TAI epoch marking the start of the Modified Julian Day scale, i.e.
    00:00:00, 17 Nov 1858 TAI.  The TAI64 label is [3fffffff2efbbf8a].
*)
val mjd_epoch: t

(** Use by {!Cf_leap_second} to set the current offset from UTC to TAI. *)
val set_current_offset_from_utc: int -> unit

(*--- End ---*)
