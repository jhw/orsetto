(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Utility functions for byte-order sensitive data. *)

(** {6 Overview}

    Primitive functions for ingesting and rendering integers and floating
    point numbers to and from subranges of strings and byte sequences using
    the "big-endian" and "little-endian" explicit octet ordering disciplines.
*)

(** {6 Module Aliases} *)
module type Profile = Cf_endian_core.Safe

(** Big-endian. *)
module BE: Profile

(** Little-endian. *)
module LE: Profile

(** According to system configuration. *)
module SE: Profile

(** Identifier type *)
type t = [ `BE | `LE | `SE ]

(** Use [create e] to get the endian functions module corresponding to the
    endianness identifier [e].
*)
val create: [< t ] -> (module Profile)

(** {6 Unsafe Interfaces} *)
module Unsafe: sig
    module type Profile = Cf_endian_core.Unsafe

    module BE: Profile
    module LE: Profile
    module SE: Profile

    val create: [< t ] -> (module Profile)
end

(*--- End ---*)
