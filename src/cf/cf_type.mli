(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Extensible runtime type indicators and equality constraints. *)

(** {6 Overview}

    This module provides logic for encapsulating arbitrary OCaml values with a
    runtime type indication, here called a {i type nym}. A function is provided
    to compose a value and its corresponding type nym into a value of
    {i opaque} type. Additional functions are provided to extract a typed
    value from an opaque value when provided with a nym for an equivalent type.

    To handle the case where the {i nym} type is extended, a class is provided
    that tests for equivalence of two nyms. Extending the {i nym} type entails
    defining an subclass with methods that test for equivalence of the new
    variant cases. The functions that extract from values of opaque type are
    declared in a module signature that can be instantiated from an instance of
    the equivalence class.
*)

(** {6 Relational Constraints} *)

(** The type of equality relation constraints. *)
type (_, _) eq = Eq: ('a, 'a) eq

(** The reflexive equality constraint. *)
val refl: ('a, 'a) eq

(** Use [symm eq] to obtain the symmetric constraint of [eq]. *)
val symm: ('a, 'b) eq -> ('b, 'a) eq

(** Use [trans a b] to obtain the transitive constraint from [a] to [b]. *)
val trans: ('a, 'b) eq -> ('b, 'c) eq -> ('a, 'c) eq

(** Use [coerce eq v] to coerce [v] into an equivalent type. *)
val coerce: ('a, 'b) eq -> 'a -> 'b

(** {6 Opaque Type}

    An opaque type is one comprising a value of existential type witnessed by a
    type nym for it.
*)

(** The extensible generalized algebraic data type of type nyms. *)
type _ nym = ..

(** An generalized algebraic type for values witnessed by a type nym. *)
type opaque = private Witness: 'a * 'a nym -> opaque

(** {6 Runtime Type Indicators} *)

(** The runtime type for the unit value. *)
type _ nym += Unit: unit nym

(** The runtime type for boolean values. *)
type _ nym += Bool: bool nym

(** The runtime type for character values. *)
type _ nym += Char: char nym

(** The runtime type for Unicode codepoint values. *)
type _ nym += Uchar: Uchar.t nym

(** The runtime type for string values. *)
type _ nym += String: string nym

(** The runtime type for byte array values. *)
type _ nym += Bytes: bytes nym

(** The runtime type for integer values. *)
type _ nym += Int: int nym

(** The runtime type for boxed 32-bt integer values. *)
type _ nym += Int32: int32 nym

(** The runtime type for boxed 64-bit integer values. *)
type _ nym += Int64: int64 nym

(** The runtime type for floating point values. *)
type _ nym += Float: float nym

(** The runtime type for option values. *)
type _ nym += Option: 'a nym -> 'a option nym

(** The runtime type for pair values. *)
type _ nym += Pair: 'a nym  * 'b nym -> ('a * 'b) nym

(** The runtime type for sequence values. *)
type _ nym += Seq: 'a nym -> 'a Seq.t nym

(** The type nym indicating a value of opaque type. This kind of recursion is
    often useful for representing containers of heterogenously typed values
    that may themselves include containers.
*)
type _ nym += Opaque: opaque nym

(** Use [witness n v] to make an opaque value of [v] witnessed by [n]. *)
val witness: 'a nym -> 'a -> opaque

(** The exception raised when extracting from an opaque value with a nym that
    is not equivalent to the encapsulated type.
*)
type exn += Type_error

(** A class that defines the equality constraint for all the type nyms defined
    above and provides for extending the equality constraint for new type nyms
    by defining subclasses.
*)
class horizon: object

    (** Define [obj#equiv a b] to apply the type equivalence constraint to [a]
        and [b]. It must return [Eq] if they are nyms for equivalent
        types, otherwise raise [Type_error]
    *)
    method equiv: 'a 'b. 'a nym -> 'b nym -> ('a, 'b) eq
end

(** The signature of modules for extracting opaque values within a given
    type equivalence horizon. Because all the functions in this module use the
    [equiv] method on the horizon used to construct it, expressions that
    directly match on the `Witness (_, nym)` type constructor are likely to be
    substantially more efficient.
*)
module type Form = sig

    (** Use [opt n v] to extract [v'] if [n] is equivalent to the encapsulated
        runtime type, otherwise raises an exception (conventionally
        [Type_error]).
    *)
    val req: 'a nym -> opaque -> 'a

    (** Use [opt n v] to extract [Some v'] if [n] is equivalent to the
        encapsulated runtime type, otherwise returns [None].
    *)
    val opt: 'a nym -> opaque -> 'a option

    (** Use [eq a b] to produce the equivalence constraint for the types
        associated with nyms [a] and [b] under the horizon. Returns [Eq] if
        [a] and [b] are nyms for equivalent types, otherwise raises
        [Type_error].
    *)
    val eq: 'a nym -> 'b nym -> ('a, 'b) eq

    (** Use [ck n v] to test whether [n] is equivalent to the encapsulated
        runtime type.
    *)
    val ck: 'a nym -> opaque -> bool
end

(** Use [form r] to create an unpacking module for an equivalence class. *)
val form: #horizon -> (module Form)

(** Include the default [Form] for the above horizon. *)
include Form

(*--- End ---*)
