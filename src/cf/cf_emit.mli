(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** Functional emitter/formatter combinators *)

(** {6 Overview}

    In counterpart to {Cf_scan}, this module provides functional combinators
    for composing imperative emitters for various arbitrary output chanenls.
    Specializations are provided for {Cf_encode.emitter}, and the {Buffer.t}
    and {Format.formatter} types in the standard library.
*)

(** {5 Type} *)

(** Use [('a, 'b) t] to represent an emitter on channels of type ['a] for
    values of type ['b].
*)
type ('a, 'b) t = F of ('a -> 'b -> unit) [@@caml.unboxed]

(** {5 Functions} *)

(** Use [apply m c v] to emit [v] on channel [c] with emitter [m]. *)
val apply: ('a, 'b) t -> 'a -> 'b -> unit

(** The empty emitter, performs no operation on the channel. *)
val nil: ('a, 'b) t

(** Use [seal m v] to make an emitter that seals [v] within [m]. *)
val seal: ('a, 'b) t -> 'b -> ('a, unit) t

(** Use [defer f] to make an emitter that applies [f] to its value to select
    the sealed emitter for it.
*)
val defer: ('b -> ('a, unit) t) -> ('a, 'b) t

(** Use [map f m] to make an emitter that applies its input value to [f] and
    emits the result with [m].
*)
val map: ('c -> 'b) -> ('a, 'b) t -> ('a, 'c) t

(** Use [coded sch] to make a emitter combinator for [sch]. *)
val coded: 'a Cf_encode.scheme -> (#Cf_encode.emitter, 'a) t

(** Use [pair a b] to make an emitter for pairs of values where [a] is the
    emitter for the first value and [b] is the emitter for the second. If
    [~sep] is provided, then it is inserted between the first and second
    values.
*)
val pair: ?sep:('a, unit) t -> ('a, 'b) t -> ('a, 'c) t -> ('a, 'b * 'c) t

(** Use [opt m] to make an emitter for optional values. If [~none] is provided,
    this its sealed value is emitter whenever the input of [m] is [None].
*)
val opt: ?none:('a, unit) t -> ('a, 'b) t -> ('a, 'b option) t

(** Use [seq m] to make an emitter for sequences where each element is emitted
    with [m]. If [~sep] is provided, then its sealed value is emitted between
    each element of the sequence.
*)
val seq: ?sep:('a, unit) t -> ('a, 'b) t -> ('a, 'b Seq.t) t

(** Use [group s] to make a sealed emitter that emits all the sealed values in
    the sequence [s]. If [~sep] is provided, then its sealed value is emitted
    between each element in the sequence.
*)
val group: ?sep:('a, unit) t -> ('a, unit) t Seq.t -> ('a, unit) t

(** Use [encl ~a ~b m] to make an emitter that first emits the sealed value in
    [a], then provides its input to [m], then emits the sealed value in [b].
*)
val encl: a:('a, unit) t -> b:('a, unit) t -> ('a, 'b) t -> ('a, 'b) t

(** {5 Abstract Data Renderers} *)

module Render: sig
    module type Output = sig type channel end

    module type Basis = sig
        type channel

        (** Same as [Cf_data_render.Basis.primitive]. *)
        val primitive: 'a Cf_type.nym -> (channel, 'a) t

        (** Same as [Cf_data_render.Basis.control]. *)
        val control: [
            | `Default
            | `Special of
                (channel, 'a) t -> ('a, 'b) Cf_data_render.control ->
                (channel, 'b) t
        ]

        (** Same as [Cf_data_render.Basis.pair]. *)
        val pair:
            'k Cf_data_render.pair -> (channel, unit) t ->
            (channel, unit) t -> (channel, unit) t

        (** Same as [Cf_data_render.Basis.sequence]. *)
        val sequence:
            'k Cf_data_render.container -> (channel, unit) t Seq.t ->
            (channel, unit) t
    end

    module Create(Out: Output)(B: Basis with type channel := Out.channel):
        Cf_data_render.Profile with type 'v scheme := (Out.channel, 'v) t

    (** Use [basis_control m c] to transform the emitter [m] to a new emitter
        according to the rendering control [c]. Raises [Invalid_argument] if
        [c] has an unrecognized extensible variant tag, i.e. not one of the
        tags defined in {Cf_data_render}.
    *)
    val basis_control:
        ('a, 'b) t -> ('b, 'c) Cf_data_render.control -> ('a, 'c) t
end

(** {5 Specializations for Buffer.t} *)

module To_buffer: sig

    (** The type of emitters to {Buffer.t} channels. *)
    type nonrec 'a t = (Buffer.t, 'a) t

    (** The emitter for characters. *)
    val char: char t

    (** The emitter for strings. *)
    val string: string t

    (** The emitter for byte arrays. *)
    val bytes: bytes t

    (** The emitter for string slices. *)
    val string_slice: string Cf_slice.t t

    (** The emitter for byte array slices. *)
    val bytes_slice: bytes Cf_slice.t t

    (** The emitter for Unicode code points encoded as UTF-8. *)
    val utf_8_uchar: Uchar.t t

    (** The emitter for Unicode code points encoded as UTF-16LE. *)
    val utf_16le_uchar: Uchar.t t

    (** The emitter for Unicode code points encoded as UTF-16BE. *)
    val utf_16be_uchar: Uchar.t t

    (** Use [substitute f] to make a string emitter than uses the keyword
        substitution method {Buffer.add_substitute} on the input.
    *)
    val substitute: (string -> string) -> string t

    (** The emitter for Buffer content. *)
    val buffer: Buffer.t t

    (** The emitter for the content read from standard input channels. *)
    val channel: int -> in_channel t

    (** Use [to_string m v] to emit [v] with [m] to a fresh buffer and return
        its contents.
    *)
    val to_string: 'a t -> 'a -> string

    module Render: sig
        module Create(B: Render.Basis with type channel := Buffer.t):
            Cf_data_render.Profile with type 'v scheme := 'v t
    end
end

(** {5 Specializations for Format.formatter} *)

module To_formatter: sig

    (** The type of emitters to {Format.formatter} channels. *)
    type nonrec 'a t = (Format.formatter, 'a) t

    (** The emitter for characters. *)
    val char: char t

    (** The emitter for strings. *)
    val string: string t

    (** The emitter for integers. *)
    val int: int t

    (** The emitter for floating point numbers. *)
    val float: float t

    (** The emitter for boolean values. *)
    val bool: bool t

    (** Use [break d] to make an emitter for formatting break hints
        for offset [d] that use the input for the number of spaces.
    *)
    val break: int -> int t

    (** The single space break hint. *)
    val space: unit t

    (** The zero space break hint. *)
    val cut: unit t

    (** Use [box d m] to enclose [m] in a compacting pretty-printing box with
        offset [d].
    *)
    val box: int -> 'a t -> 'a t

    (** Use [hbox d m] to enclose [m] in a horizontal pretty-printing box. *)
    val hbox: 'a t -> 'a t

    (** Use [vbox d m] to enclose [m] in a vertical pretty-printing box with
        offset [d].
    *)
    val vbox: int -> 'a t -> 'a t

    (** Use [hvbox d m] to enclose [m] in a horizontal/vertical pretty-printing
        box with offset [d].
    *)
    val hvbox: int -> 'a t -> 'a t

    (** Use [hovbox d m] to enclose [m] in a horizontal-or-vertical
        pretty-printing box with offset [d].
    *)
    val hovbox: int -> 'a t -> 'a t

    (** Use [brace m] to enclose [m] in matching curly-brace characters. *)
    val brace: 'a t -> 'a t

    (** Use [bracket m] to enclose [m] in matching square-bracket characters. *)
    val bracket: 'a t -> 'a t

    (** Use [paren m] to enclose [m] in matching parenthesis characters. *)
    val paren: 'a t -> 'a t

    (** Use [quote m] to enclose [m] in double-quote characters. *)
    val quote: 'a t -> 'a t

    (** Use [string_as n] to make a string emitter that formats the string as
        if it is length [n].
    *)
    val string_as: int -> string t

    (** Use [seq m] as an abbreviation for [seq ~sep:cut m]. *)
    val seq: 'a t -> 'a Seq.t t

    (** Use [group s] as an abbreviation for [group ~sep:cut s]. *)
    val group: unit t Seq.t -> unit t

    (** Use [to_string m v] to emit [v] with [m] to a fresh string formatter
        and returns its contents.
    *)
    val to_string: 'a t -> 'a -> string

    (** Use [to_string m v] to emit [v] with [m] to a fresh output channel
        formatter.
    *)
    val to_channel: out_channel -> 'a t -> 'a -> unit

    module Render: sig
        module Create(B: Render.Basis with type channel := Format.formatter):
            Cf_data_render.Profile with type 'v scheme := 'v t
    end
end

(*--- End ---*)
