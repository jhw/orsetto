(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

open OUnit2

let _ = Printexc.record_backtrace true

let jout = Cf_journal.stdout
let _ = jout#setlimit `Debug

module type T_signature = sig
    val test: test
end

(*
Gc.set {
    (Gc.get ()) with
    (* Gc.verbose = 0x3ff; *)
    Gc.verbose = 0x14;
};;

Gc.create_alarm begin fun () ->
    let min, pro, maj = Gc.counters () in
    Printf.printf "[Gc] minor=%f promoted=%f major=%f\n" min pro maj;
    flush stdout
end
*)

let _ = Printexc.record_backtrace true

type _ scancase =
    C_scan: { s: string; n: 'a Cf_type.nym; v: 'a } -> 'a scancase

let parser = Json_scan.Opaque.value ()

let try_scan (type a) ?(xreq = false) (C_scan t : a scancase) ctxt =
    match Json_scan.of_text parser @@ Ucs_text.of_string t.s with
    | exception (Json_scan.Bad_syntax xl) when not xreq ->
        let b = Buffer.create 0 in
        let pp = Format.formatter_of_buffer b in
        Format.fprintf pp "Json_scan: error @@ ";
        Json_scan.Annot.emit_form pp xl;
        Format.pp_print_newline pp ();
        Json_scan.Annot.dn xl |> Format.pp_print_string pp;
        Format.pp_print_flush pp ();
        assert_failure (Buffer.contents b)
    | exception Not_found when not xreq ->
        assert_failure (Printf.sprintf "unrecognized \"%s\"" t.s)
    | exception Not_found ->
        ()
    | exception (Json_scan.Bad_syntax _) when xreq ->
        ()
    | vl ->
        assert_equal ~ctxt ~msg:"exception required!" xreq false;
        let v = Json_scan.Annot.dn vl in
        match t.n with
        | Cf_type.Unit ->
            Json_type.ck t.n v |> assert_bool "type error [unit]"
        | Cf_type.Bool ->
            let b =
                match Json_type.opt t.n v with
                | Some b -> b
                | None -> assert_failure "type error [bool]"
            in
            let msg = Printf.sprintf "expected %B returned %B" t.v b in
            assert_equal ~ctxt ~cmp:(==) ~msg t.v b
        | Cf_type.Int ->
            let n =
                match Json_type.opt t.n v with
                | Some n -> n
                | None -> assert_failure "type error [int]"
            in
            let msg = Printf.sprintf "expected %d returned %d" t.v n in
            assert_equal ~ctxt ~cmp:Cf_relations.Int.equal ~msg t.v n
        | Cf_type.Float ->
            let n =
                match Json_type.opt t.n v with
                | Some n -> n
                | None ->
                    match Json_type.opt Cf_type.Int v with
                    | Some n -> float_of_int n
                    | None -> assert_failure "type error [float or int]"
            in
            let msg = Printf.sprintf "expected %g returned %g" t.v n in
            assert_equal ~ctxt ~cmp:(=) ~msg t.v n
        | Ucs_type.Text ->
            let s =
                match Json_type.opt t.n v with
                | Some s -> s
                | None -> assert_failure "type error [text]"
            in
            let msg =
                let Ucs_text.Octets a = t.v and Ucs_text.Octets b = s in
                Printf.sprintf {|expected "%s" returned "%s"|} a b
            in
            assert_equal ~ctxt ~cmp:Ucs_text.equal ~msg t.v s
        | Cf_type.(Seq Opaque) ->
            let s =
                match Json_type.opt t.n v with
                | Some n -> n
                | None -> assert_failure "type error [seq opaque]"
            in
            let cmptyp (type v) (nym : v Cf_type.nym) a b =
                match nym, Json_type.opt nym a, Json_type.opt nym b with
                | Cf_type.Unit, Some (), Some () -> true
                | Cf_type.Bool, Some a, Some b -> a = b
                | Cf_type.(Seq Opaque), Some a, Some b ->
                    (* NOTE WELL: comparing only the length, not the sequence
                        because a recursive opaque comparison is hard.
                    *)
                    Cf_seq.length a == Cf_seq.length b
                | _, _, _ ->
                    false
            in
            let cmp1 a b =
                cmptyp Cf_type.Unit a b ||
                cmptyp Cf_type.Bool a b ||
                cmptyp Cf_type.(Seq Opaque) a b
            in
            let cmp = Cf_seq.eqf cmp1 in
            let msg = "unexpected sequence returned" in
            assert_equal ~ctxt ~cmp ~msg t.v s
        | Cf_type.(Seq (Pair (Ucs_type.Text, Opaque))) ->
            assert_failure "type error [Seq (Pair (Ucs_type.Text, Opaque))]"
        | Cf_type.(Seq (Pair (String, Opaque))) ->
            assert_failure "type error [Seq (Pair (String, Opaque))]"
        | Cf_type.(Seq (Pair (Opaque, Opaque))) ->
            let s =
                match Json_type.opt t.n v with
                | Some n -> n
                | None ->
                    assert_failure "type error [seq (pair (opaque, opaque))]"
            in
            let cmptyp (type v) (nym : v Cf_type.nym) a b =
                let ka, va = a and kb, vb = b in
                let ka = Json_type.req Ucs_type.Text ka in
                let kb = Json_type.req Ucs_type.Text kb in
                Ucs_text.equal ka kb &&
                    match nym, Json_type.opt nym va, Json_type.opt nym vb with
                    | Cf_type.Unit, Some (), Some () -> true
                    | Cf_type.Bool, Some a, Some b -> a = b
                    | _, _, _ -> false
            in
            let cmp1 a b =
                cmptyp Cf_type.Unit a b ||
                cmptyp Cf_type.Bool a b
            in
            let cmp = Cf_seq.eqf cmp1 in
            let msg = "unexpected sequence returned" in
            assert_equal ~ctxt ~cmp ~msg t.v s
        | _ ->
            assert_failure "try_scan: incomplete! [type not supported]"

type emitcase = C_emit of {
    v: Cf_type.opaque;
    s: string;
}

let try_emit (C_emit t) ctxt =
    let Ucs_text.Octets s = Json_emit.Opaque.to_text t.v in
    let msg = Printf.sprintf "emit \"%s\" -> \"%s\"" t.s s in
    assert_equal ~ctxt ~cmp:String.equal ~msg t.s s

module T_literals: T_signature = struct
    let s_null = C_scan { s = "null"; n = Cf_type.Unit; v = () }

    let s_null_leadspace = C_scan { s = " null"; n = Cf_type.Unit; v = () }
    let s_null_trailspace = C_scan { s = "null "; n = Cf_type.Unit; v = () }

    let s_null_trailcomma = C_scan { s = "null,"; n = Cf_type.Unit; v = () }

    let s_false = C_scan { s = "false"; n = Cf_type.Bool; v = false }
    let s_true = C_scan { s = "true"; n = Cf_type.Bool; v = true }

    let e_null = C_emit { s = "null"; v = Cf_type.(witness Unit ()) }
    let e_false = C_emit { s = "false"; v = Cf_type.(witness Bool false) }
    let e_true = C_emit { s = "true"; v = Cf_type.(witness Bool true) }

    let test = "lit" >::: [
        "~null" >:: try_scan s_null;
        "~null-leadspace" >:: try_scan s_null_leadspace;
        "~null-trailspace" >:: try_scan s_null_trailspace;
        "~null-trailcomma" >:: try_scan s_null_trailcomma ~xreq:true;
        "~false" >:: try_scan s_false;
        "~true" >:: try_scan s_true;

        "!null" >:: try_emit e_null;
        "!false" >:: try_emit e_false;
        "!true" >:: try_emit e_true;
    ]
end

module T_numbers: T_signature = struct
    let mk_emit n s =
        C_emit { v = Cf_type.(witness Float n); s } |> try_emit

    let mk_scan (type a) ?xreq s (n : a Cf_type.nym) =
        match n with
        | Cf_type.Int ->
            let v = int_of_string s in
            C_scan { s; n; v } |> try_scan ?xreq
        | Cf_type.Float ->
            let v = float_of_string s in
            C_scan { s; n; v } |> try_scan ?xreq
        | _ ->
            assert false

    let test = "num" >::: [
        "~zero" >:: mk_scan "0" Cf_type.Int;
        "~one" >:: mk_scan "1" Cf_type.Int;
        "~-1" >:: mk_scan "-1" Cf_type.Int;
        "~max_int" >:: mk_scan (string_of_int max_int) Cf_type.Int;
        "~min_int" >:: mk_scan (string_of_int min_int) Cf_type.Int;
        "~0.0" >:: mk_scan "0.0" Cf_type.Float;
        "~123.0000" >:: mk_scan "123.000" Cf_type.Float;
        "~epsilon" >:: mk_scan "2.22044604925031308e-16" Cf_type.Float;
        "~1.23e12" >:: mk_scan "1.23e12" Cf_type.Float;
        "~fp:123" >:: mk_scan "123" Cf_type.Float;
        "~fp:max_int" >:: mk_scan "4611686018427387903" Cf_type.Float;

        ">zero" >:: mk_emit 0.0 "0";
        ">one" >:: mk_emit 1.0 "1";
        ">-1" >:: mk_emit (-1.0) "-1";
        ">-0.012" >:: mk_emit (-0.012) "-0.012";
        ">1.23e12" >:: mk_emit 1.23e+12 "1.23e+12";
        ">1.23e+45" >:: mk_emit 1.23e+45 "1.23e+45";
        ">1.23e-45" >:: mk_emit 1.23e-45 "1.23e-45";
        ">12e34" >:: mk_emit 12e34 "1.2e+35";
    ]
end

module T_strings: T_signature = struct
    let mk_emit v s =
        let v = Ucs_text.of_string v |> Cf_type.witness Ucs_type.Text in
        C_emit { v; s } |> try_emit

    let mk_scan ?xreq s v =
        let n = Ucs_type.Text and v = Ucs_text.of_string v in
        C_scan { s; n; v } |> try_scan ?xreq

    let test = "str" >::: [
        "~empty" >:: mk_scan {|""|} "";
        "~empty-trailx" >:: mk_scan ~xreq:true {|""xyz|} "";
        "~ab-c" >:: mk_scan {|"ab c"|} "ab c";
        "~latin-x" >:: mk_scan "\"colibr\xC3\xAC\"" "colibr\xC3\xAC";
        "~esc1" >:: mk_scan {|"<\b\t\n\f\r>"|} "<\x08\x09\x0A\x0C\x0D>";
        "~esc1a" >:: mk_scan {|"</\\\">"|} "</\\\">";
        "~jsw" >:: mk_scan {|"<\u2028>"|} "<\xE2\x80\xA8>";
        "~gclef" >:: mk_scan {|"\ud834\udd1e"|} "\xF0\x9D\x84\x9E";

        ">empty" >:: mk_emit "" {|""|};
        ">ab-c" >:: mk_emit "ab c" {|"ab c"|};
        ">latin-x" >:: mk_emit "colibr\xC3\xAC" "\"colibr\xC3\xAC\"";
        ">esc1" >:: mk_emit "<\x08\x09\x0A\x0C\x0D>" {|"<\b\t\n\f\r>"|};
        ">esc1a" >:: mk_emit "</\\\">" {|"</\">"|};
        ">jsw" >:: mk_emit "<\xE2\x80\xA8>" {|"<\u2028>"|};
        ">gclef" >:: mk_emit "<\xF0\x9D\x84\x9E>" {|"<\ud834\udd1e>"|};
    ]
end

module T_arrays: T_signature = struct
    let array_nym = Cf_type.(Seq Opaque)

    let mk_emit nym v s =
        let v =
            List.to_seq v
            |> Seq.map (Cf_type.witness nym)
            |> Cf_type.(witness array_nym)
        in
        C_emit { v; s }

    let e_empty = mk_emit Cf_type.Opaque [] "[]"
    let e_1null = mk_emit Cf_type.Unit [()] "[ null ]"
    let e_bools = mk_emit Cf_type.Bool [ false; true ] "[ false, true ]"
    let e_one = mk_emit Cf_type.(Seq Opaque) [ Seq.empty ] "[ [] ]"

    let mk_scan nym v s =
        let v = List.to_seq v |> Seq.map (Cf_type.witness nym) in
        C_scan { v; n = array_nym; s }

    let s_empty = mk_scan Cf_type.Opaque [] "[]"
    let s_1null = mk_scan Cf_type.Unit [ () ] "[ null ]"
    let s_bools = mk_scan Cf_type.Bool [ false; true ] "[ false, true ]"
    let s_one = mk_scan Cf_type.(Seq Opaque) [ Seq.empty ] "[ [] ]"

    let s_1null_trailcomma = mk_scan Cf_type.Unit [ () ] "[ null, ]"

    let test = "arr" >::: [
        "!empty" >:: try_emit e_empty;
        "!1null" >:: try_emit e_1null;
        "!bools" >:: try_emit e_bools;
        "!one" >:: try_emit e_one;

        "~empty" >:: try_scan s_empty;
        "~1null" >:: try_scan s_1null;
        "~bools" >:: try_scan s_bools;
        "~one" >:: try_scan s_one;

        "~1null-trailcomma" >:: try_scan ~xreq:true s_1null_trailcomma;
    ]
end

module T_objects: T_signature = struct
    let object_nym = Cf_type.(Seq (Pair (Opaque, Opaque)))

    let mk_inner nym (k, v) =
        let k = Cf_type.witness Ucs_type.Text @@ Ucs_text.of_string k in
        let v = Cf_type.witness nym v in
        k, v

    let mk_emit nym v s =
        let v =
            List.to_seq v
                |> Seq.map (mk_inner nym)
                |> Cf_type.(witness object_nym)
        in
        C_emit { v; s }

    let e_empty = mk_emit Cf_type.Opaque [] "{}"
    let e_null = mk_emit Cf_type.Unit [ "null", () ] {|{ "null": null }|}

    let e_bools =
        let v = [ "false", false; "true", true ] in
        let s = {|{ "false": false, "true": true }|} in
        mk_emit Cf_type.Bool v s

    let mk_scan nym v s =
        let v = List.to_seq v |> Seq.map (mk_inner nym) in
        C_scan { v; n = object_nym; s }

    let s_empty = mk_scan Cf_type.Opaque [] "{}"
    let s_null = mk_scan Cf_type.Unit [ "null", () ] {|{ "null": null }|}

    let s_bools =
        let v = [ "false", false; "true", true ] in
        let s = {|{ "false": false, "true": true }|} in
        mk_scan Cf_type.Bool v s

    let test = "obj" >::: [
        "!empty" >:: try_emit e_empty;
        "!null" >:: try_emit e_null;
        "!bools" >:: try_emit e_bools;

        "~empty" >:: try_scan s_empty;
        "~null" >:: try_scan s_null;
        "~bools" >:: try_scan s_bools;
    ]
end

let all = "t" >::: [
    T_literals.test;
    T_numbers.test;
    T_strings.test;
    T_arrays.test;
    T_objects.test;
]

let _ = run_test_tt_main all

(*--- End ---*)
