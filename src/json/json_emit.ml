(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module E0 = Cf_emit
module E = E0.To_formatter

type 'a t = 'a E.t

let null = E0.seal E.string "null"
let boolean = E0.map (function true -> "true" | false -> "false") E.string
let integer = E.int
let int32 = E0.F (fun pp i32 -> Format.fprintf pp "%ld" i32)
let int64 = E0.F (fun pp i64 -> Format.fprintf pp "%Ld" i64)
let float = E0.F (fun pp n -> Format.fprintf pp "%g" n)

let string =
    let module M = Cf_seqmonad in
    let open M.Infix in
    let put_uchar c = M.one @@ Uchar.of_char c in
    let is_whitespace uc =
        Ucs_db_aux.query_map Ucs_property_core.white_space uc
    in
    let rec sedicet n cp =
        if n > 0 then begin
            let n = n - 4 in
            let digit = (cp lsr n) land 0xf in
            let base = if digit < 10 then 48 else 87 in
            let uc = Uchar.unsafe_of_int (base + digit) in
            let* _ = M.one uc in
            (sedicet[@tailcall]) n cp
        end
        else
            M.return ()
    in
    let escape_char c =
        let* _ = put_uchar '\\' in
        put_uchar c
    in
    let escape_hexdigits cp =
        let* _ = escape_char 'u' in
        sedicet 16 cp
    in
    let rec loop s =
        match s () with
        | Seq.Nil ->
            M.return ()
        | Seq.Cons (uc, s) ->
            match Uchar.to_int uc with
            | 0x08 -> escloop1 'b' s
            | 0x09 -> escloop1 't' s
            | 0x0A -> escloop1 'n' s
            | 0x0C -> escloop1 'f' s
            | 0x0D -> escloop1 'r' s
            | cp when cp < 0x20 -> escloopu cp s
            | cp when cp >= 0x10000 -> escloopu2 cp s
            | cp when cp > 0x7F && is_whitespace uc -> escloopu cp s
            | _ ->
                let* _ = M.one uc in
                (loop[@tailcall]) s
    and escloop1 c s =
        let* _ = escape_char c in
        (loop[@tailcall]) s
    and escloopu cp s =
        let* _ = escape_hexdigits cp in
        (loop[@tailcall]) s
    and escloopu2 cp s =
        let cp = cp - 0x10000 in
        let lo = 0xdc00 lor (cp land 0x3ff) in
        let cp = cp lsr 10 in
        let hi = 0xd800 lor (cp land 0x3ff) in
        let* _ = escape_hexdigits hi in
        let* _ = escape_hexdigits lo in
        (loop[@tailcall]) s
    in
    let aux text =
        let text = Ucs_text.(of_seq @@ M.eval @@ loop @@ to_seq text) in
        let Ucs_text.Octets octets = text in
        octets
    in
    E.quote @@ E0.map aux @@ E.string

let ldelim c = E0.(seal @@ pair (seal E.char c) E.space) ((), ())
let rdelim c = E0.(seal @@ pair (E.break (-2)) (seal E.char c)) (1, ())

let comma = ldelim ','
let colon = ldelim ':'

let lbracket = ldelim '['
let rbracket = rdelim ']'
let nilarray = E0.seal E.string "[]"

let lbrace = ldelim '{'
let rbrace = rdelim '}'
let nilobject = E0.seal E.string "{}"

let array =
    let group inner =
        if Cf_seq.empty inner then
            nilarray
        else begin
            E.hvbox 2 @@
            E0.encl ~a:lbracket ~b:rbracket @@
            E0.group ~sep:comma @@
            Seq.map (E.hovbox 2) inner
        end
    in
    let aux f pp v = E0.apply (group @@ f v) pp () in
    let enter f = E0.F (aux f) in
    enter

let objval =
    let member (key, value) =
        E.hovbox 2 @@ E0.group ~sep:colon @@ List.to_seq @@ [
            E0.seal string key;
            value;
        ]
    in
    let group inner =
        if Cf_seq.empty inner then
            nilobject
        else begin
            E.hvbox 2 @@
            E0.encl ~a:lbrace ~b:rbrace @@
            E0.group ~sep:comma @@
            Seq.map member inner
        end
    in
    let aux f pp v = E0.apply (group @@ f v) pp () in
    let enter f = E0.F (aux f) in
    enter

module Opaque = struct
    type mode = Mode

    let mode () = Mode

    let value =
        let t_array = Cf_type.(Seq Opaque) in
        let t_object_obsolescent =
            Cf_type.(Seq (Pair (Ucs_type.Text, Opaque)))
        in
        let t_object = Cf_type.(Seq (Pair (Opaque, Opaque))) in
        let rec loop pp v =
            if Json_type.ck Cf_type.Opaque v then
                loop pp @@ Json_type.req Cf_type.Opaque v
            else if Json_type.ck Cf_type.Unit v then
                E0.apply null pp ()
            else if Json_type.ck Cf_type.Bool v then
                E0.apply boolean pp @@ Json_type.req Cf_type.Bool v
            else if Json_type.ck Cf_type.Int v then
                E0.apply integer pp @@ Json_type.req Cf_type.Int v
            else if Json_type.ck Cf_type.Float v then
                E0.apply float pp @@ Json_type.req Cf_type.Float v
            else if Json_type.ck Cf_type.String v then begin
                let text =
                    Ucs_text.of_string @@ Json_type.req Cf_type.String v
                in
                E0.apply string pp text
            end
            else if Json_type.ck Ucs_type.Text v then
                E0.apply string pp @@ Json_type.req Ucs_type.Text v
            else if Json_type.ck t_array v then begin
                let inner v' = E0.F (fun pp () -> loop pp v') in
                let v = Json_type.req t_array v in
                let outer () = Seq.map inner v in
                E0.apply (array outer) pp ()
            end
            else if Json_type.ck t_object v then begin
                let inner (k, v') =
                    let k =
                        match Json_type.opt Ucs_type.Text k with
                        | Some k -> k
                        | None ->
                            match Json_type.opt Cf_type.String k with
                            | Some s -> Ucs_text.of_string s
                            | None ->
                                invalid_arg (__MODULE__ ^
                                    ".value: invalid opaque field name.")
                    in
                    let v = E0.F (fun pp () -> loop pp v') in
                    k, v
                in
                let v = Json_type.req t_object v in
                let outer () = Seq.map inner v in
                E0.apply (objval outer) pp ()
            end
            else if Json_type.ck t_object_obsolescent v then begin
                let inner (k, v') = k, E0.F (fun pp () -> loop pp v') in
                let v = Json_type.req t_object_obsolescent v in
                let outer () = Seq.map inner v in
                E0.apply (objval outer) pp ()
            end
            else
                invalid_arg (__MODULE__ ^ ".value: invalid opaque value.")
        in
        let enter ?mode:_ () = E0.F loop in
        enter

    let to_text ?mode v =
        let emitter = value ?mode () in
        Ucs_text.Unsafe.of_string @@ E.to_string emitter v
end

module Render = struct
    module Basis = struct
        let p_string = Cf_emit.map Ucs_text.of_string string

        let primitive: type a. a Cf_type.nym -> a t = fun nym ->
            match nym with
            | Cf_type.Unit -> null
            | Cf_type.Bool -> boolean
            | Cf_type.Int -> integer
            | Cf_type.Int32 -> int32
            | Cf_type.Int64 -> int64
            | Cf_type.Float -> float
            | Ucs_type.Text -> string
            | Cf_type.String -> p_string
            | Cf_type.Opaque -> Opaque.value ()
            | _ -> invalid_arg (__MODULE__ ^ ": primitive undefined for JSON!")

        let control = `Default

        let pair:
            type a. a Cf_data_render.pair -> unit t -> unit t -> unit t
            = fun container first second ->
            E.hovbox 2 @@
                match container with
                | `Structure Cf_type.String
                | `Structure Ucs_type.Text
                | `Variant Cf_type.String
                | `Variant Ucs_type.Text ->
                    E0.group ~sep:colon @@ List.to_seq [ first; second ]
                | _ ->
                    E.hovbox 2 @@ E0.encl ~a:lbracket ~b:rbracket @@
                        E0.group ~sep:comma @@ List.to_seq [ first; second ]

        let affix =
            let array3 = lbracket, rbracket, nilarray in
            let object3 = lbrace, rbrace, nilobject in
            let enter:
                type k. k Cf_data_render.container -> unit t * unit t * unit t
                = fun container ->
                    match container with
                    | `Structure Cf_type.String
                    | `Structure Ucs_type.Text
                    | `Variant Cf_type.String
                    | `Variant Ucs_type.Text ->
                        object3
                    | _ ->
                        array3
            in
            enter

        let sequence container packets =
            let a, b, nil = affix container in
            if Cf_seq.empty packets then
                nil
            else begin
                E.hvbox 2 @@
                E0.encl ~a ~b @@
                E0.group ~sep:comma packets
            end
    end

    module R = E.Render.Create(Basis)

    let emit = R.scheme

    let object_model fields =
        let index = new Cf_data_render.index Cf_type.String in
        Cf_data_render.structure index fields
end

let to_text v = Opaque.to_text v
let value = Opaque.value ()

(*--- End ---*)
