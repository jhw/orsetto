(*---------------------------------------------------------------------------*
  Copyright (C) 2017-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** JavaScript Object Notation (JSON) output formatter. *)

(** {6 Overview}

    Specialization of {Cf_emit} to output texts in the JSON format. Combinators
    are provided for the basic JSON types to be used for directly emitting
    application data as JSON text. Additional conveniences are provided for
    emitting encapsulated JSON values as text. All output is encoded in UTF-8.
*)

(** {5 Type} *)

(** The type of JSON emitters. *)
type 'a t = private 'a Cf_emit.To_formatter.t

(** {5 Basic Emitters} *)

(** The emitter for the JSON {b null} literal *)
val null: unit t

(** The emitter for JSON boolean values. *)
val boolean: bool t

(** The emitter for integers as JSON numeric values. *)
val integer: int t

(** The emitter for 32-bit integers as JSON numeric values. *)
val int32: int32 t

(** The emitter for 64-bit integers as JSON numeric values. *)
val int64: int64 t

(** The emitter for floats as JSON numeric values. *)
val float: float t

(** The emitter for Unicode texts as JSON string literals. *)
val string: Ucs_text.t t

(** Use [array f] to make an emitter for values represented as JSON arrays.
    Applies the emitted value to [f] and elaborates the returned sequence of
    sealed emitters as a JSON array.
*)
val array: ('a -> unit t Seq.t) -> 'a t

(** Use [objval f] to make an emitter for values represented as JSON objects.
    Applies the emitted value to [f] and elaborates the returned sequence of
    key/value pairs as a JSON object. The value of each field is a sealed
    emitter.
*)
val objval: ('a -> (Ucs_text.t * unit t) Seq.t) -> 'a t

(** {5 Opaque Types} *)

(** A submodule containing logic for emitting JSON texts from values of type
    [Cf_type.opaque] according to optional mode selectors.
*)
module Opaque: sig

    (** The mode selectors for emitting JSON texts from opaque values. *)
    type mode

    (** Use [mode ()] to create a mode selector record for the opaque value
        emitter. Use any of the various optional parameters to set a mode
        selector to other than its default value.

        No selector modes are defined at present.
    *)
    val mode: unit -> mode

    (** Use [value ()] to create an opaque value emitter. The following table
        describes the runtime type indications required for values emitted.

        - {b null}:     [Cf_type.Unit]
        - {b boolean}:  [Cf_type.Bool]
        - {b number}:   [Cf_type.Int] or [Cf_type.Float]
        - {b string}:   [Cf_type.String] or [Ucs_type.Text]
        - {b array}:    [Cf_type.(Seq Opaque)]
        - {b object}:   [Cf_type.(Seq (Pair (Opaque, Opaque))]

        Use the [~mode] parameter to select modes other than the default.
        Raises [Invalid_argument] if the witnessed type is not valid for output
        as a JSON value.

        Note well: the first opaque value of each pair element in an object
        value must be witness either by [Ucs_type.Text] or [Cf_type.String].
    *)
    val value: ?mode:mode -> unit -> Cf_type.opaque t

    (** Use [to_text v] to format [v] as a Unicode text. Use [~mode] to select
        the encoding modes. Raises [Invalid_argument] if [v] was not witnessed
        with a type compatible with JSON encoding.
    *)
    val to_text: ?mode:mode -> Cf_type.opaque -> Ucs_text.t
end

(** {5 Abstract Data} *)

(** A submodule to encapsulate functions for use with data rendering models. *)
module Render: sig
    open Cf_data_render

    (** Use [object_model fields] to construct a structure data model for JSON
        objects with the unsorted members comprising [fields].
    *)
    val object_model: (string index, 'v) bind array -> 'v model

    (** Use [emit m] to make an emitter for values according to an abstract
        data model [m]. All containers are emitted as JSON arrays, except for
        [`Structure Cf_type.String] and [`Structure Ucs_type.Text] which are
        emitted as JSON objects.
    *)
    val emit: 'a model -> 'a t
end

(** {6 Deprecated} *)

(** Use [to_text v] to format [v] as a Unicode text. Raises [Invalid_argument]
    if [v] was not witnessed with a type compatible with JSON encoding.
*)
val to_text: Cf_type.opaque -> Ucs_text.t
    [@@ocaml.alert deprecated "Use Opaque.to_text instead!"]

(** An opaque value emitter created by [Opaque.value] with default mode. *)
val value: Cf_type.opaque t
    [@@ocaml.alert deprecated "Use Opaque.value instead!"]

(*--- End ---*)
