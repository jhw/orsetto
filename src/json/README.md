# src/json - JavaScript Object Notation (JSON)

The abstract from RFC 8259 says:

> JavaScript Object Notation (JSON) is a lightweight, text-based,
> language-independent data interchange format.  It was derived from the
> ECMAScript Programming Language Standard.  JSON defines a small set of
> formatting rules for the portable representation of structured data.

The following modules are intended to comprise public interfaces:

- *Json_emit* -- A general purpose JSON emitter.
- *Json_event* -- A representation of a JSON stream event.
- *Json_scan* -- A general purpose JSON parser.
- *Json_type* -- The type equivalence relation for JSON encodable values.
- *Json_value* -- An obsolescent representation of a JSON encoded value.

Note well: the **Json_emit.value** function should be changed to operate on a
**Cf_type.opaque** type argument (see Issue #31).
