(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2021, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

module E = Json_event

module Annot = Ucs_scan.UTF8.Annot

module Lex = struct
    module A = Annot
    module LS = Ucs_lex_scan.UTF8
    open LS.Affix

    let ( !. ) c = !: (Uchar.of_char c)

    let r_space =
        let sp = !.'\x20' in
        let tab = !.'\x09' in
        let lf = !.'\x0A' in
        let cr = !.'\x0D' in
        let exp = !+ (sp $| tab $| lf $| cr) in
        let tok = A.mv E.space in
        LS.rule exp tok

    let r_null = "null" $$> A.mv E.null
    let r_false = "false" $$> A.mv (E.boolean false)
    let r_true = "true" $$> A.mv (E.boolean true)
    let r_lbrace = LS.rule !.'{' (A.mv (E.signal `Syn_object))
    let r_rbrace = LS.rule !.'}' (A.mv (E.signal `Fin_object))
    let r_lsquare = LS.rule !.'[' (A.mv (E.signal `Syn_array))
    let r_rsquare = LS.rule !.']' (A.mv (E.signal `Fin_array))
    let r_comma = LS.rule !.',' (A.mv (E.signal `Con_element))
    let r_colon = LS.rule !.':' (A.mv (E.signal `Con_member))

    module NS = struct
        include Cf_scan.ASCII
        include Cf_number_scan.ASCII

        open! Infix

        let p_int =
            let* n = signed_int in
            let* x = fin in
            if x then return @@ E.integer n else nil

        let p_float =
            let* n = scientific_float in
            let* x = fin in
            if x then return @@ E.unsafe_float n else nil

        let p_number = alt [ p_int; p_float ]
    end

    let r_number =
        let is_nonzero_digit uc =
            let cp = Uchar.to_int uc in
            cp > 48 && cp < 58
        and is_digit uc =
            let cp = Uchar.to_int uc in
            cp > 47 && cp < 58
        in
        let zero = !.'0' and
            minus = !.'-' and
            plus = !.'+' and
            point = !.'.' and
            digit = !^ is_digit and
            nonzero = !^ is_nonzero_digit and
            echar = !.'e' $| !.'E'
        in
        let expon = echar $& (!? (plus $| minus)) $& (!+ digit) and
            fract = point $& (!+ digit) and
            nat = zero $| (nonzero $& (!* digit))
        in
        let exp =
            (!? minus) $& nat $& (!? fract) $& (!? expon)
        and tok sl =
            let Ucs_text.Octets u8s = A.dn sl in
            let event = NS.(of_string p_number u8s) in
            A.mv event sl
        in
        LS.rule exp tok

    let unescape =
        let fail_surrogate_pair _ =
            failwith "invalid escaped surrogate pair"
        in
        let rec aux_sedicet n cp s =
            if n > 0 then begin
                match s () with
                | Seq.Nil ->
                    assert (not true);
                    (aux_sedicet[@tailcall]) n cp s
                | Seq.Cons (uc, s) ->
                    let digit =
                        let cp = Uchar.to_int uc in
                        if cp >= 48 && cp <= 57 then cp - 48
                        else if cp >= 65 && cp <= 71 then cp - 55
                        else begin assert (cp >= 97 && cp <= 102); cp - 87 end
                    in
                    (aux_sedicet[@tailcall]) (pred n) (cp * 16 + digit) s
            end
            else
                cp, s
        in
        let esc = function
            | 'b' -> '\x08'
            | 'f' -> '\x0C'
            | 'n' -> '\x0A'
            | 'r' -> '\x0D'
            | 't' -> '\x09'
            | c ->
                assert (match c with '"' | '\\' | '/' -> true | _ -> false); c
        in
        let putc exr uc =
            let exr = (exr : Cf_encode.emitter) in
            exr#emit Ucs_transport.UTF8.uchar_encode_scheme uc;
        in
        let rec loop b exr s =
            match s () with
            | Seq.Nil ->
                Ucs_text.Unsafe.of_string @@ Buffer.contents b
            | Seq.Cons (uc, s) ->
                if Uchar.to_int uc <> 92 (* REVERSE SOLIDUS *) then begin
                    putc exr uc;
                    (loop[@tailcall]) b exr s
                end
                else
                    (escape[@tailcall]) b exr s
        and escape b exr s =
            match s () with
            | Seq.Nil ->
                assert (not true);
                (escape[@tailcall]) b exr s
            | Seq.Cons (uc, s) ->
                assert (Uchar.is_char uc);
                let c = Uchar.to_char uc in
                if c = 'u' then
                    (sedicet[@tailcall]) b exr s
                else begin
                    putc exr @@ Uchar.of_char @@ esc c;
                    (loop[@tailcall]) b exr s
                end
        and sedicet b exr s =
            let cp, s' = aux_sedicet 4 0 s in
            if cp < 0xd800 || cp >= 0xe000 then begin
                putc exr (Uchar.unsafe_of_int cp);
                (loop[@tailcall]) b exr s'
            end
            else if cp >= 0xdc00 then
                fail_surrogate_pair s
            else
                (low_surrogate[@tailcall]) b exr cp s'
        and low_surrogate b exr hi s =
            if Cf_seq.empty s then fail_surrogate_pair s;
            match s () with
            | Seq.Nil ->
                assert (not true);
                (low_surrogate[@tailcall]) b exr hi s
            | Seq.Cons (uc, s) ->
                if Uchar.to_int uc <> 92 (* REVERSE SOLIDUS *) then
                    fail_surrogate_pair s;
                match s () with
                | Seq.Nil ->
                    assert (not true);
                    (low_surrogate[@tailcall]) b exr hi s
                | Seq.Cons (uc, s) ->
                    if Uchar.to_int uc <> 117 (* LOWERCASE U *) then
                        fail_surrogate_pair s;
                    let cp, s' = aux_sedicet 4 0 s in
                    if cp < 0xdc00 || cp >= 0xe000 then fail_surrogate_pair s;
                    let hi = hi land 0x3ff and lo = cp land 0x3ff in
                    let cp = 0x10000 + ((hi lsl 10) lor lo) in
                    if Uchar.is_valid cp then begin
                        putc exr @@ Uchar.unsafe_of_int cp;
                        (loop[@tailcall]) b exr s'
                    end
                    else
                        fail_surrogate_pair s
        in
        let enter s =
            let b = Buffer.create 0 in
            let exr = Cf_encode.buffer_emitter b in
            (loop[@tailcall]) b exr s
        in
        enter

    let r_string =
        let escape_not_required uc =
            let cp = Uchar.to_int uc in
            assert (cp >= 0 && cp <= 0x10FFFF);
            (cp >= 0x20 && cp <= 0x21) ||
            (cp >= 0x23 && cp <= 0x5B) ||
            (cp >= 0x5D)
        and is_hexdigit uc =
            let cp = Uchar.to_int uc in
            (cp > 47 && cp < 58) ||
            (cp > 64 && cp < 71) ||
            (cp > 96 && cp < 103)
        in
        let hexdigit = !^ is_hexdigit in
        let sedicet = hexdigit $& hexdigit $& hexdigit $& hexdigit in
        let quote = !.'"' and rsolidus = !.'\\' in
        let unescaped = !^ escape_not_required
        and escaped =
            rsolidus $& (
                quote $| rsolidus $| !.'/' $| !.'b' $| !.'f' $| !.'n' $|
                !.'r' $| !.'t' $| (!.'u' $& sedicet)
            )
        in
        let exp = quote $& !* (unescaped $| escaped) $& quote in
        let tok sl =
            let event =
                let Ucs_text.Octets u8s = Annot.dn sl in
                let sx = Cf_slice.of_substring u8s 1 (String.length u8s - 1) in
                try
                    E.string @@ unescape @@ Ucs_transport.UTF8.seq_of_slice sx
                with
                | Failure msg ->
                    raise @@ Ucs_scan.UTF8.Bad_syntax (A.mv msg sl)
            in
            A.mv event sl
        in
        LS.rule exp tok

    let rules = Array.to_seq [|
        r_space; r_null; r_false; r_true; r_lbrace; r_rbrace; r_lsquare;
        r_rsquare; r_comma; r_colon; r_number; r_string
    |]

    let scan = LS.analyze rules
end

let lexer = Lex.scan

module Aux = struct
    type symbol = Uchar.t
    type position = Cf_annot.Textual.position

    module Token = Json_event
    module Form = Annot.Scan_basis.Form
    module Scan = Ucs_scan.UTF8

    open Scan.Affix

    let lexer =
        let* b = Scan.fin in
        Scan.(if b then nil else or_fail "lexical error" Lex.scan)
end

module S = Cf_scan.Staging.Create(Aux)
open S.Infix

let event =
    let rec loop () =
        let* evl = S.any in begin
            match[@warning "-4"] Annot.dn evl with
            | E.Space -> (loop[@tailcall]) ()
            | _ -> S.return evl
        end
    in
    loop ()

let space =
    let* evl = S.any in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Space -> S.return @@ Annot.mv () evl
        | _ -> S.nil
    end

let null =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Null -> S.return @@ Annot.mv () evl
        | _ -> S.nil
    end

let boolean =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.False -> S.return @@ Annot.mv false evl
        | E.True -> S.return @@ Annot.mv true evl
        | _ -> S.nil
    end

let integer =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Zero -> S.return @@ Annot.mv 0 evl
        | E.Integer n -> S.return @@ Annot.mv n evl
        | _ -> S.nil
    end

let int32 =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Zero -> S.return @@ Annot.mv 0l evl
        | E.Integer n -> S.return @@ Annot.mv (Int32.of_int n) evl
        | E.Int64 n ->
            if n <= Int64.of_int32 Int32.max_int ||
               n >= Int64.of_int32 Int32.min_int
            then
                S.return @@ Annot.mv (Int64.to_int32 n) evl
            else
                S.nil
        | _ -> S.nil
    end

let int64 =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Zero -> S.return @@ Annot.mv 0L evl
        | E.Integer n -> S.return @@ Annot.mv (Int64.of_int n) evl
        | E.Int64 n -> S.return @@ Annot.mv n evl
        | _ -> S.nil
    end

let float =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Zero -> S.return @@ Annot.mv 0.0 evl
        | E.Integer n -> S.return @@ Annot.mv (float_of_int n) evl
        | E.Int64 n -> S.return @@ Annot.mv (Int64.to_float n) evl
        | E.Float n -> S.return @@ Annot.mv n evl
        | _ -> S.nil
    end

let string =
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.String s -> S.return @@ Annot.mv s evl
        | _ -> S.nil
    end

let signal x =
    let x = (x :> Json_event.signal) in
    let* evl = event in begin
        match[@warning "-4"] Annot.dn evl with
        | E.Signal x0 when x0 == x -> S.return @@ Annot.mv () evl
        | _ -> S.nil
    end

module Chain = struct
    module Basis = struct
        type position = Cf_annot.Textual.position
        type symbol = Json_event.t
        type 'a form = 'a Annot.form

        module Scan = S
    end

    include Cf_chain_scan.Create(Basis)
end

let chain ?xf () = Chain.mk ?xf (signal `Con_element)

let syn_array = signal `Syn_array
let fin_array = signal `Fin_array |> S.or_fail "expecting end of array"

let syn_object = signal `Syn_object
let fin_object = signal `Fin_object |> S.or_fail "expecting end of object"

let group kind content =
    let syn, fin =
        match kind with
        | `Array -> syn_array, fin_array
        | `Object -> syn_object, fin_object
    in
    let* synl = syn in
    let* v = content in
    let* finl = fin in
    S.return @@ Annot.span synl finl v

module Opaque = struct
    type mode = Mode of {
        strings: [ `Text | `String ]
    }

    let mode =
        let or_text opt =
            match opt with
            | Some strings -> (strings :> [ `Text | `String ])
            | None -> `Text
        in
        let enter ?strings () =
            let strings = or_text strings in
            Mode { strings }
        in
        enter

    let value =
        let chain = chain () in
        let array_type_nym = Cf_type.(Seq Opaque) in
        let object_type_nym = Cf_type.(Seq (Pair (Opaque, Opaque))) in
        let v_string (Mode m) =
            let* textl = string in
            S.return begin
                match m.strings with
                | `Text ->
                    Annot.map (Cf_type.witness Ucs_type.Text) textl
                | `String ->
                    let Ucs_text.Octets s = Annot.dn textl in
                    let opaque = Cf_type.witness Cf_type.String s in
                    Annot.mv opaque textl
            end
        in
        let v_scalar mode =
            S.altz @@ Array.to_seq [|
                S.ntyp Cf_type.Unit null;
                S.ntyp Cf_type.Bool boolean;
                S.ntyp Cf_type.Int integer;
                S.ntyp Cf_type.Float float;
                v_string mode;
            |]
        in
        let v_array fix =
            let* synl = syn_array in
            let* vls = Chain.seq ~c:chain fix in
            let* finl = fin_array in
            S.return @@
                Annot.span synl finl @@
                Cf_type.(witness array_type_nym) @@
                Seq.map Annot.dn @@
                List.to_seq vls
        in
        let v_field mode fix =
            let* kl = v_string mode in
            let* _ = signal `Con_member in
            let* vl = fix in
            S.return (Annot.dn kl, Annot.dn vl)
        in
        let v_object mode fix =
            let* synl = syn_object in
            let* kvs =
                Chain.seq ~c:chain ?a:None ?b:None @@ v_field mode fix
            in
            let* finl = fin_object in
            S.return @@
                Annot.span synl finl @@
                Cf_type.witness object_type_nym @@
                List.to_seq kvs
        in
        let rec fix mode depth =
            if depth < 1000 then begin
                S.opt @@ v_scalar mode >>= function
                | Some v ->
                    S.return v
                | None ->
                    let fix = fix mode (succ depth) in
                    S.alt [ v_array fix; v_object mode fix ]
            end
            else
                S.fail "nested too deeply"
        in
        let default_mode = mode () in
        let enter ?(mode = default_mode) () =
            fix mode 0
        in
        enter
end

module Ingest = struct
    module D = Cf_data_ingest

    module Basis = struct
        type position = Cf_annot.Textual.position
        type symbol = Json_event.t
        type 'k frame = Frame of [ `Array | `Object ] * D.occurs

        module Form = Aux.Form
        module Scan = S

        let primitive: type a. a Cf_type.nym -> a Form.t S.t = fun nym ->
            match nym with
            | Cf_type.Unit ->
                null
            | Cf_type.Bool ->
                boolean
            | Cf_type.Int ->
                integer
            | Cf_type.Int32 ->
                int32
            | Cf_type.Int64 ->
                int64
            | Cf_type.Float ->
                float
            | Ucs_type.Text ->
                string
            | Cf_type.String ->
                let+ textl = string in
                let Ucs_text.Octets s = Form.dn textl in
                Form.mv s textl
            | Cf_type.Opaque ->
                Opaque.value ()
            | _ ->
                invalid_arg (__MODULE__ ^ ": primitive undefined for JSON!")

        let control:
            type a b. a Form.t S.t -> (a, b) D.control -> b Form.t S.t
            = fun scan control ->
                match control with
                | D.Cast f ->
                    Scan.cast f scan
                | _ ->
                    invalid_arg
                        (__MODULE__ ^ ": unrecognized JSON control variant!")

        let start:
            type k. k D.container -> D.occurs -> k frame Form.t S.t
            = fun container occurs ->
                match[@warning "-4"] container with
                | `Structure nym
                | `Variant nym ->
                    begin
                        match nym with
                        | Cf_type.String
                        | Ucs_type.Text ->
                            let+ p = syn_object in
                            Form.mv (Frame (`Object, occurs)) p
                        | _ ->
                            let+ p = syn_array in
                            Form.mv (Frame (`Array, occurs)) p
                    end
                | _ ->
                    let+ p = syn_array in
                    Form.mv (Frame (`Array, occurs)) p

        let finish frame =
            match frame with
            | Frame (`Array, _) -> fin_array
            | Frame (`Object, _) -> fin_object

        let chain_ = chain ()
        let imp_ = Form.imp ()

        let con_member = signal `Con_member |> S.or_fail "expecting colon"
        let con_element = signal `Con_element |> S.or_fail "expecting comma"

        let visit _container frame =
            let Frame (_, D.Occurs { a; b }) = frame in
            Chain.vis ?a ?b ~c:chain_

        let limit_memo =
            let v_scalar =
                let* evl = event in
                match[@warning "-4"] Form.dn evl with
                | E.Null
                | E.False
                | E.True
                | E.Zero
                | E.Integer _
                | E.Float _
                | E.String _ ->
                    S.return @@ Form.mv () evl
                | _ ->
                    S.nil
            in
            let v_array ?b fix =
                let* p0 = syn_array in
                let* _ = Chain.vis ~c:chain_ ?b (fun _ -> fix) imp_ in
                let+ p1 = fin_array in
                Form.span p0 p1 ()
            in
            let v_field fix _ =
                let* evl = event in
                match[@warning "-4"] Form.dn evl with
                | E.String _ ->
                    let* _ = con_member in
                    fix
                | _ ->
                    S.nil
            in
            let v_object ?b fix =
                let* p0 = syn_object in
                let* _ = Chain.vis ~c:chain_ ?b (v_field fix) imp_ in
                let+ p1 = fin_object in
                Form.span p0 p1 ()
            in
            let rec fix limit depth =
                if depth < 1000 then begin
                    S.opt v_scalar >>= function
                    | Some vl ->
                        S.return vl
                    | None ->
                        let fix = fix limit (succ depth) and b = limit in
                        S.alt [ v_array ?b fix; v_object ?b fix ]
                end
                else
                    S.fail "nested too deeply"
            in
            let enter limit =
                let* mark = S.cur in
                let+ form = fix limit 0 in
                Form.mv mark form
            in
            enter

        let memo = limit_memo None

        let pair:
            type k v. k D.pair -> k Form.t S.t -> v Form.t S.t ->
                (k * v) Form.t S.t
            = fun tag kscan vscan ->
                match[@warning "-4"] tag with
                | `Structure Ucs_type.Text
                | `Variant Ucs_type.Text ->
                    let* kl = kscan in
                    let* _ = con_member in
                    let+ vl = vscan in
                    Form.(span kl vl (dn kl, dn vl))
                | `Structure Cf_type.String
                | `Variant Cf_type.String ->
                    let* kl = kscan in
                    let* _ = con_member in
                    let+ vl = vscan in
                    Form.(span kl vl (dn kl, dn vl))
                | _ ->
                    let* p0l = S.or_fail "required array start" syn_array in
                    let* kl = S.or_fail "required key element" kscan in
                    let* _ = con_element in
                    let* vl = S.or_fail "required value element" vscan in
                    let+ p1l = S.or_fail "required array finish" fin_array in
                    Form.(span p0l p1l (dn kl, dn vl))
    end

    include Cf_data_ingest.Create(Basis)

    let index =
        let cmp = String.compare and model = D.primitive Cf_type.String in
        new D.index ~cmp ~model Cf_type.String
end

(*--- BEGIN OBSOLESCENT ---*)

let value =
    let chain = chain () in
    let v_scalar =
        S.altz @@ Array.to_seq [|
            S.ntyp Cf_type.Unit null;
            S.ntyp Cf_type.Bool boolean;
            S.ntyp Cf_type.Int integer;
            S.ntyp Cf_type.Float float;
            S.ntyp Ucs_type.Text string;
        |]
    in
    let v_array fix =
        let* synl = syn_array in
        let* vls = Chain.seq ~c:chain fix in
        let* finl = fin_array in
        S.return @@
            Annot.span synl finl @@
            Cf_type.(witness (Seq Opaque)) @@
            Seq.map Annot.dn @@
            List.to_seq vls
    in
    let v_field fix =
        let* kl = string in
        let* _ = signal `Con_member in
        let* vl = fix in
        S.return (Annot.dn kl, Annot.dn vl)
    in
    let v_object fix =
        let* synl = syn_object in
        let* kvs = Chain.seq ~c:chain ?a:None ?b:None @@ v_field fix in
        let* finl = fin_object in
        S.return @@
            Annot.span synl finl @@
            Cf_type.(witness (Seq (Pair (Ucs_type.Text, Opaque)))) @@
            List.to_seq kvs
    in
    let rec fix depth =
        if depth > 0 then begin
            S.opt v_scalar >>= function
            | Some v ->
                S.return v
            | None ->
                let fix = fix (pred depth) in
                S.alt [ v_array fix; v_object fix ]
        end
        else
            S.fail "nested too deeply"
    in
    fix 1000

let of_text p t =
    Ucs_text.to_seq t |> S.of_seq_staged ?start:None begin
        let* v = S.or_fail "invalid text" p in
        let* _ = S.opt space in
        let* b = S.fin in
        if not b then S.fail "unexpected trailing text" else S.return v
    end

module Object = struct
    module Aux = struct
        type symbol = Json_event.t
        type position = Cf_annot.Textual.position
        module Index = Ucs_text
        module Content = Ucs_type
        module Form = Aux.Form

        module Scan = struct
            include S

            let index = string >>: Annot.dn
            let preval = Some (signal `Con_member >>: Annot.dn)
            let chain = Some (`Non, `Non, signal `Con_element >>: Annot.dn)
        end
    end

    include Cf_record_scan.Create(Aux)
end

(*--- END OBSOLESCENT ---*)

include S

(*--- End ---*)
