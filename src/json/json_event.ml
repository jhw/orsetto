(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

type signal = [
    | `Syn_array
    | `Fin_array
    | `Syn_object
    | `Fin_object
    | `Con_element
    | `Con_member
]

let signal_equal a b = begin[@warning "-4"]
    match a, b with
    | `Syn_array, `Syn_array
    | `Fin_array, `Fin_array
    | `Syn_object, `Syn_object
    | `Fin_object, `Fin_object
    | `Con_element, `Con_element
    | `Con_member, `Con_member ->
        true
    | _ ->
        false
end

type t =
    | Space
    | Null
    | False
    | True
    | Zero
    | Integer of int
    | Int64 of int64
    | Float of float
    | String of Ucs_text.t
    | Signal of signal

let equal a b = begin[@warning "-4"]
    match a, b with
    | Space, Space
    | Null, Null
    | False, False
    | True, True
    | Zero, Zero ->
        true
    | Integer a, Integer b ->
        a == b
    | Int64 a, Int64 b ->
        Int64.equal a b
    | Float a, Float b ->
        Float.equal a b
    | String a, String b ->
        Ucs_text.equal a b
    | Signal a, Signal b ->
        signal_equal a b
    | _ ->
        false
end

let space = Space
let null = Null
let boolean = function true -> True | false -> False
let signal s = Signal (s :> signal)
let zero = Zero
let integer n = if n <> 0 then Integer n else Zero

let int64 n =
    match n with
    | 0L -> Zero
    | n when n <= Int64.of_int max_int -> Integer (Int64.to_int n)
    | n when n >= Int64.of_int min_int -> Integer (Int64.to_int n)
    | _ -> Int64 n

let string s = String s

let fp_intmin = float_of_int min_int
let fp_intmax = float_of_int max_int

let float n =
    match classify_float n with
    | FP_infinite -> invalid_arg (__MODULE__ ^ ": infinity!")
    | FP_nan -> invalid_arg (__MODULE__ ^ ": not a number!")
    | FP_zero -> Zero
    | FP_subnormal -> Float n
    | FP_normal ->
        if
            classify_float (mod_float n 1.0) = FP_zero && n >= fp_intmin &&
            n <= fp_intmax
        then
            Integer (int_of_float n)
        else
            Float n

let unsafe_float n = Float n
let unsafe_string s = String s

(*--- End ---*)
