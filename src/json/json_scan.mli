(*---------------------------------------------------------------------------*
  Copyright (C) 2019-2022, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** JavaScript Object Notation (JSON) input scanner. *)

(** {6 Overview}

    Functions that scan Unicode text for JSON interchange language syntax. A
    low-level lexical token scanner is provided for monadic parsing of any
    valid JSON text, including infinite streams. Higher-level scanners are
    provided for composing bespoke parsers according to a required schema.
    A scanner is also provided for input of arbitrary finite JSON values.
*)

(** {6 Lexical Analyzer} *)

(** A convenient alias for the UTF-8 text annotation system. *)
module Annot = Ucs_scan.UTF8.Annot

(** The lexical analyzer that recognizes JSON events. *)
val lexer: Json_event.t Annot.form Ucs_scan.UTF8.t

(** {6 Parser} *)

(** Include the scanner profile. The first stage scanner is the lexical
    analyzer augmented to raise [Bad_syntax] on failure to recognize a valid
    event.
*)
include Cf_scan.Staging.Profile
   with type symbol := Uchar.t
    and type token := Json_event.t
    and type position := Cf_annot.Textual.position
    and type 'a form := 'a Annot.form

(** The parser that returns the next annotated event that is not {b space}. *)
val event: Json_event.t Annot.form t

(** The parser that recognizes a {b space} event. *)
val space: unit Annot.form t

(** {4 Scalar Values}

    The following parsers all ignore any white space encountered in the input
    before recognizing the scalar value.
*)

(** A parser that recognizes the {b null} literal. *)
val null: unit Annot.form t

(** The parser that recognizes either the {b true} or {b false} literal and
    returns the value of annotated boolean value accordingly.
*)
val boolean: bool Annot.form t

(** The parser that recognizes a {b number} within the range of an Ocaml
    integer  and returns the annotated value accordingly.
*)
val integer: int Annot.form t

(** The parser that recognizes a {b number} within the range of an Ocaml
    {b int32}  and returns the annotated value accordingly.
*)
val int32: int32 Annot.form t

(** The parser that recognizes a {b number} within the range of an Ocaml
    {b int64}  and returns the annotated value accordingly.
*)
val int64: int64 Annot.form t

(** The parser that recognizes a {b number} within the range of Ocaml floating
    point numbers and returns the annotated float value accordingly.
*)
val float: float Annot.form t

(** The parser that recognizes a {b string} literal and returns the annotated
    Unicode text with all the escaped sequences converted.
*)
val string: Ucs_text.t Annot.form t

(** Use [signal x] to make a parser that recognizes the event signal [x]. *)
val signal: [< Json_event.signal ] -> unit Annot.form t

(** {4 Aggregate Values}

    The following interfaces are provided to facilitate various strategies for
    scanning arrays and objects.
*)

(** The chain scanner module. *)
module Chain: Cf_chain_scan.Profile
   with type symbol := Json_event.t Annot.form
    and type mark := mark
    and type 'a t := 'a t
    and type 'a form := 'a

(** Use [chain ()] to make a chain discipline for array and object content,
    which is a list of elements separated by comma characters.
*)
val chain: ?xf:(mark -> 'x) -> unit -> Chain.chain

(** Use [group kind content] to make a parser that recognizes an {b array} or
    an {b object} form according to [kind] with content that must be recognized
    by [content].

    For example, use [float |> seq ~a:1 ?b |> group `Array] to scan an array of
    one or more floating point numbers.

    Alternatively, use [Structure.scan] to scan a JSON object as a structure.
*)
val group: [< `Array | `Object ] -> 'r t -> 'r Annot.form t

(** {4 Opaque Types} *)

(** A submodule containing logic for parsing JSON texts to values of type
    [Cf_type.opaque] according to optional mode selectors.
*)
module Opaque: sig

    (** The mode selectors for scanning JSON texts to opaque values. *)
    type mode

    (** Use [mode ()] to create a mode selector record for the opaque value
        scanner. Use any of the various optional parameters to set a mode
        selector to other than its default value. The modes are as follows:

        - {strings}: Controls how JSON strings are decoded, either as
            [Ucs_text.t] (the default) or as [string].
    *)
    val mode: ?strings:[< `Text | `String ] -> unit -> mode

    (** Use [value ()] to create an opaque value parser. The following table
        describes the runtime type indications produced for values decoded.

        - {b null}:     [Cf_type.Unit]
        - {b boolean}:  [Cf_type.Bool]
        - {b number}:   [Cf_type.Int] or [Cf_type.Float]
        - {b string}:   [Ucs_type.Text]
        - {b array}:    [Cf_type.(Seq Opaque)]
        - {b object}:   [Cf_type.(Seq (Pair (Opaque, Opaque))]

        Use the [~mode] parameter to select modes other than the default.

        Note well: the {b array} and {b object} containers are sequences of
        untyped values, which must be unpacked recursively. Exceeding a maximum
        recursion depth of 1000 raises [Bad_syntax].
    *)
    val value: ?mode:mode -> unit -> Cf_type.opaque Annot.form t
end

(** {6 Abstract Data Ingestion} *)

(** The data ingestor module specialized for JSON text. *)
module Ingest: sig

    (** The data ingest index for JSON objects with string member names. *)
    val index: string Cf_data_ingest.index

    (** Use [scan m] to make a parser that recognizes the data model [m]. *)
    val scan: 'a Cf_data_ingest.model -> 'a Annot.form t
end

(** {6 Utility} *)

(** Use [of_text p t] to parse the Unicode text [s] with [p]. *)
val of_text: 'a t -> Ucs_text.t -> 'a

(** (4 Obsolescent}

    The following interfaces are obsolescent with attributes noting their
    preferable alternatives. They may be removed in a future revision.
*)

(** An opaque value parser created by [Opaque.value] with default mode, which
    maps its output so that objects are witnessed by the following type nym
    instead:

        [Cf_type.(Seq (Pair (Ucs_type.Text, Opaque))
*)
val value: Cf_type.opaque Annot.form t
    [@@ocaml.alert deprecated "Use Opaque.value instead!"]

(** The record scanner module for JSON object content. *)
module Object: Cf_record_scan.Profile
   with type index := Ucs_text.t
    and type 'a form := 'a Annot.form
    and type 'a t := 'a t
    [@@ocaml.alert deprecated "Use Ingest instead."]

(*--- End ---*)
