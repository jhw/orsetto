(*---------------------------------------------------------------------------*
  Copyright (C) 2018-2019, james woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*)

(** JavaScript Object Notation (JSON) events. *)

(** {6 Overview}

    As defined in ECMA-404 and RFC 8259, a JSON text is a sequence of Unicode
    characters conforming to a formally defined grammar comprising structured
    data.

    This module provides a safe interface for constructing valid JSON events
    used by the scanners and emitters defined in {Json_scan} and {Json_emit}.
*)

(** {6 Types} *)

(** The type of sequencing signals. *)
type signal = [
    | `Syn_array
    | `Fin_array
    | `Syn_object
    | `Fin_object
    | `Con_element
    | `Con_member
]

(** The private type of JSON events. *)
type t = private
    | Space
    | Null
    | False
    | True
    | Zero
    | Integer of int
    | Int64 of int64
    | Float of float
    | String of Ucs_text.t
    | Signal of signal

(** Equivalence relation *)
include Cf_relations.Equal with type t := t

(** {6 Constructors} *)

(** The distinguished "space" event. *)
val space: t

(** The distinguished "null" event. *)
val null: t

(** Use [signal s] to make an event signaling [s]. *)
val signal: [< signal ] -> t

(** Use [boolean b] to select either the distinguished "true" or "false" event
    according to [b].
*)
val boolean: bool -> t

(** The distinguished numeric zero event. *)
val zero: t

(** Use [integer n] to make a numeric event [Integer n], or [Zero] in the
    special case that [n = 0].
*)
val integer: int -> t

(** Use [int64 n] to make a numeric event [Int64 n], [Integer n] or [Zero] in
    the special case that [n = 0].
*)
val int64: int64 -> t

(** Use [float n] to make a numeric event representing [n]. If [n] is a zero,
    then the result is [Zero]. Otherwise if the value of [n] can be represented
    as an OCaml integer without overflow or truncation, then the result is
    [Integer (Float.to_int n)]. Otherwise, if [n] is a number and is not an
    infinity, then [Float n] is the result. Finally, in the cases where [n] is
    an infinity or not a number, raises [Invalid_argument].
*)
val float: float -> t

(** Use [string t] to make [String t]. *)
val string: Ucs_text.t -> t

(**/**)
val unsafe_float: float -> t
val unsafe_string: Ucs_text.t -> t

(*--- End ---*)
