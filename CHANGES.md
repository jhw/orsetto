## Version 1.2~dev

Improvements

- [ORS-93](https://conjury.atlassian.net/browse/ORS-93):
    Require OCaml 4.10 or later (support for 4.08 withdrawn).

- [ORS-96](https://conjury.atlassian.net/browse/ORS-96):
    Adopt the Conjury provisional v3 API in the build logic.

## Version 1.1.4

Improvements

- [ORS-115](https://conjury.atlassian.net/browse/ORS-115):
    Upgrade from Unicode 15.0 to Unicode 15.1.

## Version 1.1.3

Corrections

- [ORS-108](https://conjury.atlassian.net/browse/ORS-108):
    Compatibility with OCaml 5.0

- [ORS-110](https://conjury.atlassian.net/browse/ORS-110):
    Scheme compilation for all container models in `Cf_data_ingest` and
    `Cf_data_render` is now lazy rather than eager, to better facilitate models
    of tree data structures.

## Version 1.1.2

Improvements

- [ORS-103](https://conjury.atlassian.net/browse/ORS-103):
    New `Cf_data_render.defer` and `Cf_data_ingest.defer` constructor functions
    to facilitate construction of recursive data models. Scheme compilation for
    variant and choice models is now lazy rather than eager.

Corrections

- [ORS-69](https://conjury.atlassian.net/browse/ORS-69):
    Improved test coverage of CBOR. Also corrects the following logic errors
    uncovered by new tests:

    * Exception propagation from `Cf_decode.chars_to_vals` and cognates.
    * Decoding indefinite length groups with `Cbor_flyweight`.
    * Decoding undefined well-formed objects in `Cbor_flyweight`.
    * Calculation of annotations of decoded maps with `Cbor_decode.group`.
    * Decoding to `int32` and `int64` OCaml values.
    * Improved error handling in `Cbor_notation` emitter.
    * Encoding of raw OCaml strings as CBOR octets with `Cbor_encode.Render`.

- [ORS-90](https://conjury.atlassian.net/browse/ORS-90):
    Correction to `Cf_stdtime.create` for validating the input as UTC, i.e. to
    allow positive leap seconds only when they have been announced in the leap
    second archive, and to reject second=59 when it was announced as a negative
    leap second.

- [ORS-98](https://conjury.atlassian.net/browse/ORS-98):
    Correction to `Cf_tai64.to_unix_time` to produce correct outputs for inputs
    prior to ten seconds before the start of the Unix epoch.

- [ORS-105](https://conjury.atlassian.net/browse/ORS-105):
    Stop using OCaml API deprecated in the 4.14 release.

- [ORS-106](https://conjury.atlassian.net/browse/ORS-106):
    Upgrade from Unicode 14.0 to Unicode 15.0.

## Version 1.1.1

Improvements

- [ORS-97](https://conjury.atlassian.net/browse/ORS-97):
    Upgrade from Unicode 13.0 to Unicode 14.0.

## Version 1.1

Improvements

- [ORS-4](https://conjury.atlassian.net/browse/ORS-4):
    Improved handling of the NIST leap second archive, with introduction of the
    `Cf_leap_second` module, and the `load_from_nist_file` function.

- [ORS-41](https://conjury.atlassian.net/browse/ORS-41):
    Add the `?sort:unit` option to `Cbor_encode.map` for sorting the keys in
    deterministically encoded order. Use this in the `Opaque.value` encoder.

- [ORS-49](https://conjury.atlassian.net/browse/ORS-49):
    New module `Cbor_notation` with emitters for the CBOR diagnostic notation.

- [ORS-56](https://conjury.atlassian.net/browse/ORS-56):
    Add `Reserved` case to `Cbor_event.t` for expressly recognized reserved
    simple values as distinct from octets with `Undefined` encoding.

- [ORS-65](https://conjury.atlassian.net/browse/ORS-65):
    Augment `Cf_relations` for use with extensible types.

- [ORS-67](https://conjury.atlassian.net/browse/ORS-67): *breaking change*
    Refinements to CBOR encoder programming interface.
    - Use `list` instead of `array` for CBOR arrays and maps.
    - Don't use `Cf_type.Form` functions in the `Opaque.value` function.
    - Minor improvement to Unicode transport usage of `Cf_encode`.

- [ORS-68](https://conjury.atlassian.net/browse/ORS-68):
    Improve CBOR support for encoding and decoding `int32` and `int64` values.

- [ORS-73](https://conjury.atlassian.net/browse/ORS-73): *breaking change*
    Dropping support for OCaml 4.06 and adopting features new in OCaml 4.08.
    - Several conversions in `Cf_seq` deprecated in favor of `Stdlib`.
    - New binding operators for monad operations.
        - Renamed `Infix` to `Affix` in various `Profile` signatures.
        - Add deprecated `Infix` aliases of `Affix` modules and module types.
        - Require a new `product` entry in `Basis` for all monad signatures.
        - Revise all components to prefer binding operators over infix.
        - Use `@@` instead of `|>` where readability is improved.

- [ORS-76](https://conjury.atlassian.net/browse/ORS-76):
    Improved tests for `Cf_decode` and `Cf_encode`.
    - Corrects errors in `sat`, `pair` and `seq`.

- [ORS-78](https://conjury.atlassian.net/browse/ORS-73):
    Exhaustive tests for `Cf_type`, `Ucs_type` and `Cbor_type`.

- [ORS-79](https://conjury.atlassian.net/browse/ORS-79): *breaking change*
    A closely related set of improvements to `Cf_annot` for structured
    interchange of annotation metadata.

    - Add the `Meta` submodule to `Cf_annot`.
    - Add the `Meta` submodules to `Cf_annot.Basis` and `Cf_annot.Profile`
      module types.
    - Rename `position0` to `default_initial_position` in the `Cf_annot.Basis`
      module type.
    - Add `symbol_type` to the `Cf_annot.Coded.Basis` and
      `Cf_annot.Textual.Basis` module types, which require it for constructing
      the `Meta.Basis` module needed by the profile.
    - Add `Cbor_decode.Opaque` and `Cbor_encode.Opaque`.
    - Deprecated `Cbor_decode.value` and `Cbor_encode.value`.
    - Add `Json_scan.Opaque` and `Json_emit.Opaque`.
    - Deprecated `Json_emit.to_text`, `Json_emit.value` and `Json_scan.value`.

    Defining a new annotation scheme entails defining how annotated positions
    are structured by instantiating a Meta module in the argument to
    `Cf_annot.Create`.

- [ORS-82](https://conjury.atlassian.net/browse/ORS-82):
    Add the `Cf_annot.Form.up` function.

- [ORS-84](https://conjury.atlassian.net/browse/ORS-84):
    Revise the `Set.Create` and `Map.Create` functors in `Cf_bsearch_data` to
    accept a parameter of signature `Cf_relations.Order` (renaming the existing
    functors to `Of_ordered` respectively). Also added similar `Set.Create` and
    `Map.Create` functors to the `Cf_disjoint_interval` module. Also, added new
    deprecation warnings for various bits and bobs that are private use only,
    and which serve only to clutter the public interface. Added distinguished
    variants of disjoint interval sets and maps with `float` search keys.

- [ORS-83](https://conjury.atlassian.net/browse/ORS-87):
    Introduce the `Cf_data_ingest` and `Cf_data_render` modules for composing
    generalized data models for input and output according to an abstraction of
    structure data interchange languages. Also, specializations for JSON and
    CBOR are included. With unit tests.

Corrections

- [ORS-50](https://conjury.atlassian.net/browse/ORS-50):
    Expose `Cf_annot.Textual.position0` for creating the initial position of a
    named stream.

- [ORS-54](https://conjury.atlassian.net/browse/ORS-54):  *breaking change*
    The `Cbor_type.Tag` type nym variant must not include the CBOR tag number.
    Instead the type nym must witness the pair of a tag number with the value
    that it tags. (This breaking change is considered acceptable because the
    CBOR decoder is still provisional in the Orsetto 1.0 version series).

- [ORS-55](https://conjury.atlassian.net/browse/ORS-55):
    The `Cbor_decode.tag` function should have `int32 Annot.form t` type.

- [ORS-64](https://conjury.atlassian.net/browse/ORS-64): *breaking change*
    Replace `Cf_seq.predicate` with `Cf_seq.has_all` which is the same except
    returns [false] for an empty sequence rather than [true]. Also introduces
    `Cf_seq.has_none` and `Cf_seq.has_some`.

- [ORS-78](https://conjury.atlassian.net/browse/ORS-73):
    Corrections for logic error in type witnessing some cases, e.g.
    `Seq Reserved` where a container in the `Cf_type.horizon` is not adequately
    checked for equivalence of type parameters for types in `Ucs_type.horizon`.

- [ORS-80](https://conjury.atlassian.net/browse/ORS-80): *breaking change*
    Refactored the `Cf_type.horizon` class and how it composes modules of the
    `Cf_type.Form` signature, including the ones included in `Cf_type`,
    `Ucs_type`, and `Cbor_type`.

- [ORS-81](https://conjury.atlassian.net/browse/ORS-81):
    Correction for extremely bad performance in `Cf_encode.to_string`.

- [ORS-85](https://conjury.atlassian.net/browse/ORS-85):
    Correct CBOR encode/decode of indefinite length containers.

- [ORS-88](https://conjury.atlassian.net/browse/ORS-88):
    Correct the type of `Cf_scan.Profile.dflt` to return `'r form t` rather
    than `'r t`.

## Version 1.0.3

Corrections

- [ORS-51](https://conjury.atlassian.net/browse/ORS-51):
    Compatibility with OCaml 4.06 to 4.10.

- [ORS-52](https://conjury.atlassian.net/browse/ORS-52):
    Pad characters are optional in `base64url`.

- [ORS-53](https://conjury.atlassian.net/browse/ORS-53):
    The `Cbor_encode.text` function improperly encodes its argument as octets
    and not as text. (Note: the `Cbor_encode.text_seq` function does not have
    this problem.)

- [ORS-57](https://conjury.atlassian.net/browse/ORS-57):
    Correct error in `Cf_endian_core.to_fp16_bits_unsafe` with conversion of
    `float` to denormalized half-precision numbers. This function is used in
    the CBOR encoder.

- [ORS-58](https://conjury.atlassian.net/browse/ORS-58):
    Correct error in `Cf_decoder.window_scanner` with `~limit` option applied,
    which allowed uninitialized octets to be present in a working slice.

- [ORS-59](https://conjury.atlassian.net/browse/ORS-59):
    Correct error in `Cf_annot.Textual.Serial.compare` that caused joining of
    textual annotation forms to produce incorrect spans.

- [ORS-60](https://conjury.atlassian.net/browse/ORS-60):
    Correct error in `Cf_lex_scan.Profile.analyze` that caused lexemes to be
    returned with annotation forms that included the position of the character
    rejected by the DFA when the rule finishes.

- [ORS-61](https://conjury.atlassian.net/browse/ORS-61):
    Some uses of `Cf_decode.scheme` (in `Ucs_transport`, `Cbor_decode` and
    `Cbor_flyweight`) misused the `Cf_decode.analyze` function, allowing
    schemes to scan octets outside the working slice.

- [ORS-62](https://conjury.atlassian.net/browse/ORS-62):
    Upgrade from Unicode 12.0 to Unicode 13.0.

- [ORS-86](https://conjury.atlassian.net/browse/ORS-86):
    Raise `Invalid_argument` if the `~brk` argument is used with any of the
    `encode_xxxx` functions with invalid values, e.g. if the `check` function
    returns a value other than `Skip` for every character in the break string.
    Currently, only the `Cf_base64.Mime` variant allows break strings.

## Version 1.0.2

Corrections

- [ORS-48](https://conjury.atlassian.net/browse/ORS-48):
    Compatibility with OCaml 4.09.


## Version 1.0.1

Corrections

- [ORS-42](https://conjury.atlassian.net/browse/ORS-42):
    `Cf_scan.Staging` syntax errors in first stage must propagate correctly
    into the second stage.

- [ORS-43](https://conjury.atlassian.net/browse/ORS-43):
    `Cf_chain_scan` trailing separator consumed even when ~b:\`Non

- [ORS-44](https://conjury.atlassian.net/browse/ORS-44):
    `Json_scan.value` stack overflow when nesting level is too deep.

- [ORS-45](https://conjury.atlassian.net/browse/ORS-45):
    `Json_scan` lexical errors must be syntax errors in all value parsers.

- [ORS-46](https://conjury.atlassian.net/browse/ORS-46):
    `Json_scan.of_text` unexpected trailing text is a syntax error.
